
	tinymce.init({
        selector : '.editor',
        height : 400,
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table directionality emoticons template powerpaste a11ychecker tinymcespellchecker flags'
        ],
       
    content_css: 'css/content.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons a11ycheck spellchecker',
    spellchecker_language: 'en',
    spellchecker_dialog: true,
    branding: false,
    
    //image_uploadtab : true,
    // without images_upload_url set, Upload tab won't show up
    images_upload_url: '<?php echo base_url()?>admin/communications/images_upload',
    //relative_urls : false,
    //remove_script_host : false,
    //convert_urls : true,
    relative_urls : false,
    remove_script_host : true,
    document_base_url : "https://ennobledemos.com",
    media_alt_source: false,
    media_poster: false,
    // override default upload handler to simulate successful upload
    images_upload_handler:function(blobInfo, success, failure){
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '<?php echo base_url()?>admin/communications/images_upload');
        xhr.onload = function(){
            var json;
            if(xhr.status!=200){
                failure('Http Error: '+xhr.status);
            }
            json = JSON.parse(xhr.responseText);
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        
        xhr.send(formData);
    },
    
    
    
    setup:function () {
        let flags = (function () {
            'use strict';
            
            tinymce.PluginManager.add('flags', function (editor, url) {
        console.log("****************");
                editor.ui.registry.addAutocompleter('autocompleter-flags', {
                    ch: '#', // the trigger character to open the autocompleter
                    minChars: 2, // lower number means searching sooner - but more lookups as we go
                    columns: 1, // must be 1 for text-based results
                    fetch: function (pattern) {
                        // perform a lookup on the Pattern, and return the lookup data
                        return new tinymce.util.Promise(function (resolve) {
                            
                            // call the countries REST endpoint to look up the query, and return the name and flag
                            //https://restcountries.eu/rest/v2/name/' + pattern + '?fields=name
                            fetch('https://api.datamuse.com/words?sp=' + pattern + '*')
                                .then((resp) => resp.json()) // convert response to json
                                .then(function (data) {
                                    let results = [];
                                    console.log('hold on :'+data);
                                    // create our own results array
                                    for (let i = 0; i < data.length; i++)
                                    {
                                        let result = data[i];
                    
                                        results.push({
                                            value: result.word,
                                            text: result.word
                                            
                                        });
                                    }
                    
                                    // sort results by the "name"
                                    results.sort(function (a, b) {
                                        let x = a.text.toLowerCase();
                                        let y = b.text.toLowerCase();
                                        if (x < y)
                                        {
                                            return -1;
                                        }
                                        if (x > y)
                                        {
                                            return 1;
                                        }
                                        return 0;
                                    });
                    
                                    // resolve the initial promise
                                    resolve(results);
                                });
                        });
                    },
                    onAction: function (autocompleteApi, rng, value) {
                        // called when an item from the list is selected
                        // we still need to be the one who does the insert in to the editor
                        // split the value in to two parts - the name and the flag URL
                        // we joined it above using a pipe (|)
                        
                        
                        
                    
                        // make an image element
                        //let img = '<img src="' + flag + '" alt="' + name + '" width="48" height="24" />';
                    
                        // insert in to the editor
                        editor.selection.setRng(rng);
                        editor.insertContent(value);
                    
                        // hide the autocompleter
                        autocompleteApi.hide();
                    }
                });
            });
        }());
    }
	 });
	