-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 25, 2021 at 01:00 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sadevelopers`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(27, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers.</p>', '569a4be34c567b10dda004a9f3be2bc7.jpg', 0, '2021-01-08 20:36:08', '2021-01-08 15:00:20'),
(28, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers. </p>', 'f7ca628ee1604dfc8098a9961a1207fb.png', 1, '2021-01-08 20:36:08', '2021-01-08 15:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject_type` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `create_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(6, 'Cement ', 'In the construction industry, there are different types of cement. The differences between each type of cement are its properties, uses and composition materials used during the manufacturing process.  \r\n\r\nCement is a cover material which makes a bond between aggregates and reinforcing materials. Over the years, cement in Malaysia has further developed thanks to technology. ', '82c9a6cb0cfa5884eabc3c94916fb75d.jpg', 1, '2021-01-23 19:00:07', '2021-01-23 01:31:07'),
(7, 'fgh fghfgh', 'gfh fghfghfghfgh', '878162b31c6732a4ecccd4b03d783a37.jpg', 1, '2021-01-11 23:31:37', '2021-01-11 23:31:37'),
(8, 'dsg', 'dgdfg dfgdf', 'c734e4167bffabdc60e7074ac9d51879.jpg', 1, '2021-01-23 00:31:56', '2021-01-23 00:31:56');

-- --------------------------------------------------------

--
-- Table structure for table `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `claim_registration_id` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `pharmacy_id` int(5) DEFAULT NULL,
  `insurance_company_id` int(5) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `transaction_status` int(5) NOT NULL,
  `payment_status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claims`
--

INSERT INTO `claims` (`id`, `claim_registration_id`, `user_id`, `pharmacy_id`, `insurance_company_id`, `status`, `transaction_status`, `payment_status`, `created_date`, `updated_date`) VALUES
(1, 'CLA4475968', 30, 1, 4, 2, 10, 13, '2020-11-20 14:11:19', '2020-11-20 15:46:31'),
(2, 'CLA2911650', 30, 1, 6, 2, 9, 13, '2020-11-20 14:11:27', '2020-11-20 15:27:10'),
(3, 'CLA1555787', 1, 1, 4, 0, 7, 13, '2020-11-20 16:11:53', '2020-11-20 16:11:53'),
(4, 'CLA8476564', 30, 1, 5, 2, 7, 13, '2020-11-23 13:11:51', '2020-11-23 00:25:01'),
(5, 'CLA1136370', 30, 1, 6, 2, 7, 13, '2020-11-23 13:11:12', '2020-11-23 00:45:17'),
(6, 'CLA0991196', 31, 2, 4, 3, 10, 13, '2020-11-23 14:11:18', '2020-11-26 18:56:38'),
(7, 'CLA5200018', 31, 2, 5, 2, 8, 13, '2020-11-23 14:11:59', '2020-12-10 13:23:31'),
(8, 'CLA7515236', 31, 2, 4, 6, 12, 16, '2020-11-23 14:11:21', '2020-11-23 23:45:28'),
(9, 'CLA2686728', 32, 3, 4, 3, 8, 13, '2020-11-23 15:11:07', '2020-11-23 23:43:58'),
(10, 'CLA6077432', 32, 3, 4, 2, 10, 13, '2020-11-23 15:11:53', '2020-11-23 23:41:25'),
(11, 'CLA3233432', 32, 3, 6, 2, 8, 13, '2020-11-23 15:11:18', '2020-11-23 23:39:29'),
(12, 'CLA7934562', 33, 4, 11, 2, 9, 13, '2020-11-23 15:11:52', '2020-11-23 23:40:53'),
(13, 'CLA5553449', 33, 4, 10, 2, 9, 13, '2020-11-23 15:11:09', '2020-11-23 23:40:35'),
(14, 'CLA3100795', 33, 4, 9, 2, 12, 13, '2020-11-23 15:11:59', '2020-11-23 05:03:18'),
(15, 'CLA8463214', 34, 5, 7, 2, 12, 13, '2020-11-23 16:11:24', '2020-11-23 05:02:37'),
(16, 'CLA7469465', 34, 5, 9, 2, 8, 13, '2020-11-23 16:11:52', '2020-11-23 05:02:12'),
(17, 'CLA6164200', 34, 5, 8, 2, 9, 13, '2020-11-23 16:11:44', '2020-11-23 05:02:05'),
(18, 'CLA4880670', 35, 6, 10, 6, 12, 16, '2020-11-23 17:11:22', '2020-11-23 05:01:23'),
(19, 'CLA1493931', 35, 6, 11, 2, 12, 15, '2020-11-23 17:11:14', '2020-11-23 04:59:49'),
(20, 'CLA7824138', 35, 6, 11, 2, 11, 13, '2020-11-23 17:11:39', '2020-11-23 04:58:52'),
(21, 'CLA1848880', 36, 7, 7, 2, 10, 13, '2020-11-23 17:11:55', '2020-11-23 04:56:35'),
(22, 'CLA8243023', 34, 5, 10, 2, 12, 14, '2020-11-23 17:11:18', '2020-11-23 04:58:15'),
(23, 'CLA8588701', 34, 5, 9, 0, 7, 13, '2020-11-23 17:11:24', '2020-11-23 17:11:24'),
(24, 'CLA1683096', 1, 4, 7, 0, 7, 13, '2020-11-23 17:11:59', '2020-11-23 17:11:59'),
(25, 'CLA1778817', 1, 3, 11, 0, 7, 13, '2020-11-23 17:11:48', '2020-11-23 17:11:48'),
(26, 'CLA4130021', 1, 3, 10, 0, 7, 13, '2020-11-23 17:11:45', '2020-11-23 17:11:45'),
(27, 'CLA2472286', 1, 3, 9, 0, 7, 13, '2020-11-23 17:11:34', '2020-11-23 17:11:34'),
(28, 'CLA6153227', 1, 3, 3, 0, 7, 13, '2020-11-23 17:11:22', '2020-11-23 17:11:22'),
(29, 'CLA4844016', 1, 4, 10, 0, 7, 13, '2020-11-23 17:11:19', '2020-11-23 17:11:19'),
(30, 'CLA7937686', 35, 6, 8, 2, 9, 13, '2020-11-23 17:11:18', '2020-11-23 04:54:29'),
(31, 'CLA6364448', 35, 6, 11, 2, 8, 13, '2020-11-23 17:11:48', '2020-11-23 04:53:45'),
(32, 'CLA0215878', 35, 14, 11, 2, 8, 13, '2020-11-23 17:11:27', '2020-11-30 04:29:07'),
(33, 'CLA3016726', 1, 14, 4, 4, 12, 14, '2020-11-23 07:11:14', '2020-11-23 12:26:09'),
(34, 'CLA8587267', 1, 14, 4, 4, 12, 15, '2020-11-23 07:11:00', '2020-11-24 09:49:09'),
(35, 'CLA3147164', 1, 14, 4, 3, 10, 16, '2020-11-23 07:11:06', '2020-11-23 12:29:48'),
(36, 'CLA6384250', 1, 14, 5, 4, 12, 14, '2020-11-23 07:11:53', '2020-11-23 12:52:25'),
(37, 'CLA7683052', 1, 14, 5, 4, 12, 15, '2020-11-23 07:11:19', '2020-11-23 12:38:31'),
(38, 'CLA0096037', 1, 13, 5, 4, 9, 13, '2020-11-23 08:11:17', '2020-11-23 12:53:05'),
(39, 'CLA4252863', 1, 13, 5, 4, 12, 14, '2020-11-23 08:11:14', '2020-11-23 12:52:08'),
(40, 'CLA0648285', 1, 13, 4, 3, 10, 13, '2020-11-23 08:11:17', '2020-11-23 12:48:18'),
(41, 'CLA7963932', 1, 13, 4, 5, 11, 13, '2020-11-23 08:11:51', '2020-11-23 12:49:54'),
(42, 'CLA8620334', 1, 13, 5, 4, 9, 13, '2020-11-23 08:11:59', '2020-11-23 12:52:43'),
(43, 'CLA4017181', 1, 12, 6, 4, 12, 14, '2020-11-23 09:11:05', '2020-11-23 12:55:07'),
(44, 'CLA3764864', 1, 12, 6, 4, 12, 14, '2020-11-23 10:11:11', '2020-11-23 12:56:36'),
(45, 'CLA0590057', 1, 12, 1, 3, 10, 13, '2020-11-23 10:11:46', '2020-11-23 12:57:51'),
(46, 'CLA9279476', 1, 12, 2, 4, 12, 13, '2020-11-23 10:11:10', '2020-11-23 13:00:27'),
(47, 'CLA6341203', 1, 12, 3, 5, 11, 13, '2020-11-23 11:11:19', '2020-11-23 13:03:38'),
(48, 'CLA6837114', 1, 11, 3, 6, 12, 15, '2020-11-23 11:11:49', '2020-11-24 05:56:29'),
(49, 'CLA0639603', 1, 11, 2, 4, 12, 15, '2020-11-23 11:11:01', '2020-11-24 05:59:38'),
(50, 'CLA5727422', 1, 11, 6, 4, 12, 15, '2020-11-23 12:11:33', '2020-11-24 06:48:47'),
(51, 'CLA2507217', 1, 11, 5, 4, 12, 15, '2020-11-23 12:11:50', '2020-11-24 07:07:57'),
(52, 'CLA4543048', 1, 11, 4, 4, 12, 15, '2020-11-23 12:11:09', '2020-11-24 07:11:58'),
(53, 'CLA1370936', 1, 10, 4, 4, 12, 15, '2020-11-23 12:11:24', '2020-11-24 07:16:08'),
(54, 'CLA7835936', 1, 10, 5, 3, 10, 13, '2020-11-23 12:11:42', '2020-11-24 08:13:12'),
(55, 'CLA4630066', 1, 10, 1, 4, 12, 15, '2020-11-23 12:11:46', '2020-11-24 08:42:33'),
(56, 'CLA7914692', 1, 10, 1, 5, 11, 13, '2020-11-23 12:11:43', '2020-11-24 08:46:31'),
(57, 'CLA5836557', 1, 10, 3, 4, 12, 15, '2020-11-23 12:11:28', '2020-11-24 08:52:45'),
(58, 'CLA2149366', 1, 9, 4, 6, 12, 16, '2020-11-23 13:11:40', '2020-11-24 08:55:52'),
(59, 'CLA9682908', 1, 9, 6, 4, 12, 15, '2020-11-23 13:11:16', '2020-11-24 09:01:19'),
(60, 'CLA0105799', 1, 9, 2, 3, 10, 13, '2020-11-23 13:11:33', '2020-11-24 09:38:55'),
(61, 'CLA4521685', 1, 9, 3, 4, 12, 15, '2020-11-23 13:11:23', '2020-11-24 09:41:21'),
(62, 'CLA8088430', 1, 9, 2, 4, 12, 15, '2020-11-23 13:11:17', '2020-11-24 09:42:22'),
(63, 'CLA6418380', 34, 5, 3, 1, 7, 13, '2020-11-24 08:11:06', '2020-11-24 08:11:06'),
(64, 'CLA9068558', 43, 14, 3, 4, 12, 14, '2020-11-24 09:11:55', '2020-11-24 09:42:50'),
(65, 'CLA4318564', 43, 14, 3, 2, 9, 13, '2020-11-24 11:11:03', '2020-11-23 23:36:37'),
(66, 'CLA2243581', 43, 14, 3, 2, 12, 13, '2020-11-24 12:11:06', '2020-11-23 23:36:22'),
(67, 'CLA6239772', 1, 14, 4, 4, 12, 15, '2020-11-24 11:11:17', '2020-11-24 10:06:43'),
(68, 'CLA5274392', 1, 12, 5, 4, 12, 15, '2020-11-24 11:11:43', '2020-11-24 10:14:43'),
(69, 'CLA9522444', 1, 12, 2, 4, 12, 15, '2020-11-24 11:11:07', '2020-11-24 10:20:56'),
(70, 'CLA3499087', 1, 12, 1, 4, 12, 15, '2020-11-24 11:11:49', '2020-11-24 10:30:59'),
(71, 'CLA1442779', 1, 12, 2, 3, 10, 13, '2020-11-24 11:11:24', '2020-11-24 10:40:49'),
(72, 'CLA6577968', 1, 13, 4, 5, 11, 13, '2020-11-24 11:11:54', '2020-11-24 11:03:55'),
(73, 'CLA9852628', 1, 13, 2, 3, 10, 13, '2020-11-24 12:11:36', '2020-11-24 11:30:19'),
(74, 'CLA9400722', 1, 10, 6, 6, 12, 16, '2020-11-24 12:11:49', '2020-11-24 11:32:49'),
(75, 'CLA2232941', 1, 6, 2, 0, 7, 13, '2020-11-25 14:11:52', '2020-11-25 14:11:52'),
(76, 'CLA5645835', 1, 6, 1, 0, 7, 13, '2020-11-25 14:11:24', '2020-11-25 14:11:24'),
(77, 'CLA6939143', 35, 6, 4, 0, 7, 13, '2020-11-25 15:11:36', '2020-11-25 15:11:36'),
(78, 'CLA3021315', 1, 14, 6, 0, 7, 13, '2020-11-25 16:11:00', '2020-11-25 16:11:00'),
(79, 'CLA7022992', 1, 3, 3, 0, 7, 13, '2020-11-26 07:11:57', '2020-11-26 07:11:57'),
(80, 'CLA1161469', 37, 15, 11, 2, 9, 13, '2020-11-26 08:11:33', '2021-01-04 08:23:32'),
(81, 'CLA4277072', 37, 8, 11, 1, 7, 13, '2020-11-26 09:11:03', '2020-11-26 09:11:03'),
(82, 'CLA8563656', 43, 14, 2, 2, 7, 13, '2020-11-26 12:11:21', '2020-12-10 11:54:14'),
(83, 'CLA4540174', 43, 14, 2, 2, 9, 13, '2020-11-26 13:11:59', '2020-12-10 17:07:49'),
(84, 'CLA8376121', 1, 15, 5, 0, 7, 13, '2020-11-27 01:41:27', '2020-11-27 01:41:27'),
(85, 'CLA0971740', 1, 14, 5, 0, 7, 13, '2020-11-27 23:41:46', '2020-11-27 23:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `pharmacy_id` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `pharmacy_lbn_name` varchar(25) NOT NULL,
  `pharmacy_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `pharmacy_id`, `image`, `pharmacy_lbn_name`, `pharmacy_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR1302296', 'd28027c099f1a4c38008b6c2f6a3aba9.jpg', 'Batish Pharmacy', 'BTPPD', 'Batish Pharmacy', 'raghuvarma.testmail@gmail.com', '4558568222', '5685568', '', '', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', 1, '2020-11-20 13:11:00', NULL),
(2, 'PHR0043065', '852bbb1fbadca0f06381150c799d6ebe.jpg', 'Branch Brook Pharmacy', 'BBPBT', 'Branch Brook Pharmacy', 'branchbrooktest@gmail.com', '3434343434', '3434343', 'DEA343434', 'LN1212121', '917 Franklin Avenue', '', 'Newark', 'NJ', '07107-2809', '', 1, '2020-11-21 07:11:29', NULL),
(3, 'PHR8794968', 'a164dae69e3876096d2f0a47d3706f46.png', 'Mason Pharmacy', 'MPPPD', 'Mason Pharmacy', 'masontest@gmail.com', '5656564343', '4657678', 'DEA343434', 'LN3434343', '1255 Castle Hill Avenue', '', 'Bronx', 'NY', '10462-4813', '', 1, '2020-11-21 07:11:41', NULL),
(4, 'PHR4324832', '9da8f1ed15d32e894288f1d76d1e4ca0.jpg', 'Heidi Pharmacy', 'HPPPD', 'Heidi Pharmacy', 'heiditest@gmail.com', '5545656345', '1377765', 'DEA343434', 'LNMP43434', '522 West 181st Street', '', 'New York', 'NY', '10033-5101', '', 1, '2020-11-21 07:11:08', NULL),
(5, 'PHR0657257', '138d9b7d164abe37e6b20d0dd3fe50e6.png', 'Vim Drugs', 'VDPPD', 'Vim Drugs', 'vimtest@gmail.com', '4545454545', '4656565', 'DEA344454', 'LNVD34343', '3835 Broadway', '', 'New York', 'NY', '10032-1510', '', 1, '2020-11-21 07:11:52', NULL),
(6, 'PHR6113443', '4e9a38a1b3392a27a6de18bec0a81ca6.png', 'Mergewell Pharmacy', 'MPTCP', 'Mergewell Pharmacy', 'mergewelltest@gmail.com', '4334343434', '3434343', 'DEA343434', 'LNMP43434', '759 Washington Avenue', '', 'Brooklyn', 'NY', '11238-4504', '', 1, '2020-11-21 07:11:27', NULL),
(7, 'PHR8320501', 'ad3b69f6bcf7960307ebed528a76ecaa.png', 'Nostrand Pharmacy', 'NPPPD', 'Nostrand Pharmacy', 'nostrandtest@gmail.com', '5443343434', '8767656', 'DEA454545', 'LNNP34343', '524 Nostrand Avenue', '', 'Brooklyn', 'NY', '11216-2012', '', 1, '2020-11-21 07:11:40', NULL),
(8, 'PHR5589027', '5ec7e7278ee7b630e941f18566e2e3b0.png', 'Elmhurst Pharmacy', 'EPPPD', 'Elmhurst Pharmacy', 'elmhurstest@gmail.com', '3433434434', '3434534', 'DEA334343', 'LNEP34343', '75-23 Broadway', '', 'Elmhurst', 'NY', '11373-5612', '', 1, '2020-11-21 07:11:53', NULL),
(9, 'PHR1582089', '481327c096d195aaea60794417ed184c.png', 'Freedom Pharmacy', 'FPPPD', 'Freedom Pharmacy', 'freedomtest@gmail.com', '4545454544', '6565656', 'DEA343434', 'LNFP34343', '953 Frelinghuysen Avenue', '', 'Newark', 'NJ', '07114-2103', '', 1, '2020-11-21 07:11:57', NULL),
(10, 'PHR5966444', '67ac2d65c7660c230a85a8acc51cfba9.jpg', 'Lenda Pharmacy', 'LPPPD', 'Lenda Pharmacy', 'lendatest@gmail.com', '3434378784', '4545454', 'DEA454545', 'LNLP34343', '3397 Broadway', '', 'New York', 'NY', '10031-7416', '', 1, '2020-11-21 07:11:06', NULL),
(11, 'PHR8023521', 'erins_logo.jpg', 'Erins Pharmacy', 'EPPPD', 'Erins Pharmacy', 'erinstest@gmail.com', '3437698989', '3434343', 'DEA454545', 'LNEP34343', '360 EAST 4TH STREET', '', 'New York', 'NY', '10009-8501', '', 1, '2020-11-21 07:11:17', NULL),
(12, 'PHR0091840', 'ef6b7b3c025965839c86eff2a773a771.jpg', 'Hill Pharmacy', 'HPPPD', 'Hill Pharmacy', 'hilltest@gmail.com', '3434343443', '4454545', 'DEA454545', 'LNHP45454', '522 West 181ST Street', '', 'New York', 'NY', '10033', '', 1, '2020-11-21 07:11:12', NULL),
(13, 'PHR1362611', '7a349efa22e95b0da7826f7702d73bcd.jpg', 'Express Care Pharmacy', 'ECPPD', 'Express Care Pharmacy', 'expresscaretest@gmail.com', '4346909445', '4545454', 'DEA454545', 'LNECP3434', '15 East Tremont Avenue', '', 'Bronx', 'NY', '10453-5801', '', 1, '2020-11-21 07:11:14', NULL),
(14, 'PHR5557889', '033ecfc7b874daa7efda42905d2d185b.png', 'Carewell Pharmacy', 'CPPPD', 'Carewell Pharmacy', 'carewelltest@gmail.com', '4547454545', '5454343', 'DEA343434', 'LNCP93434', '826 East Tremont Avenue', '', 'Bronx', 'NY', '10460-4146', '', 1, '2020-11-21 08:11:36', NULL),
(15, 'PHR0378690', '', 'Care4u Pharmacy', 'C4PHCY', 'Care Pharmacy', 'info@care4upharmacy.com', '3224323455', '0000009', 'YU769JPO8', 'E4RE76JKL', 'Unit No: 78, Sunset Mathy, Berger Road,', 'Unit No: 78, Sunset Mathy, Berger Road,', 'Midrand', 'CA', '01234-0403', '', 1, '2020-11-26 14:11:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_files`
--

CREATE TABLE `communications_files` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `reply_id` varchar(25) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `contact` varchar(18) NOT NULL,
  `map` text NOT NULL,
  `fax_number` varchar(200) NOT NULL,
  `day_one` varchar(50) NOT NULL,
  `day_two` varchar(50) NOT NULL,
  `day_three` varchar(50) NOT NULL,
  `start_time` varchar(250) NOT NULL DEFAULT 'AM',
  `end_time` varchar(250) NOT NULL DEFAULT 'PM',
  `start_hour` varchar(250) NOT NULL DEFAULT 'AM',
  `end_hour` varchar(250) NOT NULL DEFAULT 'PM',
  `hours` varchar(250) NOT NULL DEFAULT '00:00:00',
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address1`, `title`, `description`, `address2`, `city`, `country`, `state`, `zip`, `status`, `email`, `phone`, `contact`, `map`, `fax_number`, `day_one`, `day_two`, `day_three`, `start_time`, `end_time`, `start_hour`, `end_hour`, `hours`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `create_date`, `update_date`) VALUES
(1, '917 Franklin Avenue', 'Contact Us', 'test', '', 'Newark', 'US', 'NJ', '07107-2809', 1, 'info@sadevelopers.com', '+1 (875) 423-4534', ' ', 'https://goo.gl/maps/q7weS', '+1 (973) 412 7303', '', '', '', '', '', '', '', '00:00:00', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.linkedin.com/               ', 'https://www.pinterest.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', '2019-03-21', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_replies`
--

CREATE TABLE `contactus_replies` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `reply_message` text NOT NULL,
  `replier_id` varchar(20) NOT NULL,
  `replier_name` varchar(255) NOT NULL,
  `read_status` int(20) NOT NULL DEFAULT '0',
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) NOT NULL,
  `file_source` varchar(255) NOT NULL,
  `document_id` varchar(255) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `pharmacy_name` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `pharmacy_name`, `contact_name`, `email_address`, `phone_number`, `notes`, `create_date`) VALUES
(1, 'dfhdfh', 'fhg dfhdfgh', 'raghuvarma.chintu@gmail.com', '', 'gdf dfgfdgdfgfd', '2020-11-23 06:43:12'),
(2, 'er tvreterter', 're ter tert', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'er treter ter tretre', '2020-11-23 06:46:07'),
(3, 'drghdfh', 'gfhfghfgh', 'raghuvarma.chintu@gmail.com', '', 'gyj fthfthfghfgh', '2020-11-23 06:54:30'),
(4, 'tyujygjgyj', 'gyuityuityi', 'raghuvarma.chintu@gmail.com', '+1 (445) 354-3413', 'tyity nrtfhftghnfgh', '2020-11-23 06:58:50'),
(5, 'fgh fghfgh', 'fgh fgh fghf', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'fgh fgh fgh fgh', '2020-11-23 06:59:24'),
(6, 'kjbkbkjb', 'hgjhvjhy', 'raghuvarma.testmail@gmail.com', '+1 (987) 456-3210', 'ygfuyuy', '2020-11-23 07:00:53'),
(7, 'dghdfghdf', 'fghgh', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', 'fghfgh fghgf', '2020-11-23 07:06:28'),
(8, 'sdfdf sd', ' sdfsdf', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', 'sdf sdfdsfsdf', '2020-11-23 07:07:20'),
(9, 'dsgsg', 'dfgfdgdfg', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'dfg dfgdfgfd', '2020-11-23 07:08:11'),
(10, 'ertyret', 'ertertert', 'raghuvarma.chintu@gmail.com', '+1 (678) 867-4853', 'ret ertretert', '2020-11-23 07:09:42'),
(11, 'BestHealth', 'Sushma', 'sushmah@ennoblegrp.com', '+1 (481) 818-2020', 'I would like to discuss and register ', '2020-11-26 13:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(5) NOT NULL,
  `feature_id` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `feature_id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'FEA7082958', 'Automated Refill Management System', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data</li>\r\n<li xss=removed>“At a glance,” visibility of all refill management processes through a Refill Compliance Dashboard. The dashboard can be customized to include preferred metrics, including scheduled refills, missed refills, expired refills, and Med Sync Rx.</li>\r\n<li xss=removed>Automatically queues each day’s scheduled refills</li>\r\n<li xss=removed>RxSync - Sync all refills to a particular date</li>\r\n<li xss=removed>Return to Stock Queue - Prescriptions not picked up by patients will be flagged, with the system easily updated to reflect missed pickups, and drugs added back</li>\r\n</ul>', '8f0ae190c1e124c55bd3baafeae2d0b6.png', 1, '2020-10-22 12:58:57', '2020-10-22 03:40:14'),
(4, 'FEA4059235', 'Customized Workflow Management and Reports', '<ul class=\"list-style\">\r\n<li>Inventory management</li>\r\n<li>Reconciliation process ensures you will never miss a payment</li>\r\n<li>Bin management feature facilitates prescription pickup process</li>\r\n<li>Seamless management of incoming prescriptions</li>\r\n<li>Generate electronic orders with wholesalers based on your customized drug ordering parameters</li>\r\n<li>Ability to show cost comparisons between wholesalers</li>\r\n<li>Automatically update true cost from EDI files received from a wholesaler(s)</li>\r\n<li>Ensure compliance with all state and federal regulatory agencies</li>\r\n<li>Automatic backups of all data; Remote server access in case of emergency</li>\r\n</ul>', '94ac640051a3823130630b451e817c3b.png', 1, '2020-10-22 13:01:09', '2020-10-22 06:40:39'),
(5, 'FEA0320767', 'Pharmacy Communication', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Outbound calls, text/SMS messaging</li>\r\n<li xss=removed>Inbound communication allows patients to respond via prompts</li>\r\n<li xss=removed>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</li>\r\n<li xss=removed>Birthday greetings and other personalized messages sent via email or text/SMS message</li>\r\n</ul>', '2a2a80a67ee71c8c417b868eec47d7e3.png', 1, '2020-10-21 19:40:11', '2020-10-21 19:40:11'),
(6, 'FEA2267615', 'Report Generation', '<ul>\r\n<li>Customized reports to support the pharmacy’s unique needs</li>\r\n<li>The daily log provides a comprehensive report of each day’s activities</li>\r\n<li>Customized business analytics</li>\r\n<li>Identify key metrics and receive regular reports relevant to that data</li>\r\n<li>Business intelligence reports built into the system - why pay extra to third-party vendors?</li>\r\n</ul>', '467d1dc879a388c655b72582f7a80059.png', 1, '2020-11-04 12:57:05', '2020-10-26 23:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `forgetpasswordtoken`
--

CREATE TABLE `forgetpasswordtoken` (
  `id` int(5) NOT NULL,
  `UserID` int(25) NOT NULL,
  `TokenNumber` varchar(255) NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `TokenTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `file_source` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `file_source`, `form_id`, `form_name`, `form_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(27, '', 'FRM4742552', 'CMS1500', '', 'af83a03dfd0113ffe8959f04ac3b5613.pdf', 1, '2020-10-31 07:10:44', '2020-10-31 07:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `file` text NOT NULL,
  `file_type` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homeaboutus`
--

CREATE TABLE `homeaboutus` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeaboutus`
--

INSERT INTO `homeaboutus` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(11, 'Streamline Manual Billing', '<p>You might already offer clinical services which allow you to practice at the top of your license and provide convenient, needed services to your patients.</p>\r\n<p>But two problems persist. First, it’s often hard to know which patients are eligible for medical benefits. And second, submitting claims from pharmacy for medical plan reimbursements has been inefficient—until now.</p>\r\n<p>With Medical Billing, medical billing within workflow can be just a few clicks away. You can now check medical eligibility in real time. You can also bill for clinical services using x12 reimbursement transaction technology. This is the same billing pathway that physicians and hospitals use to request medical service reimbursements.</p>\r\n<p><a class=\"button small animate\" href=\"#\" data-animation=\"bounceIn\" data-delay=\"100\">Purchase Now </a></p>', '', 1, '2020-10-13 05:45:49', '2020-06-16 12:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `homeservices`
--

CREATE TABLE `homeservices` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeservices`
--

INSERT INTO `homeservices` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(9, 'Claims Processing Service ', '<p>Billing System is as intricate as any other medical discipline with perpetual reimbursement concerns. Users are required to keep track of their patients, medicines, equipment, and other possible ambiguities. Managing the community pharmacy’s patient claims can be a complex and time-consuming process.</p>\r\n<p>Billing System services aid in implementing all the pharmacy claims and billing challenges by providing essential connectivity between pharmacies and insurance companies. Data can be integrated into the system and transferred to insurance companies making the claims processing tasks more efficient and accurate.</p>', 'c942bdc1a8866d59ef360dee98865c51.jpg', 1, '2021-01-21 05:49:53', '2020-10-22 00:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(5) NOT NULL,
  `client_id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `client_id`, `name`, `status`, `created_date`) VALUES
(1, 1, 'System Users', 1, '2020-09-24 19:27:27'),
(2, 1, 'System Roles', 1, '2020-09-24 19:27:27'),
(3, 1, 'Communications', 1, '2020-09-24 23:16:42'),
(4, 0, 'Services', 1, '2020-09-24 23:16:42'),
(5, 0, 'Insurance Companies', 1, '2020-09-24 23:17:16'),
(6, 0, 'Forms', 1, '2020-09-24 23:17:16'),
(7, 0, 'Insurance Forms', 1, '2020-09-24 23:17:16'),
(8, 0, 'Pharmacies', 1, '2020-09-24 23:17:16'),
(9, 0, 'Pharmacy Services', 1, '2020-09-24 23:17:16'),
(10, 1, 'Claims', 1, '2020-09-24 23:17:16'),
(11, 0, 'Reports', 1, '2020-09-24 23:18:36'),
(12, 0, 'Submissions', 1, '2020-09-24 23:18:36'),
(13, 0, 'Sliders', 1, '2020-09-24 23:18:36'),
(14, 0, 'About Us', 1, '2020-09-24 23:18:36'),
(15, 0, 'Home Services', 1, '2020-09-24 23:19:23'),
(16, 0, 'Features', 1, '2020-09-24 23:19:23'),
(17, 0, 'Testimonials', 1, '2020-09-24 23:19:23'),
(18, 0, 'All Contact Messages', 1, '2020-09-24 23:19:23'),
(19, 0, 'Contact Info', 1, '2020-09-24 23:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `menu_id`, `permission_id`) VALUES
(8, 2, 1),
(9, 2, 2),
(10, 2, 3),
(11, 2, 4),
(12, 8, 1),
(13, 8, 2),
(14, 8, 3),
(15, 8, 4),
(16, 18, 1),
(17, 18, 2),
(18, 18, 3),
(19, 18, 4),
(20, 3, 1),
(21, 3, 2),
(22, 1, 1),
(23, 1, 2),
(24, 1, 3),
(25, 1, 4),
(26, 5, 1),
(27, 5, 2),
(28, 5, 3),
(29, 5, 4),
(30, 10, 1),
(31, 10, 2),
(32, 10, 3),
(33, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `timings` varchar(255) NOT NULL,
  `subject_type` varchar(20) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `rating` int(5) NOT NULL,
  `message` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `reply_status` int(5) NOT NULL DEFAULT '0',
  `review_status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `passwordreset`
--

CREATE TABLE `passwordreset` (
  `resetid` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  `token` varchar(512) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passwordreset`
--

INSERT INTO `passwordreset` (`resetid`, `email`, `ipaddress`, `token`, `status`) VALUES
(1, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', '3gByVlonijZkOWYpr84as5IdevJATDtNq2L6U7xbSzHQRuCGhP', 1),
(2, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', 'dD9I1YSNmokMuiG8H6yw2zsUBWa45hJQOEVegp7F0Pb3tCfLlT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 04:55:00'),
(2, 'Create', 1, '2020-09-25 04:55:00'),
(3, 'Update', 1, '2020-09-25 04:55:11'),
(4, 'Delete', 1, '2020-09-25 04:55:11'),
(5, 'Change Order', 1, '2020-09-25 04:55:46'),
(6, 'Show/Hide', 1, '2020-09-25 04:55:46'),
(7, 'Generate Report', 1, '2020-09-26 04:22:43'),
(8, 'Submit Claim', 1, '2020-09-26 04:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `category_id` int(5) NOT NULL,
  `vendor_id` varchar(50) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `price` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `vendor_id`, `product_id`, `product_name`, `product_description`, `price`, `brand`, `tags`, `status`, `created_date`, `updated_date`) VALUES
(6, 6, 'b3c954af2a4005bf1b6b1de3750dc59e.jpg', 'VND2961891', 'Ordinary Portland Cement (OPC)', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '', '', '', 1, '2021-01-21 06:13:30', '2021-01-21 00:31:30'),
(7, 6, '4469e57484e6c5ee1bb04c9bb236f8cd.png', 'VND6479657', 'Portland Pozzolana Cement', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.99', '', '', 1, '2021-01-19 11:49:52', '2021-01-19 11:49:52'),
(8, 6, '210e0af42dd381962c681bb1006dd330.jpg', 'VND8516935', 'rtf rtytr', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.99', 'fgh fghfgh', 'fdgdf,fhfgh,dhfgh', 1, '2021-01-21 07:45:32', '2021-01-21 01:31:32'),
(9, 7, '3fe19b073ec16e50c4b98ed3e52b35df.jpg', 'VND8873473', 'rtf rtytr', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.99', '', '', 1, '2021-01-19 11:49:52', '2021-01-19 11:49:52'),
(10, 7, '28cf110941e5047da7d4a23bd353dad2.png', 'VND8369612', 'uyuiuy', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.99', '', '', 1, '2021-01-19 11:49:52', '2021-01-19 11:49:52'),
(11, 6, '2ee072b35b0ef7bfb539f78bfeedff44.png', 'VND4953362', 'tyi', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.99', '', '', 1, '2021-01-19 11:49:52', '2021-01-19 11:49:52'),
(12, 6, '70147aad238973a3d94d0408d2632b2d.jpg', 'PRD6053182', 'tyityuytuty', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state.\r\n\r\nThe dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', '1.33', 'Applefgg f', 'tfyrtn yrty,gjghj', 1, '2021-01-21 06:26:41', '2021-01-21 00:31:41'),
(19, 6, 'f8c842e3ac59dee43203ecdc14509c67.jpg', 'PRD5648742', 'Ordinary Portland Cement (OPC)r', 'werwerwer', '1.99', 'Applefgg f', 'tfyrtn yrty,gjghj', 1, '2021-01-21 22:31:19', NULL),
(21, 6, '2ba758ceb7d66eb1fb520b46f62fa1e7.jpg', 'PRD4510303', 'Product-1', 'Product Description', '1.55', 'Brand-1', 'tag-1,tag-2,tag-3', 1, '2021-01-22 18:32:06', '2021-01-22 00:31:06'),
(22, 0, '52', 'PRD6079518', '', '', '', '', '', 1, '2021-01-24 00:31:27', NULL),
(23, 6, '53', 'PRD4388157', 'erg dfhfghfghfgh', '<p>fgh fghf ghf</p>', '22', 'fgh fgh', 'gfh fghfgh', 1, '2021-01-25 08:16:49', '2021-01-25 08:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `image` varchar(200) NOT NULL,
  `is_primary` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `image`, `is_primary`) VALUES
(4787, 22, '600dbb213b2eb.png', 1),
(4788, 22, '600dbb21e5973.png', 0),
(4789, 22, '600dbb220a4db.png', 0),
(4790, 22, '600dbb2222ec3.png', 0),
(4791, 22, '600dbb2241959.png', 0),
(4792, 22, '600dbb2263ae7.png', 0),
(4793, 23, '600e7d408805e.png', 1),
(4794, 23, '600e7d40945f8.png', 0),
(4795, 23, '600e7d409d76b.png', 0),
(4796, 23, '600e7d40c8589.png', 0),
(4797, 23, '600e7d40cf6fc.png', 0),
(4798, 23, '600e7d40dd86d.png', 0),
(4799, 23, '600e7d40f36c8.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `client_id` varchar(50) NOT NULL,
  `project_name` varchar(30) NOT NULL,
  `project_type` varchar(30) NOT NULL,
  `project_start_date` varchar(30) NOT NULL,
  `project_end_date` varchar(30) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `project_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `p_id`, `client_id`, `project_name`, `project_type`, `project_start_date`, `project_end_date`, `address1`, `address2`, `city`, `state`, `zip_code`, `project_description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PRJ3291788', '2183149', 'dhfgfgjh', '2', '2021-01-20', '2021-01-31', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:07', '2021-01-20 : 02:01:38'),
(2, 'PRJ0207753', '2183149', 'dfhdfnh fg', '2', '1969-12-31', '1969-12-31', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:34', NULL),
(3, 'PRJ0901486', '3043818', 'dhfgfgjh', '2', '1969-12-31', '1969-12-31', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:52', NULL),
(4, 'PRJ5306683', '3043818', 'dhfgfgjh', '2', '1969-12-31', '1969-12-31', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:23', NULL),
(5, 'PRJ7837194', '3043818', 'dhfgfgjh', '2', '1969-12-31', '1969-12-31', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:14', NULL),
(6, 'PRJ8434156', '3043818', 'dhfgfgjh', '2', '2021-01-25', '2021-01-29', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 12:01:27', NULL),
(7, 'PRJ0365580', '3043818', 'stggdfg', '1', '2021-01-29', '2021-01-31', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74707', 'The ingredients are mixed in the proportion of about two parts of calcareous materials to one part of argillaceous materials and then crushed and ground in ball mills in a dry state or mixed in wet state. The dry powder or the wet slurry is then burnt in a rotary kiln at a temperature between 1400 degree C to 1500 degree C. the clinker obtained from the kiln is first cooled and then passed on to ball mills where gypsum is added and it is ground to the requisite fineness according to the class of product.', 1, '2021-01-19 01:01:40', '2021-01-20 : 11:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation`
--

CREATE TABLE `project_conversation` (
  `id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_conversation`
--

INSERT INTO `project_conversation` (`id`, `project_id`, `user_id`, `description`, `create_date`) VALUES
(1, 10, 1, '<p>Please send a scanned copy of the insurance card for more information</p>\n<p>&nbsp;</p>\n<p>Thanks,</p>\n<p>Super Admin</p>', '2020-11-24 07:42:31'),
(2, 73, 47, '<p>fghfgh fghfghfg</p>', '2021-01-18 08:27:35'),
(3, 73, 47, '<p>erter retgsdgdfgdfgdfgdfgdfgdfgdfgdfg</p>', '2021-01-18 08:29:28'),
(4, 73, 47, '<p>&nbsp;bcb dfgfbgfb</p>', '2021-01-18 09:05:26'),
(5, 62, 47, '<p>drtgd fgdfgdfg</p>', '2021-01-18 11:43:51'),
(6, 6, 1, '<p>tr yrtyrty</p>', '2021-01-21 00:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation_files`
--

CREATE TABLE `project_conversation_files` (
  `id` int(10) NOT NULL,
  `project_conversation_id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_conversation_files`
--

INSERT INTO `project_conversation_files` (`id`, `project_conversation_id`, `file_name`, `create_date`) VALUES
(1, 2, 'cropped.jpg', '2021-01-18 08:27:35'),
(2, 3, 'backblue.gif', '2021-01-18 08:29:28'),
(3, 4, 'ekait_billing_(28).sql', '2021-01-18 09:05:26'),
(4, 5, 'e5eff7162c40c83a39df15fc0c661560.sql', '2021-01-18 11:43:51'),
(5, 6, 'SADevelopers_Features.png', '2021-01-21 00:31:17');

-- --------------------------------------------------------

--
-- Table structure for table `project_documents`
--

CREATE TABLE `project_documents` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `file` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_documents`
--

INSERT INTO `project_documents` (`id`, `project_id`, `file`, `created_date`) VALUES
(6, 1, 'fdc430226e296247669142198e882729.sql', '2021-01-07 01:10:12'),
(7, 0, 'e5eff7162c40c83a39df15fc0c661560.sql', '2021-01-07 01:14:26'),
(8, 1, '12f161a1aa779b7b7ddcdeb08a5f3b43.sql', '2021-01-07 01:17:49'),
(9, 0, '9e2c5b72b10b12f71b962d8bef08df09.sql', '2021-01-07 01:18:38'),
(10, 1, 'efcc16b8f7afb72f5b8f5a334076e345.pptx', '2021-01-07 01:19:29'),
(14, 0, 'c56b484b69f095bca40ebfd73a851505.xlsx', '2021-01-07 14:38:09'),
(15, 0, 'a79ade25e7e44bee0eae9e0a334f9914.exe', '2021-01-08 06:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `project_products`
--

CREATE TABLE `project_products` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `is_choice` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_products`
--

INSERT INTO `project_products` (`id`, `project_id`, `category_id`, `product_id`, `is_choice`, `status`) VALUES
(14, 7, 7, 10, 0, 1),
(15, 7, 6, 6, 1, 0),
(16, 7, 6, 12, 1, 0),
(17, 7, 6, 8, 1, 0),
(18, 7, 6, 11, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_status`
--

CREATE TABLE `project_status` (
  `project_status_id` int(11) NOT NULL,
  `project_status_name` varchar(255) NOT NULL,
  `status_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_status`
--

INSERT INTO `project_status` (`project_status_id`, `project_status_name`, `status_type`) VALUES
(1, 'New', 'bg-dark'),
(2, 'Inprogress', 'bg-primary'),
(3, 'Pending', 'bg-success'),
(4, 'Hold', 'bg-warning'),
(5, 'Rejected', 'bg-info'),
(6, 'Completed', 'bg-dark');

-- --------------------------------------------------------

--
-- Table structure for table `project_types`
--

CREATE TABLE `project_types` (
  `id` int(5) NOT NULL,
  `project_type_name` varchar(50) NOT NULL,
  `project_type_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_types`
--

INSERT INTO `project_types` (`id`, `project_type_name`, `project_type_description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Commercial', 'rtyrty rrtyr', 1, '2021-01-25 05:05:13', '2021-01-25 : 05:01:13'),
(2, 'Residential', '', 1, '2021-01-05 18:10:15', ''),
(3, 'SA Developers Properties', '', 1, '2021-01-05 18:10:40', '');

-- --------------------------------------------------------

--
-- Table structure for table `project_updates`
--

CREATE TABLE `project_updates` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `create_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_updates`
--

INSERT INTO `project_updates` (`id`, `project_id`, `user_id`, `title`, `description`, `create_date`) VALUES
(1, 6, 1, 'Praoject Analysis Done', 'Project will be started by 1st of the feb', '2021-01-20 02:29:20'),
(2, 6, 1, 'rt rt ', 'rt yrtyrtyrty', '2021-01-20 23:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_comments`
--

CREATE TABLE `project_update_comments` (
  `id` int(5) NOT NULL,
  `project_update_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `comment` text NOT NULL,
  `created_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_comments`
--

INSERT INTO `project_update_comments` (`id`, `project_update_id`, `user_id`, `comment`, `created_date`) VALUES
(1, 1, 1, 'fgjhv fghgjhghj', '2021-01-20 : 02:01:43'),
(2, 1, 1, 'tfhfghf fghfghfg', '2021-01-20 : 02:01:07'),
(3, 1, 1, 'fg hfghfghfghfgh', '2021-01-20 : 02:01:11'),
(4, 1, 1, 'ghj hghjghj fgjghjghjghj', '2021-01-20 : 02:01:16');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_files`
--

CREATE TABLE `project_update_files` (
  `id` int(5) NOT NULL,
  `project_update_id` int(10) NOT NULL,
  `file` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_files`
--

INSERT INTO `project_update_files` (`id`, `project_update_id`, `file`, `create_date`) VALUES
(1, 1, '1d7480fd74175ca2f773d8716081f0d5.png', '2021-01-20 02:29:23'),
(2, 1, 'b8b27b92297c83548611b21e85f8a8c0.jpg', '2021-01-20 02:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `client_id` int(20) NOT NULL DEFAULT '0',
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `client_id`, `rolename`, `description`, `status`, `last_ip`, `created_at`, `updated_at`) VALUES
(0, 0, 'sadmin', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '2020-12-10 00:00:00'),
(1, 0, 'Marketing Manager', 'srt brdtdrt', 1, '', '2020-10-24 03:10:21', '0000-00-00 00:00:00'),
(2, 0, 'Admin', 'srt brdtdrt', 1, '', '2020-10-24 03:10:30', '0000-00-00 00:00:00'),
(3, 5, 'Manager', 'srt brdtdrt', 1, '', '2020-10-24 06:10:24', '0000-00-00 00:00:00'),
(4, 4, 'PAdmin', 'PAdmin', 1, '', '2020-10-31 07:10:58', '0000-00-00 00:00:00'),
(5, 14, 'Manager', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '0000-00-00 00:00:00'),
(6, 4540300, 'Manager', 'ged drfgdg', 1, '', '2021-01-05 01:01:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(1, 2, 18, 0),
(2, 2, 1, 0),
(3, 1, 1, 0),
(4, 1, 10, 0),
(5, 1, 5, 0),
(6, 1, 8, 0),
(7, 1, 5, 1),
(9, 1, 10, 1),
(13, 1, 8, 1),
(14, 1, 3, 0),
(15, 1, 3, 1),
(16, 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'SER5050780', 'Claims Processing', 'Processing medical claims from the stage of receiving the prescription to the stage of payment processing', 1, '2020-10-31 09:28:54', '2020-10-31 09:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `navigation_header` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `sidebar` varchar(255) NOT NULL,
  `font_family` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `navigation_header`, `header`, `sidebar`, `font_family`) VALUES
(1, 'color_1', 'color_2', 'color_13', 'roboto');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slides_id` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` text NOT NULL,
  `status` int(5) NOT NULL,
  `image` text NOT NULL,
  `sorting` varchar(50) NOT NULL,
  `type` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slides_id`, `title`, `subtitle`, `status`, `image`, `sorting`, `type`, `home_status`, `created_date`, `updated_date`) VALUES
(7, 'SLI2521389', 'WELCOME TO S&A DEVELOPERS', 'S & A General Construction & Development, Inc', 1, 'a82c1f5ed6fe767d8da79cceac6578b9.jpg', '', 0, 0, '2021-01-08', '2021-01-08'),
(6, 'SLI6483647', 'Welcome to S&A Developers', 'S & A General Construction & Development, Inc', 1, '982e314cd504f8952e0e3e7a5273cfeb.jpg', '', 0, 0, '2020-11-02', '2020-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(5) NOT NULL,
  `testimonials_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `update_status` int(5) NOT NULL,
  `version` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials_id`, `name`, `designation`, `description`, `image`, `status`, `home_status`, `update_status`, `version`, `created_date`, `updated_date`) VALUES
(8, 'TES0810481', 'ennoble', 'Developer', 'Billing System', '', 1, 0, 0, 0, '2020-09-22 12:48:08', '2020-09-22 12:48:08'),
(9, 'TES0147327', 'kumar praveen', 'Developer', 'Billing Admin', 'profile.jpg', 1, 0, 0, 0, '2020-09-26 06:45:24', '2020-09-26 06:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `client_id` int(10) NOT NULL,
  `user_type` int(5) NOT NULL COMMENT '0-admin, 1-client',
  `source` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` int(11) NOT NULL DEFAULT '1',
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `client_id`, `user_type`, `source`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `image`, `password`, `ein_number`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `notes`, `is_admin`, `is_new`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 0, 0, 'Superadmin', 'Developers', 'S&A', '', 'admin@sadevelopers.com', '+1 (987) 654 3210', 'profile.png', '$2y$10$rlZMfQerPQWcQLxj6RVuZ.4oMLBwUmYP2Hy3m7.j2q3Nf15R3gWPG', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 1, 0, '', 1, '2017-09-29 10:09:44', '2020-10-12 03:10:07'),
(51, 'USR2183149', 2183149, 1, 'rudroju raghuvarmarudroju', 'Rudroju', 'Rudroju Raghuvarma', 'Raghuvarma', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', 'cf81f45cbf90a5a14d36aeb7a031fc6f.jpg', '$2y$10$18tPeRIge2peEJNjlbPGyOFd9Md3g9GkHwNepuH5Ozazp6zcauRpu', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', '', 1, 1, '', 1, '2021-01-19 03:01:35', '0000-00-00 00:00:00'),
(52, 'USR3043818', 3043818, 1, 'ertretret rawetertertretret', 'ertretret', 'ertretret rawetert', '', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '1de7da803d677c379c95bb7a582c0c96.jpg', '$2y$10$MGlJlK1GcX0EP.Lb3HGaKOoBo5/bo4PtqIjFzCYX9ciaH9puZ53ki', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74707', '', 1, 0, '', 1, '2021-01-19 03:01:55', '0000-00-00 00:00:00'),
(53, 'USR3095294', 3095294, 1, 'jhjhbjhbjnhb', 'Rudroju', 'Raghuvarma', 'Raghuvarma', 'raghuvarma.testmail@gmail.com', '+1 (898) 987-9879', 'IMG_20200831_115805_8857633499747046360.jpg', '$2y$10$ZyJMI9xe5pT2RfP2JYo2AeLUieO/bOyQS.J94OYPSK6Y4f4IRENNC', '', 0, '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', ' fsefsdfd', 1, 1, '', 0, '2021-01-23 05:01:31', '0000-00-00 00:00:00'),
(54, 'USR2155901', 2155901, 1, 'eeeeeerrrr', 'ertretret', 'rawetert', 'Test2', 'dsfgd@gmail.com', '+1 (987) 654-3210', '9cc21ffc37e3b57c9153fa915fa689c8.png', '$2y$10$H1JqUmadyInqxA1zWqU7BuypKqkhGN042HpknaC2NMZMHCtlEqHdG', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74707', 'weqwrewrwerwe werwerwe', 1, 1, '', 1, '2021-01-23 06:01:35', '2021-01-23 06:01:44');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(10) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_email` varchar(2552) NOT NULL,
  `vendor_mobile_no` varchar(50) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_id`, `vendor_name`, `vendor_email`, `vendor_mobile_no`, `address1`, `address2`, `city`, `state`, `zip_code`, `website_url`, `image`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `status`, `created_at`, `updated_at`) VALUES
(52, 'VND2610573', '', '', '', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', 'dsvdvv', 'df8f28eb4b1321bb64f71e3450c961b5.jpg', 'smith', 'Mary', '', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', '$2y$10$QgU3eIF7PA8d2c.0gy7tcubxN17feVo.zdzccZpwdraVkkWdgnVS6', 1, '2021-01-11 05:01:16', '0000-00-00 00:00:00'),
(53, 'VND6065551', '', '', '', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', 'dsvdvv', '9e7e4fde50ee229153e09370516d309e.jpg', '2', 'vendor', '', 'raghguv@ennobletechnologies.com', '+1 (987) 654-3210', '$2y$10$UTCer6QAkYeX9sm66JsN2uPv0qPpbAQhAsQaX04IlI4VGN4i3.UWe', 1, '2021-01-22 05:01:11', '0000-00-00 00:00:00'),
(54, 'VND7662205', 'drtgdrtg', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'dsvdvv', '7ae7183abbb4482819007fc7b6e85d69.jpg', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', '$2y$10$HmwWfmOIhoF866Zxtcvr5.EFX2WxukaTkythHaFndSKtRYc1hrILe', 1, '2021-01-25 07:01:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_products`
--

CREATE TABLE `vendor_products` (
  `id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `vendor_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_products`
--

INSERT INTO `vendor_products` (`id`, `product_id`, `vendor_id`, `status`, `created_date`) VALUES
(1, 19, 5, 1, '2021-01-22 16:17:19'),
(2, 20, 52, 1, '2021-01-22 16:39:47'),
(8, 21, 52, 1, '2021-01-22 18:32:06'),
(9, 21, 53, 1, '2021-01-22 18:32:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_files`
--
ALTER TABLE `communications_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgetpasswordtoken`
--
ALTER TABLE `forgetpasswordtoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeservices`
--
ALTER TABLE `homeservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `passwordreset`
--
ALTER TABLE `passwordreset`
  ADD PRIMARY KEY (`resetid`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation`
--
ALTER TABLE `project_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_documents`
--
ALTER TABLE `project_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_products`
--
ALTER TABLE `project_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`project_status_id`);

--
-- Indexes for table `project_types`
--
ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_updates`
--
ALTER TABLE `project_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_files`
--
ALTER TABLE `project_update_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_products`
--
ALTER TABLE `vendor_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_files`
--
ALTER TABLE `communications_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `homeservices`
--
ALTER TABLE `homeservices`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `passwordreset`
--
ALTER TABLE `passwordreset`
  MODIFY `resetid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4800;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_conversation`
--
ALTER TABLE `project_conversation`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `project_documents`
--
ALTER TABLE `project_documents`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `project_products`
--
ALTER TABLE `project_products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `project_status`
--
ALTER TABLE `project_status`
  MODIFY `project_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `project_types`
--
ALTER TABLE `project_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `project_updates`
--
ALTER TABLE `project_updates`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `project_update_files`
--
ALTER TABLE `project_update_files`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `vendor_products`
--
ALTER TABLE `vendor_products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
