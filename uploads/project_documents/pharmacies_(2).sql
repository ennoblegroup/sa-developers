-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2021 at 09:04 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekait_billing`
--

-- --------------------------------------------------------

--
-- Table structure for table `pharmacies`
--

CREATE TABLE `pharmacies` (
  `id` int(10) NOT NULL,
  `pharmacy_id` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `pharmacy_lbn_name` varchar(25) NOT NULL,
  `pharmacy_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pharmacies`
--

INSERT INTO `pharmacies` (`id`, `pharmacy_id`, `image`, `pharmacy_lbn_name`, `pharmacy_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR1302296', 'd28027c099f1a4c38008b6c2f6a3aba9.jpg', 'Batish Pharmacy', 'BTPPD', 'Batish Pharmacy', 'raghuvarma.testmail@gmail.com', '4558568222', '5685568', '', '', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', 1, '2020-11-20 13:11:00', NULL),
(2, 'PHR0043065', '852bbb1fbadca0f06381150c799d6ebe.jpg', 'Branch Brook Pharmacy', 'BBPBT', 'Branch Brook Pharmacy', 'branchbrooktest@gmail.com', '3434343434', '3434343', 'DEA343434', 'LN1212121', '917 Franklin Avenue', '', 'Newark', 'NJ', '07107-2809', '', 1, '2020-11-21 07:11:29', NULL),
(3, 'PHR8794968', 'a164dae69e3876096d2f0a47d3706f46.png', 'Mason Pharmacy', 'MPPPD', 'Mason Pharmacy', 'masontest@gmail.com', '5656564343', '4657678', 'DEA343434', 'LN3434343', '1255 Castle Hill Avenue', '', 'Bronx', 'NY', '10462-4813', '', 1, '2020-11-21 07:11:41', NULL),
(4, 'PHR4324832', '9da8f1ed15d32e894288f1d76d1e4ca0.jpg', 'Heidi Pharmacy', 'HPPPD', 'Heidi Pharmacy', 'heiditest@gmail.com', '5545656345', '1377765', 'DEA343434', 'LNMP43434', '522 West 181st Street', '', 'New York', 'NY', '10033-5101', '', 1, '2020-11-21 07:11:08', NULL),
(5, 'PHR0657257', '138d9b7d164abe37e6b20d0dd3fe50e6.png', 'Vim Drugs', 'VDPPD', 'Vim Drugs', 'vimtest@gmail.com', '4545454545', '4656565', 'DEA344454', 'LNVD34343', '3835 Broadway', '', 'New York', 'NY', '10032-1510', '', 1, '2020-11-21 07:11:52', NULL),
(6, 'PHR6113443', '4e9a38a1b3392a27a6de18bec0a81ca6.png', 'Mergewell Pharmacy', 'MPTCP', 'Mergewell Pharmacy', 'mergewelltest@gmail.com', '4334343434', '3434343', 'DEA343434', 'LNMP43434', '759 Washington Avenue', '', 'Brooklyn', 'NY', '11238-4504', '', 1, '2020-11-21 07:11:27', NULL),
(7, 'PHR8320501', 'ad3b69f6bcf7960307ebed528a76ecaa.png', 'Nostrand Pharmacy', 'NPPPD', 'Nostrand Pharmacy', 'nostrandtest@gmail.com', '5443343434', '8767656', 'DEA454545', 'LNNP34343', '524 Nostrand Avenue', '', 'Brooklyn', 'NY', '11216-2012', '', 1, '2020-11-21 07:11:40', NULL),
(8, 'PHR5589027', '5ec7e7278ee7b630e941f18566e2e3b0.png', 'Elmhurst Pharmacy', 'EPPPD', 'Elmhurst Pharmacy', 'elmhurstest@gmail.com', '3433434434', '3434534', 'DEA334343', 'LNEP34343', '75-23 Broadway', '', 'Elmhurst', 'NY', '11373-5612', '', 1, '2020-11-21 07:11:53', NULL),
(9, 'PHR1582089', '481327c096d195aaea60794417ed184c.png', 'Freedom Pharmacy', 'FPPPD', 'Freedom Pharmacy', 'freedomtest@gmail.com', '4545454544', '6565656', 'DEA343434', 'LNFP34343', '953 Frelinghuysen Avenue', '', 'Newark', 'NJ', '07114-2103', '', 1, '2020-11-21 07:11:57', NULL),
(10, 'PHR5966444', '67ac2d65c7660c230a85a8acc51cfba9.jpg', 'Lenda Pharmacy', 'LPPPD', 'Lenda Pharmacy', 'lendatest@gmail.com', '3434378784', '4545454', 'DEA454545', 'LNLP34343', '3397 Broadway', '', 'New York', 'NY', '10031-7416', '', 1, '2020-11-21 07:11:06', NULL),
(11, 'PHR8023521', 'erins_logo.jpg', 'Erins Pharmacy', 'EPPPD', 'Erins Pharmacy', 'erinstest@gmail.com', '3437698989', '3434343', 'DEA454545', 'LNEP34343', '360 EAST 4TH STREET', '', 'New York', 'NY', '10009-8501', '', 1, '2020-11-21 07:11:17', NULL),
(12, 'PHR0091840', 'ef6b7b3c025965839c86eff2a773a771.jpg', 'Hill Pharmacy', 'HPPPD', 'Hill Pharmacy', 'hilltest@gmail.com', '3434343443', '4454545', 'DEA454545', 'LNHP45454', '522 West 181ST Street', '', 'New York', 'NY', '10033', '', 1, '2020-11-21 07:11:12', NULL),
(13, 'PHR1362611', '7a349efa22e95b0da7826f7702d73bcd.jpg', 'Express Care Pharmacy', 'ECPPD', 'Express Care Pharmacy', 'expresscaretest@gmail.com', '4346909445', '4545454', 'DEA454545', 'LNECP3434', '15 East Tremont Avenue', '', 'Bronx', 'NY', '10453-5801', '', 1, '2020-11-21 07:11:14', NULL),
(14, 'PHR5557889', '033ecfc7b874daa7efda42905d2d185b.png', 'Carewell Pharmacy', 'CPPPD', 'Carewell Pharmacy', 'carewelltest@gmail.com', '4547454545', '5454343', 'DEA343434', 'LNCP93434', '826 East Tremont Avenue', '', 'Bronx', 'NY', '10460-4146', '', 1, '2020-11-21 08:11:36', NULL),
(15, 'PHR0378690', '', 'Care4u Pharmacy', 'C4PHCY', 'Care Pharmacy', 'info@care4upharmacy.com', '3224323455', '0000009', 'YU769JPO8', 'E4RE76JKL', 'Unit No: 78, Sunset Mathy, Berger Road,', 'Unit No: 78, Sunset Mathy, Berger Road,', 'Midrand', 'CA', '01234-0403', '', 1, '2020-11-26 14:11:20', NULL),
(16, 'PHR9661303', '', 'ICare', 'ICARE', 'www.icare.com', 'icare@testmail.com', '4948918922', '5895852', '', '', '73 webster road', '', 'Cary', 'IL', '58852', '', 1, '2020-12-23 13:12:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pharmacies`
--
ALTER TABLE `pharmacies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pharmacies`
--
ALTER TABLE `pharmacies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
