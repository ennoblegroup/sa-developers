-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 16, 2021 at 06:42 PM
-- Server version: 5.7.33-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sadev_21.1.0.2`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(27, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers.</p>', '569a4be34c567b10dda004a9f3be2bc7.jpg', 0, '2021-01-08 20:36:08', '2021-01-08 15:00:20'),
(28, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers. </p>', 'f7ca628ee1604dfc8098a9961a1207fb.png', 0, '2021-02-09 14:43:50', '2021-01-08 15:06:08'),
(29, 'S & A – A Tradition Of Excellencee', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers. </p>', 'f7ca628ee1604dfc8098a9961a1207fb.png', 0, '2021-02-09 14:45:18', '2021-02-09 09:13:50'),
(30, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers. </p>', '605e86d940aa06877f18e1e42455af29.jpg', 0, '2021-02-15 21:11:38', '2021-02-09 09:22:01'),
(31, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, is currently located at 37 Meridian Road, Edison, NJ 08820.  Started on March 25, 1994, as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion for residential-construction since 2003, we have worked on countless Residential, Single Family Dwellings, and custom-built Luxury Homes and enjoy working with in-house architects and interior designers. </p>', '605e86d940aa06877f18e1e42455af29.jpg', 1, '2021-02-15 21:11:38', '2021-02-16 05:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`id`, `last_name`, `first_name`, `email`, `phone`, `date`, `time`, `subject`, `message`, `status`, `create_date`, `update_date`) VALUES
(1, 'Raghuvarma', 'Rudroju', 'raghuvarma.chintu@gmail.com', '9876543261', '2021-02-11', '19:23', 'Test', 'trh fthrtdsdghsdrthydrtfftghfghfghfghfg', '', '2021-02-09 12:55:52', NULL),
(2, 'Raghuvarma', 'Rudroju', 'raghuv@ennobletechnologies.com', '+19876543210', '2021-02-09', '20:57', 'sdfsdf', 'dsgv dsgxfgbfc', '', '2021-02-09 15:26:23', NULL),
(3, 'Rudroju Test2 Raghuvarma', 'Rudroju', 'raghuvarma.chintu@gmail.com', '98765432105488485', '2021-02-14', '20:45', 'Test', 'utfg uygu yguyguy', '', '2021-02-14 15:15:29', NULL),
(4, 'Han', 'Sushma', 'sushmah@ennoblegrp.com', '+1 45672282828282', '2021-02-18', '18:23', 'Discuss Plans', 'Appointment to discuss the planning', '', '2021-02-15 21:22:48', NULL),
(5, 'Hanumansetty', 'Sushma', 'sushmah@ennoblegrp.com', '+1 48181820208298', '2021-02-26', '17:24', 'Contract Signatures', 'Appointment', '', '2021-02-15 21:25:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` text,
  `file` text,
  `status` int(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Outdoor Decks', 'Patio, decks ', '449e110b5c5be0831fdd401b5d0eafd4.jpg', 1, '2021-02-15 17:47:21', '2021-02-16 01:02:21'),
(2, 'Bath Vanities', 'Bathroom Vanities', '5698c40f6815a3c5f9dc25db6dc2bbf1.jpg', 1, '2021-02-16 01:02:46', '2021-02-16 01:02:46'),
(3, 'Windows', 'Windows', '14845a50574fc09411ac47a669e56e33.jpg', 1, '2021-02-16 01:02:45', '2021-02-16 01:02:45'),
(4, 'Tiles', 'Tiles for flooring, walls etc', 'aa50e48771be944d5874e76c2e56fbfe.jpg', 1, '2021-02-16 01:02:01', '2021-02-16 01:02:01'),
(5, 'Wooden Flooring', 'Wood for Flooring', '860d0b8dc6bc00715d847a57efa6d088.jpg', 1, '2021-02-16 01:02:40', '2021-02-16 01:02:40'),
(6, 'Appliances', 'Kitchen Appliances', 'a82cc224f141c48905a55b451572479f.jpg', 1, '2021-02-16 01:02:08', '2021-02-16 01:02:08'),
(7, 'Doors Hardware', 'Doors Hardware', '6d8ec6ae7270b4619135b57cb2c3e5b0.jpg', 1, '2021-02-16 01:02:45', '2021-02-16 01:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `pharmacy_id` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `pharmacy_lbn_name` varchar(25) NOT NULL,
  `pharmacy_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_files`
--

CREATE TABLE `communications_files` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `reply_id` varchar(25) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `contact` varchar(18) NOT NULL,
  `map` text NOT NULL,
  `fax_number` varchar(200) NOT NULL,
  `day_one` varchar(50) NOT NULL,
  `day_two` varchar(50) NOT NULL,
  `day_three` varchar(50) NOT NULL,
  `start_time` varchar(250) NOT NULL DEFAULT 'AM',
  `end_time` varchar(250) NOT NULL DEFAULT 'PM',
  `start_hour` varchar(250) NOT NULL DEFAULT 'AM',
  `end_hour` varchar(250) NOT NULL DEFAULT 'PM',
  `hours` varchar(250) NOT NULL DEFAULT '00:00:00',
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address1`, `title`, `description`, `address2`, `city`, `country`, `state`, `zip`, `status`, `email`, `phone`, `contact`, `map`, `fax_number`, `day_one`, `day_two`, `day_three`, `start_time`, `end_time`, `start_hour`, `end_hour`, `hours`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `create_date`, `update_date`) VALUES
(1, '37 Meridian Rd', 'Contact Us', 'test', '', 'Edison', 'US', 'NJ', '08820', 1, 'info@sadevelopers.com', '+1 (908) 757-4998', ' ', 'https://goo.gl/maps/q7weS', '+1 (973) 412 7303', '', '', '', '', '', '', '', '00:00:00', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.linkedin.com/                ', 'https://www.pinterest.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', '2019-03-21', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_replies`
--

CREATE TABLE `contactus_replies` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `reply_message` text NOT NULL,
  `replier_id` varchar(20) NOT NULL,
  `replier_name` varchar(255) NOT NULL,
  `read_status` int(20) NOT NULL DEFAULT '0',
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) NOT NULL,
  `document_type_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `document_type_id`, `file_name`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 4, 'sadevelopers (3).sql', '1543e68460db85722606f1d813911c63.sql', 1, '2021-01-29 02:21:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(5) NOT NULL,
  `document_type_name` varchar(50) NOT NULL,
  `document_type_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `document_type_name`, `document_type_description`, `status`, `created_date`, `updated_date`) VALUES
(4, 'Document Type-1', 'Document Type-1', 1, '2021-01-28 05:31:43', ''),
(5, 'Document Type-2', 'Document Type-2', 1, '2021-01-28 05:31:54', ''),
(6, 'Document Type-3', 'Document Type-3', 1, '2021-01-28 05:31:09', ''),
(7, 'Contracts', 'Contract documents', 1, '2021-02-16 05:02:54', '');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `pharmacy_name` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `pharmacy_name`, `contact_name`, `email_address`, `phone_number`, `notes`, `create_date`) VALUES
(1, 'dfhdfh', 'fhg dfhdfgh', 'raghuvarma.chintu@gmail.com', '', 'gdf dfgfdgdfgfd', '2020-11-23 06:43:12'),
(2, 'er tvreterter', 're ter tert', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'er treter ter tretre', '2020-11-23 06:46:07'),
(3, 'drghdfh', 'gfhfghfgh', 'raghuvarma.chintu@gmail.com', '', 'gyj fthfthfghfgh', '2020-11-23 06:54:30'),
(4, 'tyujygjgyj', 'gyuityuityi', 'raghuvarma.chintu@gmail.com', '+1 (445) 354-3413', 'tyity nrtfhftghnfgh', '2020-11-23 06:58:50'),
(5, 'fgh fghfgh', 'fgh fgh fghf', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'fgh fgh fgh fgh', '2020-11-23 06:59:24'),
(6, 'kjbkbkjb', 'hgjhvjhy', 'raghuvarma.testmail@gmail.com', '+1 (987) 456-3210', 'ygfuyuy', '2020-11-23 07:00:53'),
(7, 'dghdfghdf', 'fghgh', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', 'fghfgh fghgf', '2020-11-23 07:06:28'),
(8, 'sdfdf sd', ' sdfsdf', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', 'sdf sdfdsfsdf', '2020-11-23 07:07:20'),
(9, 'dsgsg', 'dfgfdgdfg', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'dfg dfgdfgfd', '2020-11-23 07:08:11'),
(10, 'ertyret', 'ertertert', 'raghuvarma.chintu@gmail.com', '+1 (678) 867-4853', 'ret ertretert', '2020-11-23 07:09:42'),
(11, 'BestHealth', 'Sushma', 'sushmah@ennoblegrp.com', '+1 (481) 818-2020', 'I would like to discuss and register ', '2020-11-26 13:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(5) NOT NULL,
  `feature_id` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `feature_id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'FEA7082958', 'Automated Refill Management System', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data</li>\r\n<li xss=removed>“At a glance,” visibility of all refill management processes through a Refill Compliance Dashboard. The dashboard can be customized to include preferred metrics, including scheduled refills, missed refills, expired refills, and Med Sync Rx.</li>\r\n<li xss=removed>Automatically queues each day’s scheduled refills</li>\r\n<li xss=removed>RxSync - Sync all refills to a particular date</li>\r\n<li xss=removed>Return to Stock Queue - Prescriptions not picked up by patients will be flagged, with the system easily updated to reflect missed pickups, and drugs added back</li>\r\n</ul>', '8f0ae190c1e124c55bd3baafeae2d0b6.png', 1, '2020-10-22 12:58:57', '2020-10-22 03:40:14'),
(4, 'FEA4059235', 'Customized Workflow Management and Reports', '<ul class=\"list-style\">\r\n<li>Inventory management</li>\r\n<li>Reconciliation process ensures you will never miss a payment</li>\r\n<li>Bin management feature facilitates prescription pickup process</li>\r\n<li>Seamless management of incoming prescriptions</li>\r\n<li>Generate electronic orders with wholesalers based on your customized drug ordering parameters</li>\r\n<li>Ability to show cost comparisons between wholesalers</li>\r\n<li>Automatically update true cost from EDI files received from a wholesaler(s)</li>\r\n<li>Ensure compliance with all state and federal regulatory agencies</li>\r\n<li>Automatic backups of all data; Remote server access in case of emergency</li>\r\n</ul>', '94ac640051a3823130630b451e817c3b.png', 1, '2020-10-22 13:01:09', '2020-10-22 06:40:39'),
(5, 'FEA0320767', 'Pharmacy Communication', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Outbound calls, text/SMS messaging</li>\r\n<li xss=removed>Inbound communication allows patients to respond via prompts</li>\r\n<li xss=removed>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</li>\r\n<li xss=removed>Birthday greetings and other personalized messages sent via email or text/SMS message</li>\r\n</ul>', '2a2a80a67ee71c8c417b868eec47d7e3.png', 1, '2020-10-21 19:40:11', '2020-10-21 19:40:11'),
(6, 'FEA2267615', 'Report Generation', '<ul>\r\n<li>Customized reports to support the pharmacy’s unique needs</li>\r\n<li>The daily log provides a comprehensive report of each day’s activities</li>\r\n<li>Customized business analytics</li>\r\n<li>Identify key metrics and receive regular reports relevant to that data</li>\r\n<li>Business intelligence reports built into the system - why pay extra to third-party vendors?</li>\r\n</ul>', '467d1dc879a388c655b72582f7a80059.png', 1, '2020-11-04 12:57:05', '2020-10-26 23:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `forgetpasswordtoken`
--

CREATE TABLE `forgetpasswordtoken` (
  `id` int(5) NOT NULL,
  `UserID` int(25) NOT NULL,
  `TokenNumber` varchar(255) NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `TokenTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `gallery_type` int(5) NOT NULL,
  `visibility` int(5) NOT NULL,
  `file` text NOT NULL,
  `file_type` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `project_id`, `gallery_type`, `visibility`, `file`, `file_type`, `status`, `created_at`) VALUES
(1, 23, 1, 1, '2998af858f931150980140bac43fd104.jpg', 0, 0, '2021-02-01 17:29:20'),
(2, 23, 1, 1, 'e2d12aa0d9362decc534b678dc571a40.mp4', 1, 0, '2021-02-01 17:31:17'),
(3, 23, 0, 1, 'd4d5f209bd8d5d39ffe2ec457199668f.jpg', 0, 0, '2021-02-01 17:32:47'),
(4, 23, 1, 1, 'd568f4d508c0466e71a90751d225cbcf.png', 0, 0, '2021-02-01 17:40:23'),
(45, 23, 2, 1, '7e7688a788758c009ad5adef8762a90f.jpg', 0, 0, '2021-02-02 04:03:45'),
(46, 23, 2, 1, 'aa6a9bd1565280aaf34eb12ea69d6b19.jpg', 0, 0, '2021-02-02 04:03:46'),
(47, 23, 2, 1, '14eeac151d3a40abcb1cba954fe4a12c.jpg', 0, 0, '2021-02-02 04:03:46'),
(48, 23, 2, 1, '2721cef978a9099648811105f7b0b3cb.jpg', 0, 0, '2021-02-02 04:03:46'),
(49, 23, 1, 1, 'eb09697dcd2dc94f18b0a74266a1f839.jpg', 0, 0, '2021-02-02 05:10:45'),
(50, 23, 1, 1, '3bf313dc0aa4e454688303d9f8e4deb3.png', 0, 0, '2021-02-07 19:49:17'),
(51, 23, 1, 1, '3df182524b05ab2fbd0a2935588cfe0c.png', 0, 0, '2021-02-07 19:49:17'),
(52, 23, 1, 1, '92a1d85f4958f2509801afdf2dbf61cb.png', 0, 0, '2021-02-07 19:49:17'),
(53, 23, 1, 1, '57b7439c79945adcc51df90c7548686b.png', 0, 0, '2021-02-07 19:49:17'),
(54, 23, 1, 1, 'e541fbbe420b62ec3d29b167b6768665.png', 0, 0, '2021-02-07 19:49:17'),
(55, 24, 0, 1, 'ea321f8e9ce6c7fc4caf0efa1c94876c.jpg', 0, 0, '2021-02-15 21:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `homeaboutus`
--

CREATE TABLE `homeaboutus` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeaboutus`
--

INSERT INTO `homeaboutus` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(11, 'Streamline Manual Billing', '<p>You might already offer clinical services which allow you to practice at the top of your license and provide convenient, needed services to your patients.</p>\r\n<p>But two problems persist. First, it’s often hard to know which patients are eligible for medical benefits. And second, submitting claims from pharmacy for medical plan reimbursements has been inefficient—until now.</p>\r\n<p>With Medical Billing, medical billing within workflow can be just a few clicks away. You can now check medical eligibility in real time. You can also bill for clinical services using x12 reimbursement transaction technology. This is the same billing pathway that physicians and hospitals use to request medical service reimbursements.</p>\r\n<p><a class=\"button small animate\" href=\"#\" data-animation=\"bounceIn\" data-delay=\"100\">Purchase Now </a></p>', '', 1, '2020-10-13 05:45:49', '2020-06-16 12:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `homeservices`
--

CREATE TABLE `homeservices` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeservices`
--

INSERT INTO `homeservices` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(9, 'Claims Processing Service ', '<p>Billing System is as intricate as any other medical discipline with perpetual reimbursement concerns. Users are required to keep track of their patients, medicines, equipment, and other possible ambiguities. Managing the community pharmacy’s patient claims can be a complex and time-consuming process.</p>\r\n<p>Billing System services aid in implementing all the pharmacy claims and billing challenges by providing essential connectivity between pharmacies and insurance companies. Data can be integrated into the system and transferred to insurance companies making the claims processing tasks more efficient and accurate.</p>', 'c942bdc1a8866d59ef360dee98865c51.jpg', 1, '2021-01-21 05:49:53', '2020-10-22 00:40:01'),
(10, 'Plumbing', '<p>Plumbing Services</p>', '', 1, '2021-02-16 05:02:57', '2021-02-16 05:02:57');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(5) NOT NULL,
  `client_id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `client_id`, `name`, `status`, `created_date`) VALUES
(1, 1, 'System Users', 1, '2020-09-24 19:27:27'),
(2, 1, 'System Roles', 1, '2020-09-24 19:27:27'),
(3, 1, 'Communications', 1, '2020-09-24 23:16:42'),
(4, 0, 'Services', 1, '2020-09-24 23:16:42'),
(5, 0, 'Insurance Companies', 1, '2020-09-24 23:17:16'),
(6, 0, 'Forms', 1, '2020-09-24 23:17:16'),
(7, 0, 'Insurance Forms', 1, '2020-09-24 23:17:16'),
(8, 0, 'Pharmacies', 1, '2020-09-24 23:17:16'),
(9, 0, 'Pharmacy Services', 1, '2020-09-24 23:17:16'),
(10, 1, 'Claims', 1, '2020-09-24 23:17:16'),
(11, 0, 'Reports', 1, '2020-09-24 23:18:36'),
(12, 0, 'Submissions', 1, '2020-09-24 23:18:36'),
(13, 0, 'Sliders', 1, '2020-09-24 23:18:36'),
(14, 0, 'About Us', 1, '2020-09-24 23:18:36'),
(15, 0, 'Home Services', 1, '2020-09-24 23:19:23'),
(16, 0, 'Features', 1, '2020-09-24 23:19:23'),
(17, 0, 'Testimonials', 1, '2020-09-24 23:19:23'),
(18, 0, 'All Contact Messages', 1, '2020-09-24 23:19:23'),
(19, 0, 'Contact Info', 1, '2020-09-24 23:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `menu_id`, `permission_id`) VALUES
(8, 2, 1),
(9, 2, 2),
(10, 2, 3),
(11, 2, 4),
(12, 8, 1),
(13, 8, 2),
(14, 8, 3),
(15, 8, 4),
(16, 18, 1),
(17, 18, 2),
(18, 18, 3),
(19, 18, 4),
(20, 3, 1),
(21, 3, 2),
(22, 1, 1),
(23, 1, 2),
(24, 1, 3),
(25, 1, 4),
(26, 5, 1),
(27, 5, 2),
(28, 5, 3),
(29, 5, 4),
(30, 10, 1),
(31, 10, 2),
(32, 10, 3),
(33, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `timings` varchar(255) NOT NULL,
  `subject_type` varchar(20) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `rating` int(5) NOT NULL,
  `message` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `reply_status` int(5) NOT NULL DEFAULT '0',
  `review_status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `user_id`, `last_name`, `first_name`, `email`, `phone`, `date`, `timings`, `subject_type`, `subject`, `rating`, `message`, `source`, `reply_status`, `review_status`, `create_date`, `update_date`) VALUES
(1, 0, 'Raghuvarma', 'Rudroju', '', '+1987654321000000', '', '', 'Review', 'Test', 0, 'wertert ert r yr hghgj h', '', 0, 0, '2021-02-14 14:19:07', NULL),
(2, 0, 'Hanu', 'Sushma', '', '+1 85828914747474', '', '', 'Review', 'Feedback', 0, 'Feedback', '', 0, 0, '2021-02-15 21:40:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passwordreset`
--

CREATE TABLE `passwordreset` (
  `resetid` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  `token` varchar(512) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passwordreset`
--

INSERT INTO `passwordreset` (`resetid`, `email`, `ipaddress`, `token`, `status`) VALUES
(1, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', '3gByVlonijZkOWYpr84as5IdevJATDtNq2L6U7xbSzHQRuCGhP', 1),
(2, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', 'dD9I1YSNmokMuiG8H6yw2zsUBWa45hJQOEVegp7F0Pb3tCfLlT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 04:55:00'),
(2, 'Create', 1, '2020-09-25 04:55:00'),
(3, 'Update', 1, '2020-09-25 04:55:11'),
(4, 'Delete', 1, '2020-09-25 04:55:11'),
(5, 'Change Order', 1, '2020-09-25 04:55:46'),
(6, 'Show/Hide', 1, '2020-09-25 04:55:46'),
(7, 'Generate Report', 1, '2020-09-26 04:22:43'),
(8, 'Submit Claim', 1, '2020-09-26 04:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `category_id` int(5) NOT NULL,
  `vendor_id` varchar(50) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text,
  `summary` text,
  `price` varchar(255) DEFAULT NULL,
  `units` varchar(255) NOT NULL,
  `upc` varchar(255) NOT NULL,
  `vpc` varchar(255) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `source` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `vendor_id`, `product_id`, `product_name`, `product_description`, `summary`, `price`, `units`, `upc`, `vpc`, `brand`, `tags`, `source`, `status`, `created_date`, `updated_date`) VALUES
(1, 2, '56', 'PRD4749863', 'Product1', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing.</p>', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing.', '22', 'test', '12345', '123456', 'test', 'test', NULL, 1, '2021-02-16 03:02:18', '2021-02-15 19:44:18'),
(2, 1, '56', 'PRD4404139', 'Product3', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing.</p>', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing.', '4', 'Pack', '12345', '123456', 'abc', 'abc', NULL, 1, '2021-02-16 03:02:32', '2021-02-15 19:43:32'),
(3, 1, '56', 'PRD7932103', 'Product2', '', NULL, '5', 'Dozen', '123', '123', 'abc', 'abc', NULL, 1, '2021-02-16 03:02:33', NULL),
(4, 2, '57', 'PRD1608556', 'Product4', '', NULL, '1', 'Unit', '123', '1234', 'abc', 'abc', NULL, 1, '2021-02-16 03:02:33', NULL),
(5, 3, '59', 'PRD0113840', 'Product5', '', NULL, '6', 'Unit', '456', '4567', 'def', 'def', NULL, 1, '2021-02-16 03:02:39', NULL),
(6, 3, '61', 'PRD8730792', 'Product6', '', NULL, '30', 'Window Frame', '123', '123', 'def', 'def', NULL, 1, '2021-02-16 03:02:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `image` varchar(200) NOT NULL,
  `is_primary` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `image`, `is_primary`) VALUES
(1, 1, '601299b86b2f1.png', 1),
(2, 2, '6017f10b25d59.png', 0),
(3, 3, '6028c6f60f3ea.png', 1),
(4, 3, '6028c6f6252bd.png', 0),
(5, 3, '6028c6f652469.png', 0),
(6, 3, '6028c6f661c54.png', 0),
(7, 3, '6028c6f677366.png', 0),
(8, 3, '6028c6f683217.png', 0),
(9, 3, '6028c6f68f8fd.png', 0),
(10, 56, '6028ca6576e40.png', 0),
(11, 56, '6028ca65843aa.png', 0),
(12, 56, '6028ca659328a.png', 0),
(13, 56, '6028ca659f35f.png', 0),
(14, 56, '6028ca65b2eb6.png', 0),
(15, 56, '6028ca65d19bc.png', 0),
(16, 56, '6028ca65df23f.png', 0),
(17, 56, '6028ca866ff91.png', 0),
(18, 56, '6028ca869275d.png', 0),
(19, 56, '6028ca869dfac.png', 0),
(20, 56, '6028cadd79408.png', 0),
(21, 56, '6028cadd87e26.png', 0),
(22, 56, '6028cadd9427e.png', 0),
(23, 56, '6028cc46e56c5.png', 0),
(24, 56, '6028cc4708297.png', 0),
(25, 56, '6028cc471dba8.png', 0),
(26, 56, '6028cc47332b4.png', 0),
(27, 56, '6028cc473f200.png', 0),
(28, 56, '6028cc5672eb0.png', 0),
(29, 56, '6028cc5684742.png', 0),
(30, 56, '6028cc569252a.png', 0),
(31, 56, '6028cc569f452.png', 0),
(32, 2, '6028cc979b626.png', 0),
(33, 2, '6028cc97aa4c7.png', 0),
(34, 2, '6028cc97bdf6e.png', 0),
(35, 2, '6028d3050e374.png', 0),
(36, 2, '6028d4c489e31.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `client_id` varchar(50) NOT NULL,
  `project_name` varchar(30) NOT NULL,
  `project_start_date` varchar(30) NOT NULL,
  `project_end_date` varchar(30) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `project_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `p_id`, `client_id`, `project_name`, `project_start_date`, `project_end_date`, `address1`, `address2`, `city`, `state`, `zip_code`, `project_description`, `status`, `created_at`, `updated_at`) VALUES
(23, 'PRJ6038366', '1344460', 'Project-1', '2021-01-28', '2021-02-28', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '<h3>California Renovation Specialist Steve Roper modernised and optimised this home’s bathroom, living room, dining room and roof. </h3>\r\n<h4>Renovating a ‘70s style house in California</h4>\r\n<p>Typefaces and layouts, and in appeara nce of different general the content of dummy text is nonsensical.typefaces of dummy text is nonsensical. designers think everything done by someone else is awful, and that they could do it better themselves, which explains why I designed my own living room carpet, I suppose. the architect represents neither a Dionysian nor an Apollinian condition: here it is the mighty act of will, the will which moves mountains, the intoxication of the strong will, which demands artistic expression. The most powerful men have always inspired the architects; the architect has always been influenced by power.</p>\r\n<p>When these homeowners decided that it was time to modernise their ‘70s-style house interior, they reached out to Refresh Renovations. They were then put in touch with their local Refresh Renovations franchise owner Steve Roper, who met with them in an initial renovation consultation.</p>\r\n<p>The homeowners primarily wanted to modernise their kitchen, improve its functionality and enhance its storage. They also wanted to renovate their bathroom, living room and living room. For added comfort, they asked Steve to re-roof their home and install a modern ventilation system.</p>\r\n<p>Refresh’s design and build process made it easy to work on so many projects at once. For every aspect of the renovation; Steve took care of the design, planning, product sourcing, deliveries, scheduling in tradespeople and project managing the project through to the homeowners’ desired result.</p>\r\n<h4>Kitchen design</h4>\r\n<p>Once a design and all-inclusive quote had been developed, Steve’s team began the kitchen renovation process. They completely replaced the original kitchen, installed new appliances and created more kitchen storage using new kitchen cabinets. The new kitchen is sleek, sophisticated and offers a balanced space where the clients can both cook and socialise.</p>\r\n<h4>A modern bathroom design</h4>\r\n<p>After some demolition work had been carried out, Steve’s team began modernising the bathroom so that it would meet his clients’ specifications. His team waterproofed the bathroom and installed new bathware, Robertsons tapware and fittings. Neptune’s waterproof flooring was installed and, to finish off the space, the bathtub and shower surrounds were tiled with bathroom tiles from The Tile Depot.</p>\r\n<h4>Updating the dining room</h4>\r\n<p>For the dining room, the homeowners simply wanted to install some durable flooring that could withstand their rambunctious pet dog. They opted for the same commercial-style Neptune flooring that was used in their bathroom.</p>\r\n<h4>Smart ventilation and a feature wall</h4>\r\n<p>To complete the interior, Steve’s team installed a smart ventilation system and carried out some painting and decorating. A unique element to the final design is an aquarium-inspired feature wall, specifically chosen by the clients.</p>\r\n<h4>Re-roofing</h4>\r\n<p>Finally, Steve’s team also re-roofed the old home, ensuring it would endure the seasons for years to come.</p>\r\n<h4>The renovated ‘70s style home</h4>\r\n<p>Now complete; the home offers much more functionality, modernity and personalisation. Steve says his clients are happy with the results.</p>\r\n<p>“It’s great! It’s certainly a step up from what it was. The clients’ expectations were exceeded too. The kitchen is my favourite!”</p>', 2, '2021-01-28 06:01:59', '2021-02-12 : 09:02:12'),
(24, 'PRJ8376564', '1344460', 'Project2', '2021-02-16', '1969-12-31', '97 ForgetMeNot Road', '', 'Cary', 'NC', '25374', '', 1, '2021-02-15 03:02:50', '2021-02-15 : 03:02:29');

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation`
--

CREATE TABLE `project_conversation` (
  `id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation_files`
--

CREATE TABLE `project_conversation_files` (
  `id` int(10) NOT NULL,
  `project_conversation_id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_documents`
--

CREATE TABLE `project_documents` (
  `id` int(10) NOT NULL,
  `document_type_id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_documents`
--

INSERT INTO `project_documents` (`id`, `document_type_id`, `project_id`, `file_name`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 4, 23, 'sadevelopers (3).sql', '1543e68460db85722606f1d813911c63.sql', 1, '2021-02-01 16:05:38', '2021-02-01 16:05:38'),
(2, 4, 23, 'product_categories (2).sql', '9f6d38ad5cdd28818c51f4780877e581.sql', 1, '2021-02-01 05:47:54', NULL),
(3, 5, 23, 'database (1).php', '622375075089881f2f53ef33a6c49078.php', 1, '2021-02-01 05:49:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_mapped_images`
--

CREATE TABLE `project_mapped_images` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `simage_id` int(5) NOT NULL,
  `gallery_type` int(5) NOT NULL,
  `mimage_id` int(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_mapped_images`
--

INSERT INTO `project_mapped_images` (`id`, `project_id`, `simage_id`, `gallery_type`, `mimage_id`, `status`) VALUES
(1, 23, 49, 1, 45, 1),
(2, 23, 49, 1, 46, 1),
(4, 23, 45, 2, 1, 1),
(5, 23, 45, 2, 4, 1),
(7, 23, 46, 2, 1, 1),
(8, 23, 46, 2, 4, 1),
(9, 23, 46, 2, 49, 1),
(10, 23, 46, 2, 50, 1),
(11, 23, 47, 2, 4, 1),
(12, 23, 47, 2, 49, 1),
(15, 23, 4, 1, 47, 1),
(16, 23, 4, 1, 48, 1),
(17, 23, 1, 1, 45, 1),
(18, 23, 1, 1, 46, 1),
(19, 23, 1, 1, 47, 1),
(20, 23, 48, 2, 4, 1),
(21, 23, 48, 2, 49, 1),
(22, 23, 48, 2, 54, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_products`
--

CREATE TABLE `project_products` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_products`
--

INSERT INTO `project_products` (`id`, `project_id`, `product_id`, `status`) VALUES
(1, 23, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_services`
--

CREATE TABLE `project_services` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `service_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_services`
--

INSERT INTO `project_services` (`id`, `project_id`, `service_id`, `status`) VALUES
(1, 23, 10, 1),
(2, 23, 11, 1),
(3, 24, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_status`
--

CREATE TABLE `project_status` (
  `project_status_id` int(11) NOT NULL,
  `project_status_name` varchar(255) NOT NULL,
  `status_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_status`
--

INSERT INTO `project_status` (`project_status_id`, `project_status_name`, `status_type`) VALUES
(1, 'New', 'bg-dark'),
(2, 'Inprogress', 'bg-primary'),
(3, 'Pending', 'bg-success'),
(4, 'On-Hold', 'bg-warning'),
(5, 'Cancelled', 'bg-info'),
(6, 'Completed', 'bg-dark');

-- --------------------------------------------------------

--
-- Table structure for table `project_status_history`
--

CREATE TABLE `project_status_history` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `status_id` varchar(255) NOT NULL,
  `status_description` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_status_history`
--

INSERT INTO `project_status_history` (`id`, `project_id`, `status_id`, `status_description`, `create_date`) VALUES
(488, 23, '3', 'zdvcxvxcv', '2021-02-03 19:21:30'),
(489, 23, '2', 'werv ewsrf sfsdf', '2021-02-03 19:26:18'),
(490, 23, '6', 'eawfd fsdsdfsd', '2021-02-04 12:17:50'),
(491, 23, '2', 'gergfr', '2021-02-12 00:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `project_updates`
--

CREATE TABLE `project_updates` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `create_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_updates`
--

INSERT INTO `project_updates` (`id`, `project_id`, `user_id`, `title`, `description`, `create_date`) VALUES
(1, 23, 1, 'rdhdfhf', 'asfdsfdsf', '2021-01-30 01:48:07'),
(2, 23, 1, 'fh', '45tetert', '2021-02-01 11:36:45'),
(3, 23, 1, 'ytgfjghjgh', 'ghjghjgh', '2021-02-11 13:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_comments`
--

CREATE TABLE `project_update_comments` (
  `id` int(5) NOT NULL,
  `project_update_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `comment` text NOT NULL,
  `created_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_comments`
--

INSERT INTO `project_update_comments` (`id`, `project_update_id`, `user_id`, `comment`, `created_date`) VALUES
(1, 2, 1, 'rthrtu', '2021-02-01 : 11:02:57'),
(2, 1, 1, 'hu hkhj,jk', '2021-02-07 : 02:02:39'),
(3, 2, 1, 'ljl,jkl', '2021-02-07 : 02:02:47'),
(4, 2, 1, 'jkljkljkl', '2021-02-07 : 02:02:51'),
(5, 2, 1, 'jkl jkljkljk', '2021-02-07 : 02:02:55'),
(6, 1, 1, 'jkljkljkljkl', '2021-02-07 : 02:02:03'),
(7, 2, 1, 'jghjgjghj', '2021-02-07 : 03:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_files`
--

CREATE TABLE `project_update_files` (
  `id` int(5) NOT NULL,
  `project_update_id` int(10) NOT NULL,
  `file` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_files`
--

INSERT INTO `project_update_files` (`id`, `project_update_id`, `file`, `create_date`) VALUES
(1, 2, '0d2d5f5c9f156d819fecaf4a8998877a.jpg', '2021-02-01 11:36:45');

-- --------------------------------------------------------

--
-- Table structure for table `project_website_permissions`
--

CREATE TABLE `project_website_permissions` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `project_address` int(5) NOT NULL,
  `client_infornation` int(5) NOT NULL,
  `project_gallery` int(5) NOT NULL,
  `project_reviews` int(5) NOT NULL,
  `project_vendors` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_website_permissions`
--

INSERT INTO `project_website_permissions` (`id`, `project_id`, `project_address`, `client_infornation`, `project_gallery`, `project_reviews`, `project_vendors`, `status`, `created_at`) VALUES
(1, 23, 1, 1, 1, 1, 1, 1, '2021-02-08 11:19:47'),
(2, 24, 0, 0, 0, 0, 0, 0, '2021-02-15 20:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `client_id` int(20) NOT NULL DEFAULT '0',
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `client_id`, `rolename`, `description`, `status`, `last_ip`, `created_at`, `updated_at`) VALUES
(0, 0, 'sadmin', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '2020-12-10 00:00:00'),
(1, 0, 'Marketing Manager', 'srt brdtdrt', 1, '', '2020-10-24 03:10:21', '0000-00-00 00:00:00'),
(2, 0, 'Admin', 'srt brdtdrt', 1, '', '2020-10-24 03:10:30', '0000-00-00 00:00:00'),
(3, 5, 'Manager', 'srt brdtdrt', 1, '', '2020-10-24 06:10:24', '0000-00-00 00:00:00'),
(4, 4, 'PAdmin', 'PAdmin', 1, '', '2020-10-31 07:10:58', '0000-00-00 00:00:00'),
(5, 14, 'Manager', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '0000-00-00 00:00:00'),
(6, 4540300, 'Manager', 'ged drfgdg', 1, '', '2021-01-05 01:01:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(1, 2, 18, 0),
(2, 2, 1, 0),
(3, 1, 1, 0),
(4, 1, 10, 0),
(5, 1, 5, 0),
(6, 1, 8, 0),
(7, 1, 5, 1),
(9, 1, 10, 1),
(13, 1, 8, 1),
(14, 1, 3, 0),
(15, 1, 3, 1),
(16, 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `image`, `status`, `home_status`, `created_date`, `updated_date`) VALUES
(10, 'SER4643945', 'Home Automation', 'Home Automation', '317b3c1885e63e34220fa09c6c022902.jpg', 1, 1, '2021-02-15 21:13:57', '2021-02-15 21:13:57'),
(11, 'SER0743037', 'Kitchens', '<p>Kitchens Description</p>', '2b399c72d642d9fedf0cade12cfd2ec5.jpg', 1, 1, '2021-02-15 21:13:58', '2021-02-15 21:13:58'),
(12, 'SER0551204', 'Basement Renovations', '<p>Basement Renovations</p>', '476132e3c836d2a77d638fa05a0a2c30.jpg', 1, 1, '2021-02-15 21:13:59', '2021-02-15 21:13:59'),
(13, 'SER5873447', 'Kitchen Renovation', '<p>Renovating Kitchens</p>', '', 1, 1, '2021-02-15 21:14:00', '2021-02-15 21:14:00'),
(14, 'SER6200257', 'Flooring', '<p>Flooring</p>', '', 1, 1, '2021-02-15 21:14:01', '2021-02-15 21:14:01'),
(15, 'SER6553718', 'Customized Decks', '<p>Decks, porches </p>', '', 1, 1, '2021-02-15 21:14:02', '2021-02-15 21:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

CREATE TABLE `service_categories` (
  `id` int(5) NOT NULL,
  `service_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_categories`
--

INSERT INTO `service_categories` (`id`, `service_id`, `category_id`, `status`) VALUES
(1, 7, 6, 1),
(2, 1, 8, 1),
(3, 8, 6, 1),
(4, 1, 8, 1),
(5, 6, 6, 1),
(6, 9, 8, 1),
(7, 10, 1, 1),
(8, 10, 2, 1),
(9, 11, 9, 1),
(10, 11, 10, 1),
(11, 12, 10, 1),
(12, 13, 3, 1),
(13, 13, 4, 1),
(14, 13, 6, 1),
(15, 13, 7, 1),
(16, 14, 4, 1),
(17, 14, 5, 1),
(18, 15, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `navigation_header` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `sidebar` varchar(255) NOT NULL,
  `font_family` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `navigation_header`, `header`, `sidebar`, `font_family`) VALUES
(1, 'color_1', 'color_2', 'color_13', 'roboto');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slides_id` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` text NOT NULL,
  `status` int(5) NOT NULL,
  `before_image` text NOT NULL,
  `image` text NOT NULL,
  `sorting` varchar(50) NOT NULL,
  `type` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slides_id`, `title`, `subtitle`, `status`, `before_image`, `image`, `sorting`, `type`, `home_status`, `created_date`, `updated_date`) VALUES
(8, 'SLI7299237', 'WELCOME TO S&A DEVELOPERS', 'S & A General Construction & Development, Inc', 1, '79e61bda637326cf9f3308af8967515c.jpg', '368292fedae13b0105da62bd67a533c7.jpg', '', 0, 0, '2021-02-09', '2021-02-09');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `firstname`, `lastname`, `middlename`, `designation`, `email`, `mobile_no`, `image`, `address1`, `address2`, `city`, `state`, `zip_code`, `notes`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sarfraz Afzal', 'M', '', 'President & CEO', 'test@gmail.com', '+1 (918) 686-8657', 'c55814a159a2b3e6618420b390aa8ffe.jpg', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '', '', '', '', 1, '2021-02-14 03:02:16', '0000-00-00 00:00:00'),
(2, 'Afzal', 'Zahid', '', 'Senior Project Manager', 'test@gmail.com', '+1 (987) 654-3210', 'dd0e5c076861ce46663645f4d28ed051.jpg', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '', '', '', '', 1, '2021-02-14 03:02:50', '0000-00-00 00:00:00'),
(3, 'Elmedani', 'Omar ', '', 'Design Coordinator', 'test@gmail.com', '+1 (987) 456-3210', '963bcf78da14d325401573127d6b49f9.jpg', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '', '', '', '', 1, '2021-02-14 03:02:58', '0000-00-00 00:00:00'),
(4, ' Ellahi', 'Amad', '', 'Manager', 'test@gmail.com', '+1 (918) 686-8657', 'b946236cb1ec0ae07036c17cc4e7481d.jpg', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '', '', '', '', 1, '2021-02-14 03:02:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(5) NOT NULL,
  `testimonials_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `update_status` int(5) NOT NULL,
  `version` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials_id`, `name`, `designation`, `description`, `image`, `status`, `home_status`, `update_status`, `version`, `created_date`, `updated_date`) VALUES
(8, 'TES0810481', 'ennoble', 'Developer', 'Billing System', '', 1, 0, 0, 0, '2020-09-22 12:48:08', '2020-09-22 12:48:08'),
(9, 'TES0147327', 'kumar praveen', 'Developer', 'Billing Admin', 'profile.jpg', 1, 0, 0, 0, '2020-09-26 06:45:24', '2020-09-26 06:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `client_id` int(10) NOT NULL,
  `user_type` int(5) NOT NULL COMMENT '0-admin, 1-client',
  `source` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` int(11) NOT NULL DEFAULT '1',
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `client_id`, `user_type`, `source`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `image`, `password`, `ein_number`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `notes`, `is_admin`, `is_new`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 0, 0, 'Superadmin', 'Rudroju', 'Raghuvarma', 'Raghuvarma', 'admin@sadevelopers.com', '+1 (898) 987-9879', 'profile.png', '$2y$10$rlZMfQerPQWcQLxj6RVuZ.4oMLBwUmYP2Hy3m7.j2q3Nf15R3gWPG', '', 0, '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', 1, 0, '', 1, '2017-09-29 10:09:44', '2021-01-27 12:01:36'),
(2, 'USR1344460', 1344460, 1, 'Social Media', 'smith', 'Mary', '', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', '540b1060dae3625fd86a02520bbffd06.jpg', '$2y$10$QnsPBWenukaG4dRBMTzrbuw6DdGJSUDcGdVdlALYTi6jAbt7fDIV2', '', 0, '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'Client Notes', 1, 1, '', 1, '2021-01-28 11:01:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(10) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_email` varchar(2552) NOT NULL,
  `vendor_mobile_no` varchar(50) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_id`, `vendor_name`, `vendor_email`, `vendor_mobile_no`, `address1`, `address2`, `city`, `state`, `zip_code`, `website_url`, `image`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `status`, `created_at`, `updated_at`) VALUES
(55, 'VND3166738', 'RIOBEL', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', 'www.test.com', '43013025eeb42a776ff69ed102cad32b.jpg', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '$2y$10$Vy/5ybAmOWKIfcd96eohcuKY4P9oEVHNgvKKsl6HQBHyUN3/lK5tO', 1, '2021-01-28 10:01:46', '0000-00-00 00:00:00'),
(56, 'VND5144396', 'TimberTech', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', 'www.test.com', 'ee01099bb8ebdd414180b2d76d742783.jpg', 'smith', 'Mary', '', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '$2y$10$YVvQ0SMhrdEmi79o60/nru356yb5h.ngkmShg6pANIkWwg1Oxp35K', 1, '2021-01-28 10:01:40', '0000-00-00 00:00:00'),
(57, 'VND3964395', 'Azek', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', 'www.test.com', '7fbaf32f70862e9b45ed40647b443155.jpg', 'Rudroju', 'Raghuvarma', '', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '$2y$10$2TE7bD8mECiRrmoc/F4fRODsmWY1jIEreZRVTiE8iHrHTb4OUOsPq', 1, '2021-01-28 03:01:41', '0000-00-00 00:00:00'),
(58, 'VND9433704', 'Avanity', 'avanity@test.com', '+1 (958) 198-4901', '111 Davis Drive', '', 'Charlotte', 'NC', '17481-8841', '', '91ed55dbb95d6ee98a5c0e299597d649.jpg', 'Richard', 'Wilson', '', 'wilson@avanity.com', '', '$2y$10$48Sf6Jr1YvtPjWRUGCFl/.jwUcmTUC0TY7R7QGHDmSKugFJ6ZJzLG', 1, '2021-02-16 02:02:09', '0000-00-00 00:00:00'),
(59, 'VND2904570', 'Bertch', 'Bertch@test.com', '+1 (582) 598-1908', '111 Davis Drive', '', 'Charlotte', 'NC', '17481-8841', 'www.bertch.com', 'a736438dd2360e90d64689499b526f99.jpg', 'Richard', 'Wilson', '', 'wilson@bertch.com', '', '$2y$10$kKjGUDoptnydofoD1vPH5OaHhNRpxlfKgETBV46xTfQ7Ig79tXBOa', 1, '2021-02-16 02:02:34', '0000-00-00 00:00:00'),
(60, 'VND9261266', 'Andersen', 'Andersen@test.com', '+1 (904) 910-5290', '726 Monta Vista Lane', '', 'Wake Forest', 'DE', '47891-7479', 'andersenwindows.com', 'dd8b9134910cc265bbdff94ce32b9f02.jpg', 'James', 'Robert', '', 'Robert@andersen.com', '', '$2y$10$ug/YTvkdVg8xBczJYh9A3eU2PcrpPj9q94WQ90YGYwEtRKUUDSdr.', 1, '2021-02-16 02:02:40', '0000-00-00 00:00:00'),
(61, 'VND4296021', 'Roca', 'roca@test.com', '+1 (764) 578-9276', '856 right lane', '', 'Rodeo', 'CA', '97856', 'www.roca.com', '645607eec100424cf9ff6f4061a4a6da.jpg', 'Andersen', 'Roca', '', 'andersen@roca.com', '', '$2y$10$dCbJjnYH2UN/RYM6F3Af.uQGIeg.zyqYshm4m4IjfjLYYJbkzWlaO', 1, '2021-02-16 02:02:03', '0000-00-00 00:00:00'),
(62, 'VND1113862', 'Voguebay', 'voguebay@test.com', '+1 (619) 960-1940', '4417 CORPORATION LANE', '', 'Virginia Beach', 'VA', '23462', 'voguebay.com', 'bc78965d4519a116a7fae41ae50913e9.jpg', 'Mary', 'Vogue', '', 'mary@voguebay.com', '', '$2y$10$toRjr2O/T7xjX8qw5OR7N.nw.1EkvEGYuO4WbR7LHFIfG2RcjjSUC', 1, '2021-02-16 02:02:19', '0000-00-00 00:00:00'),
(63, 'VND8735792', 'Bruce', 'bruce@test.com', '+1 (679) 985-9184', '5000 Centregreen Way', 'Suite 350', 'Cary', 'NC', '27513', 'bruce.com', 'b5eca55a690de6f1f625fc889ad7e2d4.jpg', 'Katy', 'Bruce', '', 'katy@bruce.com', '', '$2y$10$oe4O7ekdrQ9Jx8ocuuJJu.twa0hnUMIAl8H6FHVHpmgKcU.HPqrNu', 1, '2021-02-16 02:02:45', '0000-00-00 00:00:00'),
(64, 'VND5562407', 'Hawa', 'hawa@test.com', '+1 (354) 676-4722', '37 Meridian Rd', '', 'Edison', 'NJ', '08820', 'hawa.com', 'c8e7113b49e02b55572893558317c32c.jpg', 'Kim', 'Graner', '', 'kim@hawa.com', '', '$2y$10$dxzw8gex/uj0esQV2k.fcebfKa/1Yo2KWarT6KYbQTyBLd1XHSYGm', 1, '2021-02-16 02:02:31', '0000-00-00 00:00:00'),
(65, 'VND7844022', 'LG', 'lg@test.com', '+1 (378) 578-2582', '726 Monta Vista Lane', '', 'Wake Forest', 'DE', '47891-7479', 'lg.com', 'b08e6c9e446c0d1851f57bbe79ec9d10.jpg', 'James', 'Robert', '', 'robert@lg.com', '', '$2y$10$Yq/vwZAoqq6lfuD0UbpsXujsAqPcKuQlqdKRcjSRFpgczMc8Oy3q6', 1, '2021-02-16 02:02:05', '0000-00-00 00:00:00'),
(66, 'VND5386747', 'Best', 'best@test.com', '+1 (747) 481-7491', '917 franklin street', '', 'Newark', 'NJ', '07198', 'bestrangehoods.com', 'f7f1a8a237425683a4c4f396e40891ad.jpg', 'David', 'Brown', '', 'David@best.com', '', '$2y$10$DOOV9fVwPGujhPlhy3T1Lu9GPzM1qUf1qQWiEiPDqIV/t1Utok9Ua', 1, '2021-02-16 02:02:59', '0000-00-00 00:00:00'),
(67, 'VND4244005', 'Allegion', 'allegion@test.com', '+1 (211) 367-8167', '8735 HENDERSON ROAD', '', 'Tampa', 'FL', '33634', 'allegion.com', 'fed517cababa81e798448f4ad53c5192.jpg', 'Katy', 'Graner', '', 'Graner@allegion.com', '', '$2y$10$HOrQL9c23RN33ZfX1cnJ0uUUiMLbpwpRgPpP5fSfgLUWM84PzosG2', 1, '2021-02-16 02:02:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_categories`
--

CREATE TABLE `vendor_categories` (
  `id` int(5) NOT NULL,
  `vendor_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_categories`
--

INSERT INTO `vendor_categories` (`id`, `vendor_id`, `category_id`, `status`, `created_date`) VALUES
(1, 57, 9, 1, '2021-01-28 15:43:41'),
(7, 55, 1, 1, '2021-02-15 17:37:54'),
(8, 56, 1, 1, '2021-02-15 17:59:19'),
(9, 58, 2, 1, '2021-02-15 18:03:09'),
(10, 59, 2, 1, '2021-02-15 18:05:34'),
(11, 60, 3, 1, '2021-02-15 18:07:40'),
(12, 61, 3, 1, '2021-02-15 18:10:03'),
(13, 62, 4, 1, '2021-02-15 18:12:19'),
(14, 63, 5, 1, '2021-02-15 18:15:45'),
(15, 64, 5, 1, '2021-02-15 18:18:31'),
(16, 65, 6, 1, '2021-02-15 18:21:05'),
(17, 66, 6, 1, '2021-02-15 18:22:59'),
(18, 67, 7, 1, '2021-02-15 18:24:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_files`
--
ALTER TABLE `communications_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgetpasswordtoken`
--
ALTER TABLE `forgetpasswordtoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeservices`
--
ALTER TABLE `homeservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `passwordreset`
--
ALTER TABLE `passwordreset`
  ADD PRIMARY KEY (`resetid`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation`
--
ALTER TABLE `project_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_documents`
--
ALTER TABLE `project_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_mapped_images`
--
ALTER TABLE `project_mapped_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_products`
--
ALTER TABLE `project_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_services`
--
ALTER TABLE `project_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`project_status_id`);

--
-- Indexes for table `project_status_history`
--
ALTER TABLE `project_status_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_updates`
--
ALTER TABLE `project_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_files`
--
ALTER TABLE `project_update_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_website_permissions`
--
ALTER TABLE `project_website_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_files`
--
ALTER TABLE `communications_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `homeservices`
--
ALTER TABLE `homeservices`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `passwordreset`
--
ALTER TABLE `passwordreset`
  MODIFY `resetid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `project_conversation`
--
ALTER TABLE `project_conversation`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_documents`
--
ALTER TABLE `project_documents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_mapped_images`
--
ALTER TABLE `project_mapped_images`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `project_products`
--
ALTER TABLE `project_products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_services`
--
ALTER TABLE `project_services`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_status`
--
ALTER TABLE `project_status`
  MODIFY `project_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project_status_history`
--
ALTER TABLE `project_status_history`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=492;

--
-- AUTO_INCREMENT for table `project_updates`
--
ALTER TABLE `project_updates`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_update_files`
--
ALTER TABLE `project_update_files`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_website_permissions`
--
ALTER TABLE `project_website_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
