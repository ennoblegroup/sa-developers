-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 08, 2021 at 01:21 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sadevelopers`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(27, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers.</p>', '569a4be34c567b10dda004a9f3be2bc7.jpg', 0, '2021-01-08 20:36:08', '2021-01-08 15:00:20'),
(28, 'S & A – A Tradition Of Excellence', '<p>S & A General Construction & Development, Inc, currently located at 37 Meridian Road, Edison, NJ 08820.  Started in March 25, 1994 as a Partnership, it became a Corporation in March 2003 till present. Since 1994, we have worked in both the Private and Public sectors on various projects.  As a Prime Contractor for NYC Housing Preservation & Development, NYC School Construction Authority, NYC Dept of Environmental Protection, NYC Dept of Homeless, and NYC Housing Authority etc. S&A has worked on numerous Private Commercial projects as well, such as, Office Buildings, Medical Buildings, and Restaurants.</p>\r\n<p>To follow our Passion in residential-construction since 2003 , we have worked on countless Residential, Single Family Dwellings and custom built Luxury Homes and he enjoys working with in house- architect and interior designers. </p>', 'f7ca628ee1604dfc8098a9961a1207fb.png', 1, '2021-01-08 20:36:08', '2021-01-08 15:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` text,
  `file` text,
  `status` int(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Category-1', 'ytuygutyu', 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-07 19:10:01', '2021-02-07 01:32:01'),
(2, 'Category-2', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(3, 'Category-3', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(4, 'Category-4', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(5, 'Category-5', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(6, 'Category-6', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(7, 'Category-7', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(8, 'Category-8', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(9, 'Category-9', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(10, 'Category-10', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(11, 'Category-11', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(12, 'Category-12', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(13, 'Category-13', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(14, 'Category-14', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23'),
(15, 'Category-15', NULL, 'f32110dabc7fb9476dfa075ed11e5ac6.jpg', 1, '2021-02-01 13:29:23', '2021-02-01 13:29:23');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `pharmacy_id` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `pharmacy_lbn_name` varchar(25) NOT NULL,
  `pharmacy_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_files`
--

CREATE TABLE `communications_files` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `reply_id` varchar(25) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `contact` varchar(18) NOT NULL,
  `map` text NOT NULL,
  `fax_number` varchar(200) NOT NULL,
  `day_one` varchar(50) NOT NULL,
  `day_two` varchar(50) NOT NULL,
  `day_three` varchar(50) NOT NULL,
  `start_time` varchar(250) NOT NULL DEFAULT 'AM',
  `end_time` varchar(250) NOT NULL DEFAULT 'PM',
  `start_hour` varchar(250) NOT NULL DEFAULT 'AM',
  `end_hour` varchar(250) NOT NULL DEFAULT 'PM',
  `hours` varchar(250) NOT NULL DEFAULT '00:00:00',
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address1`, `title`, `description`, `address2`, `city`, `country`, `state`, `zip`, `status`, `email`, `phone`, `contact`, `map`, `fax_number`, `day_one`, `day_two`, `day_three`, `start_time`, `end_time`, `start_hour`, `end_hour`, `hours`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `create_date`, `update_date`) VALUES
(1, '917 Franklin Avenue', 'Contact Us', 'test', '', 'Newark', 'US', 'NJ', '07107-2809', 1, 'info@sadevelopers.com', '+1 (875) 423-4534', ' ', 'https://goo.gl/maps/q7weS', '+1 (973) 412 7303', '', '', '', '', '', '', '', '00:00:00', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.linkedin.com/               ', 'https://www.pinterest.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', '2019-03-21', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_replies`
--

CREATE TABLE `contactus_replies` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `reply_message` text NOT NULL,
  `replier_id` varchar(20) NOT NULL,
  `replier_name` varchar(255) NOT NULL,
  `read_status` int(20) NOT NULL DEFAULT '0',
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) NOT NULL,
  `document_type_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `document_type_id`, `file_name`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 4, 'sadevelopers (3).sql', '1543e68460db85722606f1d813911c63.sql', 1, '2021-01-29 02:21:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(5) NOT NULL,
  `document_type_name` varchar(50) NOT NULL,
  `document_type_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `document_type_name`, `document_type_description`, `status`, `created_date`, `updated_date`) VALUES
(4, 'Document Type-1', 'Document Type-1', 1, '2021-01-28 05:31:43', ''),
(5, 'Document Type-2', 'Document Type-2', 1, '2021-01-28 05:31:54', ''),
(6, 'Document Type-3', 'Document Type-3', 1, '2021-01-28 05:31:09', '');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `pharmacy_name` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `pharmacy_name`, `contact_name`, `email_address`, `phone_number`, `notes`, `create_date`) VALUES
(1, 'dfhdfh', 'fhg dfhdfgh', 'raghuvarma.chintu@gmail.com', '', 'gdf dfgfdgdfgfd', '2020-11-23 06:43:12'),
(2, 'er tvreterter', 're ter tert', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'er treter ter tretre', '2020-11-23 06:46:07'),
(3, 'drghdfh', 'gfhfghfgh', 'raghuvarma.chintu@gmail.com', '', 'gyj fthfthfghfgh', '2020-11-23 06:54:30'),
(4, 'tyujygjgyj', 'gyuityuityi', 'raghuvarma.chintu@gmail.com', '+1 (445) 354-3413', 'tyity nrtfhftghnfgh', '2020-11-23 06:58:50'),
(5, 'fgh fghfgh', 'fgh fgh fghf', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'fgh fgh fgh fgh', '2020-11-23 06:59:24'),
(6, 'kjbkbkjb', 'hgjhvjhy', 'raghuvarma.testmail@gmail.com', '+1 (987) 456-3210', 'ygfuyuy', '2020-11-23 07:00:53'),
(7, 'dghdfghdf', 'fghgh', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', 'fghfgh fghgf', '2020-11-23 07:06:28'),
(8, 'sdfdf sd', ' sdfsdf', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', 'sdf sdfdsfsdf', '2020-11-23 07:07:20'),
(9, 'dsgsg', 'dfgfdgdfg', 'raghuvarma.testmail@gmail.com', '+1 (918) 686-8657', 'dfg dfgdfgfd', '2020-11-23 07:08:11'),
(10, 'ertyret', 'ertertert', 'raghuvarma.chintu@gmail.com', '+1 (678) 867-4853', 'ret ertretert', '2020-11-23 07:09:42'),
(11, 'BestHealth', 'Sushma', 'sushmah@ennoblegrp.com', '+1 (481) 818-2020', 'I would like to discuss and register ', '2020-11-26 13:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(5) NOT NULL,
  `feature_id` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `feature_id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'FEA7082958', 'Automated Refill Management System', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data</li>\r\n<li xss=removed>“At a glance,” visibility of all refill management processes through a Refill Compliance Dashboard. The dashboard can be customized to include preferred metrics, including scheduled refills, missed refills, expired refills, and Med Sync Rx.</li>\r\n<li xss=removed>Automatically queues each day’s scheduled refills</li>\r\n<li xss=removed>RxSync - Sync all refills to a particular date</li>\r\n<li xss=removed>Return to Stock Queue - Prescriptions not picked up by patients will be flagged, with the system easily updated to reflect missed pickups, and drugs added back</li>\r\n</ul>', '8f0ae190c1e124c55bd3baafeae2d0b6.png', 1, '2020-10-22 12:58:57', '2020-10-22 03:40:14'),
(4, 'FEA4059235', 'Customized Workflow Management and Reports', '<ul class=\"list-style\">\r\n<li>Inventory management</li>\r\n<li>Reconciliation process ensures you will never miss a payment</li>\r\n<li>Bin management feature facilitates prescription pickup process</li>\r\n<li>Seamless management of incoming prescriptions</li>\r\n<li>Generate electronic orders with wholesalers based on your customized drug ordering parameters</li>\r\n<li>Ability to show cost comparisons between wholesalers</li>\r\n<li>Automatically update true cost from EDI files received from a wholesaler(s)</li>\r\n<li>Ensure compliance with all state and federal regulatory agencies</li>\r\n<li>Automatic backups of all data; Remote server access in case of emergency</li>\r\n</ul>', '94ac640051a3823130630b451e817c3b.png', 1, '2020-10-22 13:01:09', '2020-10-22 06:40:39'),
(5, 'FEA0320767', 'Pharmacy Communication', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Outbound calls, text/SMS messaging</li>\r\n<li xss=removed>Inbound communication allows patients to respond via prompts</li>\r\n<li xss=removed>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</li>\r\n<li xss=removed>Birthday greetings and other personalized messages sent via email or text/SMS message</li>\r\n</ul>', '2a2a80a67ee71c8c417b868eec47d7e3.png', 1, '2020-10-21 19:40:11', '2020-10-21 19:40:11'),
(6, 'FEA2267615', 'Report Generation', '<ul>\r\n<li>Customized reports to support the pharmacy’s unique needs</li>\r\n<li>The daily log provides a comprehensive report of each day’s activities</li>\r\n<li>Customized business analytics</li>\r\n<li>Identify key metrics and receive regular reports relevant to that data</li>\r\n<li>Business intelligence reports built into the system - why pay extra to third-party vendors?</li>\r\n</ul>', '467d1dc879a388c655b72582f7a80059.png', 1, '2020-11-04 12:57:05', '2020-10-26 23:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `forgetpasswordtoken`
--

CREATE TABLE `forgetpasswordtoken` (
  `id` int(5) NOT NULL,
  `UserID` int(25) NOT NULL,
  `TokenNumber` varchar(255) NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `TokenTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `gallery_type` int(5) NOT NULL,
  `visibility` int(5) NOT NULL,
  `file` text NOT NULL,
  `file_type` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `project_id`, `gallery_type`, `visibility`, `file`, `file_type`, `status`, `created_at`) VALUES
(1, 23, 1, 1, '2998af858f931150980140bac43fd104.jpg', 0, 0, '2021-02-01 17:29:20'),
(2, 23, 1, 1, 'e2d12aa0d9362decc534b678dc571a40.mp4', 1, 0, '2021-02-01 17:31:17'),
(3, 23, 0, 1, 'd4d5f209bd8d5d39ffe2ec457199668f.jpg', 0, 0, '2021-02-01 17:32:47'),
(4, 23, 1, 1, 'd568f4d508c0466e71a90751d225cbcf.png', 0, 0, '2021-02-01 17:40:23'),
(45, 23, 2, 1, '7e7688a788758c009ad5adef8762a90f.jpg', 0, 0, '2021-02-02 04:03:45'),
(46, 23, 2, 1, 'aa6a9bd1565280aaf34eb12ea69d6b19.jpg', 0, 0, '2021-02-02 04:03:46'),
(47, 23, 2, 1, '14eeac151d3a40abcb1cba954fe4a12c.jpg', 0, 0, '2021-02-02 04:03:46'),
(48, 23, 2, 1, '2721cef978a9099648811105f7b0b3cb.jpg', 0, 0, '2021-02-02 04:03:46'),
(49, 23, 1, 1, 'eb09697dcd2dc94f18b0a74266a1f839.jpg', 0, 0, '2021-02-02 05:10:45'),
(50, 23, 1, 1, '3bf313dc0aa4e454688303d9f8e4deb3.png', 0, 0, '2021-02-07 19:49:17'),
(51, 23, 1, 1, '3df182524b05ab2fbd0a2935588cfe0c.png', 0, 0, '2021-02-07 19:49:17'),
(52, 23, 1, 1, '92a1d85f4958f2509801afdf2dbf61cb.png', 0, 0, '2021-02-07 19:49:17'),
(53, 23, 1, 1, '57b7439c79945adcc51df90c7548686b.png', 0, 0, '2021-02-07 19:49:17'),
(54, 23, 1, 1, 'e541fbbe420b62ec3d29b167b6768665.png', 0, 0, '2021-02-07 19:49:17');

-- --------------------------------------------------------

--
-- Table structure for table `homeaboutus`
--

CREATE TABLE `homeaboutus` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeaboutus`
--

INSERT INTO `homeaboutus` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(11, 'Streamline Manual Billing', '<p>You might already offer clinical services which allow you to practice at the top of your license and provide convenient, needed services to your patients.</p>\r\n<p>But two problems persist. First, it’s often hard to know which patients are eligible for medical benefits. And second, submitting claims from pharmacy for medical plan reimbursements has been inefficient—until now.</p>\r\n<p>With Medical Billing, medical billing within workflow can be just a few clicks away. You can now check medical eligibility in real time. You can also bill for clinical services using x12 reimbursement transaction technology. This is the same billing pathway that physicians and hospitals use to request medical service reimbursements.</p>\r\n<p><a class=\"button small animate\" href=\"#\" data-animation=\"bounceIn\" data-delay=\"100\">Purchase Now </a></p>', '', 1, '2020-10-13 05:45:49', '2020-06-16 12:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `homeservices`
--

CREATE TABLE `homeservices` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeservices`
--

INSERT INTO `homeservices` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(9, 'Claims Processing Service ', '<p>Billing System is as intricate as any other medical discipline with perpetual reimbursement concerns. Users are required to keep track of their patients, medicines, equipment, and other possible ambiguities. Managing the community pharmacy’s patient claims can be a complex and time-consuming process.</p>\r\n<p>Billing System services aid in implementing all the pharmacy claims and billing challenges by providing essential connectivity between pharmacies and insurance companies. Data can be integrated into the system and transferred to insurance companies making the claims processing tasks more efficient and accurate.</p>', 'c942bdc1a8866d59ef360dee98865c51.jpg', 1, '2021-01-21 05:49:53', '2020-10-22 00:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(5) NOT NULL,
  `client_id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `client_id`, `name`, `status`, `created_date`) VALUES
(1, 1, 'System Users', 1, '2020-09-24 19:27:27'),
(2, 1, 'System Roles', 1, '2020-09-24 19:27:27'),
(3, 1, 'Communications', 1, '2020-09-24 23:16:42'),
(4, 0, 'Services', 1, '2020-09-24 23:16:42'),
(5, 0, 'Insurance Companies', 1, '2020-09-24 23:17:16'),
(6, 0, 'Forms', 1, '2020-09-24 23:17:16'),
(7, 0, 'Insurance Forms', 1, '2020-09-24 23:17:16'),
(8, 0, 'Pharmacies', 1, '2020-09-24 23:17:16'),
(9, 0, 'Pharmacy Services', 1, '2020-09-24 23:17:16'),
(10, 1, 'Claims', 1, '2020-09-24 23:17:16'),
(11, 0, 'Reports', 1, '2020-09-24 23:18:36'),
(12, 0, 'Submissions', 1, '2020-09-24 23:18:36'),
(13, 0, 'Sliders', 1, '2020-09-24 23:18:36'),
(14, 0, 'About Us', 1, '2020-09-24 23:18:36'),
(15, 0, 'Home Services', 1, '2020-09-24 23:19:23'),
(16, 0, 'Features', 1, '2020-09-24 23:19:23'),
(17, 0, 'Testimonials', 1, '2020-09-24 23:19:23'),
(18, 0, 'All Contact Messages', 1, '2020-09-24 23:19:23'),
(19, 0, 'Contact Info', 1, '2020-09-24 23:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `menu_id`, `permission_id`) VALUES
(8, 2, 1),
(9, 2, 2),
(10, 2, 3),
(11, 2, 4),
(12, 8, 1),
(13, 8, 2),
(14, 8, 3),
(15, 8, 4),
(16, 18, 1),
(17, 18, 2),
(18, 18, 3),
(19, 18, 4),
(20, 3, 1),
(21, 3, 2),
(22, 1, 1),
(23, 1, 2),
(24, 1, 3),
(25, 1, 4),
(26, 5, 1),
(27, 5, 2),
(28, 5, 3),
(29, 5, 4),
(30, 10, 1),
(31, 10, 2),
(32, 10, 3),
(33, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `timings` varchar(255) NOT NULL,
  `subject_type` varchar(20) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `rating` int(5) NOT NULL,
  `message` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `reply_status` int(5) NOT NULL DEFAULT '0',
  `review_status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `passwordreset`
--

CREATE TABLE `passwordreset` (
  `resetid` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  `token` varchar(512) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passwordreset`
--

INSERT INTO `passwordreset` (`resetid`, `email`, `ipaddress`, `token`, `status`) VALUES
(1, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', '3gByVlonijZkOWYpr84as5IdevJATDtNq2L6U7xbSzHQRuCGhP', 1),
(2, 'gouthamsambaraju4a7@gmail.com', '106.208.76.94', 'dD9I1YSNmokMuiG8H6yw2zsUBWa45hJQOEVegp7F0Pb3tCfLlT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 04:55:00'),
(2, 'Create', 1, '2020-09-25 04:55:00'),
(3, 'Update', 1, '2020-09-25 04:55:11'),
(4, 'Delete', 1, '2020-09-25 04:55:11'),
(5, 'Change Order', 1, '2020-09-25 04:55:46'),
(6, 'Show/Hide', 1, '2020-09-25 04:55:46'),
(7, 'Generate Report', 1, '2020-09-26 04:22:43'),
(8, 'Submit Claim', 1, '2020-09-26 04:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `category_id` int(5) NOT NULL,
  `vendor_id` varchar(50) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text,
  `summary` text,
  `price` varchar(255) DEFAULT NULL,
  `units` varchar(255) NOT NULL,
  `upc` varchar(255) NOT NULL,
  `vpc` varchar(255) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `source` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `vendor_id`, `product_id`, `product_name`, `product_description`, `summary`, `price`, `units`, `upc`, `vpc`, `brand`, `tags`, `source`, `status`, `created_date`, `updated_date`) VALUES
(1, 2, '56', 'PRD4749863', 'Product-1', '<p>Test Descrption</p>', NULL, '22', 'test', '12345', '123456', 'test', 'test', NULL, 1, '2021-02-01 20:32:29', NULL),
(2, 3, '56', 'PRD4404139', 'erg dfhfghfghfgh', '<p>rtyert</p>', NULL, '4', 'hgfh', '12345', '123456', 'fgh fgh', 'fghfgh', NULL, 1, '2021-02-03 05:32:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int(11) NOT NULL,
  `product_id` int(20) NOT NULL,
  `image` varchar(200) NOT NULL,
  `is_primary` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `image`, `is_primary`) VALUES
(1, 1, '601299b86b2f1.png', 1),
(2, 2, '6017f10b25d59.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `p_id` varchar(50) NOT NULL,
  `client_id` varchar(50) NOT NULL,
  `project_name` varchar(30) NOT NULL,
  `project_start_date` varchar(30) NOT NULL,
  `project_end_date` varchar(30) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `project_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `p_id`, `client_id`, `project_name`, `project_start_date`, `project_end_date`, `address1`, `address2`, `city`, `state`, `zip_code`, `project_description`, `status`, `created_at`, `updated_at`) VALUES
(23, 'PRJ6038366', '1344460', 'Project-1', '2021-01-28', '2021-02-28', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '<p>dhfghnfghfgh</p>', 6, '2021-01-28 06:01:59', '2021-02-08 : 07:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation`
--

CREATE TABLE `project_conversation` (
  `id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_conversation_files`
--

CREATE TABLE `project_conversation_files` (
  `id` int(10) NOT NULL,
  `project_conversation_id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_documents`
--

CREATE TABLE `project_documents` (
  `id` int(10) NOT NULL,
  `document_type_id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_documents`
--

INSERT INTO `project_documents` (`id`, `document_type_id`, `project_id`, `file_name`, `file`, `status`, `created_date`, `updated_date`) VALUES
(1, 4, 23, 'sadevelopers (3).sql', '1543e68460db85722606f1d813911c63.sql', 1, '2021-02-01 16:05:38', '2021-02-01 16:05:38'),
(2, 4, 23, 'product_categories (2).sql', '9f6d38ad5cdd28818c51f4780877e581.sql', 1, '2021-02-01 05:47:54', NULL),
(3, 5, 23, 'database (1).php', '622375075089881f2f53ef33a6c49078.php', 1, '2021-02-01 05:49:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_mapped_images`
--

CREATE TABLE `project_mapped_images` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `simage_id` int(5) NOT NULL,
  `gallery_type` int(5) NOT NULL,
  `mimage_id` int(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_mapped_images`
--

INSERT INTO `project_mapped_images` (`id`, `project_id`, `simage_id`, `gallery_type`, `mimage_id`, `status`) VALUES
(1, 23, 49, 1, 45, 1),
(2, 23, 49, 1, 46, 1),
(3, 23, 49, 1, 47, 1),
(4, 23, 45, 2, 1, 1),
(5, 23, 45, 2, 4, 1),
(6, 23, 49, 1, 48, 1),
(7, 23, 46, 2, 1, 1),
(8, 23, 46, 2, 4, 1),
(9, 23, 46, 2, 49, 1),
(10, 23, 46, 2, 50, 1),
(11, 23, 47, 2, 4, 1),
(12, 23, 47, 2, 49, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_products`
--

CREATE TABLE `project_products` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_products`
--

INSERT INTO `project_products` (`id`, `project_id`, `product_id`, `status`) VALUES
(1, 23, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_services`
--

CREATE TABLE `project_services` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `service_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_services`
--

INSERT INTO `project_services` (`id`, `project_id`, `service_id`, `status`) VALUES
(1, 23, 10, 1),
(2, 23, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project_status`
--

CREATE TABLE `project_status` (
  `project_status_id` int(11) NOT NULL,
  `project_status_name` varchar(255) NOT NULL,
  `status_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_status`
--

INSERT INTO `project_status` (`project_status_id`, `project_status_name`, `status_type`) VALUES
(1, 'New', 'bg-dark'),
(2, 'Inprogress', 'bg-primary'),
(3, 'Pending', 'bg-success'),
(4, 'On-Hold', 'bg-warning'),
(5, 'Cancelled', 'bg-info'),
(6, 'Completed', 'bg-dark');

-- --------------------------------------------------------

--
-- Table structure for table `project_status_history`
--

CREATE TABLE `project_status_history` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `status_id` varchar(255) NOT NULL,
  `status_description` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_status_history`
--

INSERT INTO `project_status_history` (`id`, `project_id`, `status_id`, `status_description`, `create_date`) VALUES
(488, 23, '3', 'zdvcxvxcv', '2021-02-03 19:21:30'),
(489, 23, '2', 'werv ewsrf sfsdf', '2021-02-03 19:26:18'),
(490, 23, '6', 'eawfd fsdsdfsd', '2021-02-04 12:17:50');

-- --------------------------------------------------------

--
-- Table structure for table `project_updates`
--

CREATE TABLE `project_updates` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `create_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_updates`
--

INSERT INTO `project_updates` (`id`, `project_id`, `user_id`, `title`, `description`, `create_date`) VALUES
(1, 23, 1, 'rdhdfhf', 'asfdsfdsf', '2021-01-30 01:48:07'),
(2, 23, 1, 'fh', '45tetert', '2021-02-01 11:36:45');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_comments`
--

CREATE TABLE `project_update_comments` (
  `id` int(5) NOT NULL,
  `project_update_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `comment` text NOT NULL,
  `created_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_comments`
--

INSERT INTO `project_update_comments` (`id`, `project_update_id`, `user_id`, `comment`, `created_date`) VALUES
(1, 2, 1, 'rthrtu', '2021-02-01 : 11:02:57'),
(2, 1, 1, 'hu hkhj,jk', '2021-02-07 : 02:02:39'),
(3, 2, 1, 'ljl,jkl', '2021-02-07 : 02:02:47'),
(4, 2, 1, 'jkljkljkl', '2021-02-07 : 02:02:51'),
(5, 2, 1, 'jkl jkljkljk', '2021-02-07 : 02:02:55'),
(6, 1, 1, 'jkljkljkljkl', '2021-02-07 : 02:02:03'),
(7, 2, 1, 'jghjgjghj', '2021-02-07 : 03:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `project_update_files`
--

CREATE TABLE `project_update_files` (
  `id` int(5) NOT NULL,
  `project_update_id` int(10) NOT NULL,
  `file` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_update_files`
--

INSERT INTO `project_update_files` (`id`, `project_update_id`, `file`, `create_date`) VALUES
(1, 2, '0d2d5f5c9f156d819fecaf4a8998877a.jpg', '2021-02-01 11:36:45');

-- --------------------------------------------------------

--
-- Table structure for table `project_website_permissions`
--

CREATE TABLE `project_website_permissions` (
  `id` int(5) NOT NULL,
  `project_id` int(5) NOT NULL,
  `project_address` int(5) NOT NULL,
  `client_infornation` int(5) NOT NULL,
  `project_gallery` int(5) NOT NULL,
  `project_reviews` int(5) NOT NULL,
  `project_vendors` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_website_permissions`
--

INSERT INTO `project_website_permissions` (`id`, `project_id`, `project_address`, `client_infornation`, `project_gallery`, `project_reviews`, `project_vendors`, `status`, `created_at`) VALUES
(1, 23, 1, 1, 1, 1, 1, 1, '2021-02-08 11:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `client_id` int(20) NOT NULL DEFAULT '0',
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `client_id`, `rolename`, `description`, `status`, `last_ip`, `created_at`, `updated_at`) VALUES
(0, 0, 'sadmin', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '2020-12-10 00:00:00'),
(1, 0, 'Marketing Manager', 'srt brdtdrt', 1, '', '2020-10-24 03:10:21', '0000-00-00 00:00:00'),
(2, 0, 'Admin', 'srt brdtdrt', 1, '', '2020-10-24 03:10:30', '0000-00-00 00:00:00'),
(3, 5, 'Manager', 'srt brdtdrt', 1, '', '2020-10-24 06:10:24', '0000-00-00 00:00:00'),
(4, 4, 'PAdmin', 'PAdmin', 1, '', '2020-10-31 07:10:58', '0000-00-00 00:00:00'),
(5, 14, 'Manager', 'srt brdtdrt', 1, '', '2020-11-30 08:11:56', '0000-00-00 00:00:00'),
(6, 4540300, 'Manager', 'ged drfgdg', 1, '', '2021-01-05 01:01:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(1, 2, 18, 0),
(2, 2, 1, 0),
(3, 1, 1, 0),
(4, 1, 10, 0),
(5, 1, 5, 0),
(6, 1, 8, 0),
(7, 1, 5, 1),
(9, 1, 10, 1),
(13, 1, 8, 1),
(14, 1, 3, 0),
(15, 1, 3, 1),
(16, 1, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `image`, `status`, `home_status`, `created_date`, `updated_date`) VALUES
(10, 'SER4643945', 'Custom Homes', '<p>Custom Homes Description</p>', '900ca6b288905f20309f71eb45abebbb.jpg', 1, 1, '2021-02-08 12:23:26', '2021-02-08 12:23:26'),
(11, 'SER0743037', 'Kitchens', '<p>Kitchens Description</p>', '883977b3ba11688d6f120e53064a2617.jpg', 1, 1, '2021-02-08 12:23:28', '2021-02-08 12:23:28'),
(12, 'SER0551204', 'Basement Renovations', '<p>Basement Renovations</p>', 'ad44d66a07656edf68ffc22b5579ebde.jpg', 1, 1, '2021-02-08 12:23:29', '2021-02-08 12:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `service_categories`
--

CREATE TABLE `service_categories` (
  `id` int(5) NOT NULL,
  `service_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_categories`
--

INSERT INTO `service_categories` (`id`, `service_id`, `category_id`, `status`) VALUES
(1, 7, 6, 1),
(2, 1, 8, 1),
(3, 8, 6, 1),
(4, 1, 8, 1),
(5, 6, 6, 1),
(6, 9, 8, 1),
(7, 10, 1, 1),
(8, 10, 2, 1),
(9, 11, 9, 1),
(10, 11, 10, 1),
(11, 12, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `navigation_header` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `sidebar` varchar(255) NOT NULL,
  `font_family` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `navigation_header`, `header`, `sidebar`, `font_family`) VALUES
(1, 'color_1', 'color_2', 'color_13', 'roboto');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slides_id` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` text NOT NULL,
  `status` int(5) NOT NULL,
  `image` text NOT NULL,
  `sorting` varchar(50) NOT NULL,
  `type` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slides_id`, `title`, `subtitle`, `status`, `image`, `sorting`, `type`, `home_status`, `created_date`, `updated_date`) VALUES
(7, 'SLI2521389', 'WELCOME TO S&A DEVELOPERS', 'S & A General Construction & Development, Inc', 1, 'a82c1f5ed6fe767d8da79cceac6578b9.jpg', '', 0, 0, '2021-01-08', '2021-01-08'),
(6, 'SLI6483647', 'Welcome to S&A Developers', 'S & A General Construction & Development, Inc', 1, '982e314cd504f8952e0e3e7a5273cfeb.jpg', '', 0, 0, '2020-11-02', '2020-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(5) NOT NULL,
  `testimonials_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `update_status` int(5) NOT NULL,
  `version` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials_id`, `name`, `designation`, `description`, `image`, `status`, `home_status`, `update_status`, `version`, `created_date`, `updated_date`) VALUES
(8, 'TES0810481', 'ennoble', 'Developer', 'Billing System', '', 1, 0, 0, 0, '2020-09-22 12:48:08', '2020-09-22 12:48:08'),
(9, 'TES0147327', 'kumar praveen', 'Developer', 'Billing Admin', 'profile.jpg', 1, 0, 0, 0, '2020-09-26 06:45:24', '2020-09-26 06:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `client_id` int(10) NOT NULL,
  `user_type` int(5) NOT NULL COMMENT '0-admin, 1-client',
  `source` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` int(11) NOT NULL DEFAULT '1',
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `client_id`, `user_type`, `source`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `image`, `password`, `ein_number`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `notes`, `is_admin`, `is_new`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 0, 0, 'Superadmin', 'Rudroju', 'Raghuvarma', 'Raghuvarma', 'admin@sadevelopers.com', '+1 (898) 987-9879', 'profile.png', '$2y$10$rlZMfQerPQWcQLxj6RVuZ.4oMLBwUmYP2Hy3m7.j2q3Nf15R3gWPG', '', 0, '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', 1, 0, '', 1, '2017-09-29 10:09:44', '2021-01-27 12:01:36'),
(2, 'USR1344460', 1344460, 1, 'Social Media', 'smith', 'Mary', '', 'raghuv@ennobletechnologies.com', '+1 (898) 987-9879', '540b1060dae3625fd86a02520bbffd06.jpg', '$2y$10$QnsPBWenukaG4dRBMTzrbuw6DdGJSUDcGdVdlALYTi6jAbt7fDIV2', '', 0, '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'Client Notes', 1, 1, '', 1, '2021-01-28 11:01:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(10) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_email` varchar(2552) NOT NULL,
  `vendor_mobile_no` varchar(50) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_id`, `vendor_name`, `vendor_email`, `vendor_mobile_no`, `address1`, `address2`, `city`, `state`, `zip_code`, `website_url`, `image`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `status`, `created_at`, `updated_at`) VALUES
(55, 'VND3166738', 'RIOBELjghjhj', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', 'www.test.com', 'c965a94ae57cc07804935a9edb911ed8.jpg', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '$2y$10$Vy/5ybAmOWKIfcd96eohcuKY4P9oEVHNgvKKsl6HQBHyUN3/lK5tO', 1, '2021-01-28 10:01:46', '0000-00-00 00:00:00'),
(56, 'VND5144396', 'ASTON GLOBAL', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', 'www.test.com', '7ef58cea32df982058f5cc18301523c2.jpg', 'smith', 'Mary', '', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '$2y$10$YVvQ0SMhrdEmi79o60/nru356yb5h.ngkmShg6pANIkWwg1Oxp35K', 1, '2021-01-28 10:01:40', '0000-00-00 00:00:00'),
(57, 'VND3964395', 'gjghj', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', 'www.test.com', '548e6da5f00f44a5d365492333feae59.jpg', 'Rudroju', 'Raghuvarma', '', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '$2y$10$2TE7bD8mECiRrmoc/F4fRODsmWY1jIEreZRVTiE8iHrHTb4OUOsPq', 1, '2021-01-28 03:01:41', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_categories`
--

CREATE TABLE `vendor_categories` (
  `id` int(5) NOT NULL,
  `vendor_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_categories`
--

INSERT INTO `vendor_categories` (`id`, `vendor_id`, `category_id`, `status`, `created_date`) VALUES
(1, 57, 9, 1, '2021-01-28 15:43:41'),
(2, 57, 10, 1, '2021-01-28 15:43:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_files`
--
ALTER TABLE `communications_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgetpasswordtoken`
--
ALTER TABLE `forgetpasswordtoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeservices`
--
ALTER TABLE `homeservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `passwordreset`
--
ALTER TABLE `passwordreset`
  ADD PRIMARY KEY (`resetid`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation`
--
ALTER TABLE `project_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_documents`
--
ALTER TABLE `project_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_mapped_images`
--
ALTER TABLE `project_mapped_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_products`
--
ALTER TABLE `project_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_services`
--
ALTER TABLE `project_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`project_status_id`);

--
-- Indexes for table `project_status_history`
--
ALTER TABLE `project_status_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_updates`
--
ALTER TABLE `project_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_update_files`
--
ALTER TABLE `project_update_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_website_permissions`
--
ALTER TABLE `project_website_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_categories`
--
ALTER TABLE `service_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_files`
--
ALTER TABLE `communications_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `homeservices`
--
ALTER TABLE `homeservices`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `passwordreset`
--
ALTER TABLE `passwordreset`
  MODIFY `resetid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `project_conversation`
--
ALTER TABLE `project_conversation`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_conversation_files`
--
ALTER TABLE `project_conversation_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_documents`
--
ALTER TABLE `project_documents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_mapped_images`
--
ALTER TABLE `project_mapped_images`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `project_products`
--
ALTER TABLE `project_products`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_services`
--
ALTER TABLE `project_services`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_status`
--
ALTER TABLE `project_status`
  MODIFY `project_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `project_status_history`
--
ALTER TABLE `project_status_history`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=491;

--
-- AUTO_INCREMENT for table `project_updates`
--
ALTER TABLE `project_updates`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project_update_comments`
--
ALTER TABLE `project_update_comments`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_update_files`
--
ALTER TABLE `project_update_files`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `project_website_permissions`
--
ALTER TABLE `project_website_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `service_categories`
--
ALTER TABLE `service_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `vendor_categories`
--
ALTER TABLE `vendor_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
