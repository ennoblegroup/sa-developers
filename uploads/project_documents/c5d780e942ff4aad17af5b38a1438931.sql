-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2021 at 02:22 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sadevelopers`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `client_id` int(10) NOT NULL,
  `user_type` int(5) NOT NULL COMMENT '0-admin, 1-client',
  `pharmacy_id` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `image` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` int(11) NOT NULL DEFAULT '1',
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `client_id`, `user_type`, `pharmacy_id`, `username`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `image`, `password`, `ein_number`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `is_admin`, `is_new`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 0, 0, 0, 'Superadmin', 'smith', 'Mary', '', 'admin@sadevelopers.com', '+1 (987) 654 3210', 'profile.png', '$2y$10$AFR83Qx4DsFQ2C8gjc9kEewObQ2P7ftp4PtES4ZzMHqw1Y.VfmpOO', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 1, 0, '', 1, '2017-09-29 10:09:44', '2020-10-12 03:10:07'),
(47, 'USR4540300', 4540300, 1, 0, 'raghuvarmarudroju', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '0019d675d84a97fb56e6a5b5b141d93b.jpg', '$2y$10$J66OXpsLYcrPf8HBKYk.KOM2mDJH5ZC80CT2c.EUatTMCvsUy3C22', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', 1, 1, '', 0, '2021-01-04 09:01:06', '0000-00-00 00:00:00'),
(48, 'USR8084526', 8084526, 1, 0, 'raghuvarmarudroju', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuv@ennobletechnologies.com', '+1 (987) 654-3210', '8ea696e2859c51c3c080244469fe6e70.jpg', '$2y$10$4wpVu53zsBEogOigc.DNyeSHGwN8Jv2LL43T3eyCDRsuI56CFiBKm', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', 1, 1, '', 1, '2021-01-04 11:01:43', '2021-01-04 11:01:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
