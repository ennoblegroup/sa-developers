-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 12, 2020 at 05:16 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekait_billing`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(25, 'About Us ', '<p class=\"MsoNormal\">Apollo Billing system provides a sophisticated solution for your medical billing needs, healthcare insurance and claims process. We are aware of the challenges faced with respect to the Healthcare sector across the United States. Realizing the significance of the medical billing system in our lives, we employed experienced staff to operate, coordinate and execute this system so as to provide continuous support to our Clients. They are equipped with years of experience gained after working with many healthcare organizations.<br><br>Our comprehensive Billing System Process is your answer to the following:</p>\r\n<ul>\r\n<li>Patient Registration</li>\r\n<li>Verification of Insurance</li>\r\n<li>Medical transcription</li>\r\n<li>Medical Coding w.r.to. diagnosis and procedures</li>\r\n<li>Entry of charges</li>\r\n<li>Preparation of Claims</li>\r\n<li>Managing denials</li>\r\n<li>Posting Payments</li>\r\n</ul>\r\n<p class=\"MsoNormal\">Our system makes your life easier and simpler than before. We stand as a pillar of support with you to handle your business needs and challenges. We are here because you need us. </p>', '569a4be34c567b10dda004a9f3be2bc7.jpg', 0, '2020-11-05 05:21:24', '2020-10-31 20:50:17'),
(26, 'About Us ', '<p class=\"MsoNormal\">The billing system provides a sophisticated solution for your medical billing needs, healthcare insurance, and claims process. We are aware of the challenges faced with respect to the Healthcare sector across the United States. Realizing the significance of the medical billing system in our lives, we employed experienced staff to operate, coordinate, and execute this system so as to provide continuous support to our Clients. They are equipped with years of experience gained after working with many healthcare organizations.<br><br>Our comprehensive Billing System Process is your answer to the following:</p>\r\n<ul>\r\n<li>Patient Registration</li>\r\n<li>Verification of Insurance</li>\r\n<li>Medical transcription</li>\r\n<li>Medical Coding w.r.to. diagnosis and procedures</li>\r\n<li>Entry of charges</li>\r\n<li>Preparation of Claims</li>\r\n<li>Managing denials</li>\r\n<li>Posting Payments</li>\r\n</ul>\r\n<p class=\"MsoNormal\">Our system makes your life easier and simpler than before. We stand as a pillar of support with you to handle your business needs and challenges. We are here because you need us. </p>', '569a4be34c567b10dda004a9f3be2bc7.jpg', 1, '2020-11-05 05:21:24', '2020-11-04 23:51:24');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject_type` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `create_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `claim_registration_id` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `pharmacy_id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `transaction_status` int(5) NOT NULL,
  `payment_status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claims`
--

INSERT INTO `claims` (`id`, `claim_registration_id`, `user_id`, `pharmacy_id`, `insurance_company_id`, `status`, `transaction_status`, `payment_status`, `created_date`, `updated_date`) VALUES
(1, 'CLA4767787', 1, 1, 1, 4, 7, 12, '2020-11-12 08:20:00', '2020-11-12 08:20:00'),
(2, 'CLA2590732', 1, 1, 1, 2, 7, 12, '2020-11-12 08:23:42', '2020-11-12 08:23:42'),
(3, 'CLA0055479', 1, 1, 1, 2, 6, 12, '2020-11-12 08:17:54', '2020-11-12 08:17:54'),
(4, 'CLA0910233', 1, 1, 1, 2, 6, 12, '2020-11-12 08:17:59', '2020-11-12 08:17:59'),
(5, 'CLA0448527', 1, 1, 1, 2, 6, 12, '2020-11-12 13:45:46', '2020-11-11 19:41:46'),
(6, 'CLA2606936', 1, 1, 1, 0, 6, 12, '2020-11-12 08:18:07', '2020-11-12 08:18:07'),
(7, 'CLA6212749', 1, 1, 1, 0, 6, 12, '2020-11-12 05:41:43', '2020-11-12 05:41:43'),
(8, 'CLA1372709', 1, 1, 1, 2, 6, 12, '2020-11-11 20:41:02', '2020-11-11 20:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `claim_additional_information`
--

CREATE TABLE `claim_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `service_start_date` date NOT NULL,
  `service_end_date` date NOT NULL,
  `service_place` varchar(255) NOT NULL,
  `emg` varchar(255) NOT NULL,
  `cpt_hcps` varchar(255) NOT NULL,
  `modifier` varchar(255) NOT NULL,
  `diagnosis_pointer` varchar(255) NOT NULL,
  `charges` float NOT NULL,
  `days` varchar(255) NOT NULL,
  `epsdt` varchar(255) NOT NULL,
  `id_qual` varchar(255) NOT NULL,
  `provider_id` varchar(255) NOT NULL,
  `cnpi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `claim_conversation`
--

CREATE TABLE `claim_conversation` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `description` text NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_conversation`
--

INSERT INTO `claim_conversation` (`id`, `claim_id`, `user_id`, `description`, `create_date`) VALUES
(1, 2, 1, '', '2020-11-11 06:50:42'),
(2, 2, 1, '', '2020-11-11 06:52:17'),
(3, 2, 1, '', '2020-11-11 07:39:13'),
(4, 2, 1, '', '2020-11-11 07:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `claim_conversation_files`
--

CREATE TABLE `claim_conversation_files` (
  `id` int(10) NOT NULL,
  `claim_conversation_id` int(10) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_conversation_files`
--

INSERT INTO `claim_conversation_files` (`id`, `claim_conversation_id`, `file_name`, `create_date`) VALUES
(1, 2, 'insurance_companies_(1).sql', '2020-11-11 06:52:17'),
(2, 3, 'Billing_system_application1.8-with_screens_(9).docx', '2020-11-11 07:39:14'),
(3, 4, 'insurance_companies_(1)1.sql', '2020-11-11 07:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `claim_diagnosis`
--

CREATE TABLE `claim_diagnosis` (
  `id` int(11) NOT NULL,
  `claim_id` int(11) NOT NULL,
  `diagnosis_a` varchar(10) DEFAULT NULL,
  `diagnosis_b` varchar(10) DEFAULT NULL,
  `diagnosis_c` varchar(10) DEFAULT NULL,
  `diagnosis_d` varchar(10) DEFAULT NULL,
  `diagnosis_e` varchar(10) DEFAULT NULL,
  `diagnosis_f` varchar(10) DEFAULT NULL,
  `diagnosis_g` varchar(10) DEFAULT NULL,
  `diagnosis_h` varchar(10) DEFAULT NULL,
  `diagnosis_i` varchar(10) DEFAULT NULL,
  `diagnosis_j` varchar(10) DEFAULT NULL,
  `diagnosis_k` varchar(10) DEFAULT NULL,
  `diagnosis_l` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_diagnosis`
--

INSERT INTO `claim_diagnosis` (`id`, `claim_id`, `diagnosis_a`, `diagnosis_b`, `diagnosis_c`, `diagnosis_d`, `diagnosis_e`, `diagnosis_f`, `diagnosis_g`, `diagnosis_h`, `diagnosis_i`, `diagnosis_j`, `diagnosis_k`, `diagnosis_l`) VALUES
(1, 2, '', '', 'r', '', '', 'yrty', '', '', 'yr', '', '', ''),
(2, 3, '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 4, '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 5, '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 6, '', '', '', 'ery', '', '', '', '', 'ery', '', '', ''),
(6, 7, '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 8, '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `claim_information`
--

CREATE TABLE `claim_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `employment` varchar(255) NOT NULL,
  `auto_accident` varchar(255) NOT NULL,
  `other_accident` varchar(255) NOT NULL,
  `accident_location` varchar(255) NOT NULL,
  `claim_code` varchar(255) NOT NULL,
  `iqual` varchar(50) NOT NULL,
  `injury_date` varchar(255) NOT NULL,
  `oqual` varchar(50) NOT NULL,
  `qual_date` varchar(255) NOT NULL,
  `nonworking_start_date` varchar(50) DEFAULT NULL,
  `nonworking_end_date` varchar(50) DEFAULT NULL,
  `hospital_start_date` varchar(50) DEFAULT NULL,
  `hospital_end_date` varchar(50) DEFAULT NULL,
  `additional_claim_information` varchar(255) NOT NULL,
  `out_lab` varchar(255) NOT NULL,
  `total_charge` varchar(255) NOT NULL,
  `icd` varchar(255) NOT NULL,
  `icda` varchar(255) NOT NULL,
  `icdb` varchar(255) NOT NULL,
  `icdc` varchar(255) NOT NULL,
  `resubmission_code` varchar(255) NOT NULL,
  `original_reference_number` varchar(255) NOT NULL,
  `prior_aurthorization_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_information`
--

INSERT INTO `claim_information` (`id`, `claim_id`, `employment`, `auto_accident`, `other_accident`, `accident_location`, `claim_code`, `iqual`, `injury_date`, `oqual`, `qual_date`, `nonworking_start_date`, `nonworking_end_date`, `hospital_start_date`, `hospital_end_date`, `additional_claim_information`, `out_lab`, `total_charge`, `icd`, `icda`, `icdb`, `icdc`, `resubmission_code`, `original_reference_number`, `prior_aurthorization_number`) VALUES
(1, 2, 'yes', 'yes', 'yes', 'AZ', 'u67776ty', '675', '2020-11-08', '', '2020-11-10', '', '', '', '', '', 'yes', '55', 'hkhkh', '', '', '', 'kgkhkh', 'ukgkh', 'ohkuh'),
(2, 3, 'yes', 'yes', 'yes', '', '', '', '01/01/1970', '', '01/01/1970', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '', 'yes', '', 'restes', '', '', '', '', '', ''),
(3, 4, 'yes', 'yes', 'yes', '', '', '', '01/01/1970', '', '01/01/1970', NULL, NULL, '1970-01-01', '1970-01-01', '', 'yes', '', 'restes', '', '', '', '', '', ''),
(4, 5, 'yes', 'yes', 'yes', '', '', 'dfj', '2020-11-09', 'dfg', '2020-11-10', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '', 'yes', '', 'p[', '', '', '', '', '', ''),
(5, 6, 'yes', 'yes', 'yes', '', '', '', '1970-01-01', '', '1970-01-01', '', '', '', '', '', 'yes', '', '', '', '', '', '', '', ''),
(6, 7, 'yes', 'yes', 'yes', '', '', '', '1970-01-01', '', '1970-01-01', '', '', '', '', '', 'yes', '', '', '', '', '', '', '', ''),
(7, 8, 'yes', 'yes', 'yes', '', '', 'dfg', '2020-11-10', '', '1970-01-01', '', '', '', '', '', 'yes', '', 'restes', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `claim_status`
--

CREATE TABLE `claim_status` (
  `claim_status_id` int(11) NOT NULL,
  `claim_status_group` int(5) NOT NULL,
  `claim_status_name` varchar(255) NOT NULL,
  `status_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_status`
--

INSERT INTO `claim_status` (`claim_status_id`, `claim_status_group`, `claim_status_name`, `status_type`) VALUES
(0, 1, 'Draft', 'bg-danger'),
(1, 1, 'Ready to Submit', 'bg-dark'),
(2, 1, 'Processing\r\n', 'bg-primary'),
(3, 1, 'Approved\r\n', 'bg-warning'),
(4, 1, 'Denied\r\n', 'bg-info'),
(5, 1, 'Closed\r\n', 'bg-dark'),
(6, 2, 'New', 'bg-success'),
(7, 2, 'Report Generated', 'bg-success'),
(8, 2, 'Submitted\r\n', 'bg-danger'),
(9, 2, 'Rejected\r\n', 'bg-success'),
(10, 2, 'Denied\r\n', 'bg-success'),
(11, 2, 'Approved\r\n', 'bg-danger'),
(12, 3, 'Not Received\r\n', 'bg-primary'),
(13, 3, 'Received', 'bg-success'),
(14, 3, 'Sent', 'bg-primary'),
(15, 3, 'Closed', 'bg-danger');

-- --------------------------------------------------------

--
-- Table structure for table `claim_status_group`
--

CREATE TABLE `claim_status_group` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_status_group`
--

INSERT INTO `claim_status_group` (`id`, `name`) VALUES
(1, 'claim_status'),
(2, 'transactional_status'),
(3, 'payment_status');

-- --------------------------------------------------------

--
-- Table structure for table `claim_status_history`
--

CREATE TABLE `claim_status_history` (
  `id` int(5) NOT NULL,
  `claim_id` int(5) NOT NULL,
  `status_group` int(5) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  `status_description` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_status_history`
--

INSERT INTO `claim_status_history` (`id`, `claim_id`, `status_group`, `status_name`, `status_description`, `create_date`) VALUES
(1, 1, 0, '2', '', '2020-11-06 12:52:24'),
(2, 1, 1, '3', 'sdf sdfgdg', '2020-11-06 16:01:45'),
(3, 1, 1, '5', 'rt5yreyrt y', '2020-11-06 16:13:29'),
(4, 1, 1, '3', 'Approved', '2020-11-06 18:38:05'),
(5, 1, 1, '5', 'testf', '2020-11-06 18:44:46'),
(6, 1, 1, '3', 'Approed', '2020-11-11 16:20:13'),
(7, 1, 1, '4', 'Test ', '2020-11-11 18:15:11'),
(8, 1, 2, '7', 'Teste', '2020-11-12 13:50:00'),
(9, 1, 2, '7', 'Testeeeee', '2020-11-12 13:52:59'),
(10, 2, 2, '7', 'ftguf ftufgy uy', '2020-11-12 13:53:42'),
(11, 7, 1, '0', '', '2020-11-12 16:38:43'),
(12, 7, 2, '6', '', '2020-11-12 16:38:43'),
(13, 7, 3, '12', '', '2020-11-12 16:38:44'),
(14, 8, 1, '2', '', '2020-11-12 19:58:02'),
(15, 8, 2, '6', '', '2020-11-12 19:58:02'),
(16, 8, 3, '12', '', '2020-11-12 19:58:02');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_files`
--

CREATE TABLE `communications_files` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `reply_id` varchar(25) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `contact` varchar(18) NOT NULL,
  `map` text NOT NULL,
  `fax_number` varchar(200) NOT NULL,
  `day_one` varchar(50) NOT NULL,
  `day_two` varchar(50) NOT NULL,
  `day_three` varchar(50) NOT NULL,
  `start_time` varchar(250) NOT NULL DEFAULT 'AM',
  `end_time` varchar(250) NOT NULL DEFAULT 'PM',
  `start_hour` varchar(250) NOT NULL DEFAULT 'AM',
  `end_hour` varchar(250) NOT NULL DEFAULT 'PM',
  `hours` varchar(250) NOT NULL DEFAULT '00:00:00',
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address1`, `title`, `description`, `address2`, `city`, `country`, `state`, `zip`, `status`, `email`, `phone`, `contact`, `map`, `fax_number`, `day_one`, `day_two`, `day_three`, `start_time`, `end_time`, `start_hour`, `end_hour`, `hours`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `create_date`, `update_date`) VALUES
(1, '917 Franklin Avenue', 'Contact Us', 'test', '', 'Newark', 'US', 'NJ', '07107-2809', 1, 'info@billing.com', '+1 (645) 645-5465', ' ', 'https://goo.gl/maps/q7weS', '+1 (973) 412 7303', '', '', '', '', '', '', '', '00:00:00', 'https://www.facebook.com/', 'https://twitter.com', 'https://www.linkedin.com/              ', 'https://www.pinterest.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', '2019-03-21', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `contactus_replies`
--

CREATE TABLE `contactus_replies` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `reply_message` text NOT NULL,
  `replier_id` varchar(20) NOT NULL,
  `replier_name` varchar(255) NOT NULL,
  `read_status` int(20) NOT NULL DEFAULT '0',
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(5) NOT NULL,
  `feature_id` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `feature_id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(1, 'FEA7082958', 'Automated Refill Management System', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data</li>\r\n<li xss=removed>“At a glance,” visibility of all refill management processes through a Refill Compliance Dashboard. The dashboard can be customized to include preferred metrics, including scheduled refills, missed refills, expired refills, and Med Sync Rx.</li>\r\n<li xss=removed>Automatically queues each day’s scheduled refills</li>\r\n<li xss=removed>RxSync - Sync all refills to a particular date</li>\r\n<li xss=removed>Return to Stock Queue - Prescriptions not picked up by patients will be flagged, with the system easily updated to reflect missed pickups, and drugs added back</li>\r\n</ul>', '8f0ae190c1e124c55bd3baafeae2d0b6.png', 1, '2020-10-22 12:58:57', '2020-10-22 03:40:14'),
(4, 'FEA4059235', 'Customized Workflow Management and Reports', '<ul class=\"list-style\">\r\n<li>Inventory management</li>\r\n<li>Reconciliation process ensures you will never miss a payment</li>\r\n<li>Bin management feature facilitates prescription pickup process</li>\r\n<li>Seamless management of incoming prescriptions</li>\r\n<li>Generate electronic orders with wholesalers based on your customized drug ordering parameters</li>\r\n<li>Ability to show cost comparisons between wholesalers</li>\r\n<li>Automatically update true cost from EDI files received from a wholesaler(s)</li>\r\n<li>Ensure compliance with all state and federal regulatory agencies</li>\r\n<li>Automatic backups of all data; Remote server access in case of emergency</li>\r\n</ul>', '94ac640051a3823130630b451e817c3b.png', 1, '2020-10-22 13:01:09', '2020-10-22 06:40:39'),
(5, 'FEA0320767', 'Pharmacy Communication', '<ul class=\"list-style\" xss=removed>\r\n<li xss=removed>Outbound calls, text/SMS messaging</li>\r\n<li xss=removed>Inbound communication allows patients to respond via prompts</li>\r\n<li xss=removed>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</li>\r\n<li xss=removed>Birthday greetings and other personalized messages sent via email or text/SMS message</li>\r\n</ul>', '2a2a80a67ee71c8c417b868eec47d7e3.png', 1, '2020-10-21 19:40:11', '2020-10-21 19:40:11'),
(6, 'FEA2267615', 'Report Generation', '<ul>\r\n<li>Customized reports to support the pharmacy’s unique needs</li>\r\n<li>The daily log provides a comprehensive report of each day’s activities</li>\r\n<li>Customized business analytics</li>\r\n<li>Identify key metrics and receive regular reports relevant to that data</li>\r\n<li>Business intelligence reports built into the system - why pay extra to third-party vendors?</li>\r\n</ul>', '467d1dc879a388c655b72582f7a80059.png', 1, '2020-11-04 12:57:05', '2020-10-26 23:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `forgetpasswordtoken`
--

CREATE TABLE `forgetpasswordtoken` (
  `id` int(5) NOT NULL,
  `UserID` int(25) NOT NULL,
  `TokenNumber` varchar(255) NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `TokenTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `file_source` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `file_source`, `form_id`, `form_name`, `form_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(27, '', 'FRM4742552', 'CMS1500', '', 'af83a03dfd0113ffe8959f04ac3b5613.pdf', 1, '2020-10-31 07:10:44', '2020-10-31 07:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `generated_claims`
--

CREATE TABLE `generated_claims` (
  `id` int(5) NOT NULL,
  `claim_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generated_claims`
--

INSERT INTO `generated_claims` (`id`, `claim_id`, `file_name`, `status`, `create_date`) VALUES
(1, 2, 'CLA2590732_11-11-2020_104900.pdf', 0, '2020-11-11 10:49:00'),
(2, 2, 'CLA2590732_11-11-2020_112610.pdf', 0, '2020-11-11 11:26:10'),
(3, 2, 'CLA2590732_11-12-2020_051817.pdf', 0, '2020-11-12 05:18:17'),
(4, 2, 'CLA2590732_11-12-2020_052754.pdf', 0, '2020-11-12 05:27:55'),
(5, 2, 'CLA2590732_11-12-2020_055135.pdf', 0, '2020-11-12 05:51:35'),
(6, 2, 'CLA2590732_11-12-2020_055737.pdf', 0, '2020-11-12 05:57:37'),
(7, 2, 'CLA2590732_11-12-2020_055852.pdf', 0, '2020-11-12 05:58:52'),
(8, 2, 'CLA2590732_11-12-2020_060449.pdf', 0, '2020-11-12 06:04:49'),
(9, 2, 'CLA2590732_11-12-2020_061248.pdf', 0, '2020-11-12 06:12:48'),
(10, 2, 'CLA2590732_11-12-2020_063605.pdf', 0, '2020-11-12 06:36:05'),
(11, 2, 'CLA2590732_11-12-2020_064013.pdf', 0, '2020-11-12 06:40:13'),
(12, 2, 'CLA2590732_11-12-2020_143005.pdf', 1, '2020-11-12 14:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `homeaboutus`
--

CREATE TABLE `homeaboutus` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeaboutus`
--

INSERT INTO `homeaboutus` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(11, 'Streamline Manual Billing', '<p>You might already offer clinical services which allow you to practice at the top of your license and provide convenient, needed services to your patients.</p>\r\n<p>But two problems persist. First, it’s often hard to know which patients are eligible for medical benefits. And second, submitting claims from pharmacy for medical plan reimbursements has been inefficient—until now.</p>\r\n<p>With Medical Billing, medical billing within workflow can be just a few clicks away. You can now check medical eligibility in real time. You can also bill for clinical services using x12 reimbursement transaction technology. This is the same billing pathway that physicians and hospitals use to request medical service reimbursements.</p>\r\n<p><a class=\"button small animate\" href=\"#\" data-animation=\"bounceIn\" data-delay=\"100\">Purchase Now </a></p>', '', 1, '2020-10-13 05:45:49', '2020-06-16 12:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `homeservices`
--

CREATE TABLE `homeservices` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeservices`
--

INSERT INTO `homeservices` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(9, 'Claims Processing Service ', '<p>Billing System is as intricate as any other medical discipline with perpetual reimbursement concerns. Users are required to keep track of their patients, medicines, equipment, and other possible ambiguities. Managing the community pharmacy’s patient claims can be a complex and time-consuming process.</p>\r\n<p>Billing System services aid in implementing all the pharmacy claims and billing challenges by providing essential connectivity between pharmacies and insurance companies. Data can be integrated into the system and transferred to insurance companies making the claims processing tasks more efficient and accurate.</p>', 'c942bdc1a8866d59ef360dee98865c51.jpg', 1, '2020-11-05 05:22:12', '2020-10-22 00:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_additional_information`
--

CREATE TABLE `insurance_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_checks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` int(10) NOT NULL,
  `insurance_company_id` varchar(255) NOT NULL,
  `insurance_company_name` varchar(30) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `naic_number` varchar(25) NOT NULL,
  `naic_group_code` varchar(25) NOT NULL,
  `state_of_domicile` varchar(25) NOT NULL,
  `company_type` varchar(25) NOT NULL,
  `company_status` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `insurance_company_id`, `insurance_company_name`, `ein_number`, `naic_number`, `naic_group_code`, `state_of_domicile`, `company_type`, `company_status`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `email_address`, `status`, `created_date`, `updated_date`) VALUES
(1, 'INS7379672', 'Blue cross blue shield', '', '', '', '', '', '', '141 Mountainview Blvd', '', 'The Bronx', 'NJ', '57676-5767', '+1 (918) 686-8657', 'raghuvarma.testmail@gmail.com', 1, '2020-11-03 13:36:29', '2020-11-03 13:36:29'),
(2, 'INS9516486', 'UnitedHealthcare', '', '', '', '', '', '', 'P.O. Box 740800', '', 'Atlanta', 'GA', '30374-0800', '+1 (888) 835-9637', 'info@unitedhealthcare.com', 1, '2020-11-11 17:11:10', '2020-11-11 17:11:10'),
(3, 'INS1013709', 'Kaiser Permanente', '', '', '', '', '', '', 'NCAL PO Box 12923', '', 'Oakland', 'CA', '94604-2923', '+1 (888) 901-4636', 'info@kaiser.com', 1, '2020-11-11 17:11:05', '2020-11-11 17:11:05'),
(4, 'INS4597390', 'Anthem Blue Cross Blue Shield', '', '', '', '', '', '', 'PO Box 105557', '', 'Atlanta', 'GA', '30348', '+1 (984) 229-9397', 'info@anthem.com', 1, '2020-11-11 17:11:06', '2020-11-11 17:11:06'),
(5, 'INS2362628', 'Aetna', '', '', '', '', '', '', '5000 Centregreen Way Suite 350', '', 'Cary', 'NC', '27513', '+1 (919) 337-1800', 'info@aetna.com', 1, '2020-11-11 17:11:48', '2020-11-11 17:11:48'),
(6, 'INS4216152', 'Blue Shield of California', '', '', '', '', '', '', 'P.O. Box 1505', '', 'Red Bluff', 'CA', '96080-1505', '+1 (800) 622-0632', 'info@blueshieldca.com', 1, '2020-11-11 17:11:47', '2020-11-11 17:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_forms`
--

CREATE TABLE `insurance_forms` (
  `id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `form_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_forms`
--

INSERT INTO `insurance_forms` (`id`, `insurance_company_id`, `form_id`, `status`) VALUES
(2, 1, 27, 1),
(4, 2, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_information`
--

CREATE TABLE `insurance_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_check` varchar(50) NOT NULL,
  `insured_id_number` varchar(255) NOT NULL,
  `insured_last_name` varchar(255) NOT NULL,
  `insured_first_name` varchar(255) NOT NULL,
  `insured_middle_name` varchar(255) NOT NULL,
  `insured_phone_number` varchar(20) NOT NULL,
  `insured_address1` varchar(255) NOT NULL,
  `insured_address2` varchar(255) NOT NULL,
  `insured_city` varchar(255) NOT NULL,
  `insured_state` varchar(255) NOT NULL,
  `insured_zip_code` varchar(255) NOT NULL,
  `other_insured_last_name` varchar(255) NOT NULL,
  `other_insured_first_name` varchar(255) NOT NULL,
  `other_insured_middle_name` varchar(255) NOT NULL,
  `insured_group_number` varchar(255) NOT NULL,
  `other_group_number` varchar(255) NOT NULL,
  `insurance_plan_name` varchar(255) NOT NULL,
  `other_insurance_plan_name` varchar(255) NOT NULL,
  `insured_dob` date NOT NULL,
  `insured_gender` varchar(255) NOT NULL,
  `iclaim_id` varchar(255) NOT NULL,
  `other_benefit_plan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_information`
--

INSERT INTO `insurance_information` (`id`, `claim_id`, `insured_check`, `insured_id_number`, `insured_last_name`, `insured_first_name`, `insured_middle_name`, `insured_phone_number`, `insured_address1`, `insured_address2`, `insured_city`, `insured_state`, `insured_zip_code`, `other_insured_last_name`, `other_insured_first_name`, `other_insured_middle_name`, `insured_group_number`, `other_group_number`, `insurance_plan_name`, `other_insurance_plan_name`, `insured_dob`, `insured_gender`, `iclaim_id`, `other_benefit_plan`) VALUES
(1, 2, 'FECA/Black Lung', '345345', 'Raghuvarma', 'Rudroju', 'Test2', '', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '', '', '', 'rtyrty ty', '', 'wwetY', '', '1970-01-01', 'male', '7554745745', 'no'),
(2, 3, '', '345345', 'Raghuvarma', 'Rudroju', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '', 'rtyrty ty', '', 'wwet2', '', '1970-01-01', 'male', '7554745745', 'no'),
(3, 4, '', '345345', 'Raghuvarma', 'Rudroju', '', '', '141 Mountainview Blvd', '', 'Wayne', 'NY', '74709', '', '', '', 'wettwet', '', 'wwet2', '', '1970-01-01', 'male', '7554745745', 'no'),
(4, 5, 'MEDICARE', '345345', 'Raghuvarma', 'Rudroju', 'Test2', '', '141 Mountainview Blvd', '', 'Wayne', 'NY', '74702', '', '', '', 'wettwet', '', 'wwet2', '', '1970-01-01', 'male', '7554745745', 'no'),
(5, 6, 'MEDICARE', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', 'male', '', 'yes'),
(6, 7, 'MEDICARE', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01', 'male', '', 'yes'),
(7, 8, 'MEDICARE', '345345', 'Raghuvarma', 'Rudroju', '', '', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', '', '', '', 'rtyrty ty', '', 'wwet2', '', '1970-01-01', 'male', '7554745745', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(5) NOT NULL,
  `pharmacy_id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `pharmacy_id`, `name`, `status`, `created_date`) VALUES
(1, 1, 'System Users', 1, '2020-09-24 19:27:27'),
(2, 1, 'System Roles', 1, '2020-09-24 19:27:27'),
(3, 1, 'Communications', 1, '2020-09-24 23:16:42'),
(4, 0, 'Services', 1, '2020-09-24 23:16:42'),
(5, 0, 'Insurance Companies', 1, '2020-09-24 23:17:16'),
(6, 0, 'Forms', 1, '2020-09-24 23:17:16'),
(7, 0, 'Insurance Forms', 1, '2020-09-24 23:17:16'),
(8, 0, 'Pharmacies', 1, '2020-09-24 23:17:16'),
(9, 0, 'Pharmacy Services', 1, '2020-09-24 23:17:16'),
(10, 1, 'Claims', 1, '2020-09-24 23:17:16'),
(11, 0, 'Reports', 1, '2020-09-24 23:18:36'),
(12, 0, 'Submissions', 1, '2020-09-24 23:18:36'),
(13, 0, 'Sliders', 1, '2020-09-24 23:18:36'),
(14, 0, 'About Us', 1, '2020-09-24 23:18:36'),
(15, 0, 'Home Services', 1, '2020-09-24 23:19:23'),
(16, 0, 'Features', 1, '2020-09-24 23:19:23'),
(17, 0, 'Testimonials', 1, '2020-09-24 23:19:23'),
(18, 0, 'All Contact Messages', 1, '2020-09-24 23:19:23'),
(19, 0, 'Contact Info', 1, '2020-09-24 23:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `menu_id`, `permission_id`) VALUES
(8, 2, 1),
(9, 2, 2),
(10, 2, 3),
(11, 2, 4),
(12, 8, 1),
(13, 8, 2),
(14, 8, 3),
(15, 8, 4),
(16, 18, 1),
(17, 18, 2),
(18, 18, 3),
(19, 18, 4),
(20, 3, 1),
(21, 3, 2),
(22, 1, 1),
(23, 1, 2),
(24, 1, 3),
(25, 1, 4),
(26, 5, 1),
(27, 5, 2),
(28, 5, 3),
(29, 5, 4),
(30, 10, 1),
(31, 10, 2),
(32, 10, 3),
(33, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `timings` varchar(255) NOT NULL,
  `subject_type` varchar(20) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `rating` int(5) NOT NULL,
  `message` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `reply_status` int(5) NOT NULL DEFAULT '0',
  `review_status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `passwordreset`
--

CREATE TABLE `passwordreset` (
  `resetid` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ipaddress` varchar(25) NOT NULL,
  `token` varchar(512) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patient_insured_information`
--

CREATE TABLE `patient_insured_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `patient_dob` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `patient_signature` varchar(255) NOT NULL,
  `insured_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_insured_information`
--

INSERT INTO `patient_insured_information` (`id`, `claim_id`, `last_name`, `first_name`, `middle_name`, `patient_dob`, `phone_number`, `gender`, `address1`, `address2`, `city`, `state`, `zip_code`, `account_number`, `relation`, `patient_signature`, `insured_signature`, `created_date`, `updated_date`) VALUES
(2, 2, 'Raghuvarma', 'Rudroju', 'Test2', '2020-11-09', '+1 (987) 654-3210', 'male', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '12345678998745', 'self', '', '', '2020-11-11 06:49:43', NULL),
(3, 3, 'Raghuvarma', 'Rudroju', '', '1970-01-01', '+1 (898) 987-9879', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '12345678998745', 'self', '', '', '2020-11-11 10:42:20', '2020-11-11 18:19:38'),
(4, 4, 'Raghuvarma', 'Rudroju', '', '1970-01-01', '+1 (987) 654-3210', 'male', '141 Mountainview Blvd', '', 'Wayne', 'NY', '74709', '12345678998745', 'self', '', '', '2020-11-11 13:49:03', '2020-11-11 18:54:09'),
(5, 5, 'Raghuvarma', 'Rudroju', 'Test2', '1970-01-01', '+1 (987) 654-3210', 'male', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '12345678998745', 'self', '', '', '2020-11-12 04:38:04', '2020-11-12 13:45:47'),
(6, 6, '', '', '', '1970-01-01', '', 'male', '', '', '', '', '', '', 'self', '', '', '2020-11-12 06:38:55', NULL),
(7, 7, 'Raghuvarma', '', '', '1970-01-01', '', 'male', '', '', '', '', '', '', 'self', '', '', '2020-11-12 11:08:44', NULL),
(8, 8, 'Raghuvarma', 'Rudroju', '', '2020-11-02', '+1 (987) 654-3210', 'male', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74709', '12345678998745', 'self', '', '', '2020-11-12 14:28:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 04:55:00'),
(2, 'Create', 1, '2020-09-25 04:55:00'),
(3, 'Update', 1, '2020-09-25 04:55:11'),
(4, 'Delete', 1, '2020-09-25 04:55:11'),
(5, 'Change Order', 1, '2020-09-25 04:55:46'),
(6, 'Show/Hide', 1, '2020-09-25 04:55:46'),
(7, 'Generate Report', 1, '2020-09-26 04:22:43'),
(8, 'Submit Claim', 1, '2020-09-26 04:22:43');

-- --------------------------------------------------------

--
-- Table structure for table `pharmacies`
--

CREATE TABLE `pharmacies` (
  `id` int(10) NOT NULL,
  `pharmacy_id` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `pharmacy_lbn_name` varchar(25) NOT NULL,
  `pharmacy_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pharmacies`
--

INSERT INTO `pharmacies` (`id`, `pharmacy_id`, `image`, `pharmacy_lbn_name`, `pharmacy_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR7167472', '34149c4fcdd1d77fd7b1561a39fae0a0.jpg', 'Branch Brook Pharmacy', 'BBPTT', 'Branch Brook Pharmacy', 'raghuvarma.chintu@gmail.com', '4558568222', '4575475', '', '', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '', 1, '2020-11-11 00:41:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pharmacies_services`
--

CREATE TABLE `pharmacies_services` (
  `id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `pharmacy_id` int(10) NOT NULL,
  `start_date` varchar(25) NOT NULL,
  `end_date` varchar(25) NOT NULL,
  `price` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacies_users`
--

CREATE TABLE `pharmacies_users` (
  `id` int(10) NOT NULL,
  `pharmacy_id` int(10) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `physician_information`
--

CREATE TABLE `physician_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `qualifier` varchar(255) NOT NULL,
  `reffering_last_name` varchar(50) NOT NULL,
  `reffering_first_name` varchar(50) NOT NULL,
  `reffering_middle_name` varchar(50) NOT NULL,
  `federal_tax_type` varchar(10) NOT NULL,
  `federal_tax_number` varchar(255) NOT NULL,
  `other_id` varchar(255) NOT NULL,
  `npi` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `paid_amount` varchar(255) NOT NULL,
  `service_location_name` varchar(255) NOT NULL,
  `service_phone_number` varchar(255) NOT NULL,
  `service_address1` varchar(255) NOT NULL,
  `service_address2` varchar(255) NOT NULL,
  `service_city` varchar(255) NOT NULL,
  `service_state` varchar(255) NOT NULL,
  `service_zip_code` varchar(255) NOT NULL,
  `service_other_id` varchar(255) NOT NULL,
  `service_npi` varchar(255) NOT NULL,
  `billing_location_name` varchar(255) NOT NULL,
  `billing_phone_number` varchar(255) NOT NULL,
  `billing_address1` varchar(255) NOT NULL,
  `billing_address2` varchar(255) NOT NULL,
  `billing_city` varchar(255) NOT NULL,
  `billing_state` varchar(255) NOT NULL,
  `billing_zip_code` varchar(255) NOT NULL,
  `billing_other_id` varchar(255) NOT NULL,
  `billing_npi` varchar(255) NOT NULL,
  `physician_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `physician_information`
--

INSERT INTO `physician_information` (`id`, `claim_id`, `qualifier`, `reffering_last_name`, `reffering_first_name`, `reffering_middle_name`, `federal_tax_type`, `federal_tax_number`, `other_id`, `npi`, `total_amount`, `paid_amount`, `service_location_name`, `service_phone_number`, `service_address1`, `service_address2`, `service_city`, `service_state`, `service_zip_code`, `service_other_id`, `service_npi`, `billing_location_name`, `billing_phone_number`, `billing_address1`, `billing_address2`, `billing_city`, `billing_state`, `billing_zip_code`, `billing_other_id`, `billing_npi`, `physician_signature`, `created_date`, `updated_date`) VALUES
(1, 2, 'ss', 'Raghuvarma', 'Rudroju', 'Test2', 'ssn', 'rturtu', '225675687686786', 'ghoioljlnl', '44', '33', '', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', '', '74702', '', '', 'retrtert', '+1 (898) 987-9879', '141 Mountainview Blvd', '', 'The Bronx', 'AL', '57676-5767', '', '', '', '2020-11-11 06:49:43', '2020-11-12 05:18:14'),
(2, 3, 'ss', 'Raghuvarma', 'Rudroju', 'Test2', 'ssn', 'rturtu', '', '1234567890', '44', '33', '', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '', '', 'egrg', '+1 (918) 686-8657', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '57676-5767', '', '', '', '2020-11-11 10:42:20', '2020-11-11 18:19:38'),
(3, 4, 'ss', 'Michael', 'smith', '', 'ssn', 'rturtu', '', 'ghoioljlnl', '44', '33', '', '+1 (918) 686-8657', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', 'retrtert', '+1 (918) 686-8657', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '2020-11-11 13:49:03', '2020-11-11 18:54:09'),
(4, 5, 'ss', 'Mary', 'smith', '', 'ssn', 'rturtu', '', 'ghoioljlnl', '44', '33', '', '+1 (918) 686-8657', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', 'egrg', '+1 (898) 987-9879', '8-3-223 near peddamma temple', '', 'karimmnagar', 'AL', '50500', '', '', '', '2020-11-12 04:38:04', '2020-11-12 13:45:47'),
(5, 6, '', '', '', '', 'ssn', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-11-12 06:38:56', NULL),
(6, 7, '', '', '', '', 'ssn', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-11-12 11:08:44', NULL),
(7, 8, 'ss', 'Raghuvarma', 'Rudroju', 'Test2', 'ssn', 'rturtu', '', 'ghoioljlnl', '44', '33', '', '+1 (987) 654-3210', '141 Mountainview Blvd', '', 'Wayne', 'NJ', '74702', '', '', 'egrg', '+1 (868) 686-5707', '141 Mountainview Blvd', '', 'The Bronx', 'NY', '57676-5767', '', '', '', '2020-11-12 14:28:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `pharmacy_id` int(20) NOT NULL DEFAULT '0',
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `pharmacy_id`, `rolename`, `description`, `status`, `last_ip`, `created_at`, `updated_at`) VALUES
(1, 0, 'Marketing Manager', 'srt brdtdrt', 1, '', '2020-10-24 03:10:21', '0000-00-00 00:00:00'),
(2, 0, 'Admin', 'srt brdtdrt', 1, '', '2020-10-24 03:10:30', '0000-00-00 00:00:00'),
(3, 5, 'Manager', 'srt brdtdrt', 1, '', '2020-10-24 06:10:24', '0000-00-00 00:00:00'),
(4, 4, 'PAdmin', 'PAdmin', 1, '', '2020-10-31 07:10:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(1, 2, 18, 0),
(2, 2, 1, 0),
(3, 1, 1, 0),
(4, 1, 10, 0),
(5, 1, 5, 0),
(6, 1, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'SER5050780', 'Claims Processing', 'Processing medical claims from the stage of receiving the prescription to the stage of payment processing', 1, '2020-10-31 09:28:54', '2020-10-31 09:28:54');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(5) NOT NULL,
  `navigation_header` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `sidebar` varchar(255) NOT NULL,
  `font_family` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `navigation_header`, `header`, `sidebar`, `font_family`) VALUES
(1, 'color_2', 'color_1', 'color_2', 'roboto');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slides_id` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` text NOT NULL,
  `status` int(5) NOT NULL,
  `image` text NOT NULL,
  `sorting` varchar(50) NOT NULL,
  `type` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slides_id`, `title`, `subtitle`, `status`, `image`, `sorting`, `type`, `home_status`, `created_date`, `updated_date`) VALUES
(1, 'SLI0196339', 'Efficient Claims Management Software', 'One stop solution for processing your Claims', 1, '5f7c022d05229232dd6597ee6153fe21.jpg', '', 0, 0, '2020-10-22', '2020-10-22'),
(6, 'SLI6483647', 'Efficient Claims Management Software', 'One stop solution for processing your Claims', 1, '8e09547bf7722a70b67cebd2bac9d4ad.jpg', '', 0, 0, '2020-11-02', '2020-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(5) NOT NULL,
  `testimonials_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `update_status` int(5) NOT NULL,
  `version` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials_id`, `name`, `designation`, `description`, `image`, `status`, `home_status`, `update_status`, `version`, `created_date`, `updated_date`) VALUES
(8, 'TES0810481', 'ennoble', 'Developer', 'Billing System', '', 1, 0, 0, 0, '2020-09-22 12:48:08', '2020-09-22 12:48:08'),
(9, 'TES0147327', 'kumar praveen', 'Developer', 'Billing Admin', 'profile.jpg', 1, 0, 0, 0, '2020-09-26 06:45:24', '2020-09-26 06:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `pharmacy_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_new` int(11) NOT NULL DEFAULT '1',
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `pharmacy_id`, `username`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `ein_number`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `is_admin`, `is_new`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(1, '', 0, 'Superadmin', 'smith', 'Mary', '', 'admin@billing.com', '+1 (987) 654 3210', '$2y$10$4gAw7h3X33kwf7M068Wd.ucYoN0opA6yVG0CzdPmV5O6cw5gQHudy', '', 0, '141 Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 1, 0, '', 1, '2017-09-29 10:09:44', '2020-10-12 03:10:07'),
(27, 'USR6185587', 1, 'Rudroju Test2 Raghuvarma', 'Rudroju', 'Raghuvarma', 'Test2', 'raghuvarma.chintu@gmail.com', '+1 (987) 654-3210', '$2y$10$4gAw7h3X33kwf7M068Wd.ucYoN0opA6yVG0CzdPmV5O6cw5gQHudy', '867867845', 0, '', '', '', '', '', 1, 1, '', 1, '2020-11-11 06:11:11', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key1` (`claim_id`);

--
-- Indexes for table `claim_conversation`
--
ALTER TABLE `claim_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_conversation_files`
--
ALTER TABLE `claim_conversation_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_diagnosis`
--
ALTER TABLE `claim_diagnosis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key2` (`claim_id`);

--
-- Indexes for table `claim_information`
--
ALTER TABLE `claim_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key3` (`claim_id`);

--
-- Indexes for table `claim_status`
--
ALTER TABLE `claim_status`
  ADD PRIMARY KEY (`claim_status_id`);

--
-- Indexes for table `claim_status_group`
--
ALTER TABLE `claim_status_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_status_history`
--
ALTER TABLE `claim_status_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_files`
--
ALTER TABLE `communications_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgetpasswordtoken`
--
ALTER TABLE `forgetpasswordtoken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generated_claims`
--
ALTER TABLE `generated_claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeservices`
--
ALTER TABLE `homeservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key4` (`claim_id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`form_id`);

--
-- Indexes for table `insurance_information`
--
ALTER TABLE `insurance_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key5` (`claim_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `passwordreset`
--
ALTER TABLE `passwordreset`
  ADD PRIMARY KEY (`resetid`);

--
-- Indexes for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key6` (`claim_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pharmacies`
--
ALTER TABLE `pharmacies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pharmacies_services`
--
ALTER TABLE `pharmacies_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `pharmacy_id` (`pharmacy_id`);

--
-- Indexes for table `pharmacies_users`
--
ALTER TABLE `pharmacies_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pharmacy_id` (`pharmacy_id`);

--
-- Indexes for table `physician_information`
--
ALTER TABLE `physician_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key7` (`claim_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `claim_conversation`
--
ALTER TABLE `claim_conversation`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `claim_conversation_files`
--
ALTER TABLE `claim_conversation_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `claim_diagnosis`
--
ALTER TABLE `claim_diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `claim_information`
--
ALTER TABLE `claim_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `claim_status`
--
ALTER TABLE `claim_status`
  MODIFY `claim_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `claim_status_history`
--
ALTER TABLE `claim_status_history`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_files`
--
ALTER TABLE `communications_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contactus_replies`
--
ALTER TABLE `contactus_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `generated_claims`
--
ALTER TABLE `generated_claims`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `homeaboutus`
--
ALTER TABLE `homeaboutus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `homeservices`
--
ALTER TABLE `homeservices`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `insurance_information`
--
ALTER TABLE `insurance_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `passwordreset`
--
ALTER TABLE `passwordreset`
  MODIFY `resetid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pharmacies`
--
ALTER TABLE `pharmacies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pharmacies_services`
--
ALTER TABLE `pharmacies_services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pharmacies_users`
--
ALTER TABLE `pharmacies_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `physician_information`
--
ALTER TABLE `physician_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  ADD CONSTRAINT `foreign key1` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `claim_diagnosis`
--
ALTER TABLE `claim_diagnosis`
  ADD CONSTRAINT `foreign key2` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `claim_information`
--
ALTER TABLE `claim_information`
  ADD CONSTRAINT `foreign key3` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);

--
-- Constraints for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  ADD CONSTRAINT `foreign key4` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  ADD CONSTRAINT `foreign key` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `insurance_information`
--
ALTER TABLE `insurance_information`
  ADD CONSTRAINT `foreign key5` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  ADD CONSTRAINT `foreign key6` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `physician_information`
--
ALTER TABLE `physician_information`
  ADD CONSTRAINT `foreign key7` FOREIGN KEY (`claim_id`) REFERENCES `claims` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
