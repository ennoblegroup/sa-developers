<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart']});
		google.charts.setOnLoadCallback(drawChart);

function drawChart() {
   
		var data = google.visualization.arrayToDataTable([ 
		['Month',''],
		['Jan-2020',5],   ['Feb-2020',6],  ['Mar-2020',3],  ['Apr-2020',10],  ['May-2020',3],  ['Jun-2020',8],  ['Jul-2020',7],  ['Aug-2020',12],  ['Sep-2020',9],  ['Oct-2020',7]
        ]);
      

      var options = {
        hAxis: {
          title: 'Month',
		  format: 'date',
        },
        vAxis: {
          title: 'Average Processing Time'
        }
      };

      var chart = new google.visualization.AreaChart(document.getElementById('average_claim'));

      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Status', ''],
        ['New', 200],            
        ['Pending', 20],            
        ['Submitted', 100],
        ['Rejected', 10 ], 
        ['Approved', 50 ], 
        ['Denied', 10 ], 
        ['Payment Received', 150 ], 
        ['Payment Sent', 50 ], 
      ]);

      

      var options = {       
        hAxis: {
          title: 'Status',
		  format: 'title',          
        },
        vAxis: {
          title: '# of Claims'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('client_claims'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', ''],
        ['State Farm', 50],            
        ['Berkshire Hathaway', 63],            
        ['Liberty Mutual', 56],
        ['Allstate', 98], 
        ['Progressive', 46], 
        ['Travelers', 53],
      ]);

      

      var options = {       
        hAxis: {
          title: 'Insurance Company',
		  format: 'title',          
        },
        vAxis: {
          title: 'Number of Claims (in process)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('insurance_claims'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Claim Amount ($s)'],
        ['State Farm', 12000],            
        ['Berkshire Hathaway', 13460],            
        ['Liberty Mutual', 11876],
        ['Allstate', 23000], 
        ['Progressive', 9005], 
        ['Travelers', 11800],
      ]);

      

     var options = {
          title: 'Pending Claim Amount($s)',
		  pieSliceText: 'value',
          pieHole: 0.4,
        };

      var chart = new google.visualization.PieChart(document.getElementById('insurance_claims_amt'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['clients', 'Claim Amount ($s)'],
        ['Branch Brook client', 12000],            
        ['Mason client', 13460],            
        ['Vim client', 11876],
        ['Hill client', 23000], 
        ['Batish client', 9005], 
        ['Freedom client', 11800],
      ]);

      

      var options = {       
        hAxis: {
          title: 'clients',
		  format: 'title',          
        },
        vAxis: {
          title: 'Claim Amount($s)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('client_claims_amt'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Rejection Rate (%)'],
        ['State Farm', 5],            
        ['Berkshire Hathaway', 10],            
        ['Liberty Mutual', 2],
        ['Allstate', 4], 
        ['Progressive', 6], 
        ['Travelers', 4],
      ]);      

      var options = {       
        hAxis: {
          title: 'Insurance Company',
		  format: 'title',          
        },
        vAxis: {
          title: 'Rejection Rate (%)'
        }
      };
	 
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
	  chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {

        var dashboard = new google.visualization.Dashboard(
          document.getElementById('client_rejection'));

        // We omit "var" so that programmaticSlider is visible to changeRange.
        var programmaticSlider = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'client_control_div',
          'options': {
            'filterColumnLabel': 'client',
            'ui': {'labelStacking': 'vertical'}
          }
        });

        var programmaticChart  = new google.visualization.ChartWrapper({
          'chartType': 'PieChart',
          'containerId': 'client_chart_div',
          'options': {
            'width': 500,
            'height': 250
          }
        });
		
        var data = google.visualization.arrayToDataTable([
        ['client', 'Rejection Rate (%)'],
        ['Branch Brook client', 5],            
        ['Mason client', 10],            
        ['Vim client', 2],
        ['Hill client', 4], 
        ['Batish client', 6], 
        ['Freedom client', 4]
        ]);
		
		 var options = {       
			hAxis: {
			  title: 'clients',
			  format: 'title',          
			},
			vAxis: {
			  title: 'Rejection Rate (%)'
			}
		};
		
        dashboard.bind(programmaticSlider, programmaticChart);
        dashboard.draw(data,options);
       
      }

    </script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart', 'controls']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {

        var dashboard = new google.visualization.Dashboard(
          document.getElementById('insurance_rejection'));

        // We omit "var" so that programmaticSlider is visible to changeRange.
        var programmaticSlider = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'programmatic_control_div',
          'options': {
            'filterColumnLabel': 'Insurance Company',
            'ui': {'labelStacking': 'vertical'}
          }
        });

        var programmaticChart  = new google.visualization.ChartWrapper({
          'chartType': 'PieChart',
          'containerId': 'programmatic_chart_div',
          'options': {
            'width': 500,
            'height': 250
          }
        });

        var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Rejection Rate (%)'],
        ['State Farm', 5],            
        ['Berkshire Hathaway', 10],            
        ['Liberty Mutual', 2],
        ['Allstate', 4], 
        ['Progressive', 6], 
        ['Travelers', 4]
        ]);
		
		 var options = {       
			hAxis: {
			  title: 'Insurance Company',
			  format: 'title',          
			},
			vAxis: {
			  title: 'Rejection Rate (%)'
			}
		};
		
        dashboard.bind(programmaticSlider, programmaticChart);
        dashboard.draw(data,options);
       
      }

    </script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {

        var dashboard = new google.visualization.Dashboard(
          document.getElementById('client_denial'));

        // We omit "var" so that programmaticSlider is visible to changeRange.
        var programmaticSlider = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'client_denial_div',
          'options': {
            'filterColumnLabel': 'client',
            'ui': {'labelStacking': 'vertical'}
          }
        });

        var programmaticChart  = new google.visualization.ChartWrapper({
          'chartType': 'PieChart',
          'containerId': 'client_denialchart_div',
          'options': {
            'width': 500,
            'height': 250
          }
        });
		
        var data = google.visualization.arrayToDataTable([
        ['client', 'Rejection Rate (%)'],
        ['Branch Brook client', 5],            
        ['Mason client', 30],            
        ['Vim client', 1],
        ['Hill client', 4], 
        ['Batish client', 8], 
        ['Freedom client', 10]
        ]);
		
		 var options = {       
			hAxis: {
			  title: 'clients',
			  format: 'title',          
			},
			vAxis: {
			  title: 'Rejection Rate (%)'
			}
		};
		
        dashboard.bind(programmaticSlider, programmaticChart);
        dashboard.draw(data,options);
       
      }

    </script>