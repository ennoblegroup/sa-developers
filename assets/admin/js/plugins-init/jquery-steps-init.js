(function($) {
    "use strict"
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid Zip Code.");
    $.validator.addMethod("price_regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid amount.");
	$.validator.addMethod("dn_regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid DEA Number.");
	$.validator.addMethod("ln_regex", function(value, element, regexpr) {          
        return regexpr.test(value);
    }, "Please enter a valid License Number.");
    var form = $("#step-form-horizontal ");
    form.children('div').steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true, 
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            		
            if(currentIndex==0){
                
                var relation=$("input[name='relation']:checked").val()
                if(relation=='self'){
                    $('#insured_last_name').val($('#last_name').val());
                    $('#insured_first_name').val($('#first_name').val());
                    $('#insured_dob').val($('#patient_dob').val());
                    $('#insured_phone_number').val($('#phone_number').val());
                    $('#insured_middle_name').val($('#middle_name').val());
                    $('#insured_address1').val($('#address1').val());
                    $('#insured_address2').val($('#address2').val());
                    $('#insured_city').val($('#city').val());
                    $('#insured_state').val($('#state').val());
                    $('#insured_zip_code').val($('#zip_code').val());
                    $("#insured_state").selectpicker("refresh");
                }else{
                    $('#insured_last_name').val('');
                    $('#insured_first_name').val('');
                    $('#insured_phone_number').val('');
                    $('#insured_middle_name').val('');
                    $('#insured_address1').val('');
                    $('#insured_address2').val('');
                    $('#insured_city').val('');
                    $('#insured_state').val('');
                    $('#insured_zip_code').val('');
                    $("#insured_state").selectpicker("refresh");
                }
                var step1_errors=0;
                var stp1=form.validate({
                    onkeyup: true, 
                    onclick: true,
                    rules: {
                        "last_name": {
                            required: !0,
                            minlength: 3
                        },
                        "first_name": {
                            required: !0,
                            minlength: 3
                        },
                        "address1": {
                            required: !0,
                            minlength: 5,
                            maxlength:28
                        },
                        "city": {
                            required: !0,
                            minlength: 3
                        },
                        "state": {
                            required: !0
                        },
                        "zip_code": {
                            required: !0,
                            zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                        },
                        "account_number": {
                            required: !0,
                            maxlength:14                           
                        },
                        "patient_dob": {
                            required: !0
                        },
						"patient_signature": {
                            required: !0
                        },
						"insured_signature": {
                            required: !0
                        },
                    },
                    messages: {
                        "last_name": {
                            required: "Please enter Patient's Last Name",
                            minlength: "Patient's Last Name must consist of at least 3 characters"
                        },
                        "first_name": {
                            required: "Please enter Patient's First Name",
                            minlength: "Patient's First Name must consist of at least 3 characters"
                        },
                        "address1": {
                            required: "Please enter Address1",
                            minlength: "Address1 must consist of at least 5 characters",
                            maxlength: "Address1 must be below 29 characters"
                        },
                        "city": {
                            required: "Please enter City",
                            minlength: "City must consist of at least 3 characters"
                        },
                        "zip_code": {
                            required: "Please enter Zip Code"
                        },
                        "account_number": {
                            required: "Please enter Account Number",
                            maxlength: "Account Number must be 14 digits"
                        },
						 "patient_signature": {
                            required: "Please upload Patient's Signature"
                        },
						 "insured_signature": {
                            required: "Please enter Insured's Signature"
                        },
                         "patient_dob": "Please enter Date of Birth",
                         "state": "Please select State",
                    },
                
                    ignore: [],
                    errorClass: "invalid-feedback animated fadeInUp",
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                    },
                    invalidHandler: function(e, validator) {
                    var errors = validator.numberOfInvalids();
                    step1_errors=errors;
                        if(step1_errors>0){
                            $('#steps-uid-0-t-0').closest('li').addClass('custom_error');
                            localStorage.setItem('step1_errors', step1_errors);
                        }else{
                           
                        }
                        
                    },
                    unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                    
                    }               
                });
                if (form.valid()) {
                    $('#steps-uid-0-t-0').closest('li').removeClass('custom_error');
                    
                }else{
                    
                }
            }
            else if(currentIndex==1){
                var step2_errors=0;
                form.validate({
                    onkeyup: true, 
                    onclick: true,
                    rules: {
                        "insured_last_name": {
                            required: !0,
                            minlength: 3
                        },
                        "insured_first_name": {
                            required: !0,
                            minlength: 3
                        },
                        "insured_address1": {
                            required: !0,
                            minlength: 5,
                            maxlength:28
                        },
                        "insured_city": {
                            required: !0,
                            minlength: 3
                        },
                        "insured_state": {
                            required: !0
                        },
                        "insured_zip_code": {
                            required: !0,
                            zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                        },
                        "other_insured_last_name": {
                            required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                            minlength: 3
                        },
                        "other_insured_first_name": {
                            required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                            minlength: 3
                        },
                        "insured_group_number": {
                            required: !0,
                            minlength: 5,
                            maxlength:28
                        },
                        "other_group_number": {
                            required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                            minlength: 5,
                            maxlength:28
                        },
                        "insurance_plan_name": {
                            required: !0,
                            minlength: 5,
                            maxlength:29
                        },
                        "other_insurance_plan_name": {
                            required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                            minlength: 5,
                            maxlength:29
                        },
                        "insured_dob": {
                            required: !0
                        },
                        "claim_id": {
                            minlength: 3,
                            maxlength:10
                        },
                    },
                    messages: {
                        "insured_last_name": {
                            required: "Please enter Last Name",
                            minlength: "Last Name must consist of at least 3 characters"
                        },
                        "insured_first_name": {
                            required: "Please enter First Name",
                            minlength: "First Name must consist of at least 3 characters"
                        },
                        "insured_address1": {
                            required: "Please enter Address1",
                            minlength: "Address1 must consist of at least 5 characters",
                            maxlength: "Address1 must be below 29 characters"
                        },
                        "insured_city": {
                            required: "Please enter City",
                            minlength: "City must consist of at least 3 characters"
                        },
                        "insured_zip_code": {
                            required: "Please enter Zip Code"
                        },
                        "other_insured_last_name": {
                            required: "Please enter Last Name",
                            minlength: "Last Name must consist of at least 3 characters"
                        },
                        "other_insured_first_name": {
                            required: "Please enter First Name",
                            minlength: "First Name must consist of at least 3 characters"
                        },
                        "insured_group_number": {
                            required: "Please enter Group Number",
                            minlength: "Group Number must consist of at least 5 characters",
                            maxlength: "Group Number must be below 28 characters"
                        },
                        "other_group_number": {
                            required: "Please enter Group Number",
                            minlength: "Group Number must consist of at least 5 characters",
                            maxlength: "Group Number must be below 28 characters"
                        },
                        "insurance_plan_name": {
                            required: "Please enter Plan Name",
                            minlength: "Plan Name must consist of at least 5 characters",
                            maxlength: "Plan Name must be below 28 characters"
                        },
                        "other_insurance_plan_name": {
                            required: "Please enter Plan Name",
                            minlength: "Plan Name must consist of at least 5 characters",
                            maxlength: "Plan Name must be below 28 characters"
                        },
                        "claim_id": {
                            minlength: "Claim_id must consist of at least 3 characters",
                            maxlength: "Claim_id must be 10 characters"
                        },
                        "insured_state": "Please select State",
                        "insured_dob": "Please select Date of Birth",
                    },
                
                    ignore: [],
                    errorClass: "invalid-feedback animated fadeInUp",
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                    },
                    invalidHandler: function(e, validator) {
                        var errors = validator.numberOfInvalids();
                        step2_errors=errors;
                        if(step2_errors>0){
                            $('#steps-uid-0-t-1').closest('li').addClass('custom_error');
                            localStorage.setItem('step2_errors', step2_errors);
                        }else{

                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                    }
                
                });
                if(form.valid()) {
                    $('#steps-uid-0-t-1').closest('li').removeClass('custom_error');
                    
                }else{
                    
                }
            }
            else if(currentIndex==2){
                var step3_errors=0;
                form.validate({
                    onkeyup: true, 
                    onclick: true,
                    rules: {
                        "qualifier": {
                            required: !0,
                            minlength: 2,
                            maxlength:2
                        },
                        "reffering_last_name": {
                            required: !0,
                            minlength: 3,
                            maxlength:26
                        },
                        "reffering_first_name": {
                            required: !0,
                            minlength: 3,
                            maxlength:26
                        },
                        "federal_tax_number": {
                            required: !0,
                            minlength: 3,
                            maxlength:15
                        },
                        "npi": {
                            required: !0,
                            minlength: 10,
                            maxlength:10
                        },
                        "total_amount": {
                            required: !0,
                            price_regex: /^\d{0,4}(\.\d{0,2})?$/
                        },
                        "paid_amount": {
                            required: !0,
                            price_regex: /^\d{0,4}(\.\d{0,2})?$/
                        },
                        "admin_location_name": {
                            required: !0,
                            minlength: 3,
                            maxlength:20
                        },
                        "admin_phone_number": {
                            required: !0,
                            minlength: 14, 
                            maxlength:20
                        },
                        "admin_address1": {
                            required: !0,
                            minlength: 5,
                            maxlength:28
                        },
                        "admin_city": {
                            required: !0,
                            minlength: 3
                        },
                        "admin_state": {
                            required: !0
                        },
                        "admin_zip_code": {
                            required: !0,
                            zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                        },
                        "other_id2": {
                            required: !0,
                            minlength: 26,
                            maxlength:26
                        },
                        "npi2": {
                            required: !0,
                            minlength: 26,
                            maxlength:26
                        },
                    },
                    messages: {
                        "qualifier": {
                            required: "Please enter Qualifier",
                            minlength: "Qualifier must be 2 characters",
                            maxlength: "Qualifier must be 2 characters"
                        },
                        "reffering_last_name": {
                            required: "Please enter Last Name",
                            minlength: "Last Name  must consist of at least 3 characters",
                            maxlength: "Last Name should not be above 26 characters"
                        },
                        "reffering_first_name": {
                            required: "Please enter First Name",
                            minlength: "First Name must consist of at least 3 characters",
                            maxlength: "First Name should not be above 26 characters"
                        },
                        "federal_tax_number": {
                            required: "Please enter Federal Tax I.D. Number",
                            minlength: "Federal Tax I.D. Number must consist of at least 3 characters",
                            maxlength: "Federal Tax I.D. Number should not be above 26 characters"
                        },
                        "npi": {
                            required: "Please enter NPI",
                            minlength: "NPI must consist of 15 characters",
                            maxlength: "NPI must consist of 15 characters"
                        },
                        "total_amount": {
                            required: "Please enter Total Amount",
                            price_regex: /^\d{0,4}(\.\d{0,2})?$/
                        },
                        "paid_amount": {
                            required: "Please enter Paid Amount",
                            price_regex: /^\d{0,4}(\.\d{0,2})?$/
                        },
                        "admin_location_name": {
                            required: "Please enter Location Name",
                            minlength: "Location Name must consist of at least 3 characters",
                            maxlength: "Location Name should not be above 20 characters"
                        },
                        "admin_phone_number": {
                            required: "Please enter Phone Number",
                            minlength: "Enter a valid Phone Number",
                            maxlength: "Enter a valid Phone Number"
                        },
                        "admininsured_address1": {
                            required: "Please enter Address1",
                            minlength: "Address1 must consist of at least 5 characters",
                            maxlength: "Address1 must be below 29 characters"
                        },
                        "admin_city": {
                            required: "Please enter City",
                            minlength: "City must consist of at least 3 characters"
                        },
                        "admin_zip_code": {
                            required: "Please enter Zip Code",
                        },
                        "npi2": {
                            required: "Please enter NPI",
                            minlength: "NPI must consist of 15 characters",
                            maxlength: "NPI must consist of 15 characters"
                        },
                        "other_id2": {
                            required: "Please enter Other ID",
                            minlength: "Other ID must consist of 17 characters",
                            maxlength: "Other ID must consist of 17 characters"
                        },
                        "admin_state": "Please select a state",
                    },
                
                    ignore: [],
                    errorClass: "invalid-feedback animated fadeInUp",
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                    },
                    invalidHandler: function(e, validator) {
                        var errors = validator.numberOfInvalids();
                        step3_errors=errors;
                        if(step3_errors>0){
                            $('#steps-uid-0-t-2').closest('li').addClass('custom_error');
                            localStorage.setItem('step3_errors', step3_errors);
                        }else{
                            
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                    }
                
                });
                if (form.valid()) {
                    $('#steps-uid-0-t-2').closest('li').removeClass('custom_error');
                   
                }else{
                }
            }
			
            var numItems = $('#steps-uid-0-p-'+currentIndex).find( '.is-invalid' ).length;

            if(numItems>0){
                $('#steps-uid-0-t-'+currentIndex).closest('li').addClass('custom_error');
            }else{
                $('#steps-uid-0-t-'+currentIndex).closest('li').removeClass('custom_error');
            }
            return true;
        },
		onStepChanged: function () {

		},
		onFinished: function () {
            var validator = $( "#step-form-horizontal"  ).validate();
            validator.destroy();
            var step4_errors=0;
            form.validate({
                onkeyup: true, 
                onclick: true,
                rules: {
                    "client_id": {
                        required: !0
                    },
                    "insurance_company_id": {
                        required: !0
                    },
                    "last_name": {
                        required: !0,
                        minlength: 3
                    },
                    "first_name": {
                        required: !0,
                        minlength: 3
                    },
                    "address1": {
                        required: !0,
                        minlength: 5,
                        maxlength:28
                    },
                    "city": {
                        required: !0,
                        minlength: 3
                    },
                    "state": {
                        required: !0
                    },
                    "zip_code": {
                        required: !0,
                        zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                    },
                    "account_number": {
                        required: !0,
                        maxlength:14                           
                    },
                    "patient_dob": {
                        required: !0
                    },
                    "insured_last_name": {
                        required: !0,
                        minlength: 3
                    },
                    "insured_first_name": {
                        required: !0,
                        minlength: 3
                    },
                    "insured_id_number": {
                        required: !0,
                        maxlength:29
                    },
                    "claim_id": {
                        minlength: 3,
                        maxlength:29
                    },
                    "insured_address1": {
                        required: !0,
                        minlength: 5,
                        maxlength:28
                    },
                    "insured_city": {
                        required: !0,
                        minlength: 3
                    },
                    "insured_state": {
                        required: !0
                    },
                    "insured_zip_code": {
                        required: !0,
                        zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                    },
                    "other_insured_last_name": {
                        required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                        minlength: 3
                    },
                    "other_insured_first_name": {
                        required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                        minlength: 3
                    },
                    "insured_group_number": {
                        required: !0,
                        minlength: 5,
                        maxlength:28
                    },
                    "other_group_number": {
                        required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                        minlength: 1,
                        maxlength:28
                    },
                    "insurance_plan_name": {
                        required: !0,
                        maxlength:29
                    },
                    "other_insurance_plan_name": {
                        required: $("input[name*='other_benefit_plan']:checked").val()=='yes',
                        minlength: 1,
                        maxlength:29
                    },
                    "insured_dob": {
                        required: !0
                    },
                    "qualifier": {
                        required: !0,
                        minlength: 2,
                        maxlength:2
                    },
                    "reffering_last_name": {
                        required: !0,
                        minlength: 3,
                        maxlength:26
                    },
                    "reffering_first_name": {
                        required: !0,
                        minlength: 3,
                        maxlength:26
                    },
                    "federal_tax_number": {
                        required: !0,
                        minlength: 9,
                        maxlength:15
                    },
                    "other_id": {
                        minlength: 12,
                        maxlength:19
                    },
                    "npi": {
                        required: !0,
                        minlength: 10,
                        maxlength:10
                    },
                    "total_amount": {
                        required: !0,
                        price_regex: /^\d{0,4}(\.\d{0,2})?$/
                    },
                    "paid_amount": {
                        required: !0,
                        price_regex: /^\d{0,4}(\.\d{0,2})?$/,
                        lessThanEquals: "#total_amount"
                    },
                    "admin_location_name": {
                        required: !0,
                        minlength: 3,
                        maxlength:20
                    },
                    "admin_phone_number": {
                        required: !0,
                        minlength: 14, 
                        maxlength:20
                    },
                    "admin_address1": {
                        required: !0,
                        minlength: 5,
                        maxlength:28
                    },
                    "admin_city": {
                        required: !0,
                        minlength: 3
                    },
                    "admin_state": {
                        required: !0
                    },
                    "admin_zip_code": {
                        required: !0,
                        zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                    },
                    "admin_other_id": {
                        required: !0,
                        minlength: 13,
                        maxlength:17
                    },
                    "admin_npi": {
                        required: !0,
                        minlength: 10,
                        maxlength:10
                    },
                    "other_id2": {
                        required: !0,
                        minlength: 26,
                        maxlength:26
                    },
                    "npi2": {
                        required: !0,
                        minlength: 26,
                        maxlength:26
                    },
                    "injury_date": {
                        required: !0
                    },
                    "iqual": {
                        required: !0
                    },
                    "icd": {
                        required: !0
                    }
                },
                messages: {
                    "last_name": {
                        required: "Please enter Patient's Last Name",
                        minlength: "Patient's Last Name must consist of at least 3 characters"
                    },
                    "first_name": {
                        required: "Please enter Patient's First Name",
                        minlength: "Patient's First Name must consist of at least 3 characters"
                    },
                    "address1": {
                        required: "Please enter Address1",
                        minlength: "Address1 must consist of at least 5 characters",
                        maxlength: "Address1 must be below 29 characters"
                    },
                    "city": {
                        required: "Please enter City",
                        minlength: "City must consist of at least 3 characters"
                    },
                    "zip_code": {
                        required: "Please enter Zip Code",
                        zip_regex: "Enter a valid Zip Code"
                    },
                    "account_number": {
                        required: "Please enter Account Number",
                        maxlength: "Account Number should not be more than 14 characters"
                    },
                     "patient_dob": "Please enter Date of Birth",
                     "state": "Please select State",
                     "qualifier": {
                        required: "Please enter Qualifier",
                        minlength: "Qualifier must be 2 characters",
                        maxlength: "Qualifier must be 2 characters"
                    },
                    "reffering_last_name": {
                        required: "Please enter Last Name",
                        minlength: "Last Name must consist of at least 3 characters",
                        maxlength: "Last Name should not be above 26 characters"
                    },
                    "reffering_first_name": {
                        required: "Please enter First Name",
                        minlength: "First Name must consist of at least 3 characters",
                        maxlength: "First Name should not be above 26 characters"
                    },
                    "federal_tax_number": {
                        required: "Please enter Federal Tax I.D. Number",
                        minlength: "Federal Tax I.D. Number must consist of at least 9 characters",
                        maxlength: "Federal Tax I.D. Number should not be above 26 characters"
                    },
                    "other_id": {
                        minlength: "Other ID must consist of 12 characters",
                        maxlength: "Other ID should  be less than 20 characters"
                    },
                    "npi": {
                        required: "Please enter NPI",
                        minlength: "NPI must consist of 10 characters",
                        maxlength: "NPI must consist of 10 characters"
                    },
                    "total_amount": {
                        required: "Please enter Total Amount",
                        price_regex: /^\d{0,4}(\.\d{0,2})?$/
                    },
                    "paid_amount": {
                        required: "Please enter Paid Amount",
                        price_regex: /^\d{0,4}(\.\d{0,2})?$/,
                        lessThanEquals:"Paid amount should be less than Total amount"
                    },
                    "admin_location_name": {
                        required: "Please enter a Location Name",
                        minlength: "Location Name must consist of at least 3 characters",
                        maxlength: "Location Name should not be above 20 characters"
                    },
                    "admin_phone_number": {
                        required: "Please enter Phone Number",
                        minlength: "Enter a valid Phone Number",
                        maxlength: "Enter a valid Phone Number"
                    },
                    "admin_address1": {
                        required: "Please enter Address1",
                        minlength: "Address1 must consist of at least 5 characters",
                        maxlength: "Address1 must be below 29 characters"
                    },
                    "admin_city": {
                        required: "Please enter City",
                        minlength: "City must consist of at least 3 characters"
                    },
                    "admin_zip_code": {
                        required: "Please enter Zip Code",
                    },
                    "admin_npi": {
                        required: "Please enter NPI",
                        minlength: "NPI must consist of 10 characters",
                        maxlength: "NPI must consist of 10 characters"
                    },
                    "admin_other_id": {
                        required: "Please enter Other ID",
                        minlength: "Other ID must consist of 13 characters",
                        maxlength: "Other ID must consist of 17 characters"
                    },
                    "insured_last_name": {
                        required: "Please enter Last Name",
                        minlength: "Last Name must consist of at least 3 characters"
                    },
                    "insured_first_name": {
                        required: "Please enter First Name",
                        minlength: "First Name must consist of at least 3 characters"
                    },
                    "insured_id_number": {
                        required: "Please enter Insured's ID Number",
                        maxlength: "Insured's ID Number must be below 30 characters"
                    },
                    "claim_id": {
                        minlength: "Claim ID must consist of at least 3 characters",
                        maxlength: "Claim ID should not be more than 30 characters"
                    },
                    "insured_address1": {
                        required: "Please enter Address1",
                        minlength: "Address1 must consist of at least 5 characters",
                        maxlength: "Address1 must be below 29 characters"
                    },
                    "insured_city": {
                        required: "Please enter City",
                        minlength: "City must consist of at least 3 characters"
                    },
                    "insured_zip_code": {
                        required: "Please enter Zip Code",
                        zip_regex: "Enter a valid Zip Code"
                    },
                    "other_insured_last_name": {
                        required: "Please enter a Last Name",
                        minlength: "Last Name must consist of at least 3 characters"
                    },
                    "other_insured_first_name": {
                        required: "Please enter a First Name",
                        minlength: "First Name must consist of at least 3 characters"
                    },
                    "insured_group_number": {
                        required: "Please enter Group Number",
                        minlength: "Group Number must consist of at least 5 characters",
                        maxlength: "Group Number must be below 28 characters"
                    },
                    "other_group_number": {
                        required: "Please enter Group Number",
                        minlength: "Group Number must consist of at least 1 character",
                        maxlength: "Group Number must be below 28 characters"
                    },
                    "insurance_plan_name": {
                        required: "Please enter Plan Name",
                        maxlength: "Plan Name must be below 30 characters"
                    },
                    "other_insurance_plan_name": {
                        required: "Please enter Plan Name",
                        minlength: "Plan Name must consist of at least 1 character",
                        maxlength: "Plan Name must be below 28 characters"
                    },
                    "insured_state": "Please select State",
                    "client_id": "Please select client",
                    "insurance_company_id": "Please select Insurance Company",
                    "insured_dob": "Please select Date of Birth",
                    "npi2": {
                        required: "Please enter NPI",
                        minlength: "NPI must consist of 15 characters",
                        maxlength: "NPI must consist of 15 characters"
                    },
                    "other_id2": {
                        required: "Please enter Other ID",
                        minlength: "Other ID must consist of 17 characters",
                        maxlength: "Other ID must consist of 17 characters"
                    },
                    "admin_state": "Please select State",
                    "injury_date": {
                        required: "Please select Date of Illness"
                    },
                    "iqual": {
                        required: "Please enter QUAL"
                    },
                    "icd": {
                        required: "Please enter value"
                    }
                },
            
                ignore: [],
                errorClass: "invalid-feedback animated fadeInUp",
                errorElement: 'span',
                errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
                },
                invalidHandler: function(e, validator) {
                    
                },
                unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
                }
            
            });
            
            if(form.valid()){

                if( $('.qwert').length>=0){
                    preview();
                    $('.submit_claim').css("display","flex"); 
                   // form.submit();
                }else{                   
                    alert('Please add atleast one claim value');
                }
                
            }else{
                $('.submit_claim').css("display","none"); 
                for(var i=0;i<4;i++){
                    var numItems = $('#steps-uid-0-p-'+i).find( '.invalid-feedback' ).length
                    if(numItems>0){
                        $('#steps-uid-0-t-'+i).closest('li').addClass('custom_error');
                    }else{
                        $('#steps-uid-0-t-'+i).closest('li').removeClass('custom_error');
                    }
                }
                
            }
        }
    });
    
})(jQuery);