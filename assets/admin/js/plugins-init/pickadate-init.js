(function($) {
    "use strict"

    //date picker classic default
    $('.datepicker-default').pickadate({
			format: "mm/dd/yyyy",
			language: "en",
			autoclose: true,
			viewMode: 'years',
			todayHighlight: true,
			 disable: [
			{ from: -100000000, to: true }
		  ]
		});
    $('.datepicker-default1').pickadate({
			format: "mm/dd/yyyy",
			language: "en",
			autoclose: true,
			viewMode: 'years',
			todayHighlight: true,
			 disable: [
			{ from: -100000000, to: true }
		  ]
		});

})(jQuery);