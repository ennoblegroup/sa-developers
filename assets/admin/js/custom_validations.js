//-------------------------------------------Common Form Validations------------------------------------//
$(".us_zip").on('input', function() {
    var zip_value= $(this).val();
    var inputValue = getInputValuez(zip_value); //get value from input and make it usefull number
    var length = inputValue.length; //get lenth of input
    
    if (length == 0)
    {
        $(this).val('');
        return false;
    }    
    if (inputValue < 100000)
    {
        inputValue =inputValue;
    }
    else if (inputValue < 100000000) 
    {
        inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
    }else
    {
        inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
    }       
    $(this).val(inputValue);
    inputValue = getInputValuez(zip_value);
});
    
function getInputValuez(zip_value) {
    var inputValue = zip_value.replace(/\D/g,'');  //remove all non numeric character
    return inputValue;
}
$(".us_phone").on('input', function() {
    var phone_value=$(this).val();
    var inputValue = getInputValue_phone(phone_value); //get value from input and make it usefull number
    var length = inputValue.length; //get lenth of input
    
    if (length == 0)
    {
        $(this).val('');
        return false;
    }    
    if (inputValue < 1000)
    {
        inputValue = '+1' + ' ' + '('+inputValue;
    }else if (inputValue < 1000000) 
    {
        inputValue = '+1' + ' ' +  '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
    }else if (inputValue < 10000000000) 
    {
        inputValue ='+1' + ' ' +  '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, length);
    }else
    {
        inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, 10);
    }       
    $(this).val(inputValue); 
    inputValue = getInputValue_phone(phone_value);
});

function getInputValue_phone(phone_value) {
    var inputValue = phone_value.replace(/\D/g,'');  //remove all non numeric character
    if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
    {
        var inputValue = inputValue.substring(1, inputValue.length);
    }
    return inputValue;
}
/*$(".modifier").on('input', function() {
    var modifier_value= $(this).val();
    var length = modifier_value.length; //get lenth of input
    
    if (length == 0)
    {
        $(this).val('');
        return false;
    }    
    if (length ==2)
    {
        modifier_value = modifier_value.substring(0, 2) + '-' + modifier_value.substring(2, 8);
    }
    else if (length ==5) 
    {
        modifier_value =  modifier_value.substring(0, 2) + '-' +modifier_value.substring(3, 5) + '-' + modifier_value.substring(7, 9);
    }
    else if (length ==9) 
    {
        modifier_value = modifier_value.substring(0, 2) + '-' +modifier_value.substring(3, 5) + '-' + modifier_value.substring(7, 9)+ '-' + modifier_value.substring(11, 12);
    }else  if (length >11)
    {
        modifier_value = modifier_value.substring(0, 2) + '-' +modifier_value.substring(3, 5) + '-' + modifier_value.substring(7, 9)+ '-' + modifier_value.substring(11, 12);
    }       
    $(this).val(modifier_value);
});*/
//-------------------------------------------Common Form Validations------------------------------------// 
 $(document).ready(function(){  
	  $('#create_excel').click(function(){ 
           var excel_data = $('.claim_table').html();  
           var page = "claims/export?data=" + excel_data;  
           window.location = page;  
      });  
 });  

//-------------------------------------------Claim Form Appending Code------------------------------------//
$("#doc1").click(function() {
    $('<div class="form-group doc1_div qwert1 col-lg-3 col-md-3">'+
        '<input type="text" class="form-control" name="diagnosis[]"  >'+
        '<a href="javascript:void(0)" class="doc1_close"><i style="position: absolute;right: 8%;bottom: 28%;" class="fa fa-times" aria-hidden="true"></i></a>'+
    '</div>').insertBefore('#diagnosisdiv');
});
$("#edit_doc1").click(function() {
    $('<div class="form-group doc1_div qwert1 col-lg-3 col-md-3">'+
        '<input type="text" class="form-control" name="diagnosis[]"  >'+
        '<input type="text" class="form-control" name="diagnosis_id[]" value="" >'+
        '<a href="javascript:void(0)" class="doc1_close"><i style="position: absolute;right: 8%;bottom: 28%;" class="fa fa-times" aria-hidden="true"></i></a>'+
    '</div>').insertBefore('#diagnosisdiv');
});
$(document).on('click','.doc1_close',function(e){
    e.preventDefault();
    $(this).closest('.doc1_div').remove();
    return false;
});
$("#doc").click(function() {
    $('<div class="row doc_div qwert">'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control us_date" name="service_start_date[]" placeholder="Date" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control us_date" name="service_end_date[]" placeholder="Date" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="service_place[]" placeholder="Place of Service" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="emg[]" placeholder="EMG" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="cpt_hcps[]" placeholder="CPT/HCPCS" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="modifier[]" placeholder="Modifier" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="diagnosis_pointer[]" placeholder="Diagnosis Pointer" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="charges[]" placeholder="Charges" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="days[]" placeholder="Days or Units" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="epsdt[]" placeholder="EPSDT" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="id_qual[]" placeholder="ID QUAL" >'+
            '</div>'+
            '<div class="form-group col-lg-1 col-md-1">'+
                '<input type="text" class="form-control" name="provider_id[]" placeholder="Rendering Provider Id.#NPI" >'+
                '<a href="javascript:void(0)" class="doc_close"><i style="position: sticky;/* right: 50%; */bottom: 18.5%;float: right;right: 1%;" class="fa fa-times" aria-hidden="true"></i></a>'+
            '</div>'+
        '</div>').appendTo('#outerdiv');
    });
    $(document).on('click','.doc_close',function(e){
        e.preventDefault();
        $(this).closest('.doc_div').remove();
        return false;
    });
//-------------------------------------------Claim Form Appending Code------------------------------------//
$(document).on('click','.submit_claim',function(e){
    $("#claim_type").val(1);
    var form = $("#step-form-horizontal");
    form.submit();
});
$(document).on('click','.save_claim',function(e){
    $.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to save and exit the form?',
        buttons: {
            confirm: {
               text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
                    var filled_data_count=0;		
                    $('input[type="text"]').each(function() {
                        if($(this).val() !=""){
                            filled_data_count= filled_data_count+1;
                        }
                    });
                    if(filled_data_count>0){
                        if($("#client_id").val() !='' && $("#insurance_company_id").val() !='' ){
                            $("#claim_type").val(0);
                            var form = $("#step-form-horizontal");
                            var validator = $( "#step-form-horizontal" ).validate();
                            validator.destroy();
                            form.submit();
                        }else{
                            alert('Please select client & Insurance Company');  
                        }
                    }else{
                        alert('No data to be saved');
                    }
    			}
            },
            cancel: {
                text: 'cancel',
                btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
});
//-------------------------------------------Claim status------------------------------------------------//
jQuery("#claim_status_update").validate({
    rules: {
        "status_name": {
            required: !0,
        },
        "status_description": {
            required: !0,
            minlength: 5
        }
    },
    messages: {
        "status_name": {
            required: "Please select Status",
        },
        "status_description": {
            required: "Please enter Status Description",
            minlength: "Status Description must consist of at least 5 characters"
        }
    },

    ignore: [],
    errorClass: "invalid-feedback animated fadeInUp",
    errorElement: "div",
    errorPlacement: function(e, a) {
        jQuery(a).parents(".form-group").append(e)
    },
    highlight: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
    },
    success: function(e) {
        jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-valid")
    }
});

//-------------------------------------------Claim status------------------------------------------------//
$(document).ready(function() {
    $('body').on('click', '.multiple_claim_generate', function () {    
        var myarray = [];
        $('input:checkbox[name=claim_checkbox]').each(function() 
        {    
            if($(this).is(':checked'))
            myarray.push($(this).val());
        });
        if(myarray.length>0){
            $("#preloader").css("display","block");
            $.ajax({
                type: "POST",
                url: "claims/multiple_report",
                data: {myarray: myarray},
                cache: false,
                success: function(data){
                $("#preloader").css("display","none");
                toastr.success("", "PDF's generated Successfully", {
                    timeOut: 5000,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    positionClass: "toast-top-right",
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
                }
            });
        }else{
            alert('Please select atleast one claim');
        }
    });
    $('body').on('click', '.eedit', function() {
        var vid = $(this).attr('vid');
        $('#mvid').val(vid);
        $('.rtrt').text('Update');
        var service_start_date=$('.rs'+vid+' .service_start_date').val();
        var validator = $( "#claim_add_value"  ).validate();
        validator.destroy();
    
        var service_start_date=$('.rs'+vid+' .service_start_date').val();
        var service_end_date=$('.rs'+vid+' .service_end_date').val();
        var service_place=$('.rs'+vid+' .service_place').val();
        var emg=$('.rs'+vid+' .emg').val();
        var cpt_hcps=$('.rs'+vid+' .cpt_hcps').val();
        var modifier=$('.rs'+vid+' .modifier').val();
        var diagnosis_pointer=$('.rs'+vid+' .diagnosis_pointer').val();
        var charges=$('.rs'+vid+' .charges').val();
        var days=$('.rs'+vid+' .days').val();
        var epsdt=$('.rs'+vid+' .epsdt').val();
        var fplan=$('.rs'+vid+' .fplan').val();
        var id_qual=$('.rs'+vid+' .id_qual').val();
        var provider_id=$('.rs'+vid+' .provider_id').val();
        var cnpi=$('.rs'+vid+' .cnpi').val();
        
        $('#service_start_date').val(service_start_date);
        $('#service_end_date').val(service_end_date);
        $('#service_place').val(service_place);
        $('#emg').val(emg);
        $('#cpt_hcps').val(cpt_hcps);
        $('#modifier').val(modifier);
        $('#diagnosis_pointer').val(diagnosis_pointer);
        $('#charges').val(charges);
        $('#epsdt').val(epsdt);
        $('#days').val(days);
        $('#fplan').val(fplan);
        $('#id_qual').val(id_qual);
        $('#provider_id').val(provider_id);
        $('#cnpi').val(cnpi);
    
        $('#exampleModalLong').modal('show');
    });
    $('body').on('click', '.eadd', function() {
        $('#mvid').val('');
        $('.rtrt').text('Add');
        $('#service_start_date').val('');
        $('#service_end_date').val('');
        $('#service_place').val('');
        $('#emg').val('');
        $('#cpt_hcps').val('');
        $('#modifier').val('');
        $('#diagnosis_pointer').val('');
        $('#charges').val('');
        $('#days').val('');
        $('#epsdt').val('');
        $('#fplan').val('');
        $('#id_qual').val('');
        $('#provider_id').val('');
        $('#cnpi').val('');
        $('#exampleModalLong').modal('show');
    });
    function getFormattedDate(date) {
        let year = date.getFullYear();
        let month = (1 + date.getMonth()).toString().padStart(2, '0');
        let day = date.getDate().toString().padStart(2, '0');
      
        return month + '/' + day + '/' + year;
    }
    $('body').on('click', '.rtrt', function () {
        $.validator.addMethod("price_regex", function(value, element, regexpr) {          
            return regexpr.test(value);
        }, "Please enter a valid amount.");
        var formm = $("#claim_add_value");
        var vid = $("#mvid").val();
        formm.validate({
            rules: {
                "service_start_date": {
                    required: !0
                },
                "service_end_date": {
                    required: !0
                },
                "service_place": {
                    minlength: 2,
                    maxlength:2
                },
                "emg": {
                    required: !0,
                    maxlength:2
                },
                "cpt_hcps": {
                    required: !0, 
                    maxlength:6
                },
                "modifier": {
                    minlength: 2,
                    maxlength:8
                },
                "diagnosis_pointer": {
                    required: !0,
                    minlength: 1,
                    maxlength:4
                },
                "charges": {
                    required: !0,
                    price_regex: /^\d{0,8}(\.\d{0,2})?$/
                },
                "days": {
                    required: !0,
                    minlength: 3,
                    maxlength:3
                },
                "epsdt": {
                    minlength: 2,
                    maxlength:2
                },
                "provider_id": {
                    required: !0,
                    minlength: 11,
                    maxlength:11
                },
                "cnpi": {
                    required: !0,
                    minlength: 10,
                    maxlength:10
                },
            },
            messages: {
                "service_start_date": {
                    required: "Please select Date"
                },
                "service_end_date": {
                    required: "Please select Date"
                },
                "service_place": {
                    required: "Please enter Service Place",
                    minlength: "Service Place must be 2 digits",
                    maxlength: "Service Place must be 2 digits"
                },
                "emg": {
                    required: "Please enter EMG",
                    maxlength: "EMG must be 2 characters"
                },
                "cpt_hcps": {
                    required: "Please enter  CPT/HCPS",
                    maxlength: "CPT/HCPS must be 6 characters"
                },
                "modifier": {
                    minlength: "Modifier should be atleast 2 character",
                    maxlength: "Address1 must be 8 characters"
                },
                "diagnosis_pointer": {
                    required: "Please enter Diagnosis Pointer",
                    minlength: "Diagnosis Pointer should be atleast 1 character",
                    maxlength: "Diagnosis Pointer must be 4 characters"
                },
                "charges": {
                    required: "Please enter Charges",
                },
                "days": {
                    required: "Please enter Days or Units",
                    minlength: "Days or Units must consist of 3 characters",
                    maxlength: "Days or Units must consist of 3 characters"
                },
                "epsdt": {
                    minlength: "EPSDT must consist of 2 characters",
                    maxlength: "EPSDT must consist of 2 characters"
                },
                "provider_id": {
                    required: "Please enter Rendering Provider Id",
                    minlength: "Rendering Provider Id must consist of 11 characters",
                    maxlength: "Rendering Provider Id must consist of 11 characters"
                },
                "cnpi": {
                    required: "Please enter NPI",
                    minlength: "NPI must consist of 10 characters",
                    maxlength: "NPI must consist of 10 characters"
                },
            },
    
            ignore: [],
            errorClass: "invalid-feedback animated fadeInUp",
            errorElement: "div",
            errorPlacement: function(e, a) {
                jQuery(a).parents(".form-group").append(e)
            },
            highlight: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
            },
            success: function(e) {
                jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-valid")
            }
        });
        if (formm.valid()) {
            var service_start_date=$('#service_start_date').val();
            var service_end_date=$('#service_end_date').val();
            var service_place=$('#service_place').val();
            var emg=$('#emg').val();
            var cpt_hcps=$('#cpt_hcps').val();
            var modifier=$('#modifier').val();
            var diagnosis_pointer=$('#diagnosis_pointer').val();
            var charges=$('#charges').val();
            var days=$('#days').val();
            var epsdt=$('#epsdt').val();
            var fplan=$('#fplan').val();
            var id_qual=$('#id_qual').val();
            var provider_id=$('#provider_id').val();
            var cnpi=$('#cnpi').val();
            
            var myVals = [];
            $('.qwert').map(function(){
                myVals.push($(this).attr('tpid'));
                console.log(Math.max(myVals));
            });
            var rid= Math.max(myVals)+1;
            if(vid==''){
            $('<tr class="doc_div qwert rs'+rid+'" tpid="'+rid+'">'+
            '<td  class="td_service_start_date">'+service_start_date+'</td>'+
            '<td  class="td_service_end_date">'+service_end_date+'</td>'+
            '<td  class="td_service_place">'+service_place+'</td>'+
            '<td  class="td_emg">'+emg+'</td>'+
            '<td  class="td_cpt_hcps">'+cpt_hcps+'</td>'+
            '<td  class="td_modifier">'+modifier+'</td>'+
            '<td  class="td_diagnosis_pointer">'+diagnosis_pointer+'</td>'+
            '<td  class="td_charges">'+charges+'</td>'+
            '<td  class="td_days">'+days+'</td>'+
            '<td  class="td_epsdt">'+epsdt+'</td>'+
            '<td  class="td_fplan">'+fplan+'</td>'+
            '<td  class="td_id_qual">'+id_qual+'</td>'+
            '<td  class="td_provider_id">'+provider_id+'</td>'+
            '<td  class="td_cnpi" clas="display:flex">'+cnpi+'</td>'+
            '<td>'+
                '<input type="hidden" class="form-control us_date service_start_date" value="'+service_start_date+'" name="service_start_date[]" placeholder="Date" >'+
                '<input type="hidden" class="form-control us_date service_end_date" value="'+service_end_date+'" name="service_end_date[]" placeholder="Date" >'+
                '<input type="hidden" class="form-control service_place" value="'+service_place+'" name="service_place[]" placeholder="Place of Service" >'+
                '<input type="hidden" class="form-control emg" value="'+emg+'" name="emg[]" placeholder="EMG" >'+
                '<input type="hidden" class="form-control cpt_hcps" value="'+cpt_hcps+'" name="cpt_hcps[]" placeholder="CPT/HCPCS" >'+
                '<input type="hidden" class="form-control modifier" value="'+modifier+'" name="modifier[]" placeholder="Modifier" >'+
                '<input type="hidden" class="form-control diagnosis_pointer" value="'+diagnosis_pointer+'" name="diagnosis_pointer[]" placeholder="Diagnosis Pointer" >'+
                '<input type="hidden" class="form-control charges" value="'+charges+'" name="charges[]" placeholder="Charges" >'+
                '<input type="hidden" class="form-control days" value="'+days+'" name="days[]" placeholder="Days or Units" >'+
                '<input type="hidden" class="form-control epsdt" value="'+epsdt+'" name="epsdt[]" placeholder="EPSDT" >'+
                '<input type="hidden" class="form-control fplan" value="'+fplan+'" name="fplan[]" placeholder="Family Plan" >'+
                '<input type="hidden" class="form-control id_qual" value="'+id_qual+'" name="id_qual[]" placeholder="ID QUAL" >'+
                '<input type="hidden" class="form-control" name="value_id[]" value="">'+
                '<input type="hidden" class="form-control provider_id" value="'+provider_id+'" name="provider_id[]" style="width:50%" placeholder="Rendering Provider Id" >'+
                '<input type="hidden" class="form-control cnpi" value="'+cnpi+'" name="cnpi[]" placeholder="NPI" style="width:50%">'+
                '<a href="#" class="eedit" vid = "'+rid+'" > <button type="button" class="btn btn-sm btn-outline-dark"><i class="fa fa-pencil"></i></button> </a>'+
                '<a href="#" class="doc_close" vid = "'+rid+'"><button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></button></a>'+
            '</td>'+
            '</tr>').insertAfter('#outerdiv');
            }else{
                $('.rs'+vid+' .td_service_start_date').text(service_start_date);
                $('.rs'+vid+' .td_service_end_date').text(service_end_date);
                $('.rs'+vid+' .td_service_place').text(service_place);
                $('.rs'+vid+' .td_cpt_hcps').text(cpt_hcps);
                $('.rs'+vid+' .td_modifier').text(modifier);
                $('.rs'+vid+' .td_diagnosis_pointer').text(diagnosis_pointer);
                $('.rs'+vid+' .td_charges').text(charges);
                $('.rs'+vid+' .td_days').text(days);
                $('.rs'+vid+' .td_epsdt').text(epsdt);
                $('.rs'+vid+' .td_fplan').text(fplan);
                $('.rs'+vid+' .td_id_qual').text(id_qual);
                $('.rs'+vid+' .td_provider_id').text(provider_id);
                $('.rs'+vid+' .td_cnpi').text(cnpi);

                 $('.rs'+vid+' .service_start_date').val(service_start_date);
                 $('.rs'+vid+' .service_end_date').val(service_end_date);
                 $('.rs'+vid+' .service_place').val(service_place);
                 $('.rs'+vid+' .cpt_hcps').val(cpt_hcps);
                 $('.rs'+vid+' .modifier').val(modifier);
                 $('.rs'+vid+' .diagnosis_pointer').val(diagnosis_pointer);
                 $('.rs'+vid+' .charges').val(charges);
                 $('.rs'+vid+' .days').val(days);
                 $('.rs'+vid+' .epsdt').val(epsdt);
                 $('.rs'+vid+' .fplan').val(fplan);
                 $('.rs'+vid+' .id_qual').val(id_qual);
                 $('.rs'+vid+' .provider_id').val(provider_id);
                 $('.rs'+vid+' .cnpi').val(cnpi);
            }
            formm.trigger("reset");
            $('#exampleModalLong').modal('hide');
        }else{
            
        }
    });
});
$(document).ready(function(){
    filter_data();
    
});

$(document).ready(function(){
    $('input[type="text"]').each(function() {
		var placeholder= $(this).attr("placeholder");
		$(this).attr('title', placeholder);
	});
});

$(function() {

    $('.dateranges').daterangepicker({
        autoUpdateInput: false,
        format: 'MM/DD/YYYY',
        minDate: new Date(),
        locale: {
            cancelLabel: 'Clear'
        }
    });
  
    $('.dateranges').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('.dateranges').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
  
  });
  $(function() {

    $('.daterange').daterangepicker({
        autoUpdateInput: false,
        format: 'MM/DD/YYYY',
        maxDate: new Date(),
        locale: {
            cancelLabel: 'Clear'
        }
    });
  
    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
  
    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
  
  });
  $('body').on('click', '.reply_send', function() {
    var message = $("textarea[name='message']").val();
    var message_id = $("input[name='message_id']").val(); 
    var email = $("input[name='email_id']").val();
    var last_name = $("input[name='last_name']").val();
    var first_name = $("input[name='first_name']").val();
    var url = "../../messages/send_reply";
    var count = 0;
    if(count==0){
        $.ajax({
            url: url,
            type: 'post',
            data: {'email':email,'message':message,'message_id':message_id,'first_name':first_name,'last_name':last_name},
            success: function( data ){
                location.reload();
            }
        });
    }
});
$(document).ready(function () {
    $('#addForm').validate({
      rules: {
          file_name: {
          required: true
        },
        image: {
          required: true
        }
      },
      messages: {
          file_name: {
          required: "Please enter Template Name"
        },
        image: {
          required: "Please select Template"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
/*$(document).ready(function(){
    $(document).bind("contextmenu",function(e){
       return false;
    });
 });*/

 $("input[name='other_benefit_plan']").click(function(){
    if (this.value=='no') {
        $(".otii .invalid-feedback").css("display","none");
        $(".otii .is-invalid").removeClass('is-invalid');
    } else {
        
    }
});


 var validate = function(e) {
    var t = e.value;
    e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
  }
 function allowAlphaNumeric(e) {
    var code = ('charCode' in e) ? e.charCode : e.keyCode;
    if (!(code > 47 && code < 58) && // numeric (0-9)
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      e.preventDefault();
    }
}
function allowAlphaNumericSpace(e) {
    var code = ('charCode' in e) ? e.charCode : e.keyCode;
    if (!(code == 32) && // space
      !(code > 47 && code < 58) && // numeric (0-9)
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      e.preventDefault();
    }
}
function allowAlphabets(e) {
    var code = ('charCode' in e) ? e.charCode : e.keyCode;
    if (!(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      e.preventDefault();
    }
}
function allowNumeric(e) {
    var code = ('charCode' in e) ? e.charCode : e.keyCode;
    if (!(code > 47 && code < 58)) { // numeric (0-9)
      e.preventDefault();
    }
}

$(document).ready(function () {	
    $.validator.addMethod("password_pattern", function(value, element, regexpr) {          
        return regexpr.test(value);
    });
    $('#changePassword').validate({
        rules: {
          current: {
            required: true
          },
        Addpwd: {
            required: true,
            minlength: 8,
            password_pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
          },
        confirm: {
            required: true,
            equalTo: '[name="Addpwd"]'
          },    
        },
        messages: {
        current: {
            required: "Please enter current password"
          }, 
        Addpwd: {
            required: "Please enter New Password",
            minlength:"",
            password_pattern:""
          }, 
        confirm: {
            required: "Please enter Confirm Password",
            equalTo: "Password doesn't Match"
          },   
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });   
});
function ValidatePassword() {
    /*Array of rules and the information target*/
    var rules = [{
        Pattern: "[A-Z]",
        Target: "UpperCase"
      },
      {
        Pattern: "[a-z]",
        Target: "LowerCase"
      },
      {
        Pattern: "[0-9]",
        Target: "Numbers"
      },
      {
        Pattern: "[!@@#$%^&*]",
        Target: "Symbols"
      }
    ];
  
    //Just grab the password once
    var password = $(this).val();
  
    /*Length Check, add and remove class could be chained*/
    /*I've left them seperate here so you can see what is going on */
    /*Note the Ternary operators ? : to select the classes*/
    $("#Length").removeClass(password.length > 7 ? "glyphicon-remove" : "glyphicon-ok");
    $("#Length").addClass(password.length > 7 ? "glyphicon-ok" : "glyphicon-remove");
    
    /*Iterate our remaining rules. The logic is the same as for Length*/
    for (var i = 0; i < rules.length; i++) {
  
      $("#" + rules[i].Target).removeClass(new RegExp(rules[i].Pattern).test(password) ? "glyphicon-remove" : "glyphicon-ok"); 
      $("#" + rules[i].Target).addClass(new RegExp(rules[i].Pattern).test(password) ? "glyphicon-ok" : "glyphicon-remove");
    }
}
$(document).ready(function() {
  $("#Addpwd").on('keyup', ValidatePassword)
  
});
$('body').on('change', '.wwe', function () {  
   var claim_id= $(this).attr('claim_id');
   var s_class= $(this).attr('s_class');
   var ttt= $(this).attr('ttt');
   var submit_class= $(this).attr('submit_class');
   var edit_class= $(this).attr('edit_class');
   var status_class= $(this).attr('status_class');
   var admin_class= $(this).attr('admin_class');
   var generate_class= $(this).attr('generate_class');
   var chat_class= $(this).attr('chat_class');
   var status_id= $(this).attr('status_id');
   var transaction_status= $(this).attr('transaction_status');
   var payment_status= $(this).attr('payment_status');

   $( "#act_generate" ).addClass(admin_class);
   $( "#act_generate" ).addClass(generate_class);
   $( "#act_view_reports" ).addClass(admin_class);
   $( "#act_update_status" ).addClass(admin_class);
   $( "#act_update_status" ).addClass(status_class);
   $( "#act_edit" ).addClass(edit_class);
   $( "#act_chat" ).addClass(chat_class);
   $( "#act_submit" ).addClass(submit_class);
   $( "#act_submit" ).addClass(s_class);
});

$('body').on('click', '.cact a', function () {
   var action=  this.id;
   if($('input[name="claim_radio"]:checked').length > 0){
        var baseurl = window.location.origin+window.location.pathname;
        var id = $('input[name="claim_radio"]:checked').attr("claim_id");
        var status_id= $('input[name="claim_radio"]:checked').attr('status_id');
        var transaction_status= $('input[name="claim_radio"]:checked').attr('transaction_status');
        var payment_status= $('input[name="claim_radio"]:checked').attr('payment_status');

        switch (action) { 
            case 'act_generate': 
                window.location = baseurl+'/single_report/'+id;
                break;
            case 'act_view_reports': 
                window.location = baseurl+'/reports/'+id;
                break;
            case 'act_update_status': 
                openUpdateStatus(id,status_id,transaction_status,payment_status);
                break;		
            case 'act_status_history': 
                qwert(id);
                break;
            case 'act_edit': 
                window.location = baseurl+'/edit/'+id;
                break;
            case 'act_view': 
                window.location = baseurl+'/view_claim/'+id;
                break;
            case 'act_chat': 
                window.location = baseurl+'/conversation/'+id;
                break;
            case 'act_submit': 
                alert('act_submit Wins!');
                break;    
            default:
                alert('Error');
        }
     }else{
        alert('Select the claim from action');
     }
   
});
var baseurl = window.location.origin+'/'+window.location.pathname.split('/')[1];
$('body').on('click', '.claim_submission', function () {
    var claim_id=$('input[name="claim_radio"]:checked').attr("claim_id");
    var status_id=$('input[name="claim_radio"]:checked').attr("ttt");    
    claim_submission_extend(claim_id,status_id);
    location.reload();
});
$('body').on('click', '.claim_submissionp', function () {
    var claim_id=$(this).attr("claim_id");
    var status_id=$(this).attr("ttt");    
    claim_submission_extend(claim_id,status_id);
});
function claim_submission_extend(claim_id,status_id) {
    var status_name=2;
    var status_description="";
    var status_group=1;
    if(status_id>1){
        toastr.error("", "Claim is already Submitted ", {
            timeOut: 5000,
            closeButton: !0,
            debug: !1,
            newestOnTop: !0,
            progressBar: !0,
            positionClass: "toast-top-right",
            preventDuplicates: !0,
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
            tapToDismiss: !1
        }) 
    }
    else if(claim_id){
        $.ajax({
            type: "POST",
            url: baseurl+'/admin/claims/add_claim_status_history_pa',
            data: {'claim_id':claim_id,'status_name':status_name,'status_description':status_description,'status_group':status_group},
            cache: false,
            success: function(data){
            toastr.success("", "Submitted Successfully", {
                timeOut: 5000,
                closeButton: !0,
                debug: !1,
                newestOnTop: !0,
                progressBar: !0,
                positionClass: "toast-top-right",
                preventDuplicates: !0,
                onclick: null,
                showDuration: "300",
                hideDuration: "1000",
                extendedTimeOut: "1000",
                showEasing: "swing",
                hideEasing: "linear",
                showMethod: "fadeIn",
                hideMethod: "fadeOut",
                tapToDismiss: !1
            })
            
            }
        });
    }else{
        alert('Please select atleast one claim');
    }
}
$('body').on('click', '.claim_submission_ins', function () {    
    var claim_id=$('input[name="claim_radio"]:checked').attr("claim_id");
    var status_id=$('input[name="claim_radio"]:checked').attr("ttt");
    claim_submission_ins_extend(claim_id,status_id);   
});
$('body').on('click', '.claim_submission_insp', function () {    
    var claim_id=$(this).attr("claim_id");
    var status_id=$(this).attr("ttt");
    claim_submission_ins_extend(claim_id,status_id);   
});
function claim_submission_ins_extend(claim_id,status_id) {
    $.ajax({
        type: "POST",
        url: baseurl+'/admin/claims/claim_submssion',
        data: {'claim_id':claim_id},
        cache: false,
        success: function(data){
        toastr.success("", "Submitted Successfully", {
            timeOut: 5000,
            closeButton: !0,
            debug: !1,
            newestOnTop: !0,
            progressBar: !0,
            positionClass: "toast-top-right",
            preventDuplicates: !0,
            onclick: null,
            showDuration: "300",
            hideDuration: "1000",
            extendedTimeOut: "1000",
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut",
            tapToDismiss: !1
        })
        $('table').data.reload();
        }
    });
}
/*
$(document).ready(function() {
    var allClasses = [];
  
      var allElements = document.querySelectorAll('*');
  
      for (var i = 0; i < allElements.length; i++) {
      console.log(allElements[i].tagName);  
      var classes = allElements[i].className.toString().split(/\s+/);
      for (var j = 0; j < classes.length; j++) {
          var cls = classes[j];
          if (cls && allClasses.indexOf(cls) === -1)
          allClasses.push(cls);
      }
      }
  
      console.log(allClasses);
  });
*/
$(document).ready(function() {
    $('#examples').DataTable({
        "bRetrieve": true,
        "bProcessing": true,
        "bSort" : true,
        "bDestroy": false,
        "bPaginate": true,
        "bAutoWidth": true,
        "bFilter": true,
        "bInfo": true,
        "bJQueryUI": true
    });
    $('#example3').DataTable({
        "bRetrieve": true,
        "bProcessing": true,
		"bSort" : true,
        "bDestroy": false,
        "bPaginate": true,
        "bAutoWidth": true,
        "bFilter": true,
        "bInfo": true,
        "bJQueryUI": true
    });
    $('.claim_table').DataTable({
        "bRetrieve": true,
        "bProcessing": true,
        "bDestroy": false,
        "bSort" : true,
        "bPaginate": true,
        "bAutoWidth": true,
        "bFilter": true,
        "bInfo": true,
        "bJQueryUI": true
    });	
} );
//---------------------------------------------------------------------------------//
/*$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);

    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');

    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }

    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }

    var numItems = children.length;
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);

    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }

    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }

    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }

    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");

    children.hide();
    children.slice(0, perPage).show();

    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });

    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }

    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }

    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;

        children.css('display','none').slice(startAt, endOn).show();

        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }

        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }

        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");

    }
};
$(document).ready(function() {
    $('#filter_data').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:15}); 
});*/
//---------------------------------------------------------------------------------//

function preview(){
    $('.submit_claim').css("display","none"); 
    document.getElementById('plast_name').innerHTML = document.getElementById('last_name').value;
    document.getElementById('pmiddle_name').innerHTML = document.getElementById('middle_name').value;
    document.getElementById('pfirst_name').innerHTML = document.getElementById('first_name').value;
    document.getElementById('ppatient_dob').innerHTML = document.getElementById('patient_dob').value;
    document.getElementById('pphone_number').innerHTML = document.getElementById('phone_number').value;
    document.getElementById('psex').innerHTML = document.querySelector('input[name=gender]:checked').value;
    document.getElementById('paddress1').innerHTML = document.getElementById('address1').value;
    document.getElementById('paddress2').innerHTML = document.getElementById('address2').value;
    document.getElementById('pcity').innerHTML = document.getElementById('city').value;
    document.getElementById('pstate').innerHTML = document.getElementById('state').value;
    document.getElementById('pzip_code').innerHTML = document.getElementById('zip_code').value;
    document.getElementById('paccount_number').innerHTML = document.getElementById('account_number').value;
    document.getElementById('prelation').innerHTML = document.querySelector('input[name=relation]:checked').value;

    document.getElementById('pinsured_last_name').innerHTML = document.getElementById('insured_last_name').value;
    document.getElementById('pinsured_middle_name').innerHTML = document.getElementById('insured_middle_name').value;
    document.getElementById('pinsured_first_name').innerHTML = document.getElementById('insured_first_name').value;
    document.getElementById('pinsured_address1').innerHTML = document.getElementById('insured_address1').value;
    document.getElementById('pinsured_address2').innerHTML = document.getElementById('insured_address2').value;
    document.getElementById('pinsured_city').innerHTML = document.getElementById('insured_city').value;
    document.getElementById('pinsured_state').innerHTML = document.getElementById('insured_state').value;
    document.getElementById('pinsured_zip_code').innerHTML = document.getElementById('insured_zip_code').value;
    document.getElementById('pother_insured_last_name').innerHTML = document.getElementById('other_insured_last_name').value;
    document.getElementById('pother_insured_middle_name').innerHTML = document.getElementById('other_insured_middle_name').value;
    document.getElementById('pother_insured_first_name').innerHTML = document.getElementById('other_insured_first_name').value;
    document.getElementById('pinsured_group_number').innerHTML = document.getElementById('insured_group_number').value;
    document.getElementById('pother_group_number').innerHTML = document.getElementById('other_group_number').value;
    document.getElementById('pinsurance_plan_name').innerHTML = document.getElementById('insurance_plan_name').value;
    document.getElementById('pother_insurance_plan_name').innerHTML = document.getElementById('other_insurance_plan_name').value;
    document.getElementById('pinsured_dob').innerHTML = document.getElementById('insured_dob').value;
    document.getElementById('pinsured_sex').innerHTML = document.querySelector('input[name=insured_gender]:checked').value;
    document.getElementById('pclaim_id').innerHTML = document.getElementById('claim_id').value;
    document.getElementById('pother_benefit_plan').innerHTML = document.querySelector('input[name=other_benefit_plan]:checked').value;
    
    document.getElementById('preffering_name').innerHTML = document.getElementById('reffering_last_name').value+' '+document.getElementById('reffering_first_name').value+' '+document.getElementById('reffering_middle_name').value;
    document.getElementById('pfederal_tax_number').innerHTML = document.getElementById('federal_tax_number').value;
    document.getElementById('pother_id').innerHTML = document.getElementById('other_id').value;
    document.getElementById('pnpi').innerHTML = document.getElementById('npi').value;
    document.getElementById('ptotal_amount').innerHTML = document.getElementById('total_amount').value;
    document.getElementById('ppaid_amount').innerHTML = document.getElementById('paid_amount').value;
    document.getElementById('pservice_location_name').innerHTML = document.getElementById('service_location_name').value;
    document.getElementById('pservice_phone_number').innerHTML = document.getElementById('service_phone_number').value;
    document.getElementById('pservice_address1').innerHTML = document.getElementById('service_address1').value;
    document.getElementById('pservice_address2').innerHTML = document.getElementById('service_address2').value;
    document.getElementById('pservice_city').innerHTML = document.getElementById('service_city').value;
    document.getElementById('pservice_state').innerHTML = document.getElementById('service_state').value;
    document.getElementById('pservice_zip_code').innerHTML = document.getElementById('service_zip_code').value;
    document.getElementById('pservice_other_id').innerHTML = document.getElementById('service_other_id').value;
    document.getElementById('pservice_npi').innerHTML = document.getElementById('service_npi').value;
    document.getElementById('padmin_location_name').innerHTML = document.getElementById('admin_location_name').value;
    document.getElementById('padmin_phone_number').innerHTML = document.getElementById('admin_phone_number').value;
    document.getElementById('padmin_address1').innerHTML = document.getElementById('admin_address1').value;
    document.getElementById('padmin_address2').innerHTML = document.getElementById('admin_address2').value;
    document.getElementById('padmin_city').innerHTML = document.getElementById('admin_city').value;
    document.getElementById('padmin_state').innerHTML = document.getElementById('admin_state').value;
    document.getElementById('padmin_zip_code').innerHTML = document.getElementById('admin_zip_code').value;
    document.getElementById('padmin_other_id').innerHTML = document.getElementById('admin_other_id').value;
    document.getElementById('padmin_npi').innerHTML = document.getElementById('admin_npi').value;

    document.getElementById('pemployment').innerHTML = document.querySelector('input[name=employment]:checked').value;
    document.getElementById('pauto_accident').innerHTML = document.querySelector('input[name=auto_accident]:checked').value;
    document.getElementById('pother_accident').innerHTML = document.querySelector('input[name=other_accident]:checked').value;
    document.getElementById('paccident_location').innerHTML = document.getElementById('accident_location').value;
    document.getElementById('pclaim_code').innerHTML = document.getElementById('claim_code').value;
    document.getElementById('pinjury_date').innerHTML = document.getElementById('injury_date').value;
    document.getElementById('pqual_date').innerHTML = document.getElementById('qual_date').value;
    document.getElementById('pnonworking_dates').innerHTML = document.getElementById('nonworking_dates').value;
    document.getElementById('phospital_dates').innerHTML = document.getElementById('hospital_dates').value;
    document.getElementById('padditional_claim_information').innerHTML = document.getElementById('additional_claim_information').value;
    document.getElementById('pout_lab').innerHTML = document.querySelector('input[name=out_lab]:checked').value;
    document.getElementById('ptotal_charge').innerHTML = document.getElementById('total_charge').value;
    $('#confirmmodel').modal('show');
}


var input = $('<li aria-hidden="false" aria-disabledd="false"><a class="preview_form btn btn-outline-dark" onclick="preview()" role="menuitem">Preview</a></li><li aria-hidden="false" aria-disabledd="false"><a class="reset_claim_form btn btn-outline-dark" role="menuitem">Reset</a></li><li aria-hidden="false" aria-disabledd="false"><a class="save_claim btn btn-outline-dark" role="menuitem">Save & Exit</a></li>');
input.appendTo($('ul[aria-label=Pagination]'));