<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*------------------------Admin Login-------------------------------------------------*/
$config['adminlogin_wrong_usename/password'] = "Invalid Email Address or Password Entered";
$config['adminlogin_inactiveuser'] = "Your account is Inactive.";
$config['adminlogin_inactiverole'] = "Your role is Inactive.";
$config['forgotpassword_invalid_email'] = "Please enter registered Email Address";
$config['session_expired'] = "Session expired.Please login to continue";
$config['reset_password_email'] = "Please check your email to reset your password";
$config['reset_password_email_error'] = "It is not a valid registered E-Mail address.Please enter registered E-Mail address";
$config['account_activation_instructions'] = "Please check your email for the instructions to activate your account";
$config['link_expired'] = "Sorry! The link has expired";
$config['try_after_sometime'] = "Something went wrong please try after sometime";
$config['password_updated_successfully'] = "Your password has been updated successfully";
$config['update_password_error'] = "Wrong Password";
$config['customer_already_exists'] = "Customers Already Exists";
$config['registration_successfull'] = "Thank you for the registration. Please check your email for the instructions to activate your account. Note: Check your SPAM folder in case you don’t receive an email in inbox.";
$config['order_successfull'] = "Thank you, your order has been placed";
$config['refill_submitted_successfull'] = "Your Prescription Submitted Successfully";
$config['transfer_submitted_successfull'] = "Your Transfer Request Submitted Successfully";
$config['contactus_feedback_submitted_successfull'] = "Thank you very much for sharing your valuable feedback. We will strive to resolve the issue";
$config['contactus_suggestion_submitted_successfull'] = "Thank you for sharing your suggestion with us. We will definitely try to work on this in the near future";
$config['contactus_question_submitted_successfull'] = "Thank You. We received your question and our team will get back to you shortly";
$config['contactus_general_submitted_successfull'] = "Thank you for writing to us. We will revert to you with the information you requested";
$config['shipping_address_successfull'] = "Shipping Address added sucessfully";
$config['shipping_address_error'] = "Error! Please add again";
$config['customer_address_update_error'] = "Error! Please Update again";
$config['first_name_empty'] = "Please enter First Name";
$config['first_name_greaterthan_20characters'] = "First Name should be less than 20 characters";
$config['first_name_lessthan_2characters'] = "Please enter a valid First Name";
$config['last_name_empty'] = "Please enter Last Name";
$config['last_name_greaterthan_20characters'] = "Last Name should be less than 20 characters";
$config['last_name_lessthan_2characters'] = "Please enter a valid Last Name";
$config['email_empty'] = "Please Enter your E-Mail Address";
$config['email_invalid'] = "Enter a valid E-Mail Address";
$config['phone_number_empty'] = "Please enter Phone Number";
$config['phone_number_invalid'] = "Enter a valid Phone Number";
$config['primary_phone_number_empty'] = "Please enter Primary Phone Number";
$config['primary_phone_number_invalid'] = "Enter a valid Primary Phone Number";
$config['secondary_number_empty'] = "Please enter Phone Number";
$config['secondary_number_invalid'] = "Enter a valid Phone Number";
$config['address1_empty'] = "Please enter Address 1";
$config['city_empty'] = "Please enter City";
$config['state_empty'] = "Please select State";
$config['zip_code_empty'] = "Please enter Zip Code";
$config['zip_code_invalid'] = "Please enter Zip Code";
$config['register_password_empty'] = "Please enter Password";
$config['register_password_lessthan_8characters'] = "Password must be at least 8 characters long";
$config['login_password_empty'] = "Please enter Password";
$config['contactus_subject_empty'] = "Please enter Subject";
$config['contactus_rating_empty'] = "Please select the review";
$config['contactus_message_empty'] = "Please enter Message";


