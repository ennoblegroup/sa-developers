<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

        class Contact_model extends CI_Model  {
			  public $data = array();
			  
		public function __construct()  {
              parent::__construct();
         }
		 
		public function contact_email()
		{
			
			
			$contactemail = $this->db->get('contact')->row_array(); 
			$this->load->library('email'); 
			$email = $_POST['email'];
			$last_name = $_POST['last_name'];
			$first_name = $_POST['first_name'];
			$phone = $_POST['phone'];
			$subject_type = $_POST['subject_type'];
			$subject = $_POST['subject'];
			$message = $_POST['message'];
           
          
			$data['email']= $email; 
			$data['last_name']= $last_name;
			$data['first_name']= $first_name;
			$data['phone']= $phone;
			$data['subject_type']= $subject_type;
			$data['subject']= $subject;	 
			$data['message']= $message;
			
			$this->db->insert('messages', $data);
			return true;
		}

   		public function contact_email1()
		{
			//print_r($_POST);die;
			$this->load->library('email'); 
			$email = $_POST['email'];
			$last_name = $_POST['last_name'];
			$first_name = $_POST['first_name'];
			$phone = $_POST['phone'];
			$date = $_POST['date'];
			$time = $_POST['time'];
			$subject = $_POST['subject'];
			$message = $_POST['message'];
           
          
			$data['email']= $email; 
			$data['last_name']= $last_name;
			$data['first_name']= $first_name;
			$data['phone']= $phone;
			$data['date']= $date;
			$data['time']= $time;
			$data['status']= 0;
			$data['subject']= $subject;	 
			$data['message']= $message;
			
			$this->db->insert('appointments', $data);
			return true;
		}
		public function enquiry()
		{
			$contactemail = $this->db->get('contact')->row_array(); 
			$this->load->library('email'); 
			$email = $_POST['email'];
			$client_name = $_POST['client_name'];
			$contact_name = $_POST['contact_name'];
			$phone = $_POST['phone'];
			$notes = $_POST['notes'];
           
          
			$data['email_address']= $email; 
			$data['contact_name']= $contact_name;
			$data['client_name']= $client_name;
			$data['phone_number']= $phone; 
			$data['notes']= $notes;
			
			$this->db->insert('enquiries', $data);
			
            return true;		
		}
 		public function reviews()
		{
		    $query= $this->db->query("select * from reviews");
		    return $query->result_array();
		}
	}