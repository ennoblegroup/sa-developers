<?php
	class Category_model extends CI_Model{

		public function add_category($data){
			$this->db->insert('categories', $data);
			return true;
		}
		public function get_all_categories(){
			$this->db->select('*');
			$this->db->from('categories');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		
		public function get_all_active_categories(){
			$this->db->select('*');
			$this->db->from('categories');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_category_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM categories where id=$id");
			$data1   = $query->row_array();
			return $data1;
		}

      		public function get_all_materials(){
			$this->db->select('*,p.id as pid');
			$this->db->from('products p');
			$this->db->join('product_images pi','pi.product_id=p.id');
			
			//$this->db->join('project_services ps','ps.project_id=p.id');
			//$this->db->join('services s','s.id=ps.service_id');
			//$this->db->join('users u','u.client_id=p.client_id');
			//$this->db->join('project_website_permissions pwp','pwp.project_id=p.id');
			//$this->db->where('u.is_admin',1);
			//$this->db->order_by("p.id", "desc");
			$rs = $this->db->get();
			//print_r($rs->result_array());die;
			return $result = $rs->result_array();
		}
          

        public function get_materialsproduct_id($id){
			$this->db->select('*,p.id as pid');
			$this->db->from('products p');
			$this->db->join('product_images pi','pi.product_id=p.id');
			$this->db->where('p.id', $id );
			
			$rs = $this->db->get();
			//print_r($rs->result_array());die;
			return $result = $rs->row_array();
		}		  
		
		
		/*public function get_galleryimage($project_id){
			$this->db->select('file');
			$this->db->from('gallery');
			$this->db->where('project_id', $project_id);
			$this->db->where('file_type', 0 );
			$this->db->order_by('id', 'desc');
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}*/
	
		
		
		public function get_category_vendors(){
			$data = array();
			$query=$this->db->query("SELECT * FROM vendors ");
			//$query=$this->db->query("SELECT c.*, v.vendor_name FROM categories c,  vendor_categories vc,vendors v where c.id=vc.category_id and v.id=vc.vendor_id");
			$data   = $query->result_array();
			//$data['rp']   = $this->db->query("SELECT p.*, s.service_name,s.service_id as sid FROM project p,  gallery g where g.project_id=p.service_id  and p.project_id=$id")->result_array();
			return $data;
		}
		
		function user_product_count_all($category, $brand,$search_text)
         {
          $query = $this->make_query($category, $brand,$search_text);
          $data = $this->db->query($query);
          return $data->num_rows();
         }
		 function fetch_data($limit, $start,$category ,$brand,$search_text)
         {

          $query = $this->make_query($category, $brand,$search_text);
          $query .= 'LIMIT '.$start.', ' . $limit;

          $data = $this->db->query($query);
          $output = '';
          if($data->num_rows() > 0)
          {
           foreach($data->result_array() as $data)
           {
			$product_id = $data['product_id'];
			$data['product_detail'] = $this->sc->product_detail($product_id);
			$wishlist='fa-heart-o';
			$wishlist_class='btn-overlayyy';
			$toggle='Add to wishlist';
			
			//data-desc='". $data['product_desc']."'
            $output .= "<div class='col-md-4 col-lg-3 '><figure class='selectProduct card-product-grid' data-title='". $data['product_title']."' data-desc='". $data['product_desc']."' data-price='". $data['product_price']."'  data-id='". $data['product_id']."' data-img='".base_url()."images/products/". $data['image'] ."'  data-brand = '".$data['product_detail'][0]['brand_name']."' data-category = '".$data['product_detail'][0]['product_category_name']."'><div class='img-wrap'><a href='".base_url()."Welcome/productDetail/".$data['product_id'] ."'><img  src='".base_url()."images/material_categories/". $data['image'] ."' class='imgFill productImg".$data['product_id']."'></a></div><figcaption class='info-wrap'><div class='fix-height' style='text-align:center'><a href='".base_url()."Welcome/productDetail/". $data['product_id'] ."' data-toggle='tooltip'  title='".$data['product_title']."' class='title' style='overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;'>".$data['product_title']."</a><div class='price-wrap mt-2'>
                           <span class='price'>$". $data['product_price']."</span>
                        </div>
                     </div>
                   
					<input type='hidden' id='quantity' onkeypress='return event.charCode >= 48 && event.charCode <= 57' class='quantity".$data['product_id']." ' min='1'  style='max-width:50px;    border: 1px solid black;' value='1' >
				  </figcaption>
               </figure>
            </div>";
           }
          }
          else
          {
           $output = '<div class="col-md-12 col-lg-12"><h3 style="text-align:center">No Products Found</h3><div>';
          }
          return $output;
         }
		 
		
		
	}

?>