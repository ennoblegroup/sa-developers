<?php
	class Testimonials_model extends CI_Model{

		public function add_testimonials($data){
			$this->db->insert('testimonials', $data);
			return true;
		}
		public function get_all_testimonials(){
			$this->db->select('*');
			$this->db->from('testimonials');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_testimonials(){
			$this->db->select('*');
			$this->db->from('testimonials');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_testimonials_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM testimonials where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$testimonials_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM testimonials where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_testimonials($data, $id){
			$this->db->where('id', $id);
			$this->db->update('testimonials', $data);
			return true;
		}
		function update_status($testimonials_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $testimonials_id);
			return $this->db->update('testimonials',$data);
		}
	}

?>