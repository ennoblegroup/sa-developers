<?php
	class project_model extends CI_Model{

		public function get_all_projects(){
			$data = array();
			$query=$this->db->query("SELECT p.*, u.lastname,u.firstname,u.id as uid FROM projects p,  users u, project_website_permissions pwp where pwp.project_id = p.id and u.client_id=p.client_id and p.status=6 and pwp.status=1");
			$data   = $query->result_array();
			foreach( $data as $key=>$each ){
				$id= $each['id'];
				$data[$key]['image']   = $this->db->query("SELECT file FROM gallery where project_id=$id")->row_array();
			}
			
			return $data;
		}

		public function get_all_project_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT p.*,pwp.*, u.lastname,u.firstname,u.id as uid, p.status as status FROM projects p,project_website_permissions pwp,users u where u.client_id=p.client_id and p.id=$id and p.id=pwp.project_id and pwp.status=1");
			$data   = $query->row_array();
			$client_id=$data['client_id'];
			$before_images= $this->db->query("SELECT * FROM project_mapped_images pmi,gallery g where pmi.simage_id=g.id and pmi.project_id=$id and pmi.gallery_type=1")->result_array();
			$before_images = array_reverse(array_values( array_combine( array_column($before_images, 'simage_id'), $before_images)));
			$data['before_images']=$before_images;
			
			$vendors= $this->db->query("SELECT v.* FROM project_products pp,products p,vendors v where pp.product_id=p.id and p.vendor_id=v.id and pp.project_id=$id")->result_array();
			$vendors = array_reverse(array_values( array_combine( array_column($vendors, 'vendor_id'), $vendors)));
			$data['vendors']=$vendors;
			
			$data['videos']= $this->db->query("SELECT * FROM gallery  where file_type=1 and visibility =1 and project_id=$id")->result_array();
			$data['services'] = $this->db->query("SELECT * FROM project_services ps,services s where ps.service_id=s.id and ps.project_id=$id")->result_array();
			$data['testimonials'] = $this->db->query("SELECT * from messages where project_id=$id and review_status=1")->result_array();
			$data['related_projects'] = $this->db->query("SELECT p.*, u.lastname,u.firstname,u.id as uid, p.status as status FROM projects p,users u where u.client_id=p.client_id  and p.id!=$id and p.client_id=$client_id")->result_array();
			foreach( $data['related_projects'] as $key=>$each ){
				$id= $each['id'];
				$data['related_projects'][$key]['image']   = $this->db->query("SELECT file FROM gallery where project_id=$id")->row_array();
			}
			
			return $data;
		}
		public function get_all_after_images($id){
			$after_images= $this->db->query("SELECT * FROM project_mapped_images pmi,gallery g where pmi.mimage_id=g.id and pmi.simage_id=$id and pmi.gallery_type=1")->result_array();
			return $after_images;
		}
	}

?>