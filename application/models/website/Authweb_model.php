<?php
	class Authweb_model extends CI_Model{

		public function login($data){
			$query = $this->db->get_where('users', array('email' => $data['email']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword){
			        return $result = $query->row_array();
			    }
				
			}
		}
		public function get_all_websites(){
			$this->db->select('*');
			$this->db->from('ci_websites');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			return true;
		}

	}

?>