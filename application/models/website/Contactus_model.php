<?php
	class Contactus_model extends CI_Model{

		public function add_contactus($data){
			$this->db->insert('contact', $data);
			return true;
		}
		public function get_all_contactus(){
			$this->db->select('*');
			$this->db->from('contact');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		public function get_contactus_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM contact where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$contact_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM contact where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_contactus($data, $id){
			$this->db->where('id', $id);
			$this->db->update('contact', $data);
			return true;
		}
		function update_status($contact_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $contact_id);
			return $this->db->update('contact',$data);
		}
	}

?>