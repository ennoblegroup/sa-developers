<?php
	class Vendor_model extends CI_Model{

		public function add_vendor($data){
			$this->db->insert('vendors', $data);
			return $this->db->insert_id();
		}
		public function add_vendor_category($data){
			$this->db->insert('vendor_categories', $data);
			return true;
		}
		public function get_all_vendors(){
			$this->db->select('*');
			$this->db->from('vendors');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_vendors(){
			$this->db->select('*');
			$this->db->from('vendors');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_vendor_materials($id){
			$this->db->select('*,m.file as m_file,vm.id as vm_id');
			$this->db->from('vendor_materials vm');
			$this->db->join('materials m','m.id=vm.material_id');
			$this->db->join('material_categories mc','mc.id=m.category_id');
			$this->db->where_in('vm.vendor_id', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function add_insurance_vendor($data){
			$this->db->insert('insurance_vendors', $data);
			return true;
		}
		public function get_all_insurance_vendors(){
			$this->db->select('*,if.id as if_id');
			$this->db->from('insurance_vendors if');
			$this->db->join('insurance_companies ic','ic.id=if.insurance_company_id');
			$this->db->join('vendors f','f.id=if.vendor_id');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_vendor_by_id($id){
			$data   = $this->db->query("SELECT * FROM vendors where id=$id")->row_array();
			return $data;
		}
		public function get_vendor_by_email($email){
			$this->db->select('count(*) as cnt');
			$this->db->from('vendors');
			$this->db->where('vendor_email',$email);
			$query = $this->db->get();
			return $result = $query->row_array();
		}
		public function edit_vendor($data, $id){
			$this->db->where('id', $id);
			$this->db->update('vendors', $data);
			return true;
		}
		public function get_mapped_templates(){
			$vendor_id = $_POST['vendor_id'];
			return $this->db->query("select vm.material_id from vendors v,vendor_materials vm where vm.vendor_id=$vendor_id and vm.vendor_id=v.id")->result_array();
		}
	}

?>