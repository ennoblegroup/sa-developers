<?php   
	class Conversationweb_model extends CI_Model{

		public function add_conversation($data){
			$this->db->insert('ci_conversations', $data);
			return true;
		}
		public function get_conversation_by_id($id){
			$query = $this->db->get_where('ci_conversations', array('website_id' => $id));
			return $result = $query->result_array();
			
		}
		public function get_conversation_notification($user_id){
			$query = $this->db->query("select * from ci_conversations where user_id != $user_id and is_read =0");
			return $result = $query->num_rows();
			
		}
	}

?>