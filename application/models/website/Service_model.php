<?php
	class Service_model extends CI_Model{

		
		public function get_all_services(){
			$this->db->select('*');
			$this->db->from('services');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_services(){
			$this->db->select('*');
			$this->db->from('services');
			$this->db->where('home_status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_service_by_id($id){
			$this->db->select('*');
			$this->db->from('services');
			$this->db->where('id',$id);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}
		

	}

?>