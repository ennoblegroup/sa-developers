<?php
	class Material_model extends CI_Model{

		public function get_all_materials(){
			$this->db->select('*');
			$this->db->from('products p');
			$this->db->join('categories c','c.id=p.category_id');
			$this->db->join('vendors v','v.id = p.vendor_id');
			$this->db->join('product_images pi','pi.product_id = p.id');
			$this->db->where('pi.is_primary', 1);
			$this->db->where('p.status',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_categories(){
			$this->db->select('*');
			$this->db->from('categories');
			$this->db->where('status',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_vendors(){
			$this->db->select('*');
			$this->db->from('vendors');
			$this->db->where('status',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_material_by_id($id){
			$data = array();
			$this->db->select('*,p.id as id,p.product_id as pid');
			$this->db->from('products p');
			$this->db->join('categories c','c.id=p.category_id');
			$this->db->join('vendors v','v.id=p.vendor_id');
			$this->db->where('p.id', $id);
			$rs = $this->db->get();
			$data   = $rs->row_array();			
			$data['images'] = $this->db->query("SELECT * from product_images where product_id=$id order by is_primary DESC")->result_array();
			return $data;
		}
		function make_query($category, $vendor,$search_text){
          $query = "
          SELECT * FROM products p, product_images pi 
          WHERE p.status = '1' and p.id= pi.product_id and pi.is_primary=1 and product_name LIKE '%".$search_text."%'
          ";

          if(isset($vendor))
          {
           $vendor_filter = implode("','", $vendor);
           $query .= "
            AND vendor_id IN('".$vendor_filter."')
           ";
          }

          if(isset($category))
          {
           $ram_filter = implode("','", $category);
           $query .= "
            AND category_id IN('".$ram_filter."')
           ";
          }
          return $query;
         }
		function product_count_all($category, $vendor,$search_text)
        {
          $query = $this->make_query($category, $vendor,$search_text);
          $data = $this->db->query($query);
          return $data->num_rows();
        }
		function fetch_data($limit, $start,$category ,$vendor,$search_text)
         {

          $query = $this->make_query($category, $vendor,$search_text);
          $query .= 'LIMIT '.$start.', ' . $limit;

          $data = $this->db->query($query);
          $output = '';
          if($data->num_rows() > 0)
          {
           foreach($data->result_array() as $data)
           {
            $output .= '<div class="col-lg-3 col-md-3 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="new-arrival-product">
										<div class="new-arrivals-img-contnent">
											<div class="img-wrap"><a href="'.base_url().'website/materials/view_material/'.$data['id'] .'"><img  src="'.base_url().'images/products/'. $data['image'] .'" class="imgFill"></a></div>
										</div>
										<div class="product-content text-center mt-3">
											<h5>'.$data['product_name'].'</h5>
											<span class="price">$'. $data['price'] .'</span>
										</div>
									</div>
								</div>
							</div>
						</div>';
           }
          }
          else
          {
           $output = '<div class="col-md-12 col-lg-12"><h3 style="text-align:center">No Products Found</h3><div>';
          }
          return $output;
	}
}

?>