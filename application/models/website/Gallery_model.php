<?php
	class Gallery_model extends CI_Model{

		public function get_all_gallery(){
			$this->db->select('*');
			$this->db->from('gallery g');
			$this->db->join('gallery_types c','c.id=g.gallery_category');
			$this->db->where('c.visibility',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_categories(){
			$this->db->select('*');
			$this->db->from('gallery_types');
			$this->db->where('status',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_projects(){
			$this->db->select('*');
			$this->db->from('projects');
			$this->db->where('status',1 );
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_gallery_by_id($id){
			$data = array();
			$this->db->select('*,p.id as id,p.product_id as pid');
			$this->db->from('products p');
			$this->db->join('categories c','c.id=p.category_id');
			$this->db->join('projects v','v.id=p.project_id');
			$this->db->where('p.id', $id);
			$rs = $this->db->get();
			$data   = $rs->row_array();			
			$data['images'] = $this->db->query("SELECT * from product_images where product_id=$id order by is_primary DESC")->result_array();
			return $data;
		}
		function make_query($category, $project,$search_text){
          $query = "
          SELECT * FROM gallery
          WHERE visibility = '1' and gallery_title LIKE '%".$search_text."%'
          ";

          if(isset($project))
          {
           $project_filter = implode("','", $project);
           $query .= "
            AND project_id IN('".$project_filter."')
           ";
          }

          if(isset($category))
          {
           $ram_filter = implode("','", $category);
           $query .= "
            AND gallery_category IN('".$ram_filter."')
           ";
          }
          return $query;
         }
		function product_count_all($category, $project,$search_text)
        {
          $query = $this->make_query($category, $project,$search_text);
          $data = $this->db->query($query);
          return $data->num_rows();
        }
		function fetch_data($limit, $start,$category ,$project,$search_text)
         {

          $query = $this->make_query($category, $project,$search_text);
          $query .= 'LIMIT '.$start.', ' . $limit;

          $data = $this->db->query($query);
          $output = '';
          if($data->num_rows() > 0)
          {
           foreach($data->result_array() as $data)
           {
            $output .= '<div class="col-lg-3 col-md-3 col-sm-6">
							<div class="card">
								<div class="card-body">
									<div class="new-arrival-product">
										<div class="new-arrivals-img-contnent">
											<div class="img-wrap"><a data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" href="'.base_url().'images/project_gallery/'. $data['file'] .'"><img  src="'.base_url().'images/project_gallery/'. $data['file'] .'" class="imgFill"></a></div>
										</div>
										<div class="product-content text-center mt-3">
											<h5>'.$data['gallery_title'].'</h5>
										</div>
									</div>
								</div>
							</div>
						</div>';
           }
          }
          else
          {
           $output = '<div class="col-md-12 col-lg-12"><h3 style="text-align:center">No Images Found</h3><div>';
          }
          return $output;
	}
}

?>