<?php
	class Messages_model extends CI_Model{

		public function add_messages($data){
			$this->db->insert('messages', $data);
			return true;
		}
		public function get_all_messages(){
			$this->db->select('*');
			$this->db->from('messages');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_enquires(){
			$this->db->select('*');
			$this->db->from('enquiries');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_messages_by_id($id){
			$this->db->select('*');
			$this->db->where('message_id', $id);
			$this->db->from('messages');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_reply_messages_by_id($id){
			$this->db->select('*');
			$this->db->where('message_id', $id);
			$this->db->from('contactus_replies');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		function send_reply()	 
		{
		    $message = $_POST['message'];
		    $message_id = $_POST['message_id'];
		    $email = $_POST['email'];
		    $firstname = $_POST['first_name'];
		    $lastname = $_POST['last_name'];
		    $data['email'] = $email;
		    $data['message'] = $message;
		    $data['name'] = $lastname." ".$firstname;
		    $data['message_id'] = $message_id; 

		    $data1=array(
				"message_id"=>$message_id,
				"reply_message"=>$message,
				"replier_id"=>$_SESSION['sadevelopers_admin']['admin_id'],
				"replier_name"=>$_SESSION['sadevelopers_admin']['name']
			);
			
			$this->db->insert('contactus_replies',$data1);
			
    		$email_subject = "";
			$content = $message;			
		    $data['name'] = $lastname." ".$firstname;
			$data['email_subject'] = $email_subject;
			$data['content'] = $content;
			$data['client_name'] = 'admin System';
			$data['emaillogoclient'] = 'admin System';
			$temp =$this->load->view('email/common_email',$data,TRUE);
    		$msg="$temp";
    		$this->email->from('info@admin.com');
    		$this->email->to($email);
    		$this->email->subject($subject);
    	    $this->email->set_mailtype('html');
    		$this->email->message($msg);
    		$this->email->send();
    		return "success";
    		
		} 
		public function get_all_messages1(){
			$this->db->select('*');
			$this->db->from('messages');
			$this->db->order_by("message_id", "desc");
			$this->db->where('project_id', 0);
			$rs = $this->db->get();
			$result = $rs->result_array();
			$output = '';
			foreach ($result as $key=>$message){
				if($message['review_status']==1) { $checked='checked'; $status="<span>Active</span>"; } else { $checked=''; $status="<span>InActive</span>"; }
				$output .= "<tr>							
							<td>".$message['message_id']."</td>
							<td>".$message['last_name']." ".$message['first_name']." </td>
							<td>".$message['email']." </td>
							<td>".$message['phone']."</td>
							<td>".$message['subject']."</td>
							<td class='line-clamp'>".$message['message']."</td>
							<td>  
								<label class='switch' style='top:11px;'>
								  <input class='slider round permission_status' type='checkbox' ".$checked." id='status' message_id=".$message['message_id'].">
								  <span class='slider round'></span>
								</label>
								<span class='scl' id='status_roww". $message['message_id'] ."'>
								". $status ."
								</span>
							</td>
							<td> 
							<a href='#' class='removeRecord' message_id = ". $message['message_id'] ."><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-trash' data-toggle='tooltip' title='Delete'></i></button></a> 
							<a href='". base_url('admin/messages/view_message/'.$message['message_id']) ."'> <button type='button' class='btn btn-sm btn-outline-dark'  ><i class='fa fa-eye'></i></button> </a>
							</td>
						</tr>";
			}
			return $output;	
		}
		public function get_all_enquires1(){
		$this->db->select('*');
		$this->db->from('enquiries');
		$this->db->order_by("id", "desc");
		$rs = $this->db->get();
		$result = $rs->result_array();
		$output = '';
		foreach ($result as $key=>$message){
			$output .= "<tr>							
						<td>".$message['contact_name']."</td>
						<td>".$message['client_name']." </td>
						<td>".$message['email_address']." </td>
						<td>".$message['phone_number']."</td>
					</tr>";
			}			
			return $output;				
		}
		function update_status($message_id, $id) {
			$data=array('review_status'=> $id);
			$this->db->where('message_id', $message_id);
			return $this->db->update('messages',$data);
		}
	}

?>