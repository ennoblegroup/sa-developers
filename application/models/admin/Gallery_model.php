<?php
	class Gallery_model extends CI_Model{

		public function add_gallery($data){
			$this->db->insert('categories', $data);
			return true;
		}
		public function get_all_gallery(){
			$data = array();
			$this->db->select('*');
			$this->db->from('gallery_types');
			$rs = $this->db->get();
			$dataa = $rs->result_array();
			foreach( $dataa as $key=>$each ){
				$gallery_type_id= $each['id'];
				$cnt=$this->db->query("SELECT * FROM gallery g  where  g.gallery_category =$gallery_type_id")->num_rows();
				if($cnt > 0){
					$data[$key]['gallery']   = $this->db->query("SELECT * FROM gallery g  where  g.gallery_category=$gallery_type_id")->result_array();
					$data[$key]['gallery_type']   = $each['gallery_type_name'];
				}	
			}
			$data['gallery']= $data;
			return $data;
		}
		
		public function get_all_active_galleries(){
			$this->db->select('*');
			$this->db->from('galleries');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_gallery_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM galleries where id=$id");
			$data1   = $query->row_array();
			return $data1;
		}

		public function edit_gallery($data, $id){
			$this->db->where('id', $id);
			$this->db->update('galleries', $data);
			return true;
		}
	}

?>