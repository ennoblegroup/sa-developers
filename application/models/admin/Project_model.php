<?php
	class project_model extends CI_Model{

		public function add_project($data){
			$this->db->insert('projects', $data);
			return $this->db->insert_id();
		}
		public function add_project_service($data){
			$this->db->insert('project_services', $data);
			return true;
		}
		public function update_project($data,$project_id){
			$this->db->where('id', $project_id);
			$this->db->update('projects',$data);
			return true;
		}
		public function add_project_conversation($data){
			$this->db->insert('project_conversation', $data);
			return $this->db->insert_id();
		}
		public function add_project_update($data){
			$this->db->insert('project_updates', $data);
			return $this->db->insert_id();
		}
		public function add_project_update_comment($data){
			$this->db->insert('project_update_comments', $data);
			return $this->db->insert_id();
		}
		public function get_all_projects(){
			$this->db->select('*,u.id as user_id,u.status as status_id');
			$this->db->from('users u');
			$this->db->join('roles r','r.id=u.user_role');
			$this->db->where('u.project_id', $_SESSION['sadevelopers_admin']['project_id']);
			$this->db->where('r.id !=',0 );
			$this->db->order_by('user_id','desc');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_project_status(){
			$this->db->select('*');
			$this->db->from('project_status');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_gallery($project_id){
			$data = array();
			$this->db->select('*');
			$this->db->from('gallery_types');
			$rs = $this->db->get();
			$dataa = $rs->result_array();
			foreach( $dataa as $key=>$each ){
				$gallery_type_id= $each['id'];
				$cnt=$this->db->query("SELECT * FROM gallery g  where  g.gallery_category =$gallery_type_id and g.project_id=$project_id")->num_rows();
				if($cnt > 0){
					$data[$key]['gallery']   = $this->db->query("SELECT * FROM gallery g  where  g.gallery_category=$gallery_type_id and g.project_id=$project_id")->result_array();
					$data[$key]['gallery_type']   = $each['gallery_type_name'];
				}	
			}
			$data['gallery']= $data;
			$updates_images = [];
			$comments_images = [];
			$query=$this->db->query("SELECT * FROM project_updates where project_id=$project_id");
			$dataa   = $query->result_array();
			foreach( $dataa as $key=>$each ){
				$update_id=$each['id'];
				$ui= $this->db->query("SELECT file FROM project_update_files where project_update_id=$update_id")->result_array();
					foreach( $ui as $key=>$each ){
						array_push($updates_images,$each['file']);
					}
				$dataa2   = $this->db->query("SELECT * FROM project_update_comments where project_update_id=$update_id")->result_array();
				foreach( $dataa2 as $key=>$each ){
					$comment_id=$each['id'];
					$ci= $this->db->query("SELECT file FROM project_update_comment_files where comment_id=$comment_id")->result_array();
					foreach( $ci as $key=>$each ){
						array_push($comments_images,$each['file']);
					}
				}
			}

			
			$data['updates_images']=$updates_images;
			$data['comments_images']=$comments_images;

			return $data;
			
		}
		public function get_all_mapping_images($project_id){
			$this->db->select('*');
			$this->db->from('gallery');
			$this->db->where('gallery_type !=', 0);
			$this->db->where('file_type', 0);
			$this->db->where('visibility', 1);
			$this->db->where('project_id', $project_id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_projects1($id){
			$roleid= $_SESSION['sadevelopers_admin']['role_id'];

			$this->db->select('*,p.id as pid,p.city as city,p.state as state,,pwp.status as wstatus');
			$this->db->from('projects p');
			$this->db->join('users u','u.client_id=p.client_id');
			$this->db->join('project_website_permissions pwp','pwp.project_id = p.id');
			$this->db->join('project_status ps','ps.project_status_id=p.status');
			$this->db->where('u.is_admin',1);
			$this->db->order_by("p.id", "desc");
			if($_SESSION['sadevelopers_admin']['user_type']==1){
				$this->db->where('p.client_id',$_SESSION['sadevelopers_admin']['client_id']);
			}
			if($id !=''){
				$this->db->where('p.client_id',$id);
			}

			$rs = $this->db->get();
			$result = $rs->result_array();
			$client_id= $_SESSION['sadevelopers_admin']['client_id'];
			if($client_id > 0){
				$admin_class="hide_cls";
			}else{
				$admin_class="show_cls";
			}
			if($id ==''){
				$dft ='show_cls';
			}else{
				$dft ='hide_cls';
			}
			
			$output = '';
			foreach ($result as $phar){ 
			if($phar['status']==1) { $checked='checked'; $status="<span>Active</span>"; } else { $checked=''; $status="<span>In-Active</span>"; }
			
			if($phar['wstatus'] ==1 && $phar['project_status_id']==6){
			    $web_status= 'Published';
			}else{
				$web_status= '';
			}

			if($phar['project_end_date'] !=''){
			    $edate= date('F d, Y', strtotime(str_replace('/', '-', $phar['project_end_date'])));
			}else{
			    $edate='';
			}
			$output .= "<tr>
							<td><a data-toggle='tooltip' title='View' href='". base_url('admin/projects/view_project/'.$phar['pid'])."'>".$phar['p_id']."</a></td>
							<td>".$phar['project_name']."</td>
							<td>".$phar['city'].", ".$phar['state']."</td>
							<td class='".$dft."'>".$phar['lastname']." ".$phar['firstname']."</td>
							<td><a href='#' project_id = ".$phar['pid']." class='service_list'>See List</a></td>
							<td>  
								<div id='external-events'>
									<div class='external-event' data-class='".$phar['status_type']."'><i class='fa fa-move'></i>".$phar['project_status_name']."</div>
								</div>
							</td>
							<td>".$web_status."</td>
							<td> 
							<a data-toggle='tooltip' title='Edit' href='". base_url('admin/projects/edit/'.$phar['pid']) ."' class='btn btn-sm btn-outline-dark ".$admin_class."'><i class='fa fa-pencil'></i></a>							
							<a  title='Update Status' href='javascript:void(0)' class='".$admin_class."' onclick='openUpdateStatus(".$phar['pid'].")' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-hourglass-start' aria-hidden='true'></i></button></a>
							<a data-toggle='tooltip' title='Project Updates' href='". base_url('admin/projects/view_project_status/'.$phar['pid'])."' class='btn btn-sm btn-outline-dark'><i class='fa fa-history'></i></a>
							<a data-toggle='tooltip' title='Project Products' href='". base_url('admin/projects/link_project_products/'.$phar['pid'])."' class='btn btn-sm btn-outline-dark ".$admin_class."'><i class='fa fa-product-hunt' aria-hidden='true'></i></a>
							<!--a  title='Conversation' href='".base_url()."admin/projects/conversation/".$phar['pid']."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-comment' aria-hidden='true'></i></button></a-->
							<a  title='Documents' href='".base_url()."admin/projects/documents/".$phar['pid']."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-file-o' aria-hidden='true'></i></button></a>
							<a  title='Gallery' href='".base_url()."admin/projects/gallery/".$phar['pid']."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-picture-o' aria-hidden='true'></i></button></a>
							</td>
						</tr>";
			}
			return $output;	
		}
		public function get_project_services($id){
			
			$this->db->select('*');
			$this->db->from('project_services ps');
			$this->db->join('services s','s.id = ps.service_id');
			$this->db->where('ps.project_id',$id);
			$rs = $this->db->get();
			$result = $rs->result_array();
			
			$output = '';
			if(count($result)>0){
				foreach ($result as $phar){ 
					$output .= "<li>".$phar['service_name']."</li>";
				}
			}else{
				$output .= '<center><p>No services for this project</p></center>';
			}
			return $output;	
		}
		public function get_all_pharmacies_project(){
			$this->db->select('*,p.project_id as project_id,p.id as id');
			$this->db->from('pharmacies p');
			$this->db->order_by("p.id", "desc");
			$this->db->join('users u','u.project_id= p.id');
			$this->db->where('u.is_admin',1);
			$this->db->where('p.status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_projects(){
			$this->db->select('*');
			$this->db->from('projects');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_project_types(){
			$this->db->select('*');
			$this->db->from('project_types');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_projects(){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_type',1);
			$this->db->where('is_admin',1);
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		public function get_project_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT p.*, u.lastname,u.firstname,u.id as uid FROM projects p,  users u where u.client_id=p.client_id  and p.id=$id");
			$data   = $query->row_array();
			$data['services']   = $this->db->query("SELECT * FROM project_services where project_id=$id")->result_array();
			$data['files']   = $this->db->query("SELECT * FROM project_documents where project_id=$id")->result_array();
			$data['products']   = $this->db->query("SELECT pp.*,pi.*,pc.*,p.*,pp.id as id,pp.status as status FROM project_products pp, products p,product_images pi,categories pc where p.id=pi.product_id and p.category_id= pc.id and pp.product_id= p.id and pp.project_id=$id and pp.status=1 and pi.is_primary=1")->result_array();
			$data['website_access'] = $this->db->query("SELECT * FROM project_website_permissions where project_id=$id")->row_array();
			return $data;
		}
		public function get_project_services_by_productid($idd){
			$query= $this->db->query("SELECT service_id FROM project_services where project_id=$idd");
			$data   = $query->result_array();		
			return $data;
		}
		public function get_categories_by_service_id($services){

			$data = array();
			$query=$this->db->query("SELECT category_id FROM service_categories where service_id =$services");
			$data   = $query->result_array();		
			return $data;
		}
		public function get_project_products_by_category_id($id,$categories){
			//print_r($categories);die;
			
			foreach( $categories as $key=>$each ){
				$res=$this->db->query("SELECT category_name FROM categories where id=$each")->row_array();
				$data[$key]['category_name'] =$res['category_name'];
				$data[$key]['products'] = $this->db->query("SELECT * FROM products where category_id=$each")->result_array();
				foreach( $data[$key]['products'] as $kkey=>$each ){
					$ide= $each['id'];
					$query=$this->db->query("SELECT * FROM project_products where project_id= $id and product_id=$ide");
					$ttt =$query->num_rows();
					if($ttt>0){
						$data[$key]['products'][$kkey]['selected']=true;
					}else{
						$data[$key]['products'][$kkey]['selected']=false;
					}
				}
			}
			$data = array_filter($data);
		
			return $data;
		}
		public function get_other_project_products_by_category_id($categories){
			//print_r($categories);die;
			$res=$this->db->query("SELECT category_name FROM categories where id=$each")->row_array();
			foreach( $categories as $key=>$each ){
				$res=$this->db->query("SELECT category_name FROM categories where id=$each")->row_array();
				$data[$key]['category_name'] =$res['category_name'];
				$data[$key]['products'] = $this->db->query("SELECT * FROM products where category_id=$each")->result_array();
				//array_push($newArray, $data[$key]);
			}
			$data = array_filter($data);
		
			return $data;
		}
		public function get_all_documents_by_id($id){
			$data = array();
			
			$this->db->select('*');
			$this->db->from('document_types');
			$rs = $this->db->get();
			$dataa = $rs->result_array();
			foreach( $dataa as $key=>$each ){
				$document_type_id= $each['id'];
				$cnt=$this->db->query("SELECT * FROM project_documents d  where  d.document_type_id=$document_type_id and d.project_id=$id")->num_rows();
				if($cnt > 0){
					$data[$key]['docs']   = $this->db->query("SELECT * FROM project_documents d  where  d.document_type_id=$document_type_id and d.project_id=$id")->result_array();
					$data[$key]['document_type']   = $each['document_type_name'];
				}	
			}
			//$data =array_filter($data);
			return $data;
		}
		public function get_project_updates_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT pu.*,u.lastname,u.firstname,u.image FROM project_updates pu, users u where u.id= pu.user_id and pu.project_id=$id ORDER BY pu.create_date DESC");
			$data   = $query->result_array();
			foreach( $data as $key=>$each ){
				$id= $each['id'];
				$data[$key]['files']   = $this->db->query("SELECT * FROM project_update_files where project_update_id=$id")->result_array();
				$cmnts   = $this->db->query("SELECT *,puc.id as id,puc.created_date as created_date FROM project_update_comments  puc, users u where u.id= puc.user_id and puc.project_update_id=$id  order by  puc.id DESC")->result_array();
				foreach( $cmnts as $keyy=>$each ){
					$cid=$each['id'];
					$data[$key]['comments'][$keyy]['name']=$each['lastname'].' '.$each['firstname'];
					$data[$key]['comments'][$keyy]['created_date']=$each['created_date'];
					$data[$key]['comments'][$keyy]['comment']=$each['comment'];
					$data[$key]['comments'][$keyy]['files']=$this->db->query("SELECT * FROM project_update_comment_files where comment_id=$cid")->result_array();
				}
			}
			
			return $data;
		}
		public function get_projectuser_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM users where project_id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$project_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM users where project_id=$id")->row_array();
			}
			return $data1;
		}

		public function edit_project($data, $id){
			$this->db->where('id', $id);
			$this->db->update('pharmacies', $data);
			return true;
		}
		public function edit_project_users($data, $id){
			$this->db->where('project_id', $id);
			$this->db->update('users', $data);
			return true;
		}
		public function update_status($project_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $project_id);
			return $this->db->update('users',$data);
		}
		function add_project_status_history($project_status,$project_id,$status_name) {
			$this->db->trans_start();
			$this->db->insert('project_status_history', $project_status);
			$dataa=array('status'=> $status_name);
			$this->db->where('id', $project_id);
			$this->db->update('projects',$dataa);

			$this->db->trans_complete();
			return true;
		}
		public function add_project_products($project_id, $products) {
			$tags = implode(', ', $products);
			for($i=0;$i<count($products);$i++){
				$query=$this->db->query("SELECT * FROM project_products where project_id=$project_id and product_id=$products[$i]");
				$cnt   = $query->num_rows();
				if($cnt==0){
					$data = array(
						'project_id' => $project_id,
						'product_id' =>$products[$i],
						'status' => 1,
					);
					$this->db->insert('project_products',$data);
				}
			}
			$query=$this->db->query("DELETE  FROM project_products where project_id=$project_id and product_id NOT IN (".$tags.")");
			return true;
		}
		public function add_mapping_project_images($project_id, $radio_image, $gallery_type,$checkbox_images) {
			$tags = implode(', ', $checkbox_images);
			for($i=0;$i<count($checkbox_images);$i++){
				$query=$this->db->query("SELECT * FROM project_mapped_images where project_id=$project_id and simage_id=$radio_image and gallery_type=$gallery_type and mimage_id=$checkbox_images[$i]");
				$cnt   = $query->num_rows();
				if($cnt==0){
					$data = array(
						'project_id' => $project_id,
						'simage_id' =>$radio_image,
						'gallery_type' => $gallery_type,
						'mimage_id' => $checkbox_images[$i],
						'status' => 1,
					);
					$this->db->insert('project_mapped_images',$data);
				}
			}
			//$query=$this->db->query("DELETE  FROM project_products where project_id=$project_id and product_id NOT IN (".$tags.")");
			return true;
		}
		public function select_project_service($service_id,$project_id,$start_date,$end_date){
			$sql = "SELECT count(*) as cnt FROM pharmacies_services where service_id=$service_id and project_id=$project_id and start_date='$start_date' and end_date='$end_date'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		public function get_user_by_name($uname){
			$array = array('username' => $uname);
			$this->db->where($array);
			$query=$this->db->get("users");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_project_by_email($email){
			$array = array('business_email' => $email);
			$this->db->where($array);
			$query=$this->db->get("pharmacies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_project_by_npi($npi_number){
			$array = array('npi_number' => $npi_number);
			$this->db->where($array);
			$query=$this->db->get("pharmacies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
	}

?>