<?php
	class Communication_model extends CI_Model{

		public function submit_message($data)	 
		{
			$this->db->insert('communications', $data);
			return $this->db->insert_id();
			
		   /* $from_user_id = $this->session->userdata("user_id");
			$mail_to = $this->input->post('mail_to');
			$mail_from = $this->session->userdata("email");
			$mail_subject = $this->input->post('mail_subject');
			$mail_message = $this->input->post('mail_message');
		    foreach ($mail_to as $key=>$use){ 
			$data1['user']=$this->User_model->user_profile_email2($use);
            $to_user_id =$data1['user'][0]['user_id'];
			
			$data['from_user_id'] = $from_user_id;
			$data['to_user_id'] = $to_user_id;
			$data['mail_to'] = $use;
			$data['mail_from'] = $mail_from;
			$data['mail_subject'] = $mail_subject;
			$data['mail_message'] = $mail_message;
			// print_r($data);die;
			$this->db->insert('emails', $data);
		    }
			return true;*/
		}
		public function reply_message($data)	 
		{
			$this->db->insert('communications_replies', $data);
			return true;
		}
		public function get_users()
		{
			$pid=$_SESSION['sadevelopers_admin']['client_id'];
			if($pid==0){
				$this->db->select('*');
				$this->db->from('users');
				$this->db->where('client_id='.$pid);
				$this->db->or_where('is_admin=1 and client_id > 0');
			}else{
				$this->db->select('*');
				$this->db->from('users');
				$this->db->where('client_id='.$pid.' and is_admin=0');
				$this->db->or_where('is_admin=1 and client_id= 0');
			}
			
			//$this->db->where('id', $_SESSION['sadevelopers_admin']['id']);
			//$this->db->where('client_id',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_user_by_id($user)
		{
			$this->db->select('email');
			$this->db->from('users');
			$this->db->where('id',$user);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}		
		public function get_all_communications(){
			$id=$_SESSION['sadevelopers_admin']['admin_id'];			
			$this->db->select('*');
			$this->db->from('communications');
			$this->db->where('from_user_id='.$id.' or to_user_id='.$id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_message($id){
			$this->db->select('*');
			$this->db->from('communications');
			$this->db->where('id',$id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_reply_message($id){
			$this->db->select('*');
			$this->db->from('communications_replies');
			$this->db->where('communication_id',$id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}		
		public function get_files($communication_id)
		{
			$this->db->select('*');
			$this->db->from('communications_files');
			$this->db->where('communication_id',$communication_id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		function change_read_status($email_id)
		{
			
			$read_status_data= $this->db->query("select * from communications WHERE (id =".$email_id." AND from_user_id!=".$_SESSION['sadevelopers_admin']['id']." AND read_status = '1')")->row_array();
			if(!empty($read_status_data))
			{
				$read_status = $read_status_data['read_status'];
				if($read_status=='1')
				{
					$data1['read_status'] = '0';
					$this->db->where('id',$email_id);
					$this->db->update('communications',$data1);
				
				}
			}
			$unread_msg = $this->db->query("select * from communications_replies WHERE (communication_id =".$email_id." AND user_id!=".$_SESSION['sadevelopers_admin']['id']." AND read_status = '1')")->result_array();
			if(!empty($unread_msg))
			{
				foreach($unread_msg as $msg)
				{
					$data['read_status'] = '0';
					$this->db->where('reply_id',$msg['reply_id']);
					$this->db->update('communications_replies',$data);
				}
			}
		}
	}

?>