<?php
	class Auth_model extends CI_Model{

		public function login($data){
			$this->db->select('*,u.id as u_id,u.status as status_id,u.client_id as client_id,r.status as rstatus_id');
			$this->db->from('users u');
			$this->db->join('roles r','r.id=u.user_role');
			$this->db->where('u.email', $data['email']);
			$this->db->where('u.client_id', 0);
			$query = $this->db->get();
			
			$result = $query->row_array();
			if ($result == 0){
				return false;
			}else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
				$validPassword = password_verify($data['password'], $result['password']);
				
			    if($validPassword){
			        return $result = $query->row_array();
				}
				
			}
		}
		public function get_client($id){
			$this->db->select('*,p.client_id as p_id,p.id as client_id,');
			$this->db->from('clients p');
			$this->db->where('p.id', $id);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}
		public function check_useremail($data){
			$query = $this->db->get_where('users', array('email' => $data['email']));
			if ($query->num_rows() == 0){
				return false;
			}
			return true;
		}
		public function reset_link_email($data){
			$query = $this->db->get_where('users', array('email' => $data['email']));
			$data['user']=$query->row_array();
			$email_subject ='Reset Password';
			$sender_email = 'info@admin.com';
            $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
			$message = '';
            $message .= "<p>You are receiving this message in response to your request for password reset</p>"
                    . "<p>Follow this link to reset your password <a href='".site_url()."admin/auth/verify_resetpassword_link/?user_email=". $data['email'] ."&token=".$token."' >Reset Password</a> </p>"
                    . "<p>If You did not make this request kindly ignore!</p>"
                    . "";
			$content=$message;			
			$data['name'] = $data['user']['firstname']." ". $data['user']['lastname'];
			$name = $data['user']['firstname']." ". $data['user']['lastname'];
			$data['email_subject'] = $email_subject;
			$data['guest']='no';
			$data['content'] = $content;
			$data['client_name'] = 'admin System';
			$temp =$this->load->view('email/common_email',$data,TRUE);
			$msg=$temp;

			$this->email->from('info@admin.com');
			$this->email->to($data['user']['email']);
			//$this->email->bcc('saikrishnab@ennobletechnologies.com');
			$subject = 'Reset Password';  
			
			$this->email->subject($subject);

			$this->email->set_mailtype('html');
			$this->email->message($msg);
			$this->email->send();
			if(!$this->email->send()){			
			$eMail = $this->input->post('email');
			$ipadd = $this->input->ip_address();
			$insert = array(
				'email' => $eMail,
				'ipaddress' => $ipadd,
				'token' => $token
				);
			
			$this->db->insert('passwordreset', $insert);
			}
			return true;

		}
		public function verify_resetpassword_link($user_email,$token){
			
			$query = $this->db->get_where('passwordreset', array('email' => $user_email,'token'=>$token));
			
			if($query->row_array() > 0)
			{
				$result=$query->row_array();
				if($result['status']==0){
					return $result;
				}else{
					return false;
				}
			}
		}
		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			$data1['is_new']=0;
			$this->db->where('id', $id);
			$this->db->update('users', $data1);
			return true;
		}
		public function update_password($data, $email,$resetid){
			$this->db->trans_start();
			$this->db->where('email', $email);
			$this->db->update('users', $data);
			$data1['status']=1;
			$this->db->where('resetid', $resetid);
			$this->db->update('passwordreset', $data1);
			$this->db->trans_complete();
			return true;
		}
	}

?>