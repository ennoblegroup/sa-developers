<?php
	class team_model extends CI_Model{

		public function add_team($data){
			$this->db->insert('team', $data);
			return true;
		}
		public function edit_team($data, $id){
			$this->db->where('id', $id);
			$this->db->update('team', $data);
			return true;
		}
		public function get_all_teams(){
			$this->db->select('*');
			$this->db->from('team');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_team_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT * FROM team where id=$id");
			$data  = $query->row_array();
			return $data;
		}
		public function get_all_teams1(){
			$this->db->select('*');
			$this->db->from('team');
			$rs = $this->db->get();
			
			$result = $rs->result_array();
			$output = '';
			foreach ($result as $team){ 
			if($team['status']==1) { $checked='checked'; $status="<span>Active</span>"; } else { $checked=''; $status="<span>In-Active</span>"; }
			$output .= "<tr>							
							<td style='margin-left:10px;'>".$team['id']."</td>
							<td>".$team['lastname']." ".$team['firstname']." </td>
							<td>".$team['designation']."</td>
							<td>".$team['email']." </td>
							<td>".$team['mobile_no']."</td>
							<td>  
								<label class='switch' style='top:11px;'>
								  <input class='slider round permission_status' type='checkbox' ".$checked." id='status' team_sid=".$team['id'].">
								  <span class='slider round'></span>
								</label>
								<span class='scl' id='status_roww". $team['id'] ."'>
								". $status ."
								</span>
							</td>
							
							<td> 
							<a data-toggle='tooltip' title='Edit' href='". base_url('admin/team/edit/'.$team['id']) ."' class='btn btn-sm btn-outline-dark'><i class='fa fa-pencil'></i></a>
							<a data-toggle='tooltip' title='View' href='". base_url('admin/team/view_team/'.$team['id'])."' class='btn btn-sm btn-outline-dark'><i class='fa fa-eye'></i></a>
							<a data-toggle='tooltip' title='Delete' href='#' team_id = ". $team['id'] ." class='removeRecord btn btn-sm btn-outline-dark'><i class='fa fa-trash-o'></i></a>
							 </td>
						</tr>";
			}
			return $output;	
		}
		
		public function update_status($team_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $team_id);
			return $this->db->update('team',$data);
		}
		
	}

?>