<?php
	class Insurance_company_model extends CI_Model{

		public function add_insurance_company($data){
			$this->db->insert('insurance_companies', $data);
			return true;
		}
		public function get_all_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_insurance_company_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM insurance_companies where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$insurance_company_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM insurance_companies where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_insurance_company($data, $id){
			$this->db->where('id', $id);
			$this->db->update('insurance_companies', $data);
			return true;
		}
		function update_status($inscmp_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $inscmp_id);
			return $this->db->update('insurance_companies',$data);
		}
		public function get_user_by_email($email){
			$array = array('email' => $email);
			$this->db->where($array);
			$query=$this->db->get("users");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
	}

?>