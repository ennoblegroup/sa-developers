<?php
	class Documents_model extends CI_Model{

		public function add_document($data){
			$this->db->insert('documents', $data);
			return true;
		}
		public function get_all_client_projects(){
			$data = array();
			
			$this->db->select('*');
			$this->db->from('users');
			if($_SESSION['sadevelopers_admin']['client_id']==0){
				$this->db->where('client_id !=', 0);
			}else{
			    $this->db->where('client_id', $_SESSION['sadevelopers_admin']['client_id']);
			}
			$rs = $this->db->get();
			$dataa = $rs->result_array();
			
			foreach( $dataa as $key=>$each ){
				$client_id= $each['client_id'];
				$data[$key]['projects']   = $this->db->query("SELECT * FROM projects p  where  p.client_id=$client_id")->result_array();
				$data[$key]['client_name']   = $each['lastname'].' '.$each['firstname'];	
			}
			$data =array_filter($data);
			return $data;
		}
		public function get_all_documents_by_id(){
			$data = array();
			
			$this->db->select('*');
			$this->db->from('document_types');
			$rs = $this->db->get();
			$dataa = $rs->result_array();
			
			foreach( $dataa as $key=>$each ){
				$document_type_id= $each['id'];
				$cnt=$this->db->query("SELECT * FROM documents d  where  d.document_type_id=$document_type_id ")->num_rows();
				if($cnt > 0){
					$data[$key]['docs']   = $this->db->query("SELECT * FROM documents d  where  d.document_type_id=$document_type_id")->result_array();
					$data[$key]['document_type']   = $each['document_type_name'];
				}	
			}
			$data =array_filter($data);
			return $data;
		}
	}

?>