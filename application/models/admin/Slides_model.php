<?php
	class Slides_model extends CI_Model{

		public function add_slides($data){
			$this->db->insert('slider', $data);
			return true;
		}
		public function get_all_slides(){
			$this->db->select('*');
			$this->db->from('slider');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_slides(){
			$this->db->select('*');
			$this->db->from('slider');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_slides_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM slider where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$slides_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM slider where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_slides($data, $id){
			$this->db->where('id', $id);
			$this->db->update('slider', $data);
			return true;
		}
		function update_status($slides_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $slides_id);
			return $this->db->update('slider',$data);
		}
	}

?>