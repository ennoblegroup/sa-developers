<?php
	class client_model extends CI_Model{

		public function add_pharmacy($data){
			$this->db->insert('pharmacies', $data);
			return true;
		}
		public function add_client_service($data){
			$this->db->insert('pharmacies_services', $data);
			return true;
		}
		public function add_client_users($data){
			$this->db->insert('users', $data);
			return true;
		}
		public function get_all_clients(){
			$this->db->select('*,u.id as user_id,u.status as status_id');
			$this->db->from('users u');
			$this->db->join('roles r','r.id=u.user_role');
			$this->db->where('u.client_id', $_SESSION['sadevelopers_admin']['client_id']);
			$this->db->where('r.id !=',0 );
			$this->db->order_by('u.id','DESC');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_clients1(){
			$roleid= $_SESSION['sadevelopers_admin']['role_id'];
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',8);
			$this->db->where('permission_id',2);
			$status_countc = $this->db->get('roles_permissions')->num_rows();
			if($status_countc>0 || $roleid==0){
				$user_classc ='show_cls';
			}else{
				$user_classc ='hide_cls';
			}
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',8);
			$this->db->where('permission_id',3);
			$status_countu = $this->db->get('roles_permissions')->num_rows();
			if($status_countu >0 || $roleid==0){
				$user_classu ='show_cls';
			}else{
				$user_classu ='hide_cls';
			}
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',8);
			$this->db->where('permission_id',4);
			$status_countd = $this->db->get('roles_permissions')->num_rows();
			if($status_countd >0 || $roleid==0){
				$user_classd ='show_cls';
			}else{
				$user_classd ='hide_cls';
			}
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',8);
			$this->db->where('permission_id',6);
			$status_countd = $this->db->get('roles_permissions')->num_rows();
			if($status_countd >0 || $roleid==0){
				$user_classs ='show_cls';
			}else{
				$user_classs ='hide_cls';
			}
			$this->db->select('*,u.id as user_id,u.status as status_id');
			$this->db->from('users u');
			$this->db->where('u.client_id !=', '');
			$this->db->where('u.is_admin',1);
			$this->db->order_by('u.id','DESC');
			$rs = $this->db->get();
			$result = $rs->result_array();
			$output = '';
			foreach ($result as $phar){ 
			if($phar['status_id']==1) { $checked='checked'; $status="<span>Active</span>"; } else { $checked=''; $status="<span>In-Active</span>"; }
			
			$output .= "<tr>							
							<td style='margin-left:10px;'>".$phar['client_id']."</td>
							<td>".$phar['lastname']." ".$phar['firstname']." </td>
							<td>".$phar['city'].", ".$phar['state']." </td>
							<td>".$phar['email']." </td>
							<td>".$phar['mobile_no']."</td>
							<td class=".$user_classs.">  
								<label class='switch' style='top:11px;'>
								  <input class='slider round permission_status' type='checkbox' ".$checked." id='status' client_sid=".$phar['id'].">
								  <span class='slider round'></span>
								</label>
								<span class='scl' id='status_roww". $phar['id'] ."'>
								". $status ."
								</span>
							</td>
							<td> 
							<a data-toggle='tooltip' title='Edit' href='". base_url('admin/projects?id='.$phar['client_id'].'') ."' class='btn btn-sm btn-outline-dark'><i class='fa fa-building'></i> Projects</a>
							 </td>
							<td> 
							<a data-toggle='tooltip' title='Edit' href='". base_url('admin/clients/edit/'.$phar['id']) ."' class='btn btn-sm btn-outline-dark ".$user_classu."'><i class='fa fa-pencil'></i></a>
							<!--a data-toggle='tooltip' title='View' href='". base_url('admin/clients/view_client/'.$phar['id'])."' class='btn btn-sm btn-outline-dark'><i class='fa fa-eye'></i></a-->
							 </td>
						</tr>";
			}
			return $output;	
		}
		public function get_all_pharmacies_claim(){
			$this->db->select('*,p.pharmacy_id as pharmacy_id,p.id as id');
			$this->db->from('pharmacies p');
			$this->db->order_by("p.id", "desc");
			$this->db->join('users u','u.pharmacy_id= p.id');
			$this->db->where('u.is_admin',1);
			$this->db->where('p.status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_clients(){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_type',1);
			$this->db->where('is_admin',1);
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_clients(){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_type',1);
			$this->db->where('is_admin',1);
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		public function get_client_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM users where id=$id");
			$data1   = $query->result_array();
			return $data1;
		}
		public function get_pharmacyuser_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM users where pharmacy_id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$pharmacy_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM users where pharmacy_id=$id")->row_array();
			}
			return $data1;
		}

		public function edit_pharmacy($data, $id){
			$this->db->where('id', $id);
			$this->db->update('pharmacies', $data);
			return true;
		}
		public function edit_pharmacy_users($data, $id){
			$this->db->where('pharmacy_id', $id);
			$this->db->update('users', $data);
			return true;
		}
		public function update_status($client_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $client_id);
			return $this->db->update('users',$data);
		}
		public function select_pharmacy_service($service_id,$pharmacy_id,$start_date,$end_date){
			$sql = "SELECT count(*) as cnt FROM pharmacies_services where service_id=$service_id and pharmacy_id=$pharmacy_id and start_date='$start_date' and end_date='$end_date'";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		public function get_user_by_name($uname){
			$array = array('username' => $uname);
			$this->db->where($array);
			$query=$this->db->get("users");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_pharmacy_by_email($email){
			$array = array('business_email' => $email);
			$this->db->where($array);
			$query=$this->db->get("pharmacies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_pharmacy_by_npi($npi_number){
			$array = array('npi_number' => $npi_number);
			$this->db->where($array);
			$query=$this->db->get("pharmacies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
	}

?>