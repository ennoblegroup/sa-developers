<?php
	class Document_types_model extends CI_Model{

		public function add_document_type($data){
			$this->db->insert('document_types', $data);
			return true;
		}
		public function get_all_document_types(){
			$this->db->select('*');
			$this->db->from('document_types');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		
		public function get_all_active_document_types(){
			$this->db->select('*');
			$this->db->from('document_types');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_document_type_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM document_types where id=$id");
			$data1   = $query->row_array();
			return $data1;
		}

		public function edit_document_type($data, $id){
			$this->db->where('id', $id);
			$this->db->update('document_types', $data);
			return true;
		}
	}

?>