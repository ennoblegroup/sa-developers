<?php
	class product_model extends CI_Model{


		public function add_product($data){
			$this->db->insert('products', $data);
			return $this->db->insert_id();
		}
		public function get_all_products(){
			$this->db->select('*,p.id as id,p.product_id as pid');
			$this->db->from('products p');
			$this->db->join('categories c','c.id=p.category_id');
			$this->db->join('vendors v','v.id=p.vendor_id');
			$this->db->join('product_images pi','p.id=pi.product_id');
			$this->db->where('pi.is_primary', 1);
			$this->db->order_by('p.id','DESC');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_product_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT *,p.id as ppid,p.vendor_id as vendor_id FROM products p,categories c,vendors v where p.vendor_id=v.id and p.category_id=c.id and p.id=$id");
			$data   = $query->row_array();
			
			$data['images']   = $this->db->query("SELECT * from product_images where product_id=$id")->result_array();			
			return $data;
		}
		public function get_all_vendor_products(){
			$this->db->select('*,m.id as m_id,,m.file as m_file');
			$this->db->from('products m');
			$this->db->join('categories mc','mc.id=m.category_id');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}


		public function edit_product($data, $id){
			$this->db->where('id', $id);
			$this->db->update('products', $data);
			return true;
		}
		function update_status($product_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $product_id);
			return $this->db->update('products',$data);
		}
		function get_products_by_category() {
			$data = array();
			$query=$this->db->query("SELECT * FROM categories");
			$data   = $query->result_array();
			foreach( $data as $key=>$each ){
				$category_id= $each['id'];
				$data[$key]['products']   = $this->db->query("SELECT * FROM products where category_id=$category_id")->result_array();			
			}
			return $data;
		}
	}

?>