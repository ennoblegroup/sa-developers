<?php
	class Claim_model extends CI_Model{

		public function add_claim($data){
			$this->db->insert('claims', $data);
			return $this->db->insert_id();
		}
		public function add_claim_conversation($data){
			$this->db->insert('claim_conversation', $data);
			return $this->db->insert_id();
		}
		public function update_claim($data,$claim_id){
			$this->db->where('id', $claim_id);
			$this->db->update('claims',$data);
			return true;
		}
		public function add_claim_information($patient_data,$insurance_data,$physician_data,$claim_data){
			$this->db->trans_start();
			$this->db->insert('patient_insured_information', $patient_data);
			$this->db->insert('insurance_information', $insurance_data);
			$this->db->insert('physician_information', $physician_data);
			$this->db->insert('claim_information', $claim_data);
			$this->db->trans_complete();			
			return true;
		}
		public function update_claim_information($patient_data,$insurance_data,$physician_data,$claim_data,$claim_id){
			$this->db->trans_start();
			$this->db->where('claim_id', $claim_id);
			$this->db->update('patient_insured_information',$patient_data);
			$this->db->where('claim_id', $claim_id);
			$this->db->update('insurance_information',$insurance_data);
			$this->db->where('claim_id', $claim_id);
			$this->db->update('physician_information',$physician_data);
			$this->db->where('claim_id', $claim_id);
			$this->db->update('claim_information',$claim_data);
			$this->db->trans_complete();			
			return true;
		}
		public function add_insurance_additional_information($data){
			$this->db->insert('insurance_additional_information', $data);
			return true;
		}
		public function add_claim_additional_information($data){
			$this->db->insert('claim_additional_information', $data);
			return true;
		}
		public function update_claim_additional_information($data,$id){
			$this->db->where('id', $id);
			$this->db->update('claim_additional_information', $data);
			return true;
		}
		public function add_claim_diagnosis_information($data){
			$this->db->insert('claim_diagnosis', $data);
			return true;
		}
		public function update_claim_diagnosis_information($data,$id){
			$this->db->where('claim_id', $id);
			$this->db->update('claim_diagnosis',$data);
			return true;
		}
		public function get_all_claim_status_history(){
			$this->db->select('*');
			$this->db->from('claim_status_history');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		function get_all_claim_status($params = array()){
            $this->db->select('*');
            $this->db->from('claim_status as s');
            
            //fetch data by conditions
            if(array_key_exists("conditions",$params)){
                foreach ($params['conditions'] as $key => $value) {
                    if(strpos($key,'.') !== false){
                        $this->db->where($key,$value);
                    }else{
                        $this->db->where('s.'.$key,$value);
                    }
                }
            }
            
            $query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
    
            //return fetched data
            return $result;
        }
		public function get_all_claim_status_by_id($id){
			$this->db->select('*');
			$this->db->from('claims');
			$this->db->where('status', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_transaction_status_by_id($id){
			$this->db->select('*');
			$this->db->from('claims');
			$this->db->where('transaction_status', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_claims($client_id){
			$this->db->select('*,c.status as status_id');
			$this->db->from('claims c');
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->join('claim_status cs','cs.claim_status_id=c.status');/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			$this->db->where_in('c.client_id', $client_id);
			$this->db->where("c.status >",0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function drafts(){
			$this->db->select('*,c.status as status_id,c.id as claimm_id');
			$this->db->from('claims c');
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			$this->db->where('c.status', 0);
			$this->db->where('c.user_id', $_SESSION['sadevelopers_admin']['admin_id']);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_reports($id){
			$this->db->select('*,gc.status as status_id');
			$this->db->from('generated_claims gc');
			$this->db->join('claims c','c.id=gc.claim_id');
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->order_by('gc.status', 'DESC');/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			$this->db->where('gc.claim_id', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_report($id){
			$this->db->select('*,gc.status as status_id');
			$this->db->from('generated_claims gc');
			$this->db->join('claims c','c.id=gc.claim_id');
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->order_by('gc.status', 'DESC');/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			$this->db->where('gc.claim_id', $id);
			$this->db->where('gc.status', 1);
			$rs = $this->db->get();
			
			return $result = $rs->row_array();
		}
		public function get_claim_by_id($id){
			//$this->db->select('*,c.status as status_id,c.id as claimm_id,');
			$this->db->select('*,c.status as status_id,c.id as claimm_id,DATE_FORMAT(pii.patient_dob, "%m-%d-%Y") as patient_dob');
			$this->db->from('claims c');
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->join('claim_diagnosis cd','cd.claim_id=c.id');
			$this->db->join('insurance_forms if','if.insurance_company_id=c.insurance_company_id');
			$this->db->join('forms f','f.id=if.form_id');
			
			/*$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			$this->db->where('c.id', $id);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}
		public function get_claim_additional_information_by_id($id){
			$this->db->select('*');
			$this->db->from('claim_additional_information');
			$this->db->where('claim_id', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_claim_diagnosis_information_by_id($id){
			$this->db->select('*');
			$this->db->from('insurance_additional_information');
			$this->db->where('claim_id', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_claim_insured_checks_by_id($id){
			$this->db->select('*');
			$this->db->from('claim_diagnosis');
			$this->db->where('claim_id', $id);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function add_file_data($data,$claim_id){
			$this->db->trans_start();
			$this->db->insert('generated_claims', $data);
			$dataa['status']=0;
			$id=$this->db->insert_id();
			$this->db->where('id !=', $id);
			$this->db->where('claim_id', $claim_id);
			$this->db->update('generated_claims', $dataa);
			$this->db->trans_complete();
			return true;
		}
		function update_claim_status($claim_id, $status_id) {
			$data=array('transaction_status'=> $status_id);
			$this->db->where('id', $claim_id);
			$this->db->update('claims',$data);
		}
		function add_claim_status_history($data,$claim_id,$status_name,$status_group) {
			$this->db->trans_start();
			$this->db->insert('claim_status_history', $data);
			if($status_group==1){
				$dataa=array('status'=> $status_name);
			}elseif($status_group==2){
				$dataa=array('transaction_status'=> $status_name);
			}elseif($status_group==3){
				$dataa=array('payment_status'=> $status_name);
			}
			$this->db->where('id', $claim_id);
			$this->db->update('claims',$dataa);

			$this->db->trans_complete();
			return true;
		}
		function filter_claims($insurance_company_id,$client_id,$status_id,$start_date,$end_date) {
			$sstatus = []; 
			$tstatus = []; 
			$pstatus = [];
			foreach ($status_id as $value) {
				if($value <=6 ){
					array_push($sstatus,$value);
				}else if($value >6 && $value <=12){
					array_push($tstatus,$value);
				}else if($value >12){
					array_push($pstatus,$value);
				}
			}
			$this->db->select('*,c.status as statuss_id,c.id as cid');
			$this->db->from('claims c');
			$this->db->order_by("c.id", "desc");
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			//$this->db->join('generated_claims gc','gc.claim_id=c.id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->join('claim_status cs','cs.claim_status_id=c.status','cs.claim_status_id=c.transaction_status');
			//$this->db->where('gc.status', 1);
			/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			if($start_date=='' && $end_date==''){
				$this->db->where_in("c.created_date BETWEEN '$start_date' AND '$end_date'");
			}else{
			$this->db->where("c.created_date BETWEEN '$start_date' AND '$end_date'");
			}
			if($_SESSION['sadevelopers_admin']['client_id'] ==0){
				$this->db->where("c.status >",1);
			}else{
				$this->db->where("c.status >",0);
			}
			$this->db->where_in('c.insurance_company_id', $insurance_company_id);
			$this->db->where_in('c.client_id', $client_id);
			if(count($sstatus)>0){
				$this->db->where_in('c.status', $sstatus);
			}
			if(count($tstatus)>0){
				$this->db->or_where_in('c.transaction_status', $tstatus);
			}
			if(count($pstatus)>0){
				$this->db->or_where_in('c.payment_status', $pstatus);
			}
			
			$rs = $this->db->get();
			$result = $rs->result_array();
			$roleid= $_SESSION['sadevelopers_admin']['client_id'];
			$is_admin= $_SESSION['sadevelopers_admin']['is_admin'];
			if($roleid > 0){
				$admin_class="hide";
			}else{
				$admin_class="null";
			}
			if($roleid > 0){
				$submit_class="claim_submission";
			}else{
				$submit_class="claim_submission_ins";
			}
			if($is_admin > 0){
				$chat_class="null";
			}else{
				$chat_class="hide";
			}
			$output = '';
			if(count($result) > 0)
			{
				foreach($result as $data){
					
					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['statuss_id']);
					$s1 = $this->db->get();
					$status = $s1->row_array();
					
					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['transaction_status']);
					$s2 = $this->db->get();
					$transaction_status = $s2->row_array();

					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['payment_status']);
					$s3 = $this->db->get();
					$payment_status = $s3->row_array();

					if($data['statuss_id'] ==3 || $data['statuss_id'] ==5){
						$status_class='blocked';
					}else{
						$status_class='null';
					}
					if($roleid > 0){
						if($data['statuss_id'] ==2 || $data['statuss_id'] ==4 || $data['statuss_id'] ==5 || $data['statuss_id'] ==6){ 
							$edit_class='blocked';
						}else{
							$edit_class='null';
						}
					}else{
						if($data['statuss_id'] ==2 || $data['statuss_id'] ==4 || $data['transaction_status'] ==7 || $data['transaction_status'] ==10){ 
							$edit_class='null';
						}else{
							$edit_class='blocked';
						}
					}
					if($roleid > 0){
						$generate_class='blocked';
					}else{
						if($data['transaction_status'] ==7 || $data['transaction_status'] ==8){ 
							$generate_class='null';
						}else{
							$generate_class='blocked';
						}
					}
					if($roleid > 0){
						$status_class='blocked';
					}else{
						if($data['statuss_id'] ==6 && $data['payment_status'] ==16){ 
							$status_class='blocked';
						}else{
							$status_class='null';
						}
					}
					if($roleid > 0){
						$s_class='null';
					}else{
						if($data['transaction_status'] ==8){ 
							$s_class='null';
						}else{
							$s_class='null';
						}
					}
					$rr=$data['cid'];
					
					$output .= "<tr>
								<td class='".$admin_class."' style='padding:5px;'>
									<div class='custom-control custom-checkbox'>
										<input type='checkbox' name='claim_checkbox'  class='custom-control-input wwe' value='".$data['id']."' claim_id='".$rr."' chat_class='".$chat_class."' edit_class='".$edit_class."' s_class='".$s_class."' ttt='".$data['statuss_id']."' submit_class='".$submit_class."' status_class='".$status_class."' admin_class='".$admin_class."' generate_class='".$generate_class."'  status_id='".$data['statuss_id']."' transaction_status='".$data['transaction_status']."' payment_status='".$data['payment_status']."' id='checkbox".$data['id']."'>
										<label class='custom-control-label' for='checkbox".$data['id']."'></label>
									</div> 
								</td>
								<td>".$data['claim_registration_id']."</td>
								<td class='".$admin_class."'>".$data['client_lbn_name']."</td>
								<td>".$data['insurance_company_name']."</td>
								<td>".$data['last_name']." ".$data['first_name']."</td>
								<td>
								<div id='external-events'>
									<div class='external-event' data-class='".$status['status_type']."'><i class='fa fa-move'></i>".$status['claim_status_name']."</div>
								</div>
								</td>
								<td class='".$admin_class."'>
								<div id='external-events'>
									<div class='external-event' data-class='".$transaction_status['status_type']."'><i class='fa fa-move'></i>".$transaction_status['claim_status_name']."</div>
								</div>
								</td>
								<td class='".$admin_class."'>
								<div id='external-events'>
									<div class='external-event' data-class='".$payment_status['status_type']."'><i class='fa fa-move'></i>".$payment_status['claim_status_name']."</div>
								</div>
								</td>
								<td style='text-align:right'>
									<input type='radio' name='claim_radio'  class='wwe' value='".$data['id']."' claim_id='".$rr."' chat_class='".$chat_class."' edit_class='".$edit_class."' s_class='".$s_class."' ttt='".$data['statuss_id']."' submit_class='".$submit_class."' status_class='".$status_class."' admin_class='".$admin_class."' generate_class='".$generate_class."'  status_id='".$data['statuss_id']."' transaction_status='".$data['transaction_status']."' payment_status='".$data['payment_status']."' id='radio".$data['id']."'>
									<!--div class='dropdown custom-dropdown mb-0'>
										<div data-toggle='dropdown' style='cursor: pointer;'>
											<i class='fa fa-ellipsis-v'></i>
										</div>
										<div class='dropdown-menu dropdown-menu-right'>
											<a  href='".base_url()."admin/claims/single_report/".$rr."' title='Generate Report' class='".$admin_class." ".$generate_class."' ><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-file-pdf-o'></i></button></a>
											<a  href='".base_url()."admin/claims/reports/".$rr."' class='".$admin_class."' title='View All Reports'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-files-o' aria-hidden='true'></i></button></a>								
											<a  title='Update Status' href='javascript:void(0)' class='".$admin_class." ".$status_class."' onclick='openUpdateStatus(".$rr.",".$data['statuss_id'].",".$data['transaction_status'].",".$data['payment_status'].")' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-hourglass-start' aria-hidden='true'></i></button></a>
											<a  title='View Status History' href='javascript:void(0)' onclick='qwert(".$rr.")' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-history' aria-hidden='true'></i></button></a>
											<a  title='Edit Claim' href='".base_url()."admin/claims/edit/".$rr."' class='".$edit_class."'><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-pencil' aria-hidden='true'></i></button></a>
											<a  title='View Claim' href='".base_url()."admin/claims/view_claim/".$rr."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-eye' aria-hidden='true'></i></button></a>
											<a  title='Conversation' class='".$chat_class."' href='".base_url()."admin/claims/conversation/".$rr."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-comment' aria-hidden='true'></i></button></a>
											<a title='Submit' class='".$submit_class." ".$s_class."' ttt='".$data['statuss_id']."' claim_id='".$rr."'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></a>
										</div>
									</div-->
									
									
								</td>
							</tr>";
			 	}
			}
			else
			{
			 $output = '';
			}
			return $output;
		}
		function filter_claims_rs($insurance_company_id,$client_id,$status_id,$start_date,$end_date) {
			$this->db->select('*,c.status as statuss_id,c.id as cid');
			$this->db->from('claims c');
			$this->db->order_by("c.id", "desc");
			$this->db->join('insurance_companies ic','ic.id=c.insurance_company_id');
			$this->db->join('generated_claims gc','gc.claim_id=c.id');
			$this->db->join('clients p','p.id=c.client_id');
			$this->db->join('patient_insured_information pii','pii.claim_id=c.id');
			$this->db->join('insurance_information ii','ii.claim_id=c.id');
			$this->db->join('physician_information pi','pi.claim_id=c.id');
			$this->db->join('claim_information ci','ci.claim_id=c.id');
			$this->db->join('claim_status cs','cs.claim_status_id=c.status','cs.claim_status_id=c.transaction_status');
			$this->db->where('gc.status', 1);
			/*
			$this->db->join('insurance_additional_information iai','iai.claim_id=c.id');
			$this->db->join('claim_additional_information cai','cai.claim_id=c.id');*/
			if($start_date=='' && $end_date==''){
				$this->db->where_in("c.created_date BETWEEN '$start_date' AND '$end_date'");
			}else{
			$this->db->where("c.created_date BETWEEN '$start_date' AND '$end_date'");
			}
			if($_SESSION['sadevelopers_admin']['client_id'] ==0){
				$this->db->where("c.status >",1);
			}else{
				$this->db->where("c.status >",0);
			}
			$this->db->where_in('c.insurance_company_id', $insurance_company_id);
			$this->db->where_in('c.client_id', $client_id);
			$this->db->where_in('c.transaction_status', $status_id);
			
			
			$rs = $this->db->get();
			$result = $rs->result_array();
			$roleid= $_SESSION['sadevelopers_admin']['client_id'];
			$is_admin= $_SESSION['sadevelopers_admin']['is_admin'];
			if($roleid > 0){
				$admin_class="hide";
			}else{
				$admin_class="null";
			}
			if($roleid > 0){
				$submit_class="claim_submissionp";
			}else{
				$submit_class="claim_submission_insp";
			}
			if($is_admin > 0){
				$chat_class="null";
			}else{
				$chat_class="hide";
			}
			$output = '';
			if(count($result) > 0)
			{
				foreach($result as $data){
					
					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['statuss_id']);
					$s1 = $this->db->get();
					$status = $s1->row_array();
					
					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['transaction_status']);
					$s2 = $this->db->get();
					$transaction_status = $s2->row_array();

					$this->db->select('*');
					$this->db->from('claim_status');
					$this->db->where('claim_status_id', $data['payment_status']);
					$s3 = $this->db->get();
					$payment_status = $s3->row_array();

					if($data['statuss_id'] ==3 || $data['statuss_id'] ==5){
						$status_class='blocked';
					}else{
						$status_class='null';
					}
					if($roleid > 0){
						if($data['statuss_id'] ==2 || $data['statuss_id'] ==4 || $data['statuss_id'] ==5 || $data['statuss_id'] ==6){ 
							$edit_class='blocked';
						}else{
							$edit_class='null';
						}
					}else{
						if($data['statuss_id'] ==2 || $data['statuss_id'] ==4 || $data['transaction_status'] ==7 || $data['transaction_status'] ==10){ 
							$edit_class='null';
						}else{
							$edit_class='blocked';
						}
					}
					if($roleid > 0){
						$generate_class='blocked';
					}else{
						if($data['transaction_status'] ==7 || $data['transaction_status'] ==8){ 
							$generate_class='null';
						}else{
							$generate_class='blocked';
						}
					}
					if($roleid > 0){
						$status_class='blocked';
					}else{
						if($data['statuss_id'] ==6 && $data['payment_status'] ==16){ 
							$status_class='blocked';
						}else{
							$status_class='null';
						}
					}
					if($roleid > 0){
						$s_class='null';
					}else{
						if($data['transaction_status'] ==8){ 
							$s_class='null';
						}else{
							$s_class='null';
						}
					}
					$rr=$data['cid'];
					$output .= "<tr>
								<td style='padding:5px;'>
									<div class='custom-control custom-checkbox'>
										<input type='checkbox' name='claim_checkbox'  class='custom-control-input wwe' value='".$data['id']."' claim_id='".$rr."' chat_class='".$chat_class."' edit_class='".$edit_class."' s_class='".$s_class."' ttt='".$data['statuss_id']."' submit_class='".$submit_class."' status_class='".$status_class."' admin_class='".$admin_class."' generate_class='".$generate_class."'  status_id='".$data['statuss_id']."' transaction_status='".$data['transaction_status']."' payment_status='".$data['payment_status']."' id='checkbox".$data['id']."'>
										<label class='custom-control-label' for='checkbox".$data['id']."'></label>
									</div> 
								</td>
								<td>".$data['claim_registration_id']."</td>
								<td class='".$admin_class."'>".$data['client_lbn_name']."</td>
								<td>".$data['insurance_company_name']."</td>
								<td>".$data['last_name']." ".$data['first_name']."</td>
								<td>
									<a title='View Report' href='".base_url()."admin_files/generated_claims/".$data['file_name']."' target='_blank'>
									<button type='button' class='btn btn-sm btn-square btn-outline-dark'><i class='fa fa-file-pdf-o'></i></button></a>
								</td>
								<td style='text-align:right'>
									<a title='Submit' class='".$submit_class." ".$s_class."' ttt='".$data['statuss_id']."' claim_id='".$rr."'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></a>
									<!--div class='dropdown custom-dropdown mb-0'>
										<div data-toggle='dropdown' style='cursor: pointer;'>
											<i class='fa fa-ellipsis-v'></i>
										</div>
										<div class='dropdown-menu dropdown-menu-right'>
											<a  href='".base_url()."admin/claims/single_report/".$rr."' title='Generate Report' class='".$admin_class." ".$generate_class."' ><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-file-pdf-o'></i></button></a>
											<a  href='".base_url()."admin/claims/reports/".$rr."' class='".$admin_class."' title='View All Reports'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-files-o' aria-hidden='true'></i></button></a>								
											<a  title='Update Status' href='javascript:void(0)' class='".$admin_class." ".$status_class."' onclick='openUpdateStatus(".$rr.",".$data['statuss_id'].",".$data['transaction_status'].",".$data['payment_status'].")' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-hourglass-start' aria-hidden='true'></i></button></a>
											<a  title='View Status History' href='javascript:void(0)' onclick='qwert(".$rr.")' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-history' aria-hidden='true'></i></button></a>
											<a  title='Edit Claim' href='".base_url()."admin/claims/edit/".$rr."' class='".$edit_class."'><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-pencil' aria-hidden='true'></i></button></a>
											<a  title='View Claim' href='".base_url()."admin/claims/view_claim/".$rr."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-eye' aria-hidden='true'></i></button></a>
											<a  title='Conversation' class='".$chat_class."' href='".base_url()."admin/claims/conversation/".$rr."' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-comment' aria-hidden='true'></i></button></a>
											<a title='Submit' class='".$submit_class." ".$s_class."' ttt='".$data['statuss_id']."' claim_id='".$rr."'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></a>
										</div>
									</div-->
									
									
								</td>
							</tr>";
			 	}
			}
			else
			{
			 $output = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty">No claims found</td></tr>';
			}
			return $output;
		}
		public function timeago($time=false, $just_now=false) {
			if ($time instanceOf DateTime)
				$time = $time->getTimestamp();
			elseif (is_numeric($time))
				$time = date('m/d/y h:i A', $time);
			if (strtotime($time) === false)
				$time = date('m/d/y h:i A', time());
			$interval =  date_create($time)->diff(date_create('now'));
			$adjective = strtotime($time) > time() ? 'from now' : 'ago';
			return (
				$interval->days > 0 ? 
					$time : (
						$interval->h < 1  && $interval->i < 1 && $just_now ? 
							'just now' : 
							(
								$interval->h > 1 ? 
									$interval->h.' hour'.(
										$interval->h > 1 ? 
											's' : 
											''
									).' ago' : 
									$interval->i.' minutes'.' '.$adjective
							)
					)
			);
		}
		function claim_status_history($id,$type) {
			$this->db->select('*');
			$this->db->from('claim_status_history csh');
			$this->db->join('claim_status cs','cs.claim_status_id=csh.status_name');
			$this->db->where('csh.claim_id', $id);
			$this->db->where('csh.status_group', $type);
			$rs = $this->db->get();
			$result = $rs->result_array();
			$output = '';
			if(count($result) > 0)
			{
				
				foreach($result as $data)
			 	{
					$new_date = $this->timeago($data['create_date']);
			  		$output .= "<li>
									<div class='timeline-badge primary'></div>
									<a class='timeline-panel text-muted' href='#'>
										<span>".$new_date."</span>
										<h6 class='m-t-5'>".$data['claim_status_name']."</h6>
									</a>
								</li>";
			 	}
			}
			else
			{
			 $output = "<li>
							<div class='timeline-badge primary'></div>
							<a class='timeline-panel text-muted' href='#'>
								<span>No Date Found</span>						
								<h6 class='m-t-5'>No Status Found</h6>
							</a>
						</li>";
			}
			return $output;
		}
		public function edit_claim($data, $id){
			$this->db->where('id', $id);
			$this->db->update('claims', $data);
			return true;
		}
		public function claim_status_history_id($id,$type)
		{
			$this->db->select('*');
			$this->db->from('claim_status_history csh');
			$this->db->join('claim_status cs','cs.claim_status_id=csh.status_name');
			$this->db->where('csh.claim_id', $id);
			$this->db->where('csh.status_group', $type);
			$this->db->order_by('csh.id', 'ASC');
			$rs = $this->db->get();
			$result = $rs->result_array();
			return $result;
		}
		public function get_inurance_claims($type,$insid)
		{
			$this->db->select('count(*) as cnt');
			$this->db->from('claims');
			$this->db->where('status', $type);
			$this->db->where('insurance_company_id', $insid);
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_ts_inurance_claims($type,$insid)
		{
			$this->db->select('count(*) as cnt');
			$this->db->from('claims');
			$this->db->where('transaction_status', $type);
			$this->db->where('insurance_company_id', $insid);
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_inurance_claims_by_client_id($type,$insid,$pid)
		{
			$this->db->select('count(*) as cnt');
			$this->db->from('claims');
			$this->db->where('status', $type);
			$this->db->where('insurance_company_id', $insid);
			$this->db->where('client_id', $pid);
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_client_claims($type,$pid)
		{
			$this->db->select('count(*) as cnt');
			$this->db->from('claims');
			$this->db->where('transaction_status', $type);
			$this->db->where('client_id', $pid);
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_month_claims($month)
		{			
			$this->db->select('count(*) as cnt');
			$this->db->from('claims');
			if($month==9)
				$this->db->where('created_date BETWEEN "2020-09-01" AND "2020-09-30"');
			else if ($month==10)
				$this->db->where('created_date BETWEEN "2020-10-01" AND "2020-10-31"');
			else if ($month==11)
				$this->db->where('created_date BETWEEN "2020-11-01" AND "2020-11-30"');
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_client_claim_status_by_id($pid,$id){
			$this->db->select('*');
			$this->db->from('claims');
			$this->db->where('status',$id);
			$this->db->where('client_id',$pid);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function claim_status_history1($cid,$id,$type)
		{
			$this->db->select('*');
			$this->db->from('claim_status_history csh');
			$this->db->join('claim_status cs','cs.claim_status_id=csh.status_name');
			$this->db->where('csh.claim_id', $id);
			$this->db->where('csh.status_group', $type);
			$this->db->where('csh.status_name', $cid);
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}		
		public function claim_status_history2($cid,$id,$type)
		{
			$this->db->select('*');
			$this->db->from('claim_status_history csh');
			$this->db->join('claim_status cs','cs.claim_status_id=csh.status_name');
			$this->db->where('csh.claim_id', $id);
			$this->db->where('csh.status_group', $type);
			$this->db->where('csh.status_name > ', $cid);			
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}	
		public function get_claim_by_cid($id){
			$this->db->select('*');
			$this->db->from('claims');
			$this->db->where('id', $id);			
			$rs = $this->db->get();
			$result = $rs->row_array();
			return $result;
		}
		public function get_lclaim_by_cid($id){
			$this->db->select('*');
			$this->db->from('claim_status_history');
			$this->db->where('claim_id', $id);
			$this->db->order_by("id", "desc");	
			$this->db->limit(2);  			
			$rs = $this->db->get();
			$result = $rs->result_array();
			return $result;
		}
		public function get_lclaim_by_cidtype($id,$type){
			$this->db->select('*');
			$this->db->from('claim_status_history');
			$this->db->where('claim_id', $id);
			$this->db->where('status_group', $type);
			$this->db->order_by("id", "desc");	
			$this->db->limit(2);  			
			$rs = $this->db->get();
			$result = $rs->result_array();
			return $result;
		}
	}

?>