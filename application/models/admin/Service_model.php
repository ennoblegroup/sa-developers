<?php
	class Service_model extends CI_Model{

		public function add_service($data){
			$this->db->insert('services', $data);
			return $this->db->insert_id();
		}
		public function add_service_category($data){
			$this->db->insert('service_categories', $data);
			return true;
		}
		public function get_all_services(){
			$this->db->select('*');
			$this->db->from('services');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_services(){
			$this->db->select('*');
			$this->db->from('services');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_services(){
			$this->db->select('*');
			$this->db->from('services');
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_service_by_id($id){
			$data = array();
			$query=$this->db->query("SELECT * FROM services where id=$id");
			$data   = $query->row_array();
			$data['categories']   = $this->db->query("SELECT * FROM service_categories where service_id=$id")->result_array();			
			return $data;
		}
		public function get_service_category($id){
			$roleid= $_SESSION['sadevelopers_admin']['role_id'];

			$this->db->select('*');
			$this->db->from('service_categories sc');
			$this->db->join('categories c','c.id = sc.category_id');
			$this->db->where('sc.service_id',$id);
			$rs = $this->db->get();
			$result = $rs->result_array();
			
			$output = '';
			if(count($result)>0){
				foreach ($result as $phar){ 
					$output .= "<li>".$phar['category_name']."</li>";
				}
			}else{
				$output .= '<center><p>No Categories for this Service</p></center>';
			}
			return $output;	
		}
		public function edit_service($data, $id){
			$this->db->where('id', $id);
			$this->db->update('services', $data);
			return true;
		}
		function update_status($service_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $service_id);
			return $this->db->update('services',$data);
		}
	}

?>