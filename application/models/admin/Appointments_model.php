<?php
	class Appointments_model extends CI_Model{

		public function add_appointments($data){
			$this->db->insert('appointments', $data);
			return true;
		}
		public function get_new_appointments(){
			$this->db->select('*');
			$this->db->from('appointments');
			$this->db->where('status', 1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_appointments_by_id($id){
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->from('appointments');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_reply_appointments_by_id($id){
			$this->db->select('*');
			$this->db->where('appointment_id', $id);
			$this->db->from('contactus_replies');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		function send_reply()	 
		{
		    $appointment = $_POST['appointment'];
		    $appointment_id = $_POST['appointment_id'];
		    $email = $_POST['email'];
		    $firstname = $_POST['first_name'];
		    $lastname = $_POST['last_name'];
		    $data['email'] = $email;
		    $data['appointment'] = $appointment;
		    $data['name'] = $lastname." ".$firstname;
		    $data['appointment_id'] = $appointment_id; 

		    $data1=array(
				"appointment_id"=>$appointment_id,
				"reply_appointment"=>$appointment,
				"replier_id"=>$_SESSION['sadevelopers_admin']['admin_id'],
				"replier_name"=>$_SESSION['sadevelopers_admin']['name']
			);
			
			$this->db->insert('contactus_replies',$data1);
			
    		$email_subject = "";
			$content = $appointment;			
		    $data['name'] = $lastname." ".$firstname;
			$data['email_subject'] = $email_subject;
			$data['content'] = $content;
			$data['client_name'] = 'admin System';
			$data['emaillogoclient'] = 'admin System';
			$temp =$this->load->view('email/common_email',$data,TRUE);
    		$msg="$temp";
    		$this->email->from('info@admin.com');
    		$this->email->to($email);
    		$this->email->subject($subject);
    	    $this->email->set_mailtype('html');
    		$this->email->appointment($msg);
    		$this->email->send();
    		return "success";
    		
		}
		
		function update_status($appointment_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $appointment_id);
			return $this->db->update('appointments',$data);
		}
		public function get_appointment($id){
			$this->db->select('*');
			$this->db->from('appointments');
			$this->db->where('id', $id);
			$rs = $this->db->get();
			$appointment = $rs->row_array();
		
			$output = "<span>".$appointment['message']."</span>";
			
			return $output;	
		}
		public function get_all_appointments($status){
			$this->db->select('*');
			$this->db->from('appointments');
			$this->db->where('status', $status);
			$this->db->order_by('date','DESC');
			//$this->db->order_by("id", "desc");
			$rs = $this->db->get();
			$result = $rs->result_array();
			$output = '';
			foreach ($result as $key=>$appointment){
				if($status ==0){
					$class='confirm_booking';
					$title='Confirm Appointment';
				}elseif($status ==1){
					$class='complete_booking';
					$title='Complete Appointment';
				}else{
					$class='hide';
					$title='';
				}
				$output .= "<tr>
							<td>".$appointment['last_name']."</td>
							<td>".$appointment['first_name']." </td>
							<td>".date("m-d-Y", strtotime($appointment['date']))."&nbsp;&nbsp;".date("g:i a", strtotime($appointment['time']))." </td>
							<td>".$appointment['email']." </td>
							<td>".$appointment['phone']."</td>
							<td>".$appointment['subject']."</td>						
							<td>
							<a href='#' class='".$class."' appointment_id = ". $appointment['id'] ."><button type='button' class='btn btn-sm btn-outline-dark' data-toggle='tooltip' title='".$title."'><i class='fa fa-check' ></i></button></a>	
							<a href='#' class='removeRecord' appointment_id = ". $appointment['id'] ."><button type='button' class='btn btn-sm btn-outline-dark' data-toggle='tooltip' title='Delete'><i class='fa fa-trash' ></i></button></a> 
							<a href='#' data-toggle='tooltip' title='View Message' class='view_appointment' appointment_id = ". $appointment['id'] ."> <button type='button' class='btn btn-sm btn-outline-dark'  ><i class='fa fa-eye'></i></button> </a>
							</td>
						</tr>";
			}
			return $output;	
		}
		public function get_all_enquires1(){
		$this->db->select('*');
		$this->db->from('enquiries');
		$this->db->order_by("id", "desc");
		$rs = $this->db->get();
		$result = $rs->result_array();
		$output = '';
		foreach ($result as $key=>$appointment){
			$output .= "<tr>							
						<td>".$appointment['contact_name']."</td>
						<td>".$appointment['client_name']." </td>
						<td>".$appointment['email_address']." </td>
						<td>".$appointment['phone_number']."</td>
					</tr>";
			}			
			return $output;				
		}
	}

?>