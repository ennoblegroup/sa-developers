<?php
	class Aboutus_model extends CI_Model{

		public function add_aboutus($data){
			$this->db->insert('about_us', $data);
			return true;
		}
		public function get_all_aboutus(){
			$this->db->select('*');
			$this->db->from('about_us');
			$this->db->order_by("status", "desc");
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_aboutus(){
			$this->db->select('*');
			$this->db->from('about_us');
			$this->db->where('status',1);
			$this->db->order_by('id','desc');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_aboutus_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM about_us where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$aboutus_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM about_us where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_aboutus($data,$id){

				$id = $this->input->post('id');
			    $sql = "select * from about_us where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();				
				if ($row['title'] != $data['title'] || $row['description'] != $data['description'] ) 
				{					
					$data2['status'] = 0;
					$this->db->insert('about_us', $data);					
					$id = $this->db->insert_id();					
					$this->db->update('about_us', $data2, array('id !=' => $id));
					return true;
				}else{
					$this->db->update('about_us', $data, array('id ' => $id));
				}
				
			return true;
		}
		
		function update_status($aboutus_id, $id) {
			$data=array('status'=> $id);
            $this->db->where('id', $aboutus_id);
            $this->db->update('about_us',$data);
            $data=array('status'=> 0);
            $this->db->where('id !=', $aboutus_id);
			$this->db->update('about_us',$data);
			return true;
		}
	}

?>