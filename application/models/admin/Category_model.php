<?php
	class Category_model extends CI_Model{

		public function add_category($data){
			$this->db->insert('categories', $data);
			return true;
		}
		public function get_all_categories(){
			$this->db->select('*');
			$this->db->from('categories');
			$this->db->order_by('id','DESC');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		
		public function get_all_active_categories(){
			$this->db->select('*');
			$this->db->from('categories');
			$this->db->order_by('id','DESC');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_category_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM categories where id=$id");
			$data1   = $query->row_array();
			return $data1;
		}
		public function get_category_vendor($id){
			$roleid= $_SESSION['sadevelopers_admin']['role_id'];

			$this->db->select('*');
			$this->db->from('vendor_categories vc');
			$this->db->join('vendors v','v.id = vc.vendor_id');
			$this->db->where('vc.category_id',$id);
			$rs = $this->db->get();
			$result = $rs->result_array();
			
			$output = '';
			if(count($result)>0){
				foreach ($result as $phar){ 
					$output .= "<li>".$phar['vendor_name']."</li>";
				}
			}else{
				$output .= '<center><p>No vendors for this category</p></center>';
			}
			return $output;	
		}
		public function edit_category($data, $id){
			$this->db->where('id', $id);
			$this->db->update('categories', $data);
			return true;
		}
	}

?>