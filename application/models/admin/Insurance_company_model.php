<?php
	class Insurance_company_model extends CI_Model{

		public function add_insurance_company($data){
			$this->db->insert('insurance_companies', $data);
			return true;
		}
		public function get_all_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_insurance_companies_claim(){
			$this->db->select('*,ic.id as ic_id');
			$this->db->from('insurance_companies ic');
			$this->db->join('insurance_forms if','ic.id=if.insurance_company_id');
			$this->db->where('ic.status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_insurance_companies(){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_insurance_company_by_id($id){
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->where('id',$id);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}
		public function get_insurance_company_by_email($email){
			$array = array('email_address' => $email);
			$this->db->where($array);
			$query=$this->db->get("insurance_companies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_insurance_company_by_email_edit($email,$id){
			$array = array('email_address' => $email,'id !='=> $id);
			$this->db->where($array);
			$query=$this->db->get("insurance_companies");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function edit_insurance_company($data, $id){
			$this->db->where('id', $id);
			$this->db->update('insurance_companies', $data);
			return true;
		}
		function update_status($inscmp_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $inscmp_id);
			return $this->db->update('insurance_companies',$data);
		}		
		public function get_all_insurance_companies1(){
			$roleid= $_SESSION['sadevelopers_admin']['role_id'];
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',5);
			$this->db->where('permission_id',2);
			$status_countc = $this->db->get('roles_permissions')->num_rows();
			if($status_countc>0 || $roleid==0){
				$user_classc ='show_cls';
			}else{
				$user_classc ='hide_cls';
			}
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',5);
			$this->db->where('permission_id',3);
			$status_countu = $this->db->get('roles_permissions')->num_rows();
			if($status_countu >0 || $roleid==0){
				$user_classu ='show_cls';
			}else{
				$user_classu ='hide_cls';
			}
			$this->db->where('role_id',$roleid);
			$this->db->where('menu_id',5);
			$this->db->where('permission_id',4);
			$status_countd = $this->db->get('roles_permissions')->num_rows();
			if($status_countd >0 || $roleid==0){
				$user_classd ='show_cls';
			}else{
				$user_classd ='hide_cls';
			}
			$this->db->select('*');
			$this->db->from('insurance_companies');
			$this->db->order_by("id", "desc");
			$rs = $this->db->get();
			$result = $rs->result_array();			
			$output = '';
			foreach ($result as $inscmp){ 
			if($inscmp['status']==1) { $checked='checked'; $status="<span>Active</span>"; } else { $checked=''; $status="<span>In-Active</span>"; }
			
			$output .= "<tr>							
							<td style='margin-left:10px;'>".$inscmp['insurance_company_id']."</td>
							<td>".$inscmp['insurance_company_name']." </td>
							<td>".$inscmp['phone_number']." </td>							
							<td>  
								<label class='switch'>
									<input type='checkbox' ".$checked." id='status' inscmp_sid=". $inscmp['id'].">									  
								  <span class='slider round'></span>
								</label>
								<span id='status_roww".$inscmp['id']."'>
								". $status ."
								</span>
							</td>
							<td> 
								<a data-toggle='tooltip' title='Edit' href='". base_url('admin/insurance_companies/edit/'.$inscmp['id']) ."' class='btn btn-sm btn-outline-dark".$user_classu."'><i class='fa fa-pencil'></i></a>
								<a data-toggle='tooltip' title='Delete' href='#' inscmp_id = ". $inscmp['id'] ." class='removeRecord btn btn-sm btn-outline-dark ". $user_classd ."'><i class='fa fa-trash-o'></i></a>
							</td>
						</tr>";
			}
			
			return $output;	
		}
	}

?>