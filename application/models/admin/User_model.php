<?php
	class User_model extends CI_Model{

		public function add_user($data){
			$this->db->insert('users', $data);
			return true;
		}

		public function get_all_users(){

			$this->db->select('*,u.id as user_id,u.status as status_id');
			$this->db->from('users u');
			$this->db->join('roles r','r.id=u.user_role');
			$this->db->where('u.client_id', $_SESSION['sadevelopers_admin']['client_id']);
			$this->db->where('r.id !=',0 );
			$this->db->order_by('user_id','desc');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_users(){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_users(){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('status',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_rusers($pid){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('status',1);
			$this->db->where('client_id',$pid);
			$this->db->where('is_admin',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_inactive_rusers($pid){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('status',0);
			$this->db->where('client_id',$pid);
			$this->db->where('is_admin',0);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_user_by_id($id){
			$this->db->select('*,u.id as user_id,u.status as status_id');
			$this->db->from('users u');
			//$this->db->join('roles r','r.id=u.user_role');
			//$this->db->where('u.client_id', $_SESSION['sadevelopers_admin']['client_id']);
			$this->db->where('u.id', $id);
			$rs = $this->db->get();
			return $result = $rs->row_array();
		}
		public function get_user_by_name($uname){
			$this->db->select('count(*) as cnt');
			$this->db->from('users');
			$this->db->where('username',$uname);
			$query = $this->db->get();
			return $result = $query->row_array();
		}
		public function edit_user($data, $id){
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			return true;
		}
		public function get_user_password($password,$user_id){
			$this->db->select('count(*) as cnt');
			$this->db->from('users');
			$this->db->where('id',$user_id);
			$this->db->where('password',$password);
			$query = $this->db->get();
			return $result = $query->row_array();
		}
		public function update_status($user_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $user_id);
			return $this->db->update('users',$data);
		}
		public function update_password($data, $id) {
			//$this->db->where('id', $id);
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			return true;
		}
		public function get_user_by_email($email){
			$array = array('email' => $email);
			$this->db->where($array);
			$query=$this->db->get("users");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
		public function get_user_by_email_edit($email,$id){
			$array = array('email' => $email,'id !='=> $id);
			$this->db->where($array);
			$query=$this->db->get("users");
			if($query->num_rows()>0) {
				return $query->result_array();
			}
			return false;
		}
	}

?>