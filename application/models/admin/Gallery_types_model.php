<?php
	class Gallery_types_model extends CI_Model{

		public function add_gallery_type($data){
			$this->db->insert('gallery_types', $data);
			return true;
		}
		public function get_all_gallery_types(){
			$this->db->select('*');
			$this->db->from('gallery_types');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		
		public function get_all_active_gallery_types(){
			$this->db->select('*');
			$this->db->from('gallery_types');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_gallery_type_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM gallery_types where id=$id");
			$data1   = $query->row_array();
			return $data1;
		}

		public function edit_gallery_type($data, $id){
			$this->db->where('id', $id);
			$this->db->update('gallery_types', $data);
			return true;
		}
	}

?>