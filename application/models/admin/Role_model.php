<?php
	class Role_model extends CI_Model{

		public function add_role($data){
			$this->db->insert('roles', $data);
			return true;
		}

		public function get_all_roles(){
			$query = $this->db->get_where('roles', array('status' => 1,'client_id'=>$_SESSION['sadevelopers_admin']['client_id'],'id !=' => 0));
			//$this->db->order_by('id','desc');
			return $result = $query->result_array();
		}
		public function get_all_airoles(){
			$query = $this->db->get_where('roles', array('client_id'=>$_SESSION['sadevelopers_admin']['client_id'],'id !=' => 0));
			//$this->db->order_by('id','desc');
			return $result = $query->result_array();
		}
		public function get_role_by_id($id){
			$query = $this->db->get_where('roles', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_role($data, $id){
			$this->db->where('id', $id);
			$this->db->update('roles', $data);
			return true;
		}
		public function get_all_menues(){
			$this->db->select('*');
			$this->db->from('menus');
			if($_SESSION['sadevelopers_admin']['client_id']>0){
				$this->db->where('client_id', 1);
			}
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_permissions(){
			$query = $this->db->get('permissions');
			return $result = $query->result_array();
		}
		public function get_menu_permissions($menu_id){
			//return $this->db->query("select p.id,p.name,mp.id,mp.menu_id from permissions p,menus_permissions mp where  mp.role_id=$role_id and mp.permission_id=p.id")->result_array();
			return $this->db->query("select p.id,p.name,mp.menu_id from permissions p,menus_permissions mp where  mp.menu_id=$menu_id and mp.permission_id=p.id")->result_array();
			/*$this->db->select('*');
			$this->db->from('menus_permissions');
			$this->db->where('role_id', $roleid);
			$rs = $this->db->get();			
			return $result = $rs->result_array();*/
		}
		public function add_role_permission($data){
			$this->db->insert('roles_permissions', $data);
			return true;
		}
		public function get_roles_permissions($roleid){
			$this->db->select('*');
			$this->db->from('roles_permissions');
			$this->db->where('role_id', $roleid);
			$rs = $this->db->get();			
			return $result = $rs->result_array();
		}
		public function get_menu_mapped_permissions(){
			$menu_id = $_POST['menu_id'];
			//$role_id = $_POST['role_id'];
			
			//return $this->db->query("select p.id,p.name,mp.id from permissions p,menus_permissions mp where mp.menu_id=$menu_id and mp.role_id=$role_id and mp.permission_id=p.id")->result_array();
			return $this->db->query("select p.id,p.name from permissions p,menus_permissions mp where mp.menu_id=$menu_id and mp.permission_id=p.id")->result_array();
		}
		public function update_status($role_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $role_id);
			return $this->db->update('roles',$data);
		}
		public function get_role_by_name($rname){
			$this->db->select('*');
			$this->db->from('roles');
			$this->db->where('rolename',$rname);
			$this->db->where('client_id',$_SESSION['sadevelopers_admin']['client_id']);
			$query = $this->db->get();
			if($query->num_rows()>0) {
				return $query->row_array();
			}
			return false;
		}
		public function get_role_by_name_edit($rname,$id){
			$array = array('rolename' => $rname,'id !='=> $id);
			$this->db->select('*');
			$this->db->from('roles');
			$this->db->where($array);		
			$this->db->where('client_id',$_SESSION['sadevelopers_admin']['client_id']);
			$query = $this->db->get();
			if($query->num_rows()>0) {
				return $query->row_array();
			}
			return false;
		}
	}

?>