<?php
	class Features_model extends CI_Model{

		public function add_features($data){
			$this->db->insert('features', $data);
			return true;
		}
		public function get_all_features(){
			$this->db->select('*');
			$this->db->from('features');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_all_active_features(){
			$this->db->select('*');
			$this->db->from('features');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_features_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM features where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$features_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM features where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_features($data, $id){
			$this->db->where('id', $id);
			//print_r($data);die;
			$this->db->update('features', $data);
			return true;
		}
		
		function update_status($features_id, $id) {
			$data=array('status'=> $id);
			$this->db->where('id', $features_id);
			return $this->db->update('features',$data);
		}
	}

?>