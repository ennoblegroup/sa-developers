<?php
	class Website_model extends CI_Model{

		public function add_website($data){
			$this->db->insert('ci_websites', $data);
			return $this->db->insert_id();
		}
		public function add_documents($data){
			$this->db->insert('ci_documents', $data);
			return true;
		}
		public function get_all_websites(){
			$this->db->select('*');
			$this->db->from('ci_websites');
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		public function get_website_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM ci_websites where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$website_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM ci_websites where id=$id")->row_array();
				$data1[$key]['all_documents']   = $this->db->query("SELECT * FROM ci_documents where website_id=$website_id")->result_array();
			}
			return $data1;
		}

		public function edit_website($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_websites', $data);
			return true;
		}

	}

?>