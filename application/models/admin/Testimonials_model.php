<?php
	class Testimonials_model extends CI_Model{

		public function add_testimonial($data){
			$this->db->insert('messages', $data);
			return true;
		}
		public function get_all_projects(){
			$this->db->select('*');
			$this->db->from('projects');
			if($_SESSION['sadevelopers_admin']['client_id'] !=0){
				$this->db->where('client_id',$_SESSION['sadevelopers_admin']['client_id']);
			}
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}

		public function get_all_active_testimonials(){
			$this->db->select('*');
			$this->db->from('testimonials');
			$this->db->where('status',1);
			$rs = $this->db->get();
			return $result = $rs->result_array();
		}
		public function get_testimonials_by_id($id){
			$data1 = array();
			$query=$this->db->query("SELECT * FROM testimonials where id=$id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$testimonials_id= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM testimonials where id=$id")->row_array();			}
			return $data1;
		}

		public function edit_testimonials($data, $id){
			$this->db->where('id', $id);
			$this->db->update('testimonials', $data);
			return true;
		}
		public function get_all_testimonials(){
			$this->db->select('*');
			$this->db->from('messages ');
			$this->db->order_by("message_id", "desc");
			$this->db->where('project_id >', 0);
			$rs = $this->db->get();
			$result = $rs->result_array();
			$output = '';
			$client_id= $_SESSION['sadevelopers_admin']['client_id'];
			if($client_id > 0){
				$admin_class="hide_cls";
			}else{
				$admin_class="show_cls";
			}
			foreach ($result as $key=>$message){
				$pid = $message['project_id'];
				if($message['review_status']==1) { $checked='checked'; $status="<span>Approved</span>"; } else { $checked=''; $status="<span>Rejected</span>"; }
				$project_name= $this->db->query("SELECT project_name FROM projects where id=$pid")->row_array();			
				$output .= "<tr>							
							<td>".$message['message_id']."</td>
							<td>".$project_name['project_name']."</td>
							<td>".$message['last_name']." ".$message['first_name']." </td>
							<td>".$message['email']." </td>
							<!--td>".$message['phone']."</td-->
							<td>".$message['subject']."</td>
							<td class='line-clamp'>".$message['message']."</td>
							<td class='".$admin_class."'>  
								<label class='switch' style='top:11px;'>
								  <input class='slider round permission_status' type='checkbox' ".$checked." id='status' testimonial_id=".$message['message_id'].">
								  <span class='slider round'></span>
								</label>
								<span class='scl' id='status_roww". $message['message_id'] ."'>
								". $status ."
								</span>
							</td>
							<td> 
							<a href='#' class='removeRecord' testimonial_id = ". $message['message_id'] ."><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-trash' data-toggle='tooltip' title='Delete'></i></button></a> 
							<!--a href='". base_url('admin/messages/view_message/'.$message['message_id']) ."'> <button type='button' class='btn btn-sm btn-outline-dark'  ><i class='fa fa-eye'></i></button> </a-->
							</td>
						</tr>";
			}
			return $output;	
		}
		function update_status($testimonial_id, $id) {
			$data=array('review_status'=> $id);
			$this->db->where('message_id', $testimonial_id);
			return $this->db->update('messages',$data);
		}
	}

?>