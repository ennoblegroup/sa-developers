<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head> 
  <meta charset="UTF-8"> 
  <meta content="width=device-width, initial-scale=1" name="viewport"> 
  <meta name="x-apple-disable-message-reformatting"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta content="telephone=no" name="format-detection"> 
  <title>Branchbrook client</title> 
  <?php $contact_view=$this->Admin_model->contact_branch(); ?>
 </head> 
 <body> 
  <table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0" style="border:2px solid #3B9CCA" align="center" >
	<tbody>
		<tr style="border-collapse:collapse">
			<td class="m_-5931077089181368027hideMe" width="640" height="30" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
				<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
					<tbody>
						<tr style="border-collapse:collapse">
							<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
							<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr style="border-collapse:collapse">
			<td class="m_-5931077089181368027w100" width="100%" align="left" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse">
				<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
					<tbody>
						<tr style="border-collapse:collapse">
							<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
							<td class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
								<table class="m_-5931077089181368027w100" width="240" cellpadding="0" cellspacing="0" border="0" align="left">
									<tbody>
										<tr style="border-collapse:collapse">
											<td align="left" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
												<a href="" style="color:rgb(0,67,123);text-decoration:underline" target="_blank" data-saferedirecturl="">
													<img alt="BranchBrook client" width="220" height="70" src="<?php echo base_url();?>images/logo.png" class="m_-5931077089181368027headerImage CToWUd" border="0" align="top" style="outline:none;text-decoration:none;display:block;border:0px;width:220px;height:70px;background-color:rgb(255,255,255);color:rgb(102,89,77);font-weight:bold;font-size:16px;font-family:Arial,sans-serif;background-position:initial initial;background-repeat:initial initial">
													</a>
												</td>
											</tr>
											<tr style="border-collapse:collapse">
												<td height="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
											</tr>
										</tbody>
									</table>
									<table class="m_-5931077089181368027w100" width="370" cellpadding="0" cellspacing="0" border="0" align="left">
										<tbody>
											<tr style="border-collapse:collapse">
												<?php foreach($contact_view as $contact) {?>
												<td align="right" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
													<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:11px;line-height:15px;color:rgb(102,102,102);margin-top:0px;font-family:Arial,sans-serif">
														<br style="line-height:11px"><?php echo $contact['address1']?>,
															<br style="line-height:11px"><?php echo $contact['city']?>,
															<br style="line-height:11px"><?php echo $contact['state']." ".$contact['zip']."."?>
																<span class="m_-5931077089181368027Apple-converted-space"></span>
																<br style="line-height:11px">Phone: <?php echo $contact['phone']?>
																</font>
															</td>
															<?php }?>
														</tr>
														<tr style="border-collapse:collapse">
															<td height="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
														</tr>
													</tbody>
												</table>
											</td>
											<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr style="border-collapse:collapse">
							<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
								<table class="m_-5931077089181368027w100" cellpadding="0" cellspacing="0" border="0" bgcolor="#3B9CCA">
									<tbody>
										<tr style="border-collapse:collapse">
											<td height="45" valign="middle" align="center" width="640" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
												<font face="Arial,sans-serif" color="#ffffff" style="font-size:22px">Thanks for reaching out to <span style="color:#ffffff"><?php echo base_url()?></span></font>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr style="border-collapse:collapse">
							<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
						</tr>
						<tr style="border-collapse:collapse">
							<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
								<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
									<tbody>
										<tr style="border-collapse:collapse">
											<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
											<td class="m_-5931077089181368027w80" width="620" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
												<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">Dear,<?php echo $name;?>
													<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
													<br style="line-height:12px">
														<br style="line-height:12px"><?php echo $message;?>
															<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
															
														</font>
													</td>
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr style="border-collapse:collapse">
									<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
								</tr>
								
								
								<!--<tr style="border-collapse:collapse">
									<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
										<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
											<tbody>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w80" width="620" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<font face="Arial,sans-serif" size="3" color="#00437b" class="m_-5931077089181368027article-content-title" style="font-size:14px;line-height:18px;color:rgb(0,67,123);margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">
															<b style="line-height:14px">Frequently asked questions</b>
														</font>
													</td>
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w80" height="5" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w80" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<table class="m_-5931077089181368027w100" width="620" cellpadding="0" cellspacing="0" border="0">
															<tbody>
																<tr style="border-collapse:collapse">
																	<td width="20" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td width="15" align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">1.</font>
																	</td>
																	<td width="5" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">No website registration required</font>
																	</td>
																</tr>
																<tr style="border-collapse:collapse">
																	<td width="20" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td width="15" align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">2.</font>
																	</td>
																	<td width="5" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">No username/password to remember</font>
																	</td>
																</tr>
																<tr style="border-collapse:collapse">
																	<td width="20" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td width="15" align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">3.</font>
																	</td>
																	<td width="5" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td align="left" valign="top" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:18px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">View &amp; pay your bill right from your email inbox</font>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w80" width="620" height="20" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>-->
								
								
								<tr style="border-collapse:collapse">
									<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
								</tr>
								<tr style="border-collapse:collapse">
									<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
										<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
											<tbody>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
													<td class="m_-5931077089181368027w80" width="620" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<font face="Arial,sans-serif" size="2" class="m_-5931077089181368027article-content" style="font-size:12px;line-height:0px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">
															<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																<br style="line-height:12px">Thank you,
																	<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																		<br style="line-height:12px">Branchbrook client
																		</font>
																	</td>
																	<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<table class="m_-5931077089181368027w100" cellpadding="0" cellspacing="0" border="0" bgcolor="#3B9CCA">
															<tbody>
																<tr style="border-collapse:collapse">
																	<td height="5" width="640" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="#ffffff" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												<tr style="border-collapse:collapse">
													<td class="m_-5931077089181368027w100" width="640" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
															<tbody>
																<tr style="border-collapse:collapse">
																	<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																	<td class="m_-5931077089181368027w80" width="620" align="left" style="font-family:Arial,sans-serif;border-collapse:collapse">
																		<font face="Arial,sans-serif" size="1" class="m_-5931077089181368027footer-content" style="font-size:11px;line-height:15px;margin-top:0px;margin-bottom:15px">Your email was received at 
																			<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																			<?php $admin_view = $this->db->get_where('users',array('user_id'=>$this->session->userdata('user_id')))->result_array();?>
																			<?php foreach($admin_view as $admin) {?>
																			<a href="mailto:<?php echo $admin['email']?>" style="text-decoration:none;font-family:arial" target="_blank"><?php echo $admin['email']?></a>
																			<?php }?>
																			<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																			<br style="line-height:11px">
																				<br style="line-height:11px">This is an auto-generated reply in response to your email sent to us. 
																					<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																					<br style="line-height:11px">
																						<br style="line-height:11px">For more information, please visit
																							<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																							<a href="<?php echo base_url()?>" style="color:rgb(0,67,123);font-size:11px;text-decoration:underline" target="_blank" data-saferedirecturl="<?php echo base_url()?>">Branchbrook client</a>.
																							<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
																							</font>
																									</td>
																									<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																				<tr style="border-collapse:collapse">
																					<td height="30" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
																				</tr>
																			</tbody>
																		</table>
         </body>
        </html>