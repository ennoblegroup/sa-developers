<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <?php $contact = $this->db->get('contact')->row_array(); ?>
<head> 
<meta charset="UTF-8"> 
<meta content="width=device-width, initial-scale=1" name="viewport"> 
<meta name="x-apple-disable-message-reformatting"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta content="telephone=no" name="format-detection"> 
<title><?php echo $client_name; ?></title> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head> 
 <style>
    .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    }
    ul.header-social {
    text-align: right;
    padding: 4px 0;
    list-style: none none !important;
    }

 </style>
	<body> 
		<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0" style="border:8px solid #70bfea;background:#e9f6fc" align="center" >
			<tbody>
				<tr style="border-collapse:collapse">
					<td height="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
				</tr>
				<tr style="border-collapse:collapse">
					<td class="m_-5931077089181368027w100" width="100%" align="center" bgcolor="" style="font-family:Arial,sans-serif;border-collapse:collapse">
						<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
							<tbody>
								<tr style="border-collapse:collapse">
									<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
									<td class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
										<table class="m_-5931077089181368027w100" width="240" cellpadding="0" cellspacing="0" border="0" align="center">
											<tbody>
												<tr style="border-collapse:collapse">
													<center><td align="" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
														<a href="" style="color:rgb(0,67,123);text-decoration:underline" target="_blank" data-saferedirecturl="">
															<img alt="Logo" src="http://ennobledemos.com/branchbrook/img/banner/logo.jpg" style="padding:40px 0px;width:525px"></center>
														</a>
													</td></center>
												</tr>
												<tr style="border-collapse:collapse">
													<td height="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
												</tr>
												</tbody>
										</table>
									</td>
									<td width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="border-collapse:collapse">
					<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
						<table class="m_-5931077089181368027w100" cellpadding="0" cellspacing="0" border="0" bgcolor="#16348e">
							<tbody>
								<tr style="border-collapse:collapse">
									<td height="45" valign="middle" align="center" width="640" class="m_-5931077089181368027w100" style="font-family:Arial,sans-serif;border-collapse:collapse">
										<font face="Arial,sans-serif" color="#ffffff" style="font-size:22px"><?php echo $email_subject; ?></font>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="border-collapse:collapse">
					<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
				</tr>
				<tr style="border-collapse:collapse">
					<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
						<table class="m_-5931077089181368027w100" cellpadding="0" cellspacing="0" border="0" style="width:100%">
							<tbody>
								<tr style="border-collapse:collapse">
									<div class="row">
								        <div class="well col-xs-12 col-sm-12 col-md-12">
								            <?php if($this->session->userdata("review_data")['delivery_type']==1){?>
								              <p><b>Delivery Type:</b> Door Delivery</p>
								            <?php } else {?>
								             <p><b>Delivery Type:</b> Pickup at store</p>
								            <?php }?>
								            <?php if($this->session->userdata("review_data")['delivery_type']==1){?>
								            <div class="row">
								                <div class="col-xs-6 col-sm-6 col-md-6">
								                    <h1>Shipping Address</h1>
								                    <address>
								                        <strong><?php echo $this->session->userdata("review_data")['shippinglname'];?> <?php echo $this->session->userdata("review_data")['shippingfname'];?></strong>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['shippingaddress1'];?>
								                        <?php if($this->session->userdata("review_data")['shippingaddress2'] != ''){?>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['shippingaddress2'];?>
								                        <?php }?>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['shippingcity'];?>, <?php echo $this->session->userdata("review_data")['shippingstate'];?> <?php echo $this->session->userdata("review_data")['shippingzip'];?>
								                        <br>
								                    </address>
								                </div>
								                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
								                    <h1>admin Address</h1>
								                    <address>
								                        <strong><?php echo $this->session->userdata("review_data")['adminlname'];?> <?php echo $this->session->userdata("review_data")['adminfname'];?></strong>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['adminaddress1'];?>
								                        <?php if($this->session->userdata("review_data")['adminaddress2'] != ''){?>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['adminaddress2'];?>
								                        <?php }?>
								                        <br>
								                        <?php echo $this->session->userdata("review_data")['admincity'];?>, <?php echo $this->session->userdata("review_data")['adminstate'];?> <?php echo $this->session->userdata("review_data")['adminzip'];?>
								                        <br>
								                    </address>
								                </div>
								            </div>
								            <?php }?>
								            <div class="row">
								                <div class="col-xs-12 col-sm-12 col-md-12">
								                    <div class="text-center">
								                        <h1>Order Details</h1>
								                    </div>
								                    <table class="table table-hover">
								                        <thead>
								                            <tr>
								                                <th>Product Name</th>
								                                <th>Units</th>
								                                <th class="text-center">Price</th>
								                                <th class="text-center">Total</th>
								                            </tr>
								                        </thead>
								                        <tbody>
								                        <?php foreach($this->session->userdata("cart_dataa") as $data){ ?>
								                            <tr class="p">
								                                <td class="col-md-9"><h6><?php echo $data['name']?></h6></td>
								                                <td class="col-md-1" style="text-align: center"><?php echo $data['qty']?></td>
								                                <td class="col-md-1 text-center">$<?php echo $data['price']?></td>
								                                <td class="col-md-1 text-center rrr pricesubtotalv">$<?php echo $data['subtotal']?></td>
								                            </tr>
								                        <?php }?>	
								
								                            <tr>
								                                <td></td>
								                                <td></td>
								                                <td class="text-right">
								                                <p>
								                                    <strong>Total: </strong>
								                                </p>
								                                <p>
								                                    <strong>Tax: </strong>
								                                </p>
								                                <p>
								                                    <strong>Discount: </strong>
								                                </p>
								                                <p>
								                                    <strong>Shipping: </strong>
								                                </p>    
								                                </td>
								                                <td class="text-center">
								                                    <p>
								                                        <strong class="total"><?php echo sprintf("%.2f",$this->session->userdata("total")); ?></strong>
								                                    </p>
								                                    <p>
								                                        <strong class="tax">$<?php echo sprintf("%.2f",$this->session->userdata("tax")); ?></strong>
								                                    </p>
								                                    <p>
								                                        <strong class="tax">$<?php echo sprintf("%.2f",$this->session->userdata("discountt")); ?></strong>
								                                    </p>
								                                    <p>
								                                        <strong class="tax">$<?php echo sprintf("%.2f",$this->session->userdata("shipping")); ?></strong>
								                                    </p>
								                                </td>
								                            </tr>
								                            <tr>
								                                <td>   </td>
								                                <td>   </td>
								                                <td class="text-right"><h4><strong>Total: </strong></h4></td>
								                                <td class="text-center text-danger"><h4><strong class="final"><?php echo sprintf("%.2f",$this->session->userdata("final")); ?></strong></h4></td>
								                            </tr>
								                        </tbody>
								                    </table>
								                </div>   
								            </div>
								        </div>
								    </div>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr style="border-collapse:collapse">
					<td class="m_-5931077089181368027w100" width="640" height="20" bgcolor="" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
				</tr>
				<tr style="border-collapse:collapse;background:url('https://lh3.googleusercontent.com/-FLK1v6HDXtE/XXc9-HlS7qI/AAAAAAAAD1o/xX_1HGloklAjEJUqRd8wLA47iaG8mSQpACK8BGAs/s0/2019-09-09.jpg');">
					<td class="m_-5931077089181368027w100" width="640" style="font-family:Arial,sans-serif;border-collapse:collapse">
						<table class="m_-5931077089181368027w100" width="640" cellpadding="0" cellspacing="0" border="0">
							<tbody>
								<tr style="border-collapse:collapse" class="">
									<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
									<td class="m_-5931077089181368027w80" width="620" align="left" style="border-radius: 1.25rem;font-family:Arial,sans-serif;border-collapse:collapse">
										<font face="Arial,sans-serif" size="1" class="m_-5931077089181368027footer-content" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">Your email was received at 
											<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
											<a href="mailto:<?php echo $contact['email']; ?>" style="text-decoration:none;font-family:arial" target="_blank"><?php echo $contact['email']; ?></a>
										</font><br>
										<font face="Arial,sans-serif" size="1" class="m_-5931077089181368027footer-content" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">This is an auto-generated reply in response to your email sent to us. 
										</font><br>
										<font face="Arial,sans-serif" size="1" class="m_-5931077089181368027footer-content" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif">For more information, please visit
											<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
											<a href="http://ennobledemos.com/branchbrook" style="color:rgb(0,67,123);font-size:11px;text-decoration:underline" target="_blank" data-saferedirecturl="http://ennobledemos.com/branchbrook/"><?php echo $client_name; ?></a>.
											<span class="m_-5931077089181368027Apple-converted-space">&nbsp;</span>
										</font>
									</td>
								</tr>
								<tr height="45"style="border-collapse:collapse">
								</tr>
								<tr style="border-collapse:collapse" class="">
									<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
									<td class="m_-5931077089181368027w80" width="620" align="left" style="border-radius: 1.25rem;font-family:Arial,sans-serif;border-collapse:collapse">
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><img src="http://ennobledemos.com/branchbrook/img/flocation.png" style="padding-right:4px;"><?php echo $contact['address1'];?>
										</font><br>
										<?php if($contact['address2'] != ''){?>
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 45px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><?php echo $contact['address2'];?>
										</font><br>
										<?php }?>
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 45px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><?php echo $contact['city'];?> ,<?php echo $contact['state'];?> <?php echo $contact['zip'];?>.
										</font><br>
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><img src="http://ennobledemos.com/branchbrook/img/ftele.png" style="padding-right:4px;"><?php echo $contact['phone'];?>
										</font><br>
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><img src="http://ennobledemos.com/branchbrook/img/ffax.png" style="padding-right:4px;"><?php echo $contact['fax_number'];?>
										</font><br>
										<font face="Arial,sans-serif" size="1" color="#666666" class="m_-5931077089181368027article-content-small" style="font-size:14px;font-weight: 600;line-height:18px;padding:10px 25px;margin-top:0px;margin-bottom:18px;font-family:Arial,sans-serif"><img src="http://ennobledemos.com/branchbrook/img/femail.png" style="padding-right:4px;"><?php echo $contact['email'];?>
										</font><br><br>
									</td>
								</tr>
								<tr style="border-collapse:collapse" class="">
									<td class="m_-5931077089181368027w5" width="10" style="font-family:Arial,sans-serif;border-collapse:collapse"></td>
									<td class="m_-5931077089181368027w80" width="620" align="left" style="border-radius: 1.25rem;font-family:Arial,sans-serif;border-collapse:collapse">
										<ul class="header-social" style="list-style: none none !important;">
                                            <li>
                                                <a href="<?php echo $contact['facebook'];?>" target="_blank" title="facebook" bis_skin_checked="1">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $contact['twitter'];?>" target="_blank" title="twitter" bis_skin_checked="1">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $contact['instagram'];?>" target="_blank" title="instagram" bis_skin_checked="1">
                                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                                </a>
                                            </li>
											<!-- https://www.linkedin.com/company/branch-brook-client1 -->
                                            <li>
                                                <a href="<?php echo $contact['linkedin'];?>" target="_blank" title="linkedin" bis_skin_checked="1">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                </a>
                                            </li>
											<li>
                                                <a href="<?php echo $contact['pinterest'];?>" target="_blank" title="pinterest" bis_skin_checked="1">
                                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                                </a>
                                            </li>
											<li>
                                                <a href="<?php echo $contact['youtube'];?>" target="_blank" title="youtube" bis_skin_checked="1">
                                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                        </ul>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>