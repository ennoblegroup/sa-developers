<?php echo form_open(base_url('admin/services/edit/'.$service['id']), 'id="quickForm" enctype="multipart/form-data" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/services">Services</a></li>
							<li class="breadcrumb-item active">Edit Service</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>       
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/services" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Service Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; ?>
									<div class="row">																
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="service_name" placeholder="Service" value="<?php echo $service['service_name']?>" name="service_name" maxLength="25">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<select  title="Select Product Categories" multiple data-live-search="true"  class="selectpicker form-control" id="category_id" name="category_id[]">
												<?php $i=1; foreach($all_categories as $row): ?>
												<option value="<?= $row['id']; ?>"><?= $row['category_name']; ?> </option>
												<?php $i++; endforeach; ?>
											</select>
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control editor" id="service_description" rows="5" style="height:auto !important;" placeholder="Description" name="service_description" ><?php echo $service['service_description']?></textarea>
										</div>	
										<div class="form-group col-lg-4 col-md-4"></div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="input-group err">
												<div class="custom-file">
													<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg">
													<label class="custom-file-label">Choose file</label>
												</div>
											</div>
											<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
												<img id="blah" style="width:100%" src="<?php echo base_url();?>images/services/<?php echo $service['image']; ?>" alt="your image" />  
											</div>
                                     	</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
<?php foreach($service['categories'] as $pp){ ?>
var sid = "<?php echo $pp['category_id']?>";
$("#category_id option[value="+sid+"]").attr('selected','selected');
<?php }?>
</script>
<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
  
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      service_name: {
        required: true,
        minlength: 3
      },
      service_description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
      service_name: {
        required: "Please enter Service Name",
        minlength: "Service Name must be at least 3 characters long"
      },
      service_description: {
        required: "Please enter Service Description",
        minlength: "Service Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#service_name', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
});
</script>
<script>
$("#nav_services").addClass('active');
</script> 