<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',4);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',4);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',4);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',4);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<style>
.line-clamp {
	white-space: nowrap;
        overflow: hidden;
		text-overflow: ellipsis;
		max-width:300px;
}
.jj {
    text-decoration: underline;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			 <div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Services</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>                    
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/services/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Service</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Services</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>
										<th>Service ID</th>
										<th>Service </th>
										<th>Product Categories</th>
										<th>Description</th>
										<th>Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_services as $key=>$srv){ 
										
									?>
									<tr>
										<td> <?php echo $srv['service_id']?> </td>
										<td> <?php echo $srv['service_name']?> </td>
										<td class="jj"><a href="#" service_id = "<?php echo $srv['id'];?>" class="category_list">See List</a></td>
										<td class="line-clamp"> <?php echo $srv['service_description']?></td>
										<td class="<?php echo $user_classs;?>"> 
											<label class="switch">
											  <input type="checkbox" <?php if($srv['status']==1){echo 'checked';}?> id="status" service_sid="<?php echo $srv['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $srv['id']; ?>">
											<?php if($srv['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>

											<?php }?>
											</span>
										</td>
										<td> 
										<!--a href="<?= base_url('admin/services/edit/'.$srv['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
										<a href="#" class="removeRecord" service_id = "<?php echo $srv['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></span></a--> 
										<a data-toggle="tooltip" title="Edit" href="<?= base_url('admin/services/edit/'.$srv['id']); ?>" class="btn btn-outline-dark <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></a>
										<a data-toggle="tooltip" title="Delete" href="#" service_id = "<?php echo $srv['id'];?>" class="removeRecord btn btn-outline-dark <?php echo $user_classd;?>"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="vend">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title eew">Category List</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<ul id="qw">
								
								</ul>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$('body').on('click', '.category_list', function () {
	var id = $(this).attr('service_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/services/get_service_category');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#qw').html(data);		
		}
	});
    $('#vend').modal('show');
});
$(document).ready(function() {
	$('body').on('click', '#status', function () {
		if($(this).prop("checked") == true){
			var id = 1;
		}
		else {
			var id = 0;
		}
		var service_id = $(this).attr('service_sid');
		$.ajax({
		  type: "POST",
		  url:"<?php echo base_url('admin/services/update_status');?>",
		  data: {id: id, service_id: service_id},
		  cache: false,
		  success: function(data){
			  $("#status_roww"+service_id).load(" #status_roww"+service_id);
		  }
		});
	});
});
</script>
<script>
 $(".removeRecord").click(function(){
       var id = $(this).attr('service_id');    	
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: '<i class="fa fa-check" aria-hidden="true"></i>Confirm',
            btnClass: 'form btn btn-red',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/services/del');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: '<i class="fa fa-ban"></i>cancel',
            btnClass: 'form btn btn-grey',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
    });
</script>
<script type="text/javascript">
    /*$(".removeRecord").click(function(){
       var id = $(this).attr('service_id');
       swal({
        title: "Are you sure?",
        text: "Do you really want to delete the service? This process cannot be undone.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-outline-dark",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!"
     }). then((result) => {
        if (result.value) {
         $.ajax({
			 url:"<?php echo base_url('admin/services/del');?>",
			type:'POST',			
			data:{'id':id},
			success: function(data){
				//swal("Deleted!", "Service has been deleted.", "success");
				location.reload();
				}
			});
        } else {
          swal("Cancelled", "Service is safe :)", "error");
        }
      })
     
    });*/
</script>
<script>
/*	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});*/
</script>
<script>
$("#nav_services").addClass('active');
</script> 