<!--**********************************
	Sidebar start
***********************************-->
<script src="https://kit.fontawesome.com/780a4d076b.js" crossorigin="anonymous"></script>
<div class="deznav">
	<div class="deznav-scroll">
		<ul class="metismenu " id="menu">

			<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>" class="ai-icon" aria-expanded="false">
					<i class="fa fa-dashboard"  aria-hidden="true"></i>
					<span class="nav-text">Dashboard</span>
				</a>
			</li>
			<?php } else {?>
			<li>
				<a href="<?= base_url('admin/dashboard'); ?>" class="ai-icon" aria-expanded="false">
					<i class="fa fa-dashboard"  aria-hidden="true"></i>
					<span class="nav-text">Dashboard</span>
				</a>
			</li>
			<?php }?>
			
			
			<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
			<li class="nav-label first">Vendor Management</li>			
			<li <?php if($this->uri->segment('2')=='categories') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fa fa-cc" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Product Categories</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='categories' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='categories' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/categories'); ?>">Product Categories</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='categories' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/categories/add'); ?>">Add Product Category</a></li>
					
				</ul>
			</li>
			<li <?php if($this->uri->segment('2')=='vendors') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fas fa-building" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Vendors</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='vendors' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='vendors' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/vendors'); ?>">Vendors</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='vendors' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/vendors/add'); ?>">Add Vendor</a></li>
					
				</ul>
			</li>
			<li <?php if($this->uri->segment('2')=='products') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Products</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='products' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='products' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/products'); ?>">Products</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='products' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/products/add'); ?>">Add Product</a></li>
					
				</ul>
			</li>
			<?php }?>

			<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
			<li class="nav-label first">Client Management</li>
			<li <?php if($this->uri->segment('2')=='clients') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Clients</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='clients' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='clients' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/clients'); ?>">Clients</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='clients' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/clients/add'); ?>">Add Client</a></li>
					
				</ul>
			</li>
			<li <?php if($this->uri->segment('2')=='projects') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fa fa-building" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Projects</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/projects'); ?>">Projects</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/projects/add'); ?>">Add Project</a></li>
				</ul>
			</li>
			<?php }?>

			<li class="nav-label first">Admin Management</li>
			<?php if( $_SESSION['sadevelopers_admin']['client_id']!=0){?>			
			<li>
				<a <?php if(($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='add') ||($this->uri->segment('2')=='projects' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/projects'); ?>">
					<i class="fa fa-building" aria-hidden="true"></i>
					<span class="nav-text">Projects</span>
				</a>
			</li>
			<?php }?>
			<li>
				<a <?php if(($this->uri->segment('2')=='testimonials' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='testimonials' && $this->uri->segment('3')=='add') ||($this->uri->segment('2')=='testimonials' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/testimonials'); ?>">
					<i class="fa fa-text-width" aria-hidden="true"></i>
					<span class="nav-text">Testimonials</span>
				</a>
			</li>
			<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
						
			<li <?php if($this->uri->segment('2')=='services') { ?> class="mm-active" <?php }?>>
				<a class="has-arrow ai-icon " href="javascript:void()" aria-expanded="false">
					<i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;&nbsp;
					<span class="nav-text">Services</span>
				</a>
				<ul aria-expanded="false">
					<li class=""><a <?php if(($this->uri->segment('2')=='services' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='services' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/services'); ?>">Services</a></li>
					<li class=""><a <?php if(($this->uri->segment('2')=='services' && $this->uri->segment('3')=='add')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/services/add'); ?>">Add Service</a></li>
					
				</ul>
			</li>
			<li>
				<a <?php if(($this->uri->segment('2')=='document_types' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='document_types' && $this->uri->segment('3')=='add') ||($this->uri->segment('2')=='document_types' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/document_types'); ?>">
					<i class="fa fa-files-o" aria-hidden="true"></i>
					<span class="nav-text">Document Types</span>
				</a>
			</li>
			<li>
				<a <?php if(($this->uri->segment('2')=='gallery_types' && $this->uri->segment('3')=='') || ($this->uri->segment('2')=='gallery_types' && $this->uri->segment('3')=='add') ||($this->uri->segment('2')=='gallery_types' && $this->uri->segment('3')=='edit')) { echo 'class="mm-active"'; }?> href="<?= base_url('admin/gallery_types'); ?>">
					<i class="fa fa-files-o" aria-hidden="true"></i>
					<span class="nav-text">Gallery Types</span>
				</a>
			</li>
			<?php }?>			

			<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
			<li class="nav-label">Website Management</li>
			<li <?php if($this->uri->segment('2')=='slides') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/slides'); ?>" class="ai-icon <?php echo $user_classss;?>" aria-expanded="false">
					<i class="fab fa-slideshare"></i>&nbsp;&nbsp;
					<span class="nav-text">Sliders</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='aboutus') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/aboutus'); ?>" class="ai-icon <?php echo $user_classau;?>" aria-expanded="false">
					<i class="fa fa-address-card"></i>&nbsp;
					<span class="nav-text">About Us</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='team') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/team'); ?>" class="ai-icon <?php echo $user_classci;?>" aria-expanded="false">
					<i class="fa fa-users" aria-hidden="true"></i>&nbsp;
					<span class="nav-text">Team</span>
				</a>
			</li>
			<li>
				<a href="<?= base_url('admin/homeservices'); ?>" class="ai-icon <?php echo $user_classts;?>" aria-expanded="false">
					<i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;
					<span class="nav-text">Services</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='appointments') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/appointments'); ?>" class="ai-icon <?php echo $user_classm;?>" aria-expanded="false">
					<i class="fas fa-calendar-check"></i>&nbsp;&nbsp;
					<span class="nav-text">Appointments</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='messages') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/messages'); ?>" class="ai-icon <?php echo $user_classm;?>" aria-expanded="false">
					<i class="fas fa-address-book"></i>&nbsp;&nbsp;
					<span class="nav-text">All Contact Messages</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='contactus') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/contactus'); ?>" class="ai-icon <?php echo $user_classci;?>" aria-expanded="false">
					<i class="fas fa-address-card"></i>&nbsp;
					<span class="nav-text">Contact Info</span>
				</a>
			</li>
			<li <?php if($this->uri->segment('2')=='gallery') { ?> class="mm-active" <?php }?>>
				<a href="<?= base_url('admin/gallery'); ?>" class="ai-icon" aria-expanded="false">
					<i class="fas fa-address-card"></i>&nbsp;
					<span class="nav-text">Gallery</span>
				</a>
			</li>
			<?php }?>		
		</ul>
	</div>


</div>
<!--**********************************
	Sidebar end
***********************************-->