<style>
.table thead th 
{
	text-transform:none;
	font-size: 14px;
}
.table th, .table td
{
	border-top: 0px solid #EEEEEE;
}
.table tbody tr td {
	padding:0px;
}
.avatar-wrapper {
	 position: relative;
	 height: 200px;
	 overflow: hidden;
	 box-shadow: 1px 1px 15px -5px black;
	 transition: all 0.3s ease;
}
 .avatar-wrapper:hover {
	 transform: scale(1.05);
	 cursor: pointer;
}
 .avatar-wrapper:hover .profile-pic {
	 opacity: 0.5;
}
 .avatar-wrapper .profile-pic {
	 height: 100%;
	 width: 100%;
	 transition: all 0.3s ease;
}
 .avatar-wrapper .profile-pic:after {
	 font-family: FontAwesome;
	 content: "\f007";
	 top: 0;
	 left: 0;
	 width: 100%;
	 height: 100%;
	 position: absolute;
	 font-size: 190px;
	 background: #ecf0f1;
	 color: #34495e;
	 text-align: center;
}
 .avatar-wrapper .upload-button {
	 position: absolute;
	 top: 0;
	 left: 0;
	 height: 100%;
	 width: 100%;
}
 .avatar-wrapper .upload-button .fa-arrow-circle-up {
	 position: absolute;
	 font-size: 100px;
	 top: 40px;
     left: 30px;
	 text-align: center;
	 opacity: 0;
	 transition: all 0.3s ease;
	 color: #34495e;
}
 .avatar-wrapper .upload-button:hover .fa-arrow-circle-up {
	 opacity: 0.9;
}
 
</style>	
<?php echo form_open(base_url('admin/team/add'), 'id="quickForm" enctype="multipart/form-data" class="form-horizontal"');  ?> 		
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/team">All Team</a></li>
							<li class="breadcrumb-item active">Add team</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/team" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="card card-primary card-outline">
					<!-- /.card-header -->
					<div class="card-body custom_body">
						<div class="row">
							<div class="col-md-10" id="myDiv">
								<div class="row">
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="last_name" placeholder="*Last Name" title="Last Name" name="last_name" >
									</div>										
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="first_name" placeholder="*First Name" title="First Name" name="first_name" >
									</div>								
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="middle_name" placeholder="Middle Name" title="Middle Name" name="middle_name">
									</div>
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="designation" placeholder="*Designation" title="Designation" name="designation" >
									</div>	
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="email" placeholder="*Email Address" title="Email Address" name="email" >
									</div> 
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control us_phone" id="phone_number" placeholder="Phone Number" title="Phone Number" name="phone_number">
									</div>
									<div class="form-group col-lg-12 col-md-12">
										<input type="text" class="form-control" id="address1" placeholder="*Address1" title="Address1" name="address1" maxLength="40">
									</div>
									<div class="form-group col-lg-12 col-md-12">
										<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
									</div> 
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control" id="city" placeholder="*City" title="City" name="city" maxLength="30">
									</div>
									<div class="form-group col-lg-4 col-md-4">
										<select placeholder="State" class="form-control selectpicker" data-live-search="true"  name="state" id="state" title="*Select State">
											<option value="">Select State</option>
											<?php foreach($all_states as $state){ ?>
												<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
											<?php }?>	
										</select>
									</div>
									<div class="form-group col-lg-4 col-md-4">
										<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="Zip Code" name="zip_code" maxLength="10">
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="row">
									<div class="form-group col-lg-12 col-md-12">
										<div class="avatar-wrapper">
											<img class="profile-pic" src="https://lh3.googleusercontent.com/-EFqfBksPHew/YAwTjSOUKvI/AAAAAAAAGh4/Y0o4aIqERAMRII6O8JW7dtNOlLXXmTObwCK8BGAsYHg/s0/2021-01-23.jpg" />
											<div class="upload-button">
												<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
											</div>
											<input class="file-upload" type="file" name="image" accept="image/*"/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group col-lg-12 col-md-12">
								<textarea class="form-control" rows="7" placeholder="Notes" name="notes"></textarea>
							</div>															
							<div class="form-group col-lg-4 col-md-4">
								<input type="text" class="form-control" id="facebook" placeholder="Facebook page URL" name="facebook" >
							</div>
							<div class="form-group col-lg-4 col-md-4">
								<input type="text" class="form-control" id="twitter" placeholder="Twitter page URL" name="twitter">
							</div>
							<div class="form-group col-lg-4 col-md-4">
								<input type="text" class="form-control" id="instagram" placeholder="Instagram page URL" name="instagram" >
							</div>
							<div class="form-group col-lg-4 col-md-4">									
								<input type="text" class="form-control" id="linkedin" placeholder="Linkedin page URL" name="linkedin" >
							</div>
							<div class="form-group col-lg-4 col-md-4">
								<input type="text" class="form-control " id="pinterest" placeholder="Pinterest page URL" name="pinterest" >
							</div>
							<div class="form-group col-lg-4 col-md-4">
								<input type="text" class="form-control" id="youtube" placeholder="Youtube page URL" name="youtube">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	var teamHeight = document.getElementById('myDiv').offsetHeight;
	$('.avatar-wrapper').css('height',(teamHeight-25)+"px")
	
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
   
    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

$(document).ready(function () {
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Please enter a valid Zip Code.");
	$.validator.addMethod("letters_numbers_special", function(value, element) {
    	return this.optional(element) || /^[a-zA-Z0-9!@#$&()` .+,/"-]*$/i.test(value);
	//(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
	}, "");
  	$('#quickForm').validate({	  
		rules: {
		designation: {
			required: true
		},		
		address1: {
			required: true
		},
		city: {
			required: true
		},
		state: {
			required: true
		},
		zip_code: {
			required: true,
			zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
		},
		last_name: {
			required: true
		},
		first_name: {
			required: true
		},
		username: {
			required: true
		},
		email: {
			required: true,
			email: true,
		},
		},
		messages: {
		designation: {
			required: "Please enter Desination"
		},
		address1: {
			required: "Please enter Address 1"
		},
		city: {
			required: "Please enter City"
		},
		state: {
			required: "Please select State"
		},
		zip_code: {
			required: "Please enter Zip Code",
			zip_regex: "Please enter valid Zip Code"
		},
		last_name: {
			required: "Please enter Last Name"
		}, 
		first_name: {
			required: "Please enter First Name"
		}, 
		username: {
			required: "Please enter Username"
		},
		email: {
			required: "Please enter Email Address",
			email: "Please enter a valid Email Address"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});

});
</script>