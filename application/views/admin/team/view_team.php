<style>
.form-group {
    padding: 0px 5px;
}
.btttn td {
    border:none !important
}
table.dataTable thead th, table.dataTable thead td {
    padding: 7px 5px;
    border-bottom: 1px solid #111;
    font-size: 13px;
}
table.dataTable tbody th, table.dataTable tbody td {
    padding: 7px 5px;
}
#confirmmodel .modal-dialog {
    width: 100%;
    max-width: 100%;
    margin: 15px;
}
#confirmmodel .modal-content {
	width: 99%;
}
fieldset h4 {
    font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:5px 0px;
	border-bottom:none;
	text-decoration: underline;
}
fieldset label{
	font-weight:bold;
	font-size: 13px;
	margin: 0.25rem 0;
}
.preview{
	font-size: 13px;
	float:right;
	margin: 0.25rem 0;
}
legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:0 10px;
	border-bottom:none;
}
.preview_col{
	padding:0px;
	border-bottom: 1px solid #ccc2c2;
}
.box-row { 
	display:table-row; 
} 
 .box-cell { 
	display:table-cell; 
	width:50%; 
	padding:10px; 
} 
 .box-cell.box1 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	} 
 .box-cell.box2 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
} 
.uuu{
	padding:0 5px;
	margin: 0px;
	
}
.flex-container{
	min-height: 300px;
	margin: 0 auto;
	display: -webkit-flex;	
	display: flex;
}
.flex-container .column{
	padding: 10px;
	-webkit-flex: 1;
	-ms-flex: 1;
	flex: 1;
	border: 1px solid #ccc2c2;
}
.flex-container .column.bg-alt{
	 
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/team">All teams</a></li>
							<li class="breadcrumb-item active">View team Details</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">						
						<div class="row flex-container">
							<div class="col-md-3 column bg-alt">
								<fieldset class="scheduler-border">
									<div class="row uuu">
										<div class=""><h4>PICTURE:</h4></div>
										<div class="col-md-12 "><img src="<?= base_url() ?>images/team/<?php echo $team['image']?>" style="width: inherit;height: 300px;"></div>
																		
									</div>
								</fieldset>
							</div>
							<div class="col-md-4 column bg-alt">
								<fieldset class="scheduler-border">
									<div class="row uuu">
										<div class=""><h4>GENERAL INFORMATION:</h4></div>
										<div class="col-md-12 preview_col"><label>Last Name : </label><span class="preview" id=""><?php echo $team['lastname']?></span></div>
										<div class="col-md-12 preview_col"><label>First Name : </label><span class="preview" id=""><?php echo $team['firstname']?></span></div>
										<div class="col-md-12 preview_col"><label>Middle Name : </label><span class="preview" id=""><?php echo $team['middlename']?></span></div>
										<div class="col-md-12 preview_col"><label>Designation : </label><span class="preview" id=""><?php echo $team['designation']?></span></div>
										<div class="col-md-12 preview_col"><label>Email Address : </label><span class="preview" id=""><?php echo $team['email']?></span></div>
										<div class="col-md-12 preview_col"><label>Phone Number : </label><span class="preview" id=""><?php echo $team['mobile_no']?></span></div>
										<div class="col-md-12 preview_col"><label>Address1 : </label><span class="preview" id=""><?php echo $team['address1']?></span></div>
										<div class="col-md-12 preview_col"><label>Address2 : </label><span class="preview" id=""><?php echo $team['address2']?></span></div>
										<div class="col-md-12 preview_col"><label>City : </label><span class="preview" id=""><?php echo $team['city']?></span></div>
										<div class="col-md-12 preview_col"><label>State : </label><span class="preview" id=""><?php echo $team['state']?></span></div>
										<div class="col-md-12 preview_col"><label>Zip Code : </label><span class="preview" id=""><?php echo $team['zip_code']?></span></div>								
									</div>
								</fieldset>
							</div>
							<div class="col-md-5 column bg-alt">
								<fieldset class="scheduler-border">
									<div class="row uuu">
										<div class=""><h4>SOCIAL MEDIA:</h4></div>
										<div class="col-md-12 preview_col"><label>Facebook : </label><span class="preview" id=""><?php echo $team['facebook']?></span></div>
										<div class="col-md-12 preview_col"><label>Twitter : </label><span class="preview" id=""><?php echo $team['twitter']?></span></div>
										<div class="col-md-12 preview_col"><label>Instagram : </label><span class="preview" id=""><?php echo $team['instagram']?></span></div>
										<div class="col-md-12 preview_col"><label>Linkedin : </label><span class="preview" id=""><?php echo $team['linkedin']?></span></div>
										<div class="col-md-12 preview_col"><label>Pinterest : </label><span class="preview" id=""><?php echo $team['pinterest']?></span></div>
										<div class="col-md-12 preview_col"><label>Youtube : </label><span class="preview" id=""><?php echo $team['youtube']?></span></div>
																		
									</div>
								</fieldset>
							</div>							
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>