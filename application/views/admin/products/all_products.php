<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
?>
<!--**********************************
	Content body start
***********************************-->
<style>

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0);
  transition: background 0.5s ease;
}

.ttt:hover .overlay {
  display: block;
  background: rgba(0, 0, 0, .3);
}


.title {
  position: absolute;
  width: 500px;
  left: 0;
  top: 120px;
  font-weight: 700;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  z-index: 1;
  transition: top .5s ease;
}

.ttt:hover .title {
  top: 90px;
}

.button {
  position: absolute;
  left:35%;
  top: 30%;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {

}

.ttt:hover .button {
  opacity: 1;
}
.card {
    height: auto;
}
.ttt h4 {
  white-space: nowrap; 
  width: auto; 
  overflow: hidden;
  text-overflow: ellipsis;
}

.ttt h4:hover {
  overflow: visible;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/products">Products</a></li>
							<li class="breadcrumb-item active">Products</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
				    <?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
					<a href="<?php echo base_url()?>admin/products/add"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Product</button></a>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>										
										<!--th>Form Source</th-->
										<th>Product Id</th>
										<th>Name</th>
										<th>Category</th>
										<th>Vendor</th>	
										<th>Product Image</th>
										<th>Status</th>
										<th>Actions</th>										
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_products as $row): ?>
									<tr>
										<td><?= $row['pid']; ?></td>
										<td><?= $row['product_name']; ?></td>
										<td><?= $row['category_name']; ?></td>
										<td><?= $row['vendor_name']; ?></td>
										<td >
											<a href="<?= base_url() ?>images/products/<?= $row['image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
												<img src="<?= base_url() ?>images/products/<?= $row['image']; ?> " style="width: 100px;height: 40px;" alt="<?= $row['image']; ?>"> 
                                            </a>
										</td>
										<td> 
											<label class="switch">
											  <input type="checkbox" <?php if($row['status']==1){echo 'checked';}?> id="status" product_id="<?php echo $row['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_row<?php echo $row['id']; ?>">
											<?php if($row['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span style="white-space: nowrap;">In-Active</span>

											<?php }?>
											</span>
										</td>
										<td>
										     <?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
											<a data-toggle="tooltip"  title="Edit Product"  href="<?php echo base_url()?>admin/products/edit/<?php echo $row['id'];?>" class="btn btn-sm btn-outline-dark"><i class="fa fa-pencil"></i></a>
											<a data-toggle="tooltip" title="Delete" href="#" id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark "><i class="fa fa-trash-o"></i></a>
											<?php }?>
											<a data-toggle="tooltip" title="View Product" href="<?php echo base_url()?>admin/products/view_product/<?php echo $row['id'];?>" class="btn btn-sm btn-outline-dark "><i class="fa fa-eye"></i></a>
										</td>
									</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
function readURLL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('.blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$(".edit_product").click(function(){
	var id = $(this).attr('product_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/products/get_product_by_id');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#product_id').val(data.id);
			$('#category_id').val(data.category_id);
			$('#eproduct_name').val(data.product_name);
			$('#eproduct_price').val(data.price);
			$('#eproduct_brand').val(data.brand);
			$('#eproduct_tags').val(data.tags);
			$('#eproduct_description').text(data.product_description);
			$('.blah').attr('src','<?= base_url() ?>images/products/'+data.m_file+' ?>');
			//location.reload();$('#my_image').attr('src','second.jpg');
		}
	});
    $('#editProduct').modal('show');
});
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/products/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
</script>
<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;                
            }
            else {
                var id = 0;
            }
			var product_id = $(this).attr('product_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('admin/products/update_status'); ?>",
			  data: {id: id, product_id: product_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+product_id).load(" #status_row"+product_id);
			  }
			});
		});
		$('#quickForm1').validate({	  
			rules: {
			product_name: {
				required: true
			},			
			product_price: {
				required: true
			},
			product_brand: {
				required: true
			},
			product_tags: {
				required: true
			},
			product_description: {
				required: true
			},
			product_iamge: {
				required: true
			},
			},
			messages: {
			product_name: {
				required: "Please enter Product Name"
			},
			product_price: {
				required: "Please enter Price"
			},
			product_brand: {
				required: "Please enter Brand"
			},
			product_tags: {
				required: "Please enter Tags"
			},
			product_description: {
				required: "Please enter Description"
			},
			product_iamge: {
				required: "Please upload Product Image"
			},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
			}
		});
		$('#quickForm').validate({	  
			rules: {
			product_name: {
				required: true
			},			
			product_price: {
				required: true
			},
			product_brand: {
				required: true
			},
			product_tags: {
				required: true
			},
			product_description: {
				required: true
			},
			product_iamge: {
				required: true
			},
			},
			messages: {
			product_name: {
				required: "Please enter Product Name"
			},
			product_price: {
				required: "Please enter Price"
			},
			product_brand: {
				required: "Please enter Brand"
			},
			product_tags: {
				required: "Please enter Tags"
			},
			product_description: {
				required: "Please enter Description"
			},
			product_iamge: {
				required: "Please upload Product Image"
			},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
			}
		});
	});
</script>
<script>

	
</script> 