
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/products">Products</a></li>
							<li class="breadcrumb-item active">Products</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?php echo base_url()?>admin/products/add"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Product</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="row">
							<div class="col-xl-3">
								<!-- Tab panes -->
								<div class="tab-content">
									<?php $i=1; foreach ($product['images'] as $image){ ?>
									<div role="tabpanel" class="tab-pane fade <?php if($i==1){?>show active <?php }?>" id="img<?php echo $image['image_id'];?>">
										<img class="img-fluid" src="<?php echo base_url();?>images/products/<?php  echo $image['image']; ?>" style="height:340px;" alt="">
									</div>
									<?php $i++; }?>
								</div>
								<div class="tab-slide-content">
									<!-- Nav tabs -->
									<ul class="nav slide-item-list mt-3" role="tablist">
										<?php $j=1; foreach ($product['images'] as $image){ ?>
										<li role="presentation" class="<?php if($j==1){?>show <?php }?>">
											<a href="#img<?php echo $image['image_id'];?>" role="tab" data-toggle="tab">
												<img class="img-fluid" src="<?php echo base_url();?>images/products/<?php  echo $image['image']; ?>" alt="" width="50">
											</a>
										</li>
										<?php $j++; }?>
									</ul>
								</div>
							</div>
							<!--Tab slider End-->
							<div class="col-xl-9 col-sm-12">
								<div class="product-detail-content">
									<!--Product details-->
									<div class="new-arrival-content pr">
										<h4><?php echo $product['product_name']; ?></h4>
										<p class="price">$<?php echo $product['price']; ?>/<?php echo $product['units']; ?></p>
										<p>Product Id: <span class="item"><?php echo $product['product_id']; ?></span> </p>
										<p>Universal Product Code: <span class="item"><?php echo $product['upc']; ?></span> </p>
										<p>Vendor Product Id: <span class="item"><?php echo $product['vpc']; ?></span> </p>
										<p>Brand: <span class="item"><?php echo $product['brand']; ?></span></p>
										<p>Product tags:&nbsp;&nbsp;
											<span class="item-tag"><?php echo $product['tags']; ?></span>
										</p>
										<p class="text-content"><?php echo $product['summary']; ?></p>
										<!--Quantity start-->
									</div>
								</div>
							</div>
							<div class="col-xl-12 col-sm-12">
								<div class="product-detail-content">
									<!--Product details-->
									<div class="new-arrival-content pr">
										<p class="text-content"><?php echo $product['product_description']; ?></p>
										<!--Quantity start-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
function readURLL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('.blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$(".edit_product").click(function(){
	var id = $(this).attr('product_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/products/get_product_by_id');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#product_id').val(data.id);
			$('#category_id').val(data.category_id);
			$('#eproduct_name').val(data.product_name);
			$('#eproduct_price').val(data.price);
			$('#eproduct_brand').val(data.brand);
			$('#eproduct_tags').val(data.tags);
			$('#eproduct_description').text(data.product_description);
			$('.blah').attr('src','<?= base_url() ?>images/products/'+data.m_file+' ?>');
			//location.reload();$('#my_image').attr('src','second.jpg');
		}
	});
    $('#editProduct').modal('show');
});
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/products/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
</script>
<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
		$('#quickForm1').validate({	  
			rules: {
			product_name: {
				required: true
			},			
			product_price: {
				required: true
			},
			product_brand: {
				required: true
			},
			product_tags: {
				required: true
			},
			product_description: {
				required: true
			},
			product_iamge: {
				required: true
			},
			},
			messages: {
			product_name: {
				required: "Please enter Product Name"
			},
			product_price: {
				required: "Please enter Price"
			},
			product_brand: {
				required: "Please enter Brand"
			},
			product_tags: {
				required: "Please enter Tags"
			},
			product_description: {
				required: "Please enter Description"
			},
			product_iamge: {
				required: "Please upload Product Image"
			},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
			}
		});
		$('#quickForm').validate({	  
			rules: {
			product_name: {
				required: true
			},			
			product_price: {
				required: true
			},
			product_brand: {
				required: true
			},
			product_tags: {
				required: true
			},
			product_description: {
				required: true
			},
			product_iamge: {
				required: true
			},
			},
			messages: {
			product_name: {
				required: "Please enter Product Name"
			},
			product_price: {
				required: "Please enter Price"
			},
			product_brand: {
				required: "Please enter Brand"
			},
			product_tags: {
				required: "Please enter Tags"
			},
			product_description: {
				required: "Please enter Description"
			},
			product_iamge: {
				required: "Please upload Product Image"
			},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
			error.addClass('invalid-feedback');
			element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
			$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass('is-invalid');
			}
		});
	});
</script>
<script>

	
</script> 