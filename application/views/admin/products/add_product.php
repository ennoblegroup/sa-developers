<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<style>
.btn.btn-file>input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    opacity: 0;
    outline: 0;
    background: #fff;
    cursor: inherit;
    display: block;
}
.dragArea {
	background-color: #efefef;
	border: 3px dashed #cccccc;
	border-radius: 10px;
	text-align: center;
	padding: 10px 10px;
}
.dragAreaa{
	background-color: #efefef;
	border: 3px dashed #cccccc;
	border-radius: 10px;
	text-align: center;
	padding: 12px 10px;
}
.dragAreas {
	background-color: #efefef;
	border: 3px dashed #cccccc;
	border-radius: 10px;
	text-align: center;
	padding: 40px 10px;
}
.dragArea.over {
	border-color: #ff0000;
	background-color: #FFE9EA;
}
.resultImageWrapper {
	display: none;
	text-align: center;
}
#resultImage {
	box-shadow: rgba(0,0,0,0.4) 0 2px 5px;
}
.qqe{
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
}
.deleteimg{
	cursor: pointer;
}
.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.btn.btn-green {
    background: #95BF03;
    border-color: #95BF03;
    color: #fff;
}
.btn.btn-file {
    position: relative;
    overflow: hidden;
}
.form-group {
    padding: 0px 5px;
}
.card {
    height: unset;
}
</style>
<?php echo form_open_multipart(base_url('admin/products/add'), 'id="quickForm" class="form-valid"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/products'); ?>">Products</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Add Product</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/products" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Add Product</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10">
									<div class="row">																
										<!--div class="form-group col-lg-3 col-md-3">
											<label class="col-form-label">Source</label>
											<input type="text" class="form-control" name="file_source">
										</div-->
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" readonly id="product_id" value="<?php echo $product_id?>" name="product_id">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select  title="Select Category" data-live-search="true"  class="selectpicker form-control" name="category_id">
												<?php $i=1; foreach($all_categories as $row): ?>
												<option value="<?= $row['id']; ?>"><?= $row['category_name']; ?> </option>
												<?php $i++; endforeach; ?>
											</select>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select   title="Select Vendor" data-live-search="true"  class="selectpicker form-control" id="vendor_id" name="vendor_id">
												<?php $i=1; foreach($all_vendors as $row): ?>
												<option value="<?= $row['id']; ?>"><?= $row['vendor_name']; ?></option>
												<?php $i++; endforeach; ?>
											</select>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="product_name" placeholder="Product Name" name="product_name">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="product_price" placeholder="Product Price" name="product_price">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="units" placeholder="Units" name="units">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="product_brand" placeholder="Brand" name="product_brand">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="product_tags" placeholder="Tags" name="product_tags">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="upc" placeholder="Universal Product Code" name="upc">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="vpc" placeholder="Vendor Product Id" name="vpc">
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" rows="3" placeholder="Summary" name="product_summary"></textarea>
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control editor" placeholder="Description" name="product_description"></textarea>
										</div>
										<div class="form-group col-md-12">
											<label>Product Images</label>
											<div class="card card-primary" id="imagee">
												<div class="dragAreaa"> 
													<div class="btn btn-outline-dark btn-file">Upload Images
														<input type="file"  multiple accept="image/gif, image/jpeg, image/jpg ,image/png" id="imageFile" >
													</div></center>
												</div>
											</div>
										</div>
										<div class="form-group col-md-12" bis_skin_checked="1">
											<label>Selected Product Images</label>
											<div class="card card-primary" bis_skin_checked="1">
												<div class="card-body" bis_skin_checked="1">
													<div class="row resultImageWrapper" bis_skin_checked="1">
													</div>
												</div>	
											</div>		
										</div>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close( ); ?>
	
<script>
// Required for drag and drop file access
jQuery.event.props.push('dataTransfer');
$(function() {
  var dropTimer;
  var dropTarget = $('.dragArea');
  var fileInput = $('#imageFile');
  dropTarget.on("dragover", function(event) {
    dropTarget.addClass('over');
    return false; // Required for drop to work
  });
  dropTarget.on('dragleave', function(event) {
    dropTarget.removeClass('over');
  });
  handleDrop = function(files) {
    dropTarget.removeClass('over');
    var file;
    // iterate through all dropped files
    for (var i = 0; i < files.length; i++) {
		
		file = files[i];
		if (file.type.match('image.*')) {		 
			resizeImage(file, 100, function(result) {
				var ncc = $( ".qq" ).length;
				if(ncc==0){
					var chk='checked';
					var chkval=1;					
				}else{
					var chk='';
					var chkval=0;
				}
			// we now need to append a new Image instead of reusing a single one
				$('.resultImageWrapper').append($('<div class="col-md-2 qq" style="margin-bottom:10px;"><div class="qqe"><div class="ee" style="background:url('+result+');background-repeat: no-repeat;background-size: cover;height:120px;"><a class="deleteimg"><i class="fa fa-times-circle-o" aria-hidden="true" style="position: relative;float: right;color: #e10e0e;font-size: 24px;"></i></a><input type="hidden" class="asd" name="rrrr[]" value="'+result+'"></div><div  style="    padding: 5px;"><center><input type="radio" '+chk+' class="is_primaryy" name="is_primaryy[]" > Set Primary Image<input type="hidden" class="is_primary" value="'+chkval+'" name="is_primary[]"></center></div></div></div>'));
				$('.resultImageWrapper').show();
				$(".resultImageWrapper").css("display","flex");
			});
		} else {
			alert("That file wasn't an image.");
		}
    }
  };
  dropTarget.on('drop', function(event) {
    event.preventDefault(); // Or else the browser will open the file
    handleDrop(event.dataTransfer.files);
  });
  fileInput.on('change', function(event) {
    handleDrop(event.target.files);
  });
  resizeImage = function(file, size, callback) {
    // no need for a FileReader, a blobURI is better
    var image = new Image();
    image.onload = function() {
      var canvas = document.createElement("canvas");
      /*
      if(image.height > size) {
      	image.width *= size / image.height;
      	image.height = size;
      }
      */
      if (image.width > size) {
        image.height *= size / image.width;
        image.width = size;
      }
      var ctx = canvas.getContext("2d");
      canvas.width = image.width;
      canvas.height = image.height;
      ctx.drawImage(image, 0, 0, image.width, image.height);
      callback(canvas.toDataURL("image/png"));
    };
    image.src = URL.createObjectURL(file);

  };
});
$(document).on('click', '.deleteimg', function(e) {
   e.preventDefault();
   $(this).closest('.qq').remove();
   return false;
});
$(document).on('click', '.is_primaryy', function(e) {
    $(".is_primary").val('0');
	$(this).next('.is_primary').val('1');	
});
</script>


<script>
function filePreview(input,cnt) {
	$(".img").remove();
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_'+cnt+' + img').remove();
            $('#preview_'+cnt).after('<img src="'+e.target.result+'" width="60" height="60"/>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function filePreviewmain(input,cnt) {
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
		   $('#preview_main_'+cnt+' + img').remove();
            $('#preview_main_'+cnt).after('<img src="'+e.target.result+'" width="60" height="60"/>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function deleteExpRow(id)
{
	if(confirm('Do you want to delete this row?'))
	{
		jQuery('#'+id).remove();
	}
}

$('body').on('click', '#add_more', function() {
	
	var rowCount= $("#add_attachment > .row").length+2;
	var rowCountt= $("#add_attachment > .row").length+1;
	
	$(".img").remove();
	if($('#image_'+rowCountt).val() == "" && $('#cimage_'+rowCountt).val() == ""){
		$(".rrr_"+rowCountt).after('<span class="errorr img" style="color:red">Please upload image</span>');
	}else{
		$("#add_attachment").append('<div class="row rrr_'+rowCount+'"  style="padding-bottom: 10px;padding-top: 10px;border: 2px solid #D7E3ED !important;margin: 5px 0px;" id="doc_'+rowCount+'"><div class="col-md-9" style="margin-top: 15px;margin-bottom: 15px;" ><input id="image_'+rowCount+'" name="product_moreimage[]" required="required" type="file" accept="image/gif, image/jpeg, image/png" onchange="filePreview(this,\'pid_'+rowCount+'\');"/></div><div class="col-md-3"><div id="preview_pid_'+rowCount+'"></div><input id="cimage_'+rowCount+'" name="product_moreimage[]" type="hidden" /><div class="btn-group btn-group-sm" style="float: right;top: -15px;right: -12px;"><button type="button" class="tabledit-delete-button btn btn-sm btn-danger waves-effect waves-light" style="margin: 5px;" onclick="deleteExpRow(\'doc_'+rowCount+'\')"><i class="fa fa-times" aria-hidden="true"></i></button></div></div></div>');		
	} 
});

</script>
<script>
$(document).ready(function() {
	$.validator.addMethod("imageCheck", function (value, element) {
        return false;
    }, 'Password and Confirm Password should be same');

	var form = $('#quickForm');
	form.validate({	  
		rules: {
		rrrr: {
			imageCheck: true
		},
		product_name: {
			required: true
		},			
		product_price: {
			required: true,
			number:true
		},
		product_brand: {
			required: true
		},
		product_tags: {
			required: true
		},
		product_description: {
			required: true
		},
		category_id: {
			required: true
		},
		vendor_id: {
			required: true
		},
		units: {
			required: true
		},
		upc: {
			required: true
		},
		vpc: {
			required: true
		},
		},
		messages: {
		rrrr: {
			imageCheck: "Please enter Product Name"
		},
		product_name: {
			required: "Please enter Product Name"
		},
		product_price: {
			required: "Please enter Price",
			number: "Please enter valid amount"
		},
		product_brand: {
			required: "Please enter Brand"
		},
		product_tags: {
			required: "Please enter Tags"
		},
		product_description: {
			required: "Please enter Description"
		},
		category_id: {
			required: "Please select Category"
		},
		vendor_id: {
			required: "Please select Vendor"
		},
		units: {
			required: "Please enter Units"
		},
		upc: {
			required: "Please enter Universal Product Code"
		},
		vpc: {
			required: "Please enter Vendor Product Code"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		},
		submitHandler: function(form) {
			var numItems = $('.asd').length;
			if(numItems>0){
				form.submit();
			}else{
				alert('Please upload atleast one image');
			}
			
		}		
	});
});
</script>