<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>admin System - Admin Panel </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/admin/images/favicon.png">
    <link href="<?= base_url() ?>assets/admin/css/style.css" rel="stylesheet">
	<style>
	.h-100-image{
	background-image: url(https://dmscoadminservices.com/wp-content/uploads/2018/01/medical-admin-software.jpg);
background-size: 100%;
background-position: center;
background-repeat: no-repeat;
	}
	.authincation-content
	{
		border-radius: 16px;
	}
	.auth-form
    { padding: 21px 50px; }
    .glyphicon-remove {
  color: red;
  font-size: 13px;
}

.glyphicon-ok {
  color: green;
  font-size: 13px;
}
.glyphicon-remove:before {
  content: 'x';
  padding-right:5px;
  font-weight:bold;
}
.glyphicon-ok:before {
  content: '✓';
  padding-right:5px;
  font-weight:bold;
}
.qa{
  padding:5px 10px;
  border: 1px solid #bebaba;
  border-radius: 0.25rem;
}
	</style>
</head>
<!--body class="h-100" style="background-color: #d28d2e !important;" -->
<body class="h-100 h-100-image">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Reset Password</h4>
                                    <?php echo form_open(base_url('admin/auth/update_password'), 'id="quickForm" class="form-valide" '); ?>
                                        <?php if($this->session->flashdata('success')): ?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button"  class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <?php echo $this->session->flashdata('success'); ?>
                                        </div>
                                        <?php endif; ?>
                                        <?php if($this->session->flashdata('error')){?>
                                            <div class="alert alert-danger alert-dismissible">
                                            <button type="button"  class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>      
                                                <b> <?php echo $this->session->flashdata('error')?> </b>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <!--label><strong>Email</strong></label-->
                                            <input type="password" name="password" id="val-password" class="form-control" placeholder="New Password" title="New Password" >
                                            <input type="hidden" name="email"  value="<?= isset($email)? $email: ''; ?>"  >
                                            <input type="hidden" name="resetid"  value="<?= isset($resetid)? $resetid: ''; ?>"  >
                                        </div>
                                        <div class="form-group">
                                            <!--label><strong>Email</strong></label-->
                                            <input type="password" name="cpassword" id="val-cpassword" class="form-control" placeholder="Confirm Password" title="Confirm Password" >
                                        </div>
                                        <div class="form-group">
                                            <div class=" qa">
                                                <div id="Length" class="glyphicon glyphicon-remove">Password must be at least 8 charcters</div>
                                                <div id="UpperCase" class="glyphicon glyphicon-remove">Password must have atleast 1 upper case character</div>
                                                <div id="LowerCase" class="glyphicon glyphicon-remove">Password must have atleast 1 lower case character</div>
                                                <div id="Numbers" class="glyphicon glyphicon-remove">Password must have atleast 1 numeric character</div>
                                                <div id="Symbols" class="glyphicon glyphicon-remove">Password must have atleast 1 special character</div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <input type="submit" name="submit" id="submit" value="RESET PASSWORD"class="btn btn-outline-dark btn-block" style="border-radius: 1.25rem;height: 41px;">
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group" style="text-align:right">
                                                Go Back to <a href="<?= base_url('admin/auth'); ?>">Login</a>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                    <!--div class="Add-account mt-3">
                                        <p>Don't have an account? <a class="text-primary" href="page-register.html">Sign up</a></p>
                                    </div-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Jquery Validation -->
<script src="<?= base_url() ?>assets/admin/vendor/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("password_pattern", function(value, element, regexpr) {          
        return regexpr.test(value);
    });

	$('#quickForm').validate({
    rules: {
      password: {
        required: true,
        minlength: 8,
        password_pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
      },
	  cpassword: {
        required: true,
        equalTo: "#val-password"
      },
    },
    messages: {
	  password: {
        required: "Please enter New Password",
        minlength:"",
        password_pattern:""
      }, 
       cpassword: {
        required: "Please enter Confirm Password",
        equalTo: "Password doesn't Match"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  });
  $(document).ready(function() {
    $("#val-password").on('keyup', ValidatePassword)
  });
  function ValidatePassword() {
    /*Array of rules and the information target*/
    var rules = [{
        Pattern: "[A-Z]",
        Target: "UpperCase"
      },
      {
        Pattern: "[a-z]",
        Target: "LowerCase"
      },
      {
        Pattern: "[0-9]",
        Target: "Numbers"
      },
      {
        Pattern: "[!@@#$%^&*]",
        Target: "Symbols"
      }
    ];
  
    //Just grab the password once
    var password = $(this).val();
  
    /*Length Check, add and remove class could be chained*/
    /*I've left them seperate here so you can see what is going on */
    /*Note the Ternary operators ? : to select the classes*/
    $("#Length").removeClass(password.length > 7 ? "glyphicon-remove" : "glyphicon-ok");
    $("#Length").addClass(password.length > 7 ? "glyphicon-ok" : "glyphicon-remove");
    
    /*Iterate our remaining rules. The logic is the same as for Length*/
    for (var i = 0; i < rules.length; i++) {
  
      $("#" + rules[i].Target).removeClass(new RegExp(rules[i].Pattern).test(password) ? "glyphicon-remove" : "glyphicon-ok"); 
      $("#" + rules[i].Target).addClass(new RegExp(rules[i].Pattern).test(password) ? "glyphicon-ok" : "glyphicon-remove");
    }
  }
  </script>
</body>
<!-- Mirrored from metroadmin.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2020 06:07:01 GMT -->
</html>
