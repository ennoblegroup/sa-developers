<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>admin System - Admin Panel </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/admin/images/favicon.png">
    <link href="<?= base_url() ?>assets/admin/css/style.css" rel="stylesheet">
	<style>
	.h-100-image{
	background-image: url(https://dmscoadminservices.com/wp-content/uploads/2018/01/medical-admin-software.jpg);
    background-size: 100%;
    background-position: center;
    background-repeat: no-repeat;
	}
	.authincation-content
	{
		border-radius: 16px;
	}
	.auth-form
    { padding: 21px 50px; }
    .rree a:hover{
        color:red;
    }
	</style>
</head>
<!--body class="h-100" style="background-color: #d28d2e !important;" -->
<body class="h-100 h-100-image">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Forgot Password</h4>
                                    
                                    <?php echo form_open(base_url('admin/auth/reset_password'), 'id="quickForm" class="form-valide" '); ?>
                                        <?php if($this->session->flashdata('success')): ?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button"  class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <?php echo $this->session->flashdata('success'); ?>
                                        </div>
                                        <?php endif; ?>
                                        <?php if($this->session->flashdata('error')){?>
                                            <div class="alert alert-danger alert-dismissible">
                                            <button type="button"  class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>      
                                                <b> <?php echo $this->session->flashdata('error')?> </b>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <!--label><strong>Email</strong></label-->
                                            <input type="email" name="email" id="val-email" class="form-control" placeholder="Email" title="Email" >
                                        </div>
                                        <div class="text-center">
                                            <input type="submit" name="submit" id="submit" value="SEND EMAIL"class="btn btn-outline-dark btn-block" style="border-radius: 1.25rem;height: 41px;">
                                        </div>
                                        <div class="mt-4 mb-2">
                                            <div class="form-group rree" style="text-align:center">
                                                <a href="<?= base_url('admin/auth'); ?>"> Go Back to Login</a>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                    <!--div class="Add-account mt-3">
                                        <p>Don't have an account? <a class="text-primary" href="page-register.html">Sign up</a></p>
                                    </div-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- For admin Project Manual Validations -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
 
	<!-- Jquery Validation -->
    <script src="<?= base_url() ?>assets/admin/vendor/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	$('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
	  password: {
        required: true
      },
    },
    messages: {
	  email: {
        required: "Please enter Email Address",
        email:"Enter a valid Email Address"
      }, 
       password: {
        required: "Please enter password"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  });
  </script>
</body>
<!-- Mirrored from metroadmin.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2020 06:07:01 GMT -->
</html>
