<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>S&A Developers - Admin Panel </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/admin/images/favicon.png">
    <link href="<?= base_url() ?>assets/admin/css/style.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script>
       $(document).ready(function(){
           $('.captcha-refresh').on('click', function(){
               $.get('<?php echo base_url().'admin/auth/refresh'; ?>', function(data){
				   var out =JSON.parse(data);
                   $('#image_captcha').html(out.img);
				   $('#cap').val(out.word);
               });
           });
       });
	</script>
	<style>
	.h-100-image{
	background-image: url(https://image.freepik.com/free-vector/construction-background-construction-info-graphics-book-cover-design_40382-68.jpg);
background-size: 100%;
background-position: center;
background-repeat: no-repeat;
	}
	.authincation-content
	{
		border-radius: 16px;
	}
	.auth-form
	{ padding: 21px 50px; }
	.form-check-input {
     margin-top: 2px !important;
    }
    .form-check-label {
    margin-left: 7px !important;
    margin-top: 0px !important; 
    }
    body {font-size:14px;}
	</style>
</head>
<!--body class="h-100" style="background-color: #d28d2e !important;" -->
<body class="h-100 h-100-image">
    <div class="authincation h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4"><img src="http://sa-developers.com/wp-content/uploads/2015/06/original-7515-5764003.png" style="width:200px" alt="logo"></h4>
									
                                    <?php echo form_open(base_url('admin/auth/login'), 'id="quickForm" class="form-valide" '); ?>
										<?php if(isset($msg) || validation_errors() !== ''): ?>
                                            <div class="alert alert-danger solid alert-dismissible fade show">
                                                <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                </button>
                                                <strong><?= validation_errors();?>
												<?= isset($msg)? $msg: ''; ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($this->session->flashdata('success_msg')) {
                                            $message = $this->session->flashdata('success_msg'); ?>
                                            <div class="alert alert-success solid alert-dismissible fade show">
                                                <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                </button>
                                                <strong>
												<?= isset($message)? $message: ''; ?>
                                            </div>
                                        <?php }
										if($this->uri->segment(4)!='') { ?> 
											<div class="alert alert-success solid alert-dismissible fade show">
                                                <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                </button>
                                                <strong>Password is Updated Successfully!</strong>
                                            </div>
										<?php } ?>
                                        <div class="form-group err">
                                            <!--label><strong>Email</strong></label-->
                                            <input type="email" name="email" id="val-email" class="form-control" placeholder="Email Address" title="Email Address" value="<?php if(isset($_COOKIE["admin_email"])) { echo $_COOKIE["admin_email"]; } ?>">
                                        </div>
                                        <div class="form-group err">
                                            <!--label><strong>Password</strong></label-->
                                            <input type="password" name="password" id="val-password" class="form-control" placeholder="Password" title="Password" value="<?php if(isset($_COOKIE["admin_password"])) { echo $_COOKIE["admin_password"]; } ?>">
                                        </div>
										<div class="form-group">
											<div  class="row err" style="margin:0px">
												<input type="hidden" id="cap" value="<?php echo $this->session->userdata('valuecaptchaCode');?>">
												<input type="text" placeholder="Enter Captcha" class="form-control" name="captcha" style="width:50%" value=""/>
												<p id="image_captcha" style="width:35%;padding-left:10px;margin-bottom:0px;"><?php echo $captchaImg; ?></p>
												<a href="javascript:void(0);" style="width:15%;" class="captcha-refresh" ><img src="<?php echo base_url().'images/refresh.png'; ?>" style="width:38px;height:38px;padding: 5px;float: right;"/></a>
											</div>
										</div>
										 
                                        <div class="text-center">
                                            <input type="submit" name="submit" id="submit" value="LOGIN"class="btn btn-outline-dark btn-block" style="border-radius: 1.25rem;height: 41px;">
                                        </div>
										<!--div class="text-center">
                                            <?php echo $login_button; ?>
                                        </div-->
										
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                               <!--div class="custom-control custom-checkbox ml-1">
													<input type="checkbox" class="custom-control-input" id="basic_checkbox_1">
													<label class="custom-control-label" for="basic_checkbox_1">Remember me</label>
												</div-->
												<div class="form-check">
                                                    <input class="form-check-input" id="remember" name="remember" type="checkbox" <?php if(isset($_COOKIE["admin_email"])) { ?> checked <?php } ?>>
                                                    <!--label class="form-check-label">
                                                       Rememeber Email Address<?php echo $msg;?>
                                                    </label-->
													<label class="form-check-label">
                                                       Remember Email Address
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="<?= base_url('admin/auth/reset_password'); ?>">Forgot Password?</a>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                    <!--div class="Add-account mt-3">
                                        <p>Don't have an account? <a class="text-primary" href="page-register.html">Sign up</a></p>
                                    </div-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
        Scripts
    ***********************************-->
	
	<!-- For admin Project Manual Validations -->
	
    <script src="<?= base_url() ?>assets/admin/vendor/global/global.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/custom.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/deznav-init.js"></script>
    



	<!-- Svganimation scripts -->
    <script src="<?= base_url() ?>assets/admin/vendor/svganimation/vivus.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/svganimation/svg.animation.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/styleSwitcher.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/jquery-validation/jquery.validate.min.js"></script>
	
<script type="text/javascript">
	$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid email.");
		$('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
	  password: {
        required: true
      },
	  captcha: {
        required: true,
        equalTo: "#cap"
      },
    },
    messages: {
	  email: {
        required: "Please enter Email Address",
        email: "Enter a valid Email Address"
      }, 
       password: {
        required: "Please enter Password"
      },
	  captcha: {
        required: "Please enter Captcha",
		equalTo:"Incorrect captcha"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.err').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  });
  $(document).ready(function(){
  $(".close").click(function(){
    $(".alert").alert("close");
  });
});
</script>
</body>
<!-- Mirrored from metroadmin.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2020 06:07:01 GMT -->
</html>
