<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>S&A Developers</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/admin/images/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/vendor/toastr/css/toastr.min.css">
	<link href="<?= base_url() ?>assets/admin/vendor/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/admin/vendor/fullcalendar/css/fullcalendar.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/vendor/chartist/css/chartist.min.css">
    
    <!-- Daterange picker -->
    <link href="<?= base_url() ?>assets/admin/vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Clockpicker -->
    <link href="<?= base_url() ?>assets/admin/vendor/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <!-- asColorpicker -->
    <link href="<?= base_url() ?>assets/admin/vendor/jquery-asColorPicker/css/asColorPicker.min.css" rel="stylesheet">
    <!-- Material color picker -->
    <link href="<?= base_url() ?>assets/admin/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">

    <!-- Form step -->
    <link href="<?= base_url() ?>assets/admin/vendor/jquery-steps/css/jquery.steps.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/admin/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link href="<?= base_url() ?>assets/admin/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" />
	 <!-- Pick date -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/vendor/pickadate/themes/default.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/admin/vendor/pickadate/themes/default.date.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <link href="<?= base_url() ?>assets/admin/css/custom.css" rel="stylesheet">
	 <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/icon_font/css/icon_font.css" />
	<link href="<?= base_url() ?>assets/admin/css/jquery.transfer.css" rel="stylesheet">
	
	 
</head>
<script type="text/javascript">
var sibebarbg = "<?php echo $_SESSION['sadevelopers_admin_settings']['sidebar'] ?>";
var nav_headerbg = "<?php echo $_SESSION['sadevelopers_admin_settings']['navigation_header'] ?>";
var headerbg = "<?php echo $_SESSION['sadevelopers_admin_settings']['header'] ?>";
var font_family = "<?php echo $_SESSION['sadevelopers_admin_settings']['font_family'] ?>";
</script>
<body>
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <div id="main-wrapper">
		<?php include('include/navbar_header.php'); ?>
		<?php include('include/navbar.php'); ?>
		<?php include('include/sidebar.php'); ?>
		<?php $this->load->view($view);?>
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Ennoble Technologies</a> <?php echo date('Y'); ?> </p>
            </div>
        </div>
    </div>	
	<div class="modal fade" id="changePasswordm">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Change Password</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/users/password'), 'id="changePassword" class="form-horizontal"');   ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">
                <div class="form-group col-lg-12 col-md-12">
                  <input type="password" class="form-control" id="current" placeholder="*Current Password" name="current" maxLength="25">
                  <input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['sadevelopers_admin']['admin_id'];?>">
                  <input type="hidden" id="current_hidden" name="current_hidden">
                  <div id="current_response"></div>
                </div>																
                <div class="form-group col-lg-12 col-md-12">
                  <input type="password" class="form-control" id="Addpwd" placeholder="*New Password" name="Addpwd" maxLength="25"> 
                </div>
                <div class="form-group col-lg-12 col-md-12">
                  <input type="password" class="form-control" id="confirm" placeholder="*Confirm Password" name="confirm" maxLength="25">
                </div>
                <div class="col-lg-12 col-md-12">
                  <div class="form-group qa">
                    <div id="Length" class="glyphicon glyphicon-remove">Password must be at least 8 charcters</div>
                    <div id="UpperCase" class="glyphicon glyphicon-remove">Password must have atleast 1 upper case character</div>
                    <div id="LowerCase" class="glyphicon glyphicon-remove">Password must have atleast 1 lower case character</div>
                    <div id="Numbers" class="glyphicon glyphicon-remove">Password must have atleast 1 numeric character</div>
                    <div id="Symbols" class="glyphicon glyphicon-remove">Password must have atleast 1 special character</div>
                  </div>
                </div>
              </div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<!-- Required vendors -->
<!-- Commented for Drag and Drop in Communications Module -->
<script src="<?= base_url() ?>assets/admin/vendor/global/global.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
<!-- (Optional) Latest compiled and minified JavaScript translation files -->

<script src="<?= base_url() ?>assets/admin/js/custom.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/deznav-init.js"></script>
<!-- Datatable -->
<script src="<?= base_url() ?>assets/admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/plugins-init/datatables.init.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<!-- Jquery Validation -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/jquery-steps/build/jquery.steps.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/jquery-validation/jquery.validate.min.js"></script>
<!-- Form validate init -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/jquery.validate-init.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.js"></script>
<!-- Form step init -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/jquery-steps-init.js"></script>
<!-- Vectormap -->
<script src="<?= base_url() ?>assets/admin/vendor/jqvmap/js/jquery.vmap.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/jqvmap/js/jquery.vmap.world.js"></script>

<!-- chart js -->
<script src="<?= base_url() ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/chart.js/Chart.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/chartist/js/chartist.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>
<!--  flot-chart js -->
<script src="<?= base_url() ?>assets/admin/vendor/flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/flot/jquery.flot.resize.js"></script>

<!-- Chartist -->
<script src="<?= base_url() ?>assets/admin/vendor/chartist/js/chartist.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/plugins-init/chartist-init.js"></script>
<!-- Daterangepicker -->
<!-- momment js is must -->
<script src="<?= base_url() ?>assets/admin/vendor/moment/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- clockpicker -->
<script src="<?= base_url() ?>assets/admin/vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<!-- asColorPicker -->
<script src="<?= base_url() ?>assets/admin/vendor/jquery-asColor/jquery-asColor.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/jquery-asGradient/jquery-asGradient.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js"></script>
<!-- Material color picker -->
<script src="<?= base_url() ?>assets/admin/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<!-- pickdate -->
<script src="<?= base_url() ?>assets/admin/vendor/pickadate/picker.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/pickadate/picker.time.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/pickadate/picker.date.js"></script>
 
<!-- Daterangepicker -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/bs-daterange-picker-init.js"></script>
<!-- Clockpicker init -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/clock-picker-init.js"></script>
<!-- asColorPicker init -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/jquery-asColorPicker.init.js"></script>
<!-- Material color picker init -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/material-date-picker-init.js"></script>
<!-- Pickdate -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/pickadate-init.js"></script>
<!-- Demo scripts -->
<script src="<?= base_url() ?>assets/admin/js/dashboard/dashboard-5.js"></script>
<!-- Toastr -->
<script src="<?= base_url() ?>assets/admin/vendor/toastr/js/toastr.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/raphael/raphael.min.js"></script>
<!-- All init script -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/toastr-init.js"></script>
<!-- Fullcalendar scripts -->
<script src="<?= base_url() ?>assets/admin/vendor/jqueryui/js/jquery-ui.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/moment/moment.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/fullcalendar/js/fullcalendar.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/plugins-init/fullcalendar-init.js"></script>

<!-- Svganimation scripts -->
<script src="<?= base_url() ?>assets/admin/vendor/svganimation/vivus.min.js"></script>
<script src="<?= base_url() ?>assets/admin/vendor/svganimation/svg.animation.js"></script>
<script src="<?= base_url() ?>assets/admin/js/styleSwitcher.js"></script>
<script src="<?= base_url() ?>assets/admin/js/custom_validations.js"></script>
<script src="https://cdn.tiny.cloud/1/6vmr7bi0e890awywdao9hwtsybpqjngy8uvf9xhgjs8ra9mw/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
	$("input").on("keypress", function(e) { if (e.which === 32 && !this.value.length) e.preventDefault(); });
</script>
<script>
$('[data-fancybox="mygallery"]').fancybox({
  arrows : false,
  buttons : [ 
    'share',
    'zoom',
    'fullScreen',
    'close'
  ],
  thumbs : {
    autoStart : true
  },
  slideClass: "",
});	
$('body').on('change', '.custom-file-input', function () {
let fileName = $(this).val();
$(this).next("label").text(fileName.replace("C:\\fakepath\\", "")); 
});
</script>
<?php if($this->session->flashdata('success_msg')){?>
<script>
	$(function () {
		toastr.success("", "<?php echo $this->session->flashdata('success_msg'); ?>", {
			timeOut: 5000,
			closeButton: !0,
			debug: !1,
			AddestOnTop: !0,
			progressBar: !0,
			positionClass: "toast-top-right",
			preventDuplicates: !0,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			tapToDismiss: !1
		})
	})
</script>
<?php } ?>
<?php if($this->session->flashdata('danger_msg')){?>
<script>
	$(function () {
		toastr.error("", "<?php echo $this->session->flashdata('danger_msg'); ?>", {
			positionClass: "toast-top-right",
			timeOut: 5e3,
			closeButton: !0,
			debug: !1,
			AddestOnTop: !0,
			progressBar: !0,
			preventDuplicates: !0,
			onclick: null,
			showDuration: "300",
			hideDuration: "1000",
			extendedTimeOut: "1000",
			showEasing: "swing",
			hideEasing: "linear",
			showMethod: "fadeIn",
			hideMethod: "fadeOut",
			tapToDismiss: !1
		})
	 })
</script>
<?php } ?>
<!-- Toastr -->
<script src="<?= base_url() ?>assets/admin/vendor/toastr/js/toastr.min.js"></script>
<!-- All init script -->
<script src="<?= base_url() ?>assets/admin/js/plugins-init/toastr-init.js"></script>
 <!-- Sweetalert2 -->
<script src="<?= base_url() ?>assets/admin/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

<script>
	var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

	tinymce.init({
	selector: 'textarea.editor',
	plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
	imagetools_cors_hosts: ['picsum.photos'],
	menubar: 'file edit view insert format tools table help',
	toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
	toolbar_sticky: true,
	autosave_ask_before_unload: true,
	autosave_interval: '30s',
	autosave_prefix: '{path}{query}-{id}-',
	autosave_restore_when_empty: false,
	autosave_retention: '2m',
	image_advtab: true,
	link_list: [
		{ title: 'My page 1', value: 'https://www.tiny.cloud' },
		{ title: 'My page 2', value: 'http://www.moxiecode.com' }
	],
	image_list: [
		{ title: 'My page 1', value: 'https://www.tiny.cloud' },
		{ title: 'My page 2', value: 'http://www.moxiecode.com' }
	],
	image_class_list: [
		{ title: 'None', value: '' },
		{ title: 'Some class', value: 'class-name' }
	],
	importcss_append: true,
	file_picker_callback: function (callback, value, meta) {
		/* Provide file and text for the link dialog */
		if (meta.filetype === 'file') {
		callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
		}

		/* Provide image and alt text for the image dialog */
		if (meta.filetype === 'image') {
		callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
		}

		/* Provide alternative source and posted for the media dialog */
		if (meta.filetype === 'media') {
		callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
		}
	},
	templates: [
			{ title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
		{ title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
		{ title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
	],
	template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
	template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
	height: 200,
	image_caption: true,
	quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
	noneditable_noneditable_class: 'mceNonEditable',
	toolbar_mode: 'sliding',
	contextmenu: 'link image imagetools table',
	skin: useDarkMode ? 'oxide-dark' : 'oxide',
	content_css: useDarkMode ? 'dark' : 'default',
	content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
	});
</script>
<script> 
function BackDir() {
    var cnt= $("form").length;
    if(cnt>0){
        $("form").validate().settings.ignore="*";
    }
    window.history.back();
}
$(".refreshBtn").click(function(){
    var cnt= $("form").length;
    if(cnt>0){
        $("form").validate().settings.ignore="*";
    }
    location.reload();
});
var cnt= $("form").length;
if(cnt>0){
    
    $('body').on('keypress', '.us_date', function() {	
        if($(this).val()!=''){
            $(this).next('.error').hide();
        }
    });
	$('body').on('input', 'input', function() {
			$(this).next('.error').hide();
	});
	$('body').on('file', 'input', function() {
			$(this).next('.error').hide();
	});
	$('body').on('input', 'textarea', function() {
		$(this).next('.error').hide();
	});

}

<?php if($this->uri->segment(2)=='claims' &&$this->uri->segment(3)=='edit'){?>
<?php if($claim_data['nonworking_start_date']=='' && $claim_data['nonworking_end_date']==''){?>
$('#nonworking_dates').val('');
<?php }?>
<?php if($claim_data['hospital_start_date']=='' && $claim_data['hospital_end_date']==''){?>
$('#hospital_dates').val('');
<?php }?>
<?php if($claim_data['injury_date']=='' ){?>
$('#injury_date').val('');
<?php }?>
<?php if($claim_data['other_date']=='' ){?>
$('#other_date').val('');
<?php }?>
<?php }?>
</script>
<?php if($this->session->flashdata('cpass')){?>
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#changePasswordm').modal('show');
        });
    </script>
<?php } ?>
	
</body>

</html>