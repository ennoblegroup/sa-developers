<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<style>
.profile-info p {
    margin-bottom: 0px;
}
.profile-tab span,.form-control{
		font-size:15px;
}
.nav-tabs {
    border-bottom: 1px solid #dee2e6;
}
.card {
    height: unset;
    margin-bottom: 3.875rem;
}
.profile-pic {
    width: 200px;
	min-height:112px;
    display: block;
}
.profile .profile-photo {
    max-width: 140px;
    position: absolute;
    top: -140px;
    left: 30px;
}
.file-upload {
    display: none;
}
.circle {
    border-radius: 1000px !important;
    overflow: hidden;
    width: 128px;
    height: 128px;
    border: 8px solid rgba(255, 255, 255, 0.7);
    position: absolute;
    top: 72px;
}
img {
    max-width: 100%;
    height: auto;
}
.p-image {
  position: absolute;
  top: 167px;
  left: 100px;
  color: #666666;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.p-image:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
}
.upload-button {
  font-size: 1.8em;
}

.upload-button:hover {
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
  color: #999;
}
</style>
<div class="mini-header">
	<div class="">
      <div class="row page-titles mx-0">
         <div class="col-sm-4 p-md-0 ">
            <span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
               <button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                  <li class="breadcrumb-item active">View Profile</li>
               </ol>
               <button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
            </span>
         </div>
         <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
            <div class="welcome-text">
               
            </div>
         </div>
      </div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
        <div class="row">
			<div class="col-lg-12">
				<div class="profile">
					<div class="profile-head">
						<div class="photo-content">
							<div class="cover-photo"></div>
							
						</div>
						<div class="profile-info">
							
							<div class="row">
								<div class="col-sm-3">
									<div class="small-12 profile-photo medium-2 large-2 columns">
										 <div class="circle">
										   <!-- User Profile Image -->
										   <img class="profile-pic" src="<?php echo base_url()?>images/clients/<?php echo $user['image']?>">
										</div>
										<div class="p-image">
											<i class="fa fa-camera upload-button"></i>
											<input class="file-upload" type="file" accept="image/*"/>
										</div>
									</div>
								</div>
								<div class="col-sm-9 col-12">
									<div class="row">
										<div class="col-xl-4 col-sm-6 border-right-1">
											<div class="profile-name">
												<p>Name</p>
												<h4 class="text-primary mb-0"><?php echo $user['lastname']?> <?php echo $user['firstname']?></h4>
												
											</div>
										</div>
										<div class="col-xl-4 col-sm-6 border-right-1">
											<div class="profile-email">
												<p>Email Address</p>
												<h4 class="text-muted mb-0"><?php echo $user['email']?></h4>
												
											</div>
										</div>
										<!-- <div class="col-xl-4 col-sm-4 prf-col">
											<div class="profile-call">
												<h4 class="text-muted">(+1) 321-837-1030</h4>
												<p>Phone No.</p>
											</div>
										</div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="profile-tab">
							<div class="custom-tab-1">
								<ul class="nav nav-tabs">
									<li class="nav-item"><a href="#my-posts" data-toggle="tab" class="nav-link active show">Personal Details</a>
									</li>
									<li class="nav-item"><a href="#about-me" data-toggle="tab" class="nav-link">Address Details</a>
									</li>
									<li class="nav-item"><a href="#profile-settings" data-toggle="tab" class="nav-link">Edit Profile</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="my-posts" class="tab-pane fade active show">
										<div class="profile-personal-info pt-3">
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Last Name <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['lastname']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">First Name <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['firstname']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Middle Name <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['middlename']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Email Address<span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['email']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Phone Number <span class="pull-right">:</span></h5>
												</div>
												<div class="col-9"><span><?php echo $user['mobile_no']?></span>
												</div>
											</div>
										</div>
									</div>
									<div id="about-me" class="tab-pane fade">
										<div class="profile-personal-info pt-3">
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Address 1 <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['address1']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Address 2 <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['address2']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">City <span class="pull-right">:</span></h5>
												</div>
												<div class="col-9"><span><?php echo $user['city']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">State <span class="pull-right">:</span>
													</h5>
												</div>
												<div class="col-9"><span><?php echo $user['state']?></span>
												</div>
											</div>
											<div class="row mb-2">
												<div class="col-3">
													<h5 class="f-w-500">Zip Code <span class="pull-right">:</span></h5>
												</div>
												<div class="col-9"><span><?php echo $user['zip_code']?></span>
												</div>
											</div>
										</div>
									</div>
									<div id="profile-settings" class="tab-pane fade">
										<div class="pt-3">
											<div class="settings-form">
												<?php echo form_open(base_url('admin/users/edit/'.$user['id']), 'id="quickForm" class="form-horizontal"');  ?> 
													<div class="row">
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control" id="lastname" placeholder="*Last Name" title="*Last Name" name="lastname" maxLength="25" value="<?php echo $user['lastname']; ?>">
														</div>																
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control" id="firstname" placeholder="*First Name" title="*First Name" name="firstname" maxLength="25" value="<?= $user['firstname']; ?>">
														</div>
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control" id="middlename" placeholder="Middle Name" title="Middle Name" name="middlename" maxLength="25" value="<?= $user['middlename']; ?>">
														</div>
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control" id="email" placeholder="*E-Mail Address" title="*E-Mail Address" name="email" maxLength="50" value="<?= $user['email']; ?>">
														</div>
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control us_phone" id="mobile_no" 
															placeholder="*Phone Number" title="*Phone Number" name="mobile_no" value="<?= $user['mobile_no']; ?>">
														</div>
														<div class="form-group col-lg-12 col-md-12">
															<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40" value="<?= $user['address1']; ?>">
														</div>
														<div class="form-group col-lg-12 col-md-12">
															<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40" value="<?= $user['address2']; ?>">
														</div>
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30" value="<?= $user['city']; ?>">
														</div>
														<div class="form-group col-lg-4 col-md-4">											
															<select placeholder="*State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="state">
																<?php foreach($all_states as $state){ ?>
																	<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$user['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
																<?php }?>	
															</select>
														</div>
														<div class="form-group col-lg-4 col-md-4">
															<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="10" value="<?= $user['zip_code']; ?>">
														</div>
													</div>
													<button style="float:right;" class="btn btn-primary" type="submit" name="submit" value="submit">Update</button>
												<?php echo form_close( ); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>  
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
				var img = e.target.result;
				$.ajax({
					type:'POST',
					url:"<?php echo base_url('admin/users/update_profile_picture');?>",
					data:{'image': img},
					success: function(data){
						location.reload();    							 
					}
				});
            }
            reader.readAsDataURL(input.files[0]);
			
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
		
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

		if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
			return true;
		} else {
			return false;
		}
	}, "Please enter a valid e-mail address.");
	$('#quickForm').validate({
		rules: {
		  lastname: {
			required: true
		  },
		  firstname: {
			required: true
		  },
		  email: {
			email: true,
			validate_email: true
		  },
		  mobile_no: {
			required: true,
			minlength: 17
		  },
		  user_role: {
			required: true
		  },
		  address1: {
			required: true
		  },
		  city: {
			required: true
		  },
		  state: {
			required: true
		  },
		  zip_code: {
			required: true
		  },
		  
		},
		messages: {
		  lastname: {
			required: "Please enter Last Name"
		  }, 
		  firstname: {
			required: "Please enter First Name"
		  },
		   email: {
			email: "Please enter a valid E-Mail Address"
		  },
		  mobile_no: {
			required: "Please enter Phone Number",
			minlength: "Please enter a valid Phone Number"
		  },
		  user_role: {
			required: "Please select User Role"
		  },
		  address1: {
			required: "Please enter Address1"
		  },
		  city: {
			required: "Please enter city",
			minlength: "City must be at least 3 characters long"
		  },
		  state: {
			required: "Please select State"
		  },
		  zip_code: {
			required: "Please enter Zip Code",
			minlength: "Zip Code must be at least 5 digits long"
		  },
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		  error.addClass('invalid-feedback');
		  element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		  $(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		  $(element).removeClass('is-invalid');
		}
	});
    $('body').on('input', '#firstname', function() {
	   //alert(document.getElementById('lastname').value);
	   var uname=document.getElementById('lastname').value+document.getElementById('firstname').value;
	    $("#username").val(uname.toLowerCase());
    });  
});
</script>

