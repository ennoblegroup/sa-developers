<?php echo form_open(base_url('admin/users/add'), 'id="quickForm" class="form-horizontal"');  ?> 
	 
		<div class="mini-header">
			<div class="">
				<div class="row page-titles mx-0">
					<div class="col-sm-4 p-md-0 ">
						<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
							<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
								<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/users">All Users</a></li>
								<li class="breadcrumb-item active">Add User</li>
							</ol>
							<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
						</span>
					</div>
					<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<div class="welcome-text">
						<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
						<a type="button" href="<?php echo base_url()?>admin/users" class="form btn btn-outline-dark">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>	
        <div class="content-body">
            <div class="container-fluid">
                <!-- row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">USER DETAILS</h4>
							</div>
							<div class="card-body custom_body">
								<div class="form-validation">
								<div class="row">								
									<div class="col-md-12">
										<?php if(isset($msg) || validation_errors() !== ''): ?>
											<div class="alert alert-danger alert-dismissible">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<?= validation_errors();?>
												<?= isset($msg)? $msg: ''; ?>
											</div>
										<?php endif; ?>
										<div class="row">
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control" id="lastname" placeholder="*Last Name" title="*Last Name" name="lastname" maxLength="25">
											</div>																
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control" id="firstname" placeholder="*First Name" title="*First Name" name="firstname" maxLength="25">
											</div>
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control" id="middlename" placeholder="Middle Name" title="Middle Name" name="middlename" maxLength="25">
											</div>
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control" id="email" placeholder="*E-Mail Address" title="*E-Mail Address" name="email" maxLength="50">
											</div>									
										</div>
									</div>
								</div>
								<div class="row">								
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control" id="username" placeholder="*Username" title="*Usename" name="username" maxLength="50">
												<div id="uname_response" ></div>
											</div>
											<!--div class="form-group col-lg-3 col-md-3">
												<input type="password" class="form-control" id="password" placeholder="*Password" title="*Password" name="password" maxLength="25">
											</div-->
											<div class="form-group col-lg-3 col-md-3">
												<input type="text" class="form-control us_phone" id="mobile_no" placeholder="*Phone Number" title="*Phone Number" name="mobile_no">
											</div>
											<div class="form-group col-lg-3 col-md-3">
												<select name="user_role" title="*Select Role" id="user_role" class="form-control selectpicker" data-live-search="true">
													<option value="">Select Role</option>	
													<?php foreach($all_roles as $row): ?>
													<option value="<?= $row['id']; ?>" ><?= $row['rolename']; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div><hr>
							<h4 class="card-title">ADDRESS DETAILS</h4>
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<div class="row">											
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<select placeholder="*State" title="*Select State" class="form-control selectpicker" data-live-search="true" name="state" id="state">
												<option value="">Select State</option>
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="10">
										</div>
									</div>
									<!--div class="row">											
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="country" placeholder="*Country" title="*Country" name="country" value="USA" readonly>
										</div>
									</div-->
								</div>
							</div>
						</div>
							</div>
						</div>
					</div>
				</div>				
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">	
	
	$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid e-mail address.");


$(document).ready(function(){
	$('body').on('change input', '#username', function() {
		var user_name = $(this).val();
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/clients/fetch_uname",
		dataType: 'json',
		data: {"user_name": user_name},
		success: function(response) {
			if(response == false) {
				$(".erroruname").remove();
				$('#username').val("");
				$('#username').after('<span class="error erroruname" >Username is taken already</span>');
			} 
			else {
			 $(".erroruname").remove();
			}
			setTimeout(function(){
            $("#erroruname").hide();
            },3000)	
		},
		});
	});
});
$(document).ready(function(){
	$('body').on('change input', '#email', function() {
		var email = $(this).val();
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/clients/fetch_email",
		dataType: 'json',
		data: {"email": email},
		success: function(response) {
			if(response == false) {
				$(".error3").remove();
				$('#email').val("");
				$('#email').after('<span class="error error3" style="color:red">E-Mail Address has been registered already</span>');
				$('#email').valid();
			} 
			else {
			 $(".error3").remove();
			}
		},
		});
	});
}); 
$('#quickForm').validate({
    rules: {
      lastname: {
        required: true
      },
	  firstname: {
        required: true
      },
	  email: {
		required: true,
		email: true
      },
	  mobile_no: {
        required: true,
		minlength: 17
      },
	  user_role: {
        required: true
      },
	  address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true
      },
	  zip_code: {
        required: true
      },
	  
    },
    messages: {
	  lastname: {
        required: "Please enter Last Name"
      }, 
	  firstname: {
        required: "Please enter First Name"
      },
       email: {
        required: "Please enter E-Mail Address",
        email: "Please enter a valid E-Mail Address"
      },
	  mobile_no: {
        required: "Please enter Phone Number",
        minlength: "Please enter a valid Phone Number"
      },
	  user_role: {
        required: "Please select User Role"
      },
      address1: {
        required: "Please enter Address1"
      },
	  city: {
        required: "Please enter City",
        minlength: "City must be at least 3 characters long"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#firstname', function() {
	   //alert(document.getElementById('lastname').value);
	   var uname=document.getElementById('lastname').value+document.getElementById('firstname').value;
	    $("#username").val(uname.toLowerCase());
   });
    /*$('body').on('input', '#password', function() {		  
        var uname = document.getElementById('username').value;		
        if(uname){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/users/fetch_uname'); ?>',
                data:'user_name='+uname,
                success:function(response){	
					 $('#uname_response').html(response);  		
                }
            }); 
        }
		else{
         $("#uname_response").html("");
		}		
    });*/
	
  $('body').on('input', '#lastname', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
	$('body').on('input', '#firstname', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});
	$('body').on('input', '#middlename', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});  
	$('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
</script>
<script>
 /*$("#zip_code").on('input', function() {  //this is use for every time input change.
//alert("Hi");
       var inputValue = getInputValuez(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#zip_code").val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $("#zip_code").val(inputValue); //correct value entered to your input.
       inputValue = getInputValuez();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 200000000) && (inputValue < 999999999))
     {
         $("#zip_code").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#zip_code").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValuez() {
        var inputValue = $("#zip_code").val().replace(/\D/g,'');  //remove all non numeric character
        return inputValue;
}
$("#mobile_no").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValue(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#mobile_no").val('');
           return false;
       }    
       if (inputValue < 1000)
       {
           inputValue = '+1' + ' ' + '('+inputValue;
       }else if (inputValue < 1000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
       }else if (inputValue < 10000000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, length);
       }else
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, 10);
       }       
       $("#mobile_no").val(inputValue); //correct value entered to your input.
       inputValue = getInputValue();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 2000000000) && (inputValue < 9999999999))
     {
         $("#mobile_no").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#mobile_no").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValue() {
         var inputValue = $("#mobile_no").val().replace(/\D/g,'');  //remove all non numeric character
        if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
        {
            var inputValue = inputValue.substring(1, inputValue.length);
        }
        return inputValue;
}*/
	
</script>
<script>
$("#add_user").addClass('active');
</script>    