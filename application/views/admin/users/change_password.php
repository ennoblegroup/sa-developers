<?php echo form_open(base_url('admin/users/password'), 'id="quickForm" class="form-horizontal"');  ?> 
		<div class="mini-header">
			<div class="">
                <div class="row page-titles mx-0">
                     <div class="col-sm-4 p-md-0 ">
						<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
							<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active">Change Password</li>
								</ol>
							<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
						</span>
                    </div>
                     <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<div class="welcome-text">
							<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
							<a type="button" href="<?php echo base_url()?>admin/users/view_profile" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
						</div>
                    </div>
                </div>
			</div>
		</div>
        <div class="content-body">
            <div class="container-fluid">
                <!-- row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Change Password</h4>
							</div>
							<div class="card-body custom_body">
								<div class="form-validation">
									<div class="row">
                    <div class="col-md-4"></div>							
										<div class="col-md-4">
											<?php if(isset($msg) || validation_errors() !== ''): ?>
												<div class="alert alert-danger alert-dismissible">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													<?= validation_errors();?>
													<?= isset($msg)? $msg: ''; ?>
												</div>
											<?php endif; ?>
											<div class="row">
												<div class="form-group col-lg-12 col-md-12">
													<input type="password" class="form-control" id="current" placeholder="*Current Password" name="current" maxLength="25">
													<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION['sadevelopers_admin']['admin_id'];?>">
													<input type="hidden" id="current_hidden" name="current_hidden">
													<div id="current_response"></div>
												</div>																
												<div class="form-group col-lg-12 col-md-12">
													<input type="password" class="form-control" id="Addpwd" placeholder="*New Password" name="Addpwd" maxLength="25">
													
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="password" class="form-control" id="confirm" placeholder="*Confirm Password" name="confirm" maxLength="25">
												</div>								
											</div>
											<div  class="row">
											<div class="form-group col-lg-3 col-md-3"></div>
											<!--div>&nbsp;&nbsp;&nbsp;&nbsp;Use 8 or more characters with a mix of letters, numbers & symbols</div-->
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
				</div>				
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {	
    $.validator.addMethod("password_pattern", function(value, element, regexpr) {          
        return regexpr.test(value);
    });
    $('#quickForm').validate({
        rules: {
          current: {
            required: true
          },
        Addpwd: {
            required: true,
            minlength: 8,
            password_pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
          },
        confirm: {
            required: true,
            minlength: 8,
            equalTo: '[name="Addpwd"]'
          },    
        },
        messages: {
        current: {
            required: "Please enter current password"
          }, 
        Addpwd: {
            required: "Please enter New Password",
            minlength:"New Password must consist of at least 8 characters",
            password_pattern:"Your password must be have at least 8 characters long, 1 uppercase, 1 lowercase, 1 special character & 1 number"
          }, 
        confirm: {
            required: "Please enter Confirm Password",
            minlength:"Confirm Password must consist of at least 8 characters",
            equalTo: "Password doesn't Match"
          },   
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });   
    $('body').on('change', '#current', function() {	
		document.getElementById('current_hidden').value="";
        var current = document.getElementById('current').value;		
        var user_id = document.getElementById('user_id').value;
		//alert(user_id);
        if(current){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/users/fetch_currentpassword'); ?>',
                data:'current='+current,
                success:function(response){	
					$('#current_response').html(response); 
					$('#current_hidden').val(response); 										 
                }
            }); 
        }
		else{
         $("#current_response").html("");
		}		
    });

});
</script>
   