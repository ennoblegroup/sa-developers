<style>
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: -webkit-fill-available;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 5px 5px;
    border-bottom: 1px solid #111;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 5px !important;
}
.bootstrap-select .btn {
    border: 1px solid #eaeaea !important;
    background-color: #f8f9fa !important;
    padding: 5px 5px;
}
</style>
<?php	
			
$roleid= $_SESSION['sadevelopers_admin']['role_id'];

$this->db->where('role_id',$roleid);
$this->db->where('menu_id',1);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',1);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',1);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',1);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">All Users</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add User</button>
					<!--input type="text" id="search" placeholder="  live search"></input>
					<a href="<?= base_url('admin/users/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add User</button></a>
					<!--input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/users" class="form btn btn-outline-dark"><i class="fa fa-ban"></i>Cancel</a-->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body custom_body">	
						<table id="example3" class="ui celled table dataTable no-footer" style="width:100%">
							<thead>
								<tr>
									<th width="10%">User Id</th>
									<!--th>Username</th>
									<th>Last Name</th-->
									<th width="25%">Name</th>
									<th width="20%">Role</th>
									<th width="20%">E-Mail Address</th>
									<th class="<?php echo $user_classs;?>" width="15%">Status</th>
									<th width="10%">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($all_users as $row): ?>
								<tr>
									<td><?= $row['userid']; ?></td>
									<!--td><?= $row['username']; ?></td-->
									<td><?= $row['firstname']." ".$row['middlename']." ".$row['lastname']; ?></td>
									<td><?= $row['rolename']; ?></td>
									<td><?= $row['email']; ?></td>
									<td class="<?php echo $user_classs;?>"> 
										<label class="switch">
										  <input type="checkbox" <?php if($row['status_id']==1){echo 'checked';}?> id="status" user_sid="<?php echo $row['user_id']; ?>">
										  <span class="slider round"></span>
										</label>
										<span id="status_roww<?php echo $row['user_id']; ?>">
										<?php if($row['status_id']==1){?>
											<span>Active</span>
										<?php }else{ ?>
											<span>In-Active</span>
										<?php }?>
										</span>
									</td>
									<td> 
									<!--a href="<?= base_url('admin/users/edit/'.$row['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
									<a data-toggle="tooltip" title="Delete" href="#" class="removeRecord" user_id = "<?php echo $row['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash"></i></span></a--> 
									<a data-toggle="modal" data-target="#exampleModalLong-<?php echo $row['user_id'];?>" title="Edit" href="<?= base_url('admin/users/edit/'.$row['user_id']); ?>" class="btn btn-sm btn-outline-dark <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></a>
									<a data-toggle="tooltip" title="Delete" href="#" user_id = "<?php echo $row['user_id'];?>" class="removeRecord btn btn-sm btn-outline-dark <?php echo $user_classd;?>"><i class="fa fa-trash-o"></i></a>
									 </td>
								</tr>									
								<?php endforeach; ?>
							</tbody>
						</table>									
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add User</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/users/add'), 'id="quickForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">					
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="lastname" placeholder="*Last Name" title="*Last Name" name="lastname" maxLength="25">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="firstname" placeholder="*First Name" title="*First Name" name="firstname" maxLength="25">
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="middlename" placeholder="Middle Name" title="Middle Name" name="middlename" maxLength="25">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="email" placeholder="*E-Mail Address" title="*E-Mail Address" name="email" maxLength="50">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="username" placeholder="*Username" title="*Usename" name="username" maxLength="50">
						<div id="uname_response" ></div>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us_phone" id="mobile_no" placeholder="*Phone Number" title="*Phone Number" name="mobile_no">
					</div>										
					<div class="form-group col-lg-6 col-md-6">
						<select name="user_role" title="*Select Role" id="user_role" class="form-control selectpicker" data-live-search="true">
							<?php foreach($all_roles as $row): ?>
							<option value="<?= $row['id']; ?>" ><?= $row['rolename']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<select placeholder="State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="state">
							<?php foreach($all_states as $state){ ?>
								<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
							<?php }?>	
						</select>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="15">
					</div>
					<div class="col-lg-3 col-md-3"></div>
					<div class="col-lg-6 col-md-6">
						<div class="input-group err">
							<div class="custom-file">
								<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg">
								<label class="custom-file-label">Choose Profile Image</label>
							</div>
						</div>
						<div class="row">
							<img id="blah" style="padding-top:15px;width:100%;height: 200px;" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
						</div>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" name="submit" value="Reset" class="btn btn-outline-dark" onClick="resetValues()">Reset</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<?php 
if(count($all_users)>0)
{			
 foreach($all_users as $row): ?> 
<div class="modal fade" id="exampleModalLong-<?php echo $row['user_id'];?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit User</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/users/edit/'.$row['user_id']), 'id="editForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">					
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="lastname" placeholder="*Last Name" title="*Last Name" name="lastname" maxLength="25" value="<?php echo $row['lastname']; ?>">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="firstname" placeholder="*First Name" title="*First Name" name="firstname" maxLength="25" value="<?php echo $row['firstname']; ?>">
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="middlename" placeholder="Middle Name" title="Middle Name" name="middlename" maxLength="25" value="<?php echo $row['middlename']; ?>">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="eemail" placeholder="*E-Mail Address" title="*E-Mail Address" name="email" maxLength="50" value="<?php echo $row['email']; ?>">
						<input type="hidden" class="form-control" id="usr_id" value="<?php echo $row['user_id']; ?>" placeholder=""  name="id">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="username" placeholder="*Username" title="*Usename" name="username" maxLength="50" value="<?php echo $row['username']; ?>">
						<div id="uname_response" ></div>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us_phone" id="mobile_no" placeholder="*Phone Number" title="*Phone Number" name="mobile_no" value="<?php echo $row['mobile_no']; ?>">
					</div>										
					<div class="form-group col-lg-6 col-md-6">
						<select name="user_role" title="*Select Role" id="user_role" class="form-control selectpicker" data-live-search="true">
							<?php foreach($all_roles as $rol): ?>
							<option value="<?= $rol['id']; ?>" <?php if($rol['id']==$row['user_role']){ echo 'selected'; }?> ><?= $rol['rolename']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40" value="<?php echo $row['address1']; ?>">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40" value="<?php echo $row['address2']; ?>">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30" value="<?php echo $row['city']; ?>">
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<select placeholder="State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="state">
							<?php foreach($all_states as $state){ ?>
								<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$row['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
							<?php }?>	
						</select>
					</div>				
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="15" value="<?php echo $row['zip_code']; ?>">
					</div>				
				</div>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Update</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div> 
<?php
endforeach;
}
?>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_sid');
			//alert(user_id);
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/users/update_status');?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+user_id).load(" #status_roww"+user_id);
			  }
			});
		});
	});
function resetValues()
{		
	document.getElementById("quickForm").reset();
}
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
</script>
<script>
 $(".removeRecord").click(function(){
       var id = $(this).attr('user_id'); 
		//alert(id);
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/users/del');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: 'cancel',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
    });
</script>
<script type="text/javascript">	
	
	$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid e-mail address.");


$(document).ready(function(){
	$('body').on('change input', '#username', function() {
		var user_name = $(this).val();
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/clients/fetch_uname",
		dataType: 'json',
		data: {"user_name": user_name},
		success: function(response) {
			if(response == false) {
				$(".erroruname").remove();
				$('#username').val("");
				$('#username').after('<span class="error erroruname" >Username is taken already</span>');
			} 
			else {
			 $(".erroruname").remove();
			}
			setTimeout(function(){
            $("#erroruname").hide();
            },3000)	
		},
		});
	});
});
/*$(document).ready(function(){
	$('body').on('change input', '#email', function() {
		var email = $(this).val();
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>" + "admin/clients/fetch_email",
		dataType: 'json',
		data: {"email": email},
		success: function(response) {
			if(response == false) {
				$(".error3").remove();
				$('#email').val("");
				$('#email').after('<span class="error error3" style="color:red">E-Mail Address has been registered already</span>');
				$('#email').valid();
			} 
			else {
			 $(".error3").remove();
			}
		},
		});
	});
}); */
$('#quickForm').validate({
    rules: {
      lastname: {
        required: true
      },
	  firstname: {
        required: true
      },
	  email: {
		required: true,
		email: true
      },
	   email: {
        required: true,
		email: true,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/users/fetch_email",
			type: "post",
			data: {
			username: function() {
				return $( "#email" ).val();
			}
			}
		}
      },
	  mobile_no: {
        required: true,
		minlength: 17
      },
	  user_role: {
        required: true
      },
	  address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true
      },
	  zip_code: {
        required: true,
		minlength: 5
      },
	  
    },
    messages: {
	  lastname: {
        required: "Please enter Last Name"
      }, 
	  firstname: {
        required: "Please enter First Name"
      },
       email: {
        required: "Please enter E-Mail Address",
        email: "Please enter a valid E-Mail Address",
		remote:"E-Mail Address has been registered already"
      },
	  mobile_no: {
        required: "Please enter Phone Number",
        minlength: "Please enter a valid Phone Number"
      },
	  user_role: {
        required: "Please select User Role"
      },
      address1: {
        required: "Please enter Address1"
      },
	  city: {
        required: "Please enter City",
        minlength: "City must be at least 3 characters long"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('#editForm').validate({
    rules: {
      lastname: {
        required: true
      },
	  firstname: {
        required: true
      },	  
	  email: {
        required: true,
		email: true,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/users/fetch_email_edit",
			type: "post",
			data: {
			email: function() {
				return $( "#eemail" ).val();
			},
			usr_id: function() {
				return $( "#usr_id" ).val();
			}
			}
		}
      },
	  mobile_no: {
        required: true,
		minlength: 17
      },
	  user_role: {
        required: true
      },
	  address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true
      },
	  zip_code: {
        required: true,
		minlength: 5
      },
	  
    },
    messages: {
	  lastname: {
        required: "Please enter Last Name"
      }, 
	  firstname: {
        required: "Please enter First Name"
      },
       email: {
        required: "Please enter E-Mail Address",
        email: "Please enter a valid E-Mail Address",
		remote:"E-Mail Address has been registered already"
      },
	  mobile_no: {
        required: "Please enter Phone Number",
        minlength: "Please enter a valid Phone Number"
      },
	  user_role: {
        required: "Please select User Role"
      },
      address1: {
        required: "Please enter Address1"
      },
	  city: {
        required: "Please enter City",
        minlength: "City must be at least 3 characters long"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#firstname', function() {
	   //alert(document.getElementById('lastname').value);
	   var uname=document.getElementById('lastname').value+document.getElementById('firstname').value;
	    $("#username").val(uname.toLowerCase());
   });
    /*$('body').on('input', '#password', function() {		  
        var uname = document.getElementById('username').value;		
        if(uname){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/users/fetch_uname'); ?>',
                data:'user_name='+uname,
                success:function(response){	
					 $('#uname_response').html(response);  		
                }
            }); 
        }
		else{
         $("#uname_response").html("");
		}		
    });*/
	
  $('body').on('input', '#lastname', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
	$('body').on('input', '#firstname', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});
	$('body').on('input', '#middlename', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});  
	$('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
</script>
<script>
$("#nav_users").addClass('active');
</script>        
