<?php echo form_open(base_url('admin/users/edit_profile/'.$this->session->userdata('admin_id')), 'id="quickForm" class="form-horizontal"');  ?> 

		<div class="mini-header">
			<div class="">
                <div class="row page-titles mx-0">
                    <div class="col-sm-4 p-md-0 ">
						<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
							<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active">Edit Profile</li>
								</ol>
							<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
						</span>
                    </div>
                    <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<div class="welcome-text">
							<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
							<a type="button" href="<?php echo base_url()?>admin/users/view_profile" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
						</div>
                    </div>
                </div>
			</div>
		</div>
        <div class="content-body">
            <div class="container-fluid">
                <!-- row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">USER DETAILS</h4>
							</div>
							<div class="card-body custom_body">
								<div class="form-validation">
									<div class="row">								
										<div class="col-md-12">
											<?php if(isset($msg) || validation_errors() !== ''): ?>
												<div class="alert alert-danger alert-dismissible">
													<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
													<?= validation_errors();?>
													<?= isset($msg)? $msg: ''; ?>
												</div>
											<?php endif; ?>
											<div class="row">
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="lastname" placeholder="*Last Name" title="*Last Name" name="lastname" maxLength="25" value="<?php echo $user['lastname']; ?>">
													<input type="hidden" id="user_id" name="user_id" value="<?php echo $user['id'];?>">
												</div>																
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="firstname" placeholder="*First Name" title="*First Name" name="firstname" maxLength="25" value="<?= $user['firstname']; ?>">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="middlename" placeholder="Middle Name" title="Middle Name" name="middlename" maxLength="25" value="<?= $user['middlename']; ?>">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="email" placeholder="*Email" title="*Email" name="email" maxLength="50" value="<?= $user['email']; ?>">
												</div>									
											</div>
										</div>
									</div>
									<div class="row">								
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="username" placeholder="User Name" title="*User Name" name="username" value="<?= $user['username']; ?>" readonly>
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="mobile_no" placeholder="*Mobile No" title="*Mobile No" name="mobile_no" value="<?= $user['mobile_no']; ?>">
												</div>
												<?php if($_SESSION['sadevelopers_admin']['is_admin']!=1){?>
												<!--div class="form-group col-lg-3 col-md-3">
													<select name="user_role" id="user_role" title="*User Role" class="form-control">
														<option value="">*Select Role</option>
														<?php foreach($all_roles as $row): ?>
														<option value="<?= $row['id']; ?>" <?php if($row['id']==$user['user_role']){ echo 'selected'; }?>><?= $row['rolename']; ?></option>
														<?php endforeach; ?>
													</select>
												</div-->
												<?php } 
												else
												{
												?>
												<input type="hidden" id="user_role" name="user_role" value="<?php echo $user['user_role'];?>">
												<?php
												}
												?>
											</div>
										</div>
									</div>
									<div class="card-header">
										<h4 class="card-title">ADDRESS DETAILS</h4>
									</div>
									<div class="row">								
										<div class="col-md-12">
										<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
											<div class="row">											
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40" value="<?= $user['address1']; ?>">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40" value="<?= $user['address2']; ?>">
												</div>
												<div class="form-group col-lg-2 col-md-2">
													<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30" value="<?= $user['city']; ?>">
												</div>
												<div class="form-group col-lg-2 col-md-2">											
													<select placeholder="State" title="State" class="form-control"  name="state" id="state">
														<option value="">*Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$user['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-2 col-md-2">
													<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="10" value="<?= $user['zip_code']; ?>">
												</div>
											</div>
										<?php } 
										else { 
										$id=$_SESSION['sadevelopers_admin']['client_id'];
										$user = array();
										$query=$this->db->query("SELECT * FROM clients where id=$id");
										$user   = $query->result_array();										
										?>
											<div class="row">											
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40" value="<?= $user[0]['address1']; ?>">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40" value="<?= $user[0]['address2']; ?>">
												</div>
												<div class="form-group col-lg-2 col-md-2">
													<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30" value="<?= $user[0]['city']; ?>">
												</div>
												<div class="form-group col-lg-2 col-md-2">											
													<select placeholder="State" title="State" class="form-control"  name="state" id="state">
														<option value="">*Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$user[0]['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-2 col-md-2">
													<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="10" value="<?= $user[0]['zip_code']; ?>">
												</div>
											</div>
										<?php } ?>
											<!--div class="row">											
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="country" placeholder="*Country" title="*Country" name="country" value="USA" readonly>
												</div>
											</div-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">ADDRESS DETAILS</h4>
							</div>
							<div class="card-body custom_body">
								<div class="form-validation">
									
								</div>
							</div>
						</div>
					</div>
				</div-->
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid email.");
$('#quickForm').validate({
    rules: {
      lastname: {
        required: true
      },
	  firstname: {
        required: true
      },
	  email: {
		email: true,
        validate_email: true
      },
	  mobile_no: {
        required: true
      },
	  address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true
      },
	  zip_code: {
        required: true
      },
	  
    },
    messages: {
	  last_name: {
        required: "Please enter last name"
      }, 
	  first_name: {
        required: "Please enter first name"
      }, 
       email: {
        email: "Please enter a valid email"
      },
	  mobile_no: {
        required: "Please enter mobile no"
      },
      address1: {
        required: "Please enter address 1"
      },
	  city: {
        required: "Please enter city",
        minlength: "City must be at least 3 characters long"
      },
	  state: {
        required: "Please select state"
      },
	  zip_code: {
        required: "Please enter zip code",
        minlength: "Zip Code must be at least 5 digits long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#firstname', function() {
	   //alert(document.getElementById('lastname').value);
	   var uname=document.getElementById('lastname').value+document.getElementById('firstname').value;
	    $("#username").val(uname.toLowerCase());
   });
    /*$('body').on('input', '#password', function() {		  
        var uname = document.getElementById('username').value;		
        if(uname){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/users/fetch_uname'); ?>',
                data:'user_name='+uname,
                success:function(response){	
					 $('#uname_response').html(response);  		
                }
            }); 
        }
		else{
         $("#uname_response").html("");
		}		
    }); */
  $('body').on('input', '#lastname', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
	$('body').on('input', '#firstname', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});
	$('body').on('input', '#middlename', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});  
	$('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
</script>
<script>
$("#zip_code").on('input', function() {  //this is use for every time input change.
//alert("Hi");
       var inputValue = getInputValuez(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#zip_code").val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $("#zip_code").val(inputValue); //correct value entered to your input.
       inputValue = getInputValuez();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 200000000) && (inputValue < 999999999))
     {
         $("#zip_code").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#zip_code").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValuez() {
        var inputValue = $("#zip_code").val().replace(/\D/g,'');  //remove all non numeric character
        return inputValue;
}
 $("#mobile_no").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValue(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#mobile_no").val('');
           return false;
       }    
       if (inputValue < 1000)
       {
           inputValue = '+1' + ' ' + '('+inputValue;
       }else if (inputValue < 1000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
       }else if (inputValue < 10000000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, length);
       }else
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, 10);
       }       
       $("#mobile_no").val(inputValue); //correct value entered to your input.
       inputValue = getInputValue();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 2000000000) && (inputValue < 9999999999))
     {
         $("#mobile_no").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#mobile_no").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValue() {
         var inputValue = $("#mobile_no").val().replace(/\D/g,'');  //remove all non numeric character
        if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
        {
            var inputValue = inputValue.substring(1, inputValue.length);
        }
        return inputValue;
}

</script>
</script> 
<script>
$("#add_user").addClass('active');
</script>    