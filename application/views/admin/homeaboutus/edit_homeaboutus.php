<?php echo form_open(base_url('admin/homeaboutus/edit/'.$homeaboutus[0]['id']), 'id="quickForm" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/homeaboutus">Home About us</a></li>
					<li class="breadcrumb-item active">Edit Home About Us</li>
				</ol>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<input type="submit" name="submit" value="Update" class="btn btn-primary">&nbsp;
				<a type="button" href="<?php echo base_url()?>admin/homeaboutus" class="form btn btn-danger"><i class="fa fa-ban"></i>Cancel</a>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Home Aboutus Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; 
									$i=1; foreach ($homeaboutus as $key=>$srv){ ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="title" placeholder="Title" value="<?php echo $srv['title']?>" name="title" maxLength="25">
										</div>
									</div>
									<div class="row">	
										<div class="form-group col-lg-12 col-md-12">
										  <textarea class="editor" name="description" id="description" required>
										<?php  echo $srv['description']; ?>
									</textarea>
                                            </div>										
									</div>
									<input type="hidden" value="<?php echo $srv['id']; ?>" name="id"> 
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
	<?php echo form_close( ); ?>
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">  
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      title: {
        required: true,
        minlength: 3
      },
      description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
      title: {
        required: "Please enter  Title",
        minlength: " Title must be at least 3 characters long"
      },
      description: {
        required: "Please enter  Description",
        minlength: " Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#title', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
});
</script>
<script>
$("#nav_homeaboutus").addClass('active');
</script> 