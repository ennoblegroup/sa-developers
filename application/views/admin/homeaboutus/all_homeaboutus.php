<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Home About Us</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Home About Us</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="display" style="width:100%">
								<thead>
									<tr>
										<th> ID</th>
										<th>Title</th>
										<th> Description</th>
										<th>Status</th>												
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_homeaboutus as $key=>$srv){ 
										
									?>
									<tr>
									<!--td style="display:none"><?php echo $i; ?></td-->
										<td> <?php echo $srv['id']?> </td>
										
										<td> <?php echo $srv['title']?> </td>
										<td> <?php echo $srv['description']?></td>
										<!--td> <?php echo date("F j, Y , g:i a", strtotime($srv['create_date'])); ?> </td-->
										
										<td> 
											<label class="switch">
											  <input type="checkbox" <?php if($srv['status']==1){echo 'checked';}?> id="status" homeaboutus_aid="<?php echo $srv['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $srv['id']; ?>">
											<?php if($srv['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>

											<?php }?>
											</span>
										</td>
										<td> <a href="<?= base_url('admin/homeaboutus/edit/'.$srv['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
										</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var homeaboutus_id = $(this).attr('homeaboutus_aid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/homeaboutus/update_status');?>",
			  data: {id: id, homeaboutus_id: homeaboutus_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+homeaboutus_id).load(" #status_roww"+homeaboutus_id);
			  }
			});
		});
	});
</script>

<script>
$("#nav_homeaboutus").addClass('active');
</script> 