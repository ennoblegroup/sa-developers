<link href="<?= base_url() ?>assets/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">       
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/evo-calendar/css/evo-calendar.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/evo-calendar/css/evo-calendar.orange-coral.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/evo-calendar/css/evo-calendar.midnight-blue.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/evo-calendar/css/evo-calendar.royal-navy.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/demo.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.0/fullcalendar.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.0/fullcalendar.print.css" rel="stylesheet"  media="print" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>


<style>
.fc-toolbar.fc-header-toolbar {
    margin: 5px 10px;
}
.fc-view {
    margin-top:5px;
}
.fc-scroller.fc-day-grid-container {
    overflow: scroll;
    height: 206px !important;
}
.fc-day{
    border-style: solid !important;
    border-width: 1px !important;
    padding: 0 !important;
    vertical-align: top !important;
    border-color: #ddd !important;
} 
.social-graph-wrapper .s-icon a{
    font-size: 15px;
    position: relative;
    padding: 0 10px;
    color: white;
    text-decoration: underline;
    font-weight: 600;
    text-transform: uppercase;
}
.as{
    margin-bottom: 0px;
}
.event-indicatorr {
    position: absolute;
    width: -moz-max-content;
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
    top: 16px;
    left: 7%;
    -webkit-transform: translate(-50%, calc(-100% + -5px));
    -ms-transform: translate(-50%, calc(-100% + -5px));
    transform: translate(-50%, calc(-100% + -5px));
}
.royal-navy .calendar-sidebar>.month-list>.calendar-months {
    padding: 0 5px 0 0;
}
.calendar-months{
    overflow: scroll;
}

.calendar-months::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

.calendar-months::-webkit-scrollbar
{
	width: 6px;
	background-color: #F5F5F5;
}

.calendar-months::-webkit-scrollbar-thumb
{
	background-color: #000000;
}

.event-indicatorr>.type-bullet {
    float: left;
    padding: 4px;
}
.event-containerr>.event-icon>div.event-bullet-birthday, .event-indicatorr>.type-bullet>div.type-birthday {
    background-color: #3ca8ff;
}
.event-indicatorr>.type-bullet>div {
    width: 10px;
    height: 10px;
    border-radius: 50%;
}
.calendar-sidebar>.month-list>.calendar-months>li {
	font-size:15px !important;
	font-weight:200 !important;
}

element.style {
}
.royal-navy .calendar-sidebar {
    background-color: #216583;
    -webkit-box-shadow: 5px 0 18px -3px #216583;
    box-shadow: 5px 0 18px -3px #216583;
}

tr.calendar-body .calendar-day {
    padding: 5px 0 !important;
}
table.dataTable thead th 
{
	text-transform:none;
	font-size: 14px;
}
.custom_body
{
	min-height: auto !important;
}
table.dataTable tbody td 
{
	text-align:center;
	font-size: 72px;
}

.counter
{
	font-size: 23px;
}
.google-visualization-controls-categoryfilter
{
	padding-right: 207px !important;
}
.card {
    height: unset;
}
.border-right {
    border-right: 1px solid #EEEEEE !important;
}
.social-graph-wrapper {
    background: #6a6a6a;
}
table thead {
     background: unset; 
	 color: unset; 
     border: unset;
     text-transform: unset; 
}

table tbody, table tfoot, table thead {
     border: unset; 
    background-color: unset; 
}
.evo-calendar {
	z-index:0;
}
.chartt{
    font-size: 16px;
    font-weight: 500;
    color: #454545;
    text-transform: capitalize;
    letter-spacing: 0.05em;
}
</style>


<div class="mini-header">
	<div class="">
        <div class="row page-titles mx-0">
          <div class="col-sm-4 p-md-0 ">
            <span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
              <button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
              </ol>
              <button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
            </span>
          </div>
          <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
            <div class="welcome-text">
              
            </div>
          </div>
        </div>
	</div>
</div>
<!--**********************************
	Content body start
***********************************-->
<div class="content-body custom_body">
	<!-- row -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-xxl-6 col-lg-6 col-md-6">
				<div class="row rty">
				    <?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
					<div class="col-xl-4 col-lg-4 col-sm-12 col-xxl-4 col-md-4">
						<div class="card">
							<div class="social-graph-wrapper">
								<span class="s-icon"><a href="<?php echo base_url()?>admin/clients">Clients</a></span>
							</div>
							<div class="row">
								<div class="col-6 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $active_clients; ?></span></h4>
										<p class="m-0">Active</p>
									</div>
								</div>
								<div class="col-6">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $inactive_clients; ?></span></h4>
										<p class="m-0">Inactive</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }?>
					<div class="col-xl-8 col-lg-8 col-sm-12 col-xxl-8 col-md-8">
						<div class="card">
							<div class="social-graph-wrapper">
								<span class="s-icon"><a href="<?php echo base_url()?>admin/projects">Projects</a></span>
							</div>
							<div class="row">
								<div class="col-3 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $new_projects; ?></span></h4>
										<p class="m-0">New</p>
									</div>
								</div>
								<div class="col-3 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $inprogress_projects; ?></span></h4>
										<p class="m-0">Inprogress</p>
									</div>
								</div>
								<div class="col-3 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $completed_projects; ?></span></h4>
										<p class="m-0">Completed</p>
									</div>
								</div>
								<div class="col-3">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $other_projects; ?></span></h4>
										<p class="m-0">Other</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
					<div class="col-xl-4 col-lg-4 col-sm-12 col-xxl-4 col-md-4">
						<div class="card as">
							<div class="social-graph-wrapper">
								<span class="s-icon"><a href="<?php echo base_url()?>admin/vendors">Vendors</a></span>
							</div>
							<div class="row">
								<div class="col-6 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $active_vendors; ?></span></h4>
										<p class="m-0">Active</p>
									</div>
								</div>
								<div class="col-6">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $inactive_vendors; ?></span></h4>
										<p class="m-0">Inactive</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-8 col-lg-8 col-sm-12 col-xxl-8 col-md-8">
						<div class="card as">
							<div class="social-graph-wrapper">
								<span class="s-icon"><a href="<?php echo base_url()?>admin/products">Products</a></span>
							</div>
							<div class="row">
								<div class="col-4 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $product_categories; ?></span></h4>
										<p class="m-0">Categories</p>
									</div>
								</div>
								<div class="col-4 border-right">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $active_products; ?></span></h4>
										<p class="m-0">Active Products</p>
									</div>
								</div>
								<div class="col-4">
									<div class="pt-3 pl-0 pr-0 text-center">
										<h4 class="m-1"><span class="counter"><?php echo $inactive_products; ?></span></h4>
										<p class="m-0">Inactive Products</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
			<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
			<div class="col-xl-6 col-lg-6 col-sm-12 col-xxl-6 col-md-6">
				<div class="card eee">
					<div class="row" style="margin: 10px;">
						<div class="form-group col-lg-12 col-md-12">
						<span class="event-indicatorr">
							<div class="type-bullet"><div class="type-birthday"></div></div>
							Appointments
						</span>
						</div>
					</div>
					<div id='calendarr'></div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-sm-12 col-xxl-6 col-md-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title chartt">Projects by Status</h4>
					</div>
					<div class="">
						<div id="chartContainer" class="cvv" style="height: 320px; width: 100%;"></div>
					</div>
				</div>
			</div>
			
			<div class="col-xl-6 col-lg-6 col-sm-12 col-xxl-6 col-md-6">
				<div class="card cv">
					<div class="card-header">
						<h4 class="card-title chartt">Projects by Services</h4>
					</div>
				    <div class="row" style="margin: 10px;">
						<div class="form-group col-lg-4 col-md-4"></div>
						<div class="form-group col-lg-4 col-md-4">
							<select name="service[]"  multiple="multiple" title="Service" data-live-search="true" id="service" class="selectpicker form-control" >
								<?php $i=1; foreach($all_services as $row): ?>
								<option value="<?php echo $row['id'];?>" <?php if($i<=5){echo 'selected';}?> ><?= $row['service_name']; ?></option>
								<?php $i++; endforeach; ?>
							</select>
						</div>
					</div>
					<div id="chartContainer2" style="height: 350px; width: 100%;"></div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-sm-12 col-xxl-6 col-md-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title chartt">Product Categories by Products and Vendors</h4>
					</div>
					<div class="row" style="margin: 10px;">
						<div class="form-group col-lg-4 col-md-4"></div>
						<div class="form-group col-lg-4 col-md-4">
							<select name="category[]"  multiple="multiple" title="Category" data-live-search="true" id="category" class="selectpicker form-control" >
								<?php $i=1; foreach($all_categories as $row): ?>
								<option value="<?php echo $row['id'];?>" <?php if($i<=5){echo 'selected';}?> ><?= $row['category_name']; ?></option>
								<?php $i++; endforeach; ?>
							</select>
						</div>
					</div>
					<div id="chartContainer1" style="height: 300px; width: 100%;"></div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-sm-12 col-xxl-6 col-md-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title chartt">Projects over a Year</h4>
					</div>
					<div class="row" style="margin: 10px;">
						<div class="form-group col-lg-4 col-md-4"></div>
						<div class="form-group col-lg-4 col-md-4">
							<select name="year"  title="Year" data-live-search="true" id="year" class="selectpicker form-control" >
								<?php 
								$curYear = date('Y');
								foreach(range($curYear - 3, $curYear) as $year) {
									if($curYear==$year){
										$wer= 'selected';
									}else{
										$wer= '';
									}
									echo "<option value='$year' $wer>$year</option>";
								}?>
							</select>
						</div>
					</div>
					<div id="chartContainer3" style="height: 300px; width: 100%;"></div>
				</div>
			</div>
			<?php }?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/evo-calendar/js/evo-calendar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.3.0/fullcalendar.min.js"></script>

<script>
$(document).ready(function(){
    var calendar = $("#calendarr").fullCalendar({  
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        navLinks: true, 
        editable: true,
        eventLimit: true, 
        events: <?php echo json_encode($appointments, JSON_NUMERIC_CHECK); ?>,
        eventRender: function(event, element) {
            element.attr('title', event.description);
        }// request to load current events
       
    });
    
 });
$('#calenda===rr').evoCalendar({
    theme: 'Royal Navy',
	format: "MM dd, yyyy",
	titleFormat: "MM",
	calendarEvents: <?php echo json_encode($appointments, JSON_NUMERIC_CHECK); ?>
});
$(document).ready(function() {
    var year = $("#year").val();
	getp(year);
	
	var category = $("#category").val();
	getc(category);
	
	var service = $("#service").val();
	gets(service);
});
$('#year').on('change',function(){
	var year = $(this).val();
	getp(year);
});
$('#service').on('change',function(){
	var service = $(this).val();
	gets(service);
});
$('#category').on('change',function(){
	var category = $(this).val();
	getc(category);
});
window.onload = function() {
 
var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2",
	animationEnabled: true,
	data: [{
		type: "doughnut",
		indexLabel: "{symbol} - {y}",
		yValueFormatString: "#,##0.0\"%\"",
		showInLegend: true,
		legendText: "{label} : {y}",
		dataPoints: <?php echo json_encode($projects_vs_status, JSON_NUMERIC_CHECK); ?>
	}]
});
chart.render();

 
function toggleDataSeries(e){
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else{
		e.dataSeries.visible = true;
	}
	chart1.render();
}



}
function getp(year){
	if(year){
		$.ajax({
			type:'POST',
			url:'dashboard/get_prjects_by_months',
			data:'year='+year,
			success:function(data){
				var dataObj = jQuery.parseJSON(data);
				var chart3 = new CanvasJS.Chart("chartContainer3", {
					axisY: {
						title: "Number of Projects"
					},
					data: [{
						type: "spline",
						dataPoints: dataObj
					}]
				});
				 
				chart3.render();
			}
		}); 
	}else{
		
	}
}
function getc(category){
	if(category){
		$.ajax({
			type:'POST',
			url:'dashboard/get_vendors_and_products',
			data:'category='+category,
			success:function(data){
				var dataObj = jQuery.parseJSON(data);
				
				var chart1 = new CanvasJS.Chart("chartContainer1", {
					animationEnabled: true,
					theme: "light2",
					axisY:{
						includeZero: true
					},
					legend:{
						cursor: "pointer",
						verticalAlign: "center",
						horizontalAlign: "right",
						//itemclick: toggleDataSeries
					},
					data: [{
						type: "column",
						name: "vendors",
						indexLabel: "{y}",
						showInLegend: true,
						dataPoints: dataObj.category_vs_vendors
					},{
						type: "column",
						name: "Products",
						indexLabel: "{y}",
						showInLegend: true,
						dataPoints: dataObj.category_vs_products
					}]
				});
				chart1.render();
			}
		}); 
	}else{
		
	}
}
function gets(service){
	if(service){
		$.ajax({
			type:'POST',
			url:'dashboard/get_project_by_services',
			data:'service='+service,
			success:function(data){
				var dataObj = jQuery.parseJSON(data);
				
				var chart2 = new CanvasJS.Chart("chartContainer2", {
                	animationEnabled: true,
                	theme: "light1", // "light1", "light2", "dark1", "dark2"
                	axisY: {
                		title: "Number of Projects"
                	},
                	data: [{
                		type: "column",
                		dataPoints: dataObj.project_vs_services
                	}]
                });
                chart2.render();
			}
		}); 
	}else{
		
	}
}
$(document).ready(function(){  
	var height=$('.rty').height();
	$('.eee').css('height',height+'px');
	
	var heights=$('.cvv').height();
	$('.cv').css('height',heights+'px');
});
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>    

		