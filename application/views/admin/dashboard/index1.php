<style>
table.dataTable thead th 
{
	text-transform:none;
	font-size: 14px;
}
.custom_body
{
	min-height: auto !important;
}
table.dataTable tbody td 
{
	text-align:center;
	font-size: 72px;
}
p
{
	white-space: nowrap;
    font-size: 14px;
}
.counter
{
	font-size: 23px;
}
.google-visualization-controls-categoryfilter
{
	padding-right: 207px !important;
}
</style>
 <link href="<?= base_url() ?>assets/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
 <link href="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">       
	  
<div class="mini-header">
	<div class="">
        <div class="row page-titles mx-0">
          <div class="col-sm-4 p-md-0 ">
            <span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
              <button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
              </ol>
              <button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
            </span>
          </div>
          <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
            <div class="welcome-text">
              
            </div>
          </div>
        </div>
	</div>
</div>
<!--**********************************
	Content body start
***********************************-->
<div class="content-body custom_body">
	<!-- row -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-3 col-xxl-3 col-lg-12 col-md-12">
				<div class="row">
					<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
						<div class="card bg-purpal">
							<div class="card-body pb-0">
								<div class="row">
									<div class="col">
										<h5 class="text-white">Power</h5>
										<span class="text-white">2017.1.20</span>
									</div>
									<div class="col text-right">
										<h5 class="text-white"><i class="fa fa-caret-up"></i> 260</h5>
										<span class="text-white">+12.5(2.8%)</span>
									</div>
								</div>
							</div>
							<div class="chart-wrapper">
								<canvas id="chart_widget_1" height="122"></canvas>
							</div>
						</div>
					</div>
					<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
						<div class="card">
							<div class="card-body pb-0">
								<div class="row">
									<div class="col">
										<h5>3650</h5>
										<span>VIEWS OF YOUR PROJECT</span>
									</div>
								</div>
							</div>
							<div class="chart-wrapper">
								<canvas id="chart_widget_2" height="122"></canvas>
							</div>
						</div>
					</div>
					<div class="col-xl-12 col-lg-4 col-md-4 col-sm-12">
						<div class="card">
							<div class="card-body">
								<h5>Latency</h5>
							</div>
							<div class="chart-wrapper">
								<div id="chart_widget_17"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-xxl-9 col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-4">
						<div class="row">
							<div class="col-12">
								<div class="card">
									<div class="card-body">
										<h4 class="card-intro-title">Calendar</h4>

										<div class="">
											<div id="external-events" class="my-3">
												<p>Drag and drop your event or click in the calendar</p>
												<div class="external-event" data-class="bg-primary"><i class="fa fa-move"></i>Add Theme Release</div>
												<div class="external-event" data-class="bg-success"><i class="fa fa-move"></i>My Event
												</div>
												<div class="external-event" data-class="bg-warning"><i class="fa fa-move"></i>Meet manager</div>
												<div class="external-event" data-class="bg-danger"><i class="fa fa-move"></i>Budgut</div>
												<div class="external-event" data-class="bg-success"><i class="fa fa-move"></i>Meet team</div>
												<div class="external-event" data-class="bg-info"><i class="fa fa-move"></i>Sales</div>
												<div class="external-event" data-class="bg-warning"><i class="fa fa-move"></i>Meet manager</div>
												<div class="external-event" data-class="bg-dark"><i class="fa fa-move"></i>Create Add theme</div>
											</div>
											<!-- checkbox -->
											<div class="checkbox custom-control checkbox-event custom-checkbox pt-3 pb-5">
												<input type="checkbox" class="custom-control-input" id="drop-remove">
												<label class="custom-control-label" for="drop-remove">Remove After Drop</label>
											</div>
											<a href="javascript:void()" data-toggle="modal" data-target="#add-category" class="btn btn-outline-dark btn-event w-100">
												<span class="align-middle"><i class="ti-plus"></i></span> Create Add
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="widget-stat card">
									<div class="card-body">
										<div class="media ai-icon">
											<span class="mr-3">
												<!-- <i class="ti-user"></i> -->
												<svg id="icon-customers" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
													<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
													<circle cx="12" cy="7" r="4"></circle>
												</svg>
											</span>
											<div class="media-body">
												<p class="mb-1">Customers</p>
												<h4 class="mb-0">328.50K</h4>
												<span class="badge badge-primary">+3.5%</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						 
					</div>
					<div class="col-lg-8">
						<div class="card">
							<div class="card-body">
								<div id="calendar" class="app-fullcalendar"></div>
							</div>
						</div>
					</div>
					<!-- BEGIN MODAL -->
					<div class="modal fade none-border" id="event-modal">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title"><strong>Add Add Event</strong></h4>
								</div>
								<div class="modal-body"></div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-success save-event waves-effect waves-light">Create
										event</button>

									<button type="button" class="btn btn-outline-dark delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
								</div>
							</div>
						</div>
					</div>
					<!-- Modal Add Category -->
					<div class="modal fade none-border" id="add-category">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title"><strong>Add a category</strong></h4>
								</div>
								<div class="modal-body">
									<form>
										<div class="row">
											<div class="col-md-6">
												<label class="control-label">Category Name</label>
												<input class="form-control form-white" placeholder="Enter name" type="text" name="category-name">
											</div>
											<div class="col-md-6">
												<label class="control-label">Choose Category Color</label>
												<select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
													<option value="success">Success</option>
													<option value="danger">Danger</option>
													<option value="info">Info</option>
													<option value="pink">Pink</option>
													<option value="primary">Primary</option>
													<option value="warning">Warning</option>
												</select>
											</div>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-outline-dark waves-effect waves-light save-category" data-dismiss="modal">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-xxl-8 col-lg-8 col-md-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Visitors Country</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-xl-12 col-lg-8">
								<div id="world-map"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-xxl-4 col-lg-4 col-md-6">
				<div class="card active_users">
					<div class="card-header">
						<h4 class="card-title">Active Users</h4>
					</div>
					<div class="card-body pt-0">
						<span id="counter"></span>
						<canvas id="activeUser"></canvas>
						<div class="list-group-flush mt-4">
							<div class="list-group-item bg-transparent d-flex justify-content-between px-0 py-1 font-weight-semi-bold border-top-0" style="border-color: rgba(255, 255, 255, 0.15)">
								<p class="mb-0">Top Active Pages</p>
								<p class="mb-0">Active Users</p>
							</div>
							<div class="list-group-item bg-transparent d-flex justify-content-between px-0 py-1" style="border-color: rgba(255, 255, 255, 0.05)">
								<p class="mb-0">/bootstrap-themes/</p>
								<p class="mb-0">3</p>
							</div>
							<div class="list-group-item bg-transparent d-flex justify-content-between px-0 py-1" style="border-color: rgba(255, 255, 255, 0.05)">
								<p class="mb-0">/tags/html5/</p>
								<p class="mb-0">3</p>
							</div>
							<div class="list-group-item bg-transparent d-xxl-flex justify-content-between px-0 py-1 d-none" style="border-color: rgba(255, 255, 255, 0.05)">
								<p class="mb-0">/</p>
								<p class="mb-0">2</p>
							</div>
							<div class="list-group-item bg-transparent d-xxl-flex justify-content-between px-0 py-1 d-none" style="border-color: rgba(255, 255, 255, 0.05)">
								<p class="mb-0">/preview/falcon/dashboard/</p>
								<p class="mb-0">2</p>
							</div>
							<div class="list-group-item bg-transparent d-flex justify-content-between px-0 py-1" style="border-color: rgba(255, 255, 255, 0.05)">
								<p class="mb-0">/100-best-themes...all-time/</p>
								<p class="mb-0">1</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-12 col-xxl-12 col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Recent Purchases</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive recentOrderTable">
							<table class="table verticle-middle table-responsive-md">
								<thead>
									<tr>
										<th scope="col">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkAll">
												<label class="custom-control-label" for="checkAll"></label>
											</div>
										</th>
										<th scope="col">Product</th>
										<th scope="col">Customer</th>
										<th scope="col">Email</th>
										<th scope="col">Status</th>
										<th scope="col">Price</th>
										<th scope="col"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox2">
												<label class="custom-control-label" for="checkbox2"></label>
											</div>
										</td>
										<td>
											Gray Dashboard Template
										</td>
										<td>example@example.com</td>
										<td>
											<img width="22" src="images/avatar/1.png" alt="">
											<span class="ml-2">John Doe</span>
										</td>
										<td>
											<i class="fa fa-circle text-success"></i>
											<span class="ml-2">Successful</span>
										</td>
										<td>$59</td>
										<td>
											<div class="dropdown custom-dropdown mb-0">
												<div data-toggle="dropdown">
													<i class="fa fa-ellipsis-v"></i>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">Accept Order</a>
													<a class="dropdown-item" href="ecom-invoice.html">Order Details</a>
													<a class="dropdown-item text-danger" href="#">Cancel
														Order</a>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox3">
												<label class="custom-control-label" for="checkbox3"></label>
											</div>
										</td>
										<td>
											MetroAdmin Pro Dashboard
										</td>
										<td>example@example.com</td>
										<td>
											<img width="22" src="images/avatar/2.png" alt="">
											<span class="ml-2">John Doe</span>
										</td>
										<td>
											<i class="fa fa-circle text-warning"></i>
											<span class="ml-2">Pending</span>
										</td>
										<td>$59</td>
										<td>
											<div class="dropdown custom-dropdown mb-0">
												<div data-toggle="dropdown">
													<i class="fa fa-ellipsis-v"></i>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">Accept Order</a>
													<a class="dropdown-item" href="ecom-invoice.html">Order Details</a>
													<a class="dropdown-item text-danger" href="#">Cancel
														Order</a>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox4">
												<label class="custom-control-label" for="checkbox4"></label>
											</div>
										</td>
										<td>
											Landing Pro Template
										</td>
										<td>example@example.com</td>
										<td>
											<img width="22" src="images/avatar/3.png" alt="">
											<span class="ml-2">John Doe</span>
										</td>
										<td>
											<i class="fa fa-circle text-success"></i>
											<span class="ml-2">Successful</span>
										</td>
										<td>$59</td>
										<td>
											<div class="dropdown custom-dropdown mb-0">
												<div data-toggle="dropdown">
													<i class="fa fa-ellipsis-v"></i>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">Accept Order</a>
													<a class="dropdown-item" href="ecom-invoice.html">Order Details</a>
													<a class="dropdown-item text-danger" href="#">Cancel
														Order</a>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox5">
												<label class="custom-control-label" for="checkbox5"></label>
											</div>
										</td>
										<td>
											Neuon Dashboard HTML
										</td>
										<td>example@example.com</td>
										<td>
											<img width="22" src="images/avatar/4.png" alt="">
											<span class="ml-2">John Doe</span>
										</td>
										<td>
											<i class="fa fa-circle text-warning"></i>
											<span class="ml-2">Pending</span>
										</td>
										<td>$59</td>
										<td>
											<div class="dropdown custom-dropdown mb-0">
												<div data-toggle="dropdown">
													<i class="fa fa-ellipsis-v"></i>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">Accept Order</a>
													<a class="dropdown-item" href="ecom-invoice.html">Order Details</a>
													<a class="dropdown-item text-danger" href="#">Cancel
														Order</a>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkbox6">
												<label class="custom-control-label" for="checkbox6"></label>
											</div>
										</td>
										<td>
											Gray Dashboard Template
										</td>
										<td>example@example.com</td>
										<td>
											<img width="22" src="images/avatar/5.png" alt="">
											<span class="ml-2">John Doe</span>
										</td>
										<td>
											<i class="fa fa-circle text-danger"></i>
											<span class="ml-2">Canceled</span>
										</td>
										<td>$59</td>
										<td>
											<div class="dropdown custom-dropdown mb-0">
												<div data-toggle="dropdown">
													<i class="fa fa-ellipsis-v"></i>
												</div>
												<div class="dropdown-menu dropdown-menu-right">
													<a class="dropdown-item" href="#">Accept Order</a>
													<a class="dropdown-item" href="ecom-invoice.html">Order Details</a>
													<a class="dropdown-item text-danger" href="#">Cancel
														Order</a>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-xxl-4 col-lg-4 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Timeline</h4>
					</div>
					<div class="card-body">
						<div class="widget-timeline">
							<ul class="timeline">
								<li>
									<div class="timeline-badge primary"></div>
									<a class="timeline-panel text-muted" href="#">
										<span>10 minutes ago</span>
										<h6 class="m-t-5">Youtube, a video-sharing website, goes live.</h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge warning">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>20 minutes ago</span>
										<h6 class="m-t-5">Mashable, a Adds website and blog, goes live.</h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge danger">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>30 minutes ago</span>
										<h6 class="m-t-5">Google acquires Youtube.</h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge success">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>15 minutes ago</span>
										<h6 class="m-t-5">StumbleUpon is acquired by eBay. </h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge warning">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>20 minutes ago</span>
										<h6 class="m-t-5">Mashable, a Adds website and blog, goes live.</h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge dark">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>20 minutes ago</span>
										<h6 class="m-t-5">Mashable, a Adds website and blog, goes live.</h6>
									</a>
								</li>

								<li>
									<div class="timeline-badge info">
									</div>
									<a class="timeline-panel text-muted" href="#">
										<span>30 minutes ago</span>
										<h6 class="m-t-5">Google acquires Youtube.</h6>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-xxl-4 col-lg-4 col-md-6 col-sm-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Todo</h4>
					</div>
					<div class="card-body px-0">
						<div class="todo-list">
							<div class="tdl-holder">
								<div class="tdl-content widget-todo">
									<ul id="todo_list">
										<li><label><input type="checkbox"><i></i><span>Get up</span><a href='#'
													class="ti-trash"></a></label></li>
										<li><label><input type="checkbox" checked><i></i><span>Stand up</span><a
													href='#' class="ti-trash"></a></label></li>
										<li><label><input type="checkbox"><i></i><span>Don't give up the
													fight.</span><a href='#' class="ti-trash"></a></label></li>
										<li><label><input type="checkbox" checked><i></i><span>Do something
													else</span><a href='#' class="ti-trash"></a></label></li>
										<li><label><input type="checkbox" checked><i></i><span>Stand up</span><a
													href='#' class="ti-trash"></a></label></li>
										<li><label><input type="checkbox"><i></i><span>Don't give up the
													fight.</span><a href='#' class="ti-trash"></a></label></li>
									</ul>
								</div>
								<div class="px-4">
									<input type="text" class="tdl-Add form-control" placeholder="Write Add item and hit 'Enter'...">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-xxl-4 col-lg-4 col-sm-12 col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Product Sold</h4>
						<div class="card-action">
							<div class="dropdown custom-dropdown">
								<div data-toggle="dropdown">
									<i class="ti-more-alt"></i>
								</div>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Option 1</a>
									<a class="dropdown-item" href="#">Option 2</a>
									<a class="dropdown-item" href="#">Option 3</a>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="chart py-4">
							<canvas id="sold-product"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!--**********************************
	Content body end
***********************************-->
    
	
		