<style>
table.dataTable thead th 
{
	text-transform:none;
	font-size: 14px;
}
.custom_body
{
	min-height: auto !important;
}
table.dataTable tbody td 
{
	text-align:center;
	font-size: 72px;
}
p
{
	white-space: nowrap;
    font-size: 14px;
}
.counter
{
	font-size: 23px;
}
</style>
 <link href="<?= base_url() ?>assets/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
 <link href="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">       
	   <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body custom_body">
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
					<div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12">
						<div class="row">
							<div class="col-md-3">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body custom_body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th>Total clients</th>
												</tr>
											</thead>
											<tbody>												
												<tr>
													<td><?php 
													$clientuser = $this->client_model->get_all_active_clients();
													echo count($clientuser);?>
													</td>
												</tr>												
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<div class="col-md-3">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body custom_body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th>Total Insurance Companies</th>
												</tr>
											</thead>
											<tbody>												
												<tr>
													<td>
													<?php 
													$inscnt = $this->insurance_company_model->get_all_active_insurance_companies();
													echo count($inscnt);?>
													</td>
												</tr>												
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<div class="col-md-3">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body custom_body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th>Total Services</th>
												</tr>
											</thead>
											<tbody>												
												<tr>
													<td>
													<?php 
													$srvcnt = $this->service_model->get_all_active_services();
													echo count($srvcnt);?>
													</td>
												</tr>												
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							</div>
							<div class="row">
							<div class="col-md-3">
                                <div class="card">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Total Users
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$usrcnt = $this->user_model->get_all_active_users();
													echo count($usrcnt);?></span></h4>
                                                <p class="m-0">Active</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$usrcnt = $this->user_model->get_all_inactive_users();
													echo count($usrcnt);?></span></h4>
                                                <p class="m-0">In-Active</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-8">
                                <div class="card">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Claims Status
                                    </div>
                                    <div class="row">
                                        <div class="col-2 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(1);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Draft</p>
                                            </div>
                                        </div>
                                        <div class="col-2 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(2);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">New</p>
                                            </div>
                                        </div>
										<div class="col-2 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(3);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Pending</p>
                                            </div>
                                        </div>
										<div class="col-2 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(4);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Ready to Submit</p>
                                            </div>
                                        </div>
										<div class="col-2 border-right">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(5);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Report Generated</p>
                                            </div>
                                        </div>
										<div class="col-2">
                                            <div class="pt-4 pb-4 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(6);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Submitted</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-8">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body custom_body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th>Average Claim Processing Time</th>
												</tr>
											</thead>
											<tbody>												
												<tr>
													<td>
													<div id="chart_div" style="width:100%; height: 400px;"></div>
													</td>
												</tr>												
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>	
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<!-- Required vendors -->
    <script src="<?= base_url() ?>assets/admin/vendor/global/global.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Counter Up -->
    <script src="<?= base_url() ?>assets/admin/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Demo scripts -->
    <script src="<?= base_url() ?>assets/admin/js/dashboard/dashboard-2.js"></script>

	
		<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart', 'line']});
		google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {
   
		var data = google.visualization.arrayToDataTable([ 
		['Month','Average Processing Time'],
		['Jan-2020',5],   ['Feb-2020',6],  ['Mar-2020',3],  ['Apr-2020',10],  ['May-2020',3],  ['Jun-2020',8],  ['Jul-2020',7],  ['Aug-2020',12],  ['Sep-2020',9],  ['Oct-2020',7]
        ]);
      

      var options = {
        hAxis: {
          title: 'Month',
		  format: 'date',
        },
        vAxis: {
          title: 'Average Processing Time'
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
	</script>