<style>
table.dataTable thead th 
{
	text-transform:none;
	font-size: 14px;
}
.custom_body
{
	min-height: auto !important;
}
table.dataTable tbody td 
{
	text-align:center;
	font-size: 72px;
}
p
{
	white-space: nowrap;
    font-size: 14px;
}
.counter
{
	font-size: 23px;
}
</style>
 <link href="<?= base_url() ?>assets/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
 <link href="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">       
	   <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body custom_body">
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
					<div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12">
						<div class="row"style="margin-left: 10px;margin-bottom: -21px;">
							<h5>Overview</h5>
						</div><hr>
						<div class="row" style="margin-right: -10px;margin-left: -10px;">
							
							<div class="col-md-3">
                                <div class="card1">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Total clients
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clientuser = $this->client_model->get_all_active_clients();
													echo count($clientuser);?></span></h4>
                                                <p class="m-0">Active</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clientuser = $this->client_model->get_all_inactive_clients();
													echo count($clientuser);?></span></h4>
                                                <p class="m-0">In-Active</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
							<div class="col-md-3">
                                <div class="card1">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Total Insurance Companies
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$inscnt = $this->insurance_company_model->get_all_active_insurance_companies();
													echo count($inscnt);?></span></h4>
                                                <p class="m-0">Active</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$inscnt = $this->insurance_company_model->get_all_inactive_insurance_companies();
													echo count($inscnt);?></span></h4>
                                                <p class="m-0">In-Active</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>							
							<!--div class="col-md-3">
                                <div class="card1">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Total Services
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$srvcnt = $this->service_model->get_all_active_services();
													echo count($srvcnt);?></span></h4>
                                                <p class="m-0">Active</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$srvcnt = $this->service_model->get_all_inactive_services();
													echo count($srvcnt);?></span></h4>
                                                <p class="m-0">In-Active</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div-->							
							<div class="col-md-3">
                                <div class="card1">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Total Users
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$usrcnt = $this->user_model->get_all_active_users();
													echo count($usrcnt);?></span></h4>
                                                <p class="m-0">Active</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$usrcnt = $this->user_model->get_all_inactive_users();
													echo count($usrcnt);?></span></h4>
                                                <p class="m-0">In-Active</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-3">
                                <div class="card1" style="width:100% !important;">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Commission($s)
                                    </div>
                                    <div class="row">
                                        <div class="col-6 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php
													echo "1540";?></span></h4>
                                                <p class="m-0">Today's</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													echo "33540";?></span></h4>
                                                <p class="m-0">Total Monthly</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col-md-6" style="margin-top: -32px;">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Average Claim Processing Time</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<div id="average_claim" style="width:100%; height: 400px;"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<div class="col-md-6" style="margin-top: -32px;">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Outstanding Claims by client(Current Date)</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<div id="client_claims" style="width:100%; height: 400px;"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col-md-6" style="margin-top: -32px;">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Outstanding Claims by Insurance Company(Current Date)</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<div id="insurance_claims" style="width:100%; height: 400px;"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<div class="col-md-6" style="margin-top: -32px;">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Claim Amount Processing by Insurance Company($s)</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<div id="insurance_claims_amt" style="width:100%; height: 350px;"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
						</div>
						<div class="row" style="margin-bottom:10px;">							
							<div class="col-md-6" style="margin-top: -32px;">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Claim Amount Processing by clients($s)</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<div id="client_claims_amt" style="width:100%; height: 350px;"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>	
							<div class="col-md-6" style="margin-top: -32px;" id="client_rejection">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Rejection Rate by client</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<i class="fa fa-filter" aria-hidden="true" style="font-size: 14px;"><div id="client_control_div" style="padding-left: 2em; min-width: 250px;font-size: 14px;margin-left: 59px;margin-top: -14px;"></div>	</i>				
												<div id="client_chart_div"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col-md-6" style="margin-top: -32px;" id="insurance_rejection">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Rejection Rate by Insurance Company</th>
											</tr>
										</thead>
										<tbody>												
											<tr>
												<td>
												<i class="fa fa-filter" aria-hidden="true" style="font-size: 14px;"><div id="programmatic_control_div" style="padding-left: 2em; min-width: 250px;font-size: 14px;margin-left: 123px;margin-top: -14px;"></div>	</i>				
												<div id="programmatic_chart_div"></div>
												</td>
											</tr>												
										</tbody>
									</table>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
						</div>	
							<div class="row">
							<div class="col-md-12" style="margin-top: -32px;">
                                <div class="card2">
                                    <div class="social-graph-wrapper widget-twitter" style="background-color: #e7e3e3;color: #415a71 !important;font-size: 14px;font-weight: 700;    text-align: left;">
                                        Claims by Status(Current Date)
                                    </div>
                                    <div class="row">
                                        <div class="col-2 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(1);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Draft</p>
                                            </div>
                                        </div>
                                        <div class="col-2 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(2);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">New</p>
                                            </div>
                                        </div>
										<div class="col-2 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(3);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Pending</p>
                                            </div>
                                        </div>										
										<div class="col-2 border-right">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(5);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Report Generated</p>
                                            </div>
                                        </div>
										<div class="col-2 border-right"">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(6);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Submitted</p>
                                            </div>
                                        </div>
										<div class="col-2">
                                            <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                <h4 class="m-1"><span class="counter"><?php 
													$clmcnt = $this->claim_model->get_all_claim_status_by_id(9);
													echo count($clmcnt);?></span></h4>
                                                <p class="m-0">Approved</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<!-- Required vendors -->
    <script src="<?= base_url() ?>assets/admin/vendor/global/global.min.js"></script>
	<script src="<?= base_url() ?>assets/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Counter Up -->
    <script src="<?= base_url() ?>assets/admin/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Demo scripts -->
    <script src="<?= base_url() ?>assets/admin/js/dashboard/dashboard-2.js"></script>

	
		<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart', 'line']});
		google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {
   
		var data = google.visualization.arrayToDataTable([ 
		['Month',''],
		['Jan-2020',5],   ['Feb-2020',6],  ['Mar-2020',3],  ['Apr-2020',10],  ['May-2020',3],  ['Jun-2020',8],  ['Jul-2020',7],  ['Aug-2020',12],  ['Sep-2020',9],  ['Oct-2020',7]
        ]);
      

      var options = {
        hAxis: {
          title: 'Month',
		  format: 'date',
        },
        vAxis: {
          title: 'Average Processing Time'
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('average_claim'));

      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Status', '# of Claims'],
        ['New', 200],            
        ['Pending', 20],            
        ['Submitted', 100],
        ['Rejected', 10 ], 
        ['Approved', 50 ], 
        ['Denied', 10 ], 
        ['Payment Received', 150 ], 
        ['Payment Sent', 50 ], 
      ]);

      

      var options = {       
        hAxis: {
          title: 'Status',
		  format: 'title',          
        },
        vAxis: {
          title: '# of Claims'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('client_claims'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Number of Claims (in process)'],
        ['State Farm', 50],            
        ['Berkshire Hathaway', 63],            
        ['Liberty Mutual', 56],
        ['Allstate', 98], 
        ['Progressive', 46], 
        ['Travelers', 53],
      ]);

      

      var options = {       
        hAxis: {
          title: 'Insurance Company',
		  format: 'title',          
        },
        vAxis: {
          title: 'Number of Claims (in process)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('insurance_claims'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Claim Amount ($s)'],
        ['State Farm', 12000],            
        ['Berkshire Hathaway', 13460],            
        ['Liberty Mutual', 11876],
        ['Allstate', 23000], 
        ['Progressive', 9005], 
        ['Travelers', 11800],
      ]);

      

      var options = {       
        hAxis: {
          title: 'Insurance Company',
		  format: 'title',          
        },
        vAxis: {
          title: 'Claim Amount($s)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('insurance_claims_amt'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['clients', 'Claim Amount ($s)'],
        ['Branch Brook client', 12000],            
        ['Mason client', 13460],            
        ['Vim client', 11876],
        ['Hill client', 23000], 
        ['Batish client', 9005], 
        ['Freedom client', 11800],
      ]);

      

      var options = {       
        hAxis: {
          title: 'clients',
		  format: 'title',          
        },
        vAxis: {
          title: 'Claim Amount($s)'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('client_claims_amt'));
      chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawColColors);

function drawColColors() {
      var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Rejection Rate (%)'],
        ['State Farm', 5],            
        ['Berkshire Hathaway', 10],            
        ['Liberty Mutual', 2],
        ['Allstate', 4], 
        ['Progressive', 6], 
        ['Travelers', 4],
      ]);      

      var options = {       
        hAxis: {
          title: 'Insurance Company',
		  format: 'title',          
        },
        vAxis: {
          title: 'Rejection Rate (%)'
        }
      };
	 
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div3'));
	  chart.draw(data, options);
    }
	</script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart', 'controls']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {

        var dashboard = new google.visualization.Dashboard(
          document.getElementById('client_rejection'));

        // We omit "var" so that programmaticSlider is visible to changeRange.
        var programmaticSlider = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'client_control_div',
          'options': {
            'filterColumnLabel': 'client',
            'ui': {'labelStacking': 'vertical'}
          }
        });

        var programmaticChart  = new google.visualization.ChartWrapper({
          'chartType': 'ColumnChart',
          'containerId': 'client_chart_div',
          'options': {
            'width': 500,
            'height': 250
          }
        });
		
        var data = google.visualization.arrayToDataTable([
        ['client', 'Rejection Rate (%)'],
        ['Branch Brook client', 5],            
        ['Mason client', 10],            
        ['Vim client', 2],
        ['Hill client', 4], 
        ['Batish client', 6], 
        ['Freedom client', 4]
        ]);
		
		 var options = {       
			hAxis: {
			  title: 'clients',
			  format: 'title',          
			},
			vAxis: {
			  title: 'Rejection Rate (%)'
			}
		};
		
        dashboard.bind(programmaticSlider, programmaticChart);
        dashboard.draw(data,options);
       
      }

    </script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart', 'controls']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {

        var dashboard = new google.visualization.Dashboard(
          document.getElementById('insurance_rejection'));

        // We omit "var" so that programmaticSlider is visible to changeRange.
        var programmaticSlider = new google.visualization.ControlWrapper({
          'controlType': 'CategoryFilter',
          'containerId': 'programmatic_control_div',
          'options': {
            'filterColumnLabel': 'Insurance Company',
            'ui': {'labelStacking': 'vertical'}
          }
        });

        var programmaticChart  = new google.visualization.ChartWrapper({
          'chartType': 'ColumnChart',
          'containerId': 'programmatic_chart_div',
          'options': {
            'width': 500,
            'height': 250
          }
        });

        var data = google.visualization.arrayToDataTable([
        ['Insurance Company', 'Rejection Rate (%)'],
        ['State Farm', 5],            
        ['Berkshire Hathaway', 10],            
        ['Liberty Mutual', 2],
        ['Allstate', 4], 
        ['Progressive', 6], 
        ['Travelers', 4]
        ]);
		
		 var options = {       
			hAxis: {
			  title: 'Insurance Company',
			  format: 'title',          
			},
			vAxis: {
			  title: 'Rejection Rate (%)'
			}
		};
		
        dashboard.bind(programmaticSlider, programmaticChart);
        dashboard.draw(data,options);
       
      }

    </script>