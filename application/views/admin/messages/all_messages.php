<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
.line-clamp {
	white-space: nowrap;
        overflow: hidden;
		text-overflow: ellipsis;
		max-width:300px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Messages</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs"  id="myTab" role="tablist" style="border-bottom: 1px solid #dee2e6;">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#home8">
									<span>
										<i class="ti-home"></i>&nbsp;Contact Messages
									</span>
								</a>
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content tabcontent-border">
							<div class="tab-pane fade show active" id="home8" role="tabpanel">
								<div class="pt-4">
									<div class="card">
										<div class="card-body custom_body">
											<div class="table-responsive">
												<table id="cc" class="ui celled table" style="width:100%">
													<thead>
														<tr>
															<th>ID</th>
															<th>Name</th>
															<th>E-Mail Address</th>
															<th>Phone Number</th>
															<th>Subject</th>
															<th>Message</th>
															<th>Status</th>															
															<th>Action</th>										
															<!--th>Status</th-->
														</tr>
													</thead>
													<tbody id="filter_data">
													
												    </tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>   
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$(document).ready(function() {
	$('body').on('click', '#status', function () {
		if($(this).prop("checked") == true){
			var id = 1;
		}
		else {
			var id = 0;
		}
		var message_id = $(this).attr('message_id');
		$.ajax({
			type: "POST",
			url:"<?php echo base_url('admin/messages/update_status');?>",
			data: {id: id, message_id: message_id},
			cache: false,
			success: function(data){
				filter_data();
			}
		});
	});
});
function filter_data()
{
	$.ajax({
		url:"messages/filter_messages",
		method:"POST",
		async: true,
		dataType:"json",
		success: function(data)
		{						
			$('#filter_data').html(data.messages_list);
			tableinit();
		},
		error: function (data) {
			//var out =JSON.parse(data);
			console.log(data.responseText);
		},
		complete: function (data) {
		// Handle the complete event
			console.log(data);
		}
	})
}
function tableinit(){
	$('#cc').DataTable({
		"bRetrieve": true,
		"bProcessing": true,
		"bSort" : true,
		"bDestroy": false,
		"bPaginate": true,
		"bAutoWidth": true,
		"bFilter": true,
		"bInfo": true,
		"bJQueryUI": true,
		"ordering": true,
		columnDefs: [
			{ orderable: false, targets: -1 }
		],
		language: {
		"emptyTable": "No Appointments were found."
		}
	});
}
</script>
<script>
$('body').on('click', '.removeRecord', function() {
	 var id = $(this).attr('message_id'); 
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/messages/del');?>",
						data:{'id':id},
						success: function(data){
							filter_data();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
$('#myTab a').click(function(e) {
  e.preventDefault();
  $(this).tab('show');
});

// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
  var id = $(e.target).attr("href").substr(1);
  window.location.hash = id;
});

// on load of the page: switch to the currently selected tab
var hash = window.location.hash;
$('#myTab a[href="' + hash + '"]').tab('show');
</script>