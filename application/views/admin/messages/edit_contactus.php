<?php echo form_open(base_url('admin/contactus/edit/'.$contactus[0]['id']), 'id="quickForm" class="form-horizontal"');  ?> 		
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/contactus">Contact Info</a></li>
					<li class="breadcrumb-item active">Edit Contact Info</li>
				</ol>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
				<a type="button" href="<?php echo base_url()?>admin/contactus" class="form btn btn-outline-dark"><i class="fa fa-ban"></i>Cancel</a>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Contact Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
										<div class="alert alert-danger alert-dismissible">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<?= validation_errors();?>
											<?= isset($msg)? $msg: ''; ?>
										</div>
									<?php endif; ?>
									<?php $i=1; foreach ($contactus as $key=>$contact){ ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="name" placeholder="*Title" name="name" value="<?php echo $contact['title']?>" maxLength="30">
										</div>
									</div>
									<div class="row">	
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" id="description" rows="5" style="height:auto !important;" placeholder="Description" name="description" ><?php echo $contact['description']?></textarea>
										</div>										
									</div>
								</div>
							</div>
							<div class="row">								
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="address1" value="<?php echo $contact['address1']?>" placeholder="*Address1" title="Address1" name="address1" maxLength="30">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="address2" value="<?php echo $contact['address2']?>" placeholder="Address2" title="Address2" name="address2" maxLength="20">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="city" value="<?php echo $contact['city']?>" placeholder="*City" title="City" name="city" maxLength="15">
										</div>
										<div class="form-group col-lg-2 col-md-2">											
											<select placeholder="State" title="State" class="form-control"  name="state" id="state">
												<option value="">*Select State</option>
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$contact['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control us_zip" id="zip_code" value="<?php echo $contact['zip']?>" placeholder="*Zip Code" title="Zip Code" name="zip_code" maxLength="15">
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">								
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="phone_number" value="<?php echo $contact['phone']?>" placeholder="*Phone Number" title="Phone Number" name="phone_number">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="email_address" value="<?php echo $contact['email']?>" placeholder="E-Mail Address" title="E-Mail Address" name="email_address" maxLength="25">
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Social Media Pages</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="facebook" placeholder="Facebook page Url" name="facebook" value="<?php echo $contact['facebook'];?>" >
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="twitter" placeholder="Twitter page url" name="twitter" value="<?php echo $contact['twitter'];?>" >
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="instagram" placeholder="Instagram page url" name="instagram" value="<?php echo $contact['instagram'];?>">
										</div>
										
									</div>
								</div>
							</div>
							<div class="row">								
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-4 col-md-4">											
											<input type="text" class="form-control" id="linkedin" placeholder="Linkedin page url" name="linkedin" value="<?php echo $contact['linkedin'];?> ">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control " id="pinterest" placeholder="Pinterest page url" name="pinterest" value="<?php echo $contact['pinterest'];?>">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="youtube" placeholder="Youtube page url" name="youtube" value="<?php echo $contact['youtube'];?>">
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<?php echo form_close( ); ?>
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid email address.");
  $('#quickForm').validate({
    rules: {
      name: {
        required: true
      },
	  description: {
        required: true
      },
      address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true,
      },
	  zip_code: {
        required: true,
        minlength: 5
      }, 
	  phone_number: {
        required: true
      },
	  email_address: {
        required: true,
		email: true,
        validate_email: true
      },
	   facebook: {
        required: true
      },
      twitter: {
        required: true
      },
	   instagram: {
        required: true
      },
      linkedin: {
        required: true
      },
	   pinterest: {
        required: true
      },
      youtube: {
        required: true
      },
    },  
    messages: {
      name: {
        required: "Please enter Title"
      },
       description: {
        required: "Please enter description"
      },
       facebook: {
        required: "Please enter facebook url"
      },
       twitter: {
        required: "Please enter twitter url"
      },
	  instagram: {
        required: "Please enter instagram url"
      },
       pinterest: {
        required: "Please enter pinterest url"
      },
	  linkedin: {
        required: "Please enter linkedin url"
      },
       youtube: {
        required: "Please enter youtube url"
      },
      address1: {
        required: "Please enter Address 1"
      },
	  city: {
        required: "Please enter City"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
	  phone_number: {
        required: "Please enter phone number"
      },
       business_email: {
        required: "Please enter email address",
        email: "Please enter a valid email address"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
</script>
<script>
$("#zip_code").on('input', function() {  //this is use for every time input change.
//alert("Hi");
       var inputValue = getInputValuez(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#zip_code").val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $("#zip_code").val(inputValue); //correct value entered to your input.
       inputValue = getInputValuez();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 200000000) && (inputValue < 999999999))
     {
         $("#zip_code").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#zip_code").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValuez() {
        var inputValue = $("#zip_code").val().replace(/\D/g,'');  //remove all non numeric character
        return inputValue;
}
 $("#phone_number").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValue(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#phone_number").val('');
           return false;
       }    
       if (inputValue < 1000)
       {
           inputValue = '+1' + ' ' + '('+inputValue;
       }else if (inputValue < 1000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
       }else if (inputValue < 10000000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, length);
       }else
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, 10);
       }       
       $("#phone_number").val(inputValue); //correct value entered to your input.
       inputValue = getInputValue();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 2000000000) && (inputValue < 9999999999))
     {
         $("#phone_number").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#phone_number").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValue() {
         var inputValue = $("#phone_number").val().replace(/\D/g,'');  //remove all non numeric character
        if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
        {
            var inputValue = inputValue.substring(1, inputValue.length);
        }
        return inputValue;
}

</script>
<script>
$("#nav_contactus").addClass('active');
</script> 