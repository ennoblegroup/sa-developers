<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',16);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',16);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Features</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
				<a href="<?= base_url('admin/features/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark "><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Feature</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Features</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead> 
									<tr>
										<th>ID</th>
										<th>Title</th>
										<th class="<?php echo $user_classs;?>">Status</th>												
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_features as $key=>$srv){ 
										
									?>
									<tr>
									<!--td style="display:none"><?php echo $i; ?></td-->
										<td> <?php echo $srv['id']?> </td>										
										<td> <?php echo $srv['title']?> </td>
										<!--td> <?php echo date("F j, Y , g:i a", strtotime($srv['create_date'])); ?> </td-->										
										<td class="<?php echo $user_classs;?>"> 
											<label class="switch">
											  <input type="checkbox" <?php if($srv['status']==1){echo 'checked';}?> id="status" features_aid="<?php echo $srv['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $srv['id']; ?>">
											<?php if($srv['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>

											<?php }?>
											</span>
										</td>
										<td> <a href="<?= base_url('admin/features/edit/'.$srv['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark" <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></button> </a>
										<a href="<?= base_url('admin/features/view_feature/'.$srv['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark"  ><i class="fa fa-eye"></i></button> </a>
										<a href="#" class="removeRecord" features_id = "<?php echo $srv['id'];?>"><span class="btn btn-sm btn-outline-dark"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></span></a> </td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var features_id = $(this).attr('features_aid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/features/update_status');?>",
			  data: {id: id, features_id: features_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+features_id).load(" #status_roww"+features_id);
			  }
			});
		});
	});
</script>
<script type="text/javascript">
	$(".removeRecord").click(function(){
        var id = $(this).attr('features_id');    	
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/features/del');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: 'cancel',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
    });
</script>