<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/features">Features</a></li>
                        <li class="breadcrumb-item active">View Feature</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
                <?php $i=1; foreach ($feature_view as $key=>$add){ ?>
					<div class="card-header">
						<h4 class="card-title"><?php echo $add['title']?></h4>
					</div>
					<div class="card-body">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <img style="max-width: 100%;" src="<?php echo base_url();?>images/features/<?php echo $add['image']; ?>">
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-12">
                                <?php echo $add['description']?>
                            </div>
                        </div>
					</div>
                <?php $i++; }?>
				</div>
			</div>
		</div>
	</div>
</div>