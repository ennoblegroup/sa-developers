<?php echo form_open_multipart(base_url('admin/forms/add'), 'id="addForm" class="form-valid"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/forms'); ?>">Form Templates</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Add Template</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/forms" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Add Form</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">
								<div class="col-md-12">
									<div class="row">																
										<!--div class="form-group col-lg-3 col-md-3">
											<label class="col-form-label">Source</label>
											<input type="text" class="form-control" name="file_source">
										</div-->
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="file_name" placeholder="Template Name" name="file_name">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<textarea class="form-control" rows="1" placeholder="Description" name="file_description"></textarea>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="input-group mb-3">
												<div class="custom-file">
													<input type="file" name="image" placeholder="" accept="application/pdf" class="custom-file-input">
													<label class="custom-file-label">Choose file</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<!--div class="col-md-12" style="text-align:right">
									<input type="submit" name="submit" value="Submit" class="btn btn-rounded btn-success">
									<a href="<?= base_url('admin/forms'); ?>"><button type="button"   class="btn btn-rounded btn-outline-dark">Cancel</button></a>
								</div-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close( ); ?>