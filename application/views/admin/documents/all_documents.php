<style>
[clear]:before,[clear]:after{
	content:"";
	content:"";
	clear:both;
	display:block;
}
.fol{
	font-size:48px !important;
}
.fil {
	font-size:40px;
}
.show-up b .fa-folder{
	disply:none;
}
.show-up b .fa-folder:before{
	disply:none;
}
.theme-structure.big-file-manager {
    width: 100%;
    height: 90vh;
    background: #f7f7f7;
	border-bottom: 1px solid #2196F3;
	overflow:auto;
	padding: 10px 10px;
}
.big-file-manager.theme-structure ul {
	padding: 0;
	margin: 0;
	list-style: none;
	user-select:none;
}
.big-file-manager.theme-structure ul li {
    padding: 0;
}
.big-file-manager.theme-structure ul > li {
    cursor: pointer;
	vertical-align: top;
}
.big-file-manager.theme-structure ul > li > b{
	display: grid;
	user-select:none;
    text-align: center;
    transform: scale(1);
    transition: transform 0.1s linear;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 12px;
    padding: 10px 4px;
}
.big-file-manager.theme-structure ul > li.show-up > b {
    font-size: 14px;
    width: 100% !important;
    background: transparent;
    height: 20px;
    padding: 0;
}
.big-file-manager.theme-structure ul > li.show-up > b {
    margin-bottom: 10px;
}

.big-file-manager.theme-structure ul > li:not(.show-up) > b:active {
    transform: scale(0.95);
}
.big-file-manager.theme-structure ul > li > b svg {
    display: block;
    margin: 0 auto;
    font-size: 25px;
    margin-bottom: 5px;
}
.big-file-manager.theme-structure ul li[data-file-icon="folder"] > b > svg {
    color: #F7D774;
}
.big-file-manager .cm-folder-back {
    display: none;
}
.big-file-manager.theme-structure ul > li.show-up > b > .cm-folder-back {
    display: block;
}
.big-file-manager.theme-structure ul > li.hide-up > b > .cm-folder-back {
    display: none;
}
.hide-up{
	display: none;
}
.big-file-manager.theme-structure ul ul {
    display: none;
}
.big-file-manager.theme-structure ul > li .no-item-inside-folder {
    padding: 10px;
    opacity: 0.6;
    cursor: default;
    user-select: none;
}
.big-file-manager.theme-structure ul > li.data-moving:not(.show-up) > b {
    opacity: 0.6;
}
.big-file-manager.theme-structure ul > li > b:hover {
    background: #343a4026;
}
.big-file-manager.theme-structure ul > li.context-visible > b,
.big-file-manager.theme-structure ul > li.select > b{
    background: #03a9f42e;
    outline: 1px solid #03A9F4;
}
.big-file-manager.theme-structure ul > li a {
    border: 1px solid transparent;
	white-space: nowrap;
    width: auto;
    overflow: hidden;
    text-overflow: ellipsis;
}
.big-file-manager.theme-structure ul > li span.name:hover {
       
}
.big-file-manager.theme-structure ul > li.renaming span.name {
    border: 1px solid #929292;
	display: block;
	outline: 0;
	cursor: text;
}
/*-----------UnderStad-------------------|START--------*/
.big-file-manager.theme-structure ul li.show-up.select > b {
    background: transparent;
    outline: none;
}
.big-file-manager.theme-structure ul > li.show-up > b .cm-folder-back {
    float: left;
}
.big-file-manager.theme-structure ul > li.show-up > b {
    background: transparent;
}
.big-file-manager.theme-structure ul > li.show-up > b .cm-folder-back + svg {
    display: none !important;
}
.big-file-manager.theme-structure ul > li.show-up > b .cm-folder-back svg {
    font-size: 16px;
}
.big-file-manager.theme-structure ul li.file-sub-active.hide-up > b {
    display: none;
}
.big-file-manager.theme-structure ul > li.show-up > b .cm-folder-back {
    padding: 0 10px;
    background: #00000012;
}
.big-file-manager.theme-structure ul > li > b i.cm-folder-back svg {
    position: relative;
    top: 3px;
}
.big-file-manager.theme-structure ul li:not(.show-up):not(.file-sub-active) {
    display: inline-block;
	width: 7.9%;
	margin-bottom: 5px;
    margin-left: 0.125%;
    margin-right: 0.125%;
    border: 1px solid #c8c5c5;
}
/*-----------UnderStad------------------|END---------*/
/*-------------Folder Context Menu---------|START---*/
.append-option-box {
    position: fixed;
    background: #fff;
    border: 1px solid #b1b1b1;
    box-shadow: 0 1px 1px #0000003d;
    padding: 5px 0;
    width: 200px;
    top: 45px;
    z-index: 999;
    left: 57px;
}
.append-option-box>div>div {
    padding: 2px 15px;
	cursor:pointer;
}
.append-option-box>div>div:hover {
    background: #2196F3;
    color: #fff;
}
.renaming-box {
    position: absolute;
    background: white;
    padding: 10px;
    box-shadow: 0 1px 1px #0000002e;
    margin-left: 15px;
    margin-top: -10px;
    outline: 0;
    border: 1px solid #c2c2c2;
}
.renaming-box input {
    background: #fff;
    padding: 2px;
    font-size: 15px;
    color: #000;
    border: 1px solid #9E9E9E;
    display: inline-block;
    border-right: 0;
    line-height: 23px;
    outline: 0;
}
.renaming-box button {
    cursor: pointer;
    color: #2196F3;
    background: #e9e9e9;
    font-size: 13px;
    line-height: 25px;
    display: inline-block;
    border: 1px solid #9E9E9E;
    border-left: 0;
    position: relative;
    top: -1px;
    outline: 0;
}
/*-------------Folder Context Menu---------|END---*/
/*-------Context Menu Style-----------------|START-----*/
.append-option-box>div>div {
    position: relative;
}
.append-option-box>div>div .main-sub-menu {
    position: absolute;
    left: 100%;
    top: 0;
    background: #fff;
    border: 1px solid #b1b1b1;
    box-shadow: 0 1px 1px #0000003d;
    width: 150px;
    display: none;
    color: #000;
}
.append-option-box>div>div:hover .main-sub-menu {
    display: block;
}
.append-option-box>div>div:hover .main-sub-menu>div {
    padding: 2px 10px;
}
.append-option-box>div>div:hover .main-sub-menu>div:hover {
    background: #2196F3;
    color: #fff;
}
.append-option-box>div>div>svg {
    position: absolute;
    right: 8px;
    font-size: 10px;
    top: 5px;
    opacity: 0.5;
}
.theme-structure.big-file-manager.medium li svg {
    font-size: 35px;
}
.theme-structure.big-file-manager.largesvg {
    font-size: 45px;
}
.theme-structure.big-file-manager.large li svg {
    font-size: 45px;
}
.big-file-manager.theme-structure.large ul > li > b {
    width: 84px;
}
.big-file-manager.theme-structure.medium ul > li > b {
    width: 74px;
}

/*-------Context Menu Style-----------------|END-----*/

/*-------Search----------------|Start-----*/
.cm-address-bar-search > div {
    float: left;
    padding: 10px;
}
.cm-address-bar-search > div.address-search {
    width: 70%;
}
.cm-address-bar-search > div.search-file-and-folder {
    width: 30%;
}
.cm-address-bar-search > div input {
    width: 100%;
    padding: 5px;
    padding-right: 35px;
    background: #fff;
	outline: 0;
	height: 30px;
	border: 1px solid #dedede;
}
.cm-address-bar-search div.pos {
	position: relative;
	overflow: hidden;
}
.cm-address-bar-search div.pos .cm-button {
    position: absolute;
    top: 1px;
    right: 1px;
    padding: 6px 11px;
    background: #ffffff;
    cursor: pointer;
    color: #00000082;
    height: 28px;
    display: flex;
    align-items: center;
    z-index: 9;
}
.address-short-btn {
    position: absolute;
    height: 28px;
    overflow: hidden;
    top: 1px;
    background: #fff;
    left: 1px;
    /*padding: 0 6px;*/
    white-space: nowrap;
    user-select: none;
	/*width: calc(100% - 100px);*/
}

.address-short-btn > div {
    float: left;
    padding: 7px 4px;
    height: 28px;
    cursor: pointer;
}

.address-short-btn > div svg {
    margin-left: 9px;
}

.address-short-btn > div:last-child svg {
    display: none;
}
.address-short-btn > div svg {
    color: #00000045;
}
.address-short-btn > div svg {
    color: #00000045;
}
.address-short-btn > div:hover {
    background: #d9d9d9;
}
/*-------Search----------------|END-----*/

@media(max-width:767px){
.append-option-box {
    position: fixed;
    top: auto !important;
    bottom: 0 !important;
    width: 100%;
    left: 0 !important;
}
.append-option-box > div {
    float: left !important;
}
.append-option-box > .inner-contenxt-box {
    overflow-x: auto;
    overflow-y: hidden;
    display: flex;
    flex-flow: nowrap;
}

.append-option-box>div>div {
    float: left;
	white-space: nowrap;
}
}
.dropzone .dz-preview .dz-progress {
    display:none;
}
@media (min-width: 576px){
.modal-dialog {
    max-width: 680px;
    margin: 1.75rem auto;
}
}
.fol {
    font-size: 48px !important;
}
.fa {
    font-weight: 900;
}
.rr {
    position: relative;
    font-size: 16px;
    overflow: hidden;
    text-align: left;
	padding:5px;
}
.rr:after {
    position: absolute;
    top: 52%;
    overflow: hidden;
    width: 100%;
    height: 1px;
    content: '\a0';
    background-color: #8d8888;
}
.rr:before {
    margin-left: -50%;
    text-align: right;
}
</style>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Documents</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload Documents</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="theme-structure big-file-manager">
							<?php if(count($documents)>0){?>
							<?php $i=1; foreach($documents as $row){ ?>
							<h6 class="rr"><?= $row['document_type']; ?></h6>
							<ul>
								<?php $j=1; foreach($row['docs'] as $row): ?>
								<li data-file-id="sdfsdfsdfsdf45456sd" data-file-icon="<?php echo pathinfo($row['file'], PATHINFO_EXTENSION)?>">
									<b><a href="<?= base_url() ?>uploads/project_documents/<?= $row['file']; ?>" target="_blank"><?= $row['file_name']; ?></a></b>
								</li>
								<?php $j++; endforeach; ?>
							</ul>
							<?php $i++; } } else {?>
							<center><p>No Documents Found</p></center>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Upload Documents</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-3 col-md-3"></div>
								<div class="form-group col-lg-6 col-md-6">
									<select placeholder="Document Type" class="form-control selectpicker" data-live-search="true"  name="document_type_id" id="document_type_id" title="Document Type">
										<?php foreach($all_document_types as $document_type){ ?>
											<option value="<?php echo $document_type['id']?>"><?php echo $document_type['document_type_name']?></option>
										<?php }?>	
									</select>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="doc dropzone" ></div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="button"  id="doc_submit" class="btn btn-outline-dark">Submit</button>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;

var myDropzone_document = new Dropzone('.doc', {
  url: "documents/dragDropUpload", 
  addRemoveLinks: true,                       
  autoProcessQueue: false,
});

$('body').on('click', '#doc_submit', function() {
	var project_id= $('#project_id').val();
	var document_type_id= $('#document_type_id').val();
	myDropzone_document.on("sending", function(file, xhr, data) {
		
		data.append("document_type_id", document_type_id);
	});
	myDropzone_document.processQueue();
	
	location.reload();
});
function filesAndFolderIcons(newData) {
    function letGoSmallA(a) {
        var getType = $(a).attr('data-file-icon');
        if (getType == "folder") {
            $(a).children('b').prepend('<i class="fol fas fa-folder"></i>');
        } else if (getType == "html" || getType == "css" || getType == "js" || getType == "docx" || getType == "php" || getType == "sql") {
            $(a).children('b').prepend('<i class="fil fas fa-file"></i>');
        } else if (getType == "layout") {
            $(a).children('b').prepend('<i class="fil fas fa-th-large"></i>');
        } else if (getType == "pdf") {
            $(a).children('b').prepend('<i class="fil fas fa-file-pdf-o"></i>');
        }else if (getType == "pptx") {
            $(a).children('b').prepend('<i class="fil fas fa-file-powerpoint-o"></i>');
        }else if (getType == "image") {
            $(a).children('b').prepend('<i class="fil far fa-images"></i>');
        } else if (getType == "video") {
            $(a).children('b').prepend('<i class="fil fas fa-video"></i>');
        }else{
			 $(a).children('b').prepend('<i class="fil fas fa-file"></i>');
		}
    }
    if (newData == "newData") {
        $('[data-new="new"][data-file-icon]').each(function() {
            letGoSmallA(this);
        });
    } else {
        $('[data-file-icon]').each(function() {
            letGoSmallA(this);
        });
    }
}
filesAndFolderIcons();
</script>
