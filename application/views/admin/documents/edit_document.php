<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.css" rel="stylesheet">
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Map Templates</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/forms/add_insurance_form'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Map Template</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<form id="contact-form" method="post" action="<?php echo base_url();?>login/update_clients/<?php echo $client[0]['client_id'] ?>" enctype="multipart/form-data">
	<?php $i=1; foreach ($client as $key=>$phar){ ?>
    <section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<ol class="breadcrumb">
						
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>login/view_clients">clients</a></li>
						<li class="breadcrumb-item active">Edit client</li>
							
					</ol>
				</div>
				<div class="col-sm-6">
					<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
						<div class="mb-none float-sm-right">
						   <button type="submit" class="form btn btn-green"><!--i class="fa fa-floppy-o"></i-->Update</button>
						   <button type="button" onclick="javascript:goBack()" class="form btn btn-grey"><!--i class="fa fa-ban"></i-->Cancel</button>
						  
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="container-fluid">
			<div class="row">
				<!-- Horizontal Form -->
				<div class="col-md-12">
					<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
					<!-- /.card-header -->
					<!-- form start -->
						<div class="row">
							
							
							<div class="col-md-8">
								<div class="row form-group">
									<div class="col-md-2">
										<label>client name</label>
									</div>
									<div class="col-md-9">
										<input type="text" class="form-control" name="client_name" value="<?php echo $phar['client_name']?>" placeholder="client name" id="clientname">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-2" bis_skin_checked="1">
										<label>Youtube Channel Id</label>
									</div>
									<div class="col-md-9" bis_skin_checked="1">
										<input type="text"  class="form-control" id="youtube_api" value="<?php echo $phar['youtube_api_key']?>" name="youtube_api" placeholder="Youtube Channel Id" autocomplete="OFF">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-2" bis_skin_checked="1">
										<label>Place Id</label>
									</div>
									<div class="col-md-9" bis_skin_checked="1">
										<input type="text"  class="form-control" id="place_id" value="<?php echo $phar['place_id']?>" name="place_id" placeholder="Place Id" autocomplete="OFF">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row form-group">
									<div class="col-md-2 bis_skin_checked="1">
										<label>Image</label>
									</div>
									<div class="col-md-9" bis_skin_checked="1">
										<div class="btn btn-grey btn-file">
											<i class="fa fa-paperclip"></i> Upload Icon
											<input type="file" name="image" accept="image/gif, image/jpeg, image/png" onchange="readURL(this);">
										</div>
										<img id="blah" style="width:100px;height:100px;border:1px dashed grey;"  src="<?php echo base_url();?>images/admin/client/<?php echo $phar['file']; ?> " alt="">
										
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
					</div>
				</div>
				<!-- /.card -->
			</div>	
		</div>	
	</section>	
	<?php } ?>
	</form>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body custom_body">
						<?php if($this->session->flashdata('success')): ?>							 
						<div class="alert alert-success alert-dismissible" style="background-color:green;color:white;font-size:20px;padding: 2px 1.25rem;width: 40%;"role="alert">	
							<button type="button" style="background-color:green;color:white;padding: 5px 17px;" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo $this->session->flashdata('success'); ?>							  
						</div> 							
						<?php endif; ?>							
						<?php if($this->session->flashdata('failure')): ?>
						<div class="alert alert-error alert-dismissible" style="background-color:#950311;color:white;font-size:20px;padding: 2px 1.25rem;"role="alert">
							<button type="button" style="background-color:#950311;color:white;padding: 5px 17px;width: 40%;" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<?php echo $this->session->flashdata('failure'); ?>
						</div>
						<?php endif; ?>
						<table id="example1" class="table table-bordered table-striped dataTable">
							<thead>
								<tr>
									<th>User Id</th>
									<th>Role</th>
									<th>Username</th>
									<th>Name</th>
									<th>E-Mail Address</th>
									
									
								</tr>
							</thead>
							<tbody>
								<?php $i=1; foreach ($admins as $key=>$use){ 
									if($use['role_id']!='0' || !empty($use['role_id'])){
										$role = $use['role_id'];
										$rolename = $this->db->query("select name from role where role_id=$role")->fetch_assoc()['name'];
										//$rolename = $this->db->get_where('role',array('role_id'=>$use['role_id']))->row()->name;
									}else{
										$rolename = '---';
									}
								?>
								<tr>
									<td> <?php echo $use['registration_id']?> </td>
									<td> <?php echo $rolename; ?> </td>
									<td> <?php echo $use['username']?> </td>
									<td> <?php echo $use['first_name']?> <?php echo $use['last_name']?> </td>
									<td> <?php echo $use['email']?> </td>
                                   
									
									
								</tr>
								<?php $i++; }?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
    </section>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.es.min.js"></script>
<script >
	$(function () {
		$('#datetimepicker1').datepicker({
			format: "mm/dd/yyyy",
			language: "en",
			autoclose: true,
			todayHighlight: true
		});
	
	});
</script>
<script> 
$(document).ready(function() {
 
  $('#contact-form').submit(function(e) {
    var clientname = $('#clientname').val();
    
	
	

	
 
    $(".error").remove();
 
    if (clientname.length < 1) {
      $('#clientname').after('<span class="error" style="color:red">Please enter client Name</span>');
       e.preventDefault();
    }
  });
 
});
</script>


<script>
$("#nav_clients").addClass('active');
</script> 