	<?php echo form_open(base_url('admin/testimonials/edit/'.$testimonials[0]['id']),  'id="quickForm" enctype="multipart/form-data" class="form-horizontal"');  ?> 
	<div class="content-body">
	<div class="container-fluid">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/testimonials">Testimonials</a></li>
					<li class="breadcrumb-item active">Edit Testimonials</li>
				</ol>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
				<a type="button" href="<?php echo base_url()?>admin/testimonials" class="form btn btn-outline-dark"><i class="fa fa-ban"></i>Cancel</a>
			</div>
		</div>
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Testimonials Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; 
									$i=1; foreach ($testimonials as $key=>$srv){ ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="name" placeholder="testimonials Name" value="<?php echo $srv['name']?>" name="name" maxLength="25">
										</div>
									</div>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="designation" placeholder="Designation" value="<?php echo $srv['designation']?>" name="designation" maxLength="25">
										</div>
									</div>
									<div class="row">	
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" id="description" rows="5" style="height:auto !important;" placeholder="testimonials Description" name="description" ><?php echo $srv['description']?></textarea>
										</div>										
									</div>
									<div class="row">
									<!--div class="input-group col-lg-6 col-md-6">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input " id="profile" name="profile" accept="image/x-png,image/gif,image/jpeg">
                                                <label class="custom-file-label">Choose Profile</label>
                                            </div>
                                            
                                     </div-->
									 	<div class="input-group col-lg-6 col-md-6">
									  <div class="custom-file">
                                                <input type="file" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg"  >
                                                <label class="custom-file-label"><?php echo $srv['image']; ?></label>
                                            </div>
                                           
                                             
											
                                         <input type="hidden" value="<?php echo $srv['image']; ?>" name="image_name"> 
    
                                     </div>
									  </div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
	<?php echo form_close( ); ?>
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">  
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      name: {
        required: true,
        minlength: 3
      },
	  designation: {
        required: true,
        minlength: 3
      },
      description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
      name: {
        required: "Please enter testimonials Name",
        minlength: "Testimonials Name must be at least 3 characters long"
      },
	  designation: {
        required: "Please enter  Designation",
        minlength: "Testimonials Designation must be at least 3 characters long"
      },
      description: {
        required: "Please enter testimonials Description",
        minlength: "Testimonials Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#name', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
});
</script>
<script>
$("#nav_testimonials").addClass('active');
</script> 