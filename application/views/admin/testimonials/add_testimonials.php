<style>
.star-rating .fa{
	font-size:24px;
}
#hidden_div .error{
	text-align:center !important;
	display: block !impotant;
}
</style>
<?php echo form_open(base_url('admin/testimonials/add'), 'id="contact-form"  enctype="multipart/form-data" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/testimonials">Testimonials</a></li>
							<li class="breadcrumb-item active">Add Testimonial</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="testimonial btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/testimonials" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Testimonials Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; ?>
									<div class="row">																
										<div class="err form-group col-lg-4 col-md-4">
											<select name="project"  title="Project" data-live-search="true" id="project" class="selectpicker form-control" >
												<?php $i=1; foreach($all_projects as $row): ?>
												<option value="<?php echo $row['id'];?>"  ><?= $row['project_name']; ?></option>
												<?php $i++; endforeach; ?>
											</select>
										</div>
										<div class="err col-md-4 col-sm-4">
											<div class="form-group">
											   <input name="last_name" id="last_name" type="text"  class="form-control" placeholder="Last Name">
											   <span class="spin"></span>
											</div>
										 </div>
										 <div class="err col-md-4 col-sm-4">
											<div class="form-group">
											   <input name="first_name" id="first_name" type="text"   class="form-control" placeholder="First Name">
											   <span class="spin"></span>
											</div>
										 </div>
										 <div class="err col-md-4 col-sm-4">
											<div class="form-group">
											   <input name="phone" id="phone" type="text"  class="us_phone form-control"  placeholder="Phone Number">
											   <span class="spin"></span>
											</div>
										 </div>
										 <div class="err col-md-4 col-sm-4">
											<div class="form-group">
											   <input name="email" id="email" type="text"  class="form-control"  placeholder="E-Mail Address">
											   <span class="spin"></span>
											</div>
										 </div>
										 <div class="err col-md-4 col-sm-4">
											<div class="form-group">
												<input name="subject_type" id="subject_type" type="text"  class="form-control" value="Review" readonly>
											</div>
										 </div>
										 <div class="err col-md-12 col-sm-12">
											<div  class="err" id="hidden_div" style="margin: 6px;">
											   <div class="form-group star-rating" style="text-align:center;">
												  <span class="fa fa-star-o" data-rating="1"></span>
												  <span class="fa fa-star-o" data-rating="2"></span>
												  <span class="fa fa-star-o" data-rating="3"></span>
												  <span class="fa fa-star-o" data-rating="4"></span>
												  <span class="fa fa-star-o" data-rating="5"></span>
												  <input type="hidden" name="rating" id="rating" class="rating-value" >
											   </div>
											</div>
										 </div> 
										 <div class="err col-md-12 col-sm-12">
											<div class="form-group">
											   <input name="subject" id="subject" type="text"  class="form-control"  placeholder="Subject">
											   <span class="spin"></span>
											</div>
										 </div>
										 <div class="err col-md-12">
											<div class="form-group">
											   <textarea name="message" id="message" rows="4"  class="form-control "  placeholder="Message"></textarea>
											   <span class="spin"></span>
											</div>
										 </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
   function showDiv(elem){
     if(elem.value == 'Review')
     document.getElementById('hidden_div').style.display = "block";
     else if(elem.value !== 'Review')
     document.getElementById('hidden_div').style.display="none";
   }
</script>
<script>
   var $star_rating = $('.star-rating .fa');
   
   var SetRatingStar = function() {
     return $star_rating.each(function() {
   	if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
   	  return $(this).removeClass('fa-star-o').addClass('fa-star');
   	} else {
   	  return $(this).removeClass('fa-star').addClass('fa-star-o');
   	}
     });
   };
   
   $star_rating.on('click', function() {
     $star_rating.siblings('input.rating-value').val($(this).data('rating'));
     return SetRatingStar();
   });
   
   SetRatingStar();
   $(document).ready(function() {
   
   });
</script>
<script type="text/javascript">
   $(".us_zip").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValuezip(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $(this).val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $(this).val(inputValue); //correct value entered to your input.
       inputValue = getInputValuezip();//get value again, becuase it changed, this one using for changing color of input border
 });

function getInputValuezip() {
	var inputValue = $(this).val().replace(/\D/g,'');  //remove all non numeric character
	return inputValue;
}
$(document).ready(function () {
   var form = $("#contact-form");
   form.validate({
	 ignore: [],
	 rules: {
	   last_name: {
		 required: true,
		 minlength: 3
	   },
	 
	   first_name: {
		 required: true,
		 minlength: 3
	   },
	   phone: {
		 required: true,
		 minlength: 17,
		 maxlength: 17
	   },
	  subject: {
		 required: true
	   },
	   project: {
		 required: true
	   },
	  rating: {
		 required: true
	   },
	   email: {
		 required: true,
		 email: true
	   },
	  message: {
		 required: true,
		 minlength: 3
	   },
	 },
	 messages: {
	  last_name: {
		 required: "Please enter  Last Name",
		 minlength: "Last Name must be at least 3 characters long"
	   },
	   first_name: {
		 required: "Please enter  First Name",
		 minlength: "First Name must be at least 3 characters long"
	   },
	  subject: {
		 required: "Please enter  Subject"
	   },
	   project: {
		 required: "Please select  Project"
	   },
	  rating: {
		 required: "Please select  Rating"
	   },
	   phone: {
		 required: "Please enter  Phone Number",
		 minlength: "Enter a valid Phone Number",
		 maxlength: "Enter a valid Phone Number"
	   },
	   email: {
		 required: "Please enter  E-Mail Address",
		 email: "Enter a valid E-Mail Address"
	   },
	  message: {
		 required: "Please enter Message",
		 minlength: "Message must be at least 3 characters long"
	   }
	 },
	 errorElement: 'span',
	 errorPlacement: function (error, element) {
	   error.addClass('invalid-feedback');
	   element.closest('.err').append(error);
	 },
	 highlight: function (element, errorClass, validClass) {
	   $(element).addClass('is-invalid');
	 },
	 unhighlight: function (element, errorClass, validClass) {
	   $(element).removeClass('is-invalid');
	 }
   });	
 });	
 </script>
<script>
$("#nav_testimonials").addClass('active');
</script> 