 <!-- Datatable style -->
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">  

 <section class="content">
   <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Table With Full Features</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped ">
        <thead>
        <tr>
          <th>Website Name</th>
          <th>Test Link</th>
          <th>Live Link</th>
          <th style="width: 150px;" class="text-right">Option</th>
        </tr>
        </thead>
        <tbody>
          <?php foreach($all_websites as $row): ?>
          <tr>
            <td><?= $row['websitename']; ?></td>
            <td><?= $row['test_link']; ?></td>
            <td><?= $row['live_link']; ?></td>
			<td class="text-right"><a href="<?= base_url('admin/websites/edit/'.$row['id']); ?>" class="btn btn-outline-dark btn-flat"><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="<?= base_url('admin/websites/del/'.$row['id']); ?>" class="btn btn-outline-dark btn-flat"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="<?= base_url('admin/websites/website_conversation'); ?>" class="btn btn-success btn-flat"><i class="fa fa-comments" aria-hidden="true"></i></a></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>  

<!-- DataTables -->
<script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script> 
<script>
$("#view_websites").addClass('active');
</script>        
