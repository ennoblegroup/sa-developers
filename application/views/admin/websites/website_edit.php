<style>
.doc_remove, .doc_close {
    position: absolute;
    right: 5%;
    top: 25%;
	color:red;
}

</style>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Website</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body my-form-body">
          <?php if(isset($msg) || validation_errors() !== ''): ?>
              <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                  <?= validation_errors();?>
                  <?= isset($msg)? $msg: ''; ?>
              </div>
            <?php endif; ?>
           
            <?php echo form_open_multipart(base_url('admin/websites/edit/'.$website[0]['id']), 'class="form-horizontal"' )?> 
              <div class="form-group">
                <label for="websitename" class="col-sm-2 control-label">Website Name</label>
				<div class="col-sm-2">
                  <select type="text" name="websitetype" class="form-control" id="websitetype" >
					<option value="1" <?= ($website[0]['websitetype'] == 1)?'selected': '' ?>>General</option>
					<option value="2" <?= ($website[0]['websitetype'] == 2)?'selected': '' ?>>clients</option>
					<option value="3" <?= ($website[0]['websitetype'] == 3)?'selected': '' ?>>others</option>
				  </select>				  
                </div>
                <div class="col-sm-7">
                  <input type="text" name="websitename" value="<?= $website[0]['websitename']; ?>" class="form-control" id="websitename" placeholder="Website Name">
                </div>
              </div>
			  <div class="form-group">
                <label for="address1" class="col-sm-2 control-label">Address</label>

                <div class="col-sm-9">
                  <input type="text" name="address1" value="<?= $website[0]['address1']; ?>" class="form-control" id="address1" placeholder="Address 1">
                </div>
              </div>
			  <div class="form-group">
                <label for="address2" class="col-sm-2 control-label"></label>

                <div class="col-sm-9">
                  <input type="text" name="address2" value="<?= $website[0]['address2']; ?>" class="form-control" id="address2" placeholder="Address 2">
                </div>
              </div>
			  <div class="form-group">
                <label for="websitename" class="col-sm-2 control-label"></label>

                <div class="col-sm-3">
                  <input type="text" name="city" value="<?= $website[0]['city']; ?>" class="form-control" id="city" placeholder="City">
                </div>
				<div class="col-sm-3">
                  <input type="text" name="state" value="<?= $website[0]['state']; ?>" class="form-control" id="state" placeholder="state">
                </div>
				<div class="col-sm-3">
                  <input type="text" name="zipcode" value="<?= $website[0]['zipcode']; ?>" class="form-control" id="zipcode" placeholder="Zip Code">
                </div>
              </div>
			  <div class="form-group">
                <label for="email" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="text" name="email" value="<?= $website[0]['email']; ?>" class="form-control" id="email" placeholder="E-Mail Address">
                </div>
              </div>
			  <div class="form-group">
                <label for="phone" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                  <input type="text" name="phone" value="<?= $website[0]['phone']; ?>" class="form-control" id="phone" placeholder="Phone Number">
                </div>
              </div>
              <div class="form-group">
                <label for="test_link" class="col-sm-2 control-label">Test Link</label>

                <div class="col-sm-9">
                  <input type="text" name="test_link" value="<?= $website[0]['test_link']; ?>" class="form-control" id="test_link" placeholder="Test Link">
                </div>
              </div>

              <div class="form-group">
                <label for="live_link" class="col-sm-2 control-label">Live Link</label>

                <div class="col-sm-9">
                  <input type="text" name="live_link" value="<?= $website[0]['live_link']; ?>" class="form-control" id="live_link" placeholder="Live Link">
                </div>
              </div>
			  <?php if(count($website[0]['all_documents'])==0){?>
			  <div class="form-group">
                <label for="document" class="col-sm-2 control-label">Documents</label>
				<div class="col-sm-2">
                  <input type="text" name="document_name[]" class="form-control" placeholder="Name">
                </div>
                <div class="col-sm-7">
                  <input type="text" name="document_link[]" class="form-control" placeholder="Link">
                </div>
				<div class="col-sm-1">
                  <a href="javascript:void(0)" id="doc"><button type="button" class="btn btn-sm btn-outline-dark"><i style="" class="fa fa-plus-square" aria-hidden="true"></i></button></a>
                </div>
              </div>
			  <?php }else {?>
			  <?php $i=1; foreach($website[0]['all_documents'] as $row): ?>
			  <div class="form-group">
                <label for="document" class="col-sm-2 control-label"><?php if($i==1){?>Documents<?php }?></label>
				<div class="col-sm-2">
                  <input type="text" name="document_name[]" value="<?= $row['document_name']; ?>" class="form-control" placeholder="Name" required>
                </div>
                <div class="col-sm-7">
                  <input type="text" name="document_link[]" value="<?= $row['document_link']; ?>"class="form-control" placeholder="Link" required>
				  <a href="javascript:void(0)" class="doc_remove"><i style="" class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="col-sm-1">
				  <?php if($i==1){?>
				  <a href="javascript:void(0)" id="doc"><button type="button" class="btn btn-sm btn-outline-dark"><i style="" class="fa fa-plus-square" aria-hidden="true"></i></button></a>
				  <?php }?>
				</div>
              </div>
			  <?php $i++; endforeach; ?>
			  <?php }?>
			  <div id="outerdiv"></div>
			  <div class="form-group">
                <label for="image" class="col-sm-2 control-label">Logo</label>
                <div class="col-sm-9">
                  <input type="file" name="file" class="form-control"  accept="image/gif, image/jpeg, image/jpg ,image/png" id="image" onchange="readURL(this);"  placeholder=""><br>
				  <center><img id="blah" style="text-align:center;width:250px;height:40px;border:1px dashed grey;" src="<?php echo base_url();?>images/websites/thumb/<?= $website[0]['image']; ?> " alt=""></center>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-11">
                  <input type="submit" name="submit" value="Update Website" class="btn btn-outline-dark pull-right">
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
      </div>
    </div>
  </div>  

</section> 
<script>
   function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = Add FileReader();
    
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
    
          $('#blah').hide();
          $('#blah').fadeIn(650);
    
        }
    
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imgInp").change(function() {
      readURL(this);
    });
		$("#doc").click(function() {
	  $('<div class="form-group doc_div" >'+
			'<label for="document" class="col-sm-2 control-label"></label>'+
			'<div class="col-sm-2">'+
			  '<input type="text" name="document_name[]" class="form-control" placeholder="Name">'+
			'</div>'+
			'<div class="col-sm-7">'+
			  '<input type="text" name="document_link[]" class="form-control"  placeholder="Link">'+
			  '<a href="javascript:void(0)" class="doc_close"><i style="" class="fa fa-times" aria-hidden="true"></i></a>'+
			'</div>'+
			'<div class="col-sm-1">'+
			'</div>'+
		  '</div>').appendTo('#outerdiv');
	});
	$(document).on('click','.doc_close',function(e){
		e.preventDefault();
		$(this).closest('.doc_div').remove();
		return false;
	});
</script>