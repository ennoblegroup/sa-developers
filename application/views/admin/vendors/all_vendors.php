<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',6);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
?>
<!--**********************************
	Content body start
***********************************-->
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Vendors</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/vendors/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Vendor</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>										
										<!--th>Form Source</th-->
										<th>Vendor Id</th>
										<th>Vendor</th>
										<th>Logo</th>
										<th>Email Address</th>
										<th>Contact Person</th>									
										<th>Actions</th>										
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_vendors as $row): ?>
									<tr>
										<td><?= $row['vendor_id']; ?></td>
										<!--td><?= $row['file_source']; ?></td-->
										<td><?= $row['vendor_name']; ?></td>
										<td>
											<a href="<?= base_url() ?>images/vendors/<?= $row['image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
												<img src="<?= base_url() ?>images/vendors/<?= $row['image']; ?> " style="width: 100px;height: 40px;" alt="<?= $row['image']; ?>"> 
                                            </a>
										</td>
										<td><?= $row['vendor_email']; ?></td>
										<td><?= $row['lastname']; ?> <?= $row['firstname']; ?></td>
										<td> 
											<a href="<?= base_url('admin/vendors/edit/'.$row['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark  <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></button> </a>
											<a data-toggle="tooltip" title="Delete" href="#" id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark <?php echo $user_classd;?>"><i class="fa fa-trash-o"></i></a>
											
										</td>
									</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Venor</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open_multipart(base_url('admin/vendors/add'), 'id="addForm" class="form-valid"');  ?> 
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="vendor_name" placeholder="Venor Name" name="vendor_name">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" placeholder="Description" name="vendor_description"></textarea>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="input-group err">
										<div class="custom-file">
											<input type="file" onchange="readURL(this);" class="custom-file-input " id="imacge" name="userfile" accept="image/x-png,image/gif,image/jpeg">
											<label class="custom-file-label">Choose Profile Picture</label>
										</div>
									</div>
									<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
										<img id="blah" style="width:100%;height:200px" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
									</div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/vendors/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
</script>
<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});
</script>
<script>
$("#nav_roles").addClass('active');
</script> 