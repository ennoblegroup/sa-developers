<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/vendors/vendor_materials'); ?>">Vendor Materials</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Add Material</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
									
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Add Form</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="form-validation">
							<!--div class="row">
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
                                            <label class="col-form-label">Insurance Company</label>
                                            <select name="insurance_company_id" class="form-control">
                                                <?php $i=1; foreach ($all_vendors as $key=>$phar){ ?>
												<option value="<?php echo $phar['id']?>"><?php echo $phar['vendor_name']?></option>
												<?php $i++; }?>
											</select>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<label class="col-form-label">Template</label>
											<select name="form_id" class="form-control">
                                                <?php $i=1; foreach ($all_forms as $key=>$phar){ ?>
												<option value="<?php echo $phar['id']?>"><?php echo $phar['form_name']?></option>
												<?php $i++; }?>
											</select>
										</div>
									
									</div>
								</div>
							</div-->
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-4">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th colspan="2">Vendors</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												foreach ($all_vendors as $key=>$phar) {
												?>
												<tr>
													<td width="120">
														<input type="radio" name="vendor_id" value="<?php echo $phar['id']?>" required>
														<a href="<?php echo base_url();?>images/vendors/<?php echo $phar['file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
															<img src="<?php echo base_url();?>images/vendors/<?php echo $phar['file']; ?> " style="width: 100px;height: 40px;" > 
														</a>
													</td>
													<td> <?php echo $phar['vendor_name']?></td>
												</tr>
												<?php }?>
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<div class="col-md-4">
								<div class="card card-primary card-outline">
									<!-- /.card-header -->
									<div class="card-body">
										<table id="example1" class="table table-bordered table-striped dataTable">
											<thead>
												<tr>
													<th colspan="2">Materials</th>
												<tr>
												</tr>
											</thead>
											<tbody>
												<?php  foreach ($all_materials as $key=>$phar){ ?>
												<tr>
													<td width="120">
														<input type="checkbox" name="material_id[]" class="checkboxes ck<?php echo $phar['m_id']?>" value="<?php echo $phar['m_id']?>">  
														<a href="<?php echo base_url();?>images/materials/<?php echo $phar['m_file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
															<img src="<?php echo base_url();?>images/materials/<?php echo $phar['m_file']; ?> " style="width: 100px;height: 40px;" > 
														</a>
													</td>
													<td> <?php echo $phar['material_name']?>
													</td>
												</tr>
												<?php }?>
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /. box -->
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For Billing Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
	$(document).ready(function(){
		$("input[type='radio']").click(function(){
			
			$("input[type='radio']").each(function(i, obj) {
				if($(obj).prop("checked")){
					$(obj).parent().parent().css("background","thistle");
				}
				else{
					$(obj).parent().parent().css("background","white");
				}
				
			});
			
			
			$("input[type='checkbox']").each(function(i, obj) {
				$(obj).prop("checked",false);
				$(obj).parent().parent().css("background","white");
			});
			
			console.log($(this).val());
			var vendor_id = $(this).val();
			//var role_id = document.getElementById('role_id').value;
			var url = "<?php echo base_url()?>admin/vendors/get_mapped_templates";
			 $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                //data: { 'menu_id' : menu_id,'role_id' : role_id },
				data: { 'vendor_id' : vendor_id},
                success: function( data, textStatus, jQxhr ){
					console.log(data);
					if(data.length){
						
						$("input[type='checkbox']").each(function(i, obj) {
							for(var i=0;i<data.length;i++){
								if(data[i].material_id==$(obj).val()){
									
									$(obj).prop("checked",true);
									$(obj).parent().parent().css("background","thistle");
								}
								
							}						
						});
					}   
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
			
		});

		$("input[type='checkbox']").click(function(i, obj) {
			if($(this).prop("checked")){
				var url = "<?php echo base_url()?>admin/vendors/add_vendor_material";
				var material_id = $(this).val();
				var vendor_id = $("input[name='vendor_id']:checked").val()
				var submit = 'submit';
				$.ajax({
					url: url,
					dataType: 'json',
					type: 'post',
					//data: { 'menu_id' : menu_id,'role_id' : role_id },
					data: { 'vendor_id' : vendor_id,'material_id':material_id,'submit':submit},
					success: function( data, textStatus, jQxhr ){
						console.log(data);
						toastr.success("", data, {
						timeOut: 5000,
						closeButton: !0,
						debug: !1,
						AddestOnTop: !0,
						progressBar: !0,
						positionClass: "toast-top-right",
						preventDuplicates: !0,
						onclick: null,
						showDuration: "300",
						hideDuration: "1000",
						extendedTimeOut: "1000",
						showEasing: "swing",
						hideEasing: "linear",
						showMethod: "fadeIn",
						hideMethod: "fadeOut",
						tapToDismiss: !1
					})  
					},
					error: function( jqXhr, textStatus, errorThrown ){
						console.log( errorThrown );
					}
				});
			}
			else{
				var url = "<?php echo base_url()?>admin/vendors/del_vendor_material";
				var material_id = $(this).val();
				var vendor_id = $("input[name='vendor_id']:checked").val()
				var submit = 'submit';
				$.confirm({
				title: 'Are you sure?',
				content: 'Do you really want to delete these records? This process cannot be undone.',
				buttons: {
					confirm: {
					text: 'Confirm',
					btnClass: 'btn btn-outline-dark',
						keys: ['enter', 'shift'],
						action: function(){			
							$.ajax({
								url: url,
								dataType: 'json',
								type: 'post',
								//data: { 'menu_id' : menu_id,'role_id' : role_id },
								data: { 'vendor_id' : vendor_id,'material_id':material_id,'submit':submit},
								success: function( data, textStatus, jQxhr ){
									console.log(data);
									toastr.error("", data, {
									timeOut: 5000,
									closeButton: !0,
									debug: !1,
									AddestOnTop: !0,
									progressBar: !0,
									positionClass: "toast-top-right",
									preventDuplicates: !0,
									onclick: null,
									showDuration: "300",
									hideDuration: "1000",
									extendedTimeOut: "1000",
									showEasing: "swing",
									hideEasing: "linear",
									showMethod: "fadeIn",
									hideMethod: "fadeOut",
									tapToDismiss: !1
								})  
								},
								error: function( jqXhr, textStatus, errorThrown ){
									console.log( errorThrown );
								}
							});
						}
					},
					cancel: {
					text: 'cancel',
					btnClass: 'btn btn-outline-dark',
						keys: ['enter', 'shift'],
						action: function(){
							$(".ck"+material_id).prop('checked', true);
							}
						}
					}
				});
			}
			
		});
	})
</script>
<script>
	$(document).ready(function(){
		
		var check = $("input[type='checkbox']");
		var submitButt = $("button[type='submit']");

		submitButt.click(function(e) {
			var count = 0;
			
			for(var i=0;i<check.length;i++){
				console.log(check[i].checked);
				if(check[i].checked == true){
					count = count+1;
				}
			}
			if(count==0){
				alert('Please check atleast one template');
				e.preventDefault();
			}
			
		});
	});
</script>