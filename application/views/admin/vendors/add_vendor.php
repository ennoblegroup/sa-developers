<style>
td .form-group{
	padding:0px;
}
</style>
<?php echo form_open_multipart(base_url('admin/vendors/add'), 'id="quickForm" class="form-valid"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/vendors'); ?>">Vendors</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Add Vendor</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/vendors" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body">
								<table id="example1" class="table">
									<thead>
										<tr>
											<th colspan="4">Vendor Details</th>
									</thead>
									<tbody>
										<tr colspan="">
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="vendor_name" placeholder="*Vendor Name" title="Vendor Name" name="vendor_name" >
												</div>
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="vendor_email" placeholder="*Email Address" title="Email Address" name="vendor_email" >
												</div> 
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control us_phone" id="vendor_phone_number" placeholder="Phone Number" title="Phone Number" name="vendor_phone_number">
												</div>
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<select  title="Select Categories" multiple data-live-search="true"  class="selectpicker form-control" name="category_id[]">
														<?php $i=1; foreach($all_categories as $row): ?>
														<option value="<?= $row['id']; ?>"><?= $row['category_name']; ?> </option>
														<?php $i++; endforeach; ?>
													</select>
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="address1" placeholder="*Address1" title="Address1" name="address1" maxLength="40">
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
												</div> 
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="city" placeholder="*City" title="City" name="city" maxLength="30">
												</div>
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<select placeholder="State" class="form-control selectpicker" data-live-search="true"  name="state" id="state" title="*Select State">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="Zip Code" name="zip_code" maxLength="10">
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="website_url" placeholder="*Website URL" title="Website URL" name="website_url" >
												</div>
											</td>
											<td colspan="1">
												<div class="form-group col-lg-12 col-md-12">
													<div class="input-group err">
														<div class="custom-file">
															<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg">
															<label class="custom-file-label">Select Logo</label>
														</div>
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
								<div class="" style="padding-left:30%;width:70%; margin-top: 10px;">
									<img id="blah" style="width:100%;height:100px" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /. box -->
					</div>
					<div class="col-md-12">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
								<table id="example1" class="table">
									<thead>
										<tr>
											<th colspan="3">Contact Details</th>
									</thead>
									<tbody>
										<tr>
											<td>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="last_name" placeholder="*Last Name" title="Last Name" name="last_name" >
												</div>
											</td>
											<td>										
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="first_name" placeholder="*First Name" title="First Name" name="first_name" >
												</div>
											</td>
											<td>								
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="middle_name" placeholder="Middle Name" title="Middle Name" name="middle_name">
												</div>	
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="email" placeholder="*Email Address" title="Email Address" name="email" >
												</div> 
											</td>
											<td>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control us_phone" id="phone_number" placeholder="Phone Number" title="Phone Number" name="phone_number">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<?php echo form_close( ); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

$(document).ready(function () {
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Please enter a valid Zip Code.");
	$.validator.addMethod("letters_numbers_special", function(value, element) {
    	return this.optional(element) || /^[a-zA-Z0-9!@#$&()` .+,/"-]*$/i.test(value);
	//(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
	}, "");
  	$('#quickForm').validate({	  
		rules: {			
		address1: {
			required: true
		},
		city: {
			required: true
		},
		state: {
			required: true
		},
		zip_code: {
			required: true,
			zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
		},
		last_name: {
			required: true
		},
		first_name: {
			required: true
		},
		vendor_name: {
			required: true
		},
		image: {
			required: true
		},
		email: {
			required: true,
			email: true
		},
		vendor_email: {
			required: true,
			email: true
		},
		},
		messages: {
		address1: {
			required: "Please enter Address 1"
		},
		city: {
			required: "Please enter City"
		},
		state: {
			required: "Please select State"
		},
		zip_code: {
			required: "Please enter Zip Code",
			zip_regex: "Please enter valid Zip Code"
		},
		last_name: {
			required: "Please enter Last Name"
		}, 
		first_name: {
			required: "Please enter First Name"
		},
		vendor_name: {
			required: "Please enter Vendor Name"
		},
		image: {
			required: "Please upload Vendor Logo"
		},		
		email: {
			required: "Please enter Email Address",
			email: "Please enter a valid Email Address"
		},
		vendor_email: {
			required: "Please enter Email Address",
			email: "Please enter a valid Email Address"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
});
</script>