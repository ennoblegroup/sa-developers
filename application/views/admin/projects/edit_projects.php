<style>
.table thead th 
{
	text-transform:none;
	font-size: 14px;
}
.table th, .table td
{
	border-top: 0px solid #EEEEEE;
}

element.style {
    /* margin-top: 5px; */
}
.form-control {
    margin-top: 5px;
}
.dropzone .dz-preview .dz-progress {
    display:none;
}
.sdf{
	    min-height: 250px;
    max-height: 250px;
    overflow-y: scroll;
}
.table th, .table td {
    padding: 10px 5px;
}
fieldset {
    border: 2px ridge #dfd8d8;;
	min-width: 0;
    padding: 5px;
    margin: 5px;
}

legend {
	display: block;
    width: auto;
    padding: 0px 5px;
    font-size: 15px;
    line-height: inherit;
    color: inherit;
    white-space: normal;
    font-weight: bold;
}
label {
    padding-left: 10px;
}
#qq .form-check-inline {
    width: 20%;
}
.dropdown-menu .inner {
    max-height: 100.3px !important;
}
</style>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />		
<?php echo form_open(base_url('admin/projects/edit'), 'id="quickForm" enctype="multipart/form-data" class="form-horizontal"');  ?> 		
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/projects">All Projects</a></li>
							<li class="breadcrumb-item active">Edit Project</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/projects" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12" style="margin-bottom: 10px;">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
								<table id="example1" class="table">
									<thead>
										<tr>
											<th colspan="3">Project Details</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="2">
												<select placeholder="Client Name" class="form-control selectpicker" data-live-search="true"  name="client_id" id="client_id" title="Client Name">
													<option value="">Select Client</option>
													<?php foreach($all_clients as $client){ ?>
														<option value="<?php echo $client['client_id']?>" <?php if($project['uid']==$client['id']){echo 'selected';}?>><?php echo $client['lastname']?> <?php echo $client['firstname']?></option>
													<?php }?>	
												</select>
											</td>
											<td>
												<select placeholder="Select Services" multiple class="form-control selectpicker" data-live-search="true"  name="service_id[]" id="service_id" data-size="5" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Services ({0} selected)" title="Services">
													<?php foreach($all_services as $service){ ?>
														<option value="<?php echo $service['id']?>"><?php echo $service['service_name']?></option>
													<?php }?>	
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" id="project_name" placeholder="*Project Name" title="Project Name" value="<?php echo $project['project_name']?>" name="project_name" >
												<input type="hidden" class="form-control"  value="<?php echo $project['id']?>" name="project_id" >
												<input type="hidden" class="form-control"  value="<?php echo $project['status']?>" name="status" >
											</td>
											<td>
												<input type="text" class="form-control" id="project_start_date" name="project_start_date" value="<?php echo date("m/d/Y", strtotime($project['project_start_date']) );?>" placeholder="Start Date" >
											</td>
											<td>
												<input type="text" class="form-control" id="project_end_date" name="project_end_date" value="<?php echo date("m/d/Y", strtotime($project['project_end_date']) );?>" placeholder="End Date" >
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div class="">
													<input type="text" class="form-control" id="address1" value="<?php echo $project['address1']?>" placeholder="*Address1" title="Address1" name="address1" maxLength="40">
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div class="">
													<input type="text" class="form-control" id="address2" placeholder="Address2" value="<?php echo $project['address2']?>" title="Address2" name="address2" maxLength="40">
												</div> 
											</td>
										</tr>
										<tr>
											<td>
												<div class="">
													<input type="text" class="form-control" id="city" placeholder="*City" title="City" value="<?php echo $project['city']?>" name="city" maxLength="30">
												</div>
											</td>
											<td>
												<div class="">
													<select placeholder="State" class="form-control selectpicker" data-live-search="true"  name="state" id="state" title="*Select State">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($project['state']==$state['state_code']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
											</td>
											<td>
												<div class="">
													<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="Zip Code" value="<?php echo $project['zip_code']?>" name="zip_code" maxLength="10">
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /.card -->
					</div>
					<div class="col-md-12" style="margin-bottom: 10px;">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
								<table id="example1" class="table">
									<thead>
										<tr>
											<th >Project Description</th>
									</thead>
									<tbody>
										<tr>
											<td>
												<textarea class="editor" name="description" id="description" required>
												<?php echo $project['project_description']?>
												</textarea>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /. box -->
					</div>
					<div class="col-md-12" style="margin-bottom: 10px;">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
								<div class="form-group" style="    text-align: center;">
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" id="tt" name="website_access" <?php if ($project['website_access']['status']==1){ echo 'checked';}?> class="form-check-input" >Would you like to show project Information on website
										</label>
									</div>
								</div>
								<div class="form-group" id="qq" style="display:none">
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" <?php if ($project['website_access']['project_address']==1){ echo 'checked';}?> name="project_address">Project Address
										</label>
									</div>
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" <?php if ($project['website_access']['client_infornation']==1){ echo 'checked';}?> name="client_infornation">Client Information
										</label>
									</div>
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" <?php if ($project['website_access']['project_gallery']==1){ echo 'checked';}?> name="project_gallery">Project Gallery
										</label>
									</div>
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" <?php if ($project['website_access']['project_reviews']==1){ echo 'checked';}?> name="project_reviews">Project Reviews
										</label>
									</div>
									<div class="form-check form-check-inline">
										<label class="form-check-label">
											<input type="checkbox" class="form-check-input" <?php if ($project['website_access']['project_vendors']==1){ echo 'checked';}?> name="project_vendors">Project Vendors
										</label>
									</div>
								</div>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /. box -->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
<?php foreach($project['services'] as $pp){ ?>
var sid = "<?php echo $pp['service_id']?>";
$("#service_id option[value="+sid+"]").attr('selected','selected');
<?php }?>
</script>
<script>
$(document).ready(function(){
	if ($("#tt").is(':checked')){
		$('#qq').css('display','block');
	}
	$("#tt").change(function() {
		if(this.checked) {
			$('#qq').css('display','block');
		}else{
			$('#qq').css('display','none');
		}
	});


	$('input[type="radio"]').change(function() {
		if ($(this).is(':checked')){ //radio is now checked
			var id =$(this).attr("category_id");
			$('.ccheckbox'+id).prop('checked', false);
			var radio_arr = [];
			$(".default_radio:checked").each(function() {
				radio_arr.push($(this).val());
			});
			console.log(radio_arr);
		}
	});

	$('input[type="checkbox"]').change(function() {
		if ($(this).is(':checked')){
			var id =$(this).attr("category_id");
			$('.dradio'+id).prop('checked', false); //unchecks all checkboxes
			var checkbox_arr = [];
			$(".choice_checkbox:checked").each(function() {
				checkbox_arr.push($(this).val());
			});
			console.log(checkbox_arr);
		}
	});
})
</script>
<script>
$(document).ready(function () {
	$("#project_start_date").bootstrapMaterialDatePicker({
		format: 'MM/DD/YYYY',
		minDate: null,
		time: false,
		date: true,
		clearBtn:true,	
	}).on('change', function(e, date) {
		$("#project_end_date").bootstrapMaterialDatePicker('setMinDate', date);
	});
	$("#project_end_date").bootstrapMaterialDatePicker({
		format: 'MM/DD/YYYY',
		time: false,
		date: true,
		clearBtn:true,
	});
	
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Please enter a valid Zip Code.");
	$.validator.addMethod("letters_numbers_special", function(value, element) {
    	return this.optional(element) || /^[a-zA-Z0-9!@#$&()` .+,/"-]*$/i.test(value);
	//(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
	}, "");
  	$('#quickForm').validate({	  
		rules: {			
		address1: {
			required: true
		},
		city: {
			required: true
		},
		state: {
			required: true
		},
		zip_code: {
			required: true,
			zip_regex: /(^\d{5}$)|(^\d{5}-\d{4}$)/
		},
		project_name: {
			required: true
		},
		project_type: {
			required: true
		},
		project_period: {
			required: true
		},
		project_start_date: {
			required: true
		},
		client_id: {
			required: true
		},
		},
		messages: {
		address1: {
			required: "Please enter Address 1"
		},
		city: {
			required: "Please enter City"
		},
		state: {
			required: "Please select State"
		},
		project_start_date: {
			required: "Please select Project Start Date"
		},
		zip_code: {
			required: "Please enter Zip Code",
			zip_regex: "Please enter valid Zip Code"
		},
		project_name: {
			required: "Please enter Project Name"
		}, 
		project_type: {
			required: "Please enter Project Type"
		}, 
		project_period: {
			required: "Please select Project Period"
		},
		client_id: {
			required: "Please select Client"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('td').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
	
	$(document).ready(function(){
		$('body').on('change input', '#username', function() {
			var user_name = $(this).val();
			jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>" + "admin/projects/fetch_uname",
			dataType: 'json',
			data: {"user_name": user_name},
			success: function(response) {
				if(response == false) {
					$(".erroruname").remove();
					$('#username').val("");
					$('#username').after('<span class="error erroruname" >Username is taken already</span>');
				} 
				else {
				 $(".erroruname").remove();
				}
			},
			});
		});
	});

   $('body').on('input', '#first_name', function() {
	   //alert(document.getElementById('lastname').value);
	   var uname=document.getElementById('last_name').value+document.getElementById('first_name').value;
	    $("#username").val(uname.toLowerCase());
   });

  $('body').on('input', '#last_name,', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
	$('body').on('input', '#first_name', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});
	$('body').on('input', '#middle_name', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});  
	$('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
	$('body').on('input', '#dea_number', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z0-9.\s]/g, ''));
	});
	$('body').on('input', '#license_number', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z0-9.\s]/g, ''));
	});	
});
</script>