<style>
.nopad {
	padding-left: 0 !important;
	padding-right: 0 !important;
}
/*image gallery*/
.image-checkbox {
	cursor: pointer;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 4px solid transparent;
	margin-bottom: 0;
	outline: 0;
}
.image-checkbox input[type="checkbox"] {
	display: none;
}
.image-checkbox input[type="radio"] {
	display: none;
}
.image-checkbox-checked {
	border-color: #4783B0;
}
.image-checkbox .fa {
    position: absolute;
    color: #4A79A3;
    background-color: #fff;
    padding: 3px;
    top: 4px;
    right: 4px;;
}
.image-checkbox-checked .fa {
  display: block !important;
}
.hidden {
    display: none!important;
}

$red: #ff4136;
$green: #2ecc40;
$blue: #0074d9;

.filter-cat {
    margin: 1em 0;
}

.f-cat {
    color: #fff;
    padding: 0px;
    border: 5px solid #fff;
}

.f-cat[data-cat='cat-red'] {
    background: $red;
}
.f-cat[data-cat='cat-green'] {
    background: $green;
}
.f-cat[data-cat='cat-blue'] {
    background: $blue;
}

.filter-cat-results .f-cat {
    opacity: 0;
    display: none;
}
.filter-cat-results2 .f-cat {
    opacity: 0;
    display: none;
}
@-webkit-keyframes fadeIn {
	0% { opacity: 0; }
	100% { opacity: 1; }
}

@keyframes fadeIn {
	0% { opacity: 0; }
	100% { opacity: 1; }
}
.filter-cat-results .f-cat.active {
    opacity: 1;
    display: block;
    -webkit-animation: fadeIn 0.65s ease forwards;
    animation: fadeIn 0.65s ease forwards;
}
.filter-cat-results2 .f-cat.active {
    opacity: 1;
    display: block;
    -webkit-animation: fadeIn 0.65s ease forwards;
    animation: fadeIn 0.65s ease forwards;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">All projects</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a type="button" href="javascript:void(0)" class="form btn btn-outline-dark" id="submit_product"><!--i class="fa fa-ban"></i-->Submit</a>&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/projects" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="filter-cat row" style="margin: 5px;background-color: #fff;padding: 10px 0px;">
			<div class="col col-md-5 col-xs-6"></div>
			<div class="col col-md-2 col-xs-6">
				<select class="form-control ee">
					<option value="cat1">Before - After</option>
					<option value="cat2">After - Before</option>
				</select>
			</div>			
		</div>
		<div class="row">
			<div class="col-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title card-title1">Before Images</h4>
					</div>
					<form>
					<div class="card-body custom_body filtering" >
						<input type="hidden" value="<?php echo $project_id; ?>" id="project_id">
						
						<div class="row filter-cat-results">
							<?php $i=1; foreach($all_gallery as $row): ?>
							<div class="col-xs-2 col-sm-2 col-md-2 nopad text-center f-cat" data-cat="cat<?= $row['gallery_type']; ?>">
								<label class="image-checkbox">
								  <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $row['file']; ?>" />
								  <input type="radio" name="image[]" gallery_type="<?= $row['gallery_type']; ?>" value="<?= $row['id']; ?>" />
								  <i class="fa fa-check hidden"></i>
								</label>
							</div>
							<?php $i++; endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title card-title2">After Images</h4>
					</div>
					<div class="card-body custom_body filtering" >
						<div class="row filter-cat-results2">
							<?php $i=1; foreach($all_gallery as $row): ?>
							<div class="col-xs-2 col-sm-2 col-md-2 nopad text-center f-cat" data-cat="cat<?= $row['gallery_type']; ?>">
								<label class="image-checkbox">
								  <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $row['file']; ?>" />
								  <input type="checkbox" name="image[]" value="<?= $row['id']; ?>" />
								  <i class="fa fa-check hidden"></i>
								</label>
							</div>
							<?php $i++; endforeach; ?>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
// image gallery
// init the state from the input
$(".filter-cat-results2 .image-checkbox").each(function () {
  if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
    $(this).addClass('image-checkbox-checked');
  }
  else {
    $(this).removeClass('image-checkbox-checked');
  }
});

// sync the state to the input
$(".filter-cat-results2 .image-checkbox").on("click", function (e) {
  $(this).toggleClass('image-checkbox-checked');
  var $checkbox = $(this).find('input[type="checkbox"]');
  $checkbox.prop("checked",!$checkbox.prop("checked"))

  e.preventDefault();
});


$(".filter-cat-results .image-checkbox").each(function () {
  if ($(this).find('input[type="radio"]').first().attr("checked")) {
	$('.filter-cat-results .image-checkbox').removeClass('image-checkbox-checked');
    $(this).addClass('image-checkbox-checked');
  }
  else {
    $('.filter-cat-results .image-checkbox').removeClass('image-checkbox-checked');
  }
});

// sync the state to the input
$(".filter-cat-results .image-checkbox").on("click", function (e) {
  $(this).toggleClass('image-checkbox-checked');
  var $checkbox = $(this).find('input[type="radio"]');
  $('.filter-cat-results .image-checkbox').removeClass('image-checkbox-checked');
  $(this).addClass('image-checkbox-checked');
  $(this).prop("checked",$checkbox.prop("checked"))
  
  e.preventDefault();
});

var filterActive;

function filterCategory(category) {
	if (filterActive != category) {
		
		// reset results list
		$('.filter-cat-results .f-cat').removeClass('active');
		$('.filter-cat-results2 .f-cat').removeClass('active');
		
		$('.filter-cat-results .f-cat').filter('[data-cat="' + category + '"]').addClass('active');
		
		if(category=='cat1'){
			$('.card-title1').text('Before Images');
			$('.card-title2').text('After Images');
			$('.filter-cat-results2 .f-cat').filter('[data-cat="cat2"]').addClass('active');
		}else{
			$('.card-title1').text('After Images');
			$('.card-title2').text('Before Images');
			$('.filter-cat-results2 .f-cat').filter('[data-cat="cat1"]').addClass('active');
		}
		
		
		filterActive = category;
	}
}

$('.filter-cat-results .f-cat').filter('[data-cat="cat1"]').addClass('active');
$('.filter-cat-results2 .f-cat').filter('[data-cat="cat2"]').addClass('active');

$('.ee').change(function() {
	$('.image-checkbox').removeClass('image-checkbox-checked');
	if ($(this).val() == 'cat-all') {
		$('.filter-cat-results .f-cat').addClass('active');
		filterActive = 'cat-all';
	} else {
		filterCategory($(this).val());
	}
});

$('body').on('click', '#submit_product,', function() {
	var project_id = $('#project_id').val();
	var checkbox_images = [];

	var radio_image = $('.filter-cat-results .image-checkbox-checked input[type="radio"]').val();
	var gallery_type = $('.filter-cat-results .image-checkbox-checked input[type="radio"]').attr('gallery_type');
	$('.filter-cat-results2 .image-checkbox-checked input[type="checkbox"]').each(function(i, obj) {
		checkbox_images.push($(this).val());
	});
	if(radio_image !='' && checkbox_images !=''){
		$.ajax({
			url:"../add_mapping_project_images",
			method:"POST",
			async: false,
			dataType:"json",
			data: {project_id: project_id,radio_image:radio_image,gallery_type:gallery_type,checkbox_images:checkbox_images},
			success: function(data)
			{	
				window.location = '<?php  echo site_url("admin/projects/gallery/"); ?>'+project_id;								
			}
		})
	}else{
		alert('Please map images correctly');
	}

});
</script>
