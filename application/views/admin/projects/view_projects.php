
<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<style>
td:first-child a{ color: ##3989d3 !important;text-decoration:underline }
#qw li {
	list-style: disc;
    font-size: 15px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">All projects</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<?php if( $_SESSION['sadevelopers_admin']['client_id']==0){?>
					<a href="<?= base_url('admin/projects/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add project</button></a>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL projects</h4>
					</div-->
					<div class="card-body custom_body">	
						<div class="table-responsive">
							<input type="hidden" id="cntid" value="<?php echo $client_id; ?>">
							<table id="project_table" style="width:100%">
								<thead>
									<tr>
										<th style="width:8% !important;">Project ID</th>
										<th style="width:10% !important;">project Name</th>
										<th style="width:5% !important;">Location</th>
										<?php if($client_id =='') {?>
										<th style="width:15% !important;">Client Name</th>
										<?php }?>
										<!--th>Project Start Date</th>
										<th>Project End Date</th-->
										<th style="width:10% !important;">Project Services</th>											
										<th style="width:10% !important;" class="<?php echo $user_classs;?>" >Status</th>
										<th style="width:10% !important;">Website Status</th>
										<th style="width:20% !important;">Actions</th>
									</tr>
								</thead>
								<tbody id="filter_data">
								
								</tbody>
							</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Update Status</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form  method="POST" action="<?= base_url('admin/projects/add_project_status_history'); ?>" id="claim_status_update" class="claim_status_update">
			<div class="modal-body">
				<div class="row">																
					<div class="col-md-3 form-group">
						<input type="hidden" value="" id="project_id" name="project_id">
					</div>
					<div class="col-md-6 form-group">
						<select name="status_name" id="status_name" class="form-control selectpicker" >
							<?php $i=1; foreach($project_status as $row): ?>
							<option value="<?= $row['project_status_id']; ?>"><?= $row['project_status_name']; ?></option>
							<?php $i++; endforeach; ?>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<textarea class="form-control" name="status_description" placeholder="Status Description"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-dark">Update Status</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="serv">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title eew">Services List</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<ul id="qw">
								
								</ul>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$('body').on('click', '.service_list', function () {
	var id = $(this).attr('project_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/projects/get_project_services');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#qw').html(data);		
		}
	});
    $('#serv').modal('show');
});
function openUpdateStatus(id) {
	$("#project_id").val(id);
	$('#exampleModalLong').modal('show');
}
$(document).ready(function() {
	$('body').on('click', '#status', function () {
		if($(this).prop("checked") == true){
			var id = 1;
		}
		else {
			var id = 0;
		}
		var project_id = $(this).attr('project_sid');
		$.ajax({
		  type: "POST",
		  url:"<?php echo base_url('admin/projects/update_status');?>",
		  data: {id: id, project_id: project_id},
		  cache: false,
		  success: function(data){
			  filter_data();
			  //$("#status_roww"+project_id).load(" #status_roww"+project_id);
		  }
		});
	});
});
function filter_data()
{
	var client_id = $("#cntid").val();
	$.ajax({
		url:"projects/filter_projects",
		method:"POST",
		async: true,
		dataType:"json",
		data: {client_id: client_id},
		success: function(data)
		{	
			$('table').DataTable().clear();
			$('table').DataTable().destroy();			
			$('#filter_data').html(data.projects_list);				
			tableinit();										
		},
		error: function (data) {
			//var out =JSON.parse(data);
			console.log(data.responseText);
		},
		complete: function (data) {
		// Handle the complete event
			console.log(data);
		}
	})
}
function tableinit(){
	var tt= $('table').DataTable({
		"bRetrieve": true,
		"bSort" : true,
		"bProcessing": true,
		"bDestroy": false,
		"bPaginate": true,
		"bAutoWidth": false,
		"bInfo": true,
		"bJQueryUI": true,
		 "order": [],
		language: {
		"emptyTable": "No projects were found."
		}
	});
	tt.find('thead tr th').css('width', 'auto');
}
</script>
<script>
$(".removeRecord").click(function(){
   var id = $(this).attr('project_id');    	
	$.confirm({
	title: 'Are you sure?',
	content: 'Do you really want to delete these records? This process cannot be undone.',
	buttons: {
		confirm: {
		   text: '<i class="fa fa-check" aria-hidden="true"></i>Confirm',
		btnClass: 'form btn btn-red',
			keys: ['enter', 'shift'],
			action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/projects/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();    							 
							}
						});
					}
		},
		cancel: {
		   text: '<i class="fa fa-ban"></i>cancel',
		btnClass: 'form btn btn-grey',
			keys: ['enter', 'shift'],
			action: function(){
				}
			}
		}
	});
});
	
</script>

