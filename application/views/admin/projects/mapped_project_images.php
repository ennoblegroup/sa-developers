<style>
    .card{
        height: calc(100% - 10px);
        margin-bottom: 10px;
    }
    .delete{
        position: absolute;
        top: 0px;
        background: #ffffff;
        right: 5px;
        color: red;
    }
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-8 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/projects">Projects</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/projects/gallery/<?php echo $project_id;?>">Project Gallery</a></li>
							<li class="breadcrumb-item active">Mapped Images</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
	    <?php if(count($mapped_images)>0){?>
		<div class="row">
			<div class="col-2"><h4 class="">Before Image</h4></div>
			<div class="col-10"><h4 class="">After Images</h4></div>
		</div>	
        <?php $i=1; foreach($mapped_images as $row): ?>
		<div class="row">
			<div class="col-2">
				<div class="card">
					<div class="card-body custom_body filtering" >
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-12 nopad text-center f-cat">
								<label class="image-checkbox">
                                    <a href="<?= base_url() ?>images/project_gallery/<?= $row['before_image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
                                        <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $row['before_image']; ?>" />
                                    </a>
                                </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-10">
				<div class="card">
					<div class="card-body custom_body filtering" >
						<div class="row filter-cat-results2">
							<?php $j=1; foreach($row['after_images'] as $roww): ?>
							<div class="col-xs-1 col-sm-1 col-md-1 nopad text-center f-cat">
								<label class="image-checkbox">
                                    <a href="<?= base_url() ?>images/project_gallery/<?= $roww['file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
								        <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $roww['file']; ?>" />
                                        <a href="javascript:void(0)" image_id="<?= $roww['id']; ?>" class="unmap"><i class="fa fa-times delete"></i></a>
                                    </a>
                                </label>
							</div>
							<?php $j++; endforeach; ?>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
        <?php $i++; endforeach; ?>
        <?php }?>
         <?php if(count($mapped_images2)>0){?>
		<div class="row">
			<div class="col-2"><h4 class="">After Image</h4></div>
			<div class="col-10"><h4 class="">Before Images</h4></div>
		</div>						
		<?php $i=1; foreach($mapped_images2 as $row): ?>
		<div class="row">
			<div class="col-2">
				<div class="card">
					<div class="card-body custom_body filtering" >
						<div class="row ">
							<div class="col-xs-12 col-sm-12 col-md-12 nopad text-center f-cat">
								<label class="image-checkbox">
                                    <a href="<?= base_url() ?>images/project_gallery/<?= $row['after_image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
                                        <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $row['after_image']; ?>" />
                                    </a>
                                </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-10">
				<div class="card">
					<div class="card-body custom_body filtering" >
						<div class="row filter-cat-results2">
							<?php $j=1; foreach($row['before_images'] as $roww): ?>
							<div class="col-xs-1 col-sm-1 col-md-1 nopad text-center f-cat">
								<label class="image-checkbox">
                                    <a href="<?= base_url() ?>images/project_gallery/<?= $roww['file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
								        <img class="img-responsive" style="width:100%;height:50px" src="<?= base_url() ?>images/project_gallery/<?= $roww['file']; ?>" />
                                        <a href="javascript:void(0)" image_id="<?= $roww['id']; ?>" class="unmap"><i class="fa fa-times delete"></i></a>
                                    </a>
                                </label>
							</div>
							<?php $j++; endforeach; ?>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
        <?php $i++; endforeach; ?>
        <?php }?>
         <?php if(count($mapped_images)==0 && count($mapped_images2)==0){?>
         <center><p>No mapped images found</p></center>
         <?php }?>
	</div>
</div>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$(".unmap").click(function(){
   var id = $(this).attr('image_id');    	
	$.confirm({
	title: 'Are you sure?',
	content: 'Do you really want to delete these records? This process cannot be undone.',
	buttons: {
		confirm: {
		   text: 'Confirm',
		btnClass: 'form btn',
			keys: ['enter', 'shift'],
			action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/projects/unmap_image');?>",
						data:{'id':id},
						success: function(data){
							location.reload();    							 
							}
						});
					}
		},
		cancel: {
		   text: 'cancel',
		btnClass: 'form btn',
			keys: ['enter', 'shift'],
			action: function(){
				}
			}
		}
	});
});
</script>
