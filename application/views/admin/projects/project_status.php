<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<style>
.timeline {
    list-style-type: none;
    margin: 0;
    padding: 0;
    position: relative
}
<?php $i=1; foreach($project_updates as $row): ?>
.wmnt<?= $rowc['id']; ?> {
  display: none;
}
<?php $i++; endforeach; ?>
.f-s-12{
	border-radius:0px;
	line-height:1.6;
}
.timeline:before {
    content: '';
    position: absolute;
    top: 5px;
    bottom: 5px;
    width: 5px;
    background: #2d353c;
    left: 20%;
    margin-left: -2.5px
}

.timeline>li {
    position: relative;
    min-height: 50px;
    padding: 20px 0
}

.timeline .timeline-time {
    position: absolute;
    left: 0;
    width: 18%;
    text-align: right;
    top: 30px
}

.timeline .timeline-time .date,
.timeline .timeline-time .time {
    display: block;
    font-weight: 600
}

.timeline .timeline-time .date {
    line-height: 16px;
    font-size: 12px
}

.timeline .timeline-time .time {
    line-height: 38px;
    font-size: 20px;
    color: #242a30
}

.timeline .timeline-icon {
    left: 15%;
    position: absolute;
    width: 10%;
    text-align: center;
    top: 40px
}

.timeline .timeline-icon a {
    text-decoration: none;
    width: 20px;
    height: 20px;
    display: inline-block;
    border-radius: 20px;
    background: #d9e0e7;
    line-height: 10px;
    color: #fff;
    font-size: 14px;
    border: 5px solid #2d353c;
    transition: border-color .2s linear
}

.timeline .timeline-body {
    margin-left: 23%;
    margin-right: 17%;
    background: #e5e0e0;
    position: relative;
    padding: 20px 25px 40px 25px;
    border-radius: 6px
}

.timeline .timeline-body:before {
    content: '';
    display: block;
    position: absolute;
    border: 10px solid transparent;
    border-right-color: #fff;
    left: -20px;
    top: 20px
}

.timeline .timeline-body>div+div {
    margin-top: 5px
}

.timeline .timeline-body>div+div:last-child {
    margin-bottom: 0px;
    border-radius: 5px
}

.timeline-header {
    padding-bottom: 10px;
    border-bottom: 1px solid #e2e7eb;
    line-height: 30px
}

.timeline-header .userimage {
    float: left;
    width: 34px;
    height: 34px;
    border-radius: 40px;
    overflow: hidden;
    margin: -2px 10px -2px 0
}

.timeline-header .username {
    font-size: 16px;
    font-weight: 600
}

.timeline-header .username,
.timeline-header .username a {
    color: #2d353c
}

.timeline img {
    max-width: 100%;
    display: block
}

.timeline-content {
    letter-spacing: .25px;
    line-height: 18px;
    font-size: 13px
}
.timeline-content p{
	margin-bottom:5px;
}
.timeline-content:after,
.timeline-content:before {
    content: '';
    display: table;
    clear: both
}

.timeline-title {
    margin-top: 0
}

.timeline-footer {
    border-top: 1px solid #e2e7ec;
    padding-top: 15px
}

.timeline-footer a:not(.btn) {
    color: #575d63
}

.timeline-footer a:not(.btn):focus,
.timeline-footer a:not(.btn):hover {
    color: #2d353c
}

.timeline-likes {
    color: #6d767f;
    font-weight: 600;
    font-size: 12px;
	text-align:right;
}

.timeline-likes .stats-right {
    
}

.timeline-likes .stats-total {
    display: inline-block;
    line-height: 20px
}

.timeline-likes .stats-icon {
    float: left;
    margin-right: 5px;
    font-size: 9px
}

.timeline-likes .stats-icon+.stats-icon {
    margin-left: -2px
}

.timeline-likes .stats-text {
    line-height: 20px
}

.timeline-likes .stats-text+.stats-text {
    margin-left: 15px
}

.timeline-comment-box {
    background: #f2f3f4;
    margin-left: 0px;
    margin-right: 0px;
    padding: 5px 5px;
}

.timeline-comment-box .user {
    float: left;
    width: 34px;
    height: 34px;
    overflow: hidden;
    border-radius: 30px
}

.timeline-comment-box .user img {
    max-width: 100%;
    max-height: 100%
}

.timeline-comment-box .user+.input {
    margin-left: 44px
}

.lead {
    margin-bottom: 20px;
    font-size: 21px;
    font-weight: 300;
    line-height: 1.4;
}
.dropzone .dz-preview .dz-progress {
    display:none;
}
.text-danger, .text-red {
    color: #ff5b57!important;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/projects">Projects</a></li>
							<li class="breadcrumb-item active">Project Updates</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark "><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Project Update</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body custom_body">
						<ul class="timeline">
							<?php $i=0; foreach($project_updates as $row): ?>
						  <li>
							<?php 
								$date1 = strtotime($row['create_date']);
								$datee = new DateTime();
								$date2 = strtotime($datee->format('Y-m-d H:i:s'));
								
								// Formulate the Difference between two dates
								$diff = abs($date2 - $date1);
								
								// To get the year divide the resultant date into
								// total seconds in a year (365*60*60*24)
								$years = floor($diff / (365 * 60 * 60 * 24));
								
								// To get the month, subtract it with years and
								// divide the resultant date into
								// total seconds in a month (30*60*60*24)
								$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
								
								// To get the day, subtract it with years and
								// months and divide the resultant date into
								// total seconds in a days (60*60*24)
								$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
								
								// To get the hour, subtract it with years,
								// months & seconds and divide the resultant
								// date into total seconds in a hours (60*60)
								$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
								
								// To get the minutes, subtract it with years,
								// months, seconds and hours and divide the
								// resultant date into total seconds i.e. 60
								$minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
								
								// To get the minutes, subtract it with years,
								// months, seconds, hours and minutes
								$seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
								
								if ($years > 0) {
									$generated_on = $years . " years ago";
								} else if ($months > 0) {
									$generated_on = $months . " months ago";
								} else if ($days > 0) {
									$generated_on = $days . " days ago";
								} else if ($hours > 0) {
									$generated_on = $hours . " hours ago";
								} else if ($minutes > 0) {
									$generated_on = $minutes . " minutes ago";
								} else if ($seconds > 0) {
									$generated_on = $seconds . " seconds ago";
								} else {
									$generated_on = "just now";
								}
							?>
							 <div class="timeline-time">
								<!--span class="date"><?php echo $generated_on; ?></span-->
								<!--span class="time"><?php echo date('h:i A', strtotime($row['create_date']));?></span-->
								<span class="time"><?php echo $generated_on; ?></span>
							 </div>
							 <!-- end timeline-time -->
							 <!-- begin timeline-icon -->
							 <div class="timeline-icon">
								<a href="javascript:;">&nbsp;</a>
							 </div>
							 <!-- end timeline-icon -->
							 <!-- begin timeline-body -->
							 <div class="timeline-body">
								<div class="timeline-header">
								   <span class="userimage"><img src="<?= base_url() ?>images/clients/<?= $row['image']; ?>" alt=""></span>
								   <span class="username"><a href="javascript:;"><?= $row['title']; ?></a> <small style="float:right"> <?php echo date('F j, Y h:i A', strtotime($row['create_date']));?></small></span>
								   <span class="pull-right text-muted"></span>
								</div>
								<div class="timeline-content page-titles">
								   <p>
									  <?= $row['description']; ?>
								   </p>
								</div>
								<div class="timeline-likes">
								   <div class="stats" style="display: flex;">									
									<?php $j=1; foreach($row['files'] as $rowf): ?>										
										<a href="<?= base_url() ?>uploads/project_update_documents/<?= $rowf['file']; ?>" data-fancybox="mygallery" style="padding: 0px 5px;" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
											<?php $ext = pathinfo($rowf['file'], PATHINFO_EXTENSION);
												if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif'){
											?>
											<img src="<?= base_url() ?>uploads/project_update_documents/<?= $rowf['file']; ?>" style="width: 60px;height: 40px;" > 
											<?php } else {?>
											<img src="<?= base_url() ?>images/video_icon.jpg" style="width: 60px;height: 40px;" >	
											<?php }?>
										</a>
									<?php $j++; endforeach; ?>
									</div>
								</div>
								<?php if(count($row['comments'])>0){?>
								<h5>Comments: <span style="float:right;font-size: 12px;" class="stats-text"><?php echo count($row['comments']);?> Comments</span></h5>
								<div class="timeline-footer" style="max-height:280px;overflow-y:scroll;padding-top:0px;">
									<?php $j=1; foreach($row['comments'] as $rowc): ?>
										<div class="row page-titles content wmnt<?= $rowc['id']; ?> mx-0" style="display:block;border-bottom: 1px solid #d2cccc;">
											<h6><?= $rowc['name']; ?><small style="float:right"> <?php echo date('F j, Y h:i A', strtotime($rowc['created_date']));?></small></h6>	
											<p style="margin-bottom:5px;"><?= $rowc['comment']; ?></p>
											<div class="stats" style="display: flex;">									
											<?php $k=1; foreach($rowc['files'] as $rowf): ?>										
												<a href="<?= base_url() ?>uploads/project_update_documents/<?= $rowf['file']; ?>" data-fancybox="mygallery" style="padding-right: 5px;" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
													<?php $ext = pathinfo($rowf['file'], PATHINFO_EXTENSION);
														if($ext=='jpg' || $ext=='jpeg' || $ext=='png' || $ext=='gif'){
													?>
													<img src="<?= base_url() ?>uploads/project_update_documents/<?= $rowf['file']; ?>" style="width: 60px;height: 40px;" > 
													<?php } else {?>
													<img src="<?= base_url() ?>images/video_icon.jpg" style="width: 60px;height: 40px;" >	
													<?php }?>
												</a>
											<?php $k++; endforeach; ?>
											</div>
										</div>
										
									<?php $j++; endforeach; ?>
									
								</div>
								<?php }?>
								<button type="button"  id="uploadcomment" project_update_id="<?= $row['id']; ?>" project_id="<?php echo $project_id; ?>" style="float:right;margin-top: 5px;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Comment</button>
							 </div>
							 <!-- end timeline-body -->
						  </li>
						  <?php $i++; endforeach; ?>
					   </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addComment">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Comment</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form action="<?php echo base_url()?>admin/projects/add_comment"  method="POST" enctype="multipart/form-data" id="comment_form" class="form-horizontal">
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">	
								 <div class="input-group">
								 	<div class="form-group col-md-12">
										<input type="text" required class="form-control rounded-corner" id="comment" name="comment" placeholder="Write a comment...">
									 	<input type="hidden" value="" name="project_update_id" id="project_update_id">
										<input type="hidden" value="" name="sproject_id" id="sproject_id">
									</div>
									<div class="form-group col-md-12">
										<div class="dropzone cmnt" ></div>
									</div>
								 </div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button style="float:right" class="btn btn-outline-dark" type="submit">Comment</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Update Project Status</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form id="quickForm"  class="form-horizontal">
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="status_title" placeholder="Title" name="status_title">
									<input type="hidden" id="project_id"  value="<?php echo $project_id; ?>">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" id="status_description" placeholder="Description" name="status_description"></textarea>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="dropzone update" ></div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
$(document).ready(function(){
  $("#uploadcomment").click(function(){
      $('#project_update_id').val($(this).attr('project_update_id'));
      $('#sproject_id').val($(this).attr('project_id'));
    $('#addComment').modal('show');
  });
});
$(document).ready(function(){
	<?php $i=1; foreach($project_updates as $row): ?>
	$(".wmnt<?= $row['id']; ?>").slice(0,3).show();
	$("#sss<?= $row['id']; ?>").click(function(e){
    e.preventDefault();
    $(".wmnt<?= $row['id']; ?>:hidden").slice(0,3).fadeIn("slow");
    
    if($(".wmnt<?= $row['id']; ?>:hidden").length == 0){
       $("#sss<?= $row['id']; ?>").fadeOut("slow");
      }
  });
  <?php $i++; endforeach; ?>
})

Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('.update', {
  url: "../project_status_upload", 
  acceptedFiles: ".jpeg,.jpg,.png,.gif,video/*",  
  addRemoveLinks: true,                       
  autoProcessQueue: false,
  maxFilesize: 1000,
  timeout: 180000,
  init: function() {
	this.on("queuecomplete", function () {
		this.options.autoProcessQueue = false;
	});

	this.on("processing", function () {
		this.options.autoProcessQueue = true;
	});
	this.on('success', function(){
		if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
				//location.reload();
		}
	});
	}
});

var myDropzone2 = new Dropzone('.cmnt', {
  url: "../project_comment_status_upload",
  acceptedFiles: ".jpeg,.jpg,.png,.gif,video/*",  
  addRemoveLinks: true,                       
  autoProcessQueue: false,
  maxFilesize: 1000,
  timeout: 180000,
  init: function() {
	this.on('success', function(){
		if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
				//location.reload();
		}
	});
	}
});
$(document).ready(function () {
  	$('#quickForm').validate({	  
		rules: {			
		status_title: {
			required: true
		},
		status_description: {
			required: true
		},
		},
		messages: {
		status_title: {
			required: "Please enter Title"
		},
		status_description: {
			required: "Please enter Description"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			var project_id= $('#project_id').val();
			var status_title= $('#status_title').val();
			var status_description= $('#status_description').val();
			var submit= 'submit';
			
            $.ajax({
				url:"../add_project_update",
				method:"POST",
				async: true,
				dataType:"json",
				timeout: 180000,
				data:{submit:submit,project_id:project_id,status_title: status_title,status_description:status_description},
				success: function(data)
				{
				var out =JSON.parse(data);
				myDropzone.on("sending", function(file, xhr, data) {
					data.append("project_update_id", out);
				});
				myDropzone.processQueue(); 
				
				},
				error: function (data) {
				
				},
				complete: function (data) {
				    setTimeout(function() {
                        location.reload();
                    }, 2000);
					//window.location = "<?php  echo site_url('admin/projects'); ?>";
				}
			})
        }
	});

});
$(document).ready(function () {
  	$('#comment_form').validate({	  
		rules: {			
			comment: {
			required: true
		},
		},
		messages: {
			comment: {
			required: "Please enter Comment"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		},
		submitHandler: function (form) {
			var project_id= $('#sproject_id').val();
			var project_update_id= $('#project_update_id').val();
			var comment= $('#comment').val();
			var submit= 'submit';
			
            $.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				async: true,
				dataType:"json",
				timeout: 180000,
				success: function(data)
				{
				    
					var out =JSON.parse(data);
					myDropzone2.on("sending", function(file, xhr, data) {
						data.append("comment_id", out);
						//alert('dsgsdg');
					});
					myDropzone2.processQueue(); 
				
				},
				error: function (data) {
				
				},
				complete: function (data) {
					setTimeout(function() {
                        location.reload();
                    }, 2000);
					//window.location = "<?php  echo site_url('admin/projects'); ?>";
				}
			})
        }
	});

});
</script>