<style>
.btn-select-arrow {
    cursor: pointer;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/projects">All Projects</a></li>
							<li class="breadcrumb-item active">All projects</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a type="button" href="javascript:void(0)" class="form btn btn-outline-dark" id="submit_product"><!--i class="fa fa-ban"></i-->Submit</a>&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/projects" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Project Products</h4>
					</div>
					<div class="card-body custom_body">
						<input type="hidden" value="<?php echo $project_id; ?>" id="project_id">
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<div id="transfer4" class="transfer-demo"></div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/jquery.transfer.js?v=0.0.6"></script>

<script>

function get_products() {
	var project_id = $('#project_id').val();
	var groupDataArray2= null;
	$.ajax({
		url:"../view_project_products",
		method:"POST",
		async: false,
		dataType:"json",
		data: {project_id: project_id},
		success: function(data)
		{	
			groupDataArray2 = data;										
		}
	})
  return groupDataArray2;
}

function assign_products() {
  var groupDataArray2 = get_products();
  console.log(groupDataArray2);
  var settings4 = {
		"groupDataArray": groupDataArray2,
		"groupItemName": "category_name",
		"groupArrayName": "products",
		"itemName": "product_name",
		"valueName": "id",
		"callable": function (items) {
			//console.dir(items)
		}
	};
	var transfer = $("#transfer4").transfer(settings4);
	// get selected items
	var items = transfer.getSelectedItems();
	console.log("Manually get selected items: %o", items);
}

assign_products();

$('body').on('click', '#submit_product,', function() {
	var project_id = $('#project_id').val();
	var products = [];

	$('.transfer-double-selected-list-ul li .checkbox-normal').each(function(i, obj) {
		products.push($(this).val());
	});
	if(products.length>0){
		$.ajax({
			url:"../add_project_products",
			method:"POST",
			async: false,
			dataType:"json",
			data: {project_id: project_id,products:products},
			success: function(data)
			{	
				window.location = '<?php  echo site_url("admin/projects"); ?>';								
			}
		})
	}else{
		alert('Please select atleast one Product');
	}
});
</script>
