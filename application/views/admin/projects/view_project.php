<style>
/* PROJECTS */
.project-people,
.project-actions {
  text-align: right;
  vertical-align: middle;
}
dt {
    font-weight: 700;
    font-size: 14px;
}
dd {
    font-size: 13px;
}
dd.project-people {
  text-align: left;
  margin-top: 5px;
}
.project-people img {
  width: 32px;
  height: 32px;
}
.project-title a {
  font-size: 14px;
  color: #676a6c;
  font-weight: 600;
}
.project-list table tr td {
  border-top: none;
  border-bottom: 1px solid #e7eaec;
  padding: 15px 10px;
  vertical-align: middle;
}
.project-manager .tag-list li a {
  font-size: 10px;
  background-color: white;
  padding: 5px 12px;
  color: inherit;
  border-radius: 2px;
  border: 1px solid #e7eaec;
  margin-right: 5px;
  margin-top: 5px;
  display: block;
}
.project-files li a {
  font-size: 11px;
  color: #676a6c;
  margin-left: 10px;
  line-height: 22px;
}

/* PROFILE */
.profile-content {
  border-top: none !important;
}
.profile-stats {
  margin-right: 10px;
}
.profile-image {
  width: 120px;
  float: left;
}
.profile-image img {
  width: 96px;
  height: 96px;
}
.profile-info {
  margin-left: 120px;
}
.feed-activity-list .feed-element {
  border-bottom: 1px solid #e7eaec;
}
.feed-element:first-child {
  margin-top: 0;
}
.feed-element {
  padding-bottom: 15px;
}
.feed-element,
.feed-element .media {
  margin-top: 15px;
}
.feed-element,
.media-body {
  overflow: hidden;
}
.feed-element > .pull-left {
  margin-right: 10px;
}
.feed-element img.img-circle,
.dropdown-messages-box img.img-circle {
  width: 38px;
  height: 38px;
}
.feed-element .well {
  border: 1px solid #e7eaec;
  box-shadow: none;
  margin-top: 10px;
  margin-bottom: 5px;
  padding: 10px 20px;
  font-size: 11px;
  line-height: 16px;
}
.feed-element .actions {
  margin-top: 10px;
}
.feed-element .photos {
  margin: 10px 0;
}
.feed-photo {
  max-height: 180px;
  border-radius: 4px;
  overflow: hidden;
  margin-right: 10px;
  margin-bottom: 10px;
}
.file-list li {
  padding: 5px 10px;
  font-size: 11px;
  border-radius: 2px;
  border: 1px solid #e7eaec;
  margin-bottom: 5px;
}
.file-list li a {
  color: inherit;
}
.file-list li a:hover {
  color: #1ab394;
}
.user-friends img {
  width: 42px;
  height: 42px;
  margin-bottom: 5px;
  margin-right: 5px;
}

.ibox {
  clear: both;
  margin-bottom: 25px;
  margin-top: 0;
  padding: 0;
}
.ibox.collapsed .ibox-content {
  display: none;
}
.ibox.collapsed .fa.fa-chevron-up:before {
  content: "\f078";
}
.ibox.collapsed .fa.fa-chevron-down:before {
  content: "\f077";
}
.ibox:after,
.ibox:before {
  display: table;
}
#desc{
    display: -webkit-box;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    overflow: hidden;

}
.ibox-title {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #ffffff;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 3px 0 0;
  color: inherit;
  margin-bottom: 0;
  padding: 14px 15px 7px;
  min-height: 48px;
}
.ibox-content {
  background-color: #ffffff;
  color: inherit;
  border-color: #e7eaec;
  border-image: none;
  border-width: 1px 0;
}
.ibox-footer {
  color: inherit;
  border-top: 1px solid #e7eaec;
  font-size: 90%;
  background: #ffffff;
  padding: 10px 15px;
}
ul.notes li,
ul.tag-list li {
  list-style: none;
}
.wert{
	box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
    margin-bottom: 10px;
}
.new-arrival-content p {
    font-size: 14px;
    color: #828690;
    margin-bottom: 6px;
    line-height: 15px;
}
.new-arrival-content .price {
    margin-bottom: 10px;
}
.card{
	height: calc(100% - 30px);
}
.qwas{
	display: flex;
}
.qwas p{
	padding-right: 10px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">All projects</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL projects</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h4 style="text-transform: capitalize;"><?php echo $project['project_name']?> Project Details</h4>
								</div>
							</div>
						</div>
						<hr>						
						<div class="row">
							<div class="col-md-12">
								<div class="wrapper wrapper-content animated fadeInUp">
									<div class="ibox">
										<div class="ibox-content">
											
											<div class="row">
												<div class="col-lg-4">
													<dl class="dl-horizontal">
														<dt>Project Start Date:</dt> <dd> <?php echo  date('F j, Y', strtotime($project['project_start_date']));?></dd>
														<dt>Project End Date:</dt> <dd> <?php echo  date('F j, Y', strtotime($project['project_end_date']));?></dd>
													</dl>
													<dl class="dl-horizontal">
														<dt>Created By:</dt><dd> S&A Developers</dd>
														<dt>Created on:</dt> <dd><?php echo  date('F j, Y', strtotime($project['created_at']));?></dd>
														
													</dl>
												</div>
												<div class="col-lg-4">
													<dl class="dl-horizontal">
														<dt>Project Address:</dt> 
														<dd><?php echo $project['address1']?></dd>
														<dd><?php echo $project['address2']?></dd>
														<dd><?php echo $project['city']?>, <?php echo $project['state']?> <?php echo $project['zip_code']?></dd>
														<dt>Project Status:</dt><dd> In Progress</dd>
														<dt>Client Name:</dt> <dd><?php echo $project['lastname']?> <?php echo $project['firstname']?></dd>
													</dl>
												</div>
												<div class="col-lg-4">
													<dl class="dl-horizontal">
													    <dt>Project Documents:</dt>
														<ul class="list-unstyled project-files">
                    										<?php $i=1; foreach($project['files'] as $row): ?>
                    											<li><a href="<?= base_url() ?>uploads/project_documents/<?= $row['file']; ?>" target="_blank"><i class="fa fa-file"></i>  <?= $row['file']; ?></a></li>
                    										<?php $i++; endforeach; ?>
                    									</ul>
													</dl>
												</div>
											</div>
											<!--div class="row">
												<div class="col-lg-12">
													<dl class="dl-horizontal">
														<dt>Completed:</dt>
														<dd>
															<div class="progress progress-striped active m-b-sm">
																<div style="width: 60%;" class="progress-bar"></div>

															</div>
															<small>Project completed in <strong>60%</strong>. Remaining close the project, sign a contract and invoice.</small>
														</dd>
													</dl>
												</div>
											</div-->
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="wrapper wrapper-content project-manager">
									<h4>Project description:</h4>
									<span id="">
									<?php echo $project['project_description']?>
									</span>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<div class="m-b-md">
							<h4 style="text-transform: capitalize;"><?php echo $project['project_name']?> Project Products</h4>
						</div>
					</div>
					<div class="card-body custom_body">
						<div class="row">
							<?php $i=1; foreach($project['products'] as $row): ?>
							<div class="col-lg-6">
								<div class="card">
									<div class="card-body wert">
										<div class="row m-b-30">
											<div class="col-md-3">
												<div class="new-arrival-product">
													<div class="new-arrivals-img-contnent">
														<img class="img-fluid" style="min-height:150px" src="<?= base_url() ?>images/products/<?= $row['image']; ?>" alt="">
													</div>
												</div>
											</div>
											<div class="col-md-9">
												<div class="new-arrival-content position-relative">
													<h4><a href="<?php echo base_url()?>admin/products/view_product/<?php echo $row['id'];?>"><?= $row['product_name']; ?></a></h4>
													<p class="price">$<?= $row['price']; ?></p>
													<div class="qwas">
														<p>Category: <span class="item"> <?= $row['category_name']; ?> <i class="fa fa-check-circle text-success"></i></span></p>
														<p>Brand: <span class="item"><?= $row['brand']; ?></span></p>
														<p>Tags: <span class="item"><?= $row['tags']; ?></span></p>
													</div>
													<span class="text-content" id="desc"><?= $row['product_description']; ?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php $i++; endforeach; ?>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
