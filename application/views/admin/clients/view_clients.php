
<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',8);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">All clients</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/clients/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add client</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL clients</h4>
					</div-->
					<div class="card-body custom_body">	
						<div class="table-responsive">
							<table id="example3" style="width:100%">
								<thead>
									<tr>
										<th>client ID</th>
										<th>Client Name</th>
										<th>Location</th>
										<th>Email Address</th>
										<th>Phone Number</th>										
										<th>Status</th>
										<th>Projects</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody id="filter_data">
								
								</tbody>
							</table>						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var client_id = $(this).attr('client_sid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/clients/update_status');?>",
			  data: {id: id, client_id: client_id},
			  cache: false,
			  success: function(data){
				  filter_data();
				  //$("#status_roww"+client_id).load(" #status_roww"+client_id);
			  }
			});
		});
	});
	function filter_data()
    {
		$.ajax({
            url:"clients/filter_clients",
            method:"POST",
            async: true,
            dataType:"json",
            success: function(data)
            {	
				$('table').DataTable().clear();
				$('table').DataTable().destroy();			
				$('#filter_data').html(data.clients_list);				
				tableinit();										
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
	}
	function tableinit(){
		var tt= $('table').DataTable({
			"bRetrieve": true,
			"bProcessing": true,
			"bDestroy": false,
			"bSort" : true,
			"bPaginate": true,
			"bAutoWidth": true,
			"bFilter": true,
			"bInfo": true,
			"bJQueryUI": true,
			columnDefs: [
				{ orderable: false, targets: -1 }
			],
			language: {
			"emptyTable": "No clients were found."
			}
		});
		$('table').find('thead tr th').css('width', 'auto');
	}
</script>
<script>
 $(".removeRecord").click(function(){
       var id = $(this).attr('client_id');    	
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: '<i class="fa fa-check" aria-hidden="true"></i>Confirm',
            btnClass: 'form btn btn-red',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/clients/del');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: '<i class="fa fa-ban"></i>cancel',
            btnClass: 'form btn btn-grey',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
	});
	
</script>

