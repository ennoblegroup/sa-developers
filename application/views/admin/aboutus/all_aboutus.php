<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',14);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',14);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">About Us</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">About Us</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>
										<th> ID</th>
										<th>Title</th>
										<th>Date</th>
										<th class="<?php echo $user_classs;?>">Status</th>												
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_aboutus as $key=>$srv){ 
										
									?>
									<tr>
									<!--td style="display:none"><?php echo $i; ?></td-->
										<td> <?php echo $srv['id']?> </td>
										
										<td> <?php echo $srv['title']?> </td>
										<td> <?php echo date("F j, Y ", strtotime($srv['create_date'])); ?> </td>
										
										<td class="<?php echo $user_classs;?>"> 
											<label class="switch">
											  <input type="checkbox" <?php if($srv['status']==1){echo 'checked disabled';}?> id="status" aboutus_aid="<?php echo $srv['id']; ?>">
											  <span class="slider round" <?php if($srv['status']==1){ ?> title="Restricted" <?php } ?> ></span>
											</label>
											<span id="status_roww<?php echo $srv['id']; ?>">
											<?php if($srv['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>

											<?php }?>
											</span>
										</td>
										<td> 
											<a href="<?= base_url('admin/aboutus/edit/'.$srv['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark" <?php echo $user_classu;?> ><i class="fa fa-pencil"></i></button> </a>
											<a href="<?= base_url('admin/aboutus/view_about/'.$srv['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark"  ><i class="fa fa-eye"></i></button> </a>
											<?php if($srv['status']==0){?>
											<a href="#" class="removeRecord" aboutus_id = "<?php echo $srv['id'];?>"><button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></button></a> </td>
											<?php }?>	
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var aboutus_id = $(this).attr('aboutus_aid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/aboutus/update_status');?>",
			  data: {id: id, aboutus_id: aboutus_id},
			  cache: false,
			  success: function(data){
				location.reload(true);
			  }
			});
		});
	});
</script>

<script>
$('body').on('click', '.removeRecord', function() {
	var aboutus_id = $(this).attr('aboutus_id');
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/aboutus/del');?>",
						data:{'id':aboutus_id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
$(document).ready(function() {
    $('#example3').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );
</script>