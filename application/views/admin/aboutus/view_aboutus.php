<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/aboutus">About us</a></li>
                        <li class="breadcrumb-item active">View About Us</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">About Us</h4>
					</div>
					<div class="card-body">
                    <?php $i=1; foreach ($about_view as $key=>$add){ ?>
                        <div class="basic-list-group">
                            <ul class="list-group">
                                <li class="list-group-item"><?php echo $add['title']?></li>
                                <li class="list-group-item"><?php echo $add['description']?></li>
                            </ul>
                        </div>
                    <?php $i++; }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var aboutus_id = $(this).attr('aboutus_aid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/aboutus/update_status');?>",
			  data: {id: id, aboutus_id: aboutus_id},
			  cache: false,
			  success: function(data){
				  loction.reload();
			  }
			});
		});
	});
</script>
<script type="text/javascript">
    $(".removeRecord").click(function(){
       var aboutus_id = $(this).attr('aboutus_id');
       swal({
        title: "Are you sure?",
        text: "Do you really want to delete the About Us? This process cannot be undone.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-outline-dark",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!"
     }). then((result) => {
        if (result.value) {
         $.ajax({
			 url:"<?php echo base_url('admin/aboutus/del');?>",
			type:'POST',			
			data:{'id':aboutus_id},
			success: function(data){
				//swal("Deleted!", "Service has been deleted.", "success");
				location.reload();
				}
			});
        } else {
          swal("Cancelled", "About Us is safe :)", "error");
        }
      })
     
    });
</script>
<script>
/*	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});*/
</script>
<script>
$("#nav_aboutus").addClass('active');
</script> 