<style>
	input[type="checkbox"] {
		margin-right: 10px;
	}
</style>
<?php echo form_open(base_url('admin/aboutus/edit'), 'id="quickForm" enctype="multipart/form-data" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/aboutus">About Us</a></li>
						<li class="breadcrumb-item active">Edit About Us</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/aboutus" class="form btn btn-outline-dark">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Aboutus Details</h4>
					</div>
					<div class="card-body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; 
									$i=1; foreach ($aboutus as $key=>$srv){ ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="title" placeholder="Title" value="<?php echo $srv['title']?>" name="title" >
										</div>
										<div class="form-group col-lg-12 col-md-12">
										  	<textarea class="editor" name="description" id="description" required>
												<?php  echo $srv['description']; ?>
											</textarea>
										</div>
										<div class="form-group col-lg-12 col-md-12">
										  	<input type="checkbox" class="" <?php if( $srv['is_team']==1){echo 'checked';}?> name="is_team">Show Team in About us page
										</div>
										<div class="col-lg-4 col-md-4"></div>
										<div class="col-lg-4 col-md-4">
											<div class="row">
												<div class="custom-file">
													<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/png,image/gif,image/jpeg">
													<label class="custom-file-label">Choose file</label>
												</div>
											</div>
											<div class="row">
												<img id="blah" style="padding-top:15px;width:100%" src="<?php echo base_url();?>images/about/thumb/<?php echo $srv['image']; ?>" alt="your image" />  
											</div>
                                     	</div>										
									</div>
									<input type="hidden" value="<?php echo $srv['id']; ?>" name="id"> 
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
<?php echo form_close( ); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}


$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
		title: {
        required: true,
        minlength: 3
      },
	 
      description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
	  title: {
        required: "Please enter  Title",
        minlength: "Title must be at least 3 characters long"
      },
      description: {
        required: "Please enter  Description",
        minlength: "Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.err').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });	
});
</script>