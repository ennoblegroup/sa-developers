	<?php echo form_open(base_url('admin/services/add'), 'id="quickForm" class="form-horizontal"');  ?> 
	<div class="content-body">
	<div class="container-fluid">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/services">Services</a></li>
					<li class="breadcrumb-item active">Add Service</li>
				</ol>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
				<a type="button" href="<?php echo base_url()?>admin/services" class="form btn btn-outline-dark"><i class="fa fa-ban"></i>Cancel</a>
			</div>
		</div>
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Service Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="service_name" placeholder="Service Name" name="service_name" maxLength="25">
										</div>
										</div>
									<div class="row">	
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" id="service_description" rows="5" style="height:auto !important;" placeholder="Service Description" name="service_description" ></textarea>
										</div>										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
	<?php echo form_close( ); ?>
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(".us_zip").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValuezip(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $(this).val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $(this).val(inputValue); //correct value entered to your input.
       inputValue = getInputValuezip();//get value again, becuase it changed, this one using for changing color of input border
 });

function getInputValuezip() {
	var inputValue = $(this).val().replace(/\D/g,'');  //remove all non numeric character
	return inputValue;
}
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      service_name: {
        required: true,
        minlength: 3
      },
      service_description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
      service_name: {
        required: "Please enter Service Name",
        minlength: "Service Name must be at least 3 characters long"
      },
      service_description: {
        required: "Please enter Service Description",
        minlength: "Service Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
   $('body').on('input', '#service_name', function() {
		var txt = '';
		txt = $(this).val();
		console.log(txt);
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	});	
});
</script>
<script>
$("#nav_services").addClass('active');
</script> 