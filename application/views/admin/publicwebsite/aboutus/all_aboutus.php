<div class="content-body">
	<div class="container-fluid">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0 ">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active">About Us</li>
				</ol>				
				</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/publicwebsite/aboutus/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Service</button></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">About Us</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>
										<th>About Us ID</th>
										<th>About Us Name</th>
										<th>About Us Description</th>
										<th>Status</th>												
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_aboutus as $key=>$abo){ 
										
									?>
									<tr>
										<td> <?php echo $abo['id']?> </td>
										
										<td> <?php echo $abo['title']?> </td>
										<td> <?php echo $abo['description']?></td>
										<td> 
											<label class="switch">
											  <input type="checkbox" <?php if($abo['status']==1){echo 'checked';}?> id="status" aboutus_aid="<?php echo $abo['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $abo['id']; ?>">
											<?php if($abo['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>

											<?php }?>
											</span>
										</td>
										<td> <a href="<?= base_url('admin/publicwebsite/aboutus/edit/'.$abo['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
										<a href="#" class="removeRecord" aboutus_id = "<?php echo $abo['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></span></a> </td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var aboutus_id = $(this).attr('aboutus_aid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/website/aboutus/update_status');?>",
			  data: {id: id, aboutus_id: aboutus_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+aboutus_id).load(" #status_roww"+aboutus_id);
			  }
			});
		});
	});
</script>
<script type="text/javascript">
    $(".removeRecord").click(function(){
       var id = $(this).attr('aboutus_id');
       swal({
        title: "Are you sure?",
        text: "Do you really want to delete the About Us? This process cannot be undone.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-outline-dark",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!"
     }). then((result) => {
        if (result.value) {
         $.ajax({
			 url:"<?php echo base_url('admin/website/aboutus/del');?>",
			type:'POST',			
			data:{'id':id},
			success: function(data){
				//swal("Deleted!", "Service has been deleted.", "success");
				location.reload();
				}
			});
        } else {
          swal("Cancelled", "Service is safe :)", "error");
        }
      })
     
    });
</script>
<script>
/*	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});*/
</script>
<script>
$("#nav_services").addClass('active');
</script> 