<style>
.custom_body
{
	min-height: 279.9px !important;
	max-height: 279.5px !important;
}
</style>
<?php echo form_open(base_url('admin/roles/map'), 'id="quickForm" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/roles">Roles</a></li>
							<li class="breadcrumb-item active">Map Menus to Permissions</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/roles" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>		
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
    <!-- Main content -->
	     <div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Map Menus to Permissions</h4>
					</div-->
					<section class="content">
					<div class="container-fluid">				
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<div class="card card-primary card-outline custom_body">
								<!-- /.card-header -->
								<div class="card-body">
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<!--th>Menu Id</th-->
												<th></th>
												<th>Menu Section</th>
											</tr>
										</thead>
										<tbody>
											<?php $roleid = $this->uri->segment('4');
											foreach ($all_menues as $key=>$menu){
											?>
											<tr>
												<td><input type="radio" name="menu_id" role_id="<?php echo $roleid; ?>" value="<?php echo $menu['id']?>" required></td>
												<td> <?php echo $menu['name']?> 
												<input type="hidden" id="role_id" name="role_id" value="<?php echo $roleid; ?>">
												</td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /.card -->
						</div>
						<div class="col-md-3">
							<div class="card card-primary card-outline">
								<!-- /.card-header -->
								<div class="card-body custom_body">
									<table id="example1" class="table table-bordered table-striped dataTable">
										<thead>
											<tr>
												<th>Permission</th>
										</thead>
										<tbody>
											<?php foreach($all_permissions as $permission) { ?>
											<tr>
												<td><input type="checkbox" name="permission_id[]" id="permission_id[]" class="checkboxes" value="<?php echo $permission['id']?>"> <?php echo $permission['name']?> </td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
								<!-- /.card-body -->
							</div>
							<!-- /. box -->
						</div>
					</div>
			</div>
	    </section>
		</div>
		<!-- /.card-body -->
		</div>
		<!-- /. box -->
	</div>
</div>
</div>
		<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		$("input[type='radio']").click(function(){
			
			$("input[type='radio']").each(function(i, obj) {
				if($(obj).prop("checked")){
					$(obj).parent().parent().css("background","thistle");
				}
				else{
					$(obj).parent().parent().css("background","white");
				}
				
			});
			
			
			$("input[type='checkbox']").each(function(i, obj) {
				$(obj).prop("checked",false);
				$(obj).parent().parent().css("background","white");
			});
			
			console.log($(this).val());
			var menu_id = $(this).val();
			//var role_id = document.getElementById('role_id').value;
			
			var url = "<?php echo base_url()?>admin/roles/get_menu_mapped_permissions";
			 $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                //data: { 'menu_id' : menu_id,'role_id' : role_id },
				data: { 'menu_id' : menu_id},
                success: function( data, textStatus, jQxhr ){
					console.log(data);
					if(data.length){
						
						$("input[type='checkbox']").each(function(i, obj) {
							for(var i=0;i<data.length;i++){
								if(data[i].id==$(obj).val()){
									
									$(obj).prop("checked",true);
									$(obj).parent().parent().css("background","thistle");
								}
								
							}						
						});
					}   
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
			
		});
		
		
		
		$("input[type='checkbox']").click(function(){
			
			$("input[type='checkbox']").each(function(i, obj) {
				if($(obj).prop("checked")){
					$(obj).parent().parent().css("background","thistle");
				}
				else{
					$(obj).parent().parent().css("background","white");
				}
				
			});
		});
	})
</script>
<script>
	$(document).ready(function(){
		
		var check = $("input[type='checkbox']");
		var submitButt = $("button[type='submit']");

		submitButt.click(function(e) {
			var count = 0;
			
			for(var i=0;i<check.length;i++){
				console.log(check[i].checked);
				if(check[i].checked == true){
					count = count+1;
				}
			}
			if(count==0){
				alert('Please check atleast one permission');
				e.preventDefault();
			}
			
		});
	});
</script>