<?php echo form_open(base_url('admin/roles/edit/'.$role['id']), 'id="quickForm" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/roles">Roles</a></li>
							<li class="breadcrumb-item active">Edit Role</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/roles" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Role Details</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" name="rolename" class="form-control" id="rolename" placeholder="*Role Name" title="*Role Name" value="<?= $role['rolename']; ?>" maxLength="25">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" name="description" class="form-control" id="description" placeholder="Description" title="Description" value="<?= $role['description']; ?>" maxLength="25">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select name="role_status" id="role_status" class="form-control selectpicker" title="*Select Role Status" data-live-search="true">
												<option value="">Select Role</option>	
												<option value="1" <?= ($role['status'] == 1)?'selected': '' ?>>Active</option>
												<option value="0" <?= ($role['status'] == 0)?'selected': '' ?>>Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {	
$('#quickForm').validate({
    rules: {
      rolename: {
        required: true,
        minlength: 3
      }, 
	  role_status: {
        required: true
      },
    },
    messages: {
      rolename: {
        required: "Please enter Role Name",
        minlength: "Role Name must be at least 3 characters long"
      },
      role_status: {
        required: "Please select Role Status"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>