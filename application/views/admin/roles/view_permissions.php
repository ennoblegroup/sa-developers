<style>
.card {
    height: calc(100% + -29px);
}
input[type="checkbox"]:checked:after {
    width: 1rem;
    height: 1rem;
    display: block;
    content: "\f00c";
    font-family: 'FontAwesome';
    color: #fff;
    font-weight: 100;
    font-size: 12px;
    text-align: center;
    border-radius: 3px;
    background: none;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/roles">Roles</a></li>
							<li class="breadcrumb-item active">Role Permissions</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>	
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a type="button" href="<?php echo base_url()?>admin/roles" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>	
		</div>
	</div>
</div>
<div class="content-body" style="min-height: 693px !important;">
	<div class="container-fluid">
    <!-- Main content -->
	<?php $roleid = $this->uri->segment('4');?>
   <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Grant/Restrict permissions</h4>
				</div> 
				<div class="card-body custom_body">
					<?php foreach($all_menues as $row){ if($row['status'] ==1){
					$menu_permissions =  $this->role_model->get_menu_permissions($row['id']);
					if($menu_permissions)
					{
					?>
					<div class="">
						<?php
						$this->db->where('role_id',$roleid);
						$this->db->where('menu_id',$row['id']);
						$this->db->where('permission_id',0);
						$parent_count = $this->db->get('roles_permissions')->num_rows();
						if($parent_count==0){
							$parent_class ='hide_cls';
						}else{
							$parent_class ='show_cls';
						}
						?>
						<div class="row rowcls level1_clr" style="margin-right: 0px !important;margin-left: 0px;">		
							<div class="col-md-6 l2_style">
									<?= $row['name']; ?>
							</div>
							<div class="col-md-6" style="padding-right:20px;">
								<div class="row">
									<div class="text-right" style="">
									<p></p>
									<label class="switch" style="right: -500px;margin-top: -8px;"><input  class="slider round permission_status" <?php if($parent_count ==1){ echo 'checked';}?> type="checkbox" role_id="<?php echo $roleid; ?>" menu_id="<?= $row['id']; ?>" permission_id="0"><span class="slider round"></span></label></a>
									</div>
								</div>
							</div>
						</div>					
						<div class="row rowcls level2_clr <?php echo $parent_class;?>">
							<?php 
							
							foreach($menu_permissions as $rowa): 
							$this->db->where('role_id',$roleid);
							$this->db->where('menu_id',$row['id']);
							$this->db->where('permission_id',$rowa['id']);
							$child_count = $this->db->get('roles_permissions')->num_rows();
							if($child_count==0){
								$child_class ='hide_cls';
							}else{
								$child_class ='show_cls';
							}
							?>
							<div class="col-md-2">
								<div class="row">
									<div class="text-right" style="width:96%">									
									<span style="font-weight:bold"><?= $rowa['name']; ?></span>
									<label class="switch1"><input  class="slider round permission_status" <?php if($child_count ==1){ echo 'checked';}?> type="checkbox" role_id="<?php echo $roleid; ?>" menu_id="<?= $row['id']; ?>" permission_id="<?= $rowa['id']; ?>"><span class="slider round"></span></label></a>
									</div>
								</div>
							</div>	
							<?php endforeach; ?>
						</div>
					</div>
					<?php }}} ?>				
				</div>
			</div>
		</div>
    </div>
    <!-- /.box-body -->
  </div>
  </div>
  <!-- /.box -->

<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
//change status
	$(document).ready(function() {
		
		$('body').on('click', '.permission_status', function () {
			
            if($(this).prop("checked") == true){
                var change_status = 1;
            }
            else {
                var change_status = 0;
            }
			var role_id = $(this).attr('role_id');
			var menu_id = $(this).attr('menu_id');
			var permission_id = $(this).attr('permission_id');			
			$.ajax({
			  type: "POST",
			  url: "<?php echo base_url('admin/roles/edit_role_permissions'); ?>",
			  data: {change_status: change_status, role_id: role_id, menu_id: menu_id, permission_id: permission_id},
			  cache: false,
			  success: function(data){
				 //alert(data);
				 //location.reload();
				 console.log(data);
			  }
			});
		});
	});
	
	/*function changeuser(role_id)
	{
		window.location.href = "<?php echo base_url(); ?>admin/user_permission/"+role_id;
	}*/
</script>