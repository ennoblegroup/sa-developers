<style>
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: -webkit-fill-available;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 5px 5px;
    border-bottom: 1px solid #111;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 5px !important;
}
.bootstrap-select .btn {
    border: 1px solid #eaeaea !important;
    background-color: #f8f9fa !important;
    padding: 5px 5px;
}
</style>
<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}

$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu>0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd>0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',9);
$status_countp = $this->db->get('roles_permissions')->num_rows();
if($status_countp>0 || $roleid==0){
	$user_classp ='show_cls';
}else{
	$user_classp ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',10);
$status_countmp = $this->db->get('roles_permissions')->num_rows();
if($status_countmp>0 || $roleid==0){
	$user_classmp ='show_cls';
}else{
	$user_classmp ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',2);
$this->db->where('permission_id',6);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classs ='show_cls';
}else{
	$user_classs ='hide_cls';
}
?>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">System Roles</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
				<a href="<?= base_url('admin/roles/map'); ?>"><button type="button"  class="btn btn-outline-dark <?php echo $user_classmp;?>"><!--i class="fa fa-link" aria-hidden="true"></i-->&nbsp;Map Permissions</button>&nbsp;</a>
					<button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Role</button>
				
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
			
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Role Details</h4>
					</div-->
					<div class="card-body custom_body">
						<!--div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
										<div class="alert alert-danger alert-dismissible">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<?= validation_errors();?>
											<?= isset($msg)? $msg: ''; ?>
										</div>
									<?php endif; ?>
									<div class="row">																
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" name="rolename" class="form-control" id="rolename" placeholder="*Role Name" maxLength="25" onfocus="myFunction()">
											<div id="rname_response" ></div>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" name="description" class="form-control" id="description" placeholder="Description" maxLength="25">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select name="role_status" id="role_status" title="*Select Role Status" class="form-control selectpicker" data-live-search="true">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div-->
						<div class="table-responsive recentOrderTable">
							<table id="example3" class="ui celled table" >
								<thead>
									<tr>
										<th scope="col" width="5%">ID</th>
										<th scope="col" width="50%">Role Name</th>
										<th class="<?php echo $user_classs;?>" scope="col" width="25%">Status</th>
										<th scope="col" width="20%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_roles as $row): ?>
									<tr>
										<td><?= $i; ?></td>
										<td><?= $row['rolename']; ?></td>
										<td class="<?php echo $user_classs;?>"> 
											<label class="switch">
											  <input type="checkbox" <?php if($row['status']==1){echo 'checked';}?> id="status" role_sid="<?php echo $row['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $row['id']; ?>">
											<?php if($row['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span>In-Active</span>
											<?php }?>
											</span>
										</td>
										<td> 
											<!--div class="btn-group btn-group-sm"-->
												<a data-toggle="modal" data-target="#exampleModalLong-<?php echo $row['id'];?>" title="Edit" href="<?= base_url('admin/roles/edit/'.$row['id']); ?>" class="btn btn-sm btn-outline-dark <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></a>
												<a data-toggle="tooltip" title="Delete" href="#" id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark"><i class="fa fa-trash-o <?php echo $user_classd;?>"></i></a>
												<a data-toggle="tooltip" title="Permissions" href="<?= base_url('admin/roles/permissions/'.$row['id']); ?>" class="btn btn-sm btn-outline-dark <?php echo $user_classp;?>"><i class="fa fa-plus"><span style="font-size: 12px;">&nbsp;Permissions</span></i></a>
												<!--a data-toggle="tooltip" title="Permissions" href="<?= base_url('admin/roles/map/'.$row['id']); ?>" class="btn btn-outline-dark"><i class="fa fa-link"></i></a-->
												<!--a href="<?= base_url('admin/roles/edit/'.$row['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
												<a data-toggle="tooltip" title="Delete" href="#" class="removeRecord" id = "<?php echo $row['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash"></i></span></a> 
												<a href="<?= base_url('admin/roles/permissions/'.$row['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-plus"></i></span> </a-->
											<!--/div-->
										</td>
									</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Role</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/roles/add'), 'id="quickForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-lg-12 col-md-12">
						<input type="text" name="rolename" class="form-control" id="rolename" placeholder="*Role Name" maxLength="25">
					</div>
					<div class="form-group col-lg-12 col-md-12">
						<!--input type="text" name="description" class="form-control" id="description" placeholder="Description" maxLength="25"-->
						<textarea class="form-control" id="description" name="description" rows="4" placeholder="Description" maxLength="150"></textarea>
					</div>
					<div class="form-group col-lg-12 col-md-12">
						<select name="role_status" id="role_status" title="*Select Role Status" class="form-control selectpicker" data-live-search="true">
							<option value="" selected="selected">Select Role Status</option>
							<option value="1">Active</option>
							<option value="0">Inactive</option>
						</select>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" name="submit" value="Reset" class="btn btn-outline-dark" onClick="resetValues()">Reset</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
				<button type="button" class="btn btn-outline-dark" data-index="-1" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<?php 
if(count($all_roles)>0)
{	 
foreach($all_roles as $row): ?>
<div class="modal fade" id="exampleModalLong-<?php echo $row['id'];?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Role</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/roles/edit/'.$row['id']), 'id="editForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-lg-12 col-md-12">
						<input type="text" name="rolename" class="form-control" id="rrolename" placeholder="*Role Name" maxLength="25"  value="<?= $row['rolename']; ?>">						
						<input type="hidden" class="form-control" id="rol_id" value="<?= $row['id']; ?>" placeholder=""  name="id">
					</div>
					<div class="form-group col-lg-12 col-md-12">
						<input type="text" name="description" class="form-control" id="description" placeholder="Description" maxLength="25"  value="<?= $row['description']; ?>">
					</div>
					<div class="form-group col-lg-12 col-md-12">
						<select name="role_status" id="role_status" title="*Select Role Status" class="form-control selectpicker" data-live-search="true">
							<option value="1" <?= ($row['status'] == 1)?'selected': '' ?>>Active</option>
							<option value="0" <?= ($row['status'] == 0)?'selected': '' ?>>Inactive</option>
						</select>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Update</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<?php
endforeach;
}
?>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script type="text/javascript">
$(document).ready(function () {	
$('#quickForm').validate({
    rules: {
      rolename: {
        required: true,
        minlength: 3,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/roles/fetch_rname",
			type: "post",
			data: {
			role_name: function() {
				return $( "#rolename" ).val();
			}
			}
		}
      }, 
	  role_status: {
        required: true
      },
    },
    messages: {
      rolename: {
        required: "Please enter Role Name",
        minlength: "Role Name must be at least 3 characters long",
        remote: "Role Name already exists"
      },
      role_status: {
        required: "Please select Role Status"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('#editForm').validate({
    rules: {
      rolename: {
        required: true,
        minlength: 3,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/roles/fetch_rname_edit",
			type: "post",
			data: {
			role_name: function() {
				return $( "#rrolename" ).val();
			},
			rol_id: function() {
				return $( "#rol_id" ).val();
			}
			}
		}
      }, 
	  role_status: {
        required: true
      },
    },
    messages: {
      rolename: {
        required: "Please enter Role Name",
        minlength: "Role Name must be at least 3 characters long",
        remote: "Role Name already exists"
      },
      role_status: {
        required: "Please select Role Status"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
/*$('body').on('click', '#quickForm', function() {	  
        var rname = document.getElementById('rolename').value;
        if(rname){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/roles/fetch_rname'); ?>',
                data:'role_name='+rname,
                success:function(response){	
					$('#rname_response').html(response); 
					
                }
            }); 
        }
		else{
         $("#rname_response").html("");
		}		
    }); 
	$('body').on('input', '#description', function() {	  
        var rname = document.getElementById('rolename').value;
        if(rname){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('admin/roles/fetch_rname'); ?>',
                data:'role_name='+rname,
                success:function(response){	
					$('#rname_response').html(response); 
					
                }
            }); 
        }
		else{
         $("#rname_response").html("");
		}		
    }); */	
});
</script>
<script>
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/roles/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
function resetValues()
{	
	document.getElementById("role_status").value="";
	//alert(document.getElementById("role_status1").value);
	document.getElementById("quickForm").reset();
}
</script>
<script>
	$(document).ready(function() {		
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;                
            }
            else {
                var id = 0;
            }
			var role_id = $(this).attr('role_sid');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/roles/update_status');?>",
			  data: {id: id, role_id: role_id},
			  cache: false,
			  success: function(data){
				$("#status_roww"+role_id).load(" #status_roww"+role_id);
			  }
			});
		});
	});
	
</script>
<script>
$("#nav_roles").addClass('active');
</script> 