	<?php echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
	 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-4 p-md-0 ">
						<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
							<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
									<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/communications">Communications</a></li>
									<li class="breadcrumb-item active">View Message</li>
								</ol>
							<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
						</span>
                    </div>
                    <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<div class="welcome-text">
							<!--a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><i class="fa fa-arrow-left"></i>Back</a-->
							<input type="submit" name="submit" value="Reply" class="btn btn-outline-dark">
						</div>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body">
								<?php $i=1; foreach ($view_message as $key=>$trans){ 
								$fcommunication_id=$trans['communication_id'];
								?>    
								  <!--div class="card card-primary card-outline">
									<div class="card-header">
									  <h3 class="card-title"><?php echo $trans['mail_subject']?></h3>
									  <div class="card-tools">
									  </div>
									</div>
									<div class="box-body card-body p-0">
									  <div class="mailbox-read-info">
										<h6>From: <?php echo $trans['mail_from']?>
										  <span class="mailbox-read-time float-right"><?php echo date('m-d-Y H:i:s',strtotime($trans['created_date'])); ?></span></h6>
									  </div>
									  <div class="mailbox-read-message">
										  <?php echo $trans['mail_message']?>
									  </div>
									</div> 
								  </div-->
								  
								  <div class="col-xl-4 col-xxl-12 col-lg-12 col-sm-12">
								  <h5 class="card-title text-black">Subject:<?php echo $trans['mail_subject']?></h5>
									<div class="card text-black bg-light" style="border: 1px solid black;">
										<div class="card-header">
											<p class="card-text" style="margin-left: -410px;"> <?php echo $trans['mail_message']?></p>
											<div class="card-footer bg-transparent border-0 text-black"><?php 
											//echo date('m-d-Y H:i:s',strtotime($trans['created_date'])); 
											echo date('F j, Y, g:i a',strtotime($trans['created_date']));
											?>
											</div>
										</div>
										<div class="card-body mb-0">
											
										</div>
										
									</div>
								</div>
								<?php } $i=1; foreach ($reply_message as $key=>$trans){
								?>
									<div class="col-xl-4 col-xxl-12 col-lg-12 col-sm-12">
									<h5 class="card-title text-black">From: <?php echo $trans['reply_from']?></h5>
										<div class="card text-black bg-grey" style="border: 1px solid #283593;">
										
											<div class="card-header">
												<p class="card-text" style="margin-left: -410px;"><?php echo $trans['reply_message']?></p>
											<div class="card-footer bg-transparent border-0 text-black">
												<?php 
												//echo date('m-d-Y H:i:s',strtotime($trans['reply_date'])); 
												echo date('F j, Y, g:i a',strtotime($trans['reply_date'])); 
												?>
											</div>
											</div>
										</div>
									</div>									
								  <!--div class="card card-primary "> 
									<div class="box-body card-body p-0">
									  <div class="mailbox-read-info">
										<h6>From: <?php echo $trans['reply_from']?>
										  <span class="mailbox-read-time float-right"><?php echo date('m-d-Y H:i:s',strtotime($trans['reply_date'])); ?></span></h6>
									  </div>
									  <div class="mailbox-read-message">
										  <?php echo $trans['reply_message']?>
									  </div>
									</div>
								  </div-->
								  <?php } 
								    $view_files  = $this->communication_model->get_files($fcommunication_id);
								    if(count($view_files)>0){
								  ?>
								    <div class="col-md-12">
										<div class="row form-group">
											<div class="col-md-12" bis_skin_checked="1">
												<h5 class="card-title text-black">Documents:</h5>
											</div>
											 <?php 
											  foreach ($view_files as $key=>$file){
											  ?>
											<div class="col-md-3" bis_skin_checked="1">
												<!--img src="<?php echo base_url();?>uploads/<?php echo $file['file_name']; ?> " style="height:120px;"-->
												<b><a class="action-icons" style="position:relative;top:2px;left:20px;padding:0 5px;" title="Download Image" href="<?php echo base_url();?>uploads/<?php echo $file['file_name']; ?>" download><?php echo $file['file_name']; ?></a></b>
											</div>
										</div>
									</div>
								  <?php } } ?>
								  <input type="hidden" value="<?php echo $view_message[0]['id'] ?>" name="communication_id">
								  <!--div class=""> 
									<div class="card-footer">
									   <div>										    
											<input type="hidden" value="<?php echo $view_message[0]['id'] ?>" name="communication_id" >
									   </div>
									</div>
								  </div-->	
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php echo form_close( ); ?>
    <?php if($this->uri->segment(3)=='reply_message') { ?>
	<?php echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
	 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" style="padding-top: 0px !important;">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">                        
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<input type="hidden" value="<?php echo $communication_id?>" name="communication_id" >
                        <input type="submit" name="reply" value="Send" class="btn btn-outline-dark">&nbsp;
						<a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><i class="fa fa-ban"></i>Discard</a>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body">
                                <div class="ml-0 ml-sm-4 ml-sm-0">									
                                    <div class="compose-content">                                           
                                            <!--div class="form-group">
                                                <span style="font-weight: 900;">Subject: </span><?php echo $view_message[0]['mail_subject'];?>
                                            </div-->
                                            <div class="form-group">
                                                <textarea class="editor" id="description" name="reply_message" ></textarea>
                                            </div>
                                        <!--/form-->
                                        <!--h5 class="mb-4"><i class="fa fa-paperclip"></i> Attatchment</h5-->
                                        <!--form action="#" class="d-flex flex-column align-items-center justify-content-center"-->
                                            <!--div class="fallback w-100">
                                                <input type="file" class="dropify" data-default-file="" name="files[]" multiple>
                                            </div-->
                                        <!--/form-->
                                    </div>
									<?php echo form_close( );
									$comm_id=$view_message[0]['communication_id'];
									$random_id=substr($comm_id,3);
									?>									
									<div class="container" >
										<div class='content'>
										<form action="<?php echo base_url()?>admin/communications/files_upload" class="dropzone" id="myAwesomeDropzone">
										<input type="hidden" value="<?php echo $random_id;?>" id="random_id" name="random_id">
										</form>  
										</div> 
									</div>	
									<!--div class="container" style="padding-right: 0px;padding-left: 0px;">
										<div class="dropzone">
											<div class="dz-message needsclick">
												<strong>Drop files here or click to upload.</strong><br />
											</div>
										</div>		
									</div-->								
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php } ?>
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	
	<!--Drag and Drop Files-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
	<!--Drag and Drop Files-->
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
	<script type='text/javascript'>
        Dropzone.autoDiscover = false;
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;    
                
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url()?>admin/communications/files_upload',
                    data: {name: name,request: 2},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
        </script>
	