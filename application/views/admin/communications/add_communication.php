<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<style>
.card {
    height: auto;
}
.email-right-box {
    padding-left:0 !important;
    padding-right:0 !important;
}
.dropzone .dz-preview .dz-progress {
    display:none;
}
hr {
    margin-top: 10px;
    margin-bottom: 10px;
}
</style>
<?php
$length = 7;
$characters = '0123456789';
$random_id = "";
for ($i = 0; $length > $i; $i++) {
$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
}
?>
<div class="mini-header">
	<div class="">
        <div class="row page-titles mx-0">
           <div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
							<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/communications">Communications</a></li>
							<li class="breadcrumb-item active">Compose</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>		
            <div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Discard</a>
				</div>
			</div>
        </div>
	</div>
</div>
<div class="content-body">
    <div class="container-fluid"
        <!-- row -->
        <div class="row">
            <div class="col-lg-12">             
                <div class="card">
                    <div class="card-body">
                        <div class="email-right-box ml-0 ml-sm-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="right-box-padding">
                                        <div class="read-content">
                                            <div class="form-group pt-3">
												<div class="row">
													<div class="form-group col-lg-6 col-md-6">
														<select multiple class="form-control selectpicker" data-actions-box="true" id="users_list" name="users_list[]" required title="Select Users">
                                                        <option value="">Select Users</option>
                                                        <?php foreach($users as $user) {
															if($_SESSION['sadevelopers_admin']['id']!=$user['id'])
															{
														?>
															<option value="<?php echo $user['id']?>"><?php echo $user['firstname']?> (<?php echo $user['email']?>)</option>
															<?php } }?>	
														</select>
													</div>
													<div class="form-group col-lg-6 col-md-6">
														<input type="text" class="form-control bg-transparent" placeholder="*Subject:" name="subject" id="subject">
													</div>
												</div>
                                                <div class="row">
                                                    <div class="form-group col-lg-12 col-md-12">
                                                        <textarea class="editor" id="description" name="message"></textarea>
														<input type="hidden" value="<?php echo $random_id;?>" id="random_id" name="random_id">
                                                    </div>
                                                    <div class="form-group col-lg-12 col-md-12">
                                                        <div class="dropzone" ></div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button id="imgsubbutt" class="btn btn-primary btn-sl-sm mb-5" type="button">Send</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('.dropzone', {
  url: "../communications/dragDropUpload", 
  addRemoveLinks: true,                       
  autoProcessQueue: false,
});

$('#imgsubbutt').click(function(){
	var message= tinymce.get("description").getContent();
    var users_list= $('#users_list').val();
    var subject= $('#subject').val();
    var random_id= $('#random_id').val();
	
	
    $.ajax({
        url:"../communications/add_conversation",
        method:"POST",
        async: true,
        dataType:"json",
        data:{message: message,users_list: users_list,subject: subject,random_id:random_id},
        success: function(data)
        {			
          var out =JSON.parse(data);
          myDropzone.on("sending", function(file, xhr, data) {
            data.append("conversation_id", out);
        });
        myDropzone.processQueue();  
        },
        error: function (data) {
          
        },
        complete: function (data) {
            
        }
    })
//	alert("Hi");
});
</script>