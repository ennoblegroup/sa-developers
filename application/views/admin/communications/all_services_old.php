
    <section class="content-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">
					<ol class="breadcrumb">
						
						<li class="breadcrumb-item active">Services</li>
					</ol>
				</div>
				<div class="col-sm-6">
						<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
							<div class="mb-none float-sm-right">
							   <a href="<?= base_url('admin/services/add'); ?>"><button type="button"  style="float:right;" class="form btn btn-green"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add client</button></a>
							</div>
						</div>
					</div>
			</div>
		</div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body custom_body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Service ID</th>
									<th>Service Name</th>
									<th>Service Description</th>									
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; foreach ($all_services as $key=>$phar){ 
									
								?>
								<tr>
									<td> <?php echo $phar['service_id']?> </td>
									
									<td> <?php echo $phar['service_name']?> </td>
									<td> <?php echo $phar['service_description']?></td>
									<td> <a href="<?= base_url('admin/services/edit/'.$phar['id']); ?>"> <span class="btn btn-sm btn-green"><i class="fa fa-pencil"></i></span> </a>
									<a href="#" class="removeRecord" id = "<?php echo $phar['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></span></a> </td>
								</tr>
								<?php $i++; }?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
    </section>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: '<i class="fa fa-check" aria-hidden="true"></i>Confirm',
            btnClass: 'form btn btn-red',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/services/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: '<i class="fa fa-ban"></i>cancel',
            btnClass: 'form btn btn-grey',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
</script>
<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});
</script>
<script>
$("#nav_services").addClass('active');
</script> 