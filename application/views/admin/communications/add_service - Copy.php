	<?php echo form_open(base_url('admin/services/add'), 'id="quickForm" class="form-horizontal"');  ?> 
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-4">
						<ol class="breadcrumb">
							
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/services">Services</a></li>
							<li class="breadcrumb-item active">Add Service</li>
						</ol>
					</div>
					<div class="col-sm-8">
						<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
							<div class="mb-none float-sm-right">
							    <input type="submit" name="submit" value="Submit" class="form btn btn-green">
							   <button type="button" onclick="javascript:goBack()" class="form btn btn-grey"><i class="fa fa-ban"></i>Cancel</button>
							  
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.container-fluid -->
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card card-primary card-outline">
							<!-- /.card-header -->
							<div class="card-body custom_body">
								<fieldset>
									<div class="row">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<?php if(isset($msg) || validation_errors() !== ''): ?>
											  <div class="alert alert-danger alert-dismissible">
												  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												  <?= validation_errors();?>
												  <?= isset($msg)? $msg: ''; ?>
											  </div>
											<?php endif; ?>
											<div class="row">																
												<div class="form-group col-lg-6 col-md-6">
													<label>Service Name</label>
													<input class="form-control" id="service_name" placeholder="Service Name" name="service_name" >
												</div>												
											</div>
											<div class="row">																
												<div class="form-group col-lg-12 col-md-12">
													<label>Service Description</label>
													<textarea class="form-control" id="service_description" rows="5" style="height:auto !important;" placeholder="Service Description" name="service_description" ></textarea>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
							</div>
							<!-- /.card-body -->
						</div>
						<!-- /. box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div><!-- /.container-fluid -->
		</section>
	<?php echo form_close( ); ?>
<script type="text/javascript">
   $(".us_zip").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValuezip(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $(this).val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $(this).val(inputValue); //correct value entered to your input.
       inputValue = getInputValuezip();//get value again, becuase it changed, this one using for changing color of input border
 });

function getInputValuezip() {
	var inputValue = $(this).val().replace(/\D/g,'');  //remove all non numeric character
	return inputValue;
}
$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
      service_name: {
        required: true,
        minlength: 3
      },
      service_description: {
        required: true,
        minlength: 3
      },
    },
    messages: {
      service_name: {
        required: "Please enter Service Name",
        minlength: "Service Name must be at least 3 characters long"
      },
      service_description: {
        required: "Please enter Service Description",
        minlength: "Service Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<script>
$("#nav_services").addClass('active');
</script> 