<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<?php echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
		<div class="mini-header">
			<div class="">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)"><?php echo $this->uri->segment('2');?>Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/communications">Communications</a></li>
                            <li class="breadcrumb-item active">Reply Message</li>
                        </ol>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<input type="hidden" value="<?php echo $communication_id?>" name="communication_id" >
                        <input type="submit" name="reply" value="Send" class="btn btn-outline-dark">&nbsp;
						<a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Discard</a>
                    </div>
                </div>
			</div>
		</div>
        <div class="content-body">
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body">
                                <div class="ml-0 ml-sm-4 ml-sm-0">									
                                    <div class="compose-content">                                           
                                            <div class="form-group">
                                                <span style="font-weight: 900;">Subject: </span><?php echo $view_message[0]['mail_subject']; ?>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="editor" id="description" name="message" ></textarea>
                                            </div>
                                        <!--/form-->
                                        <!--h5 class="mb-4"><i class="fa fa-paperclip"></i> Attatchment</h5-->
                                        <!--form action="#" class="d-flex flex-column align-items-center justify-content-center"-->
                                            <!--div class="fallback w-100">
                                                <input type="file" class="dropify" data-default-file="" name="files[]" multiple>
                                            </div-->
                                        <!--/form-->
                                    </div>
									<!--div class="container">
										<h2>Drag And Drop Files</h2>		
										<div class="dropzone">
											<div class="dz-message needsclick">
												<strong>Drop files here or click to upload.</strong><br />
											</div>
										</div>		
									</div-->
									<?php echo form_close( ); ?>									
									<div class="container" >
										<div class='content'>
										<form action="<?php echo base_url()?>admin/communications/files_upload" class="dropzone" id="myAwesomeDropzone">
										<input type="hidden" value="<?php echo $random_id;?>" id="random_id" name="random_id">
										</form>  
										</div> 
									</div>	
									
                                    <!--div class="text-left mt-4 mb-5">
                                        <button class="btn btn-outline-dark btn-sl-sm mr-3" type="button">
										<span class="mr-2"><i class="fa fa-paper-plane"></i></span> Send</button>
                                        <button class="btn btn-dark btn-sl-sm" type="button">
										<span class="mr-2"><i class="fa fa-times" aria-hidden="true"></i></span> Discard</button>
                                    </div-->									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
		<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	
	<!--Drag and Drop Files-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
	<!--Drag and Drop Files-->
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
	<script type='text/javascript'>
        Dropzone.autoDiscover = false;
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;    
                
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url()?>admin/communications/files_upload',
                    data: {name: name,request: 2},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
        </script>