<style>
.count_circle {
    position: relative;
    right: 9px;
    bottom: 10px;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    font-size: 10px;
    color: #fff;
    padding: 1px;
    line-height: 5px;
    font-weight: bold;
    text-align: center;
    background: grey;
}
</style>
<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',3);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',3);
$this->db->where('permission_id',1);
$status_countv = $this->db->get('roles_permissions')->num_rows();
if($status_countv >0 || $roleid==0){
	$user_classv ='show_cls';
}else{
	$user_classv ='hide_cls';
}
?>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Communications</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/communications/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Compose</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body custom_body">
						<table id="example3" class="ui celled table" style="width:100%">
							<thead>
								<tr>
									<th width="10%" nowrap>Communication ID</th>
									<th width="20%">From</th>
									<th width="20%">To</th>
									<th width="20%">Subject</th>	
									<th width="15%">Date</th>
									<!--th>Message</th-->
									<th width="10%">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php 								
								$i=0; //print_r($all_communications);
								foreach ($all_communications as $key=>$comm){ 
								$newemail_count= $this->db->query("select * from communications WHERE (id =".$comm['id']." AND from_user_id!=".$_SESSION['sadevelopers_admin']['admin_id']." AND read_status = '1')")->num_rows();
								$unread_count= $this->db->query("select * from communications_replies WHERE (communication_id =".$comm['id']." AND user_id!=".$_SESSION['sadevelopers_admin']['admin_id']." AND read_status = '1')")->num_rows();
								if($comm['read_status']==1){ 
								?>
								<tr>
									<td> <?php echo $comm['communication_id']?> </td>	
									<td> <?php echo $comm['mail_from']?> </td>									
									<td> <?php echo $comm['mail_to']?> </td>
									<td> <?php echo $comm['mail_subject']?></td>
									<td class="mailbox-date"><?php echo date('m-d-Y',strtotime($comm['created_date'])); ?> (<?php echo $time_ago[$i]?>)</td>
									<td> <a href="<?= base_url('admin/communications/view_communication/'.$comm['id']); ?>"> <span class="btn btn-sm btn-default"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> </a> 
									<?php if($newemail_count>0 || $unread_count>0){ ?>
									<span class="btn btn-sm count_circle"><?php if($newemail_count>0){echo $newemail_count;}else if($unread_count>0){echo $unread_count;} ?></span>
									<?php } ?>
									 </td>
								</tr>
								<?php }
								if($comm['read_status']==0){ 
								?>
								<tr>
									<td> <?php echo $comm['communication_id']?> </td>	
									<td> <?php echo $comm['mail_from']?> </td>									
									<td> <?php echo $comm['mail_to']?> </td>
									<td> <?php echo $comm['mail_subject']?></td>
									<td class="mailbox-date"><?php echo date('m-d-Y',strtotime($comm['created_date'])); ?> (<?php echo $time_ago[$i]?>)</td>
									<td> <a href="<?= base_url('admin/communications/view_communication/'.$comm['id']); ?>"> <span class="btn btn-sm btn-default <?php echo $user_classv;?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> </a> 
									<?php if($newemail_count>0 || $unread_count>0){ ?>
									<span class="btn btn-sm count_circle"><?php if($newemail_count>0){echo $newemail_count;}else if($unread_count>0){echo $unread_count;} ?></span>
									<?php } ?>
									 </td>
								</tr>
								<?php }									
								$i++; }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var service_id = $(this).attr('service_sid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/services/update_status');?>",
			  data: {id: id, service_id: service_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+service_id).load(" #status_roww"+service_id);
			  }
			});
		});
	});
</script> 