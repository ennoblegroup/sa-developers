	<?php echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
	 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/communications">Communications</a></li>
                            <li class="breadcrumb-item active">View Message</li>
                        </ol>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><i class="fa fa-arrow-left"></i>Back</a>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body">
								<?php $i=1; foreach ($view_message as $key=>$trans){ ?>    
								  <!--div class="card card-primary card-outline">
									<div class="card-header">
									  <h3 class="card-title"><?php echo $trans['mail_subject']?></h3>
									  <div class="card-tools">
									  </div>
									</div>
									<div class="box-body card-body p-0">
									  <div class="mailbox-read-info">
										<h6>From: <?php echo $trans['mail_from']?>
										  <span class="mailbox-read-time float-right"><?php echo date('m-d-Y H:i:s',strtotime($trans['created_date'])); ?></span></h6>
									  </div>
									  <div class="mailbox-read-message">
										  <?php echo $trans['mail_message']?>
									  </div>
									</div> 
								  </div-->
								  <div class="col-xl-4 col-xxl-12 col-lg-12 col-sm-12">
									<div class="card text-white bg-primary">
										<div class="card-header">
											<h5 class="card-title text-white"><?php echo $trans['mail_subject']?></h5>
										</div>
										<div class="card-body mb-0">
											<p class="card-text"> <?php echo $trans['mail_message']?></p>
										</div>
										<div class="card-footer bg-transparent border-0 text-white"><?php echo date('m-d-Y H:i:s',strtotime($trans['created_date'])); ?>
										</div>
									</div>
								</div>
								<?php } $i=1; foreach ($reply_message as $key=>$trans){ ?>
									<div class="col-xl-4 col-xxl-12 col-lg-12 col-sm-12">
										<div class="card text-white bg-info">
											<div class="card-header">
												<h5 class="card-title text-white">From: <?php echo $trans['reply_from']?></h5>
											</div>
											<div class="card-body mb-0">
												<p class="card-text"><?php echo $trans['reply_message']?></p>
											</div>
											<div class="card-footer bg-transparent border-0 text-white">
												<?php echo date('m-d-Y H:i:s',strtotime($trans['reply_date'])); ?>
											</div>
										</div>
									</div>								
								  <!--div class="card card-primary "> 
									<div class="box-body card-body p-0">
									  <div class="mailbox-read-info">
										<h6>From: <?php echo $trans['reply_from']?>
										  <span class="mailbox-read-time float-right"><?php echo date('m-d-Y H:i:s',strtotime($trans['reply_date'])); ?></span></h6>
									  </div>
									  <div class="mailbox-read-message">
										  <?php echo $trans['reply_message']?>
									  </div>
									</div>
								  </div-->
								  <?php } ?>								
								  <div class=""> 
									<div class="card-footer">
									   <div>
										    <!--button class="form float-right btn btn-outline-dark" type="button" href="#myModal" data-toggle="modal" class=""><i class="fa fa-reply"></i> Reply</button-->
											<input type="hidden" value="<?php echo $view_message[0]['id'] ?>" name="communication_id" >
											<input type="submit" name="submit" value="Reply" class="btn btn-outline-dark">&nbsp;
									   </div>
									</div>
								  </div>	
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php echo form_close( ); ?>
	 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 style="text-align:left" class="modal-title">Reply</h4>
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          </div>
          <div class="modal-body">
              <form role="form" class="form-horizontal" method="post" action="<?php echo base_url();?>admin/Communications/reply_message">
                  <div class="form-group">
                      <label class="col-lg-2 control-label">Message</label>
                      <div class="col-lg-12">
                          <textarea rows="5" cols="30" class="form-control" required id="" name="reply_message"></textarea>
                          <input type="hidden" value="<?php echo $view_message[0]['id'] ?>" name="communication_id" >
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-lg-12">                         
                          <button class="btn btn-outline-dark" style="float:right" type="submit">Send</button>
                      </div>
                  </div>
              </form>
          </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>