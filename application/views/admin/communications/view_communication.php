<style>
.mt-5, .my-5 {
    margin-top: 0px !important;
}
.mb-4, .my-4 {
    margin-bottom: 0px !important;
}
.content-body .container {
  margin-top: 0px; }
  .container {
  padding-right: 0px;
  padding-left: 0px;
  }  
  .custom_body
  {
	min-height: 458.76px !important;
    max-height: 458.76px !important;
  }
</style>
<!--style>
.content-body
  {
  min-height: 585px !important;
  }
<style-->  
	<?php //echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
	 <!--**********************************
            Content body start
        ***********************************-->
		<div class="mini-header">
			<div class="">
                <div class="row page-titles mx-0">
                    <div class="col-sm-4 p-md-0 ">
						<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
							<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
									<li class="breadcrumb-item active"><a href="<?php echo base_url()?>admin/communications">Communications</a></li>
									<li class="breadcrumb-item active">View Message</li>
								</ol>
							<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
						</span>
                    </div>  										
                </div>
			</div>
		</div>
        <div class="content-body" >
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body"> 
								<?php $i=1; foreach ($view_message as $key=>$trans){ 
									$fcommunication_id=$trans['communication_id'];
									?> 
								<h5 class="my-1 text-primary">Subject: <?php echo $trans['mail_subject']?></h5>
								<div id="accordion-two" class="accordion accordion-bordered">
									<div class="accordion__item" style="border: 1px solid #d49797;">
										<div class="accordion__header" data-toggle="collapse" data-target="#bordered_collapseOne">
											<span class="accordion__header--text">From: <?php echo $trans['mail_from']?></span>
											<span class="accordion__header--text" style="float: right;">
											<?php echo date('F j, Y, g:i a',strtotime($trans['created_date']));?></span>
										</div>
										<div id="bordered_collapseOne" class="collapse accordion__body show" data-parent="#accordion-two">
                                            <div class="accordion__body--text">		
												<div class="read-content-body">
													<?php echo $trans['mail_message']?>
												</div>
												<?php 
												$view_files  = $this->communication_model->get_files($fcommunication_id);
												if(count($view_files)>0){
												?>
												<div class="read-content-body">
													<i class="fa fa-download mb-2"></i> Attachments(<?php echo count($view_files);?>)
													<div class="accordion__body--text">
														<div class="row attachment">
															<?php 
															  foreach ($view_files as $key=>$file){
															?>
															<div class="col-auto" style="padding-left: 0px;">
																<!--a href="javascript:void()" class="text-muted">My-Photo.png</a-->
																<b><a class="text-muted" style="position:relative;" title="Download Image" href="<?php echo base_url();?>uploads/<?php echo $file['file_name']; ?>" download><?php echo $file['file_name']; ?></a></b>
															</div>
															<?php
															}
															?>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
									<?php } 
								    $i=3;
									$j=1;
									foreach ($reply_message as $key=>$trans){
								    $cnt=count($reply_message);
									$fcommunication_id=$trans['reply_id'];
									?>
									<div class="accordion__item" style="border: 1px solid #98aac5;">
										<div class="accordion__header" data-toggle="collapse" data-target="#bordered_collapse<?php echo $i;?>">
											<span class="accordion__header--text">Reply: <?php echo $trans['reply_from']?></span>
											<span class="accordion__header--text" style="float: right;">
											<?php echo date('F j, Y, g:i a',strtotime($trans['reply_date']));?></span>
											</div>
									<div id="bordered_collapse<?php echo $i;?>" class="collapse accordion__body <?php if($j==$cnt) { ?> show <?php } ?>" data-parent="#accordion-two">
                                            <div class="accordion__body--text">		
												<div class="read-content-body">
													<?php echo $trans['reply_message']?>
												</div>
												<?php 
												$view_files  = $this->communication_model->get_files($fcommunication_id);
												if(count($view_files)>0){
												?>
												<div class="read-content-body">
													<i class="fa fa-download mb-2"></i> Attachments(<?php echo count($view_files);?>)
													<div class="accordion__body--text">
														<div class="row attachment">
															<?php 
															  foreach ($view_files as $key=>$file){
															?>
															<div class="col-auto" style="padding-left: 0px;">
																<b><a class="text-muted" style="position:relative;" title="Download Image" href="<?php echo base_url();?>uploads/<?php echo $file['file_name']; ?>" download><?php echo $file['file_name']; ?></a></b>
															</div>
															<?php
															}
															?>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
									</div>
									<?php $i++; $j++; 
									} 
									?>
								</div>								
								<input type="hidden" value="<?php echo $view_message[0]['id'] ?>" name="communication_id">
								<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
								<div class="welcome-text">
								<input type="submit" name="submit" id="submit" value="Reply" class="btn btn-outline-dark">
								</div>
							</div>	
                            </div>
								
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	<?php //echo form_close( ); ?>
    <?php 	
	$length = 7;
	$characters = '0123456789';
	$random_id = "";
	for ($i = 0; $length > $i; $i++) {
	$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
	}
	echo form_open(base_url('admin/Communications/reply_message/'.$view_message[0]['id']), 'id="quickForm" class="form-horizontal"  enctype="multipart/form-data"');  ?> 
	 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" id="myDiv" style="padding-top: 0px !important;display:none;">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">                        
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
						<input type="hidden" value="<?php echo $view_message[0]['id']?>" name="communication_id" >
                        <input type="submit" name="reply" value="Send" class="btn btn-outline-dark">&nbsp;
						<a type="button" href="<?php echo base_url()?>admin/communications" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Discard</a>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body custom_body">
                                <div class="ml-0 ml-sm-4 ml-sm-0">									
                                    <div class="compose-content"> 
										<div class="form-group">
											<textarea class="editor" id="description" name="reply_message" ></textarea>
											<input type="hidden" value="<?php echo $random_id;?>" id="random_id" name="random_id">
										</div>
                                    </div>
									<?php echo form_close( );
									//$comm_id=$view_message[0]['communication_id'];
									//$random_id=substr($comm_id,3);
									?>									
									<div class="container" >
										<div class='content'>
										<form action="<?php echo base_url()?>admin/communications/files_upload" class="dropzone" id="myAwesomeDropzone">
										<input type="hidden" value="<?php echo $random_id;?>" id="random_id" name="random_id">
										</form>  
										</div> 
									</div>							
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->	
	
	<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script type='text/javascript'>	
	$("#submit").click(function() {		
		document.getElementById('myDiv').style.display = "block";
		$('html, body').animate({
			scrollTop: $("#myDiv").offset().top
		}, 2000);
	});
	</script>
	<!--Drag and Drop Files-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
	<!--Drag and Drop Files-->
	<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
	<script type='text/javascript'>
        Dropzone.autoDiscover = false;
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            removedfile: function(file) {
                var name = file.name;    
                
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url()?>admin/communications/files_upload',
                    data: {name: name,request: 2},
                    sucess: function(data){
                        console.log('success: ' + data);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        });
	
    </script>
	