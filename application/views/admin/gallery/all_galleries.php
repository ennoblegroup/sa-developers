
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<style>
.ratings i {
    color: green
}

.install span {
    font-size: 12px
}

.col-md-4 {
    margin-top: 27px
}

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0);
  transition: background 0.5s ease;
}

.ttt:hover .overlay {
  display: block;
  background: rgba(0, 0, 0, .3);
}


.title {
  position: absolute;
  width: 500px;
  left: 0;
  top: 120px;
  font-weight: 700;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  z-index: 1;
  transition: top .5s ease;
}

.ttt:hover .title {
  top: 90px;
}

.button {
  position: absolute;
  left:35%;
  top: 30%;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {

}

.ttt:hover .button {
  opacity: 1;
}
.card {
    height: auto;
}
.ttt h4 {
  white-space: nowrap; 
  width: auto; 
  overflow: hidden;
  text-overflow: ellipsis;
}

.ttt h4:hover {
  overflow: visible;
}
.modal-dialog {
    max-width: 65%;
}
.gallery {
  margin: 10px;
  list-style-type: none;
  padding: 0;
  font-size: 0;
}
.gallery li {
  font-size: 13px;
  display: inline-block;
  position: relative;
  width: 20%;
  padding-bottom: 11%;
}

.card-img-top {
  height:150px;
}

.gallery a img {
  width: 100%;
  height: 100%;
}
.gallery a video {
  width: 100%;
  height: 100%;
}
.gallery a span {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  margin-bottom: -50px;
  background: #000;
  background: rgba(0, 0, 0, 0.5);
  color: #FFF;
  padding: 10px;
  text-align: center;
  transition: all 0.3s ease-out;
}

html.no-touch .gallery a:hover span,
html.touch .gallery a.touchFocus span {
  margin-bottom: 0;
}





@media only screen and (max-width: 740px) {
  /* Small desktop / ipad view: 3 tiles */
  .gallery li {
    width: 33.3%;
    padding-bottom: 33.3%;
  }
}
@media only screen and (max-width: 540px) {
  /* Tablet view: 2 tiles */
  .gallery li {
    width: 50%;
    padding-bottom: 50%;
  }
}
@media only screen and (max-width: 320px) {
  /* Smartphone view: 1 tile */
  .gallery li {
    width: 100%;
    padding-bottom: 100%;
  }
}
.col-img{
	flex: 0 0 20%;
    max-width: 20%;
	position: relative;
    width: 100%;
    padding-right: 5px;
    padding-left: 5px;
}
.col-img .card{
	border: 1px solid #0000003b;
}
.col-img .card .card-body{
	border-top: 1px solid #0000003b;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Gallery</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a type="button"  data-toggle="modal" data-target="#exampleModalLong"  class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Images</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="">
							<div class="row theme-structure big-file-manager">
								<?php if(count($all_gallery)>0){?>
								<?php $i=1; foreach($all_gallery['gallery'] as $row){ ?>
									<div class="col-md-12">
										<h6 class="rr"><?= $row['gallery_type']; ?></h6>
									</div>
									<?php $j=1; foreach($row['gallery'] as $row): ?>
									<?php if($row['file_type']==0){?>
									<div class="col-img">
										<div class="card">
											<a href="<?= base_url() ?>images/project_gallery/<?= $row['file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
												<img class="card-img-top" src="<?= base_url() ?>images/project_gallery/<?= $row['file']; ?>">

											</a>
											<div class="card-body">
												<p class="card-text"><b>Title : </b> <?php if($row['gallery_title'] !=''){echo $row['gallery_title'];}else{ echo 'No Title';}?></p>
											</div>
										</div>
									</div>
									<?php }?>
									<?php $j++; endforeach; ?>
								<?php $i++; } } else {?>
								<center><p>No Images Found</p></center>
								<?php }?>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Images/Videos</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">		
								<div class="form-group col-lg-4 col-md-4">
									<input type="text" class="form-control" name="gallery_title" placeholder="Title" id="gallery_title">
								</div>
								<div class="form-group col-lg-4 col-md-4">
									<select class="form-control selectpicker" data-live-search="true" title="Gallery Type" id="gallery_type_category" name="gallery_type_category" >
										<?php foreach ($all_gallery_types as $k => $m): ?>
										  <option value="<?php echo $m['id'] ?>"><?php echo $m['gallery_type_name'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="form-group col-lg-4 col-md-4">
									<select placeholder="Visibility" class="form-control selectpicker" data-live-search="true"  name="visibility" id="visibility" title="Visibility">
										<option value="0">Hide on Website</option>	
										<option value="1">Show on Website</option>		
									</select>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="dropzone" ></div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="button" name="submit" value="Submit" class="btn btn-outline-dark gsubmit">Submit</button>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('.dropzone', {
  url: "../admin/projects/project_gallery_upload", 
  addRemoveLinks: true,
  maxFiles: 2000,
  maxFilesize: 209715200,
  acceptedFiles: ".jpeg,.jpg,.png,.gif",  
  autoProcessQueue: false,
  dictDefaultMessage: "Drag and drop Images/Videos here",
  init: function() {
	this.on('success', function(){
		if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
				//location.reload();
		}
	});
	}
});
$( ".gsubmit" ).click(function( event ) {
	var project_id= 0;
	var gallery_type_category= $('#gallery_type_category').val();
	var gallery_title= $('#gallery_title').val();
	var visibility= $('#visibility').val();
	myDropzone.on("sending", function(file, xhr, data) {
		data.append("project_id", project_id);
		data.append("gallery_type", 0);
		data.append("gallery_type_category", gallery_type_category);
		data.append("gallery_title", gallery_title);
		data.append("visibility", visibility);
	});
	myDropzone.on("processing", function() {
		myDropzone.options.autoProcessQueue = true;
	});
	myDropzone.processQueue();
	
});
</script>