<style>
.table thead th
{
	text-transform:none !important;
	font-size: 14px !important;
}
.table th, .table td
{
	    border-top: 0px solid #EEEEEE !important;
}
</style>
<?php echo form_open(base_url('admin/insurance_companies/add'), 'id="quickForm" class="form-horizontal"');  ?> 		
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-6 p-md-0">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/insurance_companies">Insurance Companies</a></li>
							<li class="breadcrumb-item active">Add Insurance Company</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/insurance_companies" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<table id="example1" class="table">
							<thead>
								<tr>
									<th>Insurance Company Details</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="insurance_company_name" placeholder="*Insurance Company" title="Insurance Company" name="insurance_company_name" maxLength="50">
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="ein_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="EIN Number" title="EIN Number" name="ein_number" maxLength="9">
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="naic_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Number" title="NAIC Number" name="naic_number" maxLength="5">
										</div> 
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="naic_group_code" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Group Code" title="NAIC Group Code" name="naic_group_code" maxLength="4">
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<select placeholder="*State of Domicile" title="State of Domicile" class="form-control"  name="state_of_domicile" id="state_of_domicile">
												<option value="">*Select State of Domicile</option>
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<select placeholder="Company Type" title="Company Type" class="form-control"  name="company_type" id="company_type">
												<option value="">Select Company Type</option>	
												<option value="fraternal">F - Fraternal</option>												
												<option value="lah">L - Life, Accident, and Health</option>												
												<option value="pc">P - Property and Casualty</option>												
												<option value="title">T - Title</option>												
												<option value="health">X - Health</option>												
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="form-group col-lg-12 col-md-12">
											<select placeholder="Company Status" title="Company Status" class="form-control"  name="company_status" id="company_status">
												<option value="">Select Company Status</option>
												<option value="conservatorship">0 - Conservatorship</option>												
												<option value="active">1 - Active</option>												
												<option value="rehabilitation">4 - Rehabilitation, Permanent or temporarily in receivership</option>
												<option value="liquidation">6 - In Liquidation</option>												
											</select>
                                     	</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card card-primary card-outline">
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example1" class="table">
							<thead>
								<tr>
									<th colspan="2">Address Details</th>
							</thead>
							<tbody>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40">
										</div>
									</td>
								</tr>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
										</div>
									</td>
								</tr>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30">
										</div>
									</td>
								</tr>
								<tr>																
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<select placeholder="State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="state">
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="15">
										</div>
									</td>
								</tr>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="phone_number" placeholder="*Phone Number" title="*Phone Number" name="phone_number">
										</div>
									</td>
								</tr>
								<tr>
									<td> 
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="email_address" placeholder="E-Mail Address" title="E-Mail Address" name="email_address" maxLength="50">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /. box -->
			</div>
		</div>
	</div>
</div>
	<?php echo form_close( ); ?>
	<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid email address.");
  $('#quickForm').validate({
    rules: {
      insurance_company_name: {
        required: true
      },
	  naic_number: {
        required: true
      },
      naic_group_code: {
        required: true
      },
      state_of_domicile: {
        required: true
      },
      address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true,
      },
	  zip_code: {
        required: true,
        minlength: 5
      }, 
	  phone_number: {
        required: true
      },
	  email_address: {
        required: true,
		email: true,
        validate_email: true
      },
    },  
    messages: {
      insurance_company_name: {
        required: "Please enter insurance company name"
      },
       naic_number: {
        required: "Please enter naic number"
      },
       naic_group_code: {
        required: "Please enter naic group code"
      },
       state_of_domicile: {
        required: "Please select state of domicile"
      },
      address1: {
        required: "Please enter Address 1"
      },
	  city: {
        required: "Please enter City"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
	  phone_number: {
        required: "Please enter phone number"
      },
       business_email: {
        required: "Please enter email address",
        email: "Please enter a valid email address"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
</script>
<script>
$("#zip_code").on('input', function() {  //this is use for every time input change.
//alert("Hi");
       var inputValue = getInputValuez(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#zip_code").val('');
           return false;
       }    
       if (inputValue < 100000)
       {
           inputValue =inputValue;
       }
	   else if (inputValue < 100000000) 
       {
           inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }else
       {
            inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
       }       
       $("#zip_code").val(inputValue); //correct value entered to your input.
       inputValue = getInputValuez();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 200000000) && (inputValue < 999999999))
     {
         $("#zip_code").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#zip_code").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValuez() {
         var inputValue = $("#zip_code").val().replace(/\D/g,'');  //remove all non numeric character
        return inputValue;
}
 $("#phone_number").on('input', function() {  //this is use for every time input change.
       var inputValue = getInputValue(); //get value from input and make it usefull number
       var length = inputValue.length; //get lenth of input
        
        if (length == 0)
       {
           $("#phone_number").val('');
           return false;
       }    
       if (inputValue < 1000)
       {
           inputValue = '+1' + ' ' + '('+inputValue;
       }else if (inputValue < 1000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
       }else if (inputValue < 10000000000) 
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, length);
       }else
       {
           inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + ' ' + inputValue.substring(6, 10);
       }       
       $("#phone_number").val(inputValue); //correct value entered to your input.
       inputValue = getInputValue();//get value again, becuase it changed, this one using for changing color of input border
      if ((inputValue > 2000000000) && (inputValue < 9999999999))
     {
         $("#phone_number").css("border","green solid 1px");//if it is valid phone number than border will be black.
     }else
     {
         $("#phone_number").css("border","red solid 1px");//if it is invalid phone number than border will be red.
     }
 });

    function getInputValue() {
         var inputValue = $("#phone_number").val().replace(/\D/g,'');  //remove all non numeric character
        if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
        {
            var inputValue = inputValue.substring(1, inputValue.length);
        }
        return inputValue;
}

</script>
<script>
$("#nav_clients").addClass('menu-open');
$("#nav_clients>a").addClass('active');
$("#nav_clients_Add").addClass('active');
</script> 