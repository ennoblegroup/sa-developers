<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',5);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',5);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',5);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
?>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			 <div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Insurance Companies</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>       				
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<!--button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Insurance Company</button-->
					<a href="<?= base_url('admin/insurance_companies/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Insurance Company</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Insurance Companies</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="insurance_table" class="ui celled table" style="width:100%">
								<thead>
									<tr>
										<th width="15%">Insurance ID</th>
										<th width="30%">Insurance Company Name</th>
										<th width="25%">Phone Number</th>	
										<th width="15%">Status</th>									
										<th width="15%">Actions</th>
									</tr>
								</thead>
								<tbody id="filter_data">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Insurance Company</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/insurance_companies/add'), 'id="addForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="insurance_company_name" placeholder="*Insurance Company" title="Insurance Company" name="insurance_company_name" maxLength="50">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="ein_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="EIN Number" title="EIN Number" name="ein_number" maxLength="9">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="naic_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Number" title="NAIC Number" name="naic_number" maxLength="5">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="naic_group_code" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Group Code" title="NAIC Group Code" name="naic_group_code" maxLength="4">
								</div>					
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="*State of Domicile" title="State of Domicile" class="form-control"  name="state_of_domicile" id="state_of_domicile">
										<option value="">*Select State of Domicile</option>
										<?php foreach($all_states as $state){ ?>
											<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
										<?php }?>	
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="Company Type" title="Company Type" class="form-control"  name="company_type" id="company_type">
										<option value="">Select Company Type</option>	
										<option value="fraternal">F - Fraternal</option>												
										<option value="lah">L - Life, Accident, and Health</option>												
										<option value="pc">P - Property and Casualty</option>												
										<option value="title">T - Title</option>												
										<option value="health">X - Health</option>												
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="Company Status" title="Company Status" class="form-control"  name="company_status" id="company_status">
										<option value="">Select Company Status</option>
										<option value="conservatorship">0 - Conservatorship</option>												
										<option value="active">1 - Active</option>												
										<option value="rehabilitation">4 - Rehabilitation, Permanent or temporarily in receivership</option>
										<option value="liquidation">6 - In Liquidation</option>												
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="address1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="address2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="city" placeholder="*City" title="*City" name="city" maxLength="30">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="state">
									<option value="">Select State</option>	
									<?php foreach($all_states as $state){ ?>
											<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
										<?php }?>	
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control us_zip" id="zip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="15">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control us_phone" id="phone_number" placeholder="*Phone Number" title="*Phone Number" name="phone_number">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control email_address" id="email_address" placeholder="E-Mail Address" title="E-Mail Address" name="email_address" maxLength="50">
									<input type="hidden" class="form-control" id="iid"  placeholder=""  name="id">
								</div>
							</div>	
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Insurance Company</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open(base_url('admin/insurance_companies/edit'), 'id="editForm" class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="einsurance_company_name" placeholder="*Insurance Company" title="Insurance Company" name="insurance_company_name" maxLength="50">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="eein_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="EIN Number" title="EIN Number" name="ein_number" maxLength="9">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="enaic_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Number" title="NAIC Number" name="naic_number" maxLength="5">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="enaic_group_code" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="*NAIC Group Code" title="NAIC Group Code" name="naic_group_code" maxLength="4">
								</div>					
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="*State of Domicile" title="State of Domicile" class="form-control"  name="state_of_domicile" id="estate_of_domicile">
										<option value="">*Select State of Domicile</option>
										<?php foreach($all_states as $state){ ?>
											<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
										<?php }?>	
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="Company Type" title="Company Type" class="form-control"  name="company_type" id="ecompany_type">
										<option value="">Select Company Type</option>	
										<option value="fraternal">F - Fraternal</option>												
										<option value="lah">L - Life, Accident, and Health</option>												
										<option value="pc">P - Property and Casualty</option>												
										<option value="title">T - Title</option>												
										<option value="health">X - Health</option>												
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="Company Status" title="Company Status" class="form-control"  name="company_status" id="ecompany_status">
										<option value="">Select Company Status</option>
										<option value="conservatorship">0 - Conservatorship</option>												
										<option value="active">1 - Active</option>												
										<option value="rehabilitation">4 - Rehabilitation, Permanent or temporarily in receivership</option>
										<option value="liquidation">6 - In Liquidation</option>												
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="eaddress1" placeholder="*Address1" title="*Address1" name="address1" maxLength="40">
								</div>							
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="eaddress2" placeholder="Address2" title="Address2" name="address2" maxLength="40">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control" id="ecity" placeholder="*City" title="*City" name="city" maxLength="30">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<select placeholder="State" title="*Select State" class="form-control selectpicker" data-live-search="true"  name="state" id="estate">
										<option value="">Select State</option>	
										<?php foreach($all_states as $state){ ?>
											<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
										<?php }?>	
									</select>
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control us_zip" id="ezip_code" placeholder="*Zip Code" title="*Zip Code" name="zip_code" maxLength="15">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control us_phone" id="ephone_number" placeholder="*Phone Number" title="*Phone Number" name="phone_number">
								</div>
								<div class="form-group col-lg-8 col-md-8">
									<input type="text" class="form-control email_address" id="eemail_address" placeholder="E-Mail Address" title="E-Mail Address" name="email_address" >
									<input type="hidden" class="form-control" id="ins_id" placeholder=""  name="id">
								</div>
							</div>							
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Update</button>
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	jQuery.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email Address.");
  $('#addForm').validate({
    rules: {
      insurance_company_name: {
        required: true
      },
      address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true,
      },
	  zip_code: {
        required: true,
        minlength: 5
      }, 
	  phone_number: {
		required: true,
		minlength: 17,
		maxlength: 17
      },
	  email_address: {
        required: true,
		email: true,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/insurance_companies/fetch_email",
			type: "post",
			data: {
			username: function() {
				return $( "#email_address" ).val();
			}
			}
		}
      },
    },  
    messages: {
      insurance_company_name: {
        required: "Please enter Insurance Company Name"
      },
      address1: {
        required: "Please enter Address 1"
      },
	  city: {
        required: "Please enter City"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
	  phone_number: {
        required: "Please enter Phone Number"
      },
	  email_address: {
			required: "Please enter Email Address",
			email: "Please enter a valid Email Address",
			remote:"Email Address has been registered already"
		}
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('#editForm').validate({
    rules: {
      insurance_company_name: {
        required: true
      },
      address1: {
        required: true
      },
      city: {
        required: true
      },
	  state: {
        required: true,
      },
	  zip_code: {
        required: true,
        minlength: 5
      }, 
	  phone_number: {
		required: true,
		minlength: 17,
		maxlength: 17
      },
	  email_address: {
        required: true,
		email: true,
		remote: {
			url: "<?php echo base_url(); ?>" + "admin/insurance_companies/fetch_email_edit",
			type: "post",
			data: {
			email: function() {
				return $( "#eemail_address" ).val();
			},
			ins_id: function() {
				return $( "#ins_id" ).val();
			}
			}
		}
      },
    },  
    messages: {
      insurance_company_name: {
        required: "Please enter Insurance Company Name"
      },
      address1: {
        required: "Please enter Address 1"
      },
	  city: {
        required: "Please enter City"
      },
	  state: {
        required: "Please select State"
      },
	  zip_code: {
        required: "Please enter Zip Code",
        minlength: "Zip Code must be at least 5 digits long"
      },
	  phone_number: {
        required: "Please enter Phone Number"
      },
	  email_address: {
			required: "Please enter Email Address",
			email: "Please enter a valid Email Address",
			remote:"Email Address has been registered already"
		}
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
  $('body').on('input', '#city', function() {
		var txt = '';
		txt = $(this).val();
		$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
	}); 
});
function filter_data()
{
	$.ajax({
		url:"insurance_companies/filter_insurances",
		method:"POST",
		async: true,
		dataType:"json",
		success: function(data)
		{	
			$('table').DataTable().clear();
			$('table').DataTable().destroy();				
			$('#filter_data').html(data.insurances_list);
			tableinit();			
		},
		error: function (data) {
			//var out =JSON.parse(data);
			console.log(data.responseText);
		},
		complete: function (data) {
		// Handle the complete event
			console.log(data);
		}
	})
}
function tableinit(){
	var tt= $('table').DataTable({
		"bRetrieve": true,
		"bProcessing": true,
		"bDestroy": false,
		"bPaginate": true,
		"bAutoWidth": true,
		"bFilter": true,
		"bInfo": true,
		"bJQueryUI": true,
		columnDefs: [
			{ orderable: false, targets: -1 }
		],
		language: {
		"emptyTable": "No claims were found."
		}
	});
	$('table').find('thead tr th').css('width', 'auto');
}
</script>
<script>
$(document).ready(function() {
	$('body').on('click', '#status', function () {
		if($(this).prop("checked") == true){
			var id = 1;
		}
		else {
			var id = 0;
		}
		var inscmp_id = $(this).attr('inscmp_sid');
		$.ajax({
			type: "POST",
			url:"<?php echo base_url('admin/insurance_companies/update_status');?>",
			data: {id: id, inscmp_id: inscmp_id},
			cache: false,
			success: function(data){
				filter_data();
				//$("#status_roww"+inscmp_id).load(" #status_roww"+inscmp_id);
			}
		});
		filter_data();
	});
	$('body').on('click', '.editRecord', function () {
		var inscmp_id = $(this).attr('inscmp_id');
		$.ajax({
			type: "POST",
			url:"<?php echo base_url('admin/insurance_companies/ttt');?>",
			data: {inscmp_id: inscmp_id},
			cache: false,
			success: function(data){
				var out =JSON.parse(data);
				$('#einsurance_company_name').val(out.insurance_company_name);
				$('#eein_number').val(out.ein_number);
				$('#enaic_number').val(out.naic_number);
				$('#enaic_group_code').val(out.naic_group_code);
				$('#estate_of_domicile').val(out.state_of_domicile);
				$('#ecompany_type').val(out.company_type);
				$('#ecompany_status').val(out.company_status);
				$('#eaddress1').val(out.address1);
				$('#eaddress2').val(out.address2);
				$('#ecity').val(out.city);
				$("#estate option[value="+out.state+"]").prop('selected', true);
				$('#estate').attr("placeholder",out.state);
				$('#ezip_code').val(out.zip_code);
				$('#ephone_number').val(out.phone_number);
				$('#eemail_address').val(out.email_address);
				$('#ins_id').val(out.id);
				$('#exampleModalLong1').modal('show');
			}
		});
	});
});
</script>
<script>
 $(".removeRecord").click(function(){
       var id = $(this).attr('inscmp_id');    	
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/insurance_companies/del');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: '</i>cancel',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
    });
</script>
