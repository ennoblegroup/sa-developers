<?php echo form_open_multipart(base_url('admin/materials/edit'), 'id="quickForm" class="form-valid"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/materials'); ?>">Products</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Edit Product</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/forms" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Add Product</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-7">
									<div class="row">																
										<!--div class="form-group col-lg-3 col-md-3">
											<label class="col-form-label">Source</label>
											<input type="text" class="form-control" name="file_source">
										</div-->
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="ematerial_name" value="<?php echo $product['material_name']?>" placeholder="Product Name" name="material_name">
											<input type="hidden" value="<?php echo $product['id']?>" id="material_id" name="material_id">
											<input type="hidden" value="<?php echo $product['category_id']?>" id="category_id" name="category_id">
											
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="ematerial_price" value="<?php echo $product['price']?>" placeholder="Product Price" name="material_price">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<select  multiple="multiple"  title="Select Vendors" data-live-search="true"  class="selectpicker form-control"id="vendor_id" name="vendor_id[]">
												<?php $i=1; foreach($all_vendors as $row): ?>
												
												<option value="<?= $row['id']; ?>" ><?= $row['lastname']; ?> <?= $row['firstname']; ?></option>
												
												<?php $i++; endforeach; ?>
											</select>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="ematerial_brand" value="<?php echo $product['brand']?>" placeholder="Brand" name="material_brand">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="ematerial_tags" value="<?php echo $product['tags']?>" placeholder="Tags" name="material_tags">
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" rows="5" placeholder="Description" id="ematerial_description" name="material_description"><?php echo $product['material_description']?></textarea>
										</div>
										<div class="form-group col-lg-3 col-md-3"></div>
										<div class="form-group col-lg-6 col-md-6">
											
										</div>
									</div>
								</div>
								<div class="form-group col-lg-3 col-md-3">
									<div class="input-group err">
										<div class="custom-file">
											<input type="file" onchange="readURLL(this);" class="custom-file-input " id="eimage" name="ematerial_image" accept="image/x-png,image/gif,image/jpeg">
											<label style="overflow:hidden" class="custom-file-label">Upload Product Picture</label>
										</div>
									</div>
									<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
										<img class="blah" style="width:100%;height:200px" src="<?= base_url() ?>images/materials/<?php echo $product['file']?>" alt="your image" />  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close( ); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$(document).ready(function() {
	<?php $j=1; foreach($product['vendors'] as $vendor): ?>
	var id= "<?php echo $vendor['vendor_id']?>";
	$('#vendor_id option[value="<?php echo $vendor["vendor_id"]?>"]').attr('selected','selected');
	<?php $j++; endforeach; ?>
});

$(document).ready(function() {
	<?php $j=1; foreach($product['vendors'] as $vendor): ?>
	var id= "<?php echo $vendor['vendor_id']?>";
	$('#vendor_id option[value="<?php echo $vendor["vendor_id"]?>"]').attr('selected','selected');
	<?php $j++; endforeach; ?>
	$('#quickForm').validate({	  
		rules: {
		material_name: {
			required: true
		},			
		material_price: {
			required: true
		},
		material_brand: {
			required: true
		},
		material_tags: {
			required: true
		},
		material_description: {
			required: true
		},
		material_iamge: {
			required: true
		},
		},
		messages: {
		material_name: {
			required: "Please enter Product Name"
		},
		material_price: {
			required: "Please enter Price"
		},
		material_brand: {
			required: "Please enter Brand"
		},
		material_tags: {
			required: "Please enter Tags"
		},
		material_description: {
			required: "Please enter Description"
		},
		material_iamge: {
			required: "Please upload Product Image"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
});
</script>