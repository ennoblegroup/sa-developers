<?php echo form_open_multipart(base_url('admin/categories/add'), 'id="quickForm" class="form-valid"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item"><a href="<?= base_url('admin/categories'); ?>">Product Categories</a></li>
							<li class="breadcrumb-item active"><a href="javascript:void(0)">Add Product Category</a></li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/categories" class="form btn btn-outline-dark"><!--i class="fa fa-ban"></i-->Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Add Category</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6">
									<div class="row">																
										<!--div class="form-group col-lg-3 col-md-3">
											<label class="col-form-label">Source</label>
											<input type="text" class="form-control" name="file_source">
										</div-->
										<div class="form-group col-lg-12 col-md-12">
											<input type="text" class="form-control" id="category_name" placeholder="Category Name" name="category_name">
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<textarea class="form-control" rows="5" placeholder="Description" name="category_description"></textarea>
										</div>
										<div class="form-group col-lg-2 col-md-2"></div>
										<div class="form-group col-lg-8 col-md-8">
											<div class="input-group err">
												<div class="custom-file">
													<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="category_image" accept="image/x-png,image/gif,image/jpeg">
													<label class="custom-file-label">Choose Category Image</label>
												</div>
											</div>
											<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
												<img id="blah" style="width:100%;height:200px" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close( ); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

$(document).ready(function() {
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Please enter a valid Zip Code.");
	$.validator.addMethod("letters_numbers_special", function(value, element) {
    	return this.optional(element) || /^[a-zA-Z0-9!@#$&()` .+,/"-]*$/i.test(value);
	//(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
	}, "");
  	$('#quickForm').validate({	  
		rules: {			
		category_name: {
			required: true
		},
		category_description: {
			required: true
		},
		category_image: {
			required: true
		},
		},
		messages: {
		category_name: {
			required: "Please enter Category Name"
		},
		category_description: {
			required: "Please enter Category Description"
		},
		category_image: {
			required: "Please upload Category Image"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
});
</script>