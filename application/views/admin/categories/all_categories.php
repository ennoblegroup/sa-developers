
<!--**********************************
	Content body start
***********************************-->
<style>
.ratings i {
    color: green
}

.install span {
    font-size: 12px
}

.col-md-4 {
    margin-top: 27px
}

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0);
  transition: background 0.5s ease;
}

.ttt:hover .overlay {
  display: block;
  background: rgba(0, 0, 0, .3);
}


.title {
  position: absolute;
  width: 500px;
  left: 0;
  top: 120px;
  font-weight: 700;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  z-index: 1;
  transition: top .5s ease;
}

.ttt:hover .title {
  top: 90px;
}

.button {
  position: absolute;
  left:35%;
  top: 30%;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {

}

.ttt:hover .button {
  opacity: 1;
}
.card {
    height: auto;
}
.ttt h4 {
  white-space: nowrap; 
  width: auto; 
  overflow: hidden;
  text-overflow: ellipsis;
}

.ttt h4:hover {
  overflow: visible;
}
#qw li {
	list-style: disc;
    font-size: 15px;
}
.jj {
    text-decoration: underline;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Categories</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/categories/add'); ?>" ><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Category</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>										
										<th>S No</th>
										<th>Product Category</th>
										<th>Category Image</th>											
										<th>Actions</th>										
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_categories as $row): ?>
									<tr>
										<td><?= $i; ?></td>
										<td><?= $row['category_name']; ?></td>
										<td>
											<a href="<?= base_url() ?>images/material_categories/<?= $row['file']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
											<img src="<?= base_url() ?>images/material_categories/<?= $row['file']; ?> " style="width: 100px;height: 40px;" alt="<?= $row['file']; ?>"> 
                                            </a>
										</td>
										<td> 
											<a data-toggle="tooltip"  title="Edit Category"  category_id = "<?php echo $row['id'];?>"  href="#" class="btn btn-sm btn-outline-dark edit_category"><i class="fa fa-pencil"></i></a>
											<a data-toggle="tooltip" title="View Vendor List" href="#"   category_id = "<?php echo $row['id'];?>" class="vendor_list btn btn-sm btn-outline-dark "><i class="fa fa-eye"></i></a>
											<a data-toggle="tooltip" title="Delete" href="#" id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark "><i class="fa fa-trash-o"></i></a>

										</td>
									</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="vend">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title eew">Vendors List</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<ul id="qw">
								
								</ul>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add category</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open_multipart(base_url('admin/categories/add'), 'id="quickForm"  class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="category_name" placeholder="category Name" name="category_name">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" placeholder="Description" name="category_description"></textarea>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="input-group err">
										<div class="custom-file">
											<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="category_image" accept="image/x-png,image/gif,image/jpeg">
											<label class="custom-file-label">Choose Category Image</label>
										</div>
									</div>
									<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
										<img id="blah" style="width:100%;height:200px" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
									</div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<div class="modal fade" id="editcategory">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit category</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open_multipart(base_url('admin/categories/edit'), 'id="quickForm1"  class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="ecategory_name" placeholder="category Name" name="category_name">
									<input type="hidden"  name="category_id" id="category_id" value="">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" placeholder="Description" id="ecategory_description" name="category_description"></textarea>
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<div class="input-group err">
										<div class="custom-file">
											<input type="file" onchange="readURL1(this);" class="custom-file-input " id="image" name="category_image" accept="image/x-png,image/gif,image/jpeg">
											<label class="custom-file-label">Choose Category Image</label>
										</div>
									</div>
									<div class="" style="border: 1px solid #bebaba; margin-top: 10px;">
										<img id="blahh" style="width:100%;height:200px" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
									</div>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Update</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$(".vendor_list").click(function(){
	var id = $(this).attr('category_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/categories/get_category_vendor');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#qw').html(data);		
		}
	});
    $('#vend').modal('show');
});
$(".edit_category").click(function(){
	var id = $(this).attr('category_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/categories/get_category_by_id');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#category_id').val(data.id);
			$('#ecategory_name').val(data.category_name);
			$('#ecategory_description').text(data.category_description);
			$('#blahh').attr('src','<?= base_url() ?>images/material_categories/'+data.file+' ?>');
			//location.reload();$('#my_image').attr('src','second.jpg');
		}
	});
    $('#editcategory').modal('show');
});
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
function readURL1(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blahh')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/categories/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});

$(document).ready(function () {
	$.validator.addMethod("zip_regex", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Please enter a valid Zip Code.");
	$.validator.addMethod("letters_numbers_special", function(value, element) {
    	return this.optional(element) || /^[a-zA-Z0-9!@#$&()` .+,/"-]*$/i.test(value);
	//(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
	}, "");
  	$('#quickForm').validate({	  
		rules: {			
		category_name: {
			required: true
		},
		category_description: {
			required: true
		},
		category_image: {
			required: true
		},
		},
		messages: {
		category_name: {
			required: "Please enter Category Name"
		},
		category_description: {
			required: "Please enter Category Description"
		},
		category_image: {
			required: "Please upload Category Image"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
	$('#quickForm1').validate({	  
		rules: {			
		category_name: {
			required: true
		},
		category_description: {
			required: true
		},
		},
		messages: {
		category_name: {
			required: "Please enter Category Name"
		},
		category_description: {
			required: "Please enter Category Description"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
});
</script>
