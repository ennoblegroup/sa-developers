<?php echo form_open(base_url('admin/homeservices/edit/'.$homeservices[0]['id']), 'id="quickForm" enctype="multipart/form-data"  class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/homeservices">Services</a></li>
						<li class="breadcrumb-item active">Edit Service</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<input type="submit" name="submit" value="Update" class="btn btn-outline-dark">&nbsp;
					<a type="button" href="<?php echo base_url()?>admin/homeservices" class="form btn btn-outline-dark">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; 
									$i=1; foreach ($homeservices as $key=>$srv){ ?>
									<div class="row">	
										<input type="hidden" value="<?php echo $srv['id']; ?>" name="homeserviceid">															
										<div class="form-group err col-lg-6 col-md-6">
											<input type="text" class="form-control"  value="<?php echo $srv['title']?>" id="title" placeholder=" Title" name="title" >
										</div>																
										<div class="form-group col-lg-12 col-md-12">
										  	<textarea class="editor" name="description"  id="description" required>
											  <?php echo $srv['description']?>
											</textarea>
										</div>
										<div class="col-lg-4 col-md-4"></div>
										<div class="col-lg-4 col-md-4">
											<div class="input-group err row">
												<div class="custom-file">
													<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg">
													<label class="custom-file-label">Choose file</label>
													
												</div>
											</div>
											<!--div class="row">
												<img id="blah" style="padding-top:15px;width:100%" src="<?php echo base_url();?>images/homeservices/thumb/<?php echo $srv['image']; ?>" alt="your image" />  
											</div-->
											<div class="row">
												<img style="max-width: 100%;" src="<?php echo base_url();?>images/homeservices/<?php echo $srv['image']; ?>">
											</div>
                                     	</div>										
									</div> 
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
<?php echo form_close( ); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}


$(document).ready(function () {
  
  $('#quickForm').validate(
	tinyMCE.triggerSave();
	{
    rules: {
		title: {
        required: true,
        minlength: 3
      },	 
      description: {
        required: true,
        minlength: 3
      }
    },
    messages: {
	  title: {
        required: "Please enter  Title",
        minlength: "Title must be at least 3 characters long"
      },
      description: {
        required: "Please enter  Description",
        minlength: "Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.err').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });	
});
</script>