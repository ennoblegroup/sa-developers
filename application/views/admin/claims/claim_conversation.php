
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.css" />	
<style>
.card {
    height: auto;
}
.email-right-box {
    padding-left:0 !important;
    padding-right:0 !important;
}
.dropzone .dz-preview .dz-progress {
    display:none;
}
hr {
    margin-top: 10px;
    margin-bottom: 10px;
}
.read-content-body p {
    margin-bottom: 10px;
}
.read-content-attachment {
    padding: 10px 0;
}
.read-content-attachment h6 {
    font-size: 1rem;
}
</style>
<div class="mini-header">
	<div class="">
        <div class="row page-titles mx-0">
            <div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item"><a href="<?= base_url('admin/claims'); ?>">Claims</a></li>
						<li class="breadcrumb-item active">Claim Conversation</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
                    <button id="imgsubbutt" class="btn btn-outline-dark " type="button">Send</button>
				</div>
			</div>
        </div>
	</div>
</div>
<div class="content-body">
    <div class="container-fluid">
        <!-- row -->
        <div class="row">
            <div class="col-lg-12 custom_body">
                <?php $i=1; foreach($conversation as $row): ?>
                <div class="card">
                    <div class="card-body">
                        <div class="email-right-box ml-0 ml-sm-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="right-box-padding">
                                        <div class="read-content">
                                            <div class="media">
                                                <div class="media-body"><span class="pull-right"><?= $row['time']; ?></span>
                                                    <h5 class="my-1 text-primary"><?= $row['lastname']; ?> <?= $row['firstname']; ?></h5>
                                                </div>
                                            </div>
                                            <hr>
                                            <?php if($row['description'] !=''){?>
                                                <div class="read-content-body">
                                                    <?= $row['description']; ?>
                                                    <hr>
                                                </div>
                                            <?php }?>
                                            <?php if(count($row['attachments'])>0){?>
                                            <div class="read-content-attachment">
                                                <h6><i class="fa fa-download mb-2"></i> Attachments
                                                    <span>(<?php echo count($row['attachments'])?>)</span></h6>
                                                    <div class="row attachment">
                                                    <?php $i=1; foreach($row['attachments'] as $att): ?>
                                                    <div class="col-auto">
                                                        <a href="<?php echo base_url();?>uploads/<?php echo $att['file_name']; ?>" class="text-muted"><?= $att['file_name']; ?></a>
                                                    </div>
                                                    <?php $i++; endforeach; ?>
                                                </div>
                                            </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
                <div class="card">
                    <div class="card-body">
                        <div class="email-right-box ml-0 ml-sm-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="right-box-padding">
                                        <div class="read-content">
                                            <div class="form-group pt-3">
                                                <div class="row">
                                                    <div class="form-group col-lg-12 col-md-12">
                                                        <textarea class="editor" id="description" name="message"></textarea>
                                                        <input type="hidden" value="<?php echo $claim_id;?>" id="claim_id">
                                                    </div>
                                                    <div class="form-group col-lg-12 col-md-12">
                                                        <div class="dropzone" ></div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <div class="text-right btn-sl-sm mb-5">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/admin/vendor/dropzone/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;

var myDropzone = new Dropzone('.dropzone', {
  url: "../dragDropUpload", 
  addRemoveLinks: true,                       
  autoProcessQueue: false,
});

$('#imgsubbutt').click(function(){
    var message= tinymce.get("description").getContent();
    var claim_id= $('#claim_id').val();
    var count= myDropzone.files.length;
    if(message !='' || count>0){
        $.ajax({
            url:"../add_claim_conversation",
            method:"POST",
            async: true,
            dataType:"json",
            data:{message: message,claim_id:claim_id},
            success: function(data)
            {
            var out =JSON.parse(data);
            myDropzone.on("sending", function(file, xhr, data) {
                data.append("claim_conversation_id", out);
            });
            myDropzone.processQueue();  
            },
            error: function (data) {
            
            },
            complete: function (data) {
                location.reload();
            }
        })
    }else{
        
    }
});
</script>