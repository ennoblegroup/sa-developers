
<style>
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: -webkit-fill-available;
}
.dataTables_scroll {
   padding: 0;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 5px 5px;
    border-bottom: 1px solid #111;
}
#claim_table thead{
	display:none;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 5px !important;
}
.bootstrap-select .btn {
    border: 1px solid #eaeaea !important;
    background-color: #f8f9fa !important;
    padding: 5px 5px;
}
hr {
    margin-top: 0px;
    margin-bottom: 5px;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
}
.external-event {
    cursor: move;
    margin: 0;
    padding:0;
}
.blocked
{
	pointer-events: none;
}
.blocked button:hover {
    cursor:not-allowed
 }
.hide{
	display:none;
}
td .dropdown-menu.show{
	display:flex;
	right: 0px;
    left: unset !important;
}
td .dropdown-menu.show a{
	padding:0px 3px;
}
td .custom-dropdown .dropdown-menu {
    background: #e7e3e3;
	border: 1px solid #8b8383;
}
.widget-timeline .timeline > li > .timeline-panel {
    padding: 5px 5px;
}
.widget-timeline .timeline > li > .timeline-panel a h6{
    font-size: 13px;
}
#status_history .modal-dialog {
    max-width: 700px;
    margin: 1.75rem auto;
}
.widget-timeline .timeline:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 2px;
    left: 12px;
    margin-right: -1.5px;
    background: #f5f5f5;
}
.widget-timeline .timeline > li > .timeline-badge {
    border-radius: 50%;
    height: 15px;
    left: 5px;
    position: absolute;
    top: 10px;
    width: 15px;
}
.widget-timeline .timeline > li > .timeline-panel {
    left: -10px;
}
[data-id="status_id"]  {
    display:none !important;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumcb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrucb-item active">All Claims</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
					<a href="<?= base_url('admin/claims/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Claim</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL CLAIMS</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="row" style="">																
							<div class="form-group phar col-lg-2 col-md-2" <?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo 'style="display:none"';}?> >
								<select name="client_id[]" multiple id="client_id" title="All clients" <?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo '';}?> data-live-search="true"  class="selectpicker form-control" >
									<?php $i=1; foreach ($all_clients as $key=>$phar){ ?>
									<option value="<?php echo $phar['id']?>" <?php if($phar['id']== $client_id || $phar['id']==$_SESSION['sadevelopers_admin']['client_id']){ echo 'selected'; }?>><?php echo $phar['client_lbn_name']?></option>
									<?php $i++; }?>
								</select>
							</div>

							<div class="form-group col-lg-3 col-md-3">
								<select name="insurance_company_id[]" multiple="multiple"  title="All Insurance Companies" data-live-search="true" id="insurance_company_id" class="selectpicker form-control" >
									<?php $i=1; foreach ($all_insurance_companies as $key=>$phar){ ?>
									<option value="<?php echo $phar['id']?>"><?php echo $phar['insurance_company_name']?></option>
									<?php $i++; }?>
								</select>
							</div>
							<div class="form-group col-lg-2 col-md-2">
								<select name="status_id[]" multiple="multiple" title="All Status" id="status_id" style="display:none" data-live-search="true" class="selectpicker form-control" >
								<optgroup label="Claim Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==1){?>
										<option cg="1" value="<?php echo $cs['claim_status_id']?>"><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>
								<optgroup label="Transaction Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==2){ ?>
										<option  cg="2" value="<?php echo $cs['claim_status_id']?>" <?php if($cs['claim_status_id']==8){echo 'selected';}?>><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>		
								<optgroup label="Payment Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==3){?>
										<option cg="3" value="<?php echo $cs['claim_status_id']?>"><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>
								</select>
							</div>
							<div class="form-group col-lg-3 col-md-3">
							<input type="text" class="form-control daterange" name="datefilter" style="display:none" id="dates" placeholder="Date Range" >
							</div>
							<?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo '<div class="form-group col-lg-2 col-md-2"></div>';}?>
							<div class="form-group col-lg-2 col-md-2">
							<a style="float:right" href="javascript:void(0)" onclick="reset_data()"  ><button type="button" class="btn btn-outline-dark">Reset</button></a>
							<a style="float:right;margin-right: 10px;" href="javascript:void(0)" onclick="filter_data(0)"  ><button type="button" class="btn btn-outline-dark">Apply Filter</button></a>
							</div>
						</div>
						<hr>
						<div class="">
							<table id="claim_table" class="display">
								<thead>
									<tr>
										<th scope="col">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkAll">
												<label class="custom-control-label" for="checkAll"></label>
											</div>
										</th>
										<th scope="col">Claim Id</th>
										<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
										<th scope="col">client</th>
										<?php }?>
										<th scope="col">Insurance</th>
										<th scope="col">Patient</th>
										<th scope="col">Report</th>
										<th style='text-align:right' scope="col">Actions</th>
									</tr>
								</thead>
								<tbody id="filter_data">
																	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Update Claim Status</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form  method="POST" action="<?= base_url('admin/claims/add_claim_status_history'); ?>" id="claim_status_update" class="claim_status_update">
			<div class="modal-body">
				<div class="row">																
					<div class="col-md-6 form-group">
						<select name="status_group" id="status_group" class="form-control selectpicker" >
							<option value="">Select Status Group</option>
							<option value="1">Claim Status</option>
							<option value="2">Transactional Status</option>
							<option value="3">Payment Status</option>
						</select>
						<input type="hidden" value="" id="claim_id" name="claim_id">
						<input type="hidden" value="" id="sid" name="sid">
						<input type="hidden" value="" id="tid" name="tid">
						<input type="hidden" value="" id="pid" name="pid">
					</div>
					<div class="col-md-6 form-group">
						<select name="status_name" id="status_name" class="form-control selectpicker" >

						</select>
					</div>
					<div class="col-md-12 form-group">
						<textarea class="form-control" name="status_description" placeholder="Status Description"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-dark">Update Status</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="status_history">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status History</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">																
					<div class="col-md-4 form-group">
						<h6 class="">Claim Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s1">
								
							</ul>
						</div>						
					</div>
					<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
					<div class="col-md-4 form-group">
						<h6 class="">Transaction Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s2">
								
							</ul>
						</div>						
					</div>
					<div class="col-md-4 form-group">
						<h6 class="">Payment Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s3">
								
							</ul>
						</div>						
					</div>
					<?php }?>
				</div>
								
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		/* Populate data to state dropdown */
		$('#status_group').on('change',function(){
			var id = $(this).val();
			var sid=$('#sid').val();
			if(id){
				$.ajax({
					type:'POST',
					url:'claims/get_all_claim_status',
					data:'id='+id,
					success:function(data){
						
						$('#status_name').html('<option value="">Select Status</option>'); 
						var dataObj = jQuery.parseJSON(data);
						if(dataObj){
							$(dataObj).each(function(){
								var option = $('<option />');
								option.attr('value', this.claim_status_id).text(this.claim_status_name);
								if(id==1 && (this.claim_status_id==0 || this.claim_status_id ==1)){
									option.attr('disabled', 'disabled');
								}
								$('#status_name').append(option);
							});
							$("#status_name").selectpicker("refresh");
						}else{
							$('#status_name').html('<option value="" selected disabled>Status not available</option>');
							$("#status_name").selectpicker("refresh");
						}
						$("#status_name").selectpicker("refresh");
					}
				}); 
			}else{
				$('#status_name').html('<option value="">Select Status Group first</option>');
				$("#status_name").selectpicker("refresh");
			}
			
		});
		
		
	});
	function openUpdateStatus(id,sid,tid,pid) {
		$("#claim_id").val(id);
		$("#sid").val(sid);
		$("#tid").val(tid);
		$("#pid").val(pid);
		$('#exampleModalLong').modal('show');
	}
	function qwert(id){
		openStatusHistory(id,1);
		openStatusHistory(id,2);
		openStatusHistory(id,3);
		$('#status_history').modal('show');
	}
	function openStatusHistory(id,type) {
		$.ajax({
            url:"claims/claim_status_history",
            method:"POST",
            async: true,
            dataType:"json",
            data:{id:id,type:type},
            success: function(data)
            {
				
                $('.s'+type).html(data.claim_list);
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
		
	}
	function reset_data()
    {
		$('#insurance_company_id').val('');
		$('#client_id').val('');
		$('#dates').val('');
		filter_data();
	}
	
	function filter_data()
    {
		$("#preloader").css("display","block");
		var insurance_company_id =$('#insurance_company_id option:selected').toArray().map(item => item.value);
		insurance_company_id = $.grep(insurance_company_id,function(n){
			return(n);
		});
		var client_id = $('#client_id option:selected').toArray().map(item => item.value);
		client_id = $.grep(client_id,function(n){
			return(n);
		});
		var status_id = $('#status_id option:selected').toArray().map(item => item.value);
		status_id = $.grep(status_id,function(n){
			return(n);
			
		});
        var dates=$('#dates').val();
		if(insurance_company_id.length==0){
			insurance_company_id=[].toString();
		}
		if(client_id.length==0){
			client_id=[].toString();
		}
		if(status_id.length==0){
			status_id=[].toString();
		}
        $.ajax({
            url:"filter_claims_rs",
            method:"POST",
            async: true,
            dataType:"json",
            data:{insurance_company_id:insurance_company_id,client_id:client_id,status_id:status_id,dates:dates},
            success: function(data)
            {
                $('#claim_table').DataTable().clear();
				$('#claim_table').DataTable().destroy();			
				$('#filter_data').html(data.claim_list);				
				$('#claim_table').DataTable({
					scrollY:        "300px",
					scrollX:        false,
					"bRetrieve": true,
					"bProcessing": true,
					"stateSave": true,
					"destroy": true,
					"paginate": true,
					"autoWidth": true,
					"bFilter": true,
					"bInfo": true,
					"bJQueryUI": true
				});
				$('#claim_table').columns.adjust().draw();
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
		$("#preloader").css("display","none");
	}
	
</script>