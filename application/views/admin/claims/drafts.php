
<style>
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: -webkit-fill-available;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 5px 5px;
    border-bottom: 1px solid #111;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 5px !important;
}
.bootstrap-select .btn {
    border: 1px solid #eaeaea !important;
    background-color: #f8f9fa !important;
    padding: 5px 5px;
}
hr {
    margin-top: 0px;
    margin-bottom: 5px;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
}
.external-event {
    cursor: move;
    margin: 0;
    padding:0;
}
.blocked
{
	pointer-events: none;
}
.blocked button:hover {
    cursor:not-allowed
 }
.hide{
	display:none;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumcb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrucb-item active">All Drafts</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<a href="<?= base_url('admin/claims/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Claim</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL CLAIMS</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="">
							<table class="claim_table display">
								<thead>
									<tr>
										<th scope="col">Claim Id</th>
										<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
										<th scope="col">client</th>
										<?php }?>
										<th scope="col">Insurance</th>
										<th scope="col">Patient</th>
										<th scope="col">Actions</th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; foreach($all_claims as $row): ?>
									<tr>
										<td><?= $row['claim_registration_id']; ?></td>
										<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
										<td><?= $row['client_lbn_name']; ?></td>
										<?php }?>
										<td><?= $row['insurance_company_name']; ?></td>
										<td><?= $row['last_name']; ?> <?= $row['first_name']; ?></td>
										<td>
                                        <a  title='Edit Claim' href=<?= base_url('admin/claims/edit/'.$row['claimm_id']); ?> ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-pencil' aria-hidden='true'></i></button></a>							
										<a data-toggle="tooltip" title="Delete" href="#" c_id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark "><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php $i++; endforeach; ?>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
 $(".removeRecord").click(function(){
       var id = $(this).attr('c_id');    	
    	$.confirm({
        title: 'Are you sure?',
        content: 'Do you really want to delete these records? This process cannot be undone.',
        buttons: {
            confirm: {
               text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){			
    					$.ajax({
    						type:'POST',
    						url:"<?php echo base_url('admin/claims/del_draft');?>",
    						data:{'id':id},
    						success: function(data){
    							location.reload();    							 
    							}
    						});
    					}
            },
            cancel: {
               text: '</i>cancel',
            btnClass: 'btn btn-outline-dark',
                keys: ['enter', 'shift'],
                action: function(){
    				}
    			}
    		}
    	});
    });
</script>