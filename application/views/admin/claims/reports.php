<style>
	.external-event {
		cursor: move;
		margin: 0;
		padding: 0;
	}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item "><a href="<?= base_url('admin/claims'); ?>">Claims</a></li>
						<li class="breadcrumb-item active">Claim Reports</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Claim Reports</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive recentOrderTable">
							<table id="example3" class="claim_tablep display">
								<thead>
									<tr>
										<th scope="col">Claim Id</th>
										<th scope="col">client Name</th>
										<th scope="col">Insurance Company</th>
										<th scope="col">Generated On</th>
										<th scope="col">Status</th>
										<th scope="col">Report</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_reports as $row){ 
                                    // Declare and define two dates
									$date1 = strtotime($row['create_date']);
									//  $date2 = strtotime("2019-06-21 22:45:00");
									$datee = new DateTime();
									$datee->setTimezone(new DateTimeZone('Asia/Calcutta'));
									$date2 = strtotime($datee->format('Y-m-d H:i:s'));
									
									// Formulate the Difference between two dates
									$diff = abs($date2 - $date1);
									
									// To get the year divide the resultant date into
									// total seconds in a year (365*60*60*24)
									$years = floor($diff / (365 * 60 * 60 * 24));
									
									// To get the month, subtract it with years and
									// divide the resultant date into
									// total seconds in a month (30*60*60*24)
									$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
									
									// To get the day, subtract it with years and
									// months and divide the resultant date into
									// total seconds in a days (60*60*24)
									$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
									
									// To get the hour, subtract it with years,
									// months & seconds and divide the resultant
									// date into total seconds in a hours (60*60)
									$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
									
									// To get the minutes, subtract it with years,
									// months, seconds and hours and divide the
									// resultant date into total seconds i.e. 60
									$minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
									
									// To get the minutes, subtract it with years,
									// months, seconds, hours and minutes
									$seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
									
									if ($years > 0) {
										$generated_on = $years . " years ago";
									} else if ($months > 0) {
										$generated_on = $months . " months ago";
									} else if ($days > 0) {
										$generated_on = $days . " days ago";
									} else if ($hours > 0) {
										$generated_on = $hours . " hours ago";
									} else if ($minutes > 0) {
										$generated_on = $minutes . " minutes ago";
									} else if ($seconds > 0) {
										$generated_on = $seconds . " seconds ago";
									} else {
										$generated_on = "just now";
									}
                                    ?>
									<tr>
										<td><?= $row['claim_registration_id']; ?></td>
										<td>
										<?= $row['client_lbn_name']; ?>
										</td>
										<td><?= $row['insurance_company_name']; ?></td>
										<td><?= $generated_on ; ?> </td>
										<td>
											<div id='external-events'>
												<?php if($row['status_id']==1){?>
													<div class='external-event' data-class='bg-success'><i class='fa fa-move'></i>Active</div>
												<?php } else {?>
													<div class='external-event' data-class='bg-danger'><i class='fa fa-move'></i>Inactive</div>
												<?php }?>
											</div>
                                        </td>
										<td>
                                            <a title="View Report" href="<?= base_url() ?>admin_files/generated_claims/<?= $row['file_name']; ?>"" target="_blank">
											<button type="button" class="btn btn-sm btn-square btn-outline-dark"><i class="fa fa-file-pdf-o"></i></button></a>
										</td>
									</tr>
									<?php $i++; } ?>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
