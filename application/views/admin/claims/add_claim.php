<link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
<link href="https://cdn.datatables.net/1.10.22/css/dataTables.semanticui.min.css">
<style>
	[data-headerbg="color_2"][data-theme-version="dark"] .header, [data-headerbg="color_2"] .header
{
	background-color:#9fa8a3 !important;
}
[data-headerbg="color_2"][data-theme-version="dark"] .nav-control, [data-headerbg="color_2"] .nav-control
{
	background-color:#9fa8a3 !important;
}
[data-sidebar-style="full"][data-layout="vertical"] .menu-toggle .nav-header .nav-control .hamburger .line
{
	background: #2c3f4c !important;
}
[data-nav-headerbg="color_2"][data-theme-version="dark"] .nav-header, [data-nav-headerbg="color_2"] .nav-header
{
	background-color:#2c423be6 !important;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu > li > a, [data-sibebarbg="color_2"] .deznav .metismenu > li > a
{
	color:#ffffff  !important;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav, [data-sibebarbg="color_2"] .deznav
{
	background-color: #2c423b;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu > li:hover > a, [data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu > li:focus > a, [data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu > li.mm-active > a, [data-sibebarbg="color_2"] .deznav .metismenu > li:hover > a, [data-sibebarbg="color_2"] .deznav .metismenu > li:focus > a, [data-sibebarbg="color_2"] .deznav .metismenu > li.mm-active > a
{
  background-color: #ffffff;
  color: #000000e8 !important;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu ul, [data-sibebarbg="color_2"] .deznav .metismenu ul
{
	background-color: #ffffff;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu > li.mm-active, [data-sibebarbg="color_2"] .deznav .metismenu > li.mm-active {
    background-color: #fff;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu a, [data-sibebarbg="color_2"] .deznav .metismenu a
{
	color:#000000e8    !important;
}
[data-sibebarbg="color_2"][data-theme-version="dark"] .deznav .metismenu .nav-label, [data-sibebarbg="color_2"] .deznav .metismenu .nav-label
{
	color:#ffffff  !important;
}

.form-group {
    padding: 0px 5px;
}
.btttn td {
    border:none !important
}
h6{
	margin-left: -10px;
}

table th, table td {
	padding:5px !important;
	border-left: 1px solid #EEEEEE !important; */
    border-right: 1px solid #EEEEEE !important;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 7px 5px;
    border-bottom: 1px solid #111;
    font-size: 13px;
}
table.dataTable tbody th, table.dataTable tbody td {
    padding: 7px 5px;
}
#confirmmodel .modal-dialog {
    width: 100%;
    max-width: 100%;
    margin: 15px;
}
#confirmmodel .modal-content {
	width: 99%;
}
fieldset h4 {
    font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:5px 0px;
	border-bottom:none;
	text-decoration: underline;
}
fieldset label{
	font-weight:bold;
	font-size: 13px;
	margin: 0.25rem 0;
	text-transform: capitalize;
}
.preview{
	font-size: 13px;
	text-transform: capitalize;
}
legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:0 10px;
	border-bottom:none;
}
.preview_col{
	padding:0px;
	border-bottom: 1px solid #ccc2c2;
}
.box-row { 
	display:table-row; 
} 
 .box-cell { 
	display:table-cell; 
	width:50%; 
	padding:10px; 
} 
 .box-cell.box1 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	} 
 .box-cell.box2 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
} 
.uuu{
	padding:0 5px;
}

.form-group .clearfix {
    font-size: 13px;
    padding: 0px;
    top: 7px;
    position: relative;
}
.eer .bootstrap-select .btn {
    border: 1px solid #c0bcbc !important;
    padding: 0.375rem 1rem; }
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?= base_url('admin/claims'); ?>">Claims</a></li>
						<li class="breadcrumb-item active">Add Claim</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row ">
			<div class="col-xl-12 col-xxl-12 ">
				<div class="card custom_body">
					<div class="card-body">
						<form action="<?= base_url('admin/claims/add_claim'); ?>" multipart id="step-form-horizontal" method="POST" class="xstep-form-horizontal">
							<span class="row eer" style="margin-bottom:10px;">
								<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
								<div class="form-group col-lg-3 col-md-3"></div>																
								<div class="form-group col-lg-3 col-md-3">
									<select name="client_id" id="client_id" class="form-control selectpicker" data-live-search="true" title="Select client">
										<option value="">Select client</option>
										<?php $i=1; foreach ($all_clients as $key=>$phar){ ?>
										<option value="<?php echo $phar['id']?>"><?php echo $phar['client_lbn_name']?></option>
										<?php $i++; }?>
									</select>
								</div>
								<div class="form-group col-lg-3 col-md-3">
								<?php } else {?>
								<div class="form-group col-lg-4 col-md-4"></div>
								<div class="form-group col-lg-4 col-md-4">
									<input type="hidden" value="<?php echo $_SESSION['sadevelopers_admin']['client_id'];?>" name="client_id">
								<?php }?>	
									<select name="insurance_company_id" id="insurance_company_id" class="form-control selectpicker" data-live-search="true" title="Select Insurance Company">
										<option value="">Select Insurance Company</option>
										<?php $i=1; foreach ($all_insurance_companies as $key=>$phar){ ?>
										<option value="<?php echo $phar['ic_id']?>"><?php echo $phar['insurance_company_name']?></option>
										<?php $i++; }?>
									</select>
								</div>
							</span>	
							<div>								
								<h4>PATIENT/BENEFICIARY INFORMATION</h4>
								<section>
									<div class="row" data-step="1">
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="last_name" placeholder="Patient's Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="last_name" maxLength="15">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="first_name" placeholder="Patient's First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="first_name" maxLength="15">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="middle_name" placeholder="Patient's Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="middle_name" maxLength="15">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control us-date" id="patient_dob" placeholder="Patient's Date of Birth" name="patient_dob">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="input-group">
												<input type="text" class="form-control us_phone"  id="phone_number" placeholder="Phone Number" name="phone_number" >
											</div>
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="form-group clearfix">
												Sex&nbsp;:&nbsp;
											  	<div class="icheck-success d-inline"  >
													<input type="radio" name="gender" value="male" checked id="radioSuccess1">
													<label for="radioSuccess1">Male
													</label>
											  	</div>
											  	<div class="icheck-success d-inline" style="margin-right: 12px;margin-left: 12px;">
													<input type="radio" name="gender" value="female" id="radioSuccess2">
													<label for="radioSuccess2">Female
													</label>
											  	</div>
											  	<div class="icheck-success d-inline">
													<input type="radio" name="gender"  value="other">
													<label for="radioSuccess2">Other
													</label>
												</div>
											</div>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="address1" placeholder="Address 1" name="address1" maxLength="28">
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="address2" placeholder="Address 2" name="address2" maxLength="20">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="city" placeholder="City" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="city" maxLength="24">
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select placeholder="State" class="selectpicker form-control" data-live-search="true" name="state" id="state">
												<option value="">Select State</option>
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text"  class="form-control us_zip" id="zip_code" placeholder="Zip Code" name="zip_code" >
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text"  class="form-control" id="account_number" maxlength="14" placeholder="Account Number" onkeypress="allowAlphaNumericSpace(event)" name="account_number" >
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<div class="form-group clearfix">
											Patient relationship to Insured&nbsp;:&nbsp;
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio" name="relation" checked value="self">
												<label for="pri1">Self
												</label>
											  </div>
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio" name="relation" value="spouse">
												<label for="pri2" >Spouse
												</label>
											  </div>
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio" name="relation" value="child">
												<label for="pri3" >Child
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" name="relation" value="other">
												<label for="pri4" >Other
												</label>
											  </div>
											</div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="custom-file">
                                                <input type="file" name="patient_signature" id="patient_signature" class="custom-file-input">
                                                <label class="custom-file-label">Patient's Signature</label>
                                            </div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="custom-file">
                                                <input type="file" name="insured_signature" id="insured_signature" class="custom-file-input">
                                                <label class="custom-file-label">Insured's Signature</label>
                                            </div>
										</div>
									</div>
								</section>
								<h4>INSURANCE INFORMATION</h4>
								<section>
									<div class="row" data-step="2">
										<div class=" col-lg-8 col-md-8">
											<div class="form-group">
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" checked name="insured_check" value="MEDICARE" >
													<label class="form-check-label" for="check1">MEDICARE</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check"  value="Medicaid">
													<label class="form-check-label" for="check2">Medicaid</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check"  value="TRICARE" >
													<label class="form-check-label" for="check1">TRICARE</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check" value="CHAMPVA">
													<label class="form-check-label" for="check2">CHAMPVA</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check"  value="Group Health Plan" >
													<label class="form-check-label" for="check1">Group Health Plan</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check"  value="FECA/Black Lung">
													<label class="form-check-label" for="check2">FECA/Black Lung</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" name="insured_check"  value="Other">
													<label class="form-check-label" for="check2">Other</label>
												</div>
											</div>
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="form-group clearfix" style="padding-top:0px;">
											Is there another health benefit plan?
											  <div class="icheck-success d-inline">
												<input type="radio" name="other_benefit_plan" value="yes" checked>
												<label for="radioSuccess1">Yes
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" name="other_benefit_plan"  value="no">
												<label for="radioSuccess2">No
												</label>
											  </div>
											</div>
										</div>
										<div class="col-lg-8 col-md-8">
											<h6>Insured's Information:</h6>
											<div class="row">
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_last_name" placeholder="Insured's Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="insured_last_name" maxLength="25">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_first_name" placeholder="Insured's First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="insured_first_name" maxLength="25">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_middle_name" placeholder="Insured's Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="insured_middle_name" maxLength="25">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_id_number" placeholder="Insured's ID Number"  onkeypress="allowAlphaNumeric(event)" name="insured_id_number" maxLength="29">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us-date" name="insured_dob" id="insured_dob" placeholder="Insured's Date of Birth" >
												</div>
												<div class="form-group col-lg-4 col-md-3">
													<input type="text" class="form-control us_phone" id="insured_phone_number" placeholder="Insured's Phone Number"   name="insured_phone_number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<div class="form-group clearfix" style="font-size: 13px;padding-top:0px;top: 7px;position: relative;">
													Sex&nbsp;:&nbsp;&nbsp;
													<div class="icheck-success d-inline"  >
														<input type="radio" name="insured_gender" checked id="radoSuccess1" value="male">
														<label for="radioSuccess1">Male
														</label>
													</div>
													<div class="icheck-success d-inline" style="margin-right: 12px;margin-left: 12px;">
														<input type="radio" name="insured_gender"  value="female">
														<label for="radioSuccess2">Female
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" name="insured_gender"  value="other">
														<label for="radioSuccess2">Other
														</label>
													</div>
													</div>
												</div>
												<div class="form-group col-lg-6 col-md-12">
													<input type="text" class="form-control" id="insured_address1" placeholder="Address 1" name="insured_address1" maxLength="29">
												</div>
												<div class="form-group col-lg-6 col-md-12">
													<input type="text" class="form-control" id="insured_address2" placeholder="Address 2" name="insured_address2" maxLength="20">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insured_city" placeholder="City" onkeypress="return (event.charCode == 32)||(event.ch rCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="insured_city" maxLength="23">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select type="text" placeholder="State" class="form-control selectpicker"  data-live-search="true" name="insured_state" id="insured_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip"  id="insured_zip_code" placeholder="Zip Code" name="insured_zip_code" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="claim_id" maxlength="30" placeholder="Claim ID (Designated by NUCC)" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  name="claim_id" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insured_group_number" placeholder="Insured's Policy Group/FECA number" onkeypress="allowAlphaNumeric(event)"  name="insured_group_number" maxlength="29">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insurance_plan_name" placeholder="Insurance Plan Name/Program Name" onkeypress="allowAlphaNumericSpace(event)"   name="insurance_plan_name" >
												</div>
											</div>		
										</div>
										<div class=" col-lg-4 col-md-4">
											<h6>Other Insured's Information:</h6>		
											<div class="row otii">			
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_last_name" placeholder="Other Insured's Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  name="other_insured_last_name" maxLength="25">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_first_name" placeholder="Other Insured's First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  name="other_insured_first_name" maxLength="25">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_middle_name" placeholder="Other Insured's Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  name="other_insured_middle_name" maxLength="25">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insurance_plan_name" placeholder="Other Insurance Plan Name/Program Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  name="other_insurance_plan_name" maxLength="28">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_group_number" placeholder="Other Insured's Policy/Group Number" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  name="other_group_number" maxlength="28">
												</div>
											</div>
										</div>							
									</div>
								</section>
								<h4>PHYSICIAN/SUPPLIER INFORMATION</h4>
								<section>
									<div class="row">
										<div class="form-group col-lg-12 col-md-12">
											<h6 style="margin-left:0px;">Referring Provider's Information :</h6>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="qualifier" placeholder="Qualifier" onkeypress="allowAlphabets(event)"  name="qualifier" maxlength="2">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_last_name" placeholder="Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="reffering_last_name" maxlength="25">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_first_name" placeholder="First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="reffering_first_name"  maxlength="25">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_middle_name" placeholder="Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" name="reffering_middle_name"  maxlength="25">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text" style="padding: 0;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
													<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
															<input type="radio" name="federal_tax_type" checked value="ssn" >
															<label for="radioSuccess1">SSN
															</label>
														</div>
														<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
															<input type="radio" name="federal_tax_type"  value="ein">
															<label for="radioSuccess2">EIN
															</label>
														</div>
													</span>
												</div>
												<input type="text" class="form-control" id="federal_tax_number" placeholder="Federal Tax I.D. Number"  maxlength="15" onkeypress="allowNumeric(event)" name="federal_tax_number" >
											</div>
											
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="other_id" onkeypress="allowAlphaNumeric(event)" placeholder="Other Id" name="other_id"  maxlength="19">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="npi" onkeypress="allowNumeric(event)" placeholder="NPI" name="npi"  maxlength="10">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">$</span>
												</div>
												<input type="text" class="form-control" id="total_amount" name="total_amount" maxlength="9"  oninput="validate(this)" onkeypress="return (event.charCode == 46)||(event.charCode >= 48 && event.charCode <=57)" placeholder="Total Charge($)">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">$</span>
												</div>
												<input type="text" class="form-control" id="paid_amount" name="paid_amount" maxlength="9"  oninput="validate(this)" onkeypress="return (event.charCode == 46)||(event.charCode >= 48 && event.charCode <=57)" placeholder="Amount Paid($)">
											</div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="form-group clearfix">
											Accept Assignment?
											  <div class="icheck-success d-inline">
												<input type="radio" name="accept_assignment" checked value="yes" >
												<label for="radioSuccess1">Yes
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" name="accept_assignment" value="no">
												<label for="radioSuccess2">No
												</label>
											  </div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-lg-6">
											<h6>Service Facility Location Information:</h6>
											<div class="row">
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_location_name" placeholder="Location Name" name="service_location_name"  maxlength="20">
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control us_phone" id="service_phone_number" placeholder="Phone Number" name="service_phone_number" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="service_address1" placeholder="Address 1" name="service_address1"  maxlength="26">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="service_address2" placeholder="Address 2" name="service_address2"  maxlength="20">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="service_city" placeholder="City" name="service_city"  maxlength="15">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select type="text" placeholder="State" class="form-control selectpicker"  data-live-search="true" name="service_state" id="service_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip" id="service_zip_code" placeholder="Zip Code" name="service_zip_code" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_other_id" placeholder="Other Id" onkeypress="allowAlphaNumeric(event)"  name="service_other_id"  maxlength="14">
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_npi" placeholder="NPI" onkeypress="allowNumeric(event)" name="service_npi"  maxlength="10">
												</div>
											</div>
										</div>
										<div class="col-md-6 col-lg-6">
											<h6>admin Provider Information:</h6>
											<div class="row">
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_location_name" placeholder="Location Name" name="admin_location_name"  maxlength="20">
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control us_phone" id="admin_phone_number" placeholder="Phone Number" name="admin_phone_number" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="admin_address1" placeholder="Address 1" name="admin_address1"  maxlength="20">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="admin_address2" placeholder="Address 2" name="admin_address2"  maxlength="15">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="admin_city" placeholder="City" name="admin_city"  maxlength="15">
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select placeholder="State" class="form-control selectpicker" data-live-search="true" name="admin_state" id="admin_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip" id="admin_zip_code" placeholder="Zip Code" name="admin_zip_code" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_other_id" placeholder="Other Id" maxlength="17"  onkeypress="allowAlphaNumeric(event)"  name="admin_other_id" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_npi" placeholder="NPI" maxlength="10" onkeypress="allowNumeric(event)"  name="admin_npi" >
												</div>
											</div>
										</div>
									</div>
									<div class="row">					
										<div class="form-group col-lg-4 col-md-4">
											<div class="custom-file">
                                                <input type="file" name="physician_signature" class="custom-file-input">
                                                <label class="custom-file-label">Physician or Supplier's Signature</label>
                                            </div>
										</div>
									</div>
								</section>
								<h4>CLAIM INFORMATION</h4>
								<section>
									<div class="row">
										<div class="col-lg-8 col-md-8">
											<div class="row">
												<div class="form-group col-lg-12 col-md-12">
													<label ></label>
												</div>
												<div class="col-lg-6 col-md-6">
													<div class="row">
														<div class="form-group col-md-4" >
															<input type="text" class="form-control" id="iqual" name="iqual" onkeypress="allowNumeric(event)" maxlength="3" placeholder="QUAL" >
														</div>
														<div class="form-group col-md-8">
															<input type="text" class="form-control us-date" id="injury_date" name="injury_date" placeholder="Date of Illness, Injury or Pregnancy(LMP)" >
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-6">
													<div class="row">
														<div class="form-group col-md-4" >
															<input type="text" class="form-control" id="oqual" name="oqual" maxlength="3" onkeypress="allowNumeric(event)" placeholder="Other QUAL" >
														</div>
														<div class="form-group col-md-8">
															<input type="text" class="form-control us-date" id="qual_date" name="qual_date" placeholder="Other Date" >
														</div>
													</div>
												</div>					
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="claim_code" name="claim_code" onkeypress="allowAlphaNumericSpace(event)" placeholder="Claim Codes (Designated by NUCC)"  maxlength="19">
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" name="resubmission_code" onkeypress="allowAlphaNumeric(event)" placeholder="Resubmission Code" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" name="original_reference_number" onkeypress="allowAlphaNumeric(event)" placeholder="Original Ref. No." >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" name="prior_aurthorization_number" onkeypress="allowAlphaNumeric(event)" placeholder="Prior Authorization Number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control daterange input-daterange-datepicker" id="nonworking_dates" name="nonworking_dates" placeholder="Dates patient unable to work in current occupation" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control daterange input-daterange-datepicker" id="hospital_dates" name="hospital_dates" placeholder="Hospitalization dates related to current services" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="additional_claim_information"  name="additional_claim_information" placeholder="Additional Claim Information (Designated by NUCC)"  maxlength="71">
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix" ">
													Diagnosis or Nature of Illness or Injury (Relate A-L to service line below): <input type="text" class="form-control" style="float:right;display: initial;width:auto;top: -8px;position: relative;" name="icd" maxlength="1" onkeypress="allowNumeric(event)" placeholder="ICD Ind" >
													</div>
												</div>			
											</div>				
										</div>
										<div class="col-lg-4 col-md-4">				
											<div class="row">
												<div class="form-group col-lg-12 col-md-12">
													<label>Is the Patient's condition related to:</label>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix" style="top:0px">
													Employment (Current or Previous)
													<div class="icheck-success d-inline" >
														<input type="radio" name="employment" checked value="yes" >
														<label for="radioSuccess1">Yes
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" name="employment"  value="no">
														<label for="radioSuccess2">No
														</label>
													</div>
													</div>
												</div>
												
												<div class="form-group col-lg-12 col-md-12">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" style="padding: 0px 5px;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
																Auto Accident?
																<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" name="auto_accident" checked value="yes" >
																	<label for="radioSuccess1">Yes
																	</label>
																</div>
																<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" name="auto_accident"  value="no">
																	<label for="radioSuccess2">No
																	</label>
																</div>
															</span>
														</div>
														<select placeholder="State" class="form-control selectpicker" data-live-search="true" name="accident_location" placeholder="State where the Auto Accident occurred" id="accident_location">
															<option value="">Select State</option>
															<?php foreach($all_states as $state){ ?>
																<option value="<?php echo $state['state_code']?>"><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
															<?php }?>	
														</select>
													</div>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix">
													Other Accident?
													<div class="icheck-success d-inline">
														<input type="radio" name="other_accident" checked value="yes" >
														<label for="radioSuccess1">Yes
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" name="other_accident" value="no">
														<label for="radioSuccess2">No
														</label>
													</div>
													</div>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" style="padding: 0px 5px;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
																Outside Lab?
																<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" name="out_lab" checked value="yes" >
																	<label for="radioSuccess1">Yes
																	</label>
																</div>
																<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" name="out_lab"  value="no">
																	<label for="radioSuccess2">No
																	</label>
																</div>
															</span>
														</div>
														<input type="text" class="form-control" id="total_charge" name="total_charge" maxlength="9"  oninput="validate(this)" onkeypress="return (event.charCode == 46)||(event.charCode >= 48 && event.charCode <=57)" placeholder="Total Charge($)">
													</div>
												</div>			
											</div>				
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">A</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_a" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">B</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_b" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">C</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_c" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">D</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_d" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">E</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_e" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">F</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_f" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">G</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_g" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">H</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_h" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">I</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_i" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">J</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_j" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">K</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_k" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">L</span>
												</div>
												<input type="text" class="form-control"  name="diagnosis_l" maxlength="7" onkeypress="allowAlphaNumeric(event)" placeholder="">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-lg-12 col-md-12">
											<h6 style="margin-left: 0px;">Rendered Services Information:</h6>
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<table class="table table-bordered verticle-middle table-responsive-sm">
												<thead>
													<tr>
														<th scope="col">From</th>
														<th scope="col">To</th>
														<th scope="col">Place</th>
														<th scope="col">EMG</th>
														<th scope="col">CPT/HCPCS</th>
														<th scope="col">Modifier</th>
														<th scope="col">Diagnosis Pointer</th>
														<th scope="col">Charges</th>
														<th scope="col">Days/Units</th>
														<th scope="col">EPSDT</th>
														<th scope="col">Family Plan</th>
														<th scope="col">ID QUAL</th>
														<th scope="col">Provider Id</th>
														<th scope="col">NPI</th>
														<th scope="col">Action</th>
													</tr>
												</thead>
												<tbody >
												<tr class="Add_row" id="outerdiv"></tr>
												
												<tr class="btttn">
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td ><button type="button" style="float:right"  class="eadd btn btn-outline-dark"><i style="" class="fa fa-plus-square" aria-hidden="true"></i></button></td>
												</tr>	
												</tbody>
											</table>
										</div>
									</div>
								</section>
							</div>
							<input type="hidden" value="1" id="claim_type" name="claim_type">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirmmodel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Review data</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="box-row row">
					<div class="box-cell box1 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>PATIENT & INSURED INFORMATION:</h4></div>
								<div class="col-md-4 preview_col"><label>Last Name : </label>&nbsp;<span class="preview" id="plast_name"></span></div>
								<div class="col-md-4 preview_col"><label>First Name : </label>&nbsp;<span class="preview" id="pfirst_name"></span></div>
								<div class="col-md-4 preview_col"><label>Middle Name : </label>&nbsp;<span class="preview" id="pmiddle_name"></span></div>
								<div class="col-md-4 preview_col"><label>Date of Birth : </label>&nbsp;<span class="preview" id="ppatient_dob"></span></div>
								<div class="col-md-4 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="pphone_number"></span></div>								
								<div class="col-md-4 preview_col"><label>Sex : </label>&nbsp;<span class="preview" id="psex"></span></div>
								<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="paddress1"></span></div>
								<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="paddress2"></span></div>
								<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pcity"></span></div>
								<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pstate"></span></div>
								<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pzip_code"></span></div>
								<div class="col-md-6 preview_col"><label>Account Number : </label>&nbsp;<span class="preview" id="paccount_number"></span></div>
								<div class="col-md-6 preview_col"><label>Patient relationship to Insured : </label>&nbsp;<span class="preview" id="prelation"></span></div>
								<div class="col-md-6 preview_col"><label>Patient's Signature : </label>&nbsp;<span class="preview" id="ppatient_signature"></span></div>
								<div class="col-md-6 preview_col"><label>Insured's Signature : </label>&nbsp;<span class="preview" id="pinsured_signature"></span></div>
							</div>
						</fieldset>
					</div>
					<div class="box-cell box2 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>INSURANCE INFORMATION:</h4></div>
								<div class="col-md-12 preview_col"><label>Insured Check : </label>&nbsp;<span class="preview" id="plasct_name"></span></div>
								<div class="col-md-4 preview_col"><label>Last Name : </label>&nbsp;<span class="preview" id="pinsured_last_name"></span></div>
								<div class="col-md-4 preview_col"><label>First Name : </label>&nbsp;<span class="preview" id="pinsured_first_name"></span></div>
								<div class="col-md-4 preview_col"><label>Middle Name : </label>&nbsp;<span class="preview" id="pinsured_middle_name"></span></div>
								<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="pinsured_address1"></span></div>
								<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="pinsured_address2"></span></div>
								<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pinsured_city"></span></div>
								<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pinsured_state"></span></div>
								<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pinsured_zip_code"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured Last Name : </label>&nbsp;<span class="preview" id="pother_insured_last_name"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured First Name : </label>&nbsp;<span class="preview" id="pother_insured_first_name"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured Middle Name : </label>&nbsp;<span class="preview" id="pother_insured_middle_name"></span></div>
								<div class="col-md-6 preview_col"><label>Insured's Policy Group/FECA number : </label>&nbsp;<span class="preview" id="pinsured_group_number"></span></div>
								<div class="col-md-6 preview_col"><label>Other Insured's Policy/Group Number : </label>&nbsp;<span class="preview" id="pother_group_number"></span></div>
								<div class="col-md-6 preview_col"><label>Insurance Plan Name/Program Name : </label>&nbsp;<span class="preview" id="pinsurance_plan_name"></span></div>
								<div class="col-md-6 preview_col"><label>Other Insurance Plan Name/Program Name : </label>&nbsp;<span class="preview" id="pother_insurance_plan_name"></span></div>
								<div class="col-md-4 preview_col"><label>Insured's ID Number : </label>&nbsp;<span class="preview" id="pinsured_id_number"></span></div>
								<div class="col-md-4 preview_col"><label>Insured's Date of Birth : </label>&nbsp;<span class="preview" id="pinsured_dob"></span></div>
								<div class="col-md-4 preview_col"><label>Sex : </label>&nbsp;<span class="preview" id="pinsured_sex"></span></div>
								<div class="col-md-5 preview_col"><label>Claim ID (Designated by NUCC) : </label>&nbsp;<span class="preview" id="pclaim_id"></span></div>
								<div class="col-md-7 preview_col"><label>Is there another health benefit plan? : </label>&nbsp;<span class="preview" id="pother_benefit_plan"></span></div>								
							</div>
						</fieldset>
					</div>
				</div>
				<div class="box-row row">
					<div class="box-cell box1 form-group col-lg-6 col-md-6">
					<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>PHYSICIAN/SUPPLIER INFORMATION:</h4></div>
								<div class="col-md-4 preview_col"><label>Name of Referring Provider : </label>&nbsp;<span class="preview" id="preffering_name"></span></div>
								<div class="col-md-4 preview_col"><label>Federal Tax I.D. Number : </label>&nbsp;<span class="preview" id="pfederal_tax_number"></span></div>
								<div class="col-md-4 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="pother_id"></span></div>
								<div class="col-md-4 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="pnpi"></span></div>
								<div class="col-md-4 preview_col"><label>Total Charge : </label>&nbsp;<span class="preview" id="ptotal_amount"></span></div>
								<div class="col-md-4 preview_col"><label>Amount Paid : </label>&nbsp;<span class="preview" id="ppaid_amount"></span></div>
								<div class="col-md-6 preview_col">
									<div class="row" style="margin:0px">
										<div class="col-md-12"><h6>Service Details</h6></div>
										<div class="col-md-6 preview_col"><label>Location Name : </label>&nbsp;<span class="preview" id="pservice_location_name"></span></div>
										<div class="col-md-6 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="pservice_phone_number"></span></div>
										<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="pservice_address1"></span></div>
										<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="pservice_address2"></span></div>
										<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pservice_city"></span></div>
										<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pservice_state"></span></div>
										<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pservice_zip_code"></span></div>
										<div class="col-md-6 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="pservice_other_id"></span></div>
										<div class="col-md-6 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="pservice_npi"></span></div>								
									</div>
								</div>
								<div class="col-md-6 preview_col">
									<div class="row" style="margin:0px">
										<div class="col-md-12 "><h6>admin Details</h6></div>
										<div class="col-md-6 preview_col"><label>Location Name : </label>&nbsp;<span class="preview" id="padmin_location_name"></span></div>
										<div class="col-md-6 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="padmin_phone_number"></span></div>
										<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="padmin_address1"></span></div>
										<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="padmin_address2"></span></div>
										<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="padmin_city"></span></div>
										<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="padmin_state"></span></div>
										<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="padmin_zip_code"></span></div>
										<div class="col-md-6 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="padmin_other_id"></span></div>
										<div class="col-md-6 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="padmin_npi"></span></div>								
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="box-cell box2 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>CLAIM INFORMATION:</h4></div>
								<div class="col-md-12 preview_col"><label> Is Patient's condition related to Employment (Current or Previous) </label>&nbsp;<span class="preview" id="pemployment"></span></div>
								<div class="col-md-4 preview_col"><label>Auto Accident? : </label>&nbsp;<span class="preview" id="pauto_accident"></span></div>
								<div class="col-md-4 preview_col"><label>Other Accident? : </label>&nbsp;<span class="preview" id="pother_accident"></span></div>	
								<div class="col-md-4 preview_col"><label>Place(State) : </label>&nbsp;<span class="preview" id="paccident_location"></span></div>
								<div class="col-md-6 preview_col"><label>Claim Codes (Designated by NUCC) : </label>&nbsp;<span class="preview" id="pclaim_code"></span></div>	
								<div class="col-md-6 preview_col"><label>Date of Illness, Injury or Pregnancy(LMP) : </label>&nbsp;<span class="preview" id="pinjury_date"></span></div>
								<div class="col-md-4 preview_col"><label>Other Date QUAL : </label>&nbsp;<span class="preview" id="pqual_date"></span></div>	
								<div class="col-md-8 preview_col"><label>Dates patient unable to work in current occupation : </label>&nbsp;<span class="preview" id="pnonworking_dates"></span></div>	
								<div class="col-md-8 preview_col"><label>Hospitalization dates related to current services : </label>&nbsp;<span class="preview" id="phospital_dates"></span></div>
								<div class="col-md-4 preview_col"><label>Outside Lab? : </label>&nbsp;<span class="preview" id="pout_lab"></span></div>
								<div class="col-md-8 preview_col"><label>Additional Claim Information (Designated by NUCC) : </label>&nbsp;<span class="preview" id="padditional_claim_information"></span></div>	
								<div class="col-md-4 preview_col"><label>Total Charge : </label>&nbsp;<span class="preview" id="ptotal_charge"></span></div>							
																										
							</div>
						</fieldset>
					</div>					
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Back</button>
				<button type="button" class="submit_claim btn btn-outline-dark">Submit</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Rendered services Details</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form id="claim_add_value" class="claim_add_value">
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us-date" name="service_start_date" id="service_start_date" placeholder="Service Start Date" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us-date" name="service_end_date"  id="service_end_date" placeholder="Service End Date" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="service_place"  onkeypress="allowNumeric(event)" maxlength="2" id="service_place" placeholder="Place of Service" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="emg"  id="emg" onkeypress="allowAlphabets(event)" maxlength="2" placeholder="EMG" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="cpt_hcps"  maxlength="6" onkeypress="allowAlphaNumeric(event)"  id="cpt_hcps" placeholder="CPT/HCPCS" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control modifier" name="modifier" onkeypress="allowNumeric(event)"  id="modifier" maxlength="8" placeholder="Modifier" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="diagnosis_pointer"  maxlength="4" onkeypress="return ((event.charCode >= 97 && event.charCode <= 108)||(event.charCode >= 65 && event.charCode <= 76))" id="diagnosis_pointer" placeholder="Diagnosis Pointer" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="charges"  id="charges" placeholder="Charges" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="days"  oninput="validate(this)" maxlength="3" id="days" placeholder="Days or Units" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="epsdt"  onkeypress="allowAlphaNumeric(event)"  id="epsdt" placeholder="EPSDT" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="fplan"  onkeypress="allowAlphabets(event)"  maxlength="1" id="fplan" placeholder="Family Plan" >
					</div>
					<div class="form-group col-lg-3 col-md-3">
						<input type="text" class="form-control" name="id_qual"  onkeypress="allowAlphaNumeric(event)"  id="id_qual" placeholder="ID" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="provider_id"  onkeypress="allowAlphaNumeric(event)" id="provider_id" placeholder="Rendering Provider Id">
						<input type="hidden" class="form-control" name="vid"   value="" id="mvid" >
					</div>
					<div class="form-group col-lg-3 col-md-3">
						<input type="text" class="form-control" name="cnpi"  id="cnpi" onkeypress="allowNumeric(event)"  placeholder="NPI" >
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="button" class="rtrt btn btn-outline-dark">Add</button>
			</div>
			</form>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/table-edits.min.js"></script>
<script>
$("table tbody tr").editable(
	{
    keyboard: true,
    dblclick: true,
    button: true,
    buttonSelector: ".edit",
    dropdowns: {},
    maintainWidth: true,
    edit: function(values) {},
    save: function(values) {},
    cancel: function(values) {}
});
$('body').on('click', '.eedit', function() {
	var vid = $(this).attr('vid');
	$('#mvid').val(vid);
	$('.rtrt').text('Update');
	var service_start_date=$('.rs'+vid+' .service_start_date').val();
	var validator = $( "#claim_add_value"  ).validate();
    validator.destroy();

	var service_start_date=$('.rs'+vid+' .service_start_date').val();
	var service_end_date=$('.rs'+vid+' .service_end_date').val();
	var service_place=$('.rs'+vid+' .service_place').val();
	var emg=$('.rs'+vid+' .emg').val();
	var cpt_hcps=$('.rs'+vid+' .cpt_hcps').val();
	var modifier=$('.rs'+vid+' .modifier').val();
	var diagnosis_pointer=$('.rs'+vid+' .diagnosis_pointer').val();
	var charges=$('.rs'+vid+' .charges').val();
	var days=$('.rs'+vid+' .days').val();
	var epsdt=$('.rs'+vid+' .epsdt').val();
	var fplan=$('.rs'+vid+' .fplan').val();
	var id_qual=$('.rs'+vid+' .id_qual').val();
	var provider_id=$('.rs'+vid+' .provider_id').val();
	var cnpi=$('.rs'+vid+' .cnpi').val();
	
	$('#service_start_date').val(service_start_date);
	$('#service_end_date').val(service_end_date);
	$('#service_place').val(service_place);
	$('#emg').val(emg);
	$('#cpt_hcps').val(cpt_hcps);
	$('#modifier').val(modifier);
	$('#diagnosis_pointer').val(diagnosis_pointer);
	$('#charges').val(charges);
	$('#epsdt').val(epsdt);
	$('#fplan').val(fplan);
	$('#id_qual').val(id_qual);
	$('#provider_id').val(provider_id);
	$('#cnpi').val(cnpi);

	$('#exampleModalLong').modal('show');
});


</script>
