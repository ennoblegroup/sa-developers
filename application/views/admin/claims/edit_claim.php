<link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
<link href="https://cdn.datatables.net/1.10.22/css/dataTables.semanticui.min.css">
<style>
.form-group {
    padding: 0px 5px;
}
.btttn td {
    border:none !important
}
h6{
	margin-left: -10px;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 7px 5px;
    border-bottom: 1px solid #111;
    font-size: 13px;
}
table th, table td {
	padding:5px !important;
	border-left: 1px solid #EEEEEE !important; */
    border-right: 1px solid #EEEEEE !important;
}
table.dataTable tbody th, table.dataTable tbody td {
    padding: 7px 5px;
}
#confirmmodel .modal-dialog {
    width: 100%;
    max-width: 100%;
    margin: 15px;
}
#confirmmodel .modal-content {
	width: 99%;
}
fieldset h4 {
    font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:5px 0px;
	border-bottom:none;
	text-decoration: underline;
}
fieldset label{
	font-weight:bold;
	font-size: 13px;
	margin: 0.25rem 0;
	text-transform: capitalize;
}
.preview{
	font-size: 13px;
	text-transform: capitalize;
}
legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:0 10px;
	border-bottom:none;
}
.preview_col{
	padding:0px;
	border-bottom: 1px solid #ccc2c2;
}
.box-row { 
	display:table-row; 
} 
 .box-cell { 
	display:table-cell; 
	width:50%; 
	padding:10px; 
} 
 .box-cell.box1 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	} 
 .box-cell.box2 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
} 
.uuu{
	padding:0 5px;
}

.form-group .clearfix {
    font-size: 13px;
    padding-top: 0px;
    top: 7px;
    position: relative;
}
.table-responsive-sm {
    margin-top: 10px
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item"><a href="<?= base_url('admin/claims'); ?>">Claims</a></li>
						<li class="breadcrumb-item active">Edit Claim</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-xxl-12">
				<div class="card custom_body">
					<div class="card-body">
						<form action="<?= base_url('admin/claims/update_claim'); ?>" multipart id="step-form-horizontal" method="POST" class="xstep-form-horizontal">
							<span class="row" style="margin-bottom:10px;">
								<input type="hidden" value="<?php echo $claim_data['claimm_id']?>" name="claimm_id"> 
								<input type="hidden" id="sts_id" value="<?php echo $claim_data['status_id']?>" name="status_id"> 

								<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
								<div class="form-group col-lg-3 col-md-3"></div>																
								<div class="form-group col-lg-3 col-md-3">
									<select  name="client_id" class="form-control selectpicker" data-live-search="true">
										<?php $i=1; foreach ($all_clients as $key=>$phar){ ?>
										<option value="<?php echo $phar['id']?>" <?php if($phar['id']==$claim_data['client_id']){echo 'selected';}?>><?php echo $phar['client_lbn_name']?></option>
										<?php $i++; }?>
									</select>
								</div>
								<div class="form-group col-lg-3 col-md-3">
								<?php } else {?>
								<div class="form-group col-lg-4 col-md-4"></div>
								<div class="form-group col-lg-4 col-md-4">
									<input type="hidden" value="<?php echo $_SESSION['sadevelopers_admin']['client_id'];?>"  name="client_id">
								<?php }?>	
									<select  name="insurance_company_id" class="form-control selectpicker" data-live-search="true">
										<?php $i=1; foreach ($all_insurance_companies as $key=>$phar){ ?>
										<option value="<?php echo $phar['ic_id']?>" <?php if($phar['ic_id']==$claim_data['insurance_company_id']){echo 'selected';}?>><?php echo $phar['insurance_company_name']?></option>
										<?php $i++; }?>
									</select>
								</div>
							</span>	
							<div>								
								<h4>PATIENT & INSURED INFORMATION</h4>
								<section>
									<div class="row" data-step="1">
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="last_name" placeholder="Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['last_name']?>' name="last_name">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="first_name" placeholder="First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['first_name']?>' name="first_name" >
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control" id="middle_name" placeholder="Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['middle_name']?>' name="middle_name" >
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<input type="text" class="form-control us-date" id="patient_dob" placeholder="Patient's Date of Birth" value= '<?php echo date("m/d/Y", strtotime($claim_data['patient_dob']) );?>' name="patient_dob">
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="input-group">
												<input type="text" class="form-control us_phone"  id="phone_number" placeholder="Phone Number" value= '<?php echo $claim_data['phone_number']?>' name="phone_number" >
											</div>
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="form-group clearfix">
												Sex&nbsp;:&nbsp;
											  <div class="icheck-success d-inline" >
												<input type="radio" <?php if($claim_data['gender']=='male'){echo 'checked';}?> name="gender" value="male"  id="radioSuccess1">
												<label for="radioSuccess1">Male
												</label>
											  </div>
											  <div class="icheck-success d-inline" style="margin-right: 12x;margin-left:12px">
												<input type="radio"<?php if($claim_data['gender']=='female'){echo 'checked';}?>   name="gender" value="female" id="radioSuccess2">
												<label for="radioSuccess2">Female
												</label>
											  </div>
											  <div class="icheck-success d-inline">
													<input type="radio" name="gender" <?php if($claim_data['gender']=='other'){echo 'checked';}?>  value="other">
													<label for="radioSuccess2">Other
													</label>
												</div>
											</div>
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="address1" placeholder="Address 1" value= '<?php echo $claim_data['address1']?>' name="address1" >
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<input type="text" class="form-control" id="address2" placeholder="Address 2" value= '<?php echo $claim_data['address2']?>' name="address2" >
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="city" placeholder="City" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['city']?>' name="city" >
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<select placeholder="State" class="selectpicker form-control" data-live-search="true"  name="state" id="state">
												<option value="">Select State</option>
												<?php foreach($all_states as $state){ ?>
													<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$claim_data['state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
												<?php }?>	
											</select>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text"  class="form-control us_zip" id="zip_code" placeholder="Zip Code" value= '<?php echo $claim_data['zip_code']?>' name="zip_code" >
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text"  class="form-control" id="account_number" maxlength="14" placeholder="Account Number" onkeypress="allowAlphaNumericSpace(event)" value= '<?php echo $claim_data['account_number']?>' name="account_number" >
										</div>
										<div class="form-group col-lg-6 col-md-6">
											<div class="form-group clearfix">
											Patient relationship to Insured&nbsp;:&nbsp;
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio"  <?php if($claim_data['relation']=='self'){echo 'checked';}?> name="relation"  value="self">
												<label for="pri1">Self
												</label>
											  </div>
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio" <?php if($claim_data['relation']=='spouse'){echo 'checked';}?> name="relation" value="spouse">
												<label for="pri2" >Spouse
												</label>
											  </div>
											  <div class="icheck-success d-inline" style="margin-right: 25px;">
												<input type="radio" <?php if($claim_data['relation']=='child'){echo 'checked';}?> name="relation" value="child">
												<label for="pri3" >Child
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" <?php if($claim_data['relation']=='other'){echo 'checked';}?> name="relation" value="other">
												<label for="pri4" >Other
												</label>
											  </div>
											</div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="custom-file">
                                                <input type="file" value= '<?php echo $claim_data['patient_signature']?>' name="patient_signature" class="custom-file-input">
                                                <label class="custom-file-label">Patient's Signature</label>
                                            </div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="custom-file">
                                                <input type="file" value= '<?php echo $claim_data['insured_signature']?>' name="insured_signature" class="custom-file-input">
                                                <label class="custom-file-label">Insured's Signature</label>
                                            </div>
										</div>
									</div>
								</section>
								<h4>INSURANCE INFORMATION</h4>
								<section>
									<div class="row" data-step="2">
										<div class=" col-lg-8 col-md-8">
											<div class="form-group">
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" <?php if($claim_data['insured_check']=='child'){echo 'MEDICARE';}?>  name="insured_check" value="MEDICARE" >
													<label class="form-check-label" for="check1">MEDICARE</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input"  <?php if($claim_data['insured_check']=='Medicaid'){echo 'checked';}?> name="insured_check"  value="Medicaid">
													<label class="form-check-label" for="check2">Medicaid</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" <?php if($claim_data['insured_check']=='TRICARE'){echo 'checked';}?> name="insured_check"  value="TRICARE" >
													<label class="form-check-label" for="check1">TRICARE</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input"  <?php if($claim_data['insured_check']=='CHAMPVA'){echo 'checked';}?> name="insured_check" value="CHAMPVA">
													<label class="form-check-label" for="check2">CHAMPVA</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input"  <?php if($claim_data['insured_check']=='Group Health Plan'){echo 'checked';}?> name="insured_check"  value="Group Health Plan" >
													<label class="form-check-label" for="check1">Group Health Plan</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input"  <?php if($claim_data['insured_check']=='FECA/Black Lung'){echo 'checked';}?> name="insured_check"  value="FECA/Black Lung">
													<label class="form-check-label" for="check2">FECA/Black Lung</label>
												</div>
												<div class="form-check mb-2">
													<input type="radio" class="form-check-input" <?php if($claim_data['insured_check']=='Other'){echo 'checked';}?> name="insured_check"  value="Other">
													<label class="form-check-label" for="check2">Other</label>
												</div>
											</div>
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="form-group clearfix" style="padding-top:0px;">
											Is there another health benefit plan?
											  <div class="icheck-success d-inline">
												<input type="radio" <?php if($claim_data['other_benefit_plan']=='yes'){echo 'checked';}?> name="other_benefit_plan" value="yes">
												<label for="radioSuccess1">Yes
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" <?php if($claim_data['other_benefit_plan']=='no'){echo 'checked';}?> name="other_benefit_plan"  value="no">
												<label for="radioSuccess2">No
												</label>
											  </div>
											</div>
										</div>
										<div class="col-lg-8 col-md-8">
											<h6>Insured Information:</h6>
											<div class="row">
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_last_name" placeholder="Insured's Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['insured_last_name']?>' name="insured_last_name" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_first_name" placeholder="Insured's First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['insured_first_name']?>' name="insured_first_name" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_middle_name" placeholder="Insured's Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['insured_middle_name']?>' name="insured_middle_name" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" id="insured_id_number" placeholder="Insured's ID Number"  onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)" value= '<?php echo $claim_data['insured_id_number']?>' name="insured_id_number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us-date" value= '<?php echo date("m/d/Y", strtotime($claim_data['insured_dob']) );?>'  name="insured_dob" id="insured_dob" placeholder="Insured's Date of Birth" >
												</div>
												<div class="form-group col-lg-4 col-md-3">
													<input type="text" class="form-control us_phone" value= '<?php echo $claim_data['insured_phone_number']?>' id="insured_phone_number" placeholder="Insured's Phone Number"   name="insured_phone_number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<div class="form-group clearfix" style="font-size: 13px;padding-top:0px;top: 7px;position: relative;">
													Sex&nbsp;:&nbsp;&nbsp;
													<div class="icheck-success d-inline"  >
														<input type="radio" <?php if($claim_data['insured_gender']=='male'){echo 'checked';}?> name="insured_gender" id="radoSuccess1" value="male">
														<label for="radioSuccess1">Male
														</label>
													</div>
													<div class="icheck-success d-inline" style="margin-right: 12x;margin-left:12px">
														<input type="radio" <?php if($claim_data['insured_gender']=='female'){echo 'checked';}?> name="insured_gender"  value="female">
														<label for="radioSuccess2">Female
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" name="gender" <?php if($claim_data['gender']=='other'){echo 'checked';}?>  value="other">
														<label for="radioSuccess2">Other
														</label>
													</div>
													</div>
												</div>
												<div class="form-group col-lg-6 col-md-12">
													<input type="text" class="form-control" id="insured_address1" placeholder="Address 1" value= '<?php echo $claim_data['insured_address1']?>' name="insured_address1" >
												</div>
												<div class="form-group col-lg-6 col-md-12">
													<input type="text" class="form-control" id="insured_address2" placeholder="Address 2" value= '<?php echo $claim_data['insured_address2']?>' name="insured_address2" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insured_city" placeholder="City" onkeypress="return (event.charCode == 32)||(event.ch rCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['insured_city']?>' name="insured_city" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select type="text" placeholder="State" class="form-control selectpicker"  data-live-search="true" name="insured_state" id="insured_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$claim_data['insured_state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip" id="insured_zip_code" placeholder="Zip Code" value= '<?php echo $claim_data['insured_zip_code']?>' name="insured_zip_code" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="claim_id" maxlength="30" placeholder="Claim ID (Designated by NUCC)" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  value= '<?php echo $claim_data['iclaim_id']?>' name="claim_id" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insured_group_number" placeholder="Insured's Policy Group/FECA number" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  value= '<?php echo $claim_data['insured_group_number']?>' name="insured_group_number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="insurance_plan_name" placeholder="Insurance Plan Name/Program Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  value= '<?php echo $claim_data['insurance_plan_name']?>' name="insurance_plan_name" >
												</div>
											</div>		
										</div>
										<div class=" col-lg-4 col-md-4">
											<h6>Other Insured Information:</h6>		
											<div class="row">			
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_last_name" placeholder="Other Insured's Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  value= '<?php echo $claim_data['other_insured_last_name']?>' name="other_insured_last_name" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_first_name" placeholder="Other Insured's First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  value= '<?php echo $claim_data['other_insured_first_name']?>' name="other_insured_first_name" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insured_middle_name" placeholder="Other Insured's Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)"  value= '<?php echo $claim_data['other_insured_middle_name']?>' name="other_insured_middle_name" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_insurance_plan_name" placeholder="Other Insurance Plan Name/Program Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  value= '<?php echo $claim_data['other_insurance_plan_name']?>' name="other_insurance_plan_name" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="other_group_number" placeholder="Other Insured's Policy/Group Number" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122) || (event.charCode >= 48 && event.charCode <= 57)"  value= '<?php echo $claim_data['other_group_number']?>' name="other_group_number" >
												</div>
											</div>
										</div>							
									</div>
								</section>
								<h4>PHYSICIAN/SUPPLIER INFORMATION</h4>
								<section>
									<div class="row">
										<div class="form-group col-lg-12 col-md-12">
											<h6 style="margin-left:0px;">Referring Provider's Information :</h6>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="qualifier" placeholder="Qualifier" value= '<?php echo $claim_data['qualifier']?>' name="qualifier" >
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_last_name" placeholder="Last Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['reffering_last_name']?>' name="reffering_last_name">
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_first_name" placeholder="First Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['reffering_first_name']?>' name="reffering_first_name" >
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="reffering_middle_name" placeholder="Middle Name" onkeypress="return (event.charCode == 32)||(event.charCode >= 65 && event.charCode <= 90) ||(event.charCode >= 97 && event.charCode <= 122)" value= '<?php echo $claim_data['reffering_middle_name']?>' name="reffering_middle_name" >
										</div>
										<div class="form-group col-lg-4 col-md-4">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text" style="padding: 0;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
													<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
															<input type="radio" <?php if($claim_data['federal_tax_type']=='ssn'){echo 'checked';}?> name="federal_tax_type" checked value="ssn" >
															<label for="radioSuccess1">SSN
															</label>
														</div>
														<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
															<input type="radio" <?php if($claim_data['federal_tax_type']=='ein'){echo 'checked';}?>  name="federal_tax_type"  value="ein">
															<label for="radioSuccess2">EIN
															</label>
														</div>
													</span>
												</div>
												<input type="text" class="form-control" id="federal_tax_number" placeholder="Federal Tax I.D. Number" value= '<?php echo $claim_data['federal_tax_number']?>' onkeypress="allowNumeric(event)" maxlength="15" name="federal_tax_number" >
											</div>
											
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<input type="text" class="form-control" id="other_id" placeholder="Other Id" value= '<?php echo $claim_data['other_id']?>' name="other_id" >
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<input type="text" class="form-control" id="npi" placeholder="NPI" value= '<?php echo $claim_data['npi']?>' name="npi" >
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">$</span>
												</div>
												<input type="text" class="form-control" id="total_amount" value= '<?php echo $claim_data['total_amount']?>' name="total_amount" maxlength="9"  oninput="validate(this)"  placeholder="Total Charge($)">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">$</span>
												</div>
												<input type="text" class="form-control" id="paid_amount" value= '<?php echo $claim_data['paid_amount']?>' name="paid_amount" maxlength="9"  oninput="validate(this)"  placeholder="Amount Paid($)">
											</div>
										</div>
										<div class="form-group col-lg-3 col-md-3">
											<div class="form-group clearfix">
											Accept Assignment?
											  <div class="icheck-success d-inline">
												<input type="radio" <?php if($claim_data['accept_assignment']=='yes' ||$claim_data['accept_assignment']==''){echo 'checked';}?> name="accept_assignment"  value="yes" >
												<label for="radioSuccess1">Yes
												</label>
											  </div>
											  <div class="icheck-success d-inline">
												<input type="radio" <?php if($claim_data['accept_assignment']=='no'){echo 'checked';}?>  name="accept_assignment" value="no">
												<label for="radioSuccess2">No
												</label>
											  </div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 col-lg-6">
											<h6>Service Facility Location Information:</h6>
											<div class="row">
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_location_name" placeholder="Location Name" value= '<?php echo $claim_data['service_location_name']?>' name="service_location_name" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control us_phone" id="service_phone_number" placeholder="Phone Number" value= '<?php echo $claim_data['service_phone_number']?>' name="service_phone_number" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="service_address1" placeholder="Address 1" value= '<?php echo $claim_data['service_address1']?>' name="service_address1" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="service_address2" placeholder="Address 2" value= '<?php echo $claim_data['service_address2']?>' name="service_address2" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="service_city" placeholder="City" value= '<?php echo $claim_data['service_city']?>' name="service_city" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select type="text" placeholder="State" class="form-control selectpicker"  data-live-search="true"  name="service_state" id="service_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$claim_data['service_state']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip" id="service_zip_code" placeholder="Zip Code" value= '<?php echo $claim_data['service_zip_code']?>' name="service_zip_code" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_other_id" placeholder="Other Id" value= '<?php echo $claim_data['service_other_id']?>' name="service_other_id" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="service_npi" placeholder="NPI" value= '<?php echo $claim_data['service_npi']?>' name="service_npi" >
												</div>
											</div>
										</div>
										<div class="col-md-6 col-lg-6">
											<h6>admin Provider Information:</h6>
											<div class="row">
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_location_name" placeholder="Location Name" value= '<?php echo $claim_data['admin_location_name']?>' name="admin_location_name" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control us_phone" id="admin_phone_number" placeholder="Phone Number" value= '<?php echo $claim_data['admin_phone_number']?>' name="admin_phone_number" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="admin_address1" placeholder="Address 1" value= '<?php echo $claim_data['admin_address1']?>' name="admin_address1" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<input type="text" class="form-control" id="admin_address2" placeholder="Address 2" value= '<?php echo $claim_data['admin_address2']?>' name="admin_address2" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" id="admin_city" placeholder="City" value= '<?php echo $claim_data['admin_city']?>' name="admin_city" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<select placeholder="State" class="form-control selectpicker" data-live-search="true"  name="admin_state" id="admin_state">
														<option value="">Select State</option>
														<?php foreach($all_states as $state){ ?>
															<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$claim_data['admin_state']){echo 'selected';}?> ><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
														<?php }?>	
													</select>
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control us_zip" id="admin_zip_code" placeholder="Zip Code" value= '<?php echo $claim_data['admin_zip_code']?>' name="admin_zip_code" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_other_id" placeholder="Other Id" value= '<?php echo $claim_data['admin_other_id']?>'  onkeypress="allowAlphaNumeric(event)" maxlength="17" name="admin_other_id" >
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<input type="text" class="form-control" id="admin_npi" placeholder="NPI" value= '<?php echo $claim_data['admin_npi']?>' onkeypress="allowNumeric(event)" maxlength="10" name="admin_npi" >
												</div>
											</div>
										</div>
									</div>
									<div class="row">					
										<div class="form-group col-lg-4 col-md-4">
											<div class="custom-file">
                                                <input type="file" value= '<?php echo $claim_data['physician_signature']?>' name="physician_signature" class="custom-file-input">
                                                <label class="custom-file-label">Physician or Supplier's Signature</label>
                                            </div>
										</div>
									</div>
								</section>
								<h4>CLAIM INFORMATION</h4>
								<section>
									<div class="row">
										<div class="col-lg-8 col-md-8">
											<div class="row">
												<div class="form-group col-lg-6 col-md-6">
													<div class="row">
														<div class="col-md-4" style="padding-right: 0px;">
															<input type="text" class="form-control" id="iqual" value= '<?php echo $claim_data['iqual']?>' maxlength="3" name="iqual" placeholder="QUAL" >
														</div>
														<div class="col-md-8">
															<input type="text" class="form-control us-date" id="injury_date" value="<?php echo date("m/d/Y", strtotime($claim_data['injury_date']) );?>" name="injury_date" placeholder="Date of Illness, Injury or Pregnancy(LMP)" >
														</div>
													</div>
												</div>
												<div class="form-group col-lg-6 col-md-6">
													<div class="row">
														<div class="col-md-4" style="padding-right: 0px;">
															<input type="text" class="form-control" id="oqual" name="oqual" value= '<?php echo $claim_data['oqual']?>' maxlength="3" placeholder="Other QUAL" >
														</div>
														<div class="col-md-8">
															<input type="text" class="form-control us-date" id="qual_date" value="<?php echo date("m/d/Y", strtotime($claim_data['qual_date']) );?>" name="qual_date" placeholder="Other Date" >
														</div>
													</div>
												</div>					
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" value= '<?php echo $claim_data['claim_code']?>' id="claim_code" name="claim_code" placeholder="Claim Codes (Designated by NUCC)" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" value= '<?php echo $claim_data['resubmission_code']?>' name="resubmission_code" placeholder="Resubmission Code" >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" value= '<?php echo $claim_data['original_reference_number']?>' name="original_reference_number" placeholder="Original Ref. No." >
												</div>
												<div class="form-group col-lg-3 col-md-3">
													<input type="text" class="form-control" value= '<?php echo $claim_data['prior_aurthorization_number']?>' name="prior_aurthorization_number" placeholder="Prior Authorization Number" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control daterange input-daterange-datepicker" value="<?php echo date("m/d/Y", strtotime($claim_data['nonworking_start_date']) );?>-<?php echo date("m/d/Y", strtotime($claim_data['nonworking_end_date']) );?>" id="nonworking_dates" name="nonworking_dates" placeholder="Dates patient unable to work in current occupation" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control daterange input-daterange-datepicker" value="<?php echo date("m/d/Y", strtotime($claim_data['hospital_start_date']) );?>-<?php echo date("m/d/Y", strtotime($claim_data['hospital_end_date']) );?>" id="hospital_dates" name="hospital_dates" placeholder="Hospitalization dates related to current services" >
												</div>
												<div class="form-group col-lg-4 col-md-4">
													<input type="text" class="form-control" value= '<?php echo $claim_data['additional_claim_information']?>' id="additional_claim_information"  name="additional_claim_information" placeholder="Additional Claim Information (Designated by NUCC)" >
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix">
													Diagnosis or Nature of Illness or Injury (Relate A-L to service line below): <input type="text" class="form-control" style="float:right;display: initial;width:auto" value= '<?php echo $claim_data['icd']?>' maxlength="1" onkeypress="allowNumeric(event)" name="icd" placeholder="ICD Ind" >
													</div>
												</div>			
											</div>				
										</div>
										<div class="col-lg-4 col-md-4">				
											<div class="row">
												<div class="form-group col-lg-12 col-md-12">
													<h6 style="margin-left:0px;">Is the Patient's condition related to:</h6>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix" >
													Employment (Current or Previous)
													<div class="icheck-success d-inline" >
														<input type="radio" <?php if($claim_data['employment']=='yes'){echo 'checked';}?> name="employment"  value="yes" >
														<label for="radioSuccess1">Yes
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" <?php if($claim_data['employment']=='no'){echo 'checked';}?> name="employment"  value="no">
														<label for="radioSuccess2">No
														</label>
													</div>
													</div>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="form-group clearfix">
													Auto Accident?
													<div class="icheck-success d-inline">
														<input type="radio" <?php if($claim_data['auto_accident']=='yes'){echo 'checked';}?> name="auto_accident"  value="yes" >
														<label for="radioSuccess1">Yes
														</label>
													</div>
													<div class="icheck-success d-inline">
														<input type="radio" <?php if($claim_data['auto_accident']=='no'){echo 'checked';}?> name="auto_accident" value="no">
														<label for="radioSuccess2">No
														</label>
													</div>
													</div>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" style="padding: 0px 5px;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
																Other Accident?
																<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" <?php if($claim_data['other_accident']=='yes'){echo 'checked';}?> name="other_accident" value="yes" >
																	<label for="radioSuccess1">Yes
																	</label>
																</div>
																<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" <?php if($claim_data['other_accident']=='no'){echo 'checked';}?> name="other_accident"  value="no">
																	<label for="radioSuccess2">No
																	</label>
																</div>
															</span>
														</div>
														<select placeholder="State" class="form-control selectpicker" data-live-search="true" name="accident_location" placeholder="State where the Auto Accident occurred" id="accident_location">
															<option value="">Select State</option>
															<?php foreach($all_states as $state){ ?>
																<option value="<?php echo $state['state_code']?>" <?php if($state['state_code']==$claim_data['accident_location']){echo 'selected';}?>><?php echo $state['state_name']?> (<?php echo $state['state_code']?>)</option>
															<?php }?>	
														</select>
													</div>
												</div>
												<div class="form-group col-lg-12 col-md-12">
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text" style="padding: 0px 5px;background: #ffffff;color: #495057;border: 1px solid #b4abab;">
																Outside Lab?
																<div class="icheck-success d-inline"  style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" <?php if($claim_data['out_lab']=='yes'){echo 'checked';}?> name="out_lab"  value="yes" >
																	<label for="radioSuccess1">Yes
																	</label>
																</div>
																<div class="icheck-success d-inline" style="padding: 0.2rem 0.75rem;position: relative;top: 3px;">
																	<input type="radio" <?php if($claim_data['out_lab']=='no'){echo 'checked';}?> name="out_lab"  value="no">
																	<label for="radioSuccess2">No
																	</label>
																</div>
															</span>
														</div>
														<input type="text" class="form-control" id="total_charge" value= '<?php echo $claim_data['total_charge']?>' maxlength="9"  oninput="validate(this)"  name="total_charge" placeholder="Total Charge($)">
													</div>
												</div>			
											</div>				
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">A</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_a']?>" maxlength="7" name="diagnosis_a" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">B</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_b']?>" maxlength="7" name="diagnosis_b" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">C</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_c']?>" maxlength="7" name="diagnosis_c" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">D</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_d']?>" maxlength="7" name="diagnosis_d" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">E</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_e']?>" maxlength="7" name="diagnosis_e" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">F</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_f']?>" maxlength="7" name="diagnosis_f" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">G</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_g']?>" maxlength="7" ame="diagnosis_g" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">H</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_h']?>" maxlength="7" name="diagnosis_h" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">I</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_i']?>" maxlength="7" name="diagnosis_i" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">J</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_j']?>" maxlength="7" name="diagnosis_j" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">K</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_k']?>" maxlength="7" name="diagnosis_k" placeholder="">
											</div>
										</div>
										<div class="form-group col-lg-2 col-md-2">
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text">L</span>
												</div>
												<input type="text" class="form-control"  value="<?php echo $claim_data['diagnosis_l']?>" nmaxlength="7" ame="diagnosis_l" placeholder="">
												<input type="hidden" class="form-control"  value="<?php echo $claim_data['id']?>" name="diagnosis_id" placeholder="">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-lg-12 col-md-12">
											<h6 style="margin-left: 0px;">Rendered Services Information:</h6>
										</div>
										<div class="form-group col-lg-12 col-md-12">
											<table class="table table-bordered cell-border verticle-middle table-responsive-sm">
												<thead>
													<tr>
														<th scope="col">From</th>
														<th scope="col">To</th>
														<th scope="col">Place</th>
														<th scope="col">EMG</th>
														<th scope="col">CPT/HCPCS</th>
														<th scope="col">Modifier</th>
														<th scope="col">Diagnosis Pointer</th>
														<th scope="col">Charges</th>
														<th scope="col">Days/Units</th>
														<th scope="col">EPSDT</th>
														<th scope="col">Family Plan</th>
														<th scope="col">ID QUAL</th>
														<th scope="col">Provider Id</th>
														<th scope="col">NPI</th>
														<th scope="col">Actions</th>
													</tr>
												</thead>
												<tbody>
												<?php $i=1; foreach($claim_additional_data as $row): ?>
													<tr class="doc_div qwert rs<?= $row['id']; ?>" tpid="<?= $row['id']; ?>">
														<td class="td_service_start_date" ><?php echo date("m/d/Y", strtotime($row['service_start_date']) );?></td>
														<td class="td_service_end_date" ><?php echo date("m/d/Y", strtotime($row['service_end_date']) );?></td>
														<td class="td_service_place" ><?= $row['service_place']; ?></td>
														<td class="td_emg" ><?= $row['emg']; ?></td>
														<td class="td_cpt_hcps" ><?= $row['cpt_hcps']; ?></td>
														<td class="td_modifier" ><?= $row['modifier']; ?></td>
														<td class="td_diagnosis_pointer" ><?= $row['diagnosis_pointer']; ?></td>
														<td class="td_charges" ><?= $row['charges']; ?></td>
														<td class="td_days" ><?= $row['days']; ?></td>
														<td class="td_epsdt" ><?= $row['epsdt']; ?></td>
														<td class="td_fplan" ><?= $row['fplan']; ?></td>
														<td class="td_id_qual" ><?= $row['id_qual']; ?></td>
														<td class="td_provider_id" ><?= $row['provider_id']; ?> </td>
														<td class="td_cnpi" ><?= $row['cnpi']; ?></td>
														<td>
															<input type="hidden" class="form-control us_date service_start_date" value="<?php echo date("m/d/Y", strtotime($row['service_start_date']) );?>" name="service_start_date[]" placeholder="Date" >
															<input type="hidden" class="form-control us_date service_end_date" value="<?php echo date("m/d/Y", strtotime($row['service_end_date']) );?>" name="service_end_date[]" placeholder="Date" >
															<input type="hidden" class="form-control service_place" value= "<?= $row['service_place']; ?>" onkeypress="allowNumeric(event)" maxlength="2" name="service_place[]" placeholder="Place of Service" >
															<input type="hidden" class="form-control emg" value="<?= $row['emg']; ?>" name="emg[]" placeholder="EMG" >
															<input type="hidden" class="form-control cpt_hcps" value="<?= $row['cpt_hcps']; ?>" name="cpt_hcps[]" placeholder="CPT/HCPCS" >
															<input type="hidden" class="form-control modifier" value="<?= $row['modifier']; ?>" name="modifier[]" placeholder="Modifier" >
															<input type="hidden" class="form-control diagnosis_pointer" value="<?= $row['diagnosis_pointer']; ?>" maxlength="4" onkeypress="return ((event.charCode >= 97 && event.charCode <= 108)||(event.charCode >= 65 && event.charCode <= 76))" name="diagnosis_pointer[]" placeholder="Diagnosis Pointer" >
															<input type="hidden" class="form-control charges" value="<?= $row['charges']; ?>" name="charges[]" placeholder="Charges" >
															<input type="hidden" class="form-control days" value="<?= $row['days']; ?>" name="days[]" placeholder="Days or Units" >
															<input type="hidden" class="form-control epsdt" value="<?= $row['epsdt']; ?>" name="epsdt[]" placeholder="EPSDT" >
															<input type="hidden" class="form-control fplan" value="<?= $row['fplan']; ?>" name="fplan[]" onkeypress="allowAlphabets(event)"  maxlength="1" placeholder="Family Plan" >
															<input type="hidden" class="form-control id_qual" value="<?= $row['id_qual']; ?>" name="id_qual[]" placeholder="ID" >
															<input type="hidden" class="form-control" name="value_id[]" value="<?= $row['id']; ?>">
															<input type="hidden" class="form-control provider_id" value="<?= $row['provider_id']; ?>" name="provider_id[]" style="width:50%" placeholder="Rendering Provider Id" >
															<input type="hidden" class="form-control cnpi" value="<?= $row['cnpi']; ?>" name="cnpi[]" placeholder="NPI" style="width:50%">
															<a href="#" class="eedit" vid = "<?= $row['id']; ?>" > <button type="button" class="btn btn-sm btn-outline-dark"><i class="fa fa-pencil"></i></button> </a>
															<a href="#" class="removeRecord" vid = "<?= $row['id']; ?>"><button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></button></a>
														</td>
													</tr>
												<?php $i++; endforeach; ?>
												<tr class="Add_row" id="outerdiv"></tr>
												
												<tr class="btttn">
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td><button type="button" style="float:right"  class="eadd btn btn-outline-dark"><i style="" class="fa fa-plus-square" aria-hidden="true"></i></button></td>
												</tr>	
												</tbody>
											</table>
										</div>
									</div>	
								</section>
								
							</div>
							<input type="hidden" value="1" id="claim_type"  name="claim_type">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirmmodel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Review data</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="box-row row">
					<div class="box-cell box1 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>PATIENT & INSURED INFORMATION:</h4></div>
								<div class="col-md-4 preview_col"><label>Last Name : </label>&nbsp;<span class="preview" id="plast_name"></span></div>
								<div class="col-md-4 preview_col"><label>First Name : </label>&nbsp;<span class="preview" id="pfirst_name"></span></div>
								<div class="col-md-4 preview_col"><label>Middle Name : </label>&nbsp;<span class="preview" id="pmiddle_name"></span></div>
								<div class="col-md-4 preview_col"><label>Date of Birth : </label>&nbsp;<span class="preview" id="ppatient_dob"></span></div>
								<div class="col-md-4 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="pphone_number"></span></div>								
								<div class="col-md-4 preview_col"><label>Sex : </label>&nbsp;<span class="preview" id="psex"></span></div>
								<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="paddress1"></span></div>
								<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="paddress2"></span></div>
								<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pcity"></span></div>
								<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pstate"></span></div>
								<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pzip_code"></span></div>
								<div class="col-md-6 preview_col"><label>Account Number : </label>&nbsp;<span class="preview" id="paccount_number"></span></div>
								<div class="col-md-6 preview_col"><label>Patient relationship to Insured : </label>&nbsp;<span class="preview" id="prelation"></span></div>
								<div class="col-md-6 preview_col"><label>Patient's Signature : </label>&nbsp;<span class="preview" id="ppatient_signature"></span></div>
								<div class="col-md-6 preview_col"><label>Insured's Signature : </label>&nbsp;<span class="preview" id="pinsured_signature"></span></div>
							</div>
						</fieldset>
					</div>
					<div class="box-cell box2 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>INSURANCE INFORMATION:</h4></div>
								<div class="col-md-12 preview_col"><label>Insured Check : </label>&nbsp;<span class="preview" id="plasct_name"></span></div>
								<div class="col-md-4 preview_col"><label>Last Name : </label>&nbsp;<span class="preview" id="pinsured_last_name"></span></div>
								<div class="col-md-4 preview_col"><label>First Name : </label>&nbsp;<span class="preview" id="pinsured_first_name"></span></div>
								<div class="col-md-4 preview_col"><label>Middle Name : </label>&nbsp;<span class="preview" id="pinsured_middle_name"></span></div>
								<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="pinsured_address1"></span></div>
								<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="pinsured_address2"></span></div>
								<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pinsured_city"></span></div>
								<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pinsured_state"></span></div>
								<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pinsured_zip_code"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured Last Name : </label>&nbsp;<span class="preview" id="pother_insured_last_name"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured First Name : </label>&nbsp;<span class="preview" id="pother_insured_first_name"></span></div>
								<div class="col-md-4 preview_col"><label>Other Insured Middle Name : </label>&nbsp;<span class="preview" id="pother_insured_middle_name"></span></div>
								<div class="col-md-6 preview_col"><label>Insured's Policy Group/FECA number : </label>&nbsp;<span class="preview" id="pinsured_group_number"></span></div>
								<div class="col-md-6 preview_col"><label>Other Insured's Policy/Group Number : </label>&nbsp;<span class="preview" id="pother_group_number"></span></div>
								<div class="col-md-6 preview_col"><label>Insurance Plan Name/Program Name : </label>&nbsp;<span class="preview" id="pinsurance_plan_name"></span></div>
								<div class="col-md-6 preview_col"><label>Other Insurance Plan Name/Program Name : </label>&nbsp;<span class="preview" id="pother_insurance_plan_name"></span></div>
								<div class="col-md-4 preview_col"><label>Insured's ID Number : </label>&nbsp;<span class="preview" id="pinsured_id_number"></span></div>
								<div class="col-md-4 preview_col"><label>Insured's Date of Birth : </label>&nbsp;<span class="preview" id="pinsured_dob"></span></div>
								<div class="col-md-4 preview_col"><label>Sex : </label>&nbsp;<span class="preview" id="pinsured_sex"></span></div>
								<div class="col-md-5 preview_col"><label>Claim ID (Designated by NUCC) : </label>&nbsp;<span class="preview" id="pclaim_id"></span></div>
								<div class="col-md-7 preview_col"><label>Is there another health benefit plan? : </label>&nbsp;<span class="preview" id="pother_benefit_plan"></span></div>								
							</div>
						</fieldset>
					</div>
				</div>
				<div class="box-row row">
					<div class="box-cell box1 form-group col-lg-6 col-md-6">
					<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>PHYSICIAN/SUPPLIER INFORMATION:</h4></div>
								<div class="col-md-4 preview_col"><label>Name of Referring Provider : </label>&nbsp;<span class="preview" id="preffering_name"></span></div>
								<div class="col-md-4 preview_col"><label>Federal Tax I.D. Number : </label>&nbsp;<span class="preview" id="pfederal_tax_number"></span></div>
								<div class="col-md-4 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="pother_id"></span></div>
								<div class="col-md-4 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="pnpi"></span></div>
								<div class="col-md-4 preview_col"><label>Total Charge : </label>&nbsp;<span class="preview" id="ptotal_amount"></span></div>
								<div class="col-md-4 preview_col"><label>Amount Paid : </label>&nbsp;<span class="preview" id="ppaid_amount"></span></div>
								<div class="col-md-6 preview_col">
									<div class="row" style="margin:0px">
										<div class="col-md-12"><h6>Service Details</h6></div>
										<div class="col-md-6 preview_col"><label>Location Name : </label>&nbsp;<span class="preview" id="pservice_location_name"></span></div>
										<div class="col-md-6 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="pservice_phone_number"></span></div>
										<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="pservice_address1"></span></div>
										<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="pservice_address2"></span></div>
										<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="pservice_city"></span></div>
										<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="pservice_state"></span></div>
										<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="pservice_zip_code"></span></div>
										<div class="col-md-6 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="pservice_other_id"></span></div>
										<div class="col-md-6 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="pservice_npi"></span></div>								
									</div>
								</div>
								<div class="col-md-6 preview_col">
									<div class="row" style="margin:0px">
										<div class="col-md-12 "><h6>admin Details</h6></div>
										<div class="col-md-6 preview_col"><label>Location Name : </label>&nbsp;<span class="preview" id="padmin_location_name"></span></div>
										<div class="col-md-6 preview_col"><label>Phone Number : </label>&nbsp;<span class="preview" id="padmin_phone_number"></span></div>
										<div class="col-md-6 preview_col"><label>Address1 : </label>&nbsp;<span class="preview" id="padmin_address1"></span></div>
										<div class="col-md-6 preview_col"><label>Address2 : </label>&nbsp;<span class="preview" id="padmin_address2"></span></div>
										<div class="col-md-4 preview_col"><label>city : </label>&nbsp;<span class="preview" id="padmin_city"></span></div>
										<div class="col-md-4 preview_col"><label>State : </label>&nbsp;<span class="preview" id="padmin_state"></span></div>
										<div class="col-md-4 preview_col"><label>Zip Code : </label>&nbsp;<span class="preview" id="padmin_zip_code"></span></div>
										<div class="col-md-6 preview_col"><label>Other Id : </label>&nbsp;<span class="preview" id="padmin_other_id"></span></div>
										<div class="col-md-6 preview_col"><label>NPI : </label>&nbsp;<span class="preview" id="padmin_npi"></span></div>								
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="box-cell box2 form-group col-lg-6 col-md-6">
						<fieldset class="scheduler-border">
							<div class="row uuu">
								<div class="col-md-12 preview_col"><h4>CLAIM INFORMATION:</h4></div>
								<div class="col-md-12 preview_col"><label> Is Patient's condition related to Employment (Current or Previous) </label>&nbsp;<span class="preview" id="pemployment"></span></div>
								<div class="col-md-4 preview_col"><label>Auto Accident? : </label>&nbsp;<span class="preview" id="pauto_accident"></span></div>
								<div class="col-md-4 preview_col"><label>Other Accident? : </label>&nbsp;<span class="preview" id="pother_accident"></span></div>	
								<div class="col-md-4 preview_col"><label>Place(State) : </label>&nbsp;<span class="preview" id="paccident_location"></span></div>
								<div class="col-md-6 preview_col"><label>Claim Codes (Designated by NUCC) : </label>&nbsp;<span class="preview" id="pclaim_code"></span></div>	
								<div class="col-md-6 preview_col"><label>Date of Illness, Injury or Pregnancy(LMP) : </label>&nbsp;<span class="preview" id="pinjury_date"></span></div>
								<div class="col-md-4 preview_col"><label>Other Date QUAL : </label>&nbsp;<span class="preview" id="pqual_date"></span></div>	
								<div class="col-md-8 preview_col"><label>Dates patient unable to work in current occupation : </label>&nbsp;<span class="preview" id="pnonworking_dates"></span></div>	
								<div class="col-md-8 preview_col"><label>Hospitalization dates related to current services : </label>&nbsp;<span class="preview" id="phospital_dates"></span></div>
								<div class="col-md-4 preview_col"><label>Outside Lab? : </label>&nbsp;<span class="preview" id="pout_lab"></span></div>
								<div class="col-md-8 preview_col"><label>Additional Claim Information (Designated by NUCC) : </label>&nbsp;<span class="preview" id="padditional_claim_information"></span></div>	
								<div class="col-md-4 preview_col"><label>Total Charge : </label>&nbsp;<span class="preview" id="ptotal_charge"></span></div>																		
							</div>
						</fieldset>
					</div>					
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Back</button>
				<button type="button" class="submit_claim btn btn-outline-dark">Update</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Rendered services Details</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form id="claim_add_value" class="claim_add_value">
			<div class="modal-body">
			<div class="row">
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us-date" name="service_start_date" id="service_start_date" placeholder="Service Start Date" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control us-date" name="service_end_date"  id="service_end_date" placeholder="Service End Date" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="service_place"  onkeypress="allowNumeric(event)" maxlength="2" id="service_place" placeholder="Place of Service" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="emg"  id="emg" onkeypress="allowAlphabets(event)" maxlength="2" placeholder="EMG" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="cpt_hcps"  maxlength="6" onkeypress="allowAlphaNumeric(event)"  id="cpt_hcps" placeholder="CPT/HCPCS" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control modifier" name="modifier" onkeypress="allowNumeric(event)"  id="modifier" maxlength="8" placeholder="Modifier" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="diagnosis_pointer"  maxlength="4" onkeypress="return ((event.charCode >= 97 && event.charCode <= 108)||(event.charCode >= 65 && event.charCode <= 76))" id="diagnosis_pointer" placeholder="Diagnosis Pointer" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="charges"  id="charges" placeholder="Charges" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="days"  oninput="validate(this)" maxlength="3" id="days" placeholder="Days or Units" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="epsdt"  onkeypress="allowAlphaNumeric(event)"  id="epsdt" placeholder="EPSDT" >
					</div>
					<div class="form-group col-lg-4 col-md-4">
						<input type="text" class="form-control" name="fplan"  onkeypress="allowAlphabets(event)"  maxlength="1"  id="fplan" placeholder="Family Plan" >
					</div>
					<div class="form-group col-lg-3 col-md-3">
						<input type="text" class="form-control" name="id_qual"  onkeypress="allowAlphaNumeric(event)"  id="id_qual" placeholder="ID" >
					</div>
					<div class="form-group col-lg-6 col-md-6">
						<input type="text" class="form-control" name="provider_id"  onkeypress="allowAlphaNumeric(event)" id="provider_id" placeholder="Rendering Provider Id">
						<input type="hidden" class="form-control" name="vid"   value="" id="mvid" >
					</div>
					<div class="form-group col-lg-3 col-md-3">
						<input type="text" class="form-control" name="cnpi"  id="cnpi" onkeypress="allowNumeric(event)"  placeholder="NPI" >
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="button" class="rtrt btn btn-outline-dark" >Add</button>
			</div>
			</form>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function(){
	var status= $('input[name="status_id"]').val();
	if(status>0){
		$(".save_claim").closest('li').css("display", "none");
	}
	$(".reset_claim_form").closest('li').css("display", "none");
});
$('body').on('click', '.removeRecord', function() {
	var vid = $(this).attr('vid');
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
				$.ajax({
					type:'POST',
					url:"<?php echo base_url('admin/claims/del_claim_value');?>",
					data:{'id':vid},
					success: function(data){
						toastr.success("", "Render Service values Deleted Successfully", {
						timeOut: 5000,
						closeButton: !0,
						debug: !1,
						newestOnTop: !0,
						progressBar: !0,
						positionClass: "toast-top-right",
						preventDuplicates: !0,
						onclick: null,
						showDuration: "300",
						hideDuration: "1000",
						extendedTimeOut: "1000",
						showEasing: "swing",
						hideEasing: "linear",
						showMethod: "fadeIn",
						hideMethod: "fadeOut",
						tapToDismiss: !1
						})
						$('.rs'+vid).remove();
					}
				});
			}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});

</script>