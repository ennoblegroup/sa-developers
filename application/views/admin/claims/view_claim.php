
<style>
fieldset h4 {
    font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:5px 0px;
	border-bottom:none;
	text-decoration: underline;
}
fieldset label{
	font-weight:bold;
	font-size: 13px;
	margin: 0.25rem 0;
}
.preview{
	font-size: 13px;
}
legend.scheduler-border {
	font-size: 1.2em !important;
	font-weight: bold !important;
	text-align: left !important;
	width:auto;
	padding:0 10px;
	border-bottom:none;
}
.preview_col{
	padding:0px;
	border-bottom: 1px solid #ccc2c2;
}
.box-row { 
	display:table-row; 
} 
 .box-cell { 
	display:table-cell; 
	width:50%; 
	padding:10px; 
} 
 .box-cell.box1 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
	} 
 .box-cell.box2 { 
	text-align:justify;
	border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
} 
.uuu{
	padding:0 5px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/claims">Claims</a></li>
						<li class="breadcrucb-item active">View Claim</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL CLAIMS</h4>
					</div-->
					<div class="card-body custom_body">
                        <div class="box-row row">
                            <div class="box-cell box1 form-group col-lg-6 col-md-6">
                                <fieldset class="scheduler-border">
                                    <div class="row uuu">
                                        <div class="col-md-12 preview_col"><h4>PATIENT & INSURED INFORMATION:</h4></div>
                                        <div class="col-md-4 preview_col"><label>Last Name : </label><span class="preview" id="plast_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>First Name : </label><span class="preview" id="pfirst_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Middle Name : </label><span class="preview" id="pmiddle_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Date of Birth : </label><span class="preview" id="ppatient_dob"></span></div>
                                        <div class="col-md-6 preview_col"><label>Phone Number : </label><span class="preview" id="pphone_number"></span></div>								
                                        <div class="col-md-2 preview_col"><label>Sex : </label><span class="preview" id="pgender"></span></div>
                                        <div class="col-md-6 preview_col"><label>Address1 : </label><span class="preview" id="paddress1"></span></div>
                                        <div class="col-md-6 preview_col"><label>Address2 : </label><span class="preview" id="paddress2"></span></div>
                                        <div class="col-md-4 preview_col"><label>City : </label><span class="preview" id="pcity"></span></div>
                                        <div class="col-md-4 preview_col"><label>State : </label><span class="preview" id="pstate"></span></div>
                                        <div class="col-md-4 preview_col"><label>Zip Code : </label><span class="preview" id="pzip_code"></span></div>
                                        <div class="col-md-6 preview_col"><label>Account Number : </label><span class="preview" id="paccount_number"></span></div>
                                        <div class="col-md-6 preview_col"><label>Patient relationship to Insured : </label><span class="preview" id="prelation"></span></div>
                                        <div class="col-md-6 preview_col"><label>Patient's Signature : </label><span class="preview" id="ppatient_signature"></span></div>
                                        <div class="col-md-6 preview_col"><label>Insured's Signature : </label><span class="preview" id="pinsured_signature"></span></div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="box-cell box2 form-group col-lg-6 col-md-6">
                                <fieldset class="scheduler-border">
                                    
                                    <div class="row uuu">
                                        <div class="col-md-12 preview_col"><h4>INSURANCE INFORMATION:</h4></div>
                                        <div class="col-md-12 preview_col"><label>Insured Check : </label><span class="preview" id="plasct_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Last Name : </label><span class="preview" id="pinsured_last_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>First Name : </label><span class="preview" id="pinsured_first_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Middle Name : </label><span class="preview" id="pinsured_middle_name"></span></div>
                                        <div class="col-md-6 preview_col"><label>Address1 : </label><span class="preview" id="pinsured_address1"></span></div>
                                        <div class="col-md-6 preview_col"><label>Address2 : </label><span class="preview" id="pinsured_address2"></span></div>
                                        <div class="col-md-4 preview_col"><label>City : </label><span class="preview" id="pinsured_city"></span></div>
                                        <div class="col-md-4 preview_col"><label>State : </label><span class="preview" id="pinsured_state"></span></div>
                                        <div class="col-md-4 preview_col"><label>Zip Code : </label><span class="preview" id="pinsured_zip_code"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Insured Last Name : </label><span class="preview" id="pother_insured_last_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Insured First Name : </label><span class="preview" id="pother_insured_first_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Insured Middle Name : </label><span class="preview" id="pother_insured_middle_name"></span></div>
                                        <div class="col-md-6 preview_col"><label>Insured's Policy Group/FECA number : </label><span class="preview" id="pinsured_group_number"></span></div>
                                        <div class="col-md-6 preview_col"><label>Other Insured's Policy/Group Number : </label><span class="preview" id="pother_group_number"></span></div>
                                        <div class="col-md-6 preview_col"><label>Insurance Plan Name/Program Name : </label><span class="preview" id="pinsurance_plan_name"></span></div>
                                        <div class="col-md-6 preview_col"><label>Other Insurance Plan Name/Program Name : </label><span class="preview" id="pother_insurance_plan_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Insured's ID Number : </label><span class="preview" id="pinsured_id_number"></span></div>
                                        <div class="col-md-4 preview_col"><label>Insured's Date of Birth : </label><span class="preview" id="pinsured_dob"></span></div>
                                        <div class="col-md-4 preview_col"><label>Sex : </label><span class="preview" id="pinsured_sex"></span></div>
                                        <div class="col-md-5 preview_col"><label>Claim ID (Designated by NUCC) : </label><span class="preview" id="pclaim_id"></span></div>
                                        <div class="col-md-7 preview_col"><label>Is there another health benefit plan? : </label><span class="preview" id="pother_benefit_plan"></span></div>								
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="box-row row">
                            <div class="box-cell box1 form-group col-lg-6 col-md-6">
                            <fieldset class="scheduler-border">
                                    <div class="row uuu">
                                        <div class="col-md-12 preview_col"><h4>PHYSICIAN/SUPPLIER INFORMATION:</h4></div>
                                        <div class="col-md-4 preview_col"><label>Name of Referring Provider : </label><span class="preview" id="preffering_name"></span></div>
                                        <div class="col-md-4 preview_col"><label>Federal Tax I.D. Number : </label><span class="preview" id="pfederal_tax_number"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Id : </label><span class="preview" id="pother_id"></span></div>
                                        <div class="col-md-4 preview_col"><label>NPI : </label><span class="preview" id="pnpi"></span></div>
                                        <div class="col-md-4 preview_col"><label>Total Charge : </label><span class="preview" id="ptotal_amount"></span></div>
                                        <div class="col-md-4 preview_col"><label>Amount Paid : </label><span class="preview" id="ppaid_amount"></span></div>
                                        <div class="col-md-6 preview_col">
                                            <div class="row" style="margin:0px">
                                                <div class="col-md-12"><h6>Service Details</h6></div>
                                                <div class="col-md-6 preview_col"><label>Location Name : </label><span class="preview" id="pservice_location_name"></span></div>
                                                <div class="col-md-6 preview_col"><label>Phone Number : </label><span class="preview" id="pservice_phone_number"></span></div>
                                                <div class="col-md-6 preview_col"><label>Address1 : </label><span class="preview" id="pservice_address1"></span></div>
                                                <div class="col-md-6 preview_col"><label>Address2 : </label><span class="preview" id="pservice_address2"></span></div>
                                                <div class="col-md-4 preview_col"><label>City : </label><span class="preview" id="pservice_city"></span></div>
                                                <div class="col-md-4 preview_col"><label>State : </label><span class="preview" id="pservice_state"></span></div>
                                                <div class="col-md-4 preview_col"><label>Zip Code : </label><span class="preview" id="pservice_zip_code"></span></div>
                                                <div class="col-md-6 preview_col"><label>Other Id : </label><span class="preview" id="pservice_other_id"></span></div>
                                                <div class="col-md-6 preview_col"><label>NPI : </label><span class="preview" id="pservice_npi"></span></div>								
                                            </div>
                                        </div>
                                        <div class="col-md-6 preview_col">
                                            <div class="row" style="margin:0px">
                                                <div class="col-md-12 "><h6>admin Details</h6></div>
                                                <div class="col-md-6 preview_col"><label>Location Name : </label><span class="preview" id="padmin_location_name"></span></div>
                                                <div class="col-md-6 preview_col"><label>Phone Number : </label><span class="preview" id="padmin_phone_number"></span></div>
                                                <div class="col-md-6 preview_col"><label>Address1 : </label><span class="preview" id="padmin_address1"></span></div>
                                                <div class="col-md-6 preview_col"><label>Address2 : </label><span class="preview" id="padmin_address2"></span></div>
                                                <div class="col-md-4 preview_col"><label>City : </label><span class="preview" id="padmin_city"></span></div>
                                                <div class="col-md-4 preview_col"><label>State : </label><span class="preview" id="padmin_state"></span></div>
                                                <div class="col-md-4 preview_col"><label>Zip Code : </label><span class="preview" id="padmin_zip_code"></span></div>
                                                <div class="col-md-6 preview_col"><label>Other Id : </label><span class="preview" id="padmin_other_id"></span></div>
                                                <div class="col-md-6 preview_col"><label>NPI : </label><span class="preview" id="padmin_npi"></span></div>								
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="box-cell box2 form-group col-lg-6 col-md-6">
                                <fieldset class="scheduler-border">
                                    <div class="row uuu">
                                        <div class="col-md-12 preview_col"><h4>CLAIM INFORMATION:</h4></div>
                                        <div class="col-md-12 preview_col"><label> Is Patient's condition related to Employment (Current or Previous) </label><span class="preview" id="pemployment"></span></div>
                                        <div class="col-md-4 preview_col"><label>Auto Accident? : </label><span class="preview" id="pauto_accident"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Accident? : </label><span class="preview" id="pother_accident"></span></div>	
                                        <div class="col-md-4 preview_col"><label>Place(State) : </label><span class="preview" id="paccident_location"></span></div>
                                        <div class="col-md-6 preview_col"><label>Claim Codes (Designated by NUCC) : </label><span class="preview" id="pclaim_code"></span></div>	
                                        <div class="col-md-6 preview_col"><label>Date of Illness, Injury or Pregnancy(LMP) : </label><span class="preview" id="pinjury_date"></span></div>
                                        <div class="col-md-4 preview_col"><label>Other Date QUAL : </label><span class="preview" id="pqual_date"></span></div>	
                                        <div class="col-md-8 preview_col"><label>Dates patient unable to work in current occupation : </label><span class="preview" id="pnonworking_dates"></span></div>	
                                        <div class="col-md-8 preview_col"><label>Hospitalization dates related to current services : </label><span class="preview" id="phospital_dates"></span></div>	
                                        <div class="col-md-4 preview_col"><label>Total Charge : </label><span class="preview" id="ptotal_charge"></span></div>
                                        <div class="col-md-8 preview_col"><label>Additional Claim Information (Designated by NUCC) : </label><span class="preview" id="padditional_claim_information"></span></div>							
                                        <div class="col-md-4 preview_col"><label>Outside Lab? : </label><span class="preview" id="pout_lab"></span></div>																		
                                    </div>
                                </fieldset>
                            </div>					
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        view_data();
    });
	function view_data()
    {
        var id= '<?php echo $claim_id ?>';
        $.ajax({
            url:"../get_claim_by_id",
            method:"POST",
            async: true,
            dataType:"json",
            data:{id:id},
            success: function(data)
            {
                console.log(data);
                $.each(data, function( index, value ) {
                var ff= index;
                $("#p"+ff).html(value);
                });
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
		$("#preloader").css("display","none");
	}
	
</script>