
<style>
	.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: -webkit-fill-available;
}
.dataTables_scroll {
   padding: 0;
}
table.dataTable thead th, table.dataTable thead td {
    padding: 5px 5px;
    border-bottom: 1px solid #111;
}

table.dataTable tbody th, table.dataTable tbody td {
    padding: 5px 5px !important;
}
table.dataTable tbody{
	min-height: 350px !important;
	max-height: 350px !important;
	overflow-y: scroll !important;
    overflow-x: hidden !important;
}
.bootstrap-select .btn {
    border: 1px solid #eaeaea !important;
    background-color: #f8f9fa !important;
    padding: 5px 5px;
}
hr {
    margin-top: 0px;
    margin-bottom: 5px;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
}
.external-event {
    cursor: move;
    margin: 0;
    padding:0;
}
.blocked
{
	pointer-events: none;
}
.blocked button:hover {
    cursor:not-allowed
 }
.hide{
	display:none;
}
td .dropdown-menu.show{
	display:flex;
	right: 0px;
    left: unset !important;
}
td .dropdown-menu.show a{
	padding:0px 3px;
}
td .custom-dropdown .dropdown-menu {
    background: #e7e3e3;
	border: 1px solid #8b8383;
}
.widget-timeline .timeline > li > .timeline-panel {
    padding: 5px 5px;
}
.widget-timeline .timeline > li > .timeline-panel a h6{
    font-size: 13px;
}
#status_history .modal-dialog {
    max-width: 700px;
    margin: 1.75rem auto;
}
.widget-timeline .timeline:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 2px;
    left: 12px;
    margin-right: -1.5px;
    background: #f5f5f5;
}
.widget-timeline .timeline > li > .timeline-badge {
    border-radius: 50%;
    height: 15px;
    left: 5px;
    position: absolute;
    top: 10px;
    width: 15px;
}
.widget-timeline .timeline > li > .timeline-panel {
    left: -10px;
}
.shstatus{
	margin-left: 269px;
    width: 100%;
}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumcb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrucb-item active">All Claims</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<span class="cact">
						<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
							<a  id="act_generate" title='Generate Report' ><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-file-pdf-o'></i></button></a>
							<a  id="act_view_reports" title='Report Versions'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-files-o' aria-hidden='true'></i></button></a>								
							<a  id="act_update_status" title='Update Status' href='javascript:void(0)' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-hourglass-start' aria-hidden='true'></i></button></a>
						<?php }?>
						<a  id="act_status_history" title='View Status History' href='javascript:void(0)' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-history' aria-hidden='true'></i></button></a>
						<a  id="act_edit" title='Edit Claim'  class='".$edit_class."'><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-pencil' aria-hidden='true'></i></button></a>
						<a  id="act_view" title='View Claim' ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-eye' aria-hidden='true'></i></button></a>
						<a  id="act_chat" title='Conversation'  ><button type='button' class='btn btn-sm btn-outline-dark' ><i class='fa fa-comment' aria-hidden='true'></i></button></a>
						
					</span>
					<a  id="act_submit" title='Submit'><button type='button' class='btn btn-sm btn-outline-dark'><i class='fa fa-paper-plane' aria-hidden='true'></i></button></a>
					<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
						<a  href="javascript:void(0)" class="multiple_claim_generate" title="Generate Report" ><button type="button" class="btn btn-outline-dark"><i class="fa fa-file-pdf-o"></i>&nbsp;Generate Report</button></a>&nbsp;
					<?php }?>
					<a href="<?= base_url('admin/claims/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Claim</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">ALL CLAIMS</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="row" style="">																
							<div class="form-group phar col-lg-2 col-md-2" <?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo 'style="display:none"';}?> >
								<select name="client_id[]" multiple id="client_id" title="All clients" <?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo '';}?> data-live-search="true"  class="selectpicker form-control" >
									<?php $i=1; foreach ($all_clients as $key=>$phar){ ?>
									<option value="<?php echo $phar['id']?>" <?php if($phar['id']== $client_id || $phar['id']==$_SESSION['sadevelopers_admin']['client_id']){ echo 'selected'; }?>><?php echo $phar['client_lbn_name']?></option>
									<?php $i++; }?>
								</select>
							</div>

							<div class="form-group col-lg-3 col-md-3">
								<select name="insurance_company_id[]" multiple="multiple"  title="All Insurance Companies" data-live-search="true" id="insurance_company_id" class="selectpicker form-control" >
									<?php $i=1; foreach ($all_insurance_companies as $key=>$phar){ ?>
									<option value="<?php echo $phar['id']?>"><?php echo $phar['insurance_company_name']?></option>
									<?php $i++; }?>
								</select>
							</div>
							<div class="form-group col-lg-2 col-md-2">
								<select name="status_id[]" multiple="multiple" title="All Status" id="status_id" data-live-search="true" class="selectpicker form-control" >
								<optgroup label="Claim Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==1){?>
										<option cg="1" value="<?php echo $cs['claim_status_id']?>"><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>
								<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
								<optgroup label="Transaction Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==2){ ?>
										<option  cg="2" value="<?php echo $cs['claim_status_id']?>"><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>		
								<optgroup label="Payment Status" >
									<?php $i=1; foreach ($all_claim_status as $key=>$cs){ if($cs['claim_status_group']==3){?>
										<option cg="3" value="<?php echo $cs['claim_status_id']?>"><?php echo $cs['claim_status_name']?></option>
									<?php $i++; }}?>
								</optgroup>
								<?php }?>
								</select>
							</div>
							<div class="form-group col-lg-3 col-md-3">
							<input type="text" class="form-control daterange" name="datefilter" id="dates" placeholder="Date Range" >
							</div>
							<?php if($_SESSION['sadevelopers_admin']['client_id']>0){echo '<div class="form-group col-lg-2 col-md-2"></div>';}?>
							<div class="form-group col-lg-2 col-md-2">
							<a style="float:right" href="javascript:void(0)" onclick="reset_data()"  ><button type="button" class="btn btn-outline-dark">Reset</button></a>
							<a style="float:right;margin-right: 10px;" href="javascript:void(0)" onclick="filter_data()"  ><button type="button" class="btn btn-outline-dark">Apply Filter</button></a>
							</div>
						</div>
						<hr>
						<div class="">
							<table id="claim_table" class="" style="">
								<thead>
									<tr>
										<th scope="col">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="checkAll">
												<label class="custom-control-label" for="checkAll"></label>
											</div>
										</th>
										<th scope="col">Claim Id</th>
										<th  scope="col">client</th>
										<th scope="col">Insurance</th>
										<th scope="col">Patient</th>
										<th scope="col">Claim Status</th>
										<th scope="col">Transaction Status</th>
										<th scope="col">Payment Status</th>
										<th style='text-align:right' scope="col">Actions</th>
									</tr>
								</thead>
								<tbody id="filter_data">
																	
								</tbody>
							</table>
							<div class="col-md-12 text-center">
								<ul class="pagination pagination-lg pager" id="myPager"></ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Update Claim Status</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<form  method="POST" action="<?= base_url('admin/claims/add_claim_status_history'); ?>" id="claim_status_update" class="claim_status_update">
			<div class="modal-body">
				<div class="row">																
					<div class="col-md-6 form-group">
						<select name="status_group" id="status_group" class="form-control selectpicker" >
							<option value="">Select Status Group</option>
							<option value="1">Claim Status</option>
							<option value="2">Transactional Status</option>
							<option value="3">Payment Status</option>
						</select>
						<input type="hidden" value="" id="claim_id" name="claim_id">
						<input type="hidden" value="" id="sid" name="sid">
						<input type="hidden" value="" id="tid" name="tid">
						<input type="hidden" value="" id="pid" name="pid">
					</div>
					<div class="col-md-6 form-group">
						<select name="status_name" id="status_name" class="form-control selectpicker" >

						</select>
					</div>
					<div class="col-md-12 form-group">
						<textarea class="form-control" name="status_description" placeholder="Status Description"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-outline-dark">Update Status</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="status_history">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status History</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!--div class="row">																
					<div class="col-md-4 form-group">
						<h6 class="">Claim Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s1">
								
							</ul>
						</div>						
					</div>
					<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
					<div class="col-md-4 form-group">
						<h6 class="">Transaction Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s2">
								
							</ul>
						</div>						
					</div>
					<div class="col-md-4 form-group">
						<h6 class="">Payment Status</h6>
						<div id="DZ_W_TimeLine" class="widget-timeline dz-scroll" style="height:370px;">
							<ul class="timeline s3">
								
							</ul>
						</div>						
					</div>
					<?php }?>
				</div-->
				<div class="row" style="border: 2px solid grey;margin-bottom: 10px;">
					<div class="col-md-4 form-group">
						<h6 class="shstatus">Claim Status</h6>
						<div id="claim_status_div"></div>
						
						<input type="hidden" value="44" id="csval" name="csval">
					</div>
				</div>
				<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
				<div class="row" style="border: 2px solid grey;margin-bottom: 10px;">
					<div class="col-md-4 form-group">
						<h6 class="shstatus">Transaction Status</h6>
						<div id="transaction_status_div"></div>							
					</div>
				</div>
				<div class="row" style="border: 2px solid grey;margin-bottom: 10px;">
					<div class="col-md-4 form-group">
						<h6 class="shstatus">Payment Status</h6>
						<div id="payment_status_div"></div>							
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){				
		$('#status_group').on('change',function(){
			var id = $(this).val();
			var sid=$('#sid').val();
			if(id){
				$.ajax({
					type:'POST',
					url:'claims/get_all_claim_status',
					data:'id='+id,
					success:function(data){
						
						$('#status_name').html('<option value="">Select Status</option>'); 
						var dataObj = jQuery.parseJSON(data);
						if(dataObj){
							$(dataObj).each(function(){
								var option = $('<option />');
								option.attr('value', this.claim_status_id).text(this.claim_status_name);
								if(id==1 && (this.claim_status_id==0 || this.claim_status_id ==1)){
									option.attr('disabled', 'disabled');
								}
								$('#status_name').append(option);
							});
							$("#status_name").selectpicker("refresh");
						}else{
							$('#status_name').html('<option value="" selected disabled>Status not available</option>');
							$("#status_name").selectpicker("refresh");
						}
						$("#status_name").selectpicker("refresh");
					}
				}); 
			}else{
				$('#status_name').html('<option value="">Select Status Group first</option>');
				$("#status_name").selectpicker("refresh");
			}
			
		});
		
		
	});
	function openUpdateStatus(id,sid,tid,pid) {
		$("#claim_id").val(id);
		$("#sid").val(sid);
		$("#tid").val(tid);
		$("#pid").val(pid);
		$('#exampleModalLong').modal('show');
	}
	function qwert(id){
		openStatusHistory1(id,1);
		openStatusHistory2(id,2);
		openStatusHistory3(id,3);
		$('#status_history').modal('show');
	}
	function openStatusHistory(id,type) {
		$.ajax({
            url:"claims/claim_status_history",
            method:"POST",
            async: true,
            dataType:"json",
            data:{id:id,type:type},
            success: function(data)
            {
				
                $('.s'+type).html(data.claim_list);
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
		
	}
	function reset_data()
    {		
		$('#insurance_company_id').val('');
		<?php if($_SESSION['sadevelopers_admin']['client_id']==0){?>
		$('#client_id').val('');
		<?php }?>
		$('#status_id').val('');
		$('#dates').val('');
		$("select").selectpicker("refresh");
		filter_data();
		
	}
	
	function filter_data()
    {		
		$("#preloader").css("display","block");
		var insurance_company_id =$('#insurance_company_id option:selected').toArray().map(item => item.value);
		insurance_company_id = $.grep(insurance_company_id,function(n){
			return(n);
		});
		var client_id = $('#client_id option:selected').toArray().map(item => item.value);
		client_id = $.grep(client_id,function(n){
			return(n);
		});
		var status_id = $('#status_id option:selected').toArray().map(item => item.value);
		status_id = $.grep(status_id,function(n){
			return(n);
			
		});
        var dates=$('#dates').val();
		if(insurance_company_id.length==0){
			insurance_company_id=[].toString();
		}
		if(client_id.length==0){
			client_id=[].toString();
		}
		if(status_id.length==0){
			status_id=[].toString();
		}
        $.ajax({
            url:"claims/filter_claims",
            method:"POST",
            async: true,
            dataType:"json",
            data:{insurance_company_id:insurance_company_id,client_id:client_id,status_id:status_id,dates:dates},
            success: function(data)
            {	
				$('table').DataTable().clear();
				$('table').DataTable().destroy();			
				$('#filter_data').html(data.claim_list);				
				tableinit();
				
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
		})
		$("#preloader").css("display","none");
	}
	function tableinit(){
		var tt= $('table').DataTable({
			"bRetrieve": true,
			"bProcessing": true,
			"bDestroy": false,
			"bPaginate": true,
			"bAutoWidth": true,
			"bFilter": true,
			"bInfo": true,
			"bJQueryUI": true,
			columnDefs: [
				{ orderable: false, targets: -1 }
			],
			language: {
			"emptyTable": "No claims were found."
			}
		});
		$('table').find('thead tr th').css('width', 'auto');
		<?php if($_SESSION['sadevelopers_admin']['client_id']>0){?>
		tt.column(0).visible(false);
		tt.column(2).visible(false);
		tt.column(6).visible(false);
		tt.column(7).visible(false);
		<?php }?>
	}
</script>
<script>
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBackgroundColor);
function drawBackgroundColor() { 
  var data = new google.visualization.DataTable();
      data.addColumn('string', 'Date');
      data.addColumn('number', 'Status');
		data.addColumn({type: 'string', role: 'tooltip'});
		
      data.addRows([
		<?php 		
		$this->load->model('admin/claim_model', 'claim_model');
		$result=$this->claim_model->claim_status_history_id(44,1);		
		foreach($result as $cs){	
		//echo $cs['claim_status_id'];
		//echo $cs['claim_status_name'];
		//$new_date = $this->timeago($cs['create_date']);	
		$str=explode(" ",$cs['create_date']);
		 echo '["'.$str[0].'",'.$cs['claim_status_id'].', "'.$cs['claim_status_name'].'"],'; 
		 } ?>
        ]);
      var options = {
		legend:{position:'bottom'},
		curveType:'none',
		interpolateNulls:true,
        hAxis: {
          title: 'Date',
		  format: 'date',
		  slantedText:'true'
        },
        vAxis: {
          title: 'Status',
		  textPosition: 'in'
        },
		isStacked: true,
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
      };

      var chart = new google.visualization.LineChart(document.getElementById('claim_status_div9'));
      chart.draw(data, options);
    }
	</script>
	<script>
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBackgroundColor);
function drawBackgroundColor() { 
  var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Status');
		data.addColumn({type: 'string', role: 'tooltip'});
      data.addRows([
		<?php 
		$this->load->model('admin/claim_model', 'claim_model');
		$result=$this->claim_model->claim_status_history_id(44,2);		
		foreach($result as $cs){	
		//echo $cs['claim_status_id'];
		//echo $cs['claim_status_name'];
		//$new_date = $this->timeago($cs['create_date']);	
		$str=explode(" ",$cs['create_date']);
		 echo '["'.$str[0].'",'.$cs['claim_status_id'].', "'.$cs['claim_status_name'].'"],'; 
		 } ?>
		]);
      var options = {
        hAxis: {
          title: 'Date',
		  format: 'date',
        },
        vAxis: {
          title: 'Status'
        },
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
      };

      var chart = new google.visualization.LineChart(document.getElementById('transaction_status_div9'));
      chart.draw(data, options);
    }
	</script>
	<script>
google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBackgroundColor);
function drawBackgroundColor() { 
 var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Status');
	  data.addColumn({type: 'string', role: 'tooltip'});
      data.addRows([
		<?php 
		$this->load->model('admin/claim_model', 'claim_model');
		$result=$this->claim_model->claim_status_history_id(44,3);		
		foreach($result as $cs){	
		//echo $cs['claim_status_id'];
		//echo $cs['claim_status_name'];
		//$new_date = $this->timeago($cs['create_date']);	
		$str=explode(" ",$cs['create_date']);
		 echo '["'.$str[0].'",'.$cs['claim_status_id'].', "'.$cs['claim_status_name'].'"],'; 
		 } ?>
        ]);
      var options = {		  
        hAxis: {
          title: 'Date',
		  format: 'date',
        },
        vAxis: {
          title: 'Status'
        },
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
      };

      var chart = new google.visualization.LineChart(document.getElementById('payment_status_div9'));
      chart.draw(data, options);
	}

	</script>
	<script type="text/javascript">
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback();

function openStatusHistory1(id,type)
{	 	
    var temp_title = 'Claim Status';
    $.ajax({
        url:"claims/claim_status_history1",
        method:"POST",
        data:{id:id,type:type},
        dataType:"JSON",
        success:function(data)
        {	
            drawMonthwiseChart1(data, temp_title);
        }
    });
}

function drawMonthwiseChart1(chart_data, chart_main_title)
{
    var jsonData = chart_data;
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'status');
    data.addColumn('number', 'Days');
    $.each(jsonData, function(i, jsonData){
        var status = jsonData.status;
        var days = parseFloat($.trim(jsonData.days));
        data.addRows([[status,days],]);
    });
    var options = {
        //title:chart_main_title,
        hAxis: {
            title: "Status"
        },
        vAxis: {
            title: 'Days'
        },
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
    };

    var chart = new google.visualization.LineChart(document.getElementById('claim_status_div'));
    chart.draw(data, options);
}

</script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback();

function openStatusHistory2(id,type)
{	 
    var temp_title = 'Transaction Status';
    $.ajax({
        url:"claims/claim_status_history2",
        method:"POST",
        data:{id:id,type:type},
        dataType:"JSON",
        success:function(data)
        {			
            drawMonthwiseChart2(data, temp_title);
        }
    });
}

function drawMonthwiseChart2(chart_data, chart_main_title)
{
    var jsonData = chart_data;
    var data = new google.visualization.DataTable();
     data.addColumn('string', 'status');
     data.addColumn('number', 'Days');
    $.each(jsonData, function(i, jsonData){
       var status = jsonData.status;
       var days = parseFloat($.trim(jsonData.days));
       data.addRows([[status,days],]);
    });
    var options = {
       // title:chart_main_title,
        hAxis: {
            title: "Status"
        },
        vAxis: {
            title: 'Days'
        },
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
    };

    var chart = new google.visualization.LineChart(document.getElementById('transaction_status_div'));
    chart.draw(data, options);
}

</script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback();

function openStatusHistory3(id,type)
{	 
    var temp_title = 'Payment Status';
    $.ajax({
        url:"claims/claim_status_history3",
        method:"POST",
        data:{id:id,type:type},
        dataType:"JSON",
        success:function(data)
        {			
            drawMonthwiseChart3(data, temp_title);
        }
    });
}

function drawMonthwiseChart3(chart_data, chart_main_title)
{
    var jsonData = chart_data;
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'status');
     data.addColumn('number', 'Days');
    $.each(jsonData, function(i, jsonData){
        var status = jsonData.status;
       var days = parseFloat($.trim(jsonData.days));
       data.addRows([[status,days],]);
    });
    var options = {
        //title:chart_main_title,
       hAxis: {
            title: "Status"
        },
        vAxis: {
            title: 'Days'
        },
		'width': 650,
        'height': 250,
        backgroundColor: '#f1f8e9'
    };

    var chart = new google.visualization.LineChart(document.getElementById('payment_status_div'));
    chart.draw(data, options);
}

</script>
