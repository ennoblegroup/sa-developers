<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',19);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==1){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
?>
<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Contact Info</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Contact Us</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>
										<th>Address</th>
										<th>Email Address</th>
										<th>Phone Number</th>										
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; foreach ($all_contactus as $key=>$contact){ 				
								?>
								<tr>
									<td><?php echo $contact['address1']?><br>
										<?php if($contact['address2'] !=''){?>
										<?php echo $contact['address2']?><br>
										<?php }?>
										<?php echo $contact['city']?>, <?php echo $contact['state']?> <?php echo $contact['zip']?>
									 </td>
									 <td> <?php echo $contact['email']?></td>
									<td> <?php echo $contact['phone']?></td>
									<td> <a href="<?= base_url('admin/contactus/edit/'.$contact['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark" <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></button> </a>
									<!--a href="#" class="removeRecord" contact_id = "<?php echo $contact['id'];?>"><span class="btn btn-sm btn-red"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></span></a--> </td>
								</tr>
								<?php $i++; }?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>   
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var contact_id = $(this).attr('contact_sid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/contactus/update_status');?>",
			  data: {id: id, contact_id: contact_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+contact_id).load(" #status_roww"+contact_id);
			  }
			});
		});
	});
</script>
<script type="text/javascript">
    $(".removeRecord").click(function(){
       var id = $(this).attr('contact_id');
       swal({
        title: "Are you sure?",
        text: "Do you really want to delete the Contact us? This process cannot be undone.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-outline-dark",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!"
     }). then((result) => {
        if (result.value) {
         $.ajax({
			 url:"<?php echo base_url('admin/contactus/del');?>",
			type:'POST',			
			data:{'id':id},
			success: function(data){
				//swal("Deleted!", "Insurance Company has been deleted.", "success");
				location.reload();
				}
			});
        } else {
          swal("Cancelled", "Contact Us is safe :)", "error");
        }
      })
     
    });
</script>
<script>
/*$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: '<i class="fa fa-check" aria-hidden="true"></i>Confirm',
            btnClass: 'form btn btn-red',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/insurance_companies/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: '<i class="fa fa-ban"></i>cancel',
            btnClass: 'form btn btn-grey',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
                
            }
            else {
                var id = 0;
            }
			var user_id = $(this).attr('user_id');
			//var email = $(this).attr('email');
			$.ajax({
			  type: "POST",
			  url: "<?PHP echo base_url('Admin/update_user_status'); ?>",
			  data: {id: id, user_id: user_id},
			  cache: false,
			  success: function(data){
				$("#status_row"+user_id).load(" #status_row"+user_id);
			  }
			});
		});
	});*/
</script>
<script>
$("#nav_contactus").addClass('active');
</script> 