<?php echo form_open(base_url('admin/slides/add'), 'id="quickForm"  enctype="multipart/form-data" class="form-horizontal"');  ?> 
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/slides">Sliders</a></li>
						<li class="breadcrumb-item active">Add Slider</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
				<input type="submit" name="submit" value="Submit" class="btn btn-outline-dark">&nbsp;
				<a type="button" href="<?php echo base_url()?>admin/slides" class="form btn btn-outline-dark">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Sliders</h4>
					</div>
					<div class="card-body custom_body">
						<div class="form-validation">
							<div class="row">								
								<div class="col-md-12">
									<?php if(isset($msg) || validation_errors() !== ''): ?>
									    <div class="alert alert-danger alert-dismissible">
										    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										    <?= validation_errors();?>
										    <?= isset($msg)? $msg: ''; ?>
									    </div>
									<?php endif; ?>
									<div class="row">																
										<div class="form-group err col-lg-6 col-md-6">
											<input type="text" class="form-control" id="title" placeholder=" Title" name="title" >
										</div>																
										<div class="form-group err col-lg-6 col-md-6">
											<input type="text" class="form-control" id="subtitle" placeholder="Sub Title" name="subtitle" >
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="input-group err row">
												<div class="custom-file">
													<input type="file" onchange="readURLb(this);" class="custom-file-input " id="bimage" name="bimage" accept="image/x-png,image/gif,image/jpeg">
													<label class="custom-file-label">Before Image</label>
												</div>
											</div>
											<div class="row">
												<img id="bblah" style="padding-top:15px;width:100%" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<div class="input-group err row">
												<div class="custom-file">
													<input type="file" onchange="readURL(this);" class="custom-file-input " id="image" name="image" accept="image/x-png,image/gif,image/jpeg">
													<label class="custom-file-label">After Image</label>
												</div>
											</div>
											<div class="row">
												<img id="blah" style="padding-top:15px;width:100%" src="https://www.tjs-cycle.com/assets/images/no-image-selected.gif" alt="your image" />  
											</div>
                                     	</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>		
<?php echo form_close( ); ?>
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
function readURLb(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#bblah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

$(document).ready(function () {
  $('#quickForm').validate({
    rules: {
		title: {
        required: true,
        minlength: 3
      },
	 
      subtitle: {
        required: true,
        minlength: 3
      },
	  image: {
        required: true
        //minlength: 3
      },
    },
    messages: {
	  title: {
        required: "Please enter  Title",
        minlength: "Title must be at least 3 characters long"
      },
      subtitle: {
        required: "Please enter  Sub Title",
        minlength: "Sub Title must be at least 3 characters long"
      },
	  image: {
        required: "Please Choose  Image"
        //minlength: " Description must be at least 3 characters long"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.err').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });	
});
$('body').on('input', 'input', function() {	
	$(this).valid();
	$(this).next('.error').hide();
});
</script>



