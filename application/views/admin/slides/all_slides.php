<?php				
$roleid= $_SESSION['sadevelopers_admin']['role_id'];
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',13);
$this->db->where('permission_id',2);
$status_countc = $this->db->get('roles_permissions')->num_rows();
if($status_countc>0 || $roleid==0){
	$user_classc ='show_cls';
}else{
	$user_classc ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',13);
$this->db->where('permission_id',3);
$status_countu = $this->db->get('roles_permissions')->num_rows();
if($status_countu >0 || $roleid==0){
	$user_classu ='show_cls';
}else{
	$user_classu ='hide_cls';
}
$this->db->where('role_id',$roleid);
$this->db->where('menu_id',13);
$this->db->where('permission_id',4);
$status_countd = $this->db->get('roles_permissions')->num_rows();
if($status_countd >0 || $roleid==0){
	$user_classd ='show_cls';
}else{
	$user_classd ='hide_cls';
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css" type="text/css" media="screen" />
<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Sliders</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
				<!--a href="<?= base_url('admin/slides/add'); ?>"><button type="button"  style="float:right;" class="btn btn-outline-dark <?php echo $user_classc;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Slider</button></a-->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Sliders</h4>
					</div>
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>									
									    <th width="10%">Id</th>
    									<th width="40%">Title</th>
										<th width="15%">Before Image</th>
    									<th width="15%">After Image</th>
                                        <!--th>Display Order</th>
                                        <th>Change Order</th>
    									<th width="10%">Status</th-->
    									<th width="10%">Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($all_slides as $key=>$slides){ 
										
									?>
									<tr>
										<td> <?php echo $slides['slides_id']?> </td>
										
										<td> <?php echo $slides['title']?> </td>
										<td>  <a href="<?php echo base_url();?>images/slides/<?php echo $slides['before_image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
                                            <img src="<?php echo base_url();?>images/slides/<?php echo $slides['before_image']; ?> " style="width: 100px;height: 40px;" alt="<?php echo $slides['title']; ?>"> 
                                            </a> 
                                        </td>
										<td>  <a href="<?php echo base_url();?>images/slides/<?php echo $slides['image']; ?>" data-fancybox="mygallery" class="ne-zoom img-popup-icon-layout2" rel="ligthbox">
                                            <img src="<?php echo base_url();?>images/slides/<?php echo $slides['image']; ?> " style="width: 100px;height: 40px;" alt="<?php echo $slides['title']; ?>"> 
                                            </a> 
                                        </td>
										<!--td> 
											<label class="switch">
											  <input type="checkbox" <?php if($slides['status']==1){echo 'checked';}?> id="status" slides_sid="<?php echo $slides['id']; ?>">
											  <span class="slider round"></span>
											</label>
											<span id="status_roww<?php echo $slides['id']; ?>">
											<?php if($slides['status']==1){?>
												<span>Active</span>
											<?php }else{ ?>
												<span style="white-space: nowrap;">In-Active</span>

											<?php }?>
											</span>
										</td-->
										<td> <a href="<?= base_url('admin/slides/edit/'.$slides['id']); ?>"> <button type="button" class="btn btn-sm btn-outline-dark"  <?php echo $user_classu;?>"><i class="fa fa-pencil"></i></button> </a>
										<!--a href="#" class="removeRecord" slides_id = "<?php echo $slides['id'];?>"><button type="button" class="btn btn-sm btn-outline-dark"  <?php echo $user_classd;?>"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></button></a--> </td>
									</tr>
									<?php $i++; }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- For admin Project Manual Validations -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
	<script>
	$(document).ready(function() {
		$('body').on('click', '#status', function () {
            if($(this).prop("checked") == true){
                var id = 1;
            }
            else {
                var id = 0;
            }
			var slides_id = $(this).attr('slides_sid');
			$.ajax({
			  type: "POST",
			  url:"<?php echo base_url('admin/slides/update_status');?>",
			  data: {id: id, slides_id: slides_id},
			  cache: false,
			  success: function(data){
				  $("#status_roww"+slides_id).load(" #status_roww"+slides_id);
			  }
			});
		});
	});
</script>
<script>
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('slides_id');
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/slides/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
</script>
<script>
$(document).ready(function() {
  $("[data-fancybox]").fancybox();
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js"></script>




        				