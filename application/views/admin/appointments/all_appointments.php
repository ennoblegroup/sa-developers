<style>
	table.dataTable tbody td {
    padding: 5px 5px !important;
    font-size: 12px;
}
.line-clamp {
	white-space: nowrap;
        overflow: hidden;
		text-overflow: ellipsis;
		max-width:300px;
}
.nav-tabs .nav-link {
    border: 1px solid #0000002b;
}
.hide{
	display:none;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">Appointments</li>
					</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs"  id="myTab" role="tablist" style="border-bottom: 1px solid #dee2e6;">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#confirmed">
									<span>
										<i class="fa fa-calendar-check"></i>&nbsp;Confirmed
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#unconfirmed">
									<span>
										<i class="fa fa-exclamation-circle"></i>&nbsp;To be Confirmed
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#completed">
									<span>
										<i class="fa fa-check"></i>&nbsp;Completed
									</span>
								</a>
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content tabcontent-border">
							<div class="tab-pane fade show active" id="confirmed" role="tabpanel">
								<div class="pt-4">
									<div class="card">
										<div class="card-body custom_body">
											<div class="table-responsive">
												<table id="t1" class="rrr ui celled table" style="width:100%">
													<thead>
														<tr>
															<th>Last Name</th>
															<th>First Name</th>
															<th>Date & Time</th>
															<th>E-Mail Address</th>
															<th>Phone Number</th>
															<th>Subject</th>	
															<th>Action</th>										
															<!--th>Status</th-->
														</tr>
													</thead>
													<tbody id="confirmed_data">
													
												    </tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="unconfirmed" role="tabpanel">
								<div class="pt-4">
								<div class="card">
										<div class="card-body custom_body">
											<div class="table-responsive">
												<table id="t2" class="rrr ui celled table" style="width:100%">
													<thead>
														<tr>
															<th>Last Name</th>
															<th>First Name</th>
															<th>Date & Time</th>
															<th>E-Mail Address</th>
															<th>Phone Number</th>
															<th>Subject</th>	
															<th>Action</th>					
														</tr>
													</thead>
													<tbody id="unconfirmed_data">
													
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="completed" role="tabpanel">
								<div class="pt-4">
								<div class="card">
										<div class="card-body custom_body">
											<div class="table-responsive">
												<table id="t3" class=" rrr ui celled table" style="width:100%">
													<thead>
														<tr>
															<th>Last Name</th>
															<th>First Name</th>
															<th>Date & Time</th>
															<th>E-Mail Address</th>
															<th>Phone Number</th>
															<th>Subject</th>	
															<th>Action</th>					
														</tr>
													</thead>
													<tbody id="completed_data">
													
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="app">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title eew">View Appointment</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<p id="qw">
								
								</p>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>   
<!-- For admin Project Manual Validations -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$(document).ready(function() {
	$('body').on('click', '.view_appointment', function () {
		var id = $(this).attr('appointment_id');
		$.ajax({
			type:'POST',
			url:"<?php echo base_url('admin/appointments/get_appointment');?>",
			data:{'id':id},
			dataType:"json",
			success: function(data){
				console.log(data);
				$('#qw').html(data.appointment);		
			}
		});
		$('#app').modal('show');
	});
	$('body').on('click', '.confirm_booking', function () {
		var id = 1;
		var appointment_id = $(this).attr('appointment_id');
		$.ajax({
			type: "POST",
			url:"<?php echo base_url('admin/appointments/update_status');?>",
			data: {id: id, appointment_id: appointment_id},
			cache: false,
			success: function(data){
				filter_data();
			}
		});
	});
	$('body').on('click', '.complete_booking', function () {
		var id = 2;
		var appointment_id = $(this).attr('appointment_id');
		$.ajax({
			type: "POST",
			url:"<?php echo base_url('admin/appointments/update_status');?>",
			data: {id: id, appointment_id: appointment_id},
			cache: false,
			success: function(data){
				filter_data();
			}
		});
	});
});
function filter_data()
{
	$.ajax({
		url:"appointments/filter_appointments",
		method:"POST",
		async: true,
		dataType:"json",
		success: function(data)
		{						
			$('#confirmed_data').html(data.confirmed_list);
			$('#unconfirmed_data').html(data.unconfirmed_list);	
			$('#completed_data').html(data.completed_list);
			tableinit();
		},
		error: function (data) {
			//var out =JSON.parse(data);
			console.log(data.responseText);
		},
		complete: function (data) {
		// Handle the complete event
			console.log(data);
		}
	})
}
function tableinit(){
	$('#t1').DataTable({
		"bRetrieve": true,
		"bProcessing": true,
		"bSort" : true,
		"bDestroy": true,
		"bPaginate": true,
		"bAutoWidth": true,
		"bFilter": true,
		"bInfo": true,
		"bJQueryUI": true,
		"ordering": true,
		columnDefs: [
			{ orderable: false, targets: -1 }
		],
		language: {
		"emptyTable": "No Appointments were found."
		}
	});
	$('#t2').DataTable({
		"bRetrieve": true,
		"bProcessing": true,
		"bSort" : true,
		"bDestroy": true,
		"bPaginate": true,
		"bAutoWidth": true,
		"bFilter": true,
		"bInfo": true,
		"bJQueryUI": true,
		"ordering": true,
		columnDefs: [
			{ orderable: false, targets: -1 }
		],
		language: {
		"emptyTable": "No Appointments were found."
		}
	});
	$('#t3').DataTable({
		"bRetrieve": true,
		"bProcessing": true,
		"bSort" : true,
		"bDestroy": true,
		"bPaginate": true,
		"bAutoWidth": true,
		"bFilter": true,
		"bInfo": true,
		"bJQueryUI": true,
		"ordering": true,
		columnDefs: [
			{ orderable: false, targets: -1 }
		],
		language: {
		"emptyTable": "No Appointments were found."
		}
	});
}
</script>
<script>
$('body').on('click', '.removeRecord', function() {
	 var id = $(this).attr('appointment_id'); 
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/appointments/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
$('#myTab a').click(function(e) {
  e.preventDefault();
  $(this).tab('show');
});

// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
  var id = $(e.target).attr("href").substr(1);
  window.location.hash = id;
});

// on load of the page: switch to the currently selected tab
var hash = window.location.hash;

</script>