
<!--**********************************
	Content body start
***********************************-->
<style>
.ratings i {
    color: green
}

.install span {
    font-size: 12px
}

.col-md-4 {
    margin-top: 27px
}

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0);
  transition: background 0.5s ease;
}

.ttt:hover .overlay {
  display: block;
  background: rgba(0, 0, 0, .3);
}


.title {
  position: absolute;
  width: 500px;
  left: 0;
  top: 120px;
  font-weight: 700;
  font-size: 30px;
  text-align: center;
  text-transform: uppercase;
  color: white;
  z-index: 1;
  transition: top .5s ease;
}

.ttt:hover .title {
  top: 90px;
}

.button {
  position: absolute;
  left:35%;
  top: 30%;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {

}

.ttt:hover .button {
  opacity: 1;
}
.card {
    height: auto;
}
.ttt h4 {
  white-space: nowrap; 
  width: auto; 
  overflow: hidden;
  text-overflow: ellipsis;
}

.ttt h4:hover {
  overflow: visible;
}
</style>
<div class="mini-header">
	<div class="">
		<div class="row page-titles mx-0">
			<div class="col-sm-4 p-md-0 ">
				<span id="PathPlaceBtn" class="pull-left" style="width: 100%;">
					<button id="backBtn" class="backBtn btn btn-default btn-sm " title="Back" onclick="BackDir()"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
							<li class="breadcrumb-item active">Gallery Types</li>
						</ol>
					<button class="backBtn refreshBtn btn btn-default btn-sm pull-right" title="Refresh" style="margin-left:-1px;"><i class="fa fa-refresh" aria-hidden="true"></i></button>
				</span>
			</div>
			<div class="col-sm-8 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
				<div class="welcome-text">
					<button type="button"  data-toggle="modal" data-target="#exampleModalLong" style="float:right;" class="btn btn-outline-dark"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add gallery Type</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!--div class="card-header">
						<h4 class="card-title">Forms</h4>
					</div-->
					<div class="card-body custom_body">
						<div class="table-responsive">
							<table id="example3" class="ui celled table" style="width:100%">
								<thead>
									<tr>										
										<!--th>Form Source</th-->
										<th>Gallery Type Name</th>
										<th>Gallery Type Description</th>											
										<th>Actions</th>										
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($all_gallery_types as $row): ?>
									<tr>
										<td><?= $row['gallery_type_name']; ?></td>
										<td>
											<?= $row['gallery_type_description']; ?>
										</td>
										<td> 
											<a data-toggle="tooltip"  title="Edit gallery Type"  gallery_type_id = "<?php echo $row['id'];?>"  href="#" class="btn btn-sm btn-outline-dark edit_gallery_type"><i class="fa fa-pencil"></i></a>
											<a data-toggle="tooltip" title="Delete" href="#" id = "<?php echo $row['id'];?>" class="removeRecord btn btn-sm btn-outline-dark "><i class="fa fa-trash-o"></i></a>											
										</td>
									</tr>
									<?php $i++; endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModalLong">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Gallery Type</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open_multipart(base_url('admin/gallery_types/add'), 'id="quickForm"  class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="gallery_type_name" placeholder="Gallery Type Name" name="gallery_type_name">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" placeholder="Description" name="gallery_type_description"></textarea>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Submit</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<div class="modal fade" id="editgallery_type">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Gallery Type</h5>
				<button type="button" class="close" data-dismiss="modal"><span>&times;</span>
				</button>
			</div>
			<?php echo form_open_multipart(base_url('admin/gallery_types/edit'), 'id="quickForm2"  class="form-horizontal"');  ?>
			<div class="modal-body">
				<div class="row">
					<div class="card" style="width:100%;margin-bottom:0px;">
						<div class="card-body">
							<div class="row">																
								<!--div class="form-group col-lg-3 col-md-3">
									<label class="col-form-label">Source</label>
									<input type="text" class="form-control" name="file_source">
								</div-->
								<div class="form-group col-lg-12 col-md-12">
									<input type="text" class="form-control" id="egallery_type_name" placeholder="Gallery Type Name" name="gallery_type_name">
									<input type="hidden"  name="gallery_type_id" id="gallery_type_id" value="">
								</div>
								<div class="form-group col-lg-12 col-md-12">
									<textarea class="form-control" rows="5" placeholder="Description" id="egallery_type_description" name="gallery_type_description"></textarea>
								</div>
							</div>
						</div>
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
				<button type="submit" name="submit" value="Submit" class="btn btn-outline-dark">Update</button>
			</div>
			<?php echo form_close( ); ?>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>	
<script>
$(".edit_gallery_type").click(function(){
	var id = $(this).attr('gallery_type_id');
	$.ajax({
		type:'POST',
		url:"<?php echo base_url('admin/gallery_types/get_gallery_type_by_id');?>",
		data:{'id':id},
		dataType:"json",
		success: function(data){
			console.log(data);
			$('#gallery_type_id').val(data.id);
			$('#egallery_type_name').val(data.gallery_type_name);
			$('#egallery_type_description').text(data.gallery_type_description);
			//location.reload();$('#my_image').attr('src','second.jpg');
		}
	});
    $('#editgallery_type').modal('show');
});
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
function readURL1(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blahh')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}
$('body').on('click', '.removeRecord', function() {
	var id = $(this).attr('id');
	//alert(orders_id);
	$.confirm({
    title: 'Are you sure?',
    content: 'Do you really want to delete these records? This process cannot be undone.',
    buttons: {
        confirm: {
           text: 'Confirm',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){			
					$.ajax({
						type:'POST',
						url:"<?php echo base_url('admin/gallery_types/del');?>",
						data:{'id':id},
						success: function(data){
							location.reload();
							}
						});
					}
        },
        cancel: {
            text: 'cancel',
            btnClass: 'btn btn-outline-dark',
            keys: ['enter', 'shift'],
            action: function(){
				}
			}
		}
	});
});
$(gallery).ready(function() {
  	$('#quickForm').validate({	  
		rules: {			
		gallery_type_name: {
			required: true
		},
		gallery_type_description: {
			required: true
		},
		},
		messages: {
		gallery_type_name: {
			required: "Please enter Name"
		},
		gallery_type_description: {
			required: "Please enter Description"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
	$('#quickForm2').validate({	  
		rules: {			
		gallery_type_name: {
			required: true
		},
		gallery_type_description: {
			required: true
		},
		},
		messages: {
		gallery_type_name: {
			required: "Please enter Name"
		},
		gallery_type_description: {
			required: "Please enter Description"
		},
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		error.addClass('invalid-feedback');
		element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		$(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass('is-invalid');
		}
	});
});
</script>
