<style>
  .p-tb80 {
   padding-bottom: 80px;
   }
.contact-one .required .form-control {
    border-bottom: 2px solid #ddd;
    padding-left: 15px;
    padding-right: 0px;
}
.required{
  position: relative;
}
.required::after{
  content: '*';
  position: absolute;
  top: 5px;
  left: 6px;
  color: #f00
}

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="page-content">
   <!-- INNER PAGE BANNER -->
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/2.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white">Contact Us</h2>
               </div>
            </div>
            <!-- BREADCRUMB ROW -->                            
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Contact Us</li>
               </ul>
            </div>
            <!-- BREADCRUMB ROW END -->                        
         </div>
      </div>
   </div>
   <!-- SECTION CONTENTG START -->
   <div class="section-full small-device  p-tb80">
      <!-- LOCATION BLOCK-->
     					
      <!-- GOOGLE MAP & CONTACT FORM -->
      <div class="section-content overlay-wraper ">
         <div class="container">
            <!-- TITLE START -->
            <div class="section-head text-center">
               <div class="wt-separator-outer separator-center">
                  <div class="wt-separator">
                     <span class="text-primary text-uppercase sep-line-one ">Contact Us</span>
                  </div>
               </div>
               <h2>Get In Touch</h2>
            </div>
            <!-- TITLE END -->     
            <div class="center">
               <?php if($this->session->flashdata('wwww')): ?>
               <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                  </button>
                  <?php echo $this->session->flashdata('wwww'); ?>
               </div>
               <?php endif; ?>
            </div>
            <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>
            <form id="contact-form" class="" method="post" >
               <div class="contact-one">
                  <div class="row">
                     <div class="err col-md-6 col-sm-6">
                        <div class="form-group required">
                           <input name="last_name" id="last_name" type="text"  class="form-control" placeholder="Last Name">
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="err col-md-6 col-sm-6">
                        <div class="form-group required">
                           <input name="first_name" id="first_name" type="text"   class="form-control" placeholder="First Name">
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="err col-md-6 col-sm-6">
                        <div class="form-group">
                           <input name="phone" id="phone" type="text"  class="us_phone form-control"  placeholder="Phone Number">
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="err col-md-6 col-sm-6">
                        <div class="form-group required">
                           <input name="email" id="email" type="text"  class="form-control"  placeholder="E-Mail Address">
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="err col-md-6 col-sm-12">
                        <div class="form-group">
                           <select class="form-control" name="subject_type" id="subject_type" onchange="showDiv(this)">
                              <option value="General/Other">General/Other</option>
                              <option value="Review">Feedback</option>
                              <option value="Question">Client Question</option>
                              <option value="suggestion">Suggestion</option>
                           </select>
                        </div>
                        <div  class="err " id="hidden_div" style="display:none;margin: 6px;">
                           <div class="form-group star-rating" style="text-align:center;">
                              <span class="fa fa-star-o" data-rating="1"></span>
                              <span class="fa fa-star-o" data-rating="2"></span>
                              <span class="fa fa-star-o" data-rating="3"></span>
                              <span class="fa fa-star-o" data-rating="4"></span>
                              <span class="fa fa-star-o" data-rating="5"></span>
                              <input type="hidden" name="rating" id="rating" class="rating-value" value="">
                           </div>
                        </div>
                     </div>
                     <div class="err col-md-6 col-sm-6">
                        <div class="form-group required">
                           <input name="subject" id="subject" type="text"  class="form-control"  placeholder="Subject">
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="err col-md-12">
                        <div class="form-group required">
                           <textarea name="message" id="message" rows="4"  class="form-control "  placeholder="Message"></textarea>
                           <span class="spin"></span>
                        </div>
                     </div>
                     <div class="text-left col-md-12">
                        <button name="submit" type="submit" value="Submit" class="contact_button site-button site-btn-effect">
                        Submit
                        </button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
       <?php $i=1; foreach ($contactus as $key=>$contact){ ?>
      <?php if($contact['status']==1){?>
      <div class="section-content m-t50 m-b50">
         <div class="container">
            <div class="row">
               <div class="col-md-4 col-sm-12 m-b30">
                  <div class="wt-icon-box-wraper center p-lr30 p-tb50 bdr-1 bdr-gray">
                     <div class="icon-md m-b10"><i class="flaticon-smartphone"></i></div>
                     <div class="icon-content">
                        <h4>Phone number</h4>
                        <h5> <?php echo $contact['phone']; ?> </h5>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 m-b30">
                  <div class="wt-icon-box-wraper center p-lr30 p-tb50  block-shadow">
                     <div class="icon-md  m-b10"><i class="flaticon-email"></i></div>
                     <div class="icon-content">
                        <h4>Email address</h4>
                        <h5><?php echo $contact['email']; ?></h5>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-sm-12 m-b30">
                  <div class="wt-icon-box-wraper center p-lr30 p-tb50">
                     <div class="icon-md	 m-b10"><i class="flaticon-placeholder"></i></div>
                     <div class="icon-content">
                        <h4>Address info</h4>
                        <h5><?php echo $contact['address1']; ?><br>
                        <?php if($contact['address2'] !=''){?>
                        <?php echo $contact['address2']; ?><br>
                        <?php }?>
                        <?php echo $contact['city']; ?> <?php echo $contact['state']; ?>, <?php echo $contact['country']; ?></h5>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php  }}?> 
   </div>
</div>
<!-- CONTENT END -->
<script>
   function showDiv(elem){
     if(elem.value == 'Review')
     document.getElementById('hidden_div').style.display = "block";
     else if(elem.value !== 'Review')
     document.getElementById('hidden_div').style.display="none";
   }
</script>
<script>
   var $star_rating = $('.star-rating .fa');
   
   var SetRatingStar = function() {
     return $star_rating.each(function() {
   	if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
   	  return $(this).removeClass('fa-star-o').addClass('fa-star');
   	} else {
   	  return $(this).removeClass('fa-star').addClass('fa-star-o');
   	}
     });
   };
   
   $star_rating.on('click', function() {
     $star_rating.siblings('input.rating-value').val($(this).data('rating'));
     return SetRatingStar();
   });
   
   SetRatingStar();
   $(document).ready(function() {
   
   });
</script>	
		
