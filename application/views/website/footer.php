<style>
.icon-instagram:before{
content :"\f16d";
}
.icon-youtube:before{
content :"\f16a";
}
.icon-linkedin:before{
content :"\f0e1";
}
.icon-pinterest:before{
content :"\f0d2";
}
</style>
			<footer>
			<?php $i=1; foreach ($contactus as $key=>$contact){ ?>
                <div class="copyright">
                	<div class="container">
	                    <p>Powered By<a href="https://www.iekait.com/" target="_blank" style="color: #f9f6f6;"> eKAIT</a> | &copy; 2020. All Rights Reserved.</p>
                        <p class="iq-copyright" style="float:right">
						 <a href="#" style="color: #f9f6f6;" >Disclaimer</a> | <a href="#" style="color: #f9f6f6;">Privacy Policy</a> | <a style="color: #f9f6f6;" href="#" >Terms &amp; Conditions</a>
						</p>
                    </div>
                </div>
				<?php  }?>	
            </footer>
		</div>
    </div>
</div><!-- Wrapper End -->
    
    <!-- Java Scripts -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery-1.10.2.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.inview.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.nav.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery-menu.js"></script>    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.meanmenu.min.js"></script> 
    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.quovolver.min.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.donutchart.js"></script>        

	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.isotope.min.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.prettyPhoto.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.validate.min.js"></script>    
  
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.tabs.min.js"></script>
    
	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/jquery.nicescroll.min.js"></script>

	<!-- Layer Slider Starts -->
	<script src="<?php echo base_url();?>assets/website/js/layerslider/jquery-easing-1.3.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/website/js/layerslider/jquery-transit-modified.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/website/js/layerslider/layerslider.transitions.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/website/js/layerslider/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
	jQuery(document).ready(function($){	
		$('#layerslider').layerSlider({
			skinsPath : '../assets/website/js/layerslider/skins/',
			skin : 'borderlessdark3d',
			width : '940px',
			height : '500px',
			responsive : true,
			thumbnailNavigation : 'hover',
			showCircleTimer : false,
			navPrevNext	 : true,
			navButtons	 : true,
			hoverPrevNext: true
		});
	});
	</script>
	<!-- Layer Slider Ends -->
    
    <!-- Revolution Slider Starts -->
    <script src="<?php echo base_url();?>assets/website/js/revolution/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
    <script type="text/javascript">
	jQuery(document).ready(function($){	
	if($.fn.cssOriginal != undefined)
		$.fn.css = $.fn.cssOriginal;

		var api = $('.fullwidthbanner').revolution(
		{
			delay:9000,
			startwidth:940,
			startheight:570,
	
			onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off
	
			thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
			thumbHeight:50,
			thumbAmount:3,
	
			hideThumbs:200,
			navigationType:"none",				// bullet, thumb, none
			navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
	
			navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
	
			navigationHAlign:"center",				// Vertical Align top,center,bottom
			navigationVAlign:"bottom",					// Horizontal Align left,center,right
			navigationHOffset:30,
			navigationVOffset:-40,
	
			soloArrowLeftHalign:"left",
			soloArrowLeftValign:"center",
			soloArrowLeftHOffset:20,
			soloArrowLeftVOffset:0,
	
			soloArrowRightHalign:"right",
			soloArrowRightValign:"center",
			soloArrowRightHOffset:20,
			soloArrowRightVOffset:0,
	
			touchenabled:"on",						// Enable Swipe Function : on/off
	
			stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
			stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
	
			hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
			hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
			hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
	
			fullWidth:"on",
	
			shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)
		});	
	});
	</script>
    <!-- Revolution Slider Ends -->
	
    <!-- Style Picker Starts -->
	<!--script src="<?php echo base_url();?>js/jquery.cookie.js"></script>
    <script src="<?php echo base_url();?>js/controlpanel.js"></script>
    <!-- Style Picker Ends --> 
    
    <!-- **To Top** -->
    <script src="<?php echo base_url();?>assets/website/js/jquery.ui.totop.min.js"></script>
    
    <!-- **Contact Map** -->
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="<?php echo base_url();?>assets/website/js/jquery.gmap.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/website/js/custom.js"></script>
    
</body>


</html>