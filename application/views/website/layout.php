<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- META -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="keywords" content="" />
      <meta name="author" content="" />
      <meta name="robots" content="" />
      <meta name="description" content="" />
      <!-- FAVICONS ICON -->
      <link rel="icon" href="<?php echo base_url();?>images/favicon1.png" type="image/x-icon" />
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/website/images/favicon1.png" />
      <!-- PAGE TITLE HERE -->
      <title>S&A Developers</title>
      <!-- MOBILE SPECIFIC -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- [if lt IE 9]>
         <script src="js/html5shiv.min.js"></script>
         <script src="js/respond.min.js"></script>
         <![endif] -->
      <!-- BOOTSTRAP STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/bootstrap.min.css">
      <!-- FONTAWESOME STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/fontawesome/css/font-awesome.min.css" />
      <!-- OWL CAROUSEL STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/owl.carousel.min.css">
      <!-- MAGNIFIC POPUP STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/magnific-popup.min.css">
      <!-- LOADER STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/loader.min.css">
      <!-- MAIN STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/style.css">
      <!-- FLATICON STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/flaticon.min.css">
      <!-- THEME COLOR CHANGE STYLE SHEET -->
      <link rel="stylesheet" class="skin" type="text/css" href="<?php echo base_url();?>assets/website/css/skin/skin-1.css">
      <!-- SIDE SWITCHER STYLE SHEET -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/css/switcher.css">
      <!-- REVOLUTION SLIDER CSS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/plugins/revolution/revolution/css/settings.css">
      <!-- REVOLUTION NAVIGATION STYLE -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/plugins/revolution/revolution/css/navigation.css">
      <!-- BEFORE/AFTER ADD-ON FILES  MUST BE INSERTED AFTER THE SLIDER DOM ELEMENTS !-->
      <link rel='stylesheet' href='<?php echo base_url();?>assets/website/plugins/revolution/revolution-addons/beforeafter/css/revolution.addon.beforeafter.css' type='text/css' media='all' />
      <!-- GOOGLE FONTS -->
	  <link rel="stylesheet" href="<?= base_url() ?>assets/admin/vendor/toastr/css/toastr.min.css">
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" />
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,800,800i,900" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Martel:200,300,400,600,700,800,900" rel="stylesheet">
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   </head>
   <style>
   .is-invalid~.invalid-feedback, .is-invalid~.invalid-tooltip, .was-validated :invalid~.invalid-feedback, .was-validated :invalid~.invalid-tooltip {
		display: block;
	}
	.error {
		font-size: 13px;
	}

	.form-group {
		margin-bottom: 0px;
	}
	.err{
		margin-bottom: 20px;
	}
   .invalid-feedback {
		width: 100%;
		margin-top: .25rem;
		color: #dc3545;
	}
   .qwas{
      margin-left: 50px !important;
   }
   .nav-dark.header-nav .nav > li > .qwas{
      color: #50aab2;
   }
   .nav-line-animation > li > .qwas:before{
      background-color: #50aab2;
   }
   .nav-line-animation > li > .qwas:before {
      top: 20px;
      left: 0;
   }
   .nav-line-animation > li > .qwas:after{
      background-color: #50aab2;
   }
   .nav-line-animation > li > .qwas:after {
      bottom: 10px;
      right: 0;
   }
   .nav-line-animation > li > .qwas:before, .nav-line-animation > li > .qwas:after {
      height: 1px;
      position: absolute;
      content: '';
      -webkit-transition: all 0.35s ease;
      transition: all 0.35s ease;
      background-color: #50aab2;
      width: 100%;
   }

	.cd img{
    width: 100%;
		height: 200px;
	}
   .contact-one .required .form-control {
    border-bottom: 2px solid #ddd;
    padding-left: 15px;
    padding-right: 0px;
}
.required{
  position: relative;
}
.required::after{
  content: '*';
  position: absolute;
  top: 5px;
  left: 6px;
  color: #f00
}
   </style>
   <body>
      <div class="page-wraper">
         <!-- HEADER START -->
         <header class="site-header header-style-1  nav-wide">
            <div class="sticky-header main-bar-wraper">
               <div class="main-bar bg-white">
                  <div class="container header-center">
                     <div class="wt-header-left">
                        <div class="logo-header">
                           <div class="logo-header-inner logo-header-one">
                              <a href="<?php echo base_url();?>">
                              <img src="<?php echo base_url();?>assets/website/images/logo.png"  alt="" style="height:50px;" />
                              </a>
                           </div>
                        </div>
                     </div>
                     <div class="wt-header-center">
                        <!-- NAV Toggle Button -->
                        <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>                        
                        <!-- MAIN Vav -->
                        <div class="header-nav navbar-collapse collapse nav-dark">
                           <ul class=" nav navbar-nav nav-line-animation">
                              <li class="<?php if($menu=='index'){echo 'active';}?>">
                                 <a href="<?php echo base_url();?>" >Home</a>
                              </li>
                              <li class="<?php if($menu=='about'){echo 'active';}?>">
                                 <a href="<?php echo base_url();?>website/about">About us</a>
                              </li>
                              <li>
                                 <a href="<?php echo base_url();?>website/services">Services</a>
                                 <ul class="sub-menu">
                                 <?php $i=1; foreach ($menu_services as $key=>$service){ ?>
                                    <li><a href="<?php echo base_url();?>website/services/view_service/<?php echo $service['id']?>"><?php echo $service['service_name']; ?></a></li>
                                    <?php $i++;}?>                                       
                                 </ul>                                    
                              </li>
                              <li class="<?php if($menu=='projects'){echo 'active';}?>">
                                 <a href="<?php echo base_url();?>website/projects" >Projects</a>
                              </li>
                              <li class="<?php if($menu=='materials'){echo 'active';}?>">
                                 <a href="<?php echo base_url();?>website/materials" >Products</a>                                    
                              </li>
							  <li class="<?php if($menu=='gallery'){echo 'active';}?>">
                                 <a href="<?php echo base_url();?>website/gallery" >Gallery</a>                                    
                              </li>
                              <li>
                                 <a href="<?php echo base_url();?>website/contactus">Contact us</a>
                              </li>
                              <?php if($_SESSION['sadevelopers_admin'] && $_SESSION['sadevelopers_admin']['is_admin_login']==1 && $_SESSION['sadevelopers_admin']['client_id'] >0){?>
                                 <li>
                                    <a href="#" class="qwas"><img src="<?= base_url() ?>images/clients/<?= ucwords($_SESSION['sadevelopers_admin']['image']); ?>" style="width:25px;border-radius:50%" alt=""/>&nbsp;<?= ucwords($_SESSION['sadevelopers_admin']['name']); ?></a>
                                    <ul class="sub-menu">
                                       <li><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                                       <li><a href="<?= site_url('website/auth/logout'); ?>">Logout</a></li>                                        
                                    </ul>                                    
                                 </li>
                              <?php }?>
                           </ul>
                        </div>
                     </div>
					 
                     <div class="wt-header-right ">
                        <div class="bg-primary wt-header-right-child">
                           <div class="extra-nav">
                              <div class="extra-cell">
                                 <div class="right-arrow-btn">
                                    <button type="button"  data-toggle="modal" data-target="#exampleModal" href="#" target="_blank" style="font-size:14px;">Appointment</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
					
					 <?php if(!$_SESSION['sadevelopers_admin'] || $_SESSION['sadevelopers_admin']['is_admin_login']!=1 ||  $_SESSION['sadevelopers_admin']['client_id'] ==0){?>
                     <div class="wt-header-right ">
                        <div class="bg-primary wt-header-right-child">
                           <div class="extra-nav">
                              <div class="extra-cell">
                                 <div class="right-arrow-btn">
                                    <button type="button" class="btn-open contact-slide-show text-white notification-animate" style="font-size:14px;">Login</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
					  <?php }?>
                     <!-- Contact Nav -->                            
                     <div class="contact-slide-hide">
                        <div class="contact-nav">
                           <a href="javascript:void(0)" class="contact_close">&times;</a>
                           <div class="contact-nav-form p-a30">
								<?php echo form_open(base_url('website/auth/login'), 'id="loginForm" class="cons-contact-hform" '); ?>                               
								 <div class="m-b30">
                                    <!-- TITLE START -->
                                    <div class="section-head text-left" style="margin-bottom:15px">
                                       <h4 class="m-b5"><span style="color:#50aab2;">Login Here</span></h4>
                                    </div>
									<div class="alert alert-danger solid alert-dismissible fade loginalert" style="display:none;margin-bottom:5px;">
										<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span></button>
										<strong><span id="lerror"></span></strong>
									</div>
                                    <div class="input input-animate">
                                       <label for="email">Email Address</label>
                                       <input type="email" name="email" id="val-email" class="form-control"  title="Email Address" value="<?php if(isset($_COOKIE["admin_email"])) { echo $_COOKIE["admin_email"]; } ?>">
                                       <span class="spin"></span>
                                    </div>
                                    <div class="input input-animate">
                                       <label for="message">Password</label>
                                       <input type="password" name="password" id="val-password" class="form-control"  title="Password" value="<?php if(isset($_COOKIE["admin_password"])) { echo $_COOKIE["admin_password"]; } ?>">
                                       <span class="spin"></span>
                                    </div>
									<div class="form-check">
										<input class="form-check-input" id="remember" name="remember" type="checkbox" <?php if(isset($_COOKIE["admin_email"])) { ?> checked <?php } ?>>
										<!--label class="form-check-label">
										   Rememeber Email Address<?php echo $msg;?>
										</label-->
										<label class="form-check-label">
										   Rememeber Email Address
										</label>
									</div>
                                    <div class="text-right">
                                       <button name="submit" type="submit" value="Submit" class="btn-half site-button m-b15">
                                       <span>Submit</span>
                                       </button>
                                       <button name="reset" type="reset" value="Reset" class="btn-half site-button m-b15">
                                       <span>Reset</span>
                                       </button>
                                    </div>
                                 </div>
								<?php echo form_close(); ?>
                              
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
         </header>
         <!-- HEADER END -->
         <!--\\ Navigation --> 
         <?php $this->load->view($view);?>
         <!-- footer -->
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <h4 class="modal-title" id="exampleModalLabel">Book an Appointment</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:-25px;">
                     <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="modal-body">
                     <form id="contact-form2" class="" method="post" action="<?php echo base_url(); ?>welcome/contact1">
                        <div class="contact-one">
                           <div class="row">
                              <!-- TITLE END --> 
                              <div class="err col-md-6 col-sm-6">
                                 <div class="form-group required">
                                    <input name="last_name" id="last_name" type="text"  class="form-control" placeholder="Last Name">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-6 col-sm-6">
                                 <div class="form-group required">
                                    <input name="first_name" id="first_name" type="text"  class="form-control" placeholder="First Name">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-6 col-sm-6">
                                 <div class="form-group">
                                    <input name="phone" id="phone" type="text" class="us_phone form-control"  placeholder="Phone Number">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-6 col-sm-6">
                                 <div class="form-group required">
                                    <input name="email" id="email" type="text" class="form-control"  placeholder="E-Mail Address">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-5 col-sm-6">
                                 <div class="form-group required">
                                    <input name="subject" id="subject" type="text" class="form-control"  placeholder="Subject">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-4 col-sm-6">
                                 <div class="form-group required">
                                    <!--label for="date">Date</label-->
                                    <input type="date" name="date" class="form-control"  id="date">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-3 col-sm-6">
                                 <div class="form-group required">
                                    <!--label for="time">Time</label-->
                                    <input type="time" name="time"  class="form-control" id="time">
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="err col-md-12">
                                 <div class="form-group required">
                                    <textarea name="message" id="message" rows="4" class="form-control "  placeholder="Message"></textarea>
                                    <span class="spin"></span>
                                 </div>
                              </div>
                              <div class="text-left col-md-12">
                                 <button name="submit" type="submit" value="Submit" class="site-button site-btn-effect">
                                 Submit
                                 </button>
                                 <button name="reset" type="reset" value="Reset" class="btn-half site-button m-b15">
                                 <span>Reset</span>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- FOOTER START -->
         <footer class="site-footer footer-large  footer-light	footer-wide">
            <!-- FOOTER BLOCKES START -->  
            <div class="footer-top overlay-wraper">
               <div class="overlay-main"></div>
               <div class="container">
                  <div class="text-center">
                     <div class="footer-link">
                        <ul>
                           <li><a href="<?php echo base_url();?>website/about" data-hover="About">About Us</a></li>
                           <li><a href="<?php echo base_url();?>website/services" data-hover="Services">Services</a></li>
                           <li><a href="<?php echo base_url();?>website/projects" data-hover="Projects">Projects</a></li>
                           <li><a href="<?php echo base_url();?>website/materials" data-hover="Materials">Materials</a></li>
                           <li><a href="<?php echo base_url();?>website/contactus" data-hover="Contact Us">Contact Us</a></li>
                        </ul>
                     </div>
                  </div>
                  <?php $i=1; foreach ($contactus as $key=>$contact){ ?>
                  <div class="row">
                     <!-- ABOUT COMPANY -->
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="widget text-center getin-touch">
                           <h4 class="widget-title">Get In Touch</h4>
                           <div class="widget-section">
                              <ul>
                                 <li><?php echo $contact['email']; ?></li>
                                 <li><?php echo $contact['phone']; ?></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- TAGS -->
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="widget text-center widget_address m-b20">
                           <h4 class="widget-title">Address</h4>
                           <div class="widget-section">
                              <ul>
                                 <li><?php echo $contact['address1']; ?> </li>
                                 <li><?php echo $contact['address2']; ?> </li>
                                 <li><?php echo $contact['city']; ?>, <?php echo $contact['state']; ?> <?php echo $contact['zip']; ?></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- USEFUL LINKS -->
                     <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="widget text-center">
                           <h4 class="widget-title">Follow Us</h4>
                           <div class="footer-social-icon">
                              <ul class="social-icons f-social-link">
                                 <li><a href="<?php echo $contact['facebook']; ?>" class="fa fa-facebook"></a></li>
                                 <li><a href="<?php echo $contact['twitter']; ?>" class="fa fa-twitter"></a></li>
                                 <li><a href="<?php echo $contact['instagram']; ?>" class="fa fa-instagram"></a></li>
                                 <li><a href="<?php echo $contact['linkedin']; ?>" class="fa fa-linkedin"></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- NEWSLETTER -->
                  </div>
                  <?php  }?>	
               </div>
            </div>
            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom overlay-wraper">
               <div class="overlay-main"></div>
               <div class="container">
                  <div class="row">
                     <div class="wt-footer-bot-center">
                        <span class="copyrights-text">© 2021 <a href="<?php echo base_url();?>">S&A Developers</a>. Powered By <a href="http://www.ennobletechnologies.com/">Ennoble Technologies</a></span>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
         <!-- FOOTER END -->
         <!-- BUTTON TOP START -->
         <button class="scroltop"><span class="fa fa-angle-up  relative" id="btn-vibrate"></span></button>
      </div>
      <!-- LOADING AREA  END ====== -->
      <!-- JAVASCRIPT  FILES ========================================= --> 
      <script  src="<?php echo base_url();?>assets/website/js/jquery-1.12.4.min.js"></script><!-- JQUERY.MIN JS -->
      <script src="<?= base_url() ?>assets/admin/vendor/toastr/js/toastr.min.js"></script>
      <script src="<?= base_url() ?>assets/admin/js/plugins-init/toastr-init.js"></script>
	  <script  src="<?php echo base_url();?>assets/website/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
      <script src="<?php echo base_url();?>assets/website/js/swiper.min.js"></script>
      <script src="<?php echo base_url();?>assets/website/js/easyzoom.js"></script>
      <script src="<?php echo base_url();?>assets/website/js/main.js"></script>
      <script src="<?= base_url() ?>assets/admin/vendor/jquery-validation/jquery.validate.min.js"></script>
      <script  src="<?php echo base_url();?>assets/website/js/magnific-popup.min.js"></script><!-- MAGNIFIC-POPUP JS -->
      <script  src="<?php echo base_url();?>assets/website/js/waypoints.min.js"></script><!-- WAYPOINTS JS -->
      <script  src="<?php echo base_url();?>assets/website/js/counterup.min.js"></script><!-- COUNTERUP JS -->
      <script  src="<?php echo base_url();?>assets/website/js/waypoints-sticky.min.js"></script><!-- COUNTERUP JS -->
      <script  src="<?php echo base_url();?>assets/website/js/isotope.pkgd.min.js"></script><!-- MASONRY  -->
      <script  src="<?php echo base_url();?>assets/website/js/owl.carousel.min.js"></script><!-- OWL  SLIDER  -->
      <script src="<?php echo base_url();?>assets/website/js/jquery.owl-filter.js"></script>
      <script  src="<?php echo base_url();?>assets/website/js/stellar.min.js"></script><!-- PARALLAX BG IMAGE   --> 
      <script  src="<?php echo base_url();?>assets/website/js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
      <script  src="<?php echo base_url();?>assets/website/js/shortcode.js"></script><!-- SHORTCODE FUCTIONS  -->
      <script  src="<?php echo base_url();?>assets/website/js/switcher.js"></script><!-- SHORTCODE FUCTIONS  -->
      <?php if($this->uri->segment(2)=='home'){?>
      <!-- REVOLUTION JS FILES -->
      <script  src="<?php echo base_url();?>assets/website/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
      <script  src="<?php echo base_url();?>assets/website/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
      <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->	
      <script  src="<?php echo base_url();?>assets/website/plugins/revolution/revolution/js/extensions/revolution-plugin.js"></script>
      <script  src='<?php echo base_url();?>assets/website/plugins/revolution/revolution-addons/beforeafter/js/revolution.addon.beforeafter.min.js'></script>
      <!-- REVOLUTION SLIDER SCRIPT FILES -->

      <script  src="<?php echo base_url();?>assets/website/js/rev-script-2.js"></script>
      <?php }?>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
	  <?php if($this->session->flashdata('success_msg')){?>
        <script>
        	$(function () {
        		toastr.success("", "<?php echo $this->session->flashdata('success_msg'); ?>", {
        			timeOut: 5000,
        			closeButton: !0,
        			debug: !1,
        			AddestOnTop: !0,
        			progressBar: !0,
        			positionClass: "toast-top-right",
        			preventDuplicates: !0,
        			onclick: null,
        			showDuration: "300",
        			hideDuration: "1000",
        			extendedTimeOut: "1000",
        			showEasing: "swing",
        			hideEasing: "linear",
        			showMethod: "fadeIn",
        			hideMethod: "fadeOut",
        			tapToDismiss: !1
        		})
        	})
        </script>
        <?php } ?>
	  <script type="text/javascript">
		$(document).ready(function () {
			$('[data-fancybox="mygallery"]').fancybox({
				loop: false,
				arrows: false,
				buttons : [
					'share',
					'zoom',
					'fullScreen',
					'close'
				],
				thumbs : {
					autoStart : true
				},
				slideClass: "",
			});
		
			jQuery.validator.addMethod("validate_email", function(value, element) {

			if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
				return true;
			} else {
				return false;
			}
			}, "Please enter a valid email.");
			$('#quickForm').validate({
		rules: {
		  email: {
			required: true,
			email: true
		  },
		  password: {
			required: true
		  },
		},
		messages: {
		  email: {
			required: "Please enter Email Address",
			email: "Enter a valid Email Address"
		  }, 
		   password: {
			required: "Please enter Password"
		  },
		},
		errorElement: 'span',
		errorPlacement: function (error, element) {
		  error.addClass('invalid-feedback');
		  element.closest('.form-group').append(error);
		},
		highlight: function (element, errorClass, validClass) {
		  $(element).addClass('is-invalid');
		},
		unhighlight: function (element, errorClass, validClass) {
		  $(element).removeClass('is-invalid');
		}
	  });
	  });
	  $(document).ready(function(){
	  $(".close").click(function(){
		$(".alert").alert("close");
	  });
	});
  </script>
      <script>	  
		$(".us_zip").on('input', function() {
			var zip_value= $(this).val();
			var inputValue = getInputValuez(zip_value); //get value from input and make it usefull number
			var length = inputValue.length; //get lenth of input
			
			if (length == 0)
			{
				$(this).val('');
				return false;
			}    
			if (inputValue < 100000)
			{
				inputValue =inputValue;
			}
			else if (inputValue < 100000000) 
			{
				inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
			}else
			{
				inputValue = inputValue.substring(0, 5) + '-' + inputValue.substring(5, 9);
			}       
			$(this).val(inputValue);
			inputValue = getInputValuez(zip_value);
		});
			
		function getInputValuez(zip_value) {
			var inputValue = zip_value.replace(/\D/g,'');  //remove all non numeric character
			return inputValue;
		}
		$(".us_phone").on('input', function() {
			var phone_value=$(this).val();
			var inputValue = getInputValue_phone(phone_value); //get value from input and make it usefull number
			var length = inputValue.length; //get lenth of input
			
			if (length == 0)
			{
				$(this).val('');
				return false;
			}    
			if (inputValue < 1000)
			{
				inputValue = '+1' + ' ' + '('+inputValue;
			}else if (inputValue < 1000000) 
			{
				inputValue = '+1' + ' ' +  '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
			}else if (inputValue < 10000000000) 
			{
				inputValue ='+1' + ' ' +  '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, length);
			}else
			{
				inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, 10);
			}       
			$(this).val(inputValue); 
			inputValue = getInputValue_phone(phone_value);
		});

		function getInputValue_phone(phone_value) {
			var inputValue = phone_value.replace(/\D/g,'');  //remove all non numeric character
			if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
			{
				var inputValue = inputValue.substring(1, inputValue.length);
			}
			return inputValue;
		}
         $('.contact_button').click(function(e) {
           var form = $("#contact-form");
           form.validate({
             rules: {
         		  last_name: {
                 required: true,
                 minlength: 3
               },
         	 
               first_name: {
                 required: true,
                 minlength: 3
               },
               phone: {
                 minlength: 17,
                 maxlength: 17
               },
         	  subject: {
                 required: true
               },
         	  rating: {
                 required: function(element){
                     return $("#subject_type").val()=="Review";
                 }
               },
               email: {
                 required: true,
                 email: true
               },
         	  message: {
                 required: true,
                 minlength: 3
               },
             },
             messages: {
         	  last_name: {
                 required: "Please enter  Last Name",
                 minlength: "Last Name must be at least 3 characters long"
               },
               first_name: {
                 required: "Please enter  First Name",
                 minlength: "First Name must be at least 3 characters long"
               },
         	  subject: {
                 required: "Please enter  Subject"
               },
         	  rating: {
                 required: "Please select  Rating"
               },
               phone: {
                 minlength: "Enter a valid Phone Number",
                 maxlength: "Enter a valid Phone Number"
               },
               email: {
                 required: "Please enter  E-Mail Address",
                 email: "Enter a valid E-Mail Address"
               },
         	  message: {
                 required: "Please enter Message",
                 minlength: "Message must be at least 3 characters long"
               }
             },
             errorElement: 'span',
             errorPlacement: function (error, element) {
               error.addClass('invalid-feedback');
               element.closest('.err').append(error);
             },
             highlight: function (element, errorClass, validClass) {
               $(element).addClass('is-invalid');
             },
             unhighlight: function (element, errorClass, validClass) {
               $(element).removeClass('is-invalid');
             }
           });
           if(form.valid()){
               var last_name = $('#last_name').val();
         	  var first_name = $('#first_name').val();
               var email = $('#email').val();
               var phone = $('#phone').val();
               var subject =$('#subject').val();
               var subject_type =$('#subject_type').val();
               var message = $('#message').val();
               $.ajax({
                 type: "POST",
                 url: "../welcome/contact",
                 data: {'last_name':last_name,'first_name':first_name,'email':email,'phone':phone,'subject':subject,'subject_type':subject_type,'message':message},
                 cache: false,
                 success: function(data){
                   form.trigger("reset"); 
                   bootoast.toast({
                     message: 'Thank you for writing to us. We will revert to you with the information you requested',
                     type: 'success'
                   });
                 }
             });
           }	
         });
         $("#contact-form2").validate({
         rules: {
         	last_name: {
                 required: true,
                 minlength: 3
            },
         	 
               first_name: {
                 required: true,
                 minlength: 3
               },
               phone: {
                 minlength: 17,
                 maxlength: 17
               },
         	  subject: {
                 required: true
               },
				date: {
                 required: true
               },
				time: {
                 required: true
               },
               email: {
                 required: true,
                 email: true
               },
         	  message: {
                 required: true,
                 minlength: 3
               },
             },
             messages: {
         	  last_name: {
                 required: "Please enter  Last Name",
                 minlength: "Last Name must be at least 3 characters long"
               },
               first_name: {
                 required: "Please enter  First Name",
                 minlength: "First Name must be at least 3 characters long"
               },
         	  subject: {
                 required: "Please enter  Subject"
               },
         	  date: {
                 required: "Please select  Date"
               },
				time: {
                 required: "Please select  Time"
               },
               phone: {
                 minlength: "Enter a valid Phone Number",
                 maxlength: "Enter a valid Phone Number"
               },
               email: {
                 required: "Please enter  E-Mail Address",
                 email: "Enter a valid E-Mail Address"
               },
         	  message: {
                 required: "Please enter Message",
                 minlength: "Message must be at least 3 characters long"
               }
             },
         errorElement: 'span',
         errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.err').append(error);
         },
         highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
         },
         unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid');
         },
         submitHandler: function(form) {
		
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
    				
    				if(response==1){
    					$(function () {
            				toastr.success("", "Appointment request recieved", {
            					timeOut: 3000,
            					closeButton: !0,
            					debug: !1,
            					AddestOnTop: !0,
            					progressBar: !0,
            					positionClass: "toast-top-right",
            					preventDuplicates: !0,
            					onclick: null,
            					showDuration: "300",
            					hideDuration: "1000",
            					extendedTimeOut: "1000",
            					showEasing: "swing",
            					hideEasing: "linear",
            					showMethod: "fadeIn",
            					hideMethod: "fadeOut",
            					tapToDismiss: !1
            				})
            			});
            			$('form#contact-form2').trigger("reset");
            			$('#exampleModal').modal('hide');
            			
    				}else{
    					
    				}
                }            
            });
        }
         });	
      </script>
      <script>
         $(document).ready(function() {
           var sync1 = $("#sync1");
           var sync2 = $("#sync2");
           var slidesPerPage = 4; //globaly define number of elements per page
           var syncedSecondary = true;
         
         	  sync1.owlCarousel({
         		items : 1,
         		slideSpeed : 2000,
         		nav: true,
         		autoplay: true,
         		dots: false,
         		loop: true,
         		responsiveRefreshRate : 200,
         		navText: ['<i class="flaticon-back"></i> Prev', 'Next <i class="flaticon-next"></i>'],
         	  }).on('changed.owl.carousel', syncPosition);
         
         	  sync2
         		.on('initialized.owl.carousel', function () {
         		  sync2.find(".owl-item").eq(0).addClass("current");
         		})
         		.owlCarousel({
         		items : slidesPerPage,
         		dots: false,
         		nav: false,
         		margin:5,
         		smartSpeed: 200,
         		slideSpeed : 500,
         		slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
         		responsiveRefreshRate : 100
         	  }).on('changed.owl.carousel', syncPosition2);
         
           function syncPosition(el) {
             //if you set loop to false, you have to restore this next line
             //var current = el.item.index;
             
             //if you disable loop you have to comment this block
             var count = el.item.count-1;
             var current = Math.round(el.item.index - (el.item.count/2) - .5);
             
             if(current < 0) {
               current = count;
             }
             if(current > count) {
               current = 0;
             }
             
             //end block
         
             sync2
               .find(".owl-item")
               .removeClass("current")
               .eq(current)
               .addClass("current");
             var onscreen = sync2.find('.owl-item.active').length - 1;
             var start = sync2.find('.owl-item.active').first().index();
             var end = sync2.find('.owl-item.active').last().index();
             
             if (current > end) {
               sync2.data('owl.carousel').to(current, 100, true);
             }
             if (current < start) {
               sync2.data('owl.carousel').to(current - onscreen, 100, true);
             }
           }
           
           function syncPosition2(el) {
             if(syncedSecondary) {
               var number = el.item.index;
               sync1.data('owl.carousel').to(number, 100, true);
             }
           }
           
           sync2.on("click", ".owl-item", function(e){
             e.preventDefault();
             var number = $(this).index();
             sync1.data('owl.carousel').to(number, 300, true);
           });
         });
         
         
      </script>
	  <script>
		$(document).ready(function(){  
		  var width=$('.cd').width();
		  var window_width= $(window).width();
		  if(window_width >991){
			$('.cd a img').css('height',(width-width/3)+'px');  
		  } 
		});
	</script>
	<script type="text/javascript">
	$(document).ready(function () {
		jQuery.validator.addMethod("validate_email", function(value, element) {

		if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
			return true;
		} else {
			return false;
		}
	}, "Please enter a valid email.");
	$('#loginForm').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
	  password: {
        required: true
      },
    },
    messages: {
	  email: {
        required: "Please enter Email Address",
        email: "Enter a valid Email Address"
      }, 
       password: {
        required: "Please enter Password"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.input-animate').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
	submitHandler: function(form) {
		
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function(response) {
				
				if(response==1){
					 window.location.href = window.location.origin+'/'+window.location.pathname.split('/')[1]+'/admin/dashboard';
				}else{
					$(".loginalert").css("display","contents");
					$("#lerror").text(response);
				}
            }            
        });
    }
  });
  });
</script>

</body>
</html>