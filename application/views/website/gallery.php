<style>
.new-arrivals-img-contnent img {
	width: 100%;
}
.img-fluid {
    max-width: 100%;
}
.text-center {
    text-align: center !important;
}
.mt-3, .my-3 {
    margin-top: 1rem !important;
}
.star-rating li {
    display: inline-block;
}

li {
    list-style: none;
}
.star-rating li i {
    color: gold;
}
.product-content h5, .product-content ul{
    margin-bottom: 5px;
}
.card {
    margin-bottom: 1.875rem;
    background-color: #fff;
    transition: all .5s ease-in-out;
    position: relative;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: 0.25rem;
    box-shadow: 0px 0px 13px 0px rgb(82 63 105 / 5%);
    height: calc(100% - 30px);
    display: flex;
    flex-direction: column;
    min-width: 0;
    background-clip: border-box;
}

.card-body {
    padding: 1.25rem;
    flex: 1 1 auto;
}
.custom_container{
	padding:20px;
}
.filter-group {
    border-bottom: 1px solid #e4e4e4;
}
article {
    display: block;
}
.filter-group .card-header {
    border-bottom: 0;
}

.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, .03);
}
.show {
    display: block;
}
.collapse {
    display: none;
}
.checkbox {
    display: block;
    min-height: 20px;
    margin-top: 10px;
    margin-bottom: 10px;
    padding-left: 20px;
}
.checkbox label {
    display: inline;
    font-weight: 400;
    cursor: pointer;
}
.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
    float: left;
    margin-left: -20px !important;
	opacity:1 !important;
}
.list-menu{
	margin-bottom:0px;
}
.img-wrap {
    border-radius: 0.2rem 0.2rem 0 0;
    height: 220px;
    border: 1px solid rgba(0,0,0,.125);
}
.img-wrap {
    overflow: hidden;
    position: relative;
    text-align: center;
    display: block;
}
.img-wrap img {
    height: 100%;
    max-width: 100%;
    display: inline-block;
    -o-object-fit: cover;
    object-fit: cover;
}
</style>

<div class="page-content">
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/5.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white">Gallery</h2>
               </div>
            </div>                           
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Gallery</li>
               </ul>
            </div>                       
         </div>
      </div>
   </div>
   <div class="section-full small-device">
      <div class="custom_container">
         <div class="project-detail-outer">
           <div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="card">
						<article class="filter-group">
						   <header class="card-header">
								 <h6 class="title">Category</h6>
						   </header>
						   <div class="filter-content collapse show" id="collapse" style="">
							  <div class="card-body">
								 <ul class="list-menu" style="max-height: 190px;overflow: auto;">
									<?php foreach($categories as $category) { ?>	
										<div class="checkbox">
											<label><input type="checkbox"  class="common_selector category" value="<?php echo $category['id']; ?>"  > <?php echo $category['gallery_type_name']; ?></label>
										</div>
									<?php } ?>
								 </ul>
							  </div>
						   </div>
						</article>
						 <article class="filter-group">
						   <header class="card-header">
								<h6 class="title">Projects</h6>
						   </header>
						   <div class="filter-content collapse show" id="collapse_1" style="">
							  <div class="card-body">
								 <ul class="list-menu" style="max-height: 190px;overflow: auto;">
									<?php foreach($projects as $vendor) { ?>
										<div class="checkbox">
											<label><input type="checkbox"  class="common_selector project" value="<?php echo $vendor['id']; ?>"  > <?php echo $vendor['project_name']; ?></label>
										</div>
									<?php } ?>
								 </ul>
							  </div>
						   </div>
						</article>
					 </div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9">
					<div class="row ">
						<div class="col-md-12">
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="search_text" id="search_text" placeholder="Search Image by Title" class="form-control" />
								</div>
							</div>
						 </div>
					</div>
					<div class="row filter_data">
					
					</div>
					<div class="row ">
						<div class="col-md-12">
							<div style="float:right" id="pagination_link"></div>
						</div>
					</div>
				</div>			
			</div>
         </div>
      </div>  
   </div>
</div>
<script>
$(document).ready(function(){
	
	$(document).on('click', '.pagination li a', function () {
		$('html, body').animate({scrollTop:200}, 'slow');
	});
	
    filter_data(1);

    function filter_data(page)
    {
        
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var action = 'fetch_data';
        var project = get_filter('project');
        var category = get_filter('category');
		var search_text = $('#search_text').val();
        console.log("Before print");
        $.ajax({
            url:"<?php echo base_url(); ?>website/gallery/fetch_data/"+page,
            method:"POST",
            async: true,
            dataType:"json",
            data:{action:action, category:category,project:project,search_text:search_text},
            success: function(data)
            {
                $('.filter_data').html(data.product_list);
                $('#pagination_link').html(data.pagination_link);
            },
            error: function (data) {
                //var out =JSON.parse(data);
                console.log(data.responseText);
            },
            complete: function (data) {
            // Handle the complete event
                console.log(data);
            }
        })
    }
	$('#search_text').keyup(function(){
		var search = $(this).val();
		if(search != '')
		{
			filter_data(1);
		}
		else
		{
			filter_data(1);
		}
	});
    function get_filter(class_name)
    {
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        return filter;
    }

    $(document).on('click', '.pagination li a', function(event){
        event.preventDefault();
        var page = $(this).data('ci-pagination-page');
        filter_data(page);
    });

    $('.common_selector').click(function(){
        filter_data(1);
    });
});
</script>