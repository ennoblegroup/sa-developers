<style>
  .p-tb80 {
   padding-bottom: 80px;
   }
   

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="page-content">
   <!-- INNER PAGE BANNER -->
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/2.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white">Login</h2>
               </div>
            </div>
            <!-- BREADCRUMB ROW -->                            
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Login</li>
               </ul>
            </div>
            <!-- BREADCRUMB ROW END -->                        
         </div>
      </div>
   </div>
   <!-- SECTION CONTENTG START -->
   <div class="section-full small-device  p-tb80">
      <!-- LOCATION BLOCK-->
     					
      <!-- GOOGLE MAP & CONTACT FORM -->
      <div class="section-content overlay-wraper ">
         <div class="container">
            <!-- TITLE START -->
            <div class="section-head text-center">
               <div class="wt-separator-outer separator-center">
                  <div class="wt-separator">
                     <span class="text-primary text-uppercase sep-line-one ">Login</span>
                  </div>
               </div>
            </div>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="contact-nav-form p-a30">
						<?php echo form_open(base_url('website/auth/login'), 'id="loginForm" class="cons-contact-hform" '); ?>
						 <?php if(isset($msg) || validation_errors() !== ''): ?>
							<div class="alert alert-danger solid alert-dismissible fade show">
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
								</button>
								<strong><?= validation_errors();?>
								<?= isset($msg)? $msg: ''; ?>
							</div>
						<?php endif; ?>
						<?php if($this->session->flashdata('success_msg')) {
							$message = $this->session->flashdata('success_msg'); ?>
							<div class="alert alert-success solid alert-dismissible fade show">
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
								</button>
								<strong>
								<?= isset($message)? $message: ''; ?>
							</div>
						<?php }
						if($this->uri->segment(4)!='') { ?> 
							<div class="alert alert-success solid alert-dismissible fade show">
								<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
								</button>
								<strong>Password is Updated Successfully!</strong>
							</div>
						<?php } ?>
						 <div class="m-b30">
							<div class="input input-animate">
							   <label for="email">Email Address</label>
							   <input type="email" name="email" id="val-email" class="form-control"  title="Email Address" value="<?php if(isset($_COOKIE["admin_email"])) { echo $_COOKIE["admin_email"]; } ?>">
							   <span class="spin"></span>
							</div>
							<div class="input input-animate">
							   <label for="message">Password</label>
							   <input type="password" name="password" id="val-password" class="form-control"  title="Password" value="<?php if(isset($_COOKIE["admin_password"])) { echo $_COOKIE["admin_password"]; } ?>">
							   <span class="spin"></span>
							</div>
							<div class="text-right">
							   <button name="reset" type="reset" value="Reset" class="btn-half site-button m-b15">
							   <span>Reset</span>
							   </button>
							   <button name="submit" type="submit" value="Submit" class="btn-half site-button m-b15">
							   <span>Submit</span>
							   </button>
							</div>
						 </div>
						<?php echo form_close(); ?>
				   </div>
			   </div>
		   </div>
         </div>
      </div>
   </div>
</div>		
