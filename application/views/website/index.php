<html lang = "en">
   
   <head>
      <title>Ekait Dashboard</title>
      <!--link href = "css/bootstrap.min.css" rel = "stylesheet"-->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

      <style>
         body {
            padding-top: 40px;
            padding-bottom: 40px;
          /*  background-color: #ADABAB;*/
         }
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #017572;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-color:#017572;
         }
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#017572;
         }
         
         h2{
            text-align: center;
            color: #017572;
         }
      </style>
      
   </head>
	
   <body>
      
      <h2><img src="https://ennobledemos.com/ekaitdashboard_new/images/ek.jpg" class="loading" data-was-processed="true"></h6></h2> 
      <div class = "container form-signin">
      </div> <!-- /container -->
      
      <div class = "container">
		<?php if(isset($msg) || validation_errors() !== ''): ?>
		<div class="alert alert-warning alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-warning"></i> Alert!</h4>
			<?= validation_errors();?>
			<?= isset($msg)? $msg: ''; ?>
		</div>
		<?php endif; ?>
		<?php echo form_open(base_url('website/authweb/login'), 'class="form-signin" '); ?>
            <input type = "email" id="email" class = "form-control" name = "email" placeholder = "E-mail Address " 
               required autofocus></br>
            <input type = "password" id="password" class = "form-control"
               name = "password" placeholder = "password" required>
            <button type="submit" name="submit" id="submit"  value="submit" class = "btn btn-lg btn-outline-dark btn-block">Login</button>
        <?php echo form_close(); ?>
			
         <!-- Click here to clean <a href = "logout.php" tite = "Logout">Session.-->
         
      </div> 
      
   </body>
</html>