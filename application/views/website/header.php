<!doctype html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title> admin </title>
	<meta name="description" content="">
	<meta name="author" content="designthemes">
	<link type="image/x-icon" href="<?php echo base_url();?>assets/website/images/favicon1.png" rel="icon">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="<?php echo base_url();?>assets/website/css/style.css" rel="stylesheet" media="all" />
    <link id="shortcodes-css" href="<?php echo base_url();?>assets/website/css/shortcode.css" rel="stylesheet" media="all" />
	<link href="<?php echo base_url();?>assets/website/css/meanmenu.css" rel="stylesheet" type="text/css" media="all" />
    <link id="skin-css" href="<?php echo base_url();?>assets/website/skins/skyblue/style.css" rel="stylesheet" media="all" />
    
    <link href="<?php echo base_url();?>assets/website/css/responsive.css" rel="stylesheet" type="text/css" />
    
    <!-- **Animation stylesheets** -->
    <link href="<?php echo base_url();?>assets/website/css/animations.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/website/css/skin.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url();?>assets/website/css/isotope.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo base_url();?>assets/website/css/prettyPhoto.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- **Font Awesome** -->
    <link href="<?php echo base_url();?>assets/website/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	
    <!-- **Google - Fonts** -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- SLIDER STYLES STARTS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/js/revolution/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/website/js/layerslider/layerslider.css" media="screen">    
    <!-- SLIDER STYLES ENDS -->    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- **jQuery** -->
    <script src="<?php echo base_url();?>assets/website/js/modernizr-2.6.2.min.js"></script>

</head>
<style>
body{
	font-family: 'Open Sans', sans-serif !important;
	
}
.error{
	font-size:13px;
}
p{
	text-align:justify !important;
}
.h3, h3 {
    font-size: 1rem;
	font-weight:bold;
}
h1, h2, h3, h4, h5, h6, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a, .sorting-container a {
    color: #000000;
}
</style>
<body>
	<div class="wrapper">
    	<div class="inner-wrapper">
    	<!-- Header div Starts here -->
    	<header id="header">
        	<div class="container">
            	<div id="logo">
                	<a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>assets/website/images/logo2.png" style="max-height: 45px;" alt="" title=""> </a>
                </div>
                <div id="menu-container">
                    <nav id="main-menu">
                        <ul class="group">
                            <li class="menu-item current_page_item"><a href="#home">HOME</a></li>
                            <li class="menu-item"><a href="#about">ABOUT US</a></li>
							<li class="menu-item"><a href="#services">SERVICES</a></li>
                            <li class="menu-item"><a href="#features">FEATURES</a></li>
                            <li class="menu-item"><a href="#contact">CONTACT US</a></li>
                        </ul>
                    </nav>
				</div>                    
            </div>
        </header>