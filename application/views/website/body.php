<style>
   .list-style li {
   padding-bottom: 10px;
   background: transparent url(<?php echo base_url();?>assets/website/images/tick.png) no-repeat left top;
   padding-left: 40px;
   font-size:14px;
   line-height: 24px;
   }
   ul {
   list-style: none;
   }
   ul.tabs-vertical-frame li a strong {
   background: #FFFFFF;
   *background: #FFFFFF !important;
   border: 5px solid #DCDCE1;
   *border: 5px solid #DCDCE1 !important;
   border-radius: 23px 23px 23px 23px;
   color: #666666;
   float: left;
   height: 10px;
   line-height: 10px;
   margin-top: -12px;
   margin-right: 25px;
   padding: 10px;
   text-align: center;
   width: 10px;
   }
   select {
   width: 100%;
   }
   .button.small {
      padding: 5px 10px;
   }
   .small, small {
     font-size: 100%; */
      font-weight: 400;
   }
   input[type=submit], button, input[type=button] {
    color: #ffffff;
    text-transform: capitalize;
    margin: 5px 0px;
    font-size: 16px;
    padding: 5px 10px;
    float: right;
    cursor: pointer;
}
   @media screen and (max-width 768px){
   .col-md-4{
   width: 95% !important;
   }
   }
   .form-control {
    display: block;
    width: 100%;
    height: calc(1.5em + .75rem + 2px);
    padding: .375rem .75rem;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
p{
	font-family: 'Open Sans', sans-serif;
    font-size: 14px;
}
ul.tabs-vertical-frame li a {
    display: block;
    border-bottom: 5px solid #dcdce1;
    margin: 0px;
    font-size: 14px;
    padding: 10px 10px;
    background: #ffffff;
    color: #666666;
}
.h1, h1 {
    font-size: 1.75rem;
}
.ls-bottom-nav-wrapper{
	visibility: hidden !important;
}
.h4, h4 ,.border-title,h2{
    font-size: 16px;
    font-weight: bold;
}
.h3, h3 {
    font-size: 15px;
    font-weight: bold;
}
.welcome p {
    font-size: 14px;
}
.intro-text h2,.h1, h1 {
    font-size: 18px;
    font-weight:bold;
}
.legendColorBox, .legendLabel {
    float: left;
    margin-right: 10px;
}
.legendLabel {
    font-size: 14px;
}
</style>
<div id="main">
<!-- home section Starts here -->
<section id="home" class="content">
   <div class="fullwidthbanner-container banner">
      <div class="fullwidthbanner">
         <ul>
            <?php $i=1; foreach ($slides as $key=>$slide){ ?>
            <?php if($slide['status']==1){?>
            <li data-transition="random" data-slotamount="7" data-masterspeed="300" data-delay="10000" >
               <img src="<?php echo base_url();?>images/slides/<?php echo $slide['image']; ?>" alt="dddgdgdgdgd"  data-fullwidthcentering="on">
               <?php echo $slide['description']; ?>             
            </li>
            <?php } }?>
         </ul>
      </div>
   </div>
   <?php $i=1; foreach ($homeaboutus as $key=>$homeabout){ ?>
   <?php if($homeabout['status']==1){?>
   <div class="shadow"></div>
   <div class="container">
      <div class="aligncenter welcome">
         <div class="margin35"></div>
         <h1><?php echo $homeabout['title']; ?></h1>
         <?php echo $homeabout['description']; ?>
      </div>
   </div>
   <?php } }?>	
</section>
<?php $i=1; foreach ($aboutus as $key=>$about){ ?>
<?php if($about['status']==1){?>
<section id="about" class="content">
   <div class="main-title">
      <div class="container">
         <h2><?php echo $about['title']; ?></h2>
      </div>
   </div>
   <div class="content-main">
      <div class="container">
         <?php echo $about['description']; ?>
      </div>
      <div class="intro-text">
         <div class="container">
            <h2>Now its up to you to decide</h2>
            <div class="margin10"></div>
            <p>Our client software and services streamline drug dispensing, promote patient medication adherence and clinical programs, and manage medical claims and benefits. </p>
            <div class="demo-btn">
               <a href="#" class="purchase">purchase</a>
               <span>or</span>
               <a href="#" class="demo">view demo</a>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="margin20"></div>
         <div class="bottom-slider">
            <div id="layerslider-container">
               <div id="layerslider" style="margin:0px auto;">
                  <div class="ls-layer"  style="slidedirection:right; slidedelay:4000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition2d:all;">
                     <img src="<?php echo base_url();?>assets/website/images/layerslider/slider1-bg.jpg" class="ls-bg" alt="Slide background">
                     <h2  class="ls-s-1 ls1" style="position:absolute; top:157px; left:74px; slidedirection:top;  durationin:1000; durationout:1000; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:1000; delayout:0; showuntil:0;"> 
                        Features of Software 
                     </h2>
                     <img class="ls-s-1" alt="" title=""  src="<?php echo base_url();?>assets/website/images/layerslider/girl-with-ipad1.png"  style="position:absolute; top:-23px; left:381px; slidedirection:bottom;   durationin:1000; durationout:1000; easingin:easeInExpo; easingout:easeInOutQuint; delayin:0; delayout:0; showuntil:0;">
                     <h4  class="ls-s-1 ls1" style="position:absolute; top:235px; left:94px; slidedirection:left;  durationin:1000; durationout:1000; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:2000; delayout:0; showuntil:0;"> 
                        Enhanced client Software.
                     </h4>
                     <h4  class="ls-s-1 ls1" style="position:absolute; top:270px; left:94px; slidedirection:left;  durationin:1000; durationout:1000; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:2500; delayout:0; showuntil:0;"> 
                        Customized Workflow Management.
                     </h4>
                     <h4  class="ls-s-1 ls1" style="position:absolute; top:305px; left:94px; slidedirection:left;  durationin:1000; durationout:1000; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:3000; delayout:0; showuntil:0;"> 
                        Patient Communication
                     </h4>
                  </div>
                  <div class="ls-layer"  style="slidedirection:right; slidedelay:6000; durationin:1500; durationout:1500; easingin:easeInOutQuint; easingout:easeInOutQuint; delayin:0; delayout:0; timeshift:0; transition2d:all;">
                     <img src="<?php echo base_url();?>assets/website/images/layerslider/slider1-bg.jpg" class="ls-bg" alt="Slide background">
                     <img class="ls-s-1" alt="" title=""  src="<?php echo base_url();?>assets/website/images/layerslider/girl-with-slider.png"  style="position:absolute; top:43px; left:-68px; slidedirection:left;   durationin:1000; durationout:1000; easingin:easeOutQuint; easingout:easeInOutQuint; delayin:1000; delayout:0; showuntil:0;">
                     <img class="ls-s-1" alt="" title=""  src="<?php echo base_url();?>assets/website/images/layerslider/bubble-text1.png"  style="position:absolute; top:38px; left:613px; slidedirection:fade;   durationin:1000; durationout:1000; easingin:swing; easingout:easeInOutQuint; delayin:1500; delayout:0; showuntil:0;">
                     
                  </div>
               </div>
            </div>
         </div>
         <div class="margin30"></div>
      </div>
   </div>
</section>
<?php } }?>	
<section id="services" class="content">
   <div class="main-title">
      <div class="container">
         <h2>Services</h2>
      </div>
   </div>
   <div class="content-main">
      <div class="container">
         <?php $i=1; foreach ($homeservices as $key=>$hs){ ?>
         <?php if($hs['status']==1){?>
         <?php echo $hs['description']; ?>
         <?php } }?>	
         <!--h2 class="border-title">Testimonials<span></span></h2>
         <div class="margin25"></div>
         <div class="testimonial-wrapper">
            <ul class="quotes_wrapper">
               <?php $i=1; foreach ($testimonials as $key=>$testimonial){ ?>
               <?php if($testimonial['status']==1){?>
               <li>
                  <figure class="testimonial-thumb animate" data-animation="fadeInUp">
                     <span class="item-mask"> </span>
                     <img class="thumb-image" src="<?php echo base_url();?>images/testimonials/<?php echo $testimonial['image'];?>" alt="" title="" width="180" height="206">
                  </figure>
                  <div class="testimonial-content-wrapper">
                     <div class="author-meta">
                        <p><?php echo $testimonial['name'];?></p>
                        <span><?php echo $testimonial['designation'];?></span>
                     </div>
                     <blockquote> <?php echo $testimonial['description'];?></blockquote>
                  </div>
               </li>
               <?php } }?>
            </ul>
         </div-->
      </div>
   </div>
</section>
<?php $i=1; foreach ($features as $key=>$feature){ ?>
<?php if($feature['status']==1){?>
<section id="features" class="content">
   <div class="main-title">
      <div class="container">
         <h2><?php echo $feature['title']; ?></h2>
      </div>
   </div>
   <div class="content-main">
      <div class="container">
         <?php echo $feature['description']; ?>
      </div>
   </div>
</section>
<?php } }?>
<?php $i=1; foreach ($contactus as $key=>$contact){ ?>
<?php if($contact['status']==1){?>
<section id="contact" class="content">
   <div class="main-title">
      <div class="container">
         <h2><?php echo $contact['title']; ?></h2>
      </div>
   </div>
   <div class="content-main">
      <div class="container">
         <div class="location">
		 <div class="mapouter"><div class="gmap_canvas"><iframe height="280" id="gmap_canvas" src="https://maps.google.com/maps?q=917%20Franklin%20Avenue%20Newark%2C%20NJ%2007107-2809&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.whatismyip-address.com"></a></div><style>.mapouter{position:relative;text-align:right;height:280;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:280px;width:100%;}</style></div>

            <div class="margin20"></div>
            <div class="contact-info">
               <div class="one-third column">
                  <h4>Address</h4>
                  <p><i aria-hidden="true" style="color:red;font-size: 22px;    position: relative;top: 10px;" class="fa fa-map-marker"></i>&nbsp;<?php echo $contact['address1']; ?><?php if($contact['address2'] !=''){?><br><?php echo $contact['address2']; ?><?php }?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $contact['city']; ?>, <?php echo $contact['state']; ?> <?php echo $contact['zip']; ?> </p>
               </div>
               <div class="one-third column last">
                  <div class="column">
                    <h4>Contact Info</h4>
                    <p><img src="https://www.iekait.com/images/mail.png"> &nbsp;<a style="float:right;" href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a><br><img src="https://www.iekait.com/images/telephone.png">&nbsp;<a style="float:right;;" href="tel:<?php echo $contact['phone']; ?>"><?php echo $contact['phone']; ?></a></p>
                  </div>
               </div>
			   <div class="one-third column">
					<h4>Follow Us</h4>
					<?php $i=1; foreach ($contactus as $key=>$contact){ ?>
					<ul class="social-media">
						<li><a href="<?php echo $contact['facebook']; ?>" class="icon-facebook" target="_blank"></a></li>
						<li><a href="<?php echo $contact['instagram']; ?>" class="icon-instagram" target="_blank"></a></li>
						<li><a href="<?php echo $contact['twitter']; ?>" class="icon-twitter" target="_blank"></a></li>
						<li><a href="<?php echo $contact['linkedin']; ?>" class="icon-linkedin" target="_blank"></a></li>
						<li><a href="<?php echo $contact['pinterest']; ?>" class="icon-pinterest" target="_blank"></a></li>
						<li><a href="<?php echo $contact['youtube']; ?>" class="icon-youtube" target="_blank"></a></li>
					</ul>
					<?php }?>
               </div>
               <div class="margin20"> </div>
               <div id="ajax_contact_msg"></div>
               <div class="center">
                  <?php if($this->session->flashdata('success')): ?>
                  <div class="alert alert-success alert-dismissible" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                     </button>
                     <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <?php endif; ?>
               </div>
               <div id="contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>
               <form id="contact-form" class="contact-form" action="<?php echo base_url(); ?>welcome/contact" enctype="multipart/form-data" method="POST">
                  <fieldset>
                     <div class="row ">
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <input type="text" placeholder="* Last Name" class="form-control" name="last_name" id="last_name">
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
						      <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <input type="text" placeholder="* First Name" class="form-control" name="first_name" id="first_name">
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <input type="text" placeholder="*Phone Number" class="form-control us_phone" name="phone" id="phone">
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <input type="text" placeholder="*E-Mail Address" class="form-control" name="email" id="email">
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <select class="form-control" name="subject_type" id="subject" onchange="showDiv(this)">
                                 <option value="General/Other">General/Other</option>
                                 <!--option value="Review">Feedback</option-->
                                 <option value="Question">Client Question</option>
                                 <option value="suggestion">Suggestion</option>
                              </select>
                           </div>
                           <div class="col-md-offset-5 col-md-4 " id="hidden_div" style="display:none;margin: 6px;">
                              <div class="form-group star-rating" style="float:right;">
                                 <span class="fa fa-star-o" data-rating="1"></span>
                                 <span class="fa fa-star-o" data-rating="2"></span>
                                 <span class="fa fa-star-o" data-rating="3"></span>
                                 <span class="fa fa-star-o" data-rating="4"></span>
                                 <span class="fa fa-star-o" data-rating="5"></span>
                                 <input type="hidden" name="rating" id="rating" class="rating-value" value="0">
                              </div>
                           </div>
                        </div>
						      <div class="col-md-4 col-sm-12">
                           <div class="form-group">
                              <input type="text" placeholder="Subject" class="form-control" name="subject">
                              <div class="help-block with-errors"></div>
                           </div>
                        </div>
						      <div class="col-md-12 col-sm-12">
                           <div class="form-group" style="    margin-bottom: 0.5rem;">
                              <textarea placeholder="*Please type your Message here" class="textarea form-control" name="message" id="message" rows="7" cols="20" ></textarea>
                           </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                           <div class="form-group">
                              <input type="submit" class="button" value="Submit" name="btnsend">
                           </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-sm-12">
                           <div class="form-response"></div>
                        </div>
                     </div>
                  </fieldset>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<?php } }?>	
<!-- contact section Ends here -->
<script>
   function showDiv(elem){
     if(elem.value == 'Review')
     document.getElementById('hidden_div').style.display = "block";
     else if(elem.value !== 'Review')
     document.getElementById('hidden_div').style.display="none";
   }
</script>
<script>
   var $star_rating = $('.star-rating .fa');
   
   var SetRatingStar = function() {
     return $star_rating.each(function() {
   	if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
   	  return $(this).removeClass('fa-star-o').addClass('fa-star');
   	} else {
   	  return $(this).removeClass('fa-star').addClass('fa-star-o');
   	}
     });
   };
   
   $star_rating.on('click', function() {
     $star_rating.siblings('input.rating-value').val($(this).data('rating'));
     return SetRatingStar();
   });
   
   SetRatingStar();
   $(document).ready(function() {
   
   });
   
   $('body').on('keypress', 'input', function(e) {
   if (e.which === 32 && !this.value.length) e.preventDefault(); 
   
   });
   $('body').on('input', '#lname', function() {
   	var txt = '';
   	txt = $(this).val();
   	console.log(txt);
   	$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
   });
   $('body').on('input', '#fname', function() {
   	var txt = '';
   	txt = $(this).val();
   	$(this).val(txt.replace(/[^A-Za-z.\s]/g, ''));
   });
   $('body').on('input', 'input', function() {
   	$(this).next('.error').hide();
   });
   $('body').on('input', 'textarea', function() {
   	$(this).next('.error').hide();
   });
   $('body').on('change', '#subject', function() {
   	$(this).next('.error').hide();
   });
   $('body').on('change', '#message', function() {
   	$(this).next('.error').hide();
   });
</script>	
<script> 
   $(document).ready(function() {
    
     $('.contact-form').submit(function(e) {
       var last_name = $('#last_name').val();
	   var first_name = $('#first_name').val();
       var email = $('#email').val();
       var phone = $('#phone').val();
        var subject =$('#subject').val();
       var review =$('.rating-value').val();
       var message = $('#message').val();
      
    
       $(".error").remove();
       $(".error3").remove();
       if (last_name.length < 1) {
         $('#last_name').after('<span class="error last_name" style="color:red">Please enter  Last Name</span>');
          e.preventDefault();
       }else{
           if (last_name.length < 3) {
             $('#last_name').after('<span class="error last_name" style="color:red"> Last Name should be greater than 3 characters</span>');
              e.preventDefault();
           }
          
       }
		if (first_name.length < 1) {
         $('#first_name').after('<span class="error first_name" style="color:red">Please enter  First Name</span>');
          e.preventDefault();
		}else{
           if (first_name.length < 3) {
             $('#first_name').after('<span class="error first_name" style="color:red">First Name should be greater than 3 characters</span>');
              e.preventDefault();
           }
       }
     
       if (email.length < 1) {
         $('#email').after('<span class="error error3 email" style="color:red">Please enter  E-Mail Address</span>');
          e.preventDefault();
       } else {
         var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         var validEmail = regEx.test(email);
         if (!validEmail) {
           $('#email').after('<span class="error error3 email" style="color:red">Enter a valid E-Mail Address</span>');
            e.preventDefault();
         }
       }
       if (phone.length < 1) {
         $('#phone').after('<span class="error phone" style="color:red">Please enter  Phone Number</span>');
          e.preventDefault();
       } else {
         if (phone.length != 17) {
           $('#phone').after('<span class="error phone" style="color:red">Enter a valid  Phone Number</span>');
            e.preventDefault();
         }
       }
       if (subject == "Review" && review == 0) {
         $('#rating').after('<br><span class="error" style="color:red;font-size: 15px;">Please select the review</span>');
          e.preventDefault();
       }
       else {
         if (subject == "Review" && review < 3) {
            $('#rating').after('<br><span class="error" style="color:red;font-size: 15px;">Minimum 3 rating should be checked</span>');
            e.preventDefault();
         }
       }
       if (message.length < 1) {
         $('#message').after('<span class="error message" style="color:red">Please enter Message</span>');
          e.preventDefault();
       }
   
     
       
       else{
           return true;
       }
     });
    
   });
</script> 
<script>
   $(function () {
   	  $('#name').keypress(function (e) {
           var regex = new RegExp("^[a-zA-Z ]+$");
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
           if (regex.test(str)) {
   			$('.fname_error').html('');
               return true;
           }
           else
           {
           e.preventDefault();
           $('.fname_error').html('Please Enter Alphabets Only');
           return false;
           }
       });
   	
$(".us_phone").on('input', function() {
    var phone_value=$(this).val();
    var inputValue = getInputValue_phone(phone_value); //get value from input and make it usefull number
    var length = inputValue.length; //get lenth of input
    
    if (length == 0)
    {
        $(this).val('');
        return false;
    }    
    if (inputValue < 1000)
    {
        inputValue = '+1' + ' ' + '('+inputValue;
    }else if (inputValue < 1000000) 
    {
        inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, length);
    }else if (inputValue < 10000000000) 
    {
        inputValue ='+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, length);
    }else
    {
        inputValue = '+1' + ' ' + '('+ inputValue.substring(0, 3) + ')' + ' ' + inputValue.substring(3, 6) + '-' + inputValue.substring(6, 10);
    }       
    $(this).val(inputValue); 
    inputValue = getInputValue_phone(phone_value);
});

function getInputValue_phone(phone_value) {
    var inputValue = phone_value.replace(/\D/g,'');  //remove all non numeric character
    if (inputValue.charAt(0) == +1) // if first character is 1 than remove it.
    {
        var inputValue = inputValue.substring(1, inputValue.length);
    }
    return inputValue;
}
      
      $("#phone").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           $('.secphone_error').html('Please Enter Numbers Only');
                  return false;
       }else{
   		$('.secphone_error').html('');
   	}
      });
      
     
     });
</script>			
<script type="text/javascript">
   $("#template-contactform").validate({
   	submitHandler: function(form) {
   		$('.form-process').fadeIn();
   		$(form).ajaxSubmit({
   			target: '#contact-form-result',
   			success: function() {
   				$('.form-process').fadeOut();
   				$('#template-contactform').find('.form-control').val('');
   				$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
   				IGNITE.widget.notifications($('#contact-form-result'));
   			}
   		});
   	}
   });
</script>