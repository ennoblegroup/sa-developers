<style>
   .carousel {
   width: 100%;
   float: left;
   margin: 15px 0px;
   }
   .titfont{
   font-size: 50px !important;
   }
   .content {
   position: absolute;
   display: inline-block;
   transform: translate(-50%,-50%);
   top: 50%;
   left: 50%;
   color: #FFF;
   width: 100%;
   text-align: center;
   z-index: 999;
   }
   .play {
   font-size: 20px;
   cursor: pointer;
   border: 3px solid #50aab2;
   display: inline-block;
   text-align: center;
   padding: 5px 15px;
   background-color: #f5f5f5;
   }
   .play:hover {
   color: red;
   }
   .owl-stage-outer{
   margin-top: -15px;
   }
.owl-carousel .owl-item img {
    transform-style: inherit;
	 height:300px;
    min-height:300px;
    max-height:300px;
}
.carousel-control.left,.carousel-control.right{
   background-image:none !important;
}
. carousel-control{
	background-image:none;
}
.vend{
    height: 80px !important;
    min-height:80px !important;
    max-height:80px !important;
}
</style>
<script src="<?php echo base_url();?>assets/website/js/owl.carousel.js"></script>
<!-- CONTENT START -->
<div class="page-content">
   <!-- INNER PAGE BANNER -->
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/5.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white"><?php echo $project['project_name']?></h2>
               </div>
            </div>
            <!-- BREADCRUMB ROW -->                            
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li><?php echo $project['project_name']?></li>
               </ul>
            </div>
            <!-- BREADCRUMB ROW END -->                        
         </div>
      </div>
   </div>
   <!-- INNER PAGE BANNER END -->
   <!-- SECTION CONTENT START -->
   <div class="section-full small-device  p-tb80">
      <div class="container">
         <div class="project-detail-outer">
            <div class="row">
				<?php if(count($project['before_images'])>0){?>
				<div class="col-md-6">
               <h4>BEFORE</h4>
					<div id="before" class="carousel owl-carousel owl-theme">
						<?php $i=1; foreach($project['before_images'] as $row): ?>
						<div class="item <?php if($i==1){echo 'active';}?>">
							<img before_id="<?php echo $row['simage_id'] ?>" src="<?php echo base_url();?>images/project_gallery/<?php echo $row['file'] ?>">
						</div>
						<?php $i++; endforeach; ?>
					</div>
				</div>
				<div class="col-md-6">
               <h4>AFTER</h4>
					<div class="project-detail-pic m-b30">
						<div id="" class="after carousel owl-carousel owl-theme">
                        
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-md-6">
                  <div class="product-block bg-gray p-a30 p-b15 m-b10">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 ">
                           <h4 class="m-b10">Client</h4>
                           <p><?php echo $project['lastname']?> <?php echo $project['firstname']?></p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                           <h4 class="m-b10">Project Type</h4>
                           <p>
                              <?php $i=1; foreach($project['services'] as $row): ?>
                              <?php if($i != 1){ echo ', ';}?><?php echo $row['service_name'] ?>
                              <?php $i++; endforeach; ?>
                           </p>
                        </div>
                        <div class="col-md-6 col-sm-6 ">
                           <h4 class="m-b10">Location</h4>
                           <p><?php echo $project['city']?>, <?php echo $project['state']?></p>
                        </div>
                        <div class="col-md-6 col-sm-6 ">
                           <h4 class="m-b10">Started on</h4>
                           <p><?php echo  date('F j, Y', strtotime($project['project_start_date']));?></p>
                        </div>
                        <!--div class="col-md-6 col-sm-6 m-b30">
                           <h4 class="m-b10">Cost</h4>
                           <p>$163,700 USD</p>
                        </div>
                        <div class="col-md-6 col-sm-6 m-b30">
                           <h4 class="m-b10">Interesting aspects</h4>
                           <p>Commercial-style flooring</p>
                        </div-->
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="owl-carousel home-carousel-1 owl-btn-bottom-left">
					<?php $i=1; foreach ($project['testimonials'] as $key=>$test){ ?>
                     <div class="item">
                        <div class="testimonial-5">
                           <div class="testimonial-text">
                              <div class="testimonial-paragraph">
                                 <span class="fa fa-quote-left text-primary"></span>
                                 <p><?php echo $test['message']?></p>
                              </div>
                           </div>
                           <div class="clearfix">
                              <div class="testimonial-detail clearfix">
                                 <strong class="testimonial-name text-black"><?php echo $test['last_name']?> <?php echo $test['first_name']?></strong>
                              </div>
                           </div>
                        </div>
                     </div>
					<?php }?>
                  </div>
               </div>
            </div>
            <div class="project-detail-containt">
               <div class="bg-white text-black">
                  <div class="row">
                     <div class="col-md-12 col-sm-12">									
                        <?php echo $project['project_description'] ?>                                            
                     </div>
                  </div>
               </div>
            </div>
            <?php if(count($project['videos'])>0){?>
            <div class="section-full small-device p-t80 p-b40">
               <div class="container">
                  <!-- TITLE START -->
                  <div class="section-head text-center">
                     <div class="wt-separator-outer separator-center">
                        <div class="wt-separator">
                           <span class="text-primary text-uppercase sep-line-one ">Videos</span>
                        </div>
                     </div>
                     <!--h2>Latest News</h2-->
                  </div>
                  <!-- TITLE END -->                   
                  <!-- TITLE START -->
                  <!-- IMAGE CAROUSEL START -->
                  <div class="section-content">
                     <div class="section-content p-tb10 owl-btn-bottom-center">
                        <div class="owl-carousel blog-carousel-3">
							<?php $i=1; foreach($project['videos'] as $row): ?>
                           <div class="item">
                              <div class="blog-post latest-blog-1  date-style-1">
                                <a href="">
								<video width="100%" height="auto" controls>
									<source src="<?= base_url() ?>images/project_gallery/<?= $row['file']; ?>" type="video/mp4">
								</video>
								</a>
                              </div>
                           </div>
						   <?php $i++; endforeach; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="section-content">
                  <div class="row">
                     <div class="col-md-4 col-sm-12">
                     </div>
                  </div>
               </div>
            </div>
			<?php }?>
         </div>
         <!-- OUR Video END -->
         <!-- CLIENT LOGO SECTION START -->
		 <?php if(count($project['vendors'])>0){?>
         <div class="section-full small-device bg-gray p-t81 p-b61">
            <div class="container">
               <div class="section-head text-center">
                  <div class="wt-separator-outer separator-center">
                     <div class="wt-separator">
                        <span class="text-primary text-uppercase sep-line-one ">Our Vendors</span>
                     </div>
                  </div>
               </div>
               <div class="section-content">
                  <div class="section-content p-tb10 owl-btn-vertical-center">
                     <div class="owl-carousel vendd home-client-carousel-2" >
						<?php $i=1; foreach ($project['vendors'] as $key=>$vendor){ ?>
						  <?php if($vendor['status']==1){?>
						  <div class="item">
							 <div class="ow-client-logo">
								<div class="client-logo client-logo-media">
								   <a href="javascript:void(0);"><img class="vend" src="<?php echo base_url();?>images/vendors/<?php echo $vendor['image']; ?>" alt=""></a>
								</div>
							 </div>
						  </div>
						  <?php $i++; }}?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
		 <?php }?>
         <!-- CLIENT LOGO  SECTION End -->                    
         <!-- OUR RELATED PROJECTS START -->
		 <?php if(count($project['related_projects'])>0){?>
         <div class="section-full small-device p-t80 p-b40">
            <div class="container">
               <!-- TITLE START -->
               <div class="section-head text-center">
                  <div class="wt-separator-outer separator-center">
                     <div class="wt-separator">
                        <span class="text-primary text-uppercase sep-line-one ">Related Projects</span>
                     </div>
                  </div>
                  <!--h2>Latest News</h2-->
               </div>
               <!-- TITLE END -->                   
               <!-- TITLE START -->
               <!-- IMAGE CAROUSEL START -->
               <div class="section-content">
                  <div class="section-content p-tb10 owl-btn-bottom-center">
                     <div class="owl-carousel blog-carousel-3">
						<?php $i=1; foreach ($project['related_projects'] as $key=>$rp){ ?>
                        <div class="item">
                           <div class="blog-post latest-blog-1  date-style-1">
                              <div class="wt-post-media wt-img-effect">
                                 <a href="<?php echo base_url();?>website/projects/view_project/<?php echo $rp['id']?>"><img src="<?php echo base_url();?>images/project_gallery/<?php echo $rp['image']['file']; ?>" alt=""></a>
                              </div>
                              <div class="wt-post-text">
                                 <p class="post-title"><a href="<?php echo base_url();?>website/projects/view_project/<?php echo $rp['id']?>"><?php echo $rp['project_name']; ?></a></p>
                              </div>
                           </div>
                        </div>
						<?php $i++; }?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="section-content">
               <div class="row">
                  <div class="col-md-4 col-sm-12">
                  </div>
               </div>
            </div>
         </div>
		 <?php }?>
      </div>
      <!-- OUR RELATED PROJECTS END -->  
   </div>
</div>
</div>
<!-- SECTION CONTENT END  -->
</div>
<!-- CONTENT END -->
<script>
$(document).ready(function() {
    var owl = $('#before');
	var aowl= $('.after');
	var vowl = $('.vendd');
	vowl.owlCarousel({
		margin: 10,
		nav: true,
		autoplay:false,
		autoHeight:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		loop: false,
		dots:false,
		navText : ['<a class="left carousel-control"  data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>','<a class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span> </a>'],
		responsive: {
		  0: {
			items: 1
		  },
		  600: {
			items: 1
		  },
		  1000: {
			items: 1
		  }
		}
	})
	aowl.owlCarousel({
		margin: 10,
		nav: true,
		autoplay:true,
		autoplayTimeout:50000,
		autoplayHoverPause:true,
		loop: false,
		dots:false,
		onRefreshed: adjustStretchHeader,
		navText : ['<a class="left carousel-control"  data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>','<a class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span> </a>'],
		responsive: {
		  0: {
			items: 1
		  },
		  600: {
			items: 1
		  },
		  1000: {
			items: 1
		  }
		}
	})
	owl.owlCarousel({
		margin: 10,
		nav: true,
		autoplay:false,
		autoHeight:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		loop: true,
		dots:false,
		navText : ['<a class="left carousel-control"  data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>','<a class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span> </a>'],
		responsive: {
		  0: {
			items: 1
		  },
		  600: {
			items: 1
		  },
		  1000: {
			items: 1
		  }
		}
	})
	owl.on('changed.owl.carousel',function(property){
      //$('.after .item').remove();
		var current = property.item.index;
		var src = $(property.target).find(".owl-item").eq(current).find("img").attr('before_id');
		console.log('Image current is ' + src);
		$.ajax({
			type:'POST',
			url:'../get_all_after_images',
			data:'id='+src,
			success:function(data){
				var out =JSON.parse(data);
				console.log(out);
				for(var i=0 ; i< out.length ; i++) {
					$('<div class="item">'+
						'<img  src="<?php echo base_url();?>images/project_gallery/'+out[i]['file']+'?>">'+
					 '</div>').appendTo('.after');

				}
				$('.item').first().addClass('active');
				aowl.trigger('destroy.owl.carousel');
				aowl.owlCarousel({
					margin: 10,
					nav: true,
					autoHeight:false,
					autoplay:true,
					autoplayTimeout:1000,
					autoplayHoverPause:true,
					loop: false,
					dots:false,
					navText : ['<a class="left carousel-control"  data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>','<a class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span> </a>'],
					responsive: {
					  0: {
						items: 1
					  },
					  600: {
						items: 1
					  },
					  1000: {
						items: 1
					  }
					}
				});
			}
		});
	});
	function adjustStretchHeader () {
		var src = $('#before').find(".active>img").attr('before_id');
		$.ajax({
			type:'POST',
			url:'../get_all_after_images',
			data:'id='+src,
			success:function(data){
				var out =JSON.parse(data);
				console.log(out);
				for(var i=0 ; i< out.length ; i++) {
					$('<div class="item">'+
						'<img  src="<?php echo base_url();?>images/project_gallery/'+out[i]['file']+'?>">'+
					 '</div>').appendTo('.after');

				}
				$('.item').first().addClass('active');
				aowl.trigger('destroy.owl.carousel');
				aowl.owlCarousel({
					margin: 10,
					nav: true,
					autoHeight:false,
					autoplay:true,
					autoplayTimeout:1000,
					autoplayHoverPause:true,
					loop: false,
					dots:false,
					navText : ['<a class="left carousel-control"  data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>','<a class="right carousel-control" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span> </a>'],
					responsive: {
					  0: {
						items: 1
					  },
					  600: {
						items: 1
					  },
					  1000: {
						items: 1
					  }
					}
				});
			}
		});
	}
});

</script>