
	   <div class="page-content">
        
            <!-- INNER PAGE BANNER -->
            <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/1.jpg);">
            	<div class="overlay-main bg-black opacity-07"></div>
                <div class="container">
                    <div class="wt-bnr-inr-entry">
                    	<div class="banner-title-outer">
                        	<div class="banner-title-name">
                        		<h2 class="text-white">About Us</h2>
                            </div>
                        </div>
                            <div>
                                <ul class="wt-breadcrumb breadcrumb-style-2">
                                    <li><a href="<?php echo base_url();?>">Home</a></li>
                                    <li>About Us</li>
                                </ul>
                            </div>                       
                    </div>
                </div>
            </div>
	
	
	
	<div class="section-full p-tb80 bg-white">
                <div class="container">
                    <div class="section-content">
                    	<div class="row">
                        	<div class="col-md-5 col-sm-12">
                            	<img src="<?php echo base_url();?>images/about/<?php echo $aboutus['image']; ?>">
                            </div>
							
                    		<div class="col-md-7 col-sm-12">
                                <!-- TITLE START -->
                                <div class="section-head">
                                	<div class="wt-separator-outer separator-left">
                                		<div class="wt-separator">
                                            <span class="text-primary text-uppercase sep-line-one "><?php echo $aboutus['title']; ?> </span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- TITLE END -->                            
                               <?php echo $aboutus['description']; ?>                                   
								   
                                </div> 	                      
                        </div>
                        
                    </div>
                </div>
            </div>   
            
	
	
	
	
	
	
	      <?php if($aboutus['is_team']==1){?>
            <!-- OUR TEAM START -->
			 <div class="section-full small-device p-t80 p-b50 bg-gray">
				<div class="container">
                
                    <div class="section-head text-center">
                        <div class="wt-separator-outer separator-center">
                            <div class="wt-separator">
                                <span class="text-primary text-uppercase sep-line-one ">Our Team</span>
                            </div>
                        </div>
                        <!--h2>Our Team</h2-->
                    </div>
					
                    <div class="row">
					<?php $i=1; foreach ($team as $key=>$team){	?>
					<div class="col-md-3">
					   <div class="our-team-one">
						  <div class="team-bg">
							 <h4><?php echo $team['lastname']?> <?php echo $team['firstname']?></h4>
						  </div>
						  <div class="team-img">
							 <img src="<?php echo base_url();?>images/team/<?php echo $team['image']?>" alt="" />
							 <ul class="list-unstyled">
								<h5 style="color:#fff;"><?php echo $team['designation']?></h5>
								<li><a href="javascript:void(0);" class="fa fa-google"></a></li>
								<li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
								<li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
								<li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
							 </ul>
						  </div>
					   </div>
					</div>
					<?php $i++; }?> 		
				 </div>
                </div>
                
             </div>
            <?php }?>
	
	
		
            
			
			
		              
            <!-- WHAT WE DO SECTION START -->
            <div class="section-full p-t80 p-b50 bg-gray">
                <div class="container">
                    <!-- TITLE START -->
                    <div class="section-head text-center">
                        <div class="wt-separator-outer separator-center">
                            <div class="wt-separator">
                                <span class="text-primary text-uppercase sep-line-one ">What We Are</span>
                            </div>
                        </div>
                        <!--h2>What we do</h2-->
                    </div>
                    <!-- TITLE END -->
                    <div class="section-content">
                    	<div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                            	<div class="hover-box-effect  v-icon-effect">
                                    <div class="wt-icon-box-wraper center p-lr30  p-b50 p-t50 bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><i class="v-icon flaticon-sketch"></i></span>
                                        </div>
                                        <div class="icon-content text-black">
                                            <h4 class="wt-tilte m-b25">Creative</h4>
                                            <p>As a leader in our industry, we have always worked to develop technology.</p>
                                            <!--a href="#" class="site-button-link" data-hover="Read More">Read More</a-->
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>                          
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                                <div class="hover-box-effect v-icon-effect">
                                    <div class="wt-icon-box-wraper center p-lr30  p-b50 p-t50  bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><i class="v-icon flaticon-window"></i></span>
                                        </div>
                                        <div class="icon-content text-black">
                                            <h4 class="wt-tilte m-b25">Honest & Dependable</h4>
                                            <p>Our expert teams are selected specifically for each client.</p>
                                            <!--a href="#" class="site-button-link" data-hover="Read More">Read More</a-->
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                            	<div class="hover-box-effect v-icon-effect">
                                    <div class="wt-icon-box-wraper center p-lr30  p-b50 p-t50  bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><i class="v-icon flaticon-window-5"></i></span>
                                        </div>
                                        <div class="icon-content text-black">
                                            <h4 class="wt-tilte m-b25">Committed</h4>
                                            <p>We let our quality work and commitment to customer satisfaction be our slogan.</p>
                                            <!--a href="#" class="site-button-link" data-hover="Read More">Read More</a-->
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                            	<div class="hover-box-effect v-icon-effect">
                                    <div class="wt-icon-box-wraper center p-lr30  p-b50 p-t50 bg-white">
                                        <div class="icon-lg text-primary m-b20">
                                            <span class="icon-cell text-primary"><i class="v-icon flaticon-plant"></i></span>
                                        </div>
                                        <div class="icon-content text-black">
                                            <h4 class="wt-tilte m-b25">Always Improving</h4>
                                            <p>We strive to meet the needs of the world today without jeopardizing.</p>
                                            <!--a href="#" class="site-button-link" data-hover="Read More">Read More</a-->
                                        </div>
                                    </div>
                                    
                                 </div>
                            </div>                          
                        </div>                                
                    </div>
                </div>  
            </div>         
     
            		
           

        </div>
        <!-- CONTENT END -->
    