<style>
   .titfont{
   font-size: 50px !important;
   }
   .content {
   position: absolute;
   display: inline-block;
   transform: translate(-50%,-50%);
   top: 50%;
   left: 50%;
   color: #FFF;
   width: 100%;
   text-align: center;
   z-index: 999;
   }
   .play {
   font-size: 20px;
   cursor: pointer;
   border: 3px solid #50aab2;
   display: inline-block;
   text-align: center;
   padding: 5px 15px;
   background-color: #f5f5f5;
   }
   .play:hover {
   color: red;
   }
   .wt-tilte{
	   margin:0px;
   }
   .vend {
    height: 80px;
   }
   .wt-img-effect.fade-in {
        max-height: 225px;
    }
</style>
<div class="page-content">
   <!-- SLIDER START -->
   <div id="rev_slider_346_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="beforeafterslider1" data-source="gallery" style="background:#252525;padding:0px;">
      <!-- START REVOLUTION SLIDER 5.4.3.3 fullscreen mode -->
      <div id="rev_slider_346_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.3.3">
         <ul>
            <?php $i=1; foreach ($slides as $key=>$slide){ ?>
            <?php if($slide['status']==1){?>
            <!-- SLIDE 1 -->
            <li data-index="rs-964" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="{&quot;revslider-weather-addon&quot; : { &quot;type&quot; : &quot;&quot; ,&quot;name&quot; : &quot;&quot; ,&quot;woeid&quot; : &quot;&quot; ,&quot;unit&quot; : &quot;&quot; }}" data-description="" data-beforeafter='{"moveto":"50%|50%|50%|50%","bgColor":"#e7e7e7","bgType":"image","bgImage":"<?php echo base_url();?>images/slides/<?php echo $slide['before_image']; ?>","bgFit":"cover","bgPos":"center center","bgRepeat":"no-repeat","direction":"horizontal","easing":"Power2.easeInOut","delay":"500","time":"750","out":"fade","carousel":false}'>
               <!-- MAIN IMAGE -->
               <img src="<?php echo base_url();?>images/slides/<?php echo $slide['image']; ?>" data-beforeafter="after"  data-bgcolor=''  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
               <!-- LAYERS -->
               <!-- LAYER NR. 1 text -->
               <div class="tp-caption  titfont tp-resizeme rs-parallaxlevel-5" 
                  id="slide-964-layer-1" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['200','200','200','0']" 
                  data-fontsize="['120','120','120','60']"
                  data-lineheight="['120','120','120','60']"
                  data-letterspacing="['50','50','50','30']"
                  data-height="none"
                  data-whitespace="normal"
                  data-type="text" 
                  data-beforeafter="before" 
                  data-responsive_offset="on" 
                  data-frames='[{"delay":600,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},
                  {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                  data-textAlign="['center','center','center','center']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[50,50,50,50]"
                  style="z-index: 16; white-space: nowrap; font-size: 120px; line-height: 120px; font-weight: 700; color: #000; letter-spacing: 50px;font-family: 'Martel', serif;text-transform:uppercase;">S&A Developers </div>
               <!-- LAYER NR. 2 text -->
               <div class="tp-caption   tp-resizeme rs-parallaxlevel-5" 
                  id="slide-964-layer-2"  
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['300','300','300','100']" 
                  data-width="['960','960','960','320']"
                  data-height="none"
                  data-whitespace="normal"
                  data-type="text" 
                  data-beforeafter="before" 
                  data-responsive_offset="on" 
                  data-frames='[{"delay":600,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},
                  {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                  data-textAlign="['center','center','center','center']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[5,5,5,5]"
                  style="z-index: 11; min-width: 960px; max-width: 960px; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #000; letter-spacing: 5px;font-family:Montserrat;text-transform:uppercase;">Exceptional designing for exceptional Spaces</div>
               <!-- SLIDE RIGHT PART START-->
               <!-- LAYER NR. 1  text-->
               <div class="tp-caption titfont  tp-resizeme  tp-blackshadow rs-parallaxlevel-5" 
                  id="slide-964-layer-4" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['200','200','200','0']" 
                  data-fontsize="['120','120','120','60']"
                  data-lineheight="['120','120','120','60']"
                  data-letterspacing="['50','50','50','30']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-type="text" 
                  data-beforeafter="after" 
                  data-responsive_offset="on" 
                  data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},
                  {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                  data-textAlign="['center','center','center','center']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[50,50,50,50]"
                  style="z-index: 16; white-space: nowrap; font-size: 120px; line-height: 120px; font-weight: 700; color: #ffffff; letter-spacing: 50px;font-family: 'Martel', serif;text-transform:uppercase;">S&A <br> Developers </div>
               <!-- LAYER NR. 2 text -->
               <div class="tp-caption   tp-resizeme rs-parallaxlevel-5" 
                  id="slide-964-layer-5" 
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                  data-y="['middle','middle','middle','middle']" data-voffset="['300','300','300','100']" 
                  data-width="['960','960','960','320']"
                  data-height="none"
                  data-whitespace="normal"
                  data-type="text" 
                  data-beforeafter="after" 
                  data-responsive_offset="on" 
                  data-frames='[{"delay":2100,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},
                  {"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                  data-textAlign="['center','center','center','center']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[5,5,5,5]"
                  style="z-index: 17; min-width: 960px; max-width: 960px; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #ffffff; letter-spacing: 5px;font-family:Montserrat;text-transform:uppercase;">Exceptional designing for exceptional Spaces</div>
               <!-- LAYER NR. 3  button-->
            </li>
            <?php $i++; }}?> 
         </ul>
         <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
      </div>
   </div>
	<!--div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators"></ol>
		<div class="carousel-inner"></div>

		<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div-->
   <div class="section-full p-tb80 bg-white">
      <div class="container">
         <div class="section-content">
            <div class="row">
               <div class="col-md-6 col-sm-12">
                  <div class="index-about3 bg-gray">
                     <!-- TITLE START -->
                     <div class="section-head">
                        <div class="wt-separator-outer separator-left">
                           <div class="wt-separator">
                              <span class="text-primary text-uppercase sep-line-one "> <?php echo $aboutus['title']; ?>       </span>
                           </div>
                        </div>
                        <h2>We Create Amazing Designs</h2>
                     </div>
                     <!-- TITLE END -->                            
                     <?php echo $aboutus['description']; ?>                                   
                  </div>							
               </div>
               <div class="col-md-6 col-sm-12">
                  <div class="welcome-block-three">
                     <div class="wt-box wt-product-gallery on-show-slider">
                        <div id="sync1" class="owl-carousel owl-theme owl-btn-vertical-center m-b5">
                           <div class="item">
                              <div class="mfp-gallery">
                                 <div class="wt-box">
                                    <div class="wt-thum-bx index-about3">
                                       <img src="<?php echo base_url();?>images/about/<?php echo $aboutus['image']; ?>" alt="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--div class="item">
                              <div class="mfp-gallery">
                                  <div class="wt-box">
                                      <div class="wt-thum-bx">
                                          <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                                      </div>
                                  </div>
                              </div>
                              </div>
                              <div class="item">
                              <div class="mfp-gallery">
                                  <div class="wt-box">
                                      <div class="wt-thum-bx">
                                          <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                                      </div>
                                  </div>
                              </div>
                              </div>
                              <div class="item">
                              <div class="mfp-gallery">
                                  <div class="wt-box">
                                      <div class="wt-thum-bx">
                                          <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                                      </div>
                                  </div>
                              </div>
                              </div>
                              <div class="item">
                              <div class="mfp-gallery">
                                  <div class="wt-box">
                                      <div class="wt-thum-bx">
                                          <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                                      </div>
                                  </div>
                              </div>
                              </div-->
                        </div>
                        <!--div id="sync2" class="owl-carousel owl-theme">
                           <div class="item">
                               <div class="wt-media">
                                   <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                               </div>
                           </div>
                           <div class="item">
                               <div class="wt-media">
                                   <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                               </div>
                           </div>
                           <div class="item">
                               <div class="wt-media">
                                   <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                               </div>
                           </div>
                           <div class="item">
                               <div class="wt-media">
                                   <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                               </div>
                           </div>
                           <div class="item">
                               <div class="wt-media">
                                   <img src="<?php echo base_url();?>images/about/<?php echo $homeabout['image']; ?>" alt="">
                               </div>
                           </div>
                           </div-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- WELCOME  SECTION END --> 
   <!-- WHAT WE DO SECTION START -->
   <div class="section-full p-t80 p-b50 bg-gray">
      <div class="container">
         <!-- TITLE START -->
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Our Services</span>
               </div>
            </div>
            <!--h2>What we do</h2-->
         </div>
         <!-- TITLE END -->
         <div class="section-content">
            <div class="row">
               <?php $i=1; foreach ($services as $key=>$service){ ?>
               <?php if($service['status']==1){?>
               <div class="col-md-4 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                  <div class="hover-box-effect  v-icon-effect">
                     <div class="wt-box">
                        <div class="wt-thum-bx wt-img-effect fade-in">
                           <img src="<?php echo base_url();?>images/services/<?php echo $service['image']; ?>" alt="">
                           <!--div class="wt-icon-box-sm bg-white">
                              <span class="icon-cell text-primary"><i class="v-icon flaticon-sketch"></i></span>
                              </div-->                                            
                        </div>
                        <div class="p-a20 bg-white">
                           <div class="icon-content text-black">
                              <h4 class="wt-tilte"><a href="<?php echo base_url();?>website/services"><?php echo $service['service_name']; ?></a></h4>
                              <!--a href="#" class="site-button-link" data-hover="Read More">Read More</a-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php $i++; }}?> 	
            </div>
         </div>
      </div>
   </div>
   <div class="section-full p-t80 p-b50 bg-gray" style="background-color: #f6f7f885;">
      <div class="container">
         <!-- TITLE START -->
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Our Projects</span>
               </div>
            </div>
            <!--h2>What we do</h2-->
         </div>
         <!-- TITLE END -->
         <div class="section-content">
            <div class="row">
            <?php $i=1; foreach ($projects as $key=>$project){	?>
               <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                  <div class="hover-box-effect  v-icon-effect">
                     <div class="wt-box">
                        <div class="wt-thum-bx wt-img-effect fade-in cd">
                           <a href="<?php echo base_url();?>website/projects/view_project/<?php echo $project['id']?>"><img src="<?php echo base_url();?>images/project_gallery/<?php echo $project['image']['file']; ?>" alt=""></a>                                            
                        </div>
                        <div class="p-a10 bg-white">
                           <div class="icon-content text-black">
                              <h4 class="wt-tilte m-b10 m-t0"><a href="<?php echo base_url();?>website/projects/view_project/<?php echo $project['id']?>"><?php echo $project['project_name']; ?></a></h4>
						            <p class="m-b0"><?php echo $project['city']; ?>, USA</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php $i++; }?> 	
            </div>
         </div>
      </div>
   </div>
   <div class="section-full small-device p-t80 p-b50 bg-gray">
      <div class="container">
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Our Team</span>
               </div>
            </div>
            <!--h2>Our Team</h2-->
         </div>
         <div class="row">
			<?php $i=1; foreach ($team as $key=>$team){	?>
            <div class="col-md-3 col-sm-4">
               <div class="our-team-one">
                  <div class="team-bg">
                     <h4><?php echo $team['lastname']?> <?php echo $team['firstname']?></h4>
                  </div>
                  <div class="team-img">
                     <img src="<?php echo base_url();?>images/team/<?php echo $team['image']?>" alt="" />
                     <ul class="list-unstyled">
                        <h5 style="color:#fff;"><?php echo $team['designation']?></h5>
                        <li><a href="javascript:void(0);" class="fa fa-google"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                        <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                     </ul>
                  </div>
               </div>
            </div>
			<?php $i++; }?> 		
         </div>
      </div>
   </div>
   <!-- CLIENT LOGO SECTION START -->
   <div class="section-full small-device bg-white   p-t80 p-b60">
      <div class="container">
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Our Vendors</span>
               </div>
            </div>
         </div>
         <div class="section-content">
            <div class="section-content p-tb10 owl-btn-vertical-center">
               <div class="owl-carousel home-client-carousel-2">
                  <?php $i=1; foreach ($vendors as $key=>$vendor){ ?>
                  <?php if($vendor['status']==1){?>
                  <div class="item">
                     <div class="ow-client-logo">
                        <div class="client-logo client-logo-media">
                           <a href="<?php echo $vendor['website_url']; ?>" target="_blank"><img class="vend" src="<?php echo base_url();?>images/vendors/<?php echo $vendor['image']; ?>" alt=""></a>
                        </div>
                     </div>
                  </div>
                  <?php $i++; }}?> 	
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CLIENT LOGO  SECTION End -->
   <!-- TESTIMONIAL SECTION START -->
   <div class="section-full small-device  p-t80 p-b80 bg-white bg-repeat" style="background-image:url(<?php echo base_url();?>assets/website/images/background/ptn-1.png)">
      <div class="container">
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Testimonials</span>
               </div>
            </div>
            <!--h2>Latest News</h2-->
         </div>
         <div class="section-content">
            <div class="row">
               <div class="col-md-7 m-b30">
                  <div class="owl-carousel home-carousel-1 owl-btn-bottom-left">
                     <?php $i=1; foreach ($reviews as $key=>$test){ ?>
                     <div class="item">
                        <div class="testimonial-5">
                           <div class="testimonial-text">
                              <div class="testimonial-paragraph">
                                 <span class="fa fa-quote-left text-primary"></span>
                                 <p><?php echo $test['message']?></p>
                              </div>
                           </div>
                           <div class="clearfix">
                              <div class="testimonial-detail clearfix">
                                 <strong class="testimonial-name text-black"><?php echo $test['last_name']?> <?php echo $test['first_name']?></strong>
                              </div>
                              <div class="testimonial-pic-block">
                                 <div class="testimonial-pic">
                                    <img src="<?php echo base_url();?>assets/website/images/testimonials/rr.webp" width="132" height="132" alt="">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php }?>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="counter-section-one">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- TESTIMONIAL SECTION END -->               
</div>
