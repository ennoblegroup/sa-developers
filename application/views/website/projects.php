
<div class="page-content">
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>images/banner/2.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white">Project</h2>
               </div>
            </div>                          
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Project</li>
               </ul>
            </div>                     
         </div>
      </div>
   </div>
   <div class="section-full small-device p-t80 p-b30">
      <div class="container">
         <!-- TITLE START -->
         <div class="section-head text-center">
            <div class="wt-separator-outer separator-center">
               <div class="wt-separator">
                  <span class="text-primary text-uppercase sep-line-one ">Our Projects</span>
               </div>
            </div>
            <!--h2>What we do</h2-->
         </div>
         <!-- TITLE END -->
         <div class="section-content">
            <div class="row">
            <?php $i=1; foreach ($projects as $key=>$project){	?>
               <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-b30">
                  <div class="hover-box-effect  v-icon-effect">
                     <div class="wt-box">
                        <div class="wt-thum-bx wt-img-effect fade-in cd">
                           <a href="<?php echo base_url();?>website/projects/view_project/<?php echo $project['id']?>"><img src="<?php echo base_url();?>images/project_gallery/<?php echo $project['image']['file']; ?>" alt=""></a>                                            
                        </div>
                        <div class="p-a10 bg-white">
                           <div class="icon-content text-black">
                              <h4 class="wt-tilte m-b10 m-t0"><a href="<?php echo base_url();?>website/projects/view_project/<?php echo $project['id']?>"><?php echo $project['project_name']; ?></a></h4>
						            <p class="m-b0"><?php echo $project['city']; ?>, USA</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php $i++; }?> 	
            </div>
         </div>
      </div>
   </div>
</div>
