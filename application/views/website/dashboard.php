		<style>
		.budget1{
			padding:0px !important;
		}
		.budget1 p {
			margin-top: 0;
			margin-bottom: 0;
			font-size: 10px;
			font-weight: 600;
		}
		</style>
         <div class="container-fluid mt--6" style="margin-top: 30px;">
            <div class="row">
               <div class="col-md-9">
               </div>
               <div class="col-md-3" style="margin-bottom:20px;">
               </div>
               <!--Accordion wrapper-->
               <div class="accordion md-accordion col-md-12" id="accordionEx" role="tablist" aria-multiselectable="true">
                                    <!-- Accordion card -->
                  <div class="card">
                     <!-- Card header -->
                     <div class="card-header" role="tab" id="headingTwo2">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                           aria-expanded="false" aria-controls="collapseTwo2">
                           <h5 class="mb-0">
                              General<i class="fa fa-angle-down rotate-icon" style="float: right;"></i>
                           </h5>
                        </a>
                     </div>
                     <!-- Card body -->
                     <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                        data-parent="#accordionEx">
                        <div class="card-body">
                           <div class="col-xl-12">
                              <div class="row">
                                 <div class="col">
                                    <div class="card">
                                       <!-- Card header -->
                                       <!--div class="card-header border-0">
                                          <h3 class="mb-0">Light table</h3>
                                          </div-->
                                       <div class="table-responsive">
                                          <table class="table align-items-center table-flush">
                                             <thead class="thead-light">
                                                <tr>
                                                   <th scope="col" class="sort" data-sort="name" style="text-align:left">Business</th>
                                                   <th scope="col" class="sort" data-sort="status">Documents</th>
                                                   <th scope="col">Messages</th>
                                                   <!--th scope="col" class="sort" data-sort="completion">Others</th>
                                                      <th scope="col"></th-->
                                                </tr>
                                             </thead>
											 <tbody class="list">
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center">
                                                         <a>
                                                         General
                                                         </a>
                                                      </div>
                                                   </th>
                                                   <td class="budget">
                                                      <button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-folder" aria-hidden="true"></i></button>&nbsp;
													  <button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-question-circle" aria-hidden="true"></i></button>
                                                   </td>
                                                   <td class="budget">
                                                      <a href="" ><button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-eye" aria-hidden="true"></i></button></a>&nbsp;<a class="open-button" href="javascript:void(0)" onclick="openForm()"><button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-plus" aria-hidden="true"></i></button></a>&nbsp;<button type="button" class="btn btn-sm btn-outline-dark" ><i class="fa fa-comment" aria-hidden="true"></i></button>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Accordion card -->
				  <!-- Accordion card -->
                  <div class="card">
                     <!-- Card header -->
                     <div class="card-header" role="tab" id="headingOne1">
                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                           aria-controls="collapseOne1">
                           <h5 class="mb-0">
                              clients <i class="fa fa-angle-down rotate-icon" style="float: right;"></i>
                           </h5>
                        </a>
                     </div>
                     <!-- Card body -->
                     <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                        data-parent="#accordionEx">
                        <div class="card-body">
                           <div class="col-xl-12">
                              <div class="row">
                                 <div class="col">
                                    <div class="card">
                                       <!-- Card header -->
                                       <!--div class="card-header border-0">
                                          <h3 class="mb-0">Light table</h3>
                                          </div-->
                                       <div class="table-responsive">
                                          <table class="table align-items-center table-flush">
                                             <thead class="thead-light">
                                                <tr>
                                                   <th scope="col" class="sort" data-sort="name" style="text-align:left">Business</th>
												   <th scope="col" class="sort" data-sort="name">Address</th>
                                                   <th scope="col" class="sort" data-sort="budget">Website</th>
												   <th scope="col" class="sort" data-sort="budget">Marketing</th>
												   <th scope="col" class="sort" data-sort="status">Web Transactions</th>
                                                   <th scope="col" class="sort" data-sort="status">Documents</th>
                                                   <th scope="col">Messages</th>
                                                   <!--th scope="col" class="sort" data-sort="completion">Others</th>
                                                      <th scope="col"></th-->
                                                </tr>
                                             </thead>
                                             <tbody class="list">
                                                <?php foreach($all_websites as $row): ?>
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center" >
                                                         <img  alt="Image placeholder" class="ekait ekait2c" src="<?php echo base_url(); ?>images/websites/thumb/<?= $row['image']; ?>">
                                                      </div>
                                                   </th>
												   <td class="budget1">
                                                      <p><?= $row['contact'][0]['address1']; ?></p>
													  <p><?= $row['contact'][0]['address2']; ?></p>
													  <p><?= $row['contact'][0]['city']; ?>, <?= $row['contact'][0]['state']; ?> <?= $row['contact'][0]['zip']; ?></p>
													  <p><?= $row['contact'][0]['email']; ?></p>
													  <p><?= $row['contact'][0]['phone']; ?></p>
                                                   </td>
                                                   <td class="budget">
                                                      <a href="<?= $row['test_link']; ?>" target="_blank"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/w2.png" style="width:30px;height:30px;"></a>&nbsp;
													  <a href="<?= $row['live_link']; ?>" target="_blank"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/w1.png" style="width:30px;height:30px;"></a>
                                                   </td>
												   <td class="budget">
                                                      <a href="<?= $row['test_link']; ?>" target="_blank"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/w2.png" style="width:30px;height:30px;"></a>&nbsp;
													  <a href="<?= $row['live_link']; ?>" target="_blank"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/w1.png" style="width:30px;height:30px;"></a>
                                                   </td>
												   <td class="budget">
                                                      <img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/Customer.png" style="width:30px;height:30px;"><span class="badge"><?= $row['customer_count']; ?></span>&nbsp;
													  <img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/Products.png" style="width:30px;height:30px;"><span class="badge"><?= $row['product_count']; ?></span>&nbsp;
													  <img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/Orders.png" style="width:30px;height:30px;"><span class="badge"><?= $row['order_count']; ?></span>
                                                   </td>
                                                   <td class="budget">
                                                      <a data-toggle="tooltip" title="Main Folder"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/f1.png" style="width:30px;height:30px;"></a>&nbsp;
													  <a data-toggle="tooltip" title="Reports"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/f2.png" style="width:30px;height:30px;"></a>&nbsp;
													  <a data-toggle="tooltip" title="Pending"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/f4.png" style="width:30px;height:30px;"></a>
                                                   </td>
                                                   <td class="budget">
                                                      <a href="<?= base_url('website/conversation/all_conversations/'.$row['id']); ?>" ><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/m1.png" style="width:30px;height:30px;"></a>&nbsp;
													  <a class="open-button" href="javascript:void(0)" onclick="openForm(<?= $row['id']; ?>)"><img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/m3.png" style="width:30px;height:30px;"></a>&nbsp;
													  <img  alt="Image placeholder" class="" src="<?php echo base_url(); ?>images/m2.png" style="width:30px;height:30px;">
                                                   </td>
                                                </tr>
                                                <?php endforeach; ?>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card">
                     <!-- Card header -->
                     <div class="card-header" role="tab" id="headingThree3">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                           aria-expanded="false" aria-controls="collapseThree3">
                           <h5 class="mb-0">
                              Others<i class="fa fa-angle-down rotate-icon" style="float: right;"></i>
                           </h5>
                        </a>
                     </div>
                     <!-- Card body -->
                     <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                        data-parent="#accordionEx">
                        <div class="card-body">
                           <div class="col-xl-12">
                              <div class="row">
                                 <div class="col">
                                    <div class="card">
                                       <!-- Card header -->
                                       <!--div class="card-header border-0">
                                          <h3 class="mb-0">Light table</h3>
                                          </div-->
                                       <div class="table-responsive">
                                          <table class="table align-items-center table-flush">
                                             <thead class="thead-light">
                                                <tr>
                                                   <th scope="col" class="sort" data-sort="name">Businessname</th>
                                                   <th scope="col" class="sort" data-sort="budget">Website Links</th>
                                                </tr>
                                             </thead>
                                             <tbody class="list">
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center">
                                                         <a>
                                                         <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/images/ek.jpg">
                                                         </a>
                                                         <!--div class="media-body">
                                                            <span class="name mb-0 text-sm">Argon Design System</span>
                                                            </div-->
                                                      </div>
                                                   </th>
                                                   <td class="budget">
                                                      <a href="https://ennobledemos.com/ekait/" target="_blank">Test</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.iekait.com/" target="_blank">Live</a>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center">
                                                         <a>
                                                         <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/images/ix.jpg">
                                                         </a>
                                                         <!--div class="media-body">
                                                            <span class="name mb-0 text-sm">Argon Design System</span>
                                                            </div-->
                                                      </div>
                                                   </th>
                                                   <td class="budget">
                                                      Test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.ixelit.com/" target="_blank">Live</a>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center">
                                                         <a>
                                                         <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/images/xelit.png">
                                                         </a>
                                                         <!--div class="media-body">
                                                            <span class="name mb-0 text-sm">Argon Design System</span>
                                                            </div-->
                                                      </div>
                                                   </th>
                                                   <td class="budget">
                                                      Test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.callxelit.com/" target="_blank">Live</a>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <th scope="row">
                                                      <div class="media align-items-center">
                                                         <a>
                                                         <img alt="Image placeholder" style="height:50px;" src="<?php echo base_url(); ?>assets/images/elite.png">
                                                         </a>
                                                         <!--div class="media-body">
                                                            <span class="name mb-0 text-sm">Argon Design System</span>
                                                            </div-->
                                                      </div>
                                                   </th>
                                                   <td class="budget">
                                                      <a href="https://ennobledemos.com/elite/" target="_blank"> Test</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Live
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Accordion card -->
               </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <!-- Footer --> 
         </div>
		