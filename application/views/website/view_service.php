
<style>
.card {
    margin-bottom: 1.875rem;
    background-color: #fff;
    transition: all .5s ease-in-out;
    position: relative;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: 0.25rem;
    box-shadow: 0px 0px 13px 0px rgb(82 63 105 / 5%);
    height: calc(100% - 30px);
    display: flex;
    flex-direction: column;
    min-width: 0;
    background-clip: border-box;
}

.card-body {
    padding: 1.25rem;
    flex: 1 1 auto;
}
.custom_container{
	padding:20px;
}
.pr p{
	margin-bottom:5px;
}
.product__carousel .swiper-button-next.swiper-button-white, .product__carousel .swiper-button-prev.swiper-button-white {
    color: #ffffff;
}
</style>

<div class="page-content">
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/5.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white"><?php echo $service['service_name']; ?></h2>
               </div>
            </div>                           
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Services</li>
				  <li><?php echo $service['service_name']; ?></li>
               </ul>
            </div>                       
         </div>
      </div>
   </div>
   <div class="section-full small-device">
      <div class="custom_container">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12" style="text-align:center;">
								<img src="<?php echo base_url();?>images/services/<?php echo $service['image']; ?>" style="width:50%"> 
							</div>
							<!--Tab slider End-->
							<div class="col-md-12 col-sm-12">
							<?php echo $service['service_description']; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
      </div>  
   </div>
</div>

<script>
$(document).ready(function(){

});
</script>