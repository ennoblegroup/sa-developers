<link rel="stylesheet" href="<?php echo base_url();?>assets/website/css/swiper.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/website/css/easyzoom.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/website/css/main.css" />
<style>
.card {
    margin-bottom: 1.875rem;
    background-color: #fff;
    transition: all .5s ease-in-out;
    position: relative;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: 0.25rem;
    box-shadow: 0px 0px 13px 0px rgb(82 63 105 / 5%);
    height: calc(100% - 30px);
    display: flex;
    flex-direction: column;
    min-width: 0;
    background-clip: border-box;
}

.card-body {
    padding: 1.25rem;
    flex: 1 1 auto;
}
.custom_container{
	padding:20px;
}
.pr p{
	margin-bottom:5px;
}
.product__carousel .swiper-button-next.swiper-button-white, .product__carousel .swiper-button-prev.swiper-button-white {
    color: #ffffff;
}
</style>

<div class="page-content">
   <div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/5.jpg);">
      <div class="overlay-main bg-black opacity-07"></div>
      <div class="container">
         <div class="wt-bnr-inr-entry">
            <div class="banner-title-outer">
               <div class="banner-title-name">
                  <h2 class="text-white"><?php echo $material['product_name']; ?></h2>
               </div>
            </div>                           
            <div>
               <ul class="wt-breadcrumb breadcrumb-style-2">
                  <li><a href="<?php echo base_url();?>">Home</a></li>
                  <li>Materials</li>
				  <li><?php echo $material['product_name']; ?></li>
               </ul>
            </div>                       
         </div>
      </div>
   </div>
   <div class="section-full small-device">
      <div class="custom_container">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-3">
								<div class="product__carousel">
									  <div class="gallery-parent">
										<!-- SwiperJs and EasyZoom plugins start -->
										<div class="swiper-container gallery-top">
										  <div class="swiper-wrapper">
											<?php $i=1; foreach ($material['images'] as $key=>$image){ ?>
											<div class="swiper-slide easyzoom easyzoom--overlay">
											  <a href="<?php echo base_url();?>images/products/<?php echo $image['image'];?>">
												<img src="<?php echo base_url();?>images/products/<?php echo $image['image'];?>" alt="" />
											  </a>
											</div>
											<?php $i++; }?>
										  </div>
										  <!-- Add Arrows -->
										  <div class="swiper-button-next swiper-button-white"></div>
										  <div class="swiper-button-prev swiper-button-white"></div>
										</div>
										<div class="swiper-container gallery-thumbs">
										  <div class="swiper-wrapper">
											<?php $j=1; foreach ($material['images'] as $key=>$image){ ?>
											<div class="swiper-slide">
											  <img src="<?php echo base_url();?>images/products/<?php echo $image['image'];?>" alt="" />
											</div>
											<?php $j++; }?>
										  </div>
										</div>
										<!-- SwiperJs and EasyZoom plugins end -->
									  </div>
									</div>
							</div>
							<!--Tab slider End-->
							<div class="col-md-9 col-sm-12">
								<div class="product-detail-content">
									<!--Product details-->
									<div class="new-arrival-content pr">
										<h4><?php echo $material['product_name']; ?></h4>
										<p><strong>Product code:</strong> <span class="item"><?php echo $material['product_id']; ?></span> </p>
										<p><strong>Category:</strong> <span class="item"><?php echo $material['category_name']; ?></span></p>
										<p><strong>Brand:</strong> <span class="item"><?php echo $material['brand']; ?></span></p>
										<p><strong>Product tags:</strong>&nbsp;&nbsp;
											<span class="item-tag"><?php echo $material['tags']; ?></span>
										</p><br>
										<strong>Summary:</strong>
										<p class="text-content"><?php echo $material['summary']; ?></p><br>
										<strong>Description:</strong><br>
										<?php echo $material['product_description']; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
      </div>  
   </div>
</div>

<script>
$(document).ready(function(){

});
</script>