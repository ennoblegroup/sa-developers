
<div class="page-content">
	<div class="wt-bnr-inr overlay-wraper bg-center"  style="background-image:url(<?php echo base_url();?>assets/website/images/banner/2.jpg);">
		<div class="overlay-main bg-black opacity-07"></div>
		<div class="container">
			<div class="wt-bnr-inr-entry">
				<div class="banner-title-outer">
					<div class="banner-title-name">
						<h2 class="text-white">Services</h2>
					</div>
				</div>
				<div>
					<ul class="wt-breadcrumb breadcrumb-style-2">
						<li><a href="<?php echo base_url();?>">Home</a></li>
						<li>Services</li>
					</ul>
				</div>                      
			</div>
		</div>
	</div>
	<div class="section-full p-t80 p-b50 bg-white">
		<div class="container">
			<!-- TITLE START -->
			<div class="section-head text-center">
				<div class="wt-separator-outer separator-center">
					<div class="wt-separator">
						<span class="text-primary text-uppercase sep-line-one ">Which part of your home are you looking to renovate?</span>
					</div>
				</div>
			</div>
			<div class="section-content">
				<div class="row">
				<?php $i=1; foreach ($services as $key=>$service){ ?>
					<?php if($service['status']==1){?>
					<div class="col-md-4 col-sm-6 col-xs-6 col-xs-100pc m-b30">
						<div class="hover-box-effect  v-icon-effect">
							<div class="wt-icon-box-wraper center  bg-gray" style="padding: 10px;">
								<div class="text-primary m-b25">
									<span class="icon-cell text-primary"><img src="<?php echo base_url();?>images/services/<?php echo $service['image']; ?>"></span>
								</div>
								<div class="icon-content text-black">
								  <a href="<?php echo base_url();?>website/services/view_service/<?php echo $service['id']; ?>"><h4 class="wt-tilte m-b25"><?php echo $service['service_name']; ?></h4>                                  
								</div>     
							</div>                                   
						</div>
					</div>   
					<?php $i++; }}?> 							
				</div>                                
			</div>
		</div>  
	</div> 
</div>