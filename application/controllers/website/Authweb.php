<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Authweb extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('website/authweb_model', 'authweb_model');
		}

		public function index(){
			redirect('website/home');
		}

		public function login(){

			if($this->input->post('submit')){
				$this->form_validation->set_rules('email', 'Email', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->load->view('website/index');
				}
				else {
					$data = array(
					'email' => $this->input->post('email'),
					'password' => $this->input->post('password')
					);
					$result = $this->authweb_model->login($data);
					if ($result == TRUE) {
						$website_data = array(
							'user_id' => $result['id'],
						 	'email' => $result['email'],
						 	'is_website_login' => TRUE
						);
						$_SESSION['admin_website']=$website_data;
						redirect(base_url('website'), 'refresh');
					}
					else{
						$data['msg'] = 'Invalid Email or Password!';
						$this->load->view('website/index',$data);
					}
				}
			}
			else{
				$this->load->view('website/index');
			}
		}

				
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('website/authweb/login'), 'refresh');
		}
			
	}  // end class


?>