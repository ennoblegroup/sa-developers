<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Gallery extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('admin/contactus_model', 'contactus_model');
			$this->load->model('website/gallery_model', 'gallery_model');
            $this->load->model('website/service_model', 'service_model');
			
		}
		public function index(){
			$data['menu']="gallery";
            $data['menu_services'] =  $this->service_model->get_all_services();
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			//$data['gallery'] =  $this->gallery_model->get_all_gallery();
			$data['categories'] =  $this->gallery_model->get_all_categories();
			$data['projects'] =  $this->gallery_model->get_all_projects();
			$data['view'] = 'website/gallery';
			$this->load->view('website/layout', $data);
		}
		public function view_gallery($id){
            $data['menu_services'] =  $this->service_model->get_all_services();
			$data['menu']="gallery";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['gallery'] =  $this->gallery_model->get_all_gallery_by_id($id);
			$data['view'] = 'website/view_gallery';
			$this->load->view('website/layout', $data);
		}
		function fetch_data()
         {
          $project = $this->input->post('project');
          $category = $this->input->post('category');
		  $search_text = $this->input->post('search_text');
          $this->load->library('pagination');
          $config = array();
          $config['base_url'] = '#';
          $config['total_rows'] = $this->gallery_model->product_count_all($category,$project,$search_text);
          $config['per_page'] = 8;
          $config['uri_segment'] = 4;
          $config['use_page_numbers'] = TRUE;
          $config['full_tag_open'] = '<ul class="pagination">';
          $config['full_tag_close'] = '</ul>';
          $config['first_tag_open'] = '<li>';
          $config['first_tag_close'] = '</li>';
          $config['last_tag_open'] = '<li>';
          $config['last_tag_close'] = '</li>';
          $config['next_link'] = '&gt;';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['prev_link'] = '&lt;';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['cur_tag_open'] = "<li class='active'><a href='#'>";
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['num_links'] = 3;
          $this->pagination->initialize($config);
          $page = $this->uri->segment(4);
          $start = ($page - 1) * $config['per_page'];
          $output = array(
           'pagination_link'  => $this->pagination->create_links(),
           'product_list'   => $this->gallery_model->fetch_data($config["per_page"], $start, $category, $project,$search_text)
          );
          echo json_encode($output, true);
            
         }

	}

?>	