<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Services extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('ftp');
			$this->load->model('website/authweb_model', 'authweb_model');
			$this->load->model('website/conversationweb_model', 'convweb_model');
			$this->load->model('website/contactus_model', 'contactus_model');
			$this->load->model('website/service_model', 'service_model');
			
		}
		public function index(){
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['menu']="services";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['services'] =  $this->service_model->get_all_active_services();

			$data['view'] = 'website/services';
			$this->load->view('website/layout', $data);
		}
		public function view_service($id){
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['menu']="View Service";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['service'] =  $this->service_model->get_service_by_id($id);
			
			$data['view'] = 'website/view_service';
			$this->load->view('website/layout', $data);
		}

	}

?>	