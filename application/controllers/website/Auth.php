<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->config->load('messages_list', TRUE);
			$this->log_data = array(
				'messages_list' => $this->config->item('messages_list')
			);
			$this->load->model('website/auth_model', 'auth_model');
			$this->load->model('admin/settings_model', 'settings_model');
		}

		public function index(){
			if($_SESSION['sadevelopers_admin']['is_admin_login']==1)
			{
				redirect('admin/dashboard');
			}
			else{
				redirect('admin/auth/login');
			}
		}

		public function login(){

			if($this->input->post('submit')){
				$this->form_validation->set_rules('email', 'Email', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'website/login';
					$data['menu']="login";
					$this->load->view('website/layout', $data);
				}
				else {
					$email=$this->input->post('email');
					$password=$this->input->post('password');
					$data = array(
					'email' => $email,
					'password' => $password
					);
					$result = $this->auth_model->login($data);
					if($result['client_id']>0){
						//$presult = $this->auth_model->get_client($result['client_id']);
					}
					if ($result) {
						if($result['status_id']==0){
							echo $this->log_data['messages_list']['adminlogin_inactiveuser'];
							
						}elseif($result['rstatus_id']==0){
							echo $this->log_data['messages_list']['adminlogin_inactiverole'];
							
						}else{	
							$admin_data = $result;
							$array_going_second = array(
								'admin_id' => $result['u_id'],
								'role_id' => $result['user_role'],
								'name' => $result['lastname'].' '.$result['firstname'],
								'is_admin_login' => TRUE
								);
							$admin_data = array_merge($admin_data, $array_going_second);
							if($result['client_id']>0){
								//$admin_data = array_merge($admin_data, $presult);
							}
							//print_r($admin_data);die;
							$admin_settings = $this->settings_model->settings();
							$_SESSION['sadevelopers_admin']=$admin_data;
							$_SESSION['sadevelopers_admin_settings']=$admin_settings;
							if(!empty($_POST["remember"])) {
								setcookie ("admin_email",$email,time()+ (10 * 365 * 24 * 60 * 60));
								//setcookie ("admin_password",$password,time()+ (10 * 365 * 24 * 60 * 60));
							} else {
								if(isset($_COOKIE["admin_email"])) {
									setcookie ("admin_email","");
								}
								/*if(isset($_COOKIE["admin_password"])) {
									setcookie ("admin_password","");
								}*/
							}
							echo '1';	
						}
					}
					else{						
						//$this->session->set_flashdata('danger_msg', 'Invalid Email Id/Password!');
						echo $this->log_data['messages_list']['adminlogin_wrong_usename/password'];
						
					}
				}
			}
			else{
				$data['view'] = 'website/home';
				$data['menu']="login";
				$this->load->view('website/layout', $data);
			}
		}	

		public function change_pwd(){
			$id = $_SESSION['sadevelopers_admin']['admin_id'];
			if($this->input->post('submit')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|matches[password]');
				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'admin/auth/change_pwd';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
					);
					$result = $this->auth_model->change_pwd($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Password has been changed successfully!');
						redirect(base_url('admin/auth/change_pwd'));
					}
				}
			}
			else{
				$data['view'] = 'admin/auth/change_pwd';
				$this->load->view('admin/layout', $data);
			}
		}
		public function update_password(){
			$id = $_SESSION['sadevelopers_admin']['admin_id'];
			$email=$this->input->post('email');
			$resetid=$this->input->post('resetid');
			$data = array(
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
			);
			$result = $this->auth_model->update_password($data, $email,$resetid);
			if($result){
				$this->session->set_flashdata('success_msg', 'Password has been reset successfully!');
				redirect(base_url('admin/auth/login'));
			}
		}
		public function reset_password(){
			if($this->input->post('submit')){
				$data = array(
					'email' => $this->input->post('email')
				);
				$result = $this->auth_model->check_useremail($data);

				if($result){
					$result2 = $this->auth_model->reset_link_email($data);
					if($result2){
						$this->session->set_flashdata('success_msg', 'Reset Password link has been sent to your Email Address.');
						redirect(base_url('admin/auth/login'));
					}else{
						$this->session->set_flashdata('error', "Server Error");
						$this->load->view('admin/auth/forgot_password', $data);
					}					
				}else{
					$this->session->set_flashdata('error', $this->log_data['messages_list']['forgotpassword_invalid_email']);
					$this->load->view('admin/auth/forgot_password', $data);
				}
			}
			else{
				//$data['view'] = 'admin/auth/forgot_password';
				$this->load->view('admin/auth/forgot_password');
			}
		}
		public function new_password($data){
			
			$this->load->view('admin/auth/new_password',$data);
		}
		public function verify_resetpassword_link(){
			
			$user_email = $this->input->get('user_email');
			$token = $this->input->get('token');
			$data=$this->auth_model->verify_resetpassword_link($user_email,$token);

			if($data)
			{
				$this->new_password($data);
				//$this->load->view('admin/auth/new_password', $data);
			}
			else
			{
				$this->session->set_flashdata('error','Link Expired');
				$this->load->view('admin/auth/forgot_password', $data);
			}
		}	
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('website'), 'refresh');
		}
			
	}  // end class


?>