<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboardweb extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('ftp');
			$this->load->model('website/authweb_model', 'authweb_model');
			$this->load->model('website/conversationweb_model', 'convweb_model');
			$this->load->model('admin/aboutus_model', 'aboutus_model');
			$this->load->model('admin/contactus_model', 'contactus_model');
			$this->load->model('admin/features_model', 'features_model');
			$this->load->model('admin/testimonials_model', 'testimonials_model');
			$this->load->model('admin/homeservices_model', 'homeservices_model');
			$this->load->model('admin/slides_model', 'slides_model');
			$this->load->model('admin/homeaboutus_model', 'homeaboutus_model');
			
		}
		public function index(){
			$data['aboutus'] =  $this->aboutus_model->get_all_active_aboutus();
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['features'] =  $this->features_model->get_all_features();	
			$data['testimonials'] =  $this->testimonials_model->get_all_testimonials();	
			$data['homeservices'] =  $this->homeservices_model->get_all_homeservices();
			$data['slides'] =  $this->slides_model->get_all_slides();
			$data['homeaboutus'] =  $this->homeaboutus_model->get_all_homeaboutus();

			$data['view'] = 'website/home';
			$this->load->view('website/layout', $data);
		}

	}

?>	