<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class About extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('website/Aboutus_model', 'aboutus_model');
			$this->load->model('website/contactus_model', 'contactus_model');
			$this->load->model('website/team_model', 'team_model');
			$this->load->model('website/service_model', 'service_model');
			
		}
		public function index(){
			$data['menu']="about";
			$data['aboutus'] =  $this->aboutus_model->get_all_active_aboutus();
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['team'] =  $this->team_model->get_all_team();
			$data['view'] = 'website/about';
			$this->load->view('website/layout', $data);
		}

	}

?>	