<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Conversation extends MY_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('website/authweb_model', 'authweb_model');
			$this->load->model('website/conversationweb_model', 'convweb_model');
		}

		public function index($id){
			$data['conversation'] = $this->convweb_model->get_conversation_by_id($id);
			$this->load->view('website/conversation');
		}
		public function add_conversation(){
			$data = array(
				'user_id' => $this->session->userdata('admin_id'),
				'website_id' => $this->input->post('website_id'),
				'message' => $this->input->post('msg'),
				'created_at' => date('Y-m-d : h:m:s'),
				'updated_at' => date('Y-m-d : h:m:s'),
			);
			$data = $this->security->xss_clean($data);
			$result = $this->convweb_model->add_conversation($data);
			if($result){
				$this->session->set_flashdata('msg', 'Record is Added Successfully!');
				redirect(base_url('website'), 'refresh');
			}
			
		}
		public function all_conversations($id){
			$user_id = $_SESSION['ekait_dashboard_website']['user_id'];
			$data['conversation_notification'] = $this->convweb_model->get_conversation_notification($user_id);
			$data['conversations'] = $this->convweb_model->get_conversation_by_id($id);
			$data['view'] = 'website/conversation';
			$this->load->view('website/layout', $data);
		}

	}

?>	