<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Contactus extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('website/contactus_model', 'contactus_model');
			$this->load->model('website/service_model', 'service_model');
			
		}
		public function index(){
			$data['menu']="contactus";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['view'] = 'website/contactus';
			$this->load->view('website/layout', $data);
		}

	}

?>	