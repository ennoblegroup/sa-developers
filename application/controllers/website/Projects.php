<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Projects extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('admin/contactus_model', 'contactus_model');
			$this->load->model('website/project_model', 'project_model');
			$this->load->model('website/service_model', 'service_model');
			
		}
		public function index(){
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['menu']="projects";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['projects'] =  $this->project_model->get_all_projects();
			$data['view'] = 'website/projects';
			$this->load->view('website/layout', $data);
		}
		public function view_project($id){
			$data['menu_services'] =  $this->service_model->get_all_services();
			$data['menu']="projects";
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['project'] =  $this->project_model->get_all_project_by_id($id);
			//print_r($data['project']);die;
			$data['view'] = 'website/projectdetail';
			$this->load->view('website/layout', $data);
		}
		public function get_all_after_images(){
			$id=$this->input->post('id');
			$after_images =  $this->project_model->get_all_after_images($id);
			echo json_encode($after_images);
		}
		

	}

?>	