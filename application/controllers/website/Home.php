<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Home extends WEB_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->library('ftp');
			$this->load->model('website/authweb_model', 'authweb_model');
			$this->load->model('website/conversationweb_model', 'convweb_model');
			$this->load->model('website/aboutus_model', 'aboutus_model');
			$this->load->model('website/contactus_model', 'contactus_model');
			$this->load->model('website/testimonials_model', 'testimonials_model');
			$this->load->model('website/service_model', 'service_model');
			$this->load->model('website/team_model', 'team_model');
			$this->load->model('website/vendor_model', 'vendor_model');
			$this->load->model('website/project_model', 'project_model');
			$this->load->model('website/messages_model', 'messages_model');
			$this->load->model('website/slides_model', 'slides_model');
			
		}
		public function index(){
			$data['menu']="index";
			$data['aboutus'] =  $this->aboutus_model->get_all_active_aboutus();
			$data['contactus'] =  $this->contactus_model->get_all_contactus();
			$data['menu_services'] =  $this->service_model->get_all_services();
            $data['services'] =  $this->service_model->get_all_active_services();			
			$data['testimonials'] =  $this->testimonials_model->get_all_testimonials();	
			$data['slides'] =  $this->slides_model->get_all_active_slides();
			$data['homeaboutus'] =  $this->aboutus_model->get_all_active_aboutus();
            $data['vendors'] =  $this->vendor_model->get_all_vendors();
            $data['reviews'] =  $this->messages_model->get_all_reviews();
			$data['projects'] =  $this->project_model->get_all_projects();
			$data['team'] =  $this->team_model->get_all_team();
			$data['reviews'] =  $this->messages_model->get_all_reviews();
			  
			 

			$data['view'] = 'website/home';
			$this->load->view('website/layout', $data);
		}

	}

?>	