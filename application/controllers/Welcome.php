<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		redirect(base_url('website'), 'refresh');
	}
	public function contact()
    {
		$this->load->model('Contact_model', 'Contact_model');
        $query = $this->Contact_model->contact_email();
        if($query){
			$this->session->set_flashdata('success_msg', 'Thank You for Contacting Us');
		
			redirect(base_url('website/contactus'));
		}
	}
	
	public function contact1()
    {
		$this->load->model('Contact_model', 'Contact_model');
        $query = $this->Contact_model->contact_email1();
		if($query){
			echo '1';
		}
	}
	
	public function enquiry()
    {
		$this->load->model('Contact_model', 'Contact_model');
        $query = $this->Contact_model->enquiry();
        return $query;
    }
}
