<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class clients extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/client_model', 'client_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/service_model', 'service_model');
		}

		public function index(){
			$data['all_clients'] =  $this->client_model->get_all_clients();
			$data['view'] = 'admin/clients/view_clients';
			$this->load->view('admin/layout', $data);
		}
		public function view_client($id){
			$data['client'] = $this->client_model->get_client_by_id($id);
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/clients/view_client';
			$this->load->view('admin/layout', $data);
		}
		public function client_conversation(){
			$data['view'] = 'admin/clients/client_conversation';
			$this->load->view('admin/layout', $data);
		}		
		public function client_services(){
			if($this->input->post('submit')){
					
					$service_id = $this->input->post('service');
					$client_id = $this->input->post('client');					
					$daterange = $this->input->post('daterange');
					$daterange = explode(" - ",$this->input->post('daterange'));
					$start_date = $daterange[0];
					$end_date = $daterange[1];
					$result = $this->client_model->select_client_service($service_id,$client_id,$start_date,$end_date);
					if($result['cnt']==0)
					{
						$data = array(
							'service_id' => $service_id,
							'client_id' => $client_id,
							'start_date' => $start_date,
							'end_date' => $end_date,
							'price' => $this->input->post('price'),
							'created_date' => date('Y-m-d : h:m:s'),
						);
						$data = $this->security->xss_clean($data);
						$result = $this->client_model->add_client_service($data);
						if($result){
							//$this->session->set_flashdata('message', 'success');
							$data['all_clients'] =  $this->client_model->get_all_active_clients();
							$data['all_services'] =  $this->service_model->get_all_active_services();
							$data['client_services'] =  $this->client_model->get_all_client_services();
							$this->session->set_flashdata('success_msg', 'client & Service Maped Successfully!');
							$data['view'] = 'admin/clients/map_service_clients';
							$this->load->view('admin/layout', $data);
						}	
					}	
					else
					{
						$data['all_clients'] =  $this->client_model->get_all_active_clients();
						$data['all_services'] =  $this->service_model->get_all_active_services();
						$data['client_services'] =  $this->client_model->get_all_client_services();
						$this->session->set_flashdata('danger_msg', 'client & Service are already Maped!');
						$data['view'] = 'admin/clients/map_service_clients';
						$this->load->view('admin/layout', $data);
						
					}
			}
			else{
				$data['all_clients'] =  $this->client_model->get_all_active_clients();
				$data['all_services'] =  $this->service_model->get_all_active_services();
				$data['client_services'] =  $this->client_model->get_all_client_services();
				$data['view'] = 'admin/clients/map_service_clients';
				$this->load->view('admin/layout', $data);
			}
			
		}
		function randomPassword($length,$count, $characters) {

			// $length - the length of the generated password
			// $count - number of passwords to be generated
			// $characters - types of characters to be used in the password
			 
			// define variables used within the function    
				$symbols = array();
				$passwords = array();
				$used_symbols = '';
				$pass = '';
			 
			// an array of different character types    
				$symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
				$symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$symbols["numbers"] = '1234567890';
				$symbols["special_symbols"] = '!?~@#-_';
			 
				$characters = explode(",",$characters); // get characters types to be used for the passsword
				foreach ($characters as $key=>$value) {
					$used_symbols .= $symbols[$value]; // build a string with all characters
				}
				$symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
				 
				for ($p = 0; $p < $count; $p++) {
					$pass = '';
					for ($i = 0; $i < $length; $i++) {
						$n = rand(0, $symbols_length); // get a random character from the string with all characters
						$pass .= $used_symbols[$n]; // add the character to the password string
					}
					$passwords[] = $pass;
				}
				 
				return $passwords[0];// return the generated password
		}
		public function add(){
			if($this->input->post('submit')){					
				
				$config['upload_path'] = 'images/clients/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/clients/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 160;
				$config['height'] = 30;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$notes= $this->input->post('notes');
				$source= $this->input->post('source');
				$last_name=$this->input->post('last_name');
				$first_name=$this->input->post('first_name');
				$email=$this->input->post('email');
				$user_password=$this->randomPassword(8,1,"lower_case,upper_case,numbers,special_symbols");
				//$_SESSION['sadevelopers_admin']['Role_id']; 
				$ulength = 7;
				$ucharacters = '0123456789';
				$urandom_id = "";
				for ($i = 0; $ulength > $i; $i++) {
				$urandom_id .= $ucharacters[mt_rand(0, strlen($ucharacters) -1)];
				}
				$data = array(	
					'userid' => 'USR'.$urandom_id,
					'client_id' => $urandom_id,						
					'lastname' => $last_name,
					'firstname' => $first_name,
					'middlename	' => $this->input->post('middle_name'),
					'mobile_no' => $this->input->post('phone_number'),						
					'email' => $email,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'image' => $fname,
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'is_admin' => 1,
					'notes' => $notes,
					'user_role' => 0,
					'source' => $source,
					'user_type' => 1,
					'password' => password_hash($user_password, PASSWORD_BCRYPT),
					'status' =>1,
					'created_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->client_model->add_client_users($data);
				if($result){
					$email_subject ='Client Registration';
					$content="You have been successfully registered with S&A Developers. Please find the details below to login to the Client Portal.";			
					$data['name'] =$first_name." ". $last_name;
					$data['email_subject'] = $email_subject;
					$data['login_url'] = site_url()."admin";
					$data['uname'] = $email;
					$data['password'] = $user_password;
					$data['content'] = $content;
					$data['client_name'] = 'S&A Developers';
					$temp =$this->load->view('email/common_email',$data,TRUE);
					$msg="$temp";
					$this->email->from('admin@sadevelopers.com');
					$this->email->to($email);
					$subject = 'Client Registration';
					$this->email->subject($subject);
					$this->email->set_mailtype('html');
					$this->email->message($msg);
					$this->email->send();
					$this->session->set_flashdata('success_msg', 'Client Added Successfully!');
					redirect(base_url('admin/clients'));
					
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				
				$data['view'] = 'admin/clients/add_clients';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id	= $this->input->post('uid');
				$name = $_FILES['image']['name'];
				$sql = "select * from users where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{	
					$path = "images/clients/" . $row['image'];
					$path1 = "images/clients/thumb" . $row['image'];

					$config['upload_path'] = 'images/clients/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/clients/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '160';
					$config['height'] = '200';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}
				$uid	= $this->input->post('uid');
				$notes= $this->input->post('notes');
				$source= $this->input->post('source');
				$udata = array(	
					'firstname' => $this->input->post('first_name'),
					'lastname' => $this->input->post('last_name'),
					'middlename' => $this->input->post('middle_name'),
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'image' => $fileName,
					'email' => $this->input->post('email'),
					'mobile_no' => $this->input->post('phone_number'),
					'notes' => $this->input->post('notes'),
					'source' => $this->input->post('source'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($udata);
				$result = $this->user_model->edit_user($udata, $uid);
				if($result){
					$this->session->set_flashdata('success_msg', 'client Updated Successfully!');
					redirect(base_url('admin/clients'));
				}
			}
			else{
				$data['client'] = $this->client_model->get_client_by_id($id);
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/clients/edit_clients';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('clients', array('id' => $id));
			$this->db->delete('users', array('client_id' => $id));
			$this->session->set_flashdata('danger_msg', 'client Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$client_id = $_POST['client_id'];
			$id         = $_POST['id'];
			return $this->client_model->update_status($client_id, $id);
		}
		public function delps(){
			$id= $this->input->post('id');
			$this->db->delete('clients_services', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'client Service Deleted Successfully!');
			echo 'success';
		}
		public function fetch_uname()
		{
			$uname = $this->input->post('user_name');
			$result   = $this->client_model->get_user_by_name($uname);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
			  
		}
		public function fetch_email()
		{
			$email = $this->input->post('business_email');
			$result   = $this->client_model->get_client_by_email($email);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function fetch_npi()
		{
			$npi_number = $this->input->post('npi_number');
			$result   = $this->client_model->get_client_by_npi($npi_number);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function filter_clients()
		{
			//$data['all_clients'] =  $this->client_model->get_all_clients();
			$rrr=$this->client_model->get_all_clients1();
			$output = array('clients_list'   => $rrr);
			echo json_encode($output, true);
		}
	}


?>