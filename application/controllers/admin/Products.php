<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Products extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/product_model', 'product_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('state_model', 'state_model');
		}
		
		public function index(){
			$data['all_products'] =  $this->product_model->get_all_products();
			$data['view'] = 'admin/products/all_products';
			$this->load->view('admin/layout', $data);
		}
		public function view_product($id){
			$data['product']=$this->product_model->get_product_by_id($id);
			$data['view'] = 'admin/products/view_product';
			$this->load->view('admin/layout', $data);
		}
		public function get_product_by_id(){
			$id =$this->input->post('id');
			$rrr= $this->product_model->get_product_by_id($id);
			echo json_encode($rrr);	

		}
		public function add(){
			if($this->input->post('submit')){
				$product_id =$this->input->post('product_id');
				$vendor_id =$this->input->post('vendor_id');
				$category_id =$this->input->post('category_id');
				$product_name =$this->input->post('product_name');
				$product_summary = $this->input->post('product_summary');
				$product_description = $this->input->post('product_description');
				$price =$this->input->post('product_price');
				$units =$this->input->post('units');
				$upc =$this->input->post('upc');
				$vpc =$this->input->post('vpc');
				$brand =$this->input->post('product_brand');
				$tags = $this->input->post('product_tags');
				//$file_source = $this->input->post('file_source');
				$data = array(
					//'file_source' => $file_source,
					'product_id' => $product_id,
					'category_id' => $category_id,
					'product_name' => $product_name,
					'summary' => $product_summary,
					'product_description' => $product_description,
					'price' => $price,
					'units' => $units,
					'upc' => $upc,
					'vpc' => $vpc,
					'brand' => $brand,
					'tags' => $tags,
					'vendor_id' => $vendor_id,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->product_model->add_product($data);
				if($result){
					$count = count($_POST['rrrr']);
					for($k=0; $k<$count; $k++)
					{
						$img = $_POST['rrrr'][$k];
						$folderPath = "images/products/";
					  
						$image_parts = explode(";base64,", $img);
						$image_type_aux = explode("image/", $image_parts[0]);
						$image_type = $image_type_aux[1];
					  
						$image_base64 = base64_decode($image_parts[1]);
						$fileName = uniqid() . '.png';
					  
						$file = $folderPath . $fileName;
						file_put_contents($file, $image_base64);
						$fname=$fileName;
						$data2['image'] = $fname;
						$data2['product_id'] = $result;
						$data2['is_primary'] = $_POST['is_primary'][$k];
						$this->db->insert('product_images',$data2);
					}
					$this->session->set_flashdata('success_msg', 'Product Added Successfully!');
					redirect(base_url('admin/products'));
				}
			}
			else{
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_vendors'] =  $this->vendor_model->get_all_active_vendors();
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data['product_id'] = 'PRD'.$random_id;
				$data['view'] = 'admin/products/add_product';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$mid = $this->input->post('ppid');
				
				$product_id =$this->input->post('product_id');
				$vendor_id =$this->input->post('vendor_id');
				$category_id =$this->input->post('category_id');
				$product_name =$this->input->post('product_name');
				$product_summary = $this->input->post('product_summary');
				$product_description = $this->input->post('product_description');
				$price =$this->input->post('product_price');
				$units =$this->input->post('units');
				$upc =$this->input->post('upc');
				$vpc =$this->input->post('vpc');
				$brand =$this->input->post('product_brand');
				$tags = $this->input->post('product_tags');
				//$file_source = $this->input->post('file_source');
				$data = array(
					//'file_source' => $file_source,
					'product_id' => $product_id,
					'category_id' => $category_id,
					'product_name' => $product_name,
					'summary' => $product_summary,
					'product_description' => $product_description,
					'price' => $price,
					'units' => $units,
					'upc' => $upc,
					'vpc' => $vpc,
					'brand' => $brand,
					'tags' => $tags,
					'vendor_id' => $vendor_id,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->product_model->edit_product($data, $mid);
				if($result){
					$count = count($_POST['rrrr']);
					for($k=0; $k<$count; $k++)
					{
						if($_POST['rrrr'][$k] !=''){
							$img = $_POST['rrrr'][$k];
							$folderPath = "images/products/";			  
							$image_parts = explode(";base64,", $img);				
							$image_type_aux = explode("image/", $image_parts[0]);
							$image_type = $image_type_aux[1];			    
							$image_base64 = base64_decode($image_parts[1]);				
							$fileName = uniqid() . '.png';		  
							$file = $folderPath . $fileName;
							file_put_contents($file, $image_base64);
							$fname=$fileName;
							$data2['image'] = $fname;
							$data2['product_id'] = $mid;
							$data2['is_primary'] = $_POST['is_primary'][$k];
							$this->db->insert('product_images',$data2);
							
						}
					}
					$this->session->set_flashdata('success_msg', 'Product Updated Successfully!');
					redirect(base_url('admin/products'));
				}
			}
			else{
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_vendors'] =  $this->vendor_model->get_all_active_vendors();
				$data['product'] =  $this->product_model->get_product_by_id($id);
				$data['view'] = 'admin/products/edit_product';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('products', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Product Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$product_id = $_POST['product_id'];
			$id         = $_POST['id'];
			return $this->product_model->update_status($product_id, $id);
		}
		public function delete_product_image(){
			$id= $this->input->post('image_id');
			$this->db->delete('product_images', array('image_id' => $id));
			$this->session->set_flashdata('danger_msg', 'Product Image Deleted Successfully!');
			echo 'success';
		}
	}


?>