<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Services extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/service_model', 'service_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/category_model', 'category_model');
		}

		public function index(){
			$data['all_services'] =  $this->service_model->get_all_services();
			$data['view'] = 'admin/services/all_services';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){
				
				$config['upload_path'] = 'images/services/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/services/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 100;
				$config['height'] = 100;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$category_id=$this->input->post('category_id');
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data = array(
					'service_id' => 'SER'.$random_id,
					'service_name' => $this->input->post('service_name'),
					'service_description' => $this->input->post('service_description'),
					'image' => $fname,
					'status' =>1,
					'created_date' => date('Y-m-d : h:m:s'),
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->service_model->add_service($data);
				if($result){
					for($i=0;$i<count($category_id);$i++){
						$cdata = array(
							'service_id' => $result,
							'category_id' => $category_id[$i],
							'status' =>1,
						);
						if($category_id[$i] !=''){
							$data = $this->security->xss_clean($cdata);
							$resultt = $this->service_model->add_service_category($cdata);
						}
					}
					$this->session->set_flashdata('success_msg', 'Service Added Successfully!');
					redirect(base_url('admin/services'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['view'] = 'admin/services/add_service';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$name = $_FILES['image']['name'];
				
				$sql = "select * from services where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{	
					$path = "images/services/" . $row['image'];
					$path1 = "images/services/thumb" . $row['image'];

					$config['upload_path'] = 'images/services/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/services/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '100';
					$config['height'] = '100';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}
				$category_id=$this->input->post('category_id');
				$data = array(
					'service_name' => $this->input->post('service_name'),
					'service_description' => $this->input->post('service_description'),
					'image' => $fileName,
				);					
				$data = $this->security->xss_clean($data);
				$result = $this->service_model->edit_service($data, $id);
				
				if($result && $category_id !=''){
					$tags = implode(', ', $category_id);
					for($i=0;$i<count($category_id);$i++){
						$query=$this->db->query("SELECT * FROM service_categories where service_id=$id and category_id=$category_id[$i]");
						$cnt   = $query->num_rows();
						if($cnt==0){
							$vdata = array(
								'service_id' => $id,
								'category_id' => $category_id[$i],
								'status' =>1,
							);
							if($category_id[$i] !=''){
								$vdata = $this->security->xss_clean($vdata);
								$resultt = $this->service_model->add_service_category($vdata);
							}
						}
					}
					$query=$this->db->query("DELETE  FROM service_categories where service_id=$id and category_id NOT IN (".$tags.")");
				}
				$this->session->set_flashdata('success_msg', 'Service Updated Successfully!');
				redirect(base_url('admin/services'));
			}
			else{
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['service'] = $this->service_model->get_service_by_id($id);				
				$data['view'] = 'admin/services/edit_service';
				$this->load->view('admin/layout', $data);
			}
		}
		public function get_service_category(){
			$id =$this->input->post('id');
			$rrr= $this->service_model->get_service_category($id);
			echo json_encode($rrr);
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('services', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Service is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$service_id = $_POST['service_id'];
			$id         = $_POST['id'];
			return $this->service_model->update_status($service_id, $id);
		}
	}


?>