<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Homeaboutus extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/homeaboutus_model', 'homeaboutus_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_homeaboutus'] =  $this->homeaboutus_model->get_all_homeaboutus();
			$data['view'] = 'admin/homeaboutus/all_homeaboutus';
			$this->load->view('admin/layout', $data);
		}
		
		
		public function add(){
			if($this->input->post('submit')){
				/*$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required');
				$this->form_validation->set_rules('service_description', 'Service Description', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'admin/services/add_service';
					$this->load->view('admin/layout', $data);
				}
				else{*/
					$length = 7;
					$characters = '0123456789';
					$random_id = "";
					for ($i = 0; $length > $i; $i++) {
					$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
					}
					$data = array(
						//'service_id' => 'SER'.$random_id,
						'homeaboutus_name' => $this->input->post('homeaboutus_name'),
						'description' => $this->input->post('description'),
						'status' =>1,
						'created_date' => date('Y-m-d : h:m:s'),
						'updated_date' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->homeaboutus_model->add_homeaboutus($data);
					if($result){
						$this->session->set_flashdata('success_msg', 'Service Added Successfully!');
						redirect(base_url('admin/homeaboutus'));
					}
				//}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/homeaboutus/add_homeaboutus';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				/*$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required');
				$this->form_validation->set_rules('service_description', 'Service Description', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['service'] = $this->service_model->get_service_by_id($id);
					$data['view'] = 'admin/services/edit_service';
					$this->load->view('admin/layout', $data);
				}
				else{*/
					$data = array(
						'title' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						//'updated_at' => date('Y-m-d : h:m:s'),
					);					
					$data = $this->security->xss_clean($data);
					$result = $this->homeaboutus_model->edit_homeaboutus($data, $id);
					if($result){
						$this->session->set_flashdata('success_msg', 'homeaboutus Updated Successfully!');
						redirect(base_url('admin/homeaboutus'));
					}
				//}
			}
			else{
				$data['homeaboutus'] = $this->homeaboutus_model->get_homeaboutus_by_id($id);				
				$data['view'] = 'admin/homeaboutus/edit_homeaboutus';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('homeaboutus', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'About Us is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$homeaboutus_id = $_POST['homeaboutus_id'];
			$id         = $_POST['id'];
			return $this->homeaboutus_model->update_status($homeaboutus_id, $id);
		}
	}


?>