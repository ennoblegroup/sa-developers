<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Settings extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/settings_model', 'settings_model');
		}

		public function update_bgsettings(){
            $layout_type=$_POST['layout_type'];
            $color=$_POST['color'];
            $bg_setings =  $this->settings_model->update_bgsettings($layout_type,$color);
            $_SESSION['ekait_admin_settings'][$layout_type]= $color;
            echo true;
		}
		

	}


?>