<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Homeservices extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/homeservices_model', 'homeservices_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_homeservices'] =  $this->homeservices_model->get_all_active_homeservices();
			$data['view'] = 'admin/homeservices/all_homeservices';
			$this->load->view('admin/layout', $data);
		}
		
		public function view_service($id){
			$data['service_view'] =  $this->homeservices_model->get_homeservices_by_id($id);		
			$data['view'] = 'admin/homeservices/view_service';
			$this->load->view('admin/layout', $data);
		}

		public function add(){
			if($this->input->post('submit')){				
				$title = $_POST['title'];
				$description = $_POST['description'];	

				$config['upload_path'] = 'images/homeservices/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/homeservices/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 400;
				$config['height'] = 100;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$length = 6;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data = array(
					'title' =>  $title,
					'description' => $description,
					'image' => $fname,
					'status' =>1,
					'create_date' => date('Y-m-d : h:m:s'),
					'update_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->homeservices_model->add_homeservices($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Homeservice Added Successfully!');
					redirect(base_url('admin/homeservices'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/homeservices/add_homeservice';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$name = $_FILES['image']['name'];
				$homeserviceid = $this->input->post('homeserviceid');
				$sql = "select * from homeservices where id = '$homeserviceid'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{	
					$path = "images/homeservices/" . $row['image'];
					$path1 = "images/homeservices/thumb" . $row['image'];

					$config['upload_path'] = 'images/homeservices/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/homeservices/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '400';
					$config['height'] = '100';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}

				$title = $_POST['title'];
				$description = $_POST['description'];

				$data = array(
				'title' => $title,
				'description' => $description,
				'image' => $fileName,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->homeservices_model->edit_homeservices($data, $homeserviceid);
				if($result){
				$this->session->set_flashdata('success_msg', 'Homeservice Updated Successfully!');
				redirect(base_url('admin/homeservices'));
				}
			}
			else{
				$data['homeservices'] = $this->homeservices_model->get_homeservices_by_id($id);				
				$data['view'] = 'admin/homeservices/edit_homeservices';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('homeservices', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Service Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$homeservices_id = $_POST['homeservices_id'];
			$id         = $_POST['id'];
			return $this->homeservices_model->update_status($homeservices_id, $id);
		}
	}


?>