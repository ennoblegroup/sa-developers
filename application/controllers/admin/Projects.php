<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class projects extends MY_Controller {

		public function __construct(){
			parent::__construct();
			date_default_timezone_set('America/New_York');
			$this->load->model('admin/project_model', 'project_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/client_model', 'client_model');
			$this->load->model('admin/product_model', 'product_model');
			$this->load->model('admin/service_model', 'service_model');
			$this->load->model('admin/document_types_model', 'document_types_model');
			$this->load->model('admin/gallery_types_model', 'gallery_types_model');
			$this->load->model('admin/documents_model', 'document_model');
			$this->load->model('admin/gallery_model', 'gallery_model');
		}

		public function index(){
			$data['client_id'] = $this->input->get('id');
			$data['project_status'] = $this->project_model->get_project_status();
			$data['view'] = 'admin/projects/view_projects';
			$this->load->view('admin/layout', $data);
		}
		public function view_project($id){
			$data['project_id'] = $id;
			$data['project'] = $this->project_model->get_project_by_id($id);
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/projects/view_project';
			$this->load->view('admin/layout', $data);
		}
		public function documents($id){
			$data['project_id']=$id;
			$data['documents'] =  $this->project_model->get_all_documents_by_id($id);
			$data['all_document_types'] =  $this->document_types_model->get_all_active_document_types();
			$data['view'] = 'admin/projects/project_documents';
			$this->load->view('admin/layout', $data);
		}
		public function gallery($id){
			$data['project_id']=$id;
			$data['all_gallery_types'] =  $this->gallery_types_model->get_all_gallery_types();
			$data['all_gallery'] =  $this->project_model->get_all_gallery($id);
			$data['all_local_gallery'] =  $this->gallery_model->get_all_gallery();
			$data['view'] = 'admin/projects/project_gallery';
			$this->load->view('admin/layout', $data);
		}
		public function map_images($id){
			$data['project_id']=$id;
			$data['all_gallery'] =  $this->project_model->get_all_mapping_images($id);
			$data['view'] = 'admin/projects/map_project_images';
			$this->load->view('admin/layout', $data);
		}
		public function mapped_images($id){
			$data['project_id']=$id;
			$before_images= $this->db->query("SELECT * FROM project_mapped_images pmi,gallery g where pmi.simage_id=g.id and pmi.project_id=$id and pmi.gallery_type=1")->result_array();
			$before_images = array_reverse(array_values( array_combine( array_column($before_images, 'simage_id'), $before_images)));
			
			foreach($before_images as $key=>$each){
				$bid= $each['simage_id'];
				$dataa[$key]['before_image']= $each['file'];
				$dataa[$key]['after_images'] = $this->db->query("SELECT *,pmi.id as id FROM project_mapped_images pmi,gallery g where pmi.mimage_id=g.id and pmi.simage_id=$bid and pmi.gallery_type=1")->result_array();
			}
			$data['mapped_images']=$dataa;

			$after_images= $this->db->query("SELECT * FROM project_mapped_images pmi,gallery g where pmi.simage_id=g.id and pmi.project_id=$id and pmi.gallery_type=2")->result_array();
			$after_images = array_reverse(array_values( array_combine( array_column($after_images, 'simage_id'), $after_images)));
			
			foreach($after_images as $key=>$each){
				$bid= $each['simage_id'];
				$dataaa[$key]['after_image']= $each['file'];
				$dataaa[$key]['before_images'] = $this->db->query("SELECT *,pmi.id as id FROM project_mapped_images pmi,gallery g where pmi.mimage_id=g.id and pmi.simage_id=$bid and pmi.gallery_type=2")->result_array();
			}
			$data['mapped_images']=$dataa;
			$data['mapped_images2']=$dataaa;

			$data['view'] = 'admin/projects/mapped_project_images';
			$this->load->view('admin/layout', $data);
		}
		public function link_project_products($id){
			$data['project_id']=$id;
			
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/projects/project_products';
			$this->load->view('admin/layout', $data);
		}
		public function view_project_products(){
			$idd = $this->input->get('project_id');
			$project_services = $this->project_model->get_project_services_by_productid($idd);

			$service_categories = array();
			for($i=0;$i<count($project_services);$i++){
				$categories = $this->project_model->get_categories_by_service_id($project_services[$i]['service_id']);
				array_push($service_categories, $categories);
			}
			$array = array_map('array_filter', $service_categories);
			$array = array_filter($array);
			
			$pp = array();
			foreach($array as $x => $val) {
				foreach($val as $u => $vl) {
					array_push($pp, $vl);
				}
			
			}

			$newArray = array();
			$projectCategories = array();
			foreach ( $pp AS $line ) {
				if ( !in_array($line['category_id'], $projectCategories) ) {
					$projectCategories[] = $line['category_id'];
					$filterCategories[] = $line['category_id'];
				}
			}

			$products = $this->project_model->get_project_products_by_category_id($idd,$filterCategories);
			
			echo json_encode($products);
			
		}
		public function view_project_other_products(){
			$idd = $this->input->get('project_id');
			$project_services = $this->project_model->get_project_services_by_productid($idd);

			$service_categories = array();
			for($i=0;$i<count($project_services);$i++){
				$categories = $this->project_model->get_categories_by_service_id($project_services[$i]['service_id']);
				array_push($service_categories, $categories);
			}
			$array = array_map('array_filter', $service_categories);
			$array = array_filter($array);
			
			$pp = array();
			foreach($array as $x => $val) {
				foreach($val as $u => $vl) {
					array_push($pp, $vl);
				}
			
			}

			$newArray = array();
			$projectCategories = array();
			foreach ( $pp AS $line ) {
				if ( !in_array($line['category_id'], $projectCategories) ) {
					$projectCategories[] = $line['category_id'];
					$filterCategories[] = $line['category_id'];
				}
			}

			$products = $this->project_model->get_other_project_products_by_category_id($filterCategories);
			
			echo json_encode($products);
			
		}
		public function view_project_status($id){
			$data['project_id']=$id;
			$data['project_updates'] = $this->project_model->get_project_updates_by_id($id);
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/projects/project_status';
			$this->load->view('admin/layout', $data);
		}
		public function conversation($id){
			$data['project_id']=$id;
			$query=$this->db->query("SELECT * FROM project_conversation WHERE project_id = $id order by id desc");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$idd= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM project_conversation pc, users u WHERE pc.user_id=u.id and   pc.id =$idd")->row_array();
				$data1[$key]['attachments']   = $this->db->query("SELECT * FROM project_conversation_files WHERE project_conversation_id = $idd")->result_array();
				$data1[$key]['time']   = $this->time_ago($each['create_date']); 
			}
			
			$data['conversation']=array();
			$data['conversation']= $data1;
			$data['view'] = 'admin/projects/project_conversation';
			$this->load->view('admin/layout', $data);
		}
		public function time_ago($time=false, $just_now=false) {
			if ($time instanceOf DateTime)
				$time = $time->getTimestamp();
			elseif (is_numeric($time))
				$time = date('m/d/y h:i A', $time);
			if (strtotime($time) === false)
				$time = date('m/d/y h:i A', time());
			$interval =  date_create($time)->diff(date_create('now'));
			$adjective = strtotime($time) > time() ? 'from now' : 'ago';
			return (
				$interval->days > 0 ? 
					$time : (
						$interval->h < 1  && $interval->i < 1 && $just_now ? 
							'just now' : 
							(
								$interval->h > 1 ? 
									$interval->h.' hour'.(
										$interval->h > 1 ? 
											's' : 
											''
									).' ago' : 
									$interval->i.' minutes'.' '.$adjective
							)
					)
			);
		}
		public function dragDropUpload(){
			$project_id= $this->input->post('project_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/project_documents'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['project_id'] = $project_id;
					$uploadData['file'] = $fileData['file_name']; 
					$uploadData['created_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('project_documents', $uploadData);
					$this->session->set_flashdata('success_msg', 'project Added Successfully!');
				} 
			} 
		}
		public function upload_documents(){
			$project_id= $this->input->post('project_id');
			$document_type_id= $this->input->post('document_type_id');
			if(!empty($_FILES)){ 
				$uploadData['file_name'] =  $_FILES['file']['name'];
				
				$uploadPath = 'uploads/project_documents'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['document_type_id'] = $document_type_id;
					$uploadData['project_id'] = $project_id;
					$uploadData['file'] = $fileData['file_name']; 
					$uploadData['status'] = 1; 
					$uploadData['created_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('project_documents', $uploadData);
					$this->session->set_flashdata('success_msg', 'Document uploaded Successfully!');
				}
			} 
		}
		public function project_gallery_upload(){
			$projectid= $this->input->post('project_id');
			$gallery_type= $this->input->post('gallery_type');
			$gallery_title= $this->input->post('gallery_title');
			$gallery_type_category= $this->input->post('gallery_type_category');
			$visibility= $this->input->post('visibility');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'images/project_gallery'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				
				
				// Upload file to the server 
				if($this->upload->do_upload('file')){
					$fileData = $this->upload->data();
					$filename= $fileData['file_name'];
					$file_ext = pathinfo($filename,PATHINFO_EXTENSION);
					if($file_ext=='png' || $file_ext=='jpg' || $file_ext=='jpeg' || $file_ext=='gif'){
						$file_type= '0';
					}else{
						$file_type= '1';
					}
					$uploadData['project_id'] = $projectid;
					$uploadData['gallery_type'] = $gallery_type;
					$uploadData['gallery_category'] = $gallery_type_category;
					$uploadData['gallery_title'] = $gallery_title;
					$uploadData['visibility'] = $visibility;
					$uploadData['file'] = $fileData['file_name'];
					$uploadData['file_type'] = $file_type;
					
					$this->db->insert('gallery', $uploadData);
					echo 'success';
					$this->session->set_flashdata('success_msg', 'Images Added Successfully!');
				} 
			} 
		}
		public function project_gallery_upload_server(){
			$projectid= $this->input->post('project_id');
			$gallery_type= $this->input->post('gallery_type');
			$gallery_title= $this->input->post('gallery_title');
			$gallery_type_category= $this->input->post('gallery_type_category');
			$visibility= $this->input->post('visibility');
			$file_name= $this->input->post('file_name');
			for($i=0;$i<count($file_name);$i++){
				$uploadData['project_id'] = $projectid;
				$uploadData['gallery_type'] = $gallery_type;
				$uploadData['gallery_category'] = $gallery_type_category;
				$uploadData['gallery_title'] = $gallery_title;
				$uploadData['visibility'] = $visibility;
				$uploadData['file'] = $file_name[$i];
				$uploadData['file_type'] = 0;				
				$this->db->insert('gallery', $uploadData);
			}
					
			echo 'success';
			$this->session->set_flashdata('success_msg', 'Images Added Successfully!');
		}
		public function add_project_conversation_files(){
			$project_conversation_id= $this->input->post('project_conversation_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['project_conversation_id'] = $project_conversation_id;
					$uploadData['file_name'] = $fileData['file_name']; 
					$uploadData['create_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('project_conversation_files', $uploadData);
				} 
			} 
		}
		public function add_project_conversation()
		{
			$message= $this->input->post('message');
			$project_id   = $this->input->post('project_id');
			$conversation_data = array(
				'user_id' => $_SESSION['sadevelopers_admin']['admin_id'],
				'project_id' => $project_id,
				'description' =>$message,
				'create_date' => date("Y-m-d H:i:s"),
			);
			$conversation_status     = $this->project_model->add_project_conversation($conversation_data);
			echo json_encode($conversation_status);
		}
		public function add_project_products()
		{
			$project_id   = $this->input->post('project_id');
			$products= $this->input->post('products');

			$rrr     = $this->project_model->add_project_products($project_id, $products);
			echo $rrr;
		}
		public function add_mapping_project_images()
		{
			$project_id   = $this->input->post('project_id');
			$radio_image= $this->input->post('radio_image');
			$gallery_type= $this->input->post('gallery_type');
			$checkbox_images= $this->input->post('checkbox_images');
			
			$rrr     = $this->project_model->add_mapping_project_images($project_id, $radio_image, $gallery_type,$checkbox_images);
			echo $rrr;
		}
		public function get_products_by_category(){
			
			$categoty_id=$_POST['categoty_id'];

			$rrr=$this->product_model->get_products_by_category($categoty_id);
			return $rrr;
		}
		function add_project_status_history()
		{
			$project_id = $_POST['project_id'];
			$status_name         = $_POST['status_name'];
			$status_description         = $_POST['status_description'];
			$project_status = array(
				'project_id' => $project_id,
				'status_id' => 	$status_name,
				'status_description' => 	$status_description			
			);
			$rrr=$this->project_model->add_project_status_history($project_status,$project_id,$status_name);
			if($rrr){
				$this->session->set_flashdata('success_msg', 'Status Updated Successfully');
				redirect(base_url('admin/projects'));
			}
		}		
		public function add(){
			if($this->input->post('submit')){
				
				if($this->input->post('project_start_date') !=''){
					$project_start_date = date("Y-m-d",  strtotime($this->input->post('project_start_date')));
				}else{
					$project_start_date='';
				}
				if($this->input->post('project_end_date') !=''){
					$project_end_date=date("Y-m-d",  strtotime($this->input->post('project_end_date')));
				}else{
					$project_end_date='';
				}
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
					$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$service_id=$this->input->post('service_id');
				$data = array(
					'p_id' => 'PRJ'.$random_id,				
					'project_name' => $this->input->post('project_name'),
					'project_description' => $this->input->post('description'),
					'client_id	' => $this->input->post('client_id'),
					'project_start_date' => $project_start_date,
					'project_end_date	' => $project_end_date,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'status' =>1,
					'created_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->project_model->add_project($data);
				$project_id=$result;
				if($result){
					for($i=0;$i<count($service_id);$i++){
						$sdata = array(
							'project_id' => $project_id,				
							'service_id' => $service_id[$i],
							'status' =>1
						);
						$sdata = $this->security->xss_clean($sdata);
						$resultt = $this->project_model->add_project_service($sdata);
					}
					$dataa['status'] = 0;
					$dataa['project_address'] = 0;
					$dataa['client_infornation'] = 0;
					$dataa['project_gallery'] = 0;
					$dataa['project_reviews'] = 0;
					$dataa['project_vendors'] = 0;
					$dataa['project_id'] = $project_id;
					$this->db->insert('project_website_permissions', $dataa);
					
					echo json_encode($result);
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_products'] =  $this->product_model->get_products_by_category();
				$data['all_clients'] =  $this->client_model->get_all_active_clients();
				$data['all_services'] =  $this->service_model->get_all_active_services();
				$data['view'] = 'admin/projects/add_projects';
				$this->load->view('admin/layout', $data);
			}
			
		}
		public function project_status_upload(){
			$project_update_id= $this->input->post('project_update_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/project_update_documents'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['project_update_id'] = $project_update_id;
					$uploadData['file'] = $fileData['file_name']; 
					$uploadData['create_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('project_update_files', $uploadData);
					$this->session->set_flashdata('success_msg', 'project Added Successfully!');
				} 
			} 
		}
		public function project_comment_status_upload(){
			$comment_id= $this->input->post('comment_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/project_update_documents'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['comment_id'] = $comment_id;
					$uploadData['file'] = $fileData['file_name']; 
					$uploadData['create_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('project_update_comment_files', $uploadData);
					$this->session->set_flashdata('success_msg', 'Commented Successfully!');
				} 
			} 
		}
		public function add_project_update(){
			if($this->input->post('submit')){
				$project_id=$this->input->post('project_id');				
				$data = array(
					'project_id' => $this->input->post('project_id'),
					'user_id' => $_SESSION['sadevelopers_admin']['admin_id'],					
					'title' => $this->input->post('status_title'),
					'description	' => $this->input->post('status_description'),
					'create_date' => date('Y-m-d H:i:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->project_model->add_project_update($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Update Added Successfully!');
					echo json_encode($result);
				}
			}
			
		}
		public function add_comment(){
			$project_id=$this->input->post('sproject_id');
			$data = array(
				'project_update_id' => $this->input->post('project_update_id'),
				'user_id' => $_SESSION['sadevelopers_admin']['admin_id'],					
				'comment' => $this->input->post('comment')
			);
			$data = $this->security->xss_clean($data);
			$result = $this->project_model->add_project_update_comment($data);
			if($result){
				echo json_encode($result);
			}
			
		}
		public function edit($id = 0){
			if($this->input->post('submit')){
				
				$project_id=$this->input->post('project_id');
				if($this->input->post('project_start_date') !=''){
					$project_start_date = date("Y-m-d",  strtotime($this->input->post('project_start_date')));
				}else{
					$project_start_date='';
				}
				if($this->input->post('project_end_date') !=''){
					$project_end_date=date("Y-m-d",  strtotime($this->input->post('project_end_date')));
				}else{
					$project_end_date='';
				}
				$data = array(				
					'project_name' => $this->input->post('project_name'),
					'client_id	' => $this->input->post('client_id'),
					'project_start_date' => $project_start_date,
					'project_end_date	' => $project_end_date,
					'project_description' => $this->input->post('description'),
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'status' =>$this->input->post('status'),
					'updated_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->project_model->update_project($data,$project_id);
				$service_id=$this->input->post('service_id');
				if($result){
					for($i=0;$i<count($service_id);$i++){
						$sdata = array(
							'project_id' => $project_id,				
							'service_id' => $service_id[$i],
							'status' =>1
						);
						$sdata = $this->security->xss_clean($sdata);
						$count=$this->db->query("SELECT * FROM project_services where project_id=$project_id and service_id=$service_id[$i]")->num_rows();
						if($count==0){
							$resultt = $this->project_model->add_project_service($sdata);
						}else{
							$this->db->where('project_id', $project_id);
							$this->db->update('project_website_permissions', $dataa);
						}
					}
				}
				if (isset($_POST['website_access'])) {
				  $dataa['status'] = 1;
				} else {
				  $dataa['status'] = 0;
				}
				if (isset($_POST['project_address'])) {
				  $dataa['project_address'] = 1;
				} else {
				  $dataa['project_address'] = 0;
				}
				if (isset($_POST['client_infornation'])) {
				  $dataa['client_infornation'] = 1;
				} else {
				  $dataa['client_infornation'] = 0;
				}if (isset($_POST['project_gallery'])) {
				  $dataa['project_gallery'] = 1;
				} else {
				  $dataa['project_gallery'] = 0;
				}
				if (isset($_POST['project_reviews'])) {
				  $dataa['project_reviews'] = 1;
				} else {
				  $dataa['project_reviews'] = 0;
				}
				if (isset($_POST['project_vendors'])) {
				  $dataa['project_vendors'] = 1;
				} else {
				  $dataa['project_vendors'] = 0;
				}
				$dataa['project_id'] = $project_id;
				$count=$this->db->query("SELECT * FROM project_website_permissions where project_id=$project_id")->num_rows();
				if($count==0){
					$this->db->insert('project_website_permissions', $dataa);
				}else{
					$this->db->where('project_id', $project_id);
					$this->db->update('project_website_permissions', $dataa);
				}
				if($result){
					$this->session->set_flashdata('success_msg', 'Project Updated Successfully!');
					redirect(base_url('admin/projects'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_products'] =  $this->product_model->get_products_by_category();
				$data['all_clients'] =  $this->client_model->get_all_active_clients();
				$data['all_services'] =  $this->service_model->get_all_active_services();
				$data['project'] = $this->project_model->get_project_by_id($id);			
				$data['view'] = 'admin/projects/edit_projects';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('projects', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'project Deleted Successfully!');
			echo 'success';
		}
		public function unmap_image(){
			$id= $this->input->post('id');
			$this->db->delete('project_mapped_images', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'project Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$project_id = $_POST['project_id'];
			$id         = $_POST['id'];
			return $this->project_model->update_status($project_id, $id);
		}
		function update_project_products()
		{
			$project_id = $_POST['project_id'];
			$products         = $_POST['radio_arr'];
			return $this->project_model->update_project_products($project_id, $products);
		}
		public function delps(){
			$id= $this->input->post('id');
			$this->db->delete('projects_services', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'project Service Deleted Successfully!');
			echo 'success';
		}
		public function fetch_uname()
		{
			$uname = $this->input->post('user_name');
			$result   = $this->project_model->get_user_by_name($uname);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
			  
		}
		public function fetch_email()
		{
			$email = $this->input->post('business_email');
			$result   = $this->project_model->get_project_by_email($email);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function fetch_npi()
		{
			$npi_number = $this->input->post('npi_number');
			$result   = $this->project_model->get_project_by_npi($npi_number);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function filter_projects()
		{
			//$data['all_projects'] =  $this->project_model->get_all_projects();
			$id = $this->input->post('client_id');
			$rrr=$this->project_model->get_all_projects1($id);
			$output = array('projects_list'   => $rrr);
			echo json_encode($output, true);
		}
		public function get_project_services(){
			$id =$this->input->post('id');
			$rrr= $this->project_model->get_project_services($id);
			echo json_encode($rrr);
		}
	}


?>