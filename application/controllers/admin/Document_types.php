<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Document_types extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/document_types_model', 'document_types_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('state_model', 'state_model');
		}
		
		public function index(){
			$data['all_document_types'] =  $this->document_types_model->get_all_document_types();
			$data['view'] = 'admin/document_types/all_document_types';
			$this->load->view('admin/layout', $data);
		}
		public function get_document_type_by_id(){
			$id =$this->input->post('id');
			$rrr= $this->document_types_model->get_document_type_by_id($id);
			echo json_encode($rrr);
		}
		public function add(){
			if($this->input->post('submit')){
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$document_type_name =$this->input->post('document_type_name');
				$document_type_description = $this->input->post('document_type_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'document_type_name' => $document_type_name,
					'document_type_description' => $document_type_description,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->document_types_model->add_document_type($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'document_type Added Successfully!');
					redirect(base_url('admin/document_types'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/document_types/add_document_type';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id =$this->input->post('document_type_id');
				$document_type_name =$this->input->post('document_type_name');
				$document_type_description = $this->input->post('document_type_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'document_type_name' => $document_type_name,
					'document_type_description' => $document_type_description,
					'status'=>1,
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->document_types_model->edit_document_type($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'document_type Updated Successfully!');
					redirect(base_url('admin/document_types/'));
				}
			}
			else{
				$data['view'] = 'admin/document_types/edit_document_type';
				$this->load->view('admin/layout', $data);
			}
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('document_types', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Project Type Deleted Successfully!');
			echo 'success';
		}
	}


?>