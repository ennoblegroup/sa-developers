<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Vendors extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->helper(array('form', 'url'));
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('admin/product_model', 'product_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/category_model', 'category_model');
		}

		public function index(){
			$data['all_vendors'] =  $this->vendor_model->get_all_vendors();
			$data['view'] = 'admin/vendors/all_vendors';
			$this->load->view('admin/layout', $data);
		}
		public function vendor_products($id=null){
			$data['all_products'] =  $this->vendor_model->get_all_vendor_products($id);
			$data['view'] = 'admin/vendors/all_vendor_products';
			$this->load->view('admin/layout', $data);
		}
		function randomPassword($length,$count, $characters) {

			// $length - the length of the generated password
			// $count - number of passwords to be generated
			// $characters - types of characters to be used in the password
			 
			// define variables used within the function    
				$symbols = array();
				$passwords = array();
				$used_symbols = '';
				$pass = '';
			 
			// an array of different character types    
				$symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
				$symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$symbols["numbers"] = '1234567890';
				$symbols["special_symbols"] = '!?~@#-_';
			 
				$characters = explode(",",$characters); // get characters types to be used for the passsword
				foreach ($characters as $key=>$value) {
					$used_symbols .= $symbols[$value]; // build a string with all characters
				}
				$symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
				 
				for ($p = 0; $p < $count; $p++) {
					$pass = '';
					for ($i = 0; $i < $length; $i++) {
						$n = rand(0, $symbols_length); // get a random character from the string with all characters
						$pass .= $used_symbols[$n]; // add the character to the password string
					}
					$passwords[] = $pass;
				}
				 
				return $passwords[0];// return the generated password
		}
		public function add(){
			if($this->input->post('submit')){
				$config['upload_path'] = 'images/vendors/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/vendors/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 160;
				$config['height'] = 30;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$category_id=$this->input->post('category_id');
				$website_url= $this->input->post('website_url');
				$last_name=$this->input->post('last_name');
				$first_name=$this->input->post('first_name');
				$email=$this->input->post('email');
				$user_password=$this->randomPassword(8,1,"lower_case,upper_case,numbers,special_symbols");
				//$_SESSION['sadevelopers_admin']['Role_id']; 
				$ulength = 7;
				$ucharacters = '0123456789';
				$urandom_id = "";
				for ($i = 0; $ulength > $i; $i++) {
				$urandom_id .= $ucharacters[mt_rand(0, strlen($ucharacters) -1)];
				}
				$data = array(	
					'vendor_id' => 'VND'.$urandom_id,
					'vendor_name	' => $this->input->post('vendor_name'),
					'vendor_mobile_no' => $this->input->post('vendor_phone_number'),						
					'vendor_email' => $this->input->post('vendor_email'),		
					'lastname' => $last_name,
					'firstname' => $first_name,
					'middlename	' => $this->input->post('middle_name'),
					'mobile_no' => $this->input->post('phone_number'),						
					'email' => $email,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'image' => $fname,
					'website_url' => $website_url,
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'password' => password_hash($user_password, PASSWORD_BCRYPT),
					'status' =>1,
					'created_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->vendor_model->add_vendor($data);
				if($result){
					for($i=0;$i<count($category_id);$i++){
						$vdata = array(
							'vendor_id' => $result,
							'category_id' => $category_id[$i],
							'status' =>1,
						);
						if($category_id[$i] !=''){
							$vdata = $this->security->xss_clean($vdata);
							$resultt = $this->vendor_model->add_vendor_category($vdata);
						}
					}
					$this->session->set_flashdata('success_msg', 'vendor Added Successfully!');
					redirect(base_url('admin/vendors'));
				}
			}
			else{
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/vendors/add_vendor';
				$this->load->view('admin/layout', $data);
			}
			
		}
		public function add_vendor_product(){
			if($this->input->post('submit')){				
				$vendor_id =$this->input->post('vendor_id');
				$product_id = $this->input->post('product_id');
				$data = array(
					'vendor_id' => $vendor_id,
					'product_id' => $product_id,
					'status'=>1,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->vendor_model->add_vendor_product($data);
				if($result){
					echo json_encode('Added successfully',true);
				}
			}
			else{
				$data['all_products'] =  $this->product_model->get_all_vendor_products();
				$data['all_vendors'] =  $this->vendor_model->get_all_vendors();
				$data['view'] = 'admin/vendors/add_vendor_product';
				$this->load->view('admin/layout', $data);
			}
			
		}
		public function edit($id = 0){
			
			if($this->input->post('submit')){
				$id = $this->input->post('vendor_id');
				$name = $_FILES['image']['name'];
				$sql = "select * from vendors where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fname = $row['image'];
				if($name!='') 
				{	
					$path = "images/vendors/" . $row['image'];
					$path1 = "images/vendors/thumb" . $row['image'];
					@unlink($path);
					@unlink($path1);
					$config['upload_path'] = 'images/vendors/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                  
					$this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/vendors/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';
					$config['width'] = 180;
					$config['height'] = 40;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					$fname = $file_data['file_name'];
				}
				$category_id=$this->input->post('category_id');
				$website_url= $this->input->post('website_url');
				$last_name=$this->input->post('last_name');
				$first_name=$this->input->post('first_name');
				$email=$this->input->post('email');
				
				$data = array(
					'vendor_name' => $this->input->post('vendor_name'),
					'vendor_mobile_no' => $this->input->post('vendor_phone_number'),						
					'vendor_email' => $this->input->post('vendor_email'),		
					'lastname' => $last_name,
					'firstname' => $first_name,
					'middlename	' => $this->input->post('middle_name'),
					'mobile_no' => $this->input->post('phone_number'),						
					'email' => $email,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'image' => $fname,
					'website_url' => $website_url,
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->vendor_model->edit_vendor($data, $id);
				if($result && $category_id !=''){
					$tags = implode(', ', $category_id);
					for($i=0;$i<count($category_id);$i++){
						$query=$this->db->query("SELECT * FROM vendor_categories where vendor_id=$id and category_id=$category_id[$i]");
						$cnt   = $query->num_rows();
						if($cnt==0){
							$vdata = array(
								'vendor_id' => $id,
								'category_id' => $category_id[$i],
								'status' =>1,
							);
							if($category_id[$i] !=''){
								$vdata = $this->security->xss_clean($vdata);
								$resultt = $this->vendor_model->add_vendor_category($vdata);
							}
						}
					}
					$query=$this->db->query("DELETE  FROM vendor_categories where vendor_id=$id and category_id NOT IN (".$tags.")");
				}
				$this->session->set_flashdata('success_msg', 'vendor Updated Successfully!');
				redirect(base_url('admin/vendors'));
				
			}
			else{
				$data['all_categories'] =  $this->category_model->get_all_active_categories();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['vendor'] = $this->vendor_model->get_vendor_by_id($id);				
				$data['view'] = 'admin/vendors/edit_vendor';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('vendors', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'vendor is Deleted Successfully!');
			echo 'success';
		}
		public function fetch_email()
		{
			$email= $this->input->post('email');
			$vendor = $this->vendor_model->get_vendor_by_email($email);
			//return $data;
			 if($vendor['cnt'] > 0){
				return true;
			}
			else
			{
				return false; 
			}
		}
		public function del_vendor_product(){
			
			$id =$this->input->post('id');
			$this->db->delete('vendor_products', array('id' => $id));
			echo json_encode('Deleted successfully',true);
		}
		public function get_mapped_templates(){
			
			echo json_encode($this->vendor_model->get_mapped_templates());
		}
	}


?>