<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Aboutus extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/aboutus_model', 'aboutus_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_aboutus'] =  $this->aboutus_model->get_all_aboutus();
			$data['view'] = 'admin/aboutus/all_aboutus';
			$this->load->view('admin/layout', $data);
		}
		public function view_about($id){
			$data['about_view'] =  $this->aboutus_model->get_aboutus_by_id($id);		
			$data['view'] = 'admin/aboutus/view_aboutus';
			$this->load->view('admin/layout', $data);
		}
		
		public function add(){
			if($this->input->post('submit')){
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data = array(
					//'service_id' => 'SER'.$random_id,
					'aboutus_name' => $this->input->post('aboutus_name'),
					'description' => $this->input->post('description'),
					'status' =>1,
					'created_date' => date('Y-m-d : h:m:s'),
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->aboutus_model->add_aboutus($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Service Added Successfully!');
					redirect(base_url('admin/aboutus'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/aboutus/add_aboutus';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$name = $_FILES['image']['name'];
				$id = $this->input->post('id');
				$sql = "select * from about_us where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{
					
					$path = "images/about/" . $row['image'];
					$path1 = "images/about/thumb" . $row['image'];
					$config['upload_path'] = 'images/about/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/about/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '';
					$config['height'] = '350';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];				
				}
				if(isset($_POST['is_team'])){
					$is_team=1;
				}else{
					$is_team=0;
				}
				$title = $_POST['title'];
				$description = $_POST['description'];
				$data = array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'image' => $fileName,
					'is_team'=>$is_team,
					'status'=>1,
					'update_date'=>date('Y-m-d H:i:s'),
				);					
				$data = $this->security->xss_clean($data);
				$result = $this->aboutus_model->edit_aboutus($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'Aboutus Updated Successfully!');
					redirect(base_url('admin/aboutus'));
				}
			}
			else{
				$data['aboutus'] = $this->aboutus_model->get_aboutus_by_id($id);				
				$data['view'] = 'admin/aboutus/edit_aboutus';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('about_us', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'About Us is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$aboutus_id = $_POST['aboutus_id'];
			$id         = $_POST['id'];
			$this->aboutus_model->update_status($aboutus_id,$id);
			return true;
		}
	}


?>