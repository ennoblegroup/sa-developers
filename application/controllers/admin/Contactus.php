<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Contactus extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/contactus_model', 'contactus_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_contactus'] =  $this->contactus_model->get_all_contactus();
			$data['view'] = 'admin/contactus/all_contactus';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){
				/*$this->form_validation->set_rules('insurance_company_name', 'client Name', 'trim|required');
				$this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
				$this->form_validation->set_rules('city', 'City', 'trim|required');
				$this->form_validation->set_rules('state', 'State', 'trim|required');
				$this->form_validation->set_rules('zip_code', 'Zip Code', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['all_states'] =  $this->state_model->get_all_states();
					$data['view'] = 'admin/insurance_companies/add_insurance_company';
					$this->load->view('admin/layout', $data);
				}
				else{*/
					$length = 7;
					$characters = '0123456789';
					$random_id = "";
					for ($i = 0; $length > $i; $i++) {
					$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
					}
					$data = array(
						'id' => 'CON'.$random_id,
						'name' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						'facebook' => $this->input->post('facebook'),
						'instagram' => $this->input->post('instagram'),
						'linkedin' => $this->input->post('linkedin'),
						'pinterest' => $this->input->post('pinterest'),
						'youtube' => $this->input->post('youtube'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'map' => $this->input->post('map'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'phone_number' => $this->input->post('phone_number'),
						'email_address' => $this->input->post('email_address'),
						'status' =>1,
						'created_date' => date('Y-m-d : h:m:s'),
						'updated_date' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->Contactus_model->add_contactus($data);
					if($result){
						$this->session->set_flashdata('success_msg', 'Contact Us Added Successfully!');
						redirect(base_url('admin/contactus'));
					}
				//}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/contactus/add_contactus';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				/*$this->form_validation->set_rules('insurance_companyname', 'Website Name', 'trim|required');
				$this->form_validation->set_rules('test_link', 'Test Link', 'trim|required');
				$this->form_validation->set_rules('live_link', 'Live Link', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['insurance_company'] = $this->insurance_company_model->get_insurance_company_by_id($id);
					$data['view'] = 'admin/insurance_companies/edit_insurance_company';
					$this->load->view('admin/layout', $data);
				}
				else{*/
					/*$name = $_FILES['file']['name'];
					if($name!='') 
					{
						$sql = "select * from contact where id = '$id'";
						$query = $this->db->query($sql);
						$row = $query->row_array();		
						$path = "images/contactus/" . $row['image'];
						$path1 = "images/contactus/thumb" . $row['image'];
						@unlink($path);
						@unlink($path1);
						$config['upload_path'] = 'images/contactus/';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['max_size'] = '0';
						$config['encrypt_name'] = true;
						$this->load->library('upload', $config);                                  
						$this->upload->initialize($config);
						$this->upload->do_upload('file');
						$file_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $file_data['full_path'];
						$config['new_image'] = 'images/contactus/thumb/';
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = false;
						$config['thumb_marker'] = '';
						$config['width'] = 180;
						$config['height'] = 40;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$fname = $file_data['file_name'];
					}*/
					$data = array(
						//'insurance_companytype' => $this->input->post('insurance_companytype'),
						'title' => $this->input->post('name'),
						'description' => $this->input->post('description'),
						'twitter' => $this->input->post('twitter'),
						'facebook' => $this->input->post('facebook'),
						'instagram' => $this->input->post('instagram'),
						'linkedin' => $this->input->post('linkedin'),
						'pinterest' => $this->input->post('pinterest'),
						'youtube' => $this->input->post('youtube'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'map' => $this->input->post('map'),
						'state' => $this->input->post('state'),
						'zip' => $this->input->post('zip_code'),
						'phone' => $this->input->post('phone_number'),
						'email' => $this->input->post('email_address'),
						//'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);	
					$result = $this->contactus_model->edit_contactus($data, $id);					
					if($result){
						$this->session->set_flashdata('success_msg', 'Contact Us is Updated Successfully!');
						redirect(base_url('admin/contactus'));
					}
				//}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['contactus'] = $this->contactus_model->get_contactus_by_id($id);				
				$data['view'] = 'admin/contactus/edit_contactus';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('contactus', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Contact Us is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$contact_id = $_POST['contact_id'];
			$id         = $_POST['id'];
			return $this->contactus_model->update_status($contact_id, $id);
		}
	}


?>