<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Gallery_types extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/gallery_types_model', 'gallery_types_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('state_model', 'state_model');
		}
		
		public function index(){
			$data['all_gallery_types'] =  $this->gallery_types_model->get_all_gallery_types();
			$data['view'] = 'admin/gallery_types/all_gallery_types';
			$this->load->view('admin/layout', $data);
		}
		public function get_gallery_type_by_id(){
			$id =$this->input->post('id');
			$rrr= $this->gallery_types_model->get_gallery_type_by_id($id);
			echo json_encode($rrr);
		}
		public function add(){
			if($this->input->post('submit')){
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$gallery_type_name =$this->input->post('gallery_type_name');
				$gallery_type_description = $this->input->post('gallery_type_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'gallery_type_name' => $gallery_type_name,
					'gallery_type_description' => $gallery_type_description,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->gallery_types_model->add_gallery_type($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'gallery_type Added Successfully!');
					redirect(base_url('admin/gallery_types'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/gallery_types/add_gallery_type';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id =$this->input->post('gallery_type_id');
				$gallery_type_name =$this->input->post('gallery_type_name');
				$gallery_type_description = $this->input->post('gallery_type_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'gallery_type_name' => $gallery_type_name,
					'gallery_type_description' => $gallery_type_description,
					'status'=>1,
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->gallery_types_model->edit_gallery_type($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'gallery_type Updated Successfully!');
					redirect(base_url('admin/gallery_types/'));
				}
			}
			else{
				$data['view'] = 'admin/gallery_types/edit_gallery_type';
				$this->load->view('admin/layout', $data);
			}
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('gallery_types', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Project Type Deleted Successfully!');
			echo 'success';
		}
	}


?>