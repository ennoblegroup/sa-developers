<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends MY_Controller {
		public function __construct(){
			parent::__construct();
			
			$this->load->model('admin/client_model', 'client_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/service_model', 'service_model');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('admin/claim_model', 'claim_model');
		}

		public function index(){
		    $client_id= $_SESSION['sadevelopers_admin']['client_id'];
			$data['all_categories'] =  $this->category_model->get_all_categories();
			$data['all_services'] =  $this->service_model->get_all_active_services();
			
			$data['active_clients'] =  $this->db->query("SELECT * FROM users where client_id !=0 and status=1")->num_rows();
			$data['inactive_clients'] =  $this->db->query("SELECT * FROM users where client_id !=0 and status=0")->num_rows();
			
			$data['active_vendors'] =  $this->db->query("SELECT * FROM vendors where status=1")->num_rows();
			$data['inactive_vendors'] =  $this->db->query("SELECT * FROM vendors where status=0")->num_rows();
			
			$data['active_products'] =  $this->db->query("SELECT * FROM products where status=1")->num_rows();
			$data['inactive_products'] =  $this->db->query("SELECT * FROM products where status=0")->num_rows();
			$prr =$this->db->query("SELECT category_id FROM products")->result_array();
			$prr = array_unique($prr, SORT_REGULAR);
			$data['product_categories'] = count($prr);
			
			if($client_id==0){
				$data['new_projects'] =  $this->db->query("SELECT * FROM projects where status=1")->num_rows();
				$data['inprogress_projects'] =  $this->db->query("SELECT * FROM projects where status=2")->num_rows();
				$data['completed_projects'] =  $this->db->query("SELECT * FROM projects where status=6")->num_rows();
				$data['other_projects'] =  $this->db->query("SELECT * FROM projects where status IN (3,4,5)")->num_rows();
			}else{
			    $data['new_projects'] =  $this->db->query("SELECT * FROM projects where status=1 and client_id='$client_id'")->num_rows();
			    $data['inprogress_projects'] =  $this->db->query("SELECT * FROM projects where status=2 and client_id= '$client_id'")->num_rows();
				$data['completed_projects'] =  $this->db->query("SELECT * FROM projects where status=6 and client_id= '$client_id'")->num_rows();
				$data['other_projects'] =  $this->db->query("SELECT * FROM projects where client_id= '$client_id' and status IN (3,4,5)")->num_rows();
			}
			
			$query1=$this->db->query("SELECT * FROM project_status");
			$data1   = $query1->result_array();
			foreach( $data1 as $key=>$each ){
				$id= $each['project_status_id'];
				$dataa1[$key]   = $this->db->query("SELECT * FROM projects where status=$id")->num_rows();						
			}
			foreach( $data1 as $key=>$each ){
				$id= $each['project_status_id'];
				$dat1[$key]['label']   = $each['project_status_name'];
				$dat1[$key]['symbol']   = $each['project_status_name'];
				$dat1[$key]['y']   = ($dataa1[$key]/array_sum($dataa1))*100;
			}
			$data['projects_vs_status']=$dat1;
			
			
			$query3=$this->db->query("SELECT * FROM services limit 5");
			$data3   = $query3->result_array();
			foreach( $data3 as $key=>$each ){
				$id= $each['id'];
				$dat4[$key]['label']   = $each['service_name'];
				$dat4[$key]['y']   = $this->db->query("SELECT * FROM project_services where service_id=$id ")->num_rows();
			}
			$data['project_vs_services']=$dat4;
			date_default_timezone_set('America/New_York');
			$query4=$this->db->query("select * from appointments");
			$data4   = $query4->result_array();
			foreach( $data4 as $key=>$each ){
				$dat5[$key]['title']   = $each['time'];
				$dat5[$key]['description']   =  $each['last_name'].' '.$each['first_name'].'-'.$each['subject'].'-'.$each['time'];
				$dat5[$key]['start']   =  $each['date'];
				//$dat5[$key]['type']   =  'birthday';
			}
			$data['appointments']=$dat5;
			
			$query=$this->db->query("SELECT * FROM services");
			$data3   = $query3->result_array();
			foreach( $data3 as $key=>$each ){
				$id= $each['id'];
				$dat4[$key]['label']   = $each['service_name'];
				$dat4[$key]['y']   = $this->db->query("SELECT * FROM project_services where service_id=$id")->num_rows();
			}
			$data['project_vs_services']=$dat4;
			
			
			$data['view'] = 'admin/dashboard/index';
			$this->load->view('admin/layout', $data);
		}
		public function index1(){
			
			$data['all_clients'] =  $this->client_model->get_all_active_clients();
			$data['view'] = 'admin/dashboard/index1';
			$this->load->view('admin/layout', $data);
		}
		public function index2(){
			$data['view'] = 'admin/dashboard/index2';
			$this->load->view('admin/layout', $data);
		}
		public function get_prjects_by_months(){
			$year= $this->input->post('year');
			$projects = $this->db->query("SELECT  MONTHNAME(project_start_date) AS label, COUNT(DISTINCT id) as y FROM projects where YEAR(project_start_date)=$year GROUP BY label ")->result_array();
			$dat6 = array();
			for ($m=1; $m<=12; $m++) {
				$month = date('F', mktime(0,0,0,$m, 1, date('Y')));
				$dats['label']   = $month;
				$dats['y']   = $this->db->query("SELECT * FROM projects where YEAR(project_start_date)=$year and MONTH(project_start_date)=$m")->num_rows();
				array_push($dat6,$dats);
			}
				
			echo json_encode($dat6);
		}
		public function get_vendors_and_products(){
			$category= $this->input->post('category');
			
			//$tags = implode(', ', $category);
			$query2=$this->db->query("SELECT * FROM categories where id IN (".$category.")");
			$data2   = $query2->result_array();
			foreach( $data2 as $key=>$each ){
				$id= $each['id'];
				$dat2[$key]['label']   = $each['category_name'];
				$dat2[$key]['y']   = $this->db->query("SELECT * FROM vendor_categories where category_id=$id")->num_rows();
			}
			$data['category_vs_vendors']=$dat2;
			
			foreach( $data2 as $key=>$each ){
				$id= $each['id'];
				$dat3[$key]['label']   = $each['category_name'];
				$dat3[$key]['y']   = $this->db->query("SELECT * FROM products where category_id=$id")->num_rows();
			}
			$data['category_vs_products']=$dat3;

			$output = array('category_vs_vendors'   => $dat2,'category_vs_products'   => $dat3);
			echo json_encode($output);
		}
		public function get_project_by_services(){
			$service= $this->input->post('service');
			
			$query3=$this->db->query("SELECT * FROM services where id IN (".$service.")");
			$data3   = $query3->result_array();
			foreach( $data3 as $key=>$each ){
				$id= $each['id'];
				$dat4[$key]['label']   = $each['service_name'];
				$dat4[$key]['y']   = $this->db->query("SELECT * FROM project_services where service_id=$id ")->num_rows();
			}
			$data['project_vs_services']=$dat4;

			$output = array('project_vs_services'   => $dat4);
			echo json_encode($output);
		}
	}

?>	