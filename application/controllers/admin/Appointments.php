<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Appointments extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/appointments_model', 'appointments_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['view'] = 'admin/appointments/all_appointments';
			$this->load->view('admin/layout', $data);
		}
		public function view_appointment($id){
			$data['appointment_view'] =  $this->appointments_model->get_appointments_by_id($id);		
			$data['view'] = 'admin/messages/view_message';
			$this->load->view('admin/layout', $data);
		}
		public function send_reply()
    	{
        $query = $this->appointments_model->send_reply();
        echo $query;
   		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('appointments', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'appointment is Deleted Successfully!');
			echo 'success';
		}
		public function filter_appointments()
		{			
			$confirmed=$this->appointments_model->get_all_appointments(1);
			$unconfirmed=$this->appointments_model->get_all_appointments(0);
			$completed=$this->appointments_model->get_all_appointments(2);
			$output = array('confirmed_list'   => $confirmed,'unconfirmed_list'   => $unconfirmed, 'completed_list'   => $completed);
			echo json_encode($output, true);
		}
		public function get_appointment()
		{	$appointment_id = $_POST['id'];	
			$appointment=$this->appointments_model->get_appointment($appointment_id);
			$output = array('appointment'   => $appointment);
			echo json_encode($output, true);
		}
		public function filter_enquires()
		{			
			$rrr=$this->appointments_model->get_all_enquires1();
			$output = array('enquires_list'   => $rrr);
			echo json_encode($output, true);
		}
		function update_status()
		{
			$appointment_id = $_POST['appointment_id'];
			$id         = $_POST['id'];
			return $this->appointments_model->update_status($appointment_id, $id);
		}
	}


?>