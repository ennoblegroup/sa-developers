<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Websites extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/website_model', 'website_model');
			$this->load->model('admin/role_model', 'role_model');
		}

		public function index(){
			$data['all_websites'] =  $this->website_model->get_all_websites();
			$data['view'] = 'admin/websites/website_list';
			$this->load->view('admin/layout', $data);
		}
		public function website_conversation(){
			$data['view'] = 'admin/websites/website_conversation';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){

				$this->form_validation->set_rules('websitename', 'Website Name', 'trim|required');
				$this->form_validation->set_rules('test_link', 'Test Link', 'trim|required');
				$this->form_validation->set_rules('live_link', 'Live Link', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['all_roles'] =  $this->role_model->get_all_roles();
					$data['view'] = 'admin/websites/website_add';
					$this->load->view('admin/layout', $data);
				}
				else{
					$config['upload_path'] = 'images/websites/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                  					
					$this->upload->initialize($config);					
					$this->upload->do_upload('file');
					$file_data = $this->upload->data();					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/websites/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';
					$config['width'] = 250;
					$config['height'] = 40;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					$fname = $file_data['file_name'];
					$doc_count =count($this->input->post('document_name'));
					$document_name = $this->input->post('document_name');
					$document_link = $this->input->post('document_link');
					$data = array(
						'websitename' => $this->input->post('websitename'),
						'websitetype' => $this->input->post('websitetype'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zipcode' => $this->input->post('zipcode'),
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'test_link' => $this->input->post('test_link'),
						'live_link' => $this->input->post('live_link'),
						'image' => $fname,
						'created_at' => date('Y-m-d : h:m:s'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->website_model->add_website($data);
					$website_id = $result;
					for($i=0;$i<$doc_count;$i++){
						if($document_name[$i] !='' && $document_link[$i] !=''){
							$data = array(
								'website_id' => $website_id,
								'document_name' => $document_name[$i],
								'document_link' => $document_link[$i],
								'created_at' => date('Y-m-d : h:m:s'),
								'updated_at' => date('Y-m-d : h:m:s'),
							);
							$data = $this->security->xss_clean($data);
							$doc_result = $this->website_model->add_documents($data);
						}
					}
					if($result){
						$this->session->set_flashdata('msg', 'Record is Added Successfully!');
						redirect(base_url('admin/websites'));
					}
				}
			}
			else{
				$data['all_roles'] =  $this->role_model->get_all_roles();
				$data['view'] = 'admin/websites/website_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('websitename', 'Website Name', 'trim|required');
				$this->form_validation->set_rules('test_link', 'Test Link', 'trim|required');
				$this->form_validation->set_rules('live_link', 'Live Link', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['website'] = $this->website_model->get_website_by_id($id);
					$data['view'] = 'admin/websites/website_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$name = $_FILES['file']['name'];
					if($name!='') 
					{
						$sql = "select * from ci_websites where id = '$id'";
						$query = $this->db->query($sql);
						$row = $query->row_array();		
						$path = "images/websites/" . $row['image'];
						$path1 = "images/websites/thumb" . $row['image'];
						unlink($path);
						@unlink($path1);
						$config['upload_path'] = 'images/websites/';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['max_size'] = '0';
						$config['encrypt_name'] = true;
						$this->load->library('upload', $config);                                  
						$this->upload->initialize($config);
						$this->upload->do_upload('file');
						$file_data = $this->upload->data();
						$config['image_library'] = 'gd2';
						$config['source_image'] = $file_data['full_path'];
						$config['new_image'] = 'images/websites/thumb/';
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = false;
						$config['thumb_marker'] = '';
						$config['width'] = 180;
						$config['height'] = 40;
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();
						$fname = $file_data['file_name'];
					}
					$data = array(
						'websitename' => $this->input->post('websitename'),
						'websitetype' => $this->input->post('websitetype'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zipcode' => $this->input->post('zipcode'),						
						'email' => $this->input->post('email'),
						'phone' => $this->input->post('phone'),
						'test_link' => $this->input->post('test_link'),
						'live_link' => $this->input->post('live_link'),
						'image' => $fname,
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->website_model->edit_website($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Record is Updated Successfully!');
						redirect(base_url('admin/websites'));
					}
				}
			}
			else{
				$data['website'] = $this->website_model->get_website_by_id($id);
				
				$data['view'] = 'admin/websites/website_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del($id = 0){
			$this->db->delete('ci_websites', array('id' => $id));
			$this->session->set_flashdata('msg', 'Record is Deleted Successfully!');
			redirect(base_url('admin/websites'));
		}

	}


?>