<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Features extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/features_model', 'features_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_features'] =  $this->features_model->get_all_features();
			$data['view'] = 'admin/features/all_features';
			$this->load->view('admin/layout', $data);
		}
		
		public function view_feature($id){
			$data['feature_view'] =  $this->features_model->get_features_by_id($id);		
			$data['view'] = 'admin/features/view_feature';
			$this->load->view('admin/layout', $data);
		}

		public function add(){
			if($this->input->post('submit')){				
				$title = $_POST['title'];
				$description = $_POST['description'];	

				$config['upload_path'] = 'images/features/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/features/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 100;
				$config['height'] = 100;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data = array(
					'feature_id' => 'FEA'.$random_id,
					'title' =>  $title,
					'description' => $description,
					'image' => $fname,
					'status' =>1,
					'create_date' => date('Y-m-d : h:m:s'),
					'update_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->features_model->add_features($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Feature Added Successfully!');
					redirect(base_url('admin/features'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/features/add_feature';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$name = $_FILES['image']['name'];
				$featureid = $this->input->post('featureid');
				$sql = "select * from features where id = '$featureid'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{	
					$path = "images/features/" . $row['image'];
					$path1 = "images/features/thumb" . $row['image'];

					$config['upload_path'] = 'images/features/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/features/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '';
					$config['height'] = '350';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}

				$title = $_POST['title'];
				$description = $_POST['description'];

				$data = array(
				'title' => $title,
				'description' => $description,
				'image' => $fileName,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->features_model->edit_features($data, $featureid);
				if($result){
				$this->session->set_flashdata('success_msg', 'Feature Updated Successfully!');
				redirect(base_url('admin/features'));
				}
			}
			else{
				$data['features'] = $this->features_model->get_features_by_id($id);				
				$data['view'] = 'admin/features/edit_features';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('features', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Feature is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$features_id = $_POST['features_id'];
			$id         = $_POST['id'];
			return $this->features_model->update_status($features_id, $id);
		}
	}


?>