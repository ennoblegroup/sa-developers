<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_users'] =  $this->user_model->get_all_users();
			$data['all_roles'] =  $this->role_model->get_all_roles();				
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/users/user_list';
			$this->load->view('admin/layout', $data);
		}
		
		public function add(){
			
			if($this->input->post('submit')){
				$uname= $this->input->post('username');
				$user = $this->user_model->get_user_by_name($uname);					
				if($user['cnt'] > 0){
				$this->session->set_flashdata('danger_msg', 'User Name already exists!');		
				//$this->form_validation->set_rules('username', 'User Name', 'trim|required');				
				$data['all_roles'] =  $this->role_model->get_all_roles();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/users/user_add';
				$this->load->view('admin/layout', $data);
				}				
				else
				{	
					$lastname=$this->input->post('lastname');
					$firstname=$this->input->post('firstname');
					$email=$this->input->post('email');
					$user_password=$this->randomPassword(8,1,"lower_case,upper_case,numbers,special_symbols");
					$length = 7;
					$characters = '0123456789';
					$random_id = "";
					for ($i = 0; $length > $i; $i++) {
					$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
					} 
					$data = array(					
						'userid' => 'USR'.$random_id,
						'client_id'=>$_SESSION['sadevelopers_admin']['client_id'],
						'user_role' => $this->input->post('user_role'),
						//'username' => $this->input->post('firstname').' '.$this->input->post('lastname'),
						'firstname' => $firstname,
						'lastname' => $lastname,
						'middlename' => $this->input->post('middlename'),
						'email' => $email,
						'mobile_no' => $this->input->post('mobile_no'),
						'username' =>$uname,
						'password' => password_hash($user_password, PASSWORD_BCRYPT),
						'is_admin' => 0,
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'created_at' => date('Y-m-d : h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->add_user($data);
					if($result){
						$email_subject ='User Registration';
						$content="User has been sucessfully created.";			
						$data['name'] =$firstname." ". $lastname;
						$data['email_subject'] = $email_subject;
						$data['login_url'] = site_url()."admin";
						$data['uname'] = $email;
						$data['password'] = $user_password;
						$data['content'] = $content;
						$data['client_name'] = 'admin System';
						$temp =$this->load->view('email/common_email',$data,TRUE);
						$msg="$temp";
						$this->email->from('admin@sadevelopers.com');
						$this->email->to($email);
						
						//$this->email->bcc('anila@ennobletechnologies.com');
						$subject = 'User Registration';  
						
						$this->email->subject($subject);
				
						$this->email->set_mailtype('html');
						$this->email->message($msg);
						
						$this->email->send();
						$this->session->set_flashdata('success_msg', 'User is Added Successfully!');
						redirect(base_url('admin/users'));
					}
			    }
			}
			else{
				$data['all_roles'] =  $this->role_model->get_all_roles();				
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/users/user_add';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){					
					$data = array(
						//'username' => $this->input->post('firstname').' '.$this->input->post('lastname'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						'lastname' => $this->input->post('lastname'),
						'middlename' => $this->input->post('middlename'),
						'email' => $this->input->post('email'),
						'mobile_no' => $this->input->post('mobile_no'),
						'user_role' => $this->input->post('user_role'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->edit_user($data, $id);
					if($result){
						$this->session->set_flashdata('success_msg', 'User is Updated Successfully!');
						redirect(base_url('admin/users/view_profile'));
					}				
			}
			else{
				$data['all_roles'] =  $this->role_model->get_all_roles();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['user'] = $this->user_model->get_user_by_id($id);				
				$data['view'] = 'admin/users/user_edit';
				$this->load->view('admin/layout', $data);
			}
		}	
		public function password(){
			if($this->input->post('submit')){				
				$curval= $this->input->post('current_hidden');									
				if(trim($curval)!=''){
					$this->session->set_flashdata('danger_msg', 'Please enter correct password!');		
					$data['view'] = 'admin/users/change_password';
					$this->load->view('admin/layout', $data);
				}				
				else
				{
					
					$id=$this->input->post('user_id');
					$data = array(
					'password' =>  password_hash($this->input->post('Addpwd'), PASSWORD_BCRYPT),
					'is_new' =>  0,					
					);
					
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->update_password($data,$id);
					
					if($result){
						$_SESSION['sadevelopers_admin']='';
						$this->session->set_flashdata('success_msg', 'Password is Updated Successfully!');
						$data['msg']='fgh';
						//setcookie ("change_pwd","change");
						redirect(base_url('admin/auth/login'),$data);
					}
				}				
			}
			else{				
				$data['view'] = 'admin/users/change_password';
				$this->load->view('admin/layout', $data);
			}
		}
		public function edit_profile(){
			if($this->input->post('submit')){	
					$id	= $this->input->post('user_id');
					$uname= $this->input->post('username');
					$data = array(
						//'username' => $this->input->post('firstname').' '.$this->input->post('lastname'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						'lastname' => $this->input->post('lastname'),
						'middlename' => $this->input->post('middlename'),
						'email' => $this->input->post('email'),
						'mobile_no' => $this->input->post('mobile_no'),
						'username' =>$uname,
						'is_admin' => $this->input->post('user_role'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->edit_user($data, $id);
					if($result){
						$this->session->set_flashdata('success_msg', 'User Profile is Updated Successfully!');
						redirect(base_url('admin/users/view_profile'));
					}				
			}
			else{
				$id=$_SESSION['sadevelopers_admin']['admin_id'];
				$data['all_roles'] =  $this->role_model->get_all_roles();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['view'] = 'admin/users/user_profile';
				$this->load->view('admin/layout', $data);
			}
		}
		public function view_profile(){
				$id=$_SESSION['sadevelopers_admin']['admin_id'];
				$data['all_roles'] =  $this->role_model->get_all_roles();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['user'] = $this->user_model->get_user_by_id($id);
				$data['view'] = 'admin/users/view_profile';
				$this->load->view('admin/layout', $data);
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('users', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'User is Deleted Successfully!');
			redirect(base_url('admin/users'));
		}
		public function fetch_uname()
		{
			$uname= $this->input->post('user_name');
			$user = $this->user_model->get_user_by_name($uname);
			//return $data;
			 if($user['cnt'] > 0){
          $response = "<span style='color: red;'>User Name is not available.</span>";
		  }
		  else
		  {
			$response = "<span style='color: green;'>User Name  is available.</span>"; 
		  }
			echo $response;
		}
		public function fetch_currentpassword()
		{
			$current= $this->input->post('current');
			$user_id= $_SESSION['sadevelopers_admin']['admin_id'];
			
			$query = $this->db->get_where('users', array('id' => $user_id));			
			if ($query->num_rows() == 0){
				$result=123;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($current, $result['password']);				
			    if($validPassword){
			        //$result = $query->row_array();
					$response = "";
			    }
				else
				{
					$response = "<span style='color: red;'>Please enter correct password.</span>"; 
				}
				
			}			
			echo $response;
		}
		public function fetch_email()
		{
			$email = $this->input->post('email');
			$result   = $this->user_model->get_user_by_email($email);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		function update_status()
		{
			$user_id = $_POST['user_id'];
			$id         = $_POST['id'];
			return $this->user_model->update_status($user_id, $id);
		}
		public function update_profile_picture()
		{
			$id=$_SESSION['sadevelopers_admin']['admin_id'];
			$img = $_POST['image'];
			$folderPath = "images/clients/";
		  
			$image_parts = explode(";base64,", $img);
			$image_type_aux = explode("image/", $image_parts[0]);
			$image_type = $image_type_aux[1];
		  
			$image_base64 = base64_decode($image_parts[1]);
			$fileName = uniqid() . '.png';
		  
			$file = $folderPath . $fileName;
			file_put_contents($file, $image_base64);
			$fname=$fileName;
			$data['image'] = $fname;
			$result = $this->user_model->edit_user($data, $id);
		}
		public function fetch_email_edit()
		{
			$email = $this->input->post('email');
			$id = $this->input->post('usr_id');
			$result   = $this->user_model->get_user_by_email_edit($email,$id);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		function randomPassword($length,$count, $characters) {
 
			// $length - the length of the generated password
			// $count - number of passwords to be generated
			// $characters - types of characters to be used in the password
			 
			// define variables used within the function    
				$symbols = array();
				$passwords = array();
				$used_symbols = '';
				$pass = '';
			 
			// an array of different character types    
				$symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
				$symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$symbols["numbers"] = '1234567890';
				$symbols["special_symbols"] = '!?~@#-_';
			 
				$characters = explode(",",$characters); // get characters types to be used for the passsword
				foreach ($characters as $key=>$value) {
					$used_symbols .= $symbols[$value]; // build a string with all characters
				}
				$symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
				 
				for ($p = 0; $p < $count; $p++) {
					$pass = '';
					for ($i = 0; $i < $length; $i++) {
						$n = rand(0, $symbols_length); // get a random character from the string with all characters
						$pass .= $used_symbols[$n]; // add the character to the password string
					}
					$passwords[] = $pass;
				}
				 
				return $passwords[0];// return the generated password
			}
	}
?>