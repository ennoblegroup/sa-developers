<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Testimonials extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/testimonials_model', 'testimonials_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_testimonials'] =  $this->testimonials_model->get_all_testimonials();
			
			$data['view'] = 'admin/testimonials/all_testimonials';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){
				
					$project = $_POST['project'];
					$last_name = $_POST['last_name'];
					$first_name = $_POST['first_name'];
					$email = $_POST['email'];
					$phone = $_POST['phone'];
					$date = $_POST['date'];
					$subject_type = $_POST['subject_type'];
					$rating = $_POST['rating'];
					$subject = $_POST['subject'];
					$message = $_POST['message'];
					
					$data = array(
						'project_id' => $project,
						'last_name' => $last_name,
						'first_name' => $first_name,
						'email' => $email,
						'phone' => $phone,
						'subject_type' => $subject_type,
						'subject' => $subject,
						'rating' => $rating,
						'message' => $message,
						'create_date' => date('Y-m-d : h:m:s'),
						'update_date' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->testimonials_model->add_testimonial($data);
					if($result){
						$this->session->set_flashdata('success_msg', 'Testimonial Added Successfully!');
						redirect(base_url('admin/testimonials'));
					}
			}
			else{
				$data['all_projects'] =  $this->testimonials_model->get_all_projects();
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/testimonials/add_testimonials';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				if(!empty($_FILES['image']['name'])) {   
					$uploadDir = "images/testimonials/";
					$fileName = $_FILES['image']['name'];
					$uploadedFile = $uploadDir.$fileName;  
								
					if(move_uploaded_file($_FILES['image']['tmp_name'],$uploadedFile)) {
					
					}  
					}
				else{
					$fileName = $this->input->post('image_name');
				}
				$data = array(
					'name' => $this->input->post('name'),
					'designation' => $this->input->post('designation'),
					'description' => $this->input->post('description'),
					'image' => $fileName
					
				);					
				$data = $this->security->xss_clean($data);
				$result = $this->testimonials_model->edit_testimonials($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'Testimonial Updated Successfully!');
					redirect(base_url('admin/testimonials'));
				}			
			}
			else{
				$data['testimonials'] = $this->testimonials_model->get_testimonials_by_id($id);				
				$data['view'] = 'admin/testimonials/edit_testimonials';
				$this->load->view('admin/layout', $data);
			}
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('messages', array('message_id' => $id));
			$this->session->set_flashdata('danger_msg', 'Testimonial is Deleted Successfully!');
			echo 'success';
		}
		public function filter_testimonials()
		{			
			$rrr=$this->testimonials_model->get_all_testimonials();
			$output = array('testimonials_list'   => $rrr);
			echo json_encode($output, true);
		}
		function update_status()
		{
			$testimonial_id = $_POST['testimonial_id'];
			$id         = $_POST['id'];
			return $this->testimonials_model->update_status($testimonial_id, $id);
		}
	}


?>