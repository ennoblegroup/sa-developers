<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Categories extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/category_model', 'category_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('state_model', 'state_model');
		}
		
		public function index(){
			$data['all_categories'] =  $this->category_model->get_all_categories();
			$data['view'] = 'admin/categories/all_categories';
			$this->load->view('admin/layout', $data);
		}
		public function get_category_by_id(){
			$id =$this->input->post('id');
			$rrr= $this->category_model->get_category_by_id($id);
			echo json_encode($rrr);
		}
		public function get_category_vendor(){
			$id =$this->input->post('id');
			$rrr= $this->category_model->get_category_vendor($id);
			echo json_encode($rrr);
		}
		public function add(){
			if($this->input->post('submit')){
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$config['upload_path'] = 'images/material_categories/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('category_image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/material_categories/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['height'] = 200;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];

				if (!empty($fname)) {
					$data['image'] = $fname;
				}
				$category_name =$this->input->post('category_name');
				$category_description = $this->input->post('category_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'category_name' => $category_name,
					'category_description' => $category_description,
					'file' => $fname,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->category_model->add_category($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Category Added Successfully!');
					redirect(base_url('admin/categories'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/categories/add_category';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id =$this->input->post('category_id');
				$name = $_FILES['category_image']['name'];
				$sql = "select * from categories where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fname = $row['file'];
				if($name!='') 
				{	
					$path = "images/categories/" . $row['file'];
					$path1 = "images/categories/thumb" . $row['file'];

					$config['upload_path'] = 'images/material_categories/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                
					$this->upload->initialize($config);				
					$this->upload->do_upload('category_image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/material_categories/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';
					$config['width'] = 260;
					$config['height'] = 250;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$fname = $file_data['file_name'];	               
				}
				$category_name =$this->input->post('category_name');
				$category_description = $this->input->post('category_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'category_name' => $category_name,
					'category_description' => $category_description,
					'file' => $fname,
					'status'=>1,
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->category_model->edit_category($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'Category Updated Successfully!');
					redirect(base_url('admin/categories/'));
				}
			}
			else{
				$data['view'] = 'admin/categories/edit_category';
				$this->load->view('admin/layout', $data);
			}
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('categories', array('id' => $id));
			$this->db->delete('products', array('category_id' => $id));
			$this->session->set_flashdata('danger_msg', 'Category Deleted Successfully!');
			echo 'success';
		}
	}


?>