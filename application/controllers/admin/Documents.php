<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Documents extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/project_model', 'project_model');
			$this->load->model('admin/client_model', 'client_model');
			$this->load->model('admin/documents_model', 'document_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/document_types_model', 'document_types_model');
		}

		public function index(){
			$data['documents'] =  $this->document_model->get_all_documents_by_id();
			$data['all_document_types'] =  $this->document_types_model->get_all_active_document_types();
			$data['view'] = 'admin/documents/all_documents';
			$this->load->view('admin/layout', $data);
		}
		public function view_documents($id){
			$data['documents'] =  $this->document_model->get_all_documents_by_id($id);
			
			$data['view'] = 'admin/documents/all_documents';
			$this->load->view('admin/layout', $data);
		}
		public function dragDropUpload(){
			$project_id= $this->input->post('project_id');
			$document_type_id= $this->input->post('document_type_id');
			if(!empty($_FILES)){ 
				$uploadData['file_name'] =  $_FILES['file']['name'];
				
				$uploadPath = 'uploads/project_documents'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['document_type_id'] = $document_type_id;
					$uploadData['file'] = $fileData['file_name']; 
					$uploadData['status'] = 1; 
					$uploadData['created_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('documents', $uploadData);
					$this->session->set_flashdata('success_msg', 'Document uploaded Successfully!');
				}
			} 
		}
	}


?>