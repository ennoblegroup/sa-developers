<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->config->load('messages_list', TRUE);
			$this->log_data = array(
				'messages_list' => $this->config->item('messages_list')
			);
			$this->load->model('admin/auth_model', 'auth_model');
			$this->load->model('admin/settings_model', 'settings_model');
			$this->load->helper('captcha');
			//include_once APPPATH . "libraries/vendor/autoload.php";
			
			
		}

		public function index(){
			if($_SESSION['sadevelopers_admin']['is_admin_login']==1)
			{
				redirect('admin/dashboard');
			}
			else{
				redirect('admin/auth/login');
			}
		}

		public function login(){
			
			// $google_client = new Google_Client();
			// $google_client->setClientId('1095080886321-a4e1q9c0bj1i3dogk5hki0bdtumkb3sf.apps.googleusercontent.com'); //Define your ClientID

			// $google_client->setClientSecret('4q8_xpSOMquRXxckxOZoxCFr'); //Define your Client Secret Key

			// $google_client->setRedirectUri(base_url('admin/auth/google_login')); //Define your Redirect Uri

			// $google_client->addScope('email');

			// $google_client->addScope('profile');
				
			if($this->input->post('submit')){
				$email=$this->input->post('email');
				$password=$this->input->post('password');
				$data = array(
				'email' => $email,
				'password' => $password
				);
				$result = $this->auth_model->login($data);
				if($result['client_id']>0){
					//$presult = $this->auth_model->get_client($result['client_id']);
				}
				if ($result) {
					if($result['status_id']==0){
						$data['msg'] = $this->log_data['messages_list']['adminlogin_inactiveuser'];
						$config = array(
							'img_url' => base_url() . 'image_for_captcha/',
							'img_path' => 'image_for_captcha/',
							'img_height' => 38,
							'word_length' => 6,
							'img_width' => 100,
							'font_size' => 50
						);
						$captcha = create_captcha($config);
						$this->session->unset_userdata('valuecaptchaCode');
						$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
						$data['captchaImg'] = $captcha['image'];
						//print_r($data['captchaImg']);die;
						$this->load->view('admin/auth/login', $data);
					}elseif($result['rstatus_id']==0){
						$data['msg'] = $this->log_data['messages_list']['adminlogin_inactiverole'];
						$config = array(
							'img_url' => base_url() . 'image_for_captcha/',
							'img_path' => 'image_for_captcha/',
							'img_height' => 38,
							'word_length' => 6,
							'img_width' => 100,
							'font_size' => 50
						);
						$captcha = create_captcha($config);
						$this->session->unset_userdata('valuecaptchaCode');
						$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
						$data['captchaImg'] = $captcha['image'];
						//print_r($data['captchaImg']);die;
						$this->load->view('admin/auth/login', $data);
					}else{	
						$admin_data = $result;
						$array_going_second = array(
							'admin_id' => $result['u_id'],
							'role_id' => $result['user_role'],
							'name' => $result['lastname'].' '.$result['firstname'],
							'is_admin_login' => TRUE
							);
						$admin_data = array_merge($admin_data, $array_going_second);
						if($result['client_id']>0){
							//$admin_data = array_merge($admin_data, $presult);
						}
						//print_r($admin_data);die;
						$admin_settings = $this->settings_model->settings();
						$_SESSION['sadevelopers_admin']=$admin_data;
						$_SESSION['sadevelopers_admin_settings']=$admin_settings;
						if(!empty($_POST["remember"])) {
							setcookie ("admin_email",$email,time()+ (10 * 365 * 24 * 60 * 60));
							//setcookie ("admin_password",$password,time()+ (10 * 365 * 24 * 60 * 60));
						} else {
							if(isset($_COOKIE["admin_email"])) {
								setcookie ("admin_email","");
							}
							/*if(isset($_COOKIE["admin_password"])) {
								setcookie ("admin_password","");
							}*/
						}
						$roleid= $admin_data['role_id'];
						$this->db->where('role_id',$roleid);
						$this->db->where('menu_id',10);
						$status_countc = $this->db->get('roles_permissions')->num_rows();
						if($status_countc>0 || $roleid==0){
							if($admin_data['client_id']==0){
								redirect(base_url('admin/dashboard'), 'refresh');
							}elseif($admin_data['client_id']>0){
								if($result['status']==1)
								{
									if($result['is_new']==1){
										$this->session->set_flashdata('cpass', 'password not updated');
									}
									redirect(base_url('admin/dashboard/index1'));								
								}
								else
								{
									$data['msg'] = $this->log_data['messages_list']['adminlogin_inactiveuser'];
									$config = array(
										'img_url' => base_url() . 'image_for_captcha/',
										'img_path' => 'image_for_captcha/',
										'img_height' => 38,
										'word_length' => 6,
										'img_width' => 100,
										'font_size' => 50
									);
									$captcha = create_captcha($config);
									$this->session->unset_userdata('valuecaptchaCode');
									$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
									$data['captchaImg'] = $captcha['image'];
									//print_r($data['captchaImg']);die;
									$this->load->view('admin/auth/login', $data);
								}
							}
						}else{
							redirect(base_url('admin/dashboard/index2'), 'refresh');
						}
						
					}
				}
				else{						
					//$this->session->set_flashdata('danger_msg', 'Invalid Email Id/Password!');
					$data['msg'] = $this->log_data['messages_list']['adminlogin_wrong_usename/password'];
					$config = array(
						'img_url' => base_url() . 'image_for_captcha/',
						'img_path' => 'image_for_captcha/',
						'img_height' => 38,
						'word_length' => 6,
						'img_width' => 100,
						'font_size' => 50
					);
					$captcha = create_captcha($config);
					$this->session->unset_userdata('valuecaptchaCode');
					$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
					$data['captchaImg'] = $captcha['image'];
					//print_r($data['captchaImg']);die;
					$this->load->view('admin/auth/login', $data);
				}
			}
			else{
				$config = array(
					'img_url' => base_url() . 'image_for_captcha/',
					'img_path' => 'image_for_captcha/',
					'img_height' => 38,
					'word_length' => 6,
					'img_width' => 100,
					'font_size' => 50
				);
				$captcha = create_captcha($config);
				$this->session->unset_userdata('valuecaptchaCode');
				$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
				$data['captchaImg'] = $captcha['image'];
                
				
				//$login_button = '<a href="'.$google_client->createAuthUrl().'"><img style="padding-top: 15px;width: 50%;" src="https://www.oncrashreboot.com/images/create-apple-google-signin-buttons-quick-dirty-way-google.png" /></a>';
				//$data['login_button'] = $login_button;
  
				$this->load->view('admin/auth/login', $data);
			}
		}
		public function google_login()
		{	
			$google_client = new Google_Client();
			if(isset($_GET["code"]))
			  {
			   $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

			   if(!isset($token["error"]))
			   {
				$google_client->setAccessToken($token['access_token']);

				$this->session->set_userdata('access_token', $token['access_token']);

				$google_service = new Google_Service_Oauth2($google_client);

				$data = $google_service->userinfo->get();

				$current_datetime = date('Y-m-d H:i:s');

				if($this->google_login_model->Is_already_register($data['id']))
				{
				 //update data
				 $user_data = array(
				  'first_name' => $data['given_name'],
				  'last_name'  => $data['family_name'],
				  'email_address' => $data['email'],
				  'profile_picture'=> $data['picture'],
				  'updated_at' => $current_datetime
				 );

				 
				}
				else
				{
				 //insert data
				 $user_data = array(
				  'login_oauth_uid' => $data['id'],
				  'first_name'  => $data['given_name'],
				  'last_name'   => $data['family_name'],
				  'email_address'  => $data['email'],
				  'profile_picture' => $data['picture'],
				  'created_at'  => $current_datetime
				 );

				 
				}
				$this->session->set_userdata('user_data', $user_data);
			   }
			  }
		}	
		public function refresh()
		{
			$config = array(
				'img_url' => base_url() . 'image_for_captcha/',
				'img_path' => 'image_for_captcha/',
				'img_height' => 38,
				'word_length' => 6,
				'img_width' => 100,
				'font_size' => 50
			);
			$captcha = create_captcha($config);
			$this->session->unset_userdata('valuecaptchaCode');
			$this->session->set_userdata('valuecaptchaCode', $captcha['word']);
			$output = array('img'   => $captcha['image'],'word'   => $captcha['word']);
			echo json_encode($output, true);
		}
		public function change_pwd(){
			$id = $_SESSION['sadevelopers_admin']['admin_id'];
			if($this->input->post('submit')){
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|matches[password]');
				if ($this->form_validation->run() == FALSE) {
					$data['view'] = 'admin/auth/change_pwd';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
					);
					$result = $this->auth_model->change_pwd($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'Password has been changed successfully!');
						redirect(base_url('admin/auth/change_pwd'));
					}
				}
			}
			else{
				$data['view'] = 'admin/auth/change_pwd';
				$this->load->view('admin/layout', $data);
			}
		}
		public function update_password(){
			$id = $_SESSION['sadevelopers_admin']['admin_id'];
			$email=$this->input->post('email');
			$resetid=$this->input->post('resetid');
			$data = array(
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
			);
			$result = $this->auth_model->update_password($data, $email,$resetid);
			if($result){
				$this->session->set_flashdata('success_msg', 'Password has been reset successfully!');
				redirect(base_url('admin/auth/login'));
			}
		}
		public function reset_password(){
			if($this->input->post('submit')){
				$data = array(
					'email' => $this->input->post('email')
				);
				$result = $this->auth_model->check_useremail($data);

				if($result){
					$result2 = $this->auth_model->reset_link_email($data);
					if($result2){
						$this->session->set_flashdata('success_msg', 'Reset Password link has been sent to your Email Address.');
						redirect(base_url('admin/auth/login'));
					}else{
						$this->session->set_flashdata('error', "Server Error");
						$this->load->view('admin/auth/forgot_password', $data);
					}					
				}else{
					$this->session->set_flashdata('error', $this->log_data['messages_list']['forgotpassword_invalid_email']);
					$this->load->view('admin/auth/forgot_password', $data);
				}
			}
			else{
				//$data['view'] = 'admin/auth/forgot_password';
				$this->load->view('admin/auth/forgot_password');
			}
		}
		public function new_password($data){
			
			$this->load->view('admin/auth/new_password',$data);
		}
		public function verify_resetpassword_link(){
			
			$user_email = $this->input->get('user_email');
			$token = $this->input->get('token');
			$data=$this->auth_model->verify_resetpassword_link($user_email,$token);

			if($data)
			{
				$this->new_password($data);
				//$this->load->view('admin/auth/new_password', $data);
			}
			else
			{
				$this->session->set_flashdata('error','Link Expired');
				$this->load->view('admin/auth/forgot_password', $data);
			}
		}	
		public function logout(){
			$rr=$_SESSION['sadevelopers_admin']['client_id'];
			$this->session->unset_userdata('sadevelopers_admin');
			$this->session->unset_userdata('sadevelopers_admin_settings');
			if($rr>0){
				redirect(base_url('/'), 'refresh');
			}
			else{
				redirect(base_url('admin/auth/login'), 'refresh');	
			}
		}
			
	}  // end class


?>