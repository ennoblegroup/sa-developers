<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Roles extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/role_model', 'role_model');
		}

		public function index(){
			$data['all_roles'] =  $this->role_model->get_all_airoles();
			$data['view'] = 'admin/roles/role_list';
			$this->load->view('admin/layout', $data);
		}
		public function db(){
			$this->load->helper('file');
			$this->load->helper('download');
			$this->load->library('zip');

			//load database
			$this->load->dbutil();

			//create format
			$db_format=array('format'=>'zip','filename'=>'backup.sql');

			$backup=& $this->dbutil->backup($db_format);

			// file name

			$dbname='backup-on-'.date('d-m-y H:i').'.zip';
			$save='assets/db_backup/'.$dbname;

			// write file

			write_file($save,$backup);

			// and force download
			force_download($dbname,$backup);
		}
		public function add(){
			if($this->input->post('submit')){
				$rname= $this->input->post('rolename');
				$role = $this->role_model->get_role_by_name($rname);					
				if($role['cnt'] > 0){
				//$this->form_validation->set_rules('rolename', 'Role Name', 'trim|required');	
				$this->session->set_flashdata('danger_msg', 'Role Name already exists!');		
				$data['view'] = 'admin/roles';
				$this->load->view('admin/layout', $data);
				}				
				else
				{					
					$data = array(
						'client_id'=>$_SESSION['sadevelopers_admin']['client_id'],
						'rolename' => $this->input->post('rolename'),
						'description' => $this->input->post('description'),
						'status' => $this->input->post('role_status'),
						'created_at' => date('Y-m-d : h:m:s')
					);
					$data = $this->security->xss_clean($data);
					$result = $this->role_model->add_role($data);
					if($result){
						$this->session->set_flashdata('success_msg', 'Role Added Successfully!');
						redirect(base_url('admin/roles'));
					}	
				}
			}
			else{
				$data['view'] = 'admin/roles/role_add';
				$this->load->view('admin/layout', $data);
			}			
		}

		public function edit($id = 0){
			
			if($this->input->post('submit')){
				
				$data = array(
					'rolename' => $this->input->post('rolename'),
					'status' => $this->input->post('role_status'),
					'created_at' => date('Y-m-d : h:m:s'),
					'updated_at' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->role_model->edit_role($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'Role Updated Successfully!');
					redirect(base_url('admin/roles'));
				}
				
			}
			/*else{
				$data['role'] = $this->role_model->get_role_by_id($id);
				$data['view'] = 'admin/roles/role_edit';
				$this->load->view('admin/layout', $data);
			}*/
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('roles', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Role Deleted Successfully!');
			echo 'success';
		}
		public function permissions($id){
			$roleid = $this->uri->segment('4');
			$data['all_menues'] =  $this->role_model->get_all_menues();
			//$data['all_permissions'] =  $this->role_model->get_menu_permissions();
			//$data['all_permissions'] =  $this->role_model->get_menu_permissions($roleid);
			//$data['all_permissions'] =  $this->role_model->get_all_permissions();
			$data['view'] = 'admin/roles/view_permissions';
			$this->load->view('admin/layout', $data);
		}
		public function edit_role_permissions(){
			$change_status   = $_POST['change_status'];
            $role_id         = $_POST['role_id'];
            $menu_id     = $_POST['menu_id'];
            $permission_id   = $_POST['permission_id'];
			if($change_status==1){
					$data = array(
						'role_id' => $role_id,
						'menu_id' => $menu_id,
						'permission_id' => $permission_id,
					);
					$data = $this->security->xss_clean($data);
					$result = $this->role_model->add_role_permission($data);
			}else{
				$this->db->delete('roles_permissions', array('role_id' => $role_id,'menu_id' => $menu_id,'permission_id' => $permission_id));
			}
			
		}
		public function get_menu_mapped_permissions(){
			
			echo json_encode($this->role_model->get_menu_mapped_permissions());
		}
		public function map()
		{
			if($this->input->post('submit')){
				//$role_id         = $_POST['role_id'];
				$menu_id     = $_POST['menu_id'];
				$permission_id   = $_POST['permission_id'];
				
				//$count = $this->db->get_where('menus_permissions',array('role_id'=>$role_id,'menu_id'=>$menu_id))->num_rows();
				$count = $this->db->get_where('menus_permissions',array('menu_id'=>$menu_id))->num_rows();
				if($count>0){
					//$this->db->delete('menus_permissions', array('role_id'=>$role_id,'menu_id'=>$menu_id));
					$this->db->delete('menus_permissions', array('menu_id'=>$menu_id));
					$this->db->delete('roles_permissions', array('menu_id'=>$menu_id));
				}
				foreach($_POST["permission_id"] as $row)
				{
					//$this->db->insert('menus_permissions',array('role_id'=>$role_id,'menu_id'=>$menu_id,'permission_id'=>$row));
					$this->db->insert('menus_permissions',array('menu_id'=>$menu_id,'permission_id'=>$row));
				}
				$this->session->set_flashdata('success_msg', 'Menus and Permissions Mapped Successfully!');
				redirect(base_url('admin/roles'));
			}
			else
			{
				$data['all_menues'] =  $this->role_model->get_all_menues();
				$data['all_permissions'] =  $this->role_model->get_all_permissions();
				$data['view'] = 'admin/roles/map_menu_permissions';
				$this->load->view('admin/layout', $data);
			}
		}
		function update_status()
		{
			$role_id = $_POST['role_id'];
			$id         = $_POST['id'];
			return $this->role_model->update_status($role_id, $id);
		}
		/*public function fetch_rname()
		{
			$rname= $this->input->post('role_name');
			$role = $this->role_model->get_role_by_name($rname);
			//return $data;
			if($role['cnt'] > 0){
			$response = "<span style='color: red;'>Role Name already exists.</span>";
			}
			else
			{
			$response = ""; 
			}
			echo $response;
		}*/
		public function fetch_rname()
		{
			$role = $this->input->post('role_name');
			$result   = $this->role_model->get_role_by_name($role);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function fetch_rname_edit()
		{
			$role = $this->input->post('role_name');
			$id = $this->input->post('rol_id');
			$result   = $this->user_model->get_role_by_name_edit($role,$id);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
	}


?>