<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Messages extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/messages_model', 'messages_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_enquires'] =  $this->messages_model->get_all_enquires();
			$data['all_messages'] =  $this->messages_model->get_all_messages();
			$data['view'] = 'admin/messages/all_messages';
			$this->load->view('admin/layout', $data);
		}
		public function view_message($id){
			$data['message_view'] =  $this->messages_model->get_messages_by_id($id);	
			$data['reply_message_view'] =  $this->messages_model->get_reply_messages_by_id($id);	
			$data['view'] = 'admin/messages/view_message';
			$this->load->view('admin/layout', $data);
		}
		public function send_reply()
    	{
        $query = $this->messages_model->send_reply();
        echo $query;
   		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('messages', array('message_id' => $id));
			$this->session->set_flashdata('danger_msg', 'Message is Deleted Successfully!');
			echo 'success';
		}
		public function filter_messages()
		{			
			$rrr=$this->messages_model->get_all_messages1();
			$output = array('messages_list'   => $rrr);
			echo json_encode($output, true);
		}
		public function filter_enquires()
		{			
			$rrr=$this->messages_model->get_all_enquires1();
			$output = array('enquires_list'   => $rrr);
			echo json_encode($output, true);
		}
		function update_status()
		{
			$message_id = $_POST['message_id'];
			$id         = $_POST['id'];
			return $this->messages_model->update_status($message_id, $id);
		}
	}


?>