<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Slides extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/slides_model', 'slides_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_slides'] =  $this->slides_model->get_all_slides();
			$data['view'] = 'admin/slides/all_slides';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){				
				$title = $_POST['title'];
				$subtitle = $_POST['subtitle'];	

				$config['upload_path'] = 'images/slides/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/slides/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 400;
				$config['height'] = 100;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				
				$config2['upload_path'] = 'images/slides/';
				$config2['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config2['max_size'] = '1000';
				$config2['encrypt_name'] = true;
				$this->load->library('upload', $config2);                                                
                $this->upload->initialize($config2);				
				$this->upload->do_upload('bimage');
				$file_data2 = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config2['source_image'] = $file_data2['full_path'];
				$config2['new_image'] = 'images/slides/thumb/';
				$config2['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = false;
				$config2['thumb_marker'] = '';
				$config2['width'] = 400;
				$config2['height'] = 100;
				$this->load->library('image_lib', $config2);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$fname2 = $file_data2['file_name'];
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$data = array(
					'slides_id' => 'SLI'.$random_id,
					'title' =>  $title,
					'subtitle' => $subtitle,
					'image' => $fname,
					'before_image' => $fname2,
					'status' =>1,
					'created_date' => date('Y-m-d : h:m:s'),
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->slides_model->add_slides($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Slide Added Successfully!');
					redirect(base_url('admin/slides'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/slides/add_slides';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$name = $_FILES['image']['name'];
				$name2 = $_FILES['bimage']['name'];
				$id = $this->input->post('id');
				$sql = "select * from slider where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				$fileName2 = $row['before_image'];
				if($name!='') 
				{
					
					$path = "images/slides/" . $row['image'];
					$path1 = "images/slides/thumb" . $row['image'];

					$config['upload_path'] = 'images/slides/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/slides/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '400';
					$config['height'] = '100';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}
				if($name2!='') 
				{
					$config['upload_path'] = 'images/slides/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('bimage');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/slides/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '400';
					$config['height'] = '100';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName2 = $file_data['file_name'];	               
				}
				$title = $_POST['title'];
				$subtitle = $_POST['subtitle'];

				$data = array(
				'title' => $title,
				'subtitle' => $subtitle,
				'image' => $fileName,
				'before_image' => $fileName2,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->slides_model->edit_slides($data, $id);
				if($result){
				$this->session->set_flashdata('success_msg', 'Slide Updated Successfully!');
				redirect(base_url('admin/slides'));
				}
			}
			else{
				$data['slides'] = $this->slides_model->get_slides_by_id($id);
				$data['view'] = 'admin/slides/edit_slides';
				$this->load->view('admin/layout', $data);
				}
			}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('slider', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Slide is Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$slides_id = $_POST['slides_id'];
			$id         = $_POST['id'];
			return $this->slides_model->update_status($slides_id, $id);
		}
	}


?>