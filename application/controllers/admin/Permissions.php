<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Permissions extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/permission_model', 'permission_model');
		}

		public function index(){
			$data['all_clients'] =  $this->permission_model->get_all_permissions();
			$data['view'] = 'admin/roles/view_permissions';
			$this->load->view('admin/layout', $data);
		}

	}


?>