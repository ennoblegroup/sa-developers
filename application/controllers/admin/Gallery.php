<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Gallery extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/gallery_types_model', 'gallery_types_model');
			$this->load->model('admin/gallery_model', 'gallery_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/vendor_model', 'vendor_model');
			$this->load->model('state_model', 'state_model');
		}
		
		public function index(){
			$data['all_gallery_types'] =  $this->gallery_types_model->get_all_gallery_types();
			$data['all_gallery'] =  $this->gallery_model->get_all_gallery();
			$data['view'] = 'admin/gallery/all_galleries';
			$this->load->view('admin/layout', $data);
		}
		public function project_gallery_upload(){
			$projectid= $this->input->post('project_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'images/project_gallery'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				$config['encrypt_name'] = TRUE; 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){
					$fileData = $this->upload->data();
					$filename= $fileData['file_name'];
					$file_ext = pathinfo($filename,PATHINFO_EXTENSION);
					if($file_ext=='png' || $file_ext=='jpg' || $file_ext=='jpeg' || $file_ext=='gif'){
						$file_type= '0';
					}else{
						$file_type= '1';
					}
					$uploadData['project_id'] = $projectid;
					$uploadData['file'] = $fileData['file_name'];
					$uploadData['file_type'] = $file_type;
					
					$this->db->insert('gallery', $uploadData);
					$this->session->set_flashdata('success_msg', 'Images/Videos Added Successfully!');
				} 
			} 
		}
		public function get_gallery_by_id(){
			$id =$this->input->post('id');
			$rrr= $this->gallery_model->get_gallery_by_id($id);
			echo json_encode($rrr);
		}
		public function add(){
			if($this->input->post('submit')){
				$length = 7;
				$characters = '0123456789';
				$random_id = "";
				for ($i = 0; $length > $i; $i++) {
				$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
				}
				$config['upload_path'] = 'images/material_gallery/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('gallery_image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/material_gallery/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['height'] = 200;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];

				if (!empty($fname)) {
					$data['image'] = $fname;
				}
				$gallery_name =$this->input->post('gallery_name');
				$gallery_description = $this->input->post('gallery_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'gallery_name' => $gallery_name,
					'gallery_description' => $gallery_description,
					'file' => $fname,
					'status'=>1,
					'created_date' => date('Y-m-d : h:m:s'),
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->gallery_model->add_gallery($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'gallery Added Successfully!');
					redirect(base_url('admin/gallery'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/gallery/add_gallery';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id =$this->input->post('gallery_id');
				$name = $_FILES['gallery_image']['name'];
				$sql = "select * from gallery where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fname = $row['file'];
				if($name!='') 
				{	
					$path = "images/gallery/" . $row['file'];
					$path1 = "images/gallery/thumb" . $row['file'];

					$config['upload_path'] = 'images/material_gallery/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                
					$this->upload->initialize($config);				
					$this->upload->do_upload('gallery_image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/material_gallery/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';
					$config['width'] = 260;
					$config['height'] = 250;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$fname = $file_data['file_name'];	               
				}
				$gallery_name =$this->input->post('gallery_name');
				$gallery_description = $this->input->post('gallery_description');
				//$file_source = $this->input->post('file_source');
				$data = array(
					'gallery_name' => $gallery_name,
					'gallery_description' => $gallery_description,
					'file' => $fname,
					'status'=>1,
					'updated_date' => date('Y-m-d : h:m:s'),
				);
				$data = $this->security->xss_clean($data);
				$result = $this->gallery_model->edit_gallery($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'gallery Updated Successfully!');
					redirect(base_url('admin/gallery/'));
				}
			}
			else{
				$data['view'] = 'admin/gallery/edit_gallery';
				$this->load->view('admin/layout', $data);
			}
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('material_gallery', array('id' => $id));
			$this->db->delete('materials', array('gallery_id' => $id));
			$this->session->set_flashdata('danger_msg', 'gallery Deleted Successfully!');
			echo 'success';
		}
	}


?>