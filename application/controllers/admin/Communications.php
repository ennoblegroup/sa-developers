<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Communications extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/service_model', 'service_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/communication_model', 'communication_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_communications'] =$this->communication_model->get_all_communications();
			foreach ($data['all_communications'] as $demo_time) {
                $data['time_ago'][] = $this->timeago($demo_time['created_date']);
            }
			$data['view'] = 'admin/communications/all_communications';
			$this->load->view('admin/layout', $data);
		}
			public function files_upload(){
				/*if(!empty($_FILES)){     
					$uploadDir = "uploads/";
					$fileName = $_FILES['file']['name'];
					$uploadedFile = $uploadDir.$fileName;    
					if(move_uploaded_file($_FILES['file']['tmp_name'],$uploadedFile)) {
						$data = array(
							'file_name' => $fileName,
						);
						$data = $this->security->xss_clean($data);
						$this->db->insert('communications_files', $data);						
						//$mysqlInsert = "INSERT INTO uploads (file_name, upload_time)VALUES('".$fileName."','".date("Y-m-d H:i:s")."')";
						//mysqli_query($conn, $mysqlInsert);
					}   
				}*/
				
				$target_dir = "uploads/"; // Upload directory

				$request = 1;
				if(isset($_POST['request'])){
					$request = $_POST['request'];
				}

				// Upload file
				if($request == 1){
					$target_file = $target_dir . basename($_FILES["file"]["name"]);
					$fileName = $_FILES['file']['name'];
					$msg = "";
					if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir.$_FILES['file']['name'])) {
						$msg = "Successfully uploaded";
						$random_id      = $this->input->post('random_id');
						$data = array(
							'file_name' => $fileName,
							'communication_id' => 'COM'.$random_id,
						);
						$data = $this->security->xss_clean($data);
						$this->db->insert('communications_files', $data);
						
					}else{
						$msg = "Error while uploading";
					}
					echo $msg;
				}

				// Remove file
				if($request == 2){
					$filename = $target_dir.$_POST['name'];
					$dfileName=$_POST['name'];	
					$this->db->delete('communications_files', array('file_name' => $dfileName));
					unlink($filename); exit;
				}
				
			}
			public function images_upload(){

              $accepted_origins = array("http://localhost");

              $imageFolder = "tinyimages/";
            
              reset ($_FILES);
              $temp = current($_FILES);
              if (is_uploaded_file($temp['tmp_name'])){
                if (isset($_SERVER['HTTP_ORIGIN'])) {
                  // same-origin requests won't set an origin. If the origin is set, it must be valid.
                  if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                  } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                  }
                }

                if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                    header("HTTP/1.1 400 Invalid file name.");
                    return;
                }
            
                // Verify extension
                if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
                    header("HTTP/1.1 400 Invalid extension.");
                    return;
                }
            
                // Accept upload if there was no origin, or if it is an accepted origin
                
                $filetowrite = $imageFolder . $temp['name'];
                move_uploaded_file($temp['tmp_name'], $filetowrite);
            
                // Respond to the successful upload with JSON.
                // Use a location key to specify the path to the saved image resource.
                // { location : '/your/uploaded/image/file'}
                
                echo json_encode(array('location' => base_url().$filetowrite));
              } else {
                // Notify editor that the upload failed
                header("HTTP/1.1 500 Server Error");
              }
		}
		public function dragDropUpload(){
			$conversation_id= $this->input->post('conversation_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();					
					$uploadData['communication_id'] = $conversation_id;
					$uploadData['file_name'] = $fileData['file_name']; 
					$uploadData['upload_time'] = date("Y-m-d H:i:s"); 
					$this->db->insert('communications_files', $uploadData);
				} 
			} 
		}
		public function add_conversation(){
			$users      = $this->input->post('users_list');
			$mail_subject = $this->input->post('subject');
			$mail_message = $this->input->post('message');
			$random_id      = $this->input->post('random_id');
			foreach($users as $key=>$user) { 
			$from_user_id=$_SESSION['sadevelopers_admin']['admin_id'];
			$to_user_id=$user;
			$mail_to = $this->communication_model->get_user_by_id($user);
			$mail_to=$mail_to['email'];
			//$mail_from    = $this->session->userdata("email");
			$mail_from    = 'admin@admin.com';
			
			  	$users      = $this->input->post('users_list');
				$mail_subject = $this->input->post('subject');
				$mail_message = $this->input->post('message');
				$random_id      = $this->input->post('random_id');
				foreach($users as $key=>$user) { 
				$from_user_id=$_SESSION['sadevelopers_admin']['admin_id'];
				$to_user_id=$user;
				$mail_to = $this->communication_model->get_user_by_id($user);
				$mail_to=$mail_to['email'];
				//$mail_from    = $this->session->userdata("email");
				$mail_from    = 'admin@admin.com';
				
				$read_status = 1;
				$data = array(
							'communication_id' => 'COM'.$random_id,
							'from_user_id' => $from_user_id,
							'to_user_id' => $to_user_id,
							'mail_to' => $mail_to,
							'mail_from' => $mail_from,
							'mail_subject' => $mail_subject,
							'mail_message' => $mail_message,
							'read_status' => $read_status,
							//'created_date' => date('Y-m-d : h:m:s'),
						);						
						$data = $this->security->xss_clean($data);						
						$conversation_status = $this->communication_model->submit_message($data);
						echo json_encode($conversation_status);						
				}
		}
		}
		public function add(){				
			$data['users'] =$this->communication_model->get_users();				
			$data['view'] = 'admin/communications/add_communication';
			$this->load->view('admin/layout', $data);
		}
		public function view_communication($id)
		{			
			$data['view_message']  = $this->communication_model->get_message($id);
			$data['reply_message'] = $this->communication_model->get_reply_message($id);
			$this->communication_model->change_read_status($id);				
			$data['view'] = 'admin/communications/view_communication';
			$this->load->view('admin/layout', $data);			
		}
		public function reply_message($id)
		{						
			if($this->input->post('reply')){
				$random_id      = $this->input->post('random_id');
				$communication_id      = $this->input->post('communication_id');
				$from_user_id=$_SESSION['sadevelopers_admin']['admin_id'];
				$reply_from    = 'admin@admin.com';
				$reply_message = $this->input->post('reply_message');
				$data = array(
					'reply_id' => 'COM'.$random_id,
					'communication_id' => $communication_id,
					'user_id' => $from_user_id,
					'reply_from' => $reply_from,
					'reply_message' => $reply_message,
					'read_status' => 1,
				);
				$data = $this->security->xss_clean($data);
				$result = $this->communication_model->reply_message($data);
				$this->session->set_flashdata('success_msg', 'Reply Message Sent Successfully!');
				redirect(base_url('admin/communications'));
			}
			else{
				$data['view_message']  = $this->communication_model->get_message($id);
				$data['reply_message'] = $this->communication_model->get_reply_message($id);
				$data['communication_id']=$id;
				$data['view'] = 'admin/communications/view_communication';
				$this->load->view('admin/layout', $data);
			}
		}
		function timeago($time, $tense = 'ago')
		{
			
			// Declare and define two dates
			$date1 = strtotime($time);
			//  $date2 = strtotime("2019-06-21 22:45:00");
			$datee = new DateTime();
			$date2 = strtotime($datee->format('Y-m-d H:i:s'));
			
			// Formulate the Difference between two dates
			$diff = abs($date2 - $date1);
			
			// To get the year divide the resultant date into
			// total seconds in a year (365*60*60*24)
			$years = floor($diff / (365 * 60 * 60 * 24));
			
			// To get the month, subtract it with years and
			// divide the resultant date into
			// total seconds in a month (30*60*60*24)
			$months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
			
			// To get the day, subtract it with years and
			// months and divide the resultant date into
			// total seconds in a days (60*60*24)
			$days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
			
			// To get the hour, subtract it with years,
			// months & seconds and divide the resultant
			// date into total seconds in a hours (60*60)
			$hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
			
			// To get the minutes, subtract it with years,
			// months, seconds and hours and divide the
			// resultant date into total seconds i.e. 60
			$minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
			
			// To get the minutes, subtract it with years,
			// months, seconds, hours and minutes
			$seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
			
			if ($years > 0) {
				return $years . " years ago";
			} else if ($months > 0) {
				return $months . " months ago";
			} else if ($days > 0) {
				return $days . " days ago";
			} else if ($hours > 0) {
				return $hours . " hours ago";
			} else if ($minutes > 0) {
				return $minutes . " minutes ago";
			} else if ($seconds > 0) {
				return $seconds . " seconds ago";
			} else {
				return "just now";
			}
			
		}
		
	}


?>