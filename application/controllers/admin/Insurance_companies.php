<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Insurance_companies extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index(){
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies();
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/insurance_companies/all_insurance_companies';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			if($this->input->post('submit')){
				/*$this->form_validation->set_rules('insurance_company_name', 'client Name', 'trim|required');
				$this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
				$this->form_validation->set_rules('city', 'City', 'trim|required');
				$this->form_validation->set_rules('state', 'State', 'trim|required');
				$this->form_validation->set_rules('zip_code', 'Zip Code', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['all_states'] =  $this->state_model->get_all_states();
					$data['view'] = 'admin/insurance_companies/add_insurance_company';
					$this->load->view('admin/layout', $data);
				}
				else{*/
					$insurance_company_name=$this->input->post('insurance_company_name');
					//$icname=substr($insurance_company_name,0,3);
					//$ficname= strtoupper($icname);
					$length = 7;
					$characters = '0123456789';
					$random_id = "";
					for ($i = 0; $length > $i; $i++) {
					$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
					}
					$data = array(
						//'insurance_company_id' => 'INS'.$ficname.$random_id,
						'insurance_company_id' => 'INS'.$random_id,
						'insurance_company_name' => $insurance_company_name,
						'ein_number' => $this->input->post('ein_number'),
						'naic_number' => $this->input->post('naic_number'),
						'naic_group_code' => $this->input->post('naic_group_code'),
						'state_of_domicile' => $this->input->post('state_of_domicile'),
						'company_type' => $this->input->post('company_type'),
						'company_status' => $this->input->post('company_status'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'phone_number' => $this->input->post('phone_number'),
						'email_address' => $this->input->post('email_address'),
						'status' =>1,
						'created_date' => date('Y-m-d : h:m:s'),
						'updated_date' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->insurance_company_model->add_insurance_company($data);
					if($result){
						$this->session->set_flashdata('success_msg', 'Insurance Company Added Successfully!');
						redirect(base_url('admin/insurance_companies'));
					}
				//}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/insurance_companies/add_insurance_company';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
					$id= $this->input->post('id');
					$data = array(
						//'insurance_companytype' => $this->input->post('insurance_companytype'),
						'insurance_company_name' => $this->input->post('insurance_company_name'),
						'ein_number' => $this->input->post('ein_number'),
						'naic_number' => $this->input->post('naic_number'),
						'naic_group_code' => $this->input->post('naic_group_code'),
						'state_of_domicile' => $this->input->post('state_of_domicile'),
						'company_type' => $this->input->post('company_type'),
						'company_status' => $this->input->post('company_status'),
						'address1' => $this->input->post('address1'),
						'address2' => $this->input->post('address2'),
						'city' => $this->input->post('city'),
						'state' => $this->input->post('state'),
						'zip_code' => $this->input->post('zip_code'),
						'phone_number' => $this->input->post('phone_number'),
						'email_address' => $this->input->post('email_address'),
						//'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);	
					$result = $this->insurance_company_model->edit_insurance_company($data, $id);					
					if($result){
						$this->session->set_flashdata('success_msg', 'Insurance Company Updated Successfully!');
						redirect(base_url('admin/insurance_companies'));
					}
				//}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['insurancecompany'] = $this->insurance_company_model->get_insurance_company_by_id($id);
				$data['view'] = 'admin/insurance_companies/edit_insurance_company';
				$this->load->view('admin/layout', $data);
			}
		}
		public function ttt(){
			$id = $_POST['inscmp_id'];
			$rrr= $this->insurance_company_model->get_insurance_company_by_id($id);
			echo json_encode($rrr, true);
		}
		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('insurance_companies', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Insurance Company Deleted Successfully!');
			echo 'success';
		}
		public function fetch_email()
		{
			$email = $this->input->post('email_address');
			$result   = $this->insurance_company_model->get_insurance_company_by_email($email);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		public function fetch_email_edit()
		{
			$email = $this->input->post('email');
			$id = $this->input->post('ins_id');
			$result   = $this->insurance_company_model->get_insurance_company_by_email_edit($email,$id);
			if ($result) {
				echo "false";
			} else {
				echo "true";
			}
		}
		function update_status()
		{
			$inscmp_id = $_POST['inscmp_id'];
			$id         = $_POST['id'];
			return $this->insurance_company_model->update_status($inscmp_id, $id);
		}
		public function filter_insurances()
		{			
			$rrr=$this->insurance_company_model->get_all_insurance_companies1();
			$output = array('insurances_list'   => $rrr);
			echo json_encode($output, true);
		}
	}


?>