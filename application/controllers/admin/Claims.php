<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Claims extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/client_model', 'client_model');
			$this->load->model('admin/insurance_company_model', 'insurance_company_model');
			$this->load->model('admin/communication_model', 'communication_model');
			$this->load->model('admin/claim_model', 'claim_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('state_model', 'state_model');
		}

		public function index($client_id = NULL){
			$data['client_id'] = $client_id;
			$data['all_clients'] =  $this->client_model->get_all_clients();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies();
			$data['all_claims'] =  $this->claim_model->get_all_claims($client_id);
			$data['all_claim_status_history'] =  $this->claim_model->get_all_claim_status_history();
			$con['conditions'] = array(
				
			);
			$data['all_claim_status'] =  $this->claim_model->get_all_claim_status($con);
			$data['view'] = 'admin/claims/all_claims';
			$this->load->view('admin/layout', $data);
		}
		public function ready_to_submit($client_id = NULL){
			$data['client_id'] = $client_id;
			$data['all_clients'] =  $this->client_model->get_all_clients();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies();
			$data['all_claims'] =  $this->claim_model->get_all_claims($client_id);
			$data['all_claim_status_history'] =  $this->claim_model->get_all_claim_status_history();
			$con['conditions'] = array(
				
			);
			$data['all_claim_status'] =  $this->claim_model->get_all_claim_status($con);
			$data['view'] = 'admin/claims/ready_to_submit';
			$this->load->view('admin/layout', $data);
		}
		public function conversation($id){
			$data['claim_id']=$id;
			$query=$this->db->query("SELECT * FROM claim_conversation WHERE claim_id = $id");
			$data1   = $query->result_array();
			foreach( $data1 as $key=>$each ){
				$idd= $each['id'];
				$data1[$key]   = $this->db->query("SELECT * FROM claim_conversation cc, users u WHERE cc.user_id=u.id and   cc.id =$idd")->row_array();
				$data1[$key]['attachments']   = $this->db->query("SELECT * FROM claim_conversation_files WHERE claim_conversation_id = $idd")->result_array();
				$data1[$key]['time']   = $this->time_ago($each['create_date']); 
			}
			
			$data['conversation']=array();
			$data['conversation']= $data1;
			$data['view'] = 'admin/claims/claim_conversation';
			$this->load->view('admin/layout', $data);
		}
		public function time_ago($time=false, $just_now=false) {
			if ($time instanceOf DateTime)
				$time = $time->getTimestamp();
			elseif (is_numeric($time))
				$time = date('m/d/y h:i A', $time);
			if (strtotime($time) === false)
				$time = date('m/d/y h:i A', time());
			$interval =  date_create($time)->diff(date_create('now'));
			$adjective = strtotime($time) > time() ? 'from now' : 'ago';
			return (
				$interval->days > 0 ? 
					$time : (
						$interval->h < 1  && $interval->i < 1 && $just_now ? 
							'just now' : 
							(
								$interval->h > 1 ? 
									$interval->h.' hour'.(
										$interval->h > 1 ? 
											's' : 
											''
									).' ago' : 
									$interval->i.' minutes'.' '.$adjective
							)
					)
			);
		}
		public function get_all_claim_status()
		{
			$claim_status = array();
			$group_id   = $this->input->post('id');
			if ($group_id) {
				$con['conditions'] = array(
					'claim_status_group' => $group_id
				);
				$claim_status     = $this->claim_model->get_all_claim_status($con);
			}
			echo json_encode($claim_status);
		}
		public function add_claim_conversation()
		{
			$message= $this->input->post('message');
			$claim_id   = $this->input->post('claim_id');
			$conversation_data = array(
				'user_id' => $_SESSION['sadevelopers_admin']['admin_id'],
				'claim_id' => $claim_id,
				'description' =>$message,
				'create_date' => date("Y-m-d H:i:s"),
			);
			$conversation_status     = $this->claim_model->add_claim_conversation($conversation_data);
			echo json_encode($conversation_status);
		}
		public function dragDropUpload(){
			$claim_conversation_id= $this->input->post('claim_conversation_id');
			if(!empty($_FILES)){ 
				// File upload configuration 
				$uploadPath = 'uploads/'; 
				$config['upload_path'] = $uploadPath; 
				$config['allowed_types'] = '*'; 
				 
				// Load and initialize upload library 
				$this->load->library('upload', $config); 
				$this->upload->initialize($config); 
				 
				// Upload file to the server 
				if($this->upload->do_upload('file')){ 
					$fileData = $this->upload->data();
					$uploadData['claim_conversation_id'] = $claim_conversation_id;
					$uploadData['file_name'] = $fileData['file_name']; 
					$uploadData['create_date'] = date("Y-m-d H:i:s"); 

					$this->db->insert('claim_conversation_files', $uploadData);
				} 
			} 
		}
		public function drafts(){
			$data['all_clients'] =  $this->client_model->get_all_clients();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies();
			$data['all_claims'] =  $this->claim_model->drafts();
			$data['all_claim_status_history'] =  $this->claim_model->get_all_claim_status_history();
			//$data['all_claim_status'] =  $this->claim_model->get_all_claim_status();
			$data['view'] = 'admin/claims/drafts';
			$this->load->view('admin/layout', $data);
		}
		public function get_claims_by_clientid($client_id=NULL){
			$data['client_id'] = $client_id;
			$data['all_clients'] =  $this->client_model->get_all_clients();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies();
			$data['all_claims'] =  $this->claim_model->get_all_claims($client_id);
			$data['all_claim_status_history'] =  $this->claim_model->get_all_claim_status_history();
			//$data['all_claim_status'] =  $this->claim_model->get_all_claim_status();
			$data['view'] = 'admin/claims/all_claims';
			$this->load->view('admin/layout', $data);
		}
		public function export(){
			header('Content-Type: application/vnd.ms-excel');  
			header('Content-disposition: attachment; filename='.rand().'.xls');  
			echo $_GET["data"]; 
		}
		public function reports($id){
			$data['all_reports'] =  $this->claim_model->get_all_reports($id);
			$data['view'] = 'admin/claims/reports';
			$this->load->view('admin/layout', $data);
		}
		public function add(){
			$data['all_clients'] =  $this->client_model->get_all_clients_claim();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies_claim();
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/claims/add_claim';
			$this->load->view('admin/layout', $data);			
		}
		public function edit($id){
			$data['all_clients'] =  $this->client_model->get_all_clients_claim();
			$data['all_insurance_companies'] =  $this->insurance_company_model->get_all_insurance_companies_claim();
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['claim_data'] =  $this->claim_model->get_claim_by_id($id);
			$data['claim_additional_data'] =  $this->claim_model->get_claim_additional_information_by_id($id);
			$data['claim_diagnosis_data'] =  $this->claim_model->get_claim_diagnosis_information_by_id($id);
			$data['insured_checks'] =  $this->claim_model->get_claim_insured_checks_by_id($id);
			//print_r($data);die;
			$data['view'] = 'admin/claims/edit_claim';
			$this->load->view('admin/layout', $data);			
		}
		public function view_claim($id){

			$data['claim_id'] =  $id;

			$data['view'] = 'admin/claims/view_claim';
			$this->load->view('admin/layout', $data);			
		}
		public function get_claim_by_id(){
			$id =  $this->input->post('id');
			$data   = array();
			$query=$this->claim_model->get_claim_by_id($id);
			$data   = $query;
			echo json_encode($data, true);		
		}
		public function add_claim(){
			$claim_type=$this->input->post('claim_type');
			$client_id=$this->input->post('client_id');	
			//$client_details=$this->client_model->get_client_by_id($client_id);
			//$client_lbn_name=$client_details[0]['client_lbn_name'];
			//$pname=substr($client_lbn_name,0,3);
			//$fpname= strtoupper($pname);
			$length = 7;
			$characters = '0123456789';
			$random_id = "";
			for ($i = 0; $length > $i; $i++) {
			$random_id .= $characters[mt_rand(0, strlen($characters) -1)];
			}
			if( $_SESSION['sadevelopers_admin']['client_id']>0){
				if($claim_type==0){
					$slaim_status=0;
				}else{
					$slaim_status=1;
				}
			}else{
				if($claim_type==0){
					$slaim_status=0;
				}else{
					$slaim_status=2;
				}
			}
			$claim_data = array(
				'user_id' => $_SESSION['sadevelopers_admin']['admin_id'],
				'claim_registration_id' => 'CLA'.$random_id,
				'client_id' =>$client_id,
				'insurance_company_id' => $this->input->post('insurance_company_id'),
				'status' => $slaim_status,
				'transaction_status' =>7,
				'payment_status' => 13,
				'created_date' => date('Y-m-d : h:m:s'),
				'updated_date' => date('Y-m-d : h:m:s'),
			);
			$claim_data = $this->security->xss_clean($claim_data);
			$result = $this->claim_model->add_claim($claim_data);
			$claim_id=$result;
			$claim_status = array(
				'claim_id' => $claim_id,
				'status_group' => 	1,
				'status_name' => 	$slaim_status,
				'status_description' => 	'',			
			);
			$rrr=$this->claim_model->add_claim_status_history($claim_status,$claim_id,$status_name,$status_group);
			$transaction_status = array(
				'claim_id' => $claim_id,
				'status_group' => 	2,
				'status_name' => 	6,
				'status_description' => '',			
			);
			$rrr=$this->claim_model->add_claim_status_history($transaction_status,$claim_id,$status_name,$status_group);
			$payment_status = array(
				'claim_id' => $claim_id,
				'status_group' => 	3,
				'status_name' => 	12,
				'status_description' => '',			
			);
			$rrr=$this->claim_model->add_claim_status_history($payment_status,$claim_id,$status_name,$status_group);
			
			if($this->input->post('nonworking_dates') !=''){
				$string = explode('-',$this->input->post('nonworking_dates'));
				$nonworking_start_date=date("Y-m-d", strtotime($string[0]));
				$nonworking_end_date=date("Y-m-d", strtotime($string[1]));
			}
			if($this->input->post('hospital_dates') !=''){	
				$string2 = explode('-',$this->input->post('hospital_dates'));
				$hospital_start_date=date("Y-m-d", strtotime($string2[0]));
				$hospital_end_date=date("Y-m-d", strtotime($string2[1]));
			}
			if($this->input->post('patient_dob') !=''){
				$patient_dob =date("Y-m-d", strtotime($this->input->post('patient_dob')));
			}else{
				$patient_dob = '';
			}
			if($this->input->post('injury_date') !=''){
				$injury_date =date("Y-m-d", strtotime($this->input->post('injury_date')));
			}else{
				$injury_date = '';
			}
			if($this->input->post('qual_date') !=''){
				$qual_date =date("Y-m-d", strtotime($this->input->post('qual_date')));
			}else{
				$qual_date = '';
			}
		
			$patient_data = array(
				'claim_id' => $claim_id,
				'last_name' => $this->input->post('last_name'),
				'first_name' => $this->input->post('first_name'),
				'middle_name' => $this->input->post('middle_name'),
				'patient_dob' => $patient_dob,
				'phone_number' => $this->input->post('phone_number'),
				'gender' => $this->input->post('gender'),
				'address1' => $this->input->post('address1'),
				'address2' => $this->input->post('address2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zip_code' => $this->input->post('zip_code'),
				'account_number' => $this->input->post('account_number'),
				'relation' => $this->input->post('relation'),
				'patient_signature' => $this->input->post('patient_signature'),
				'insured_signature' => $this->input->post('insured_signature'), 
			);
			$insurance_data = array(
				'claim_id' => $claim_id,
				'insured_check' => $this->input->post('insured_check'),
				'insured_id_number' => $this->input->post('insured_id_number'),
				'insured_last_name' => $this->input->post('insured_last_name'),
				'insured_first_name' => $this->input->post('insured_first_name'),
				'insured_middle_name' => $this->input->post('insured_middle_name'),
				'insured_phone_number' => $this->input->post('insured_phone_number'),
				'insured_address1' => $this->input->post('insured_address1'),
				'insured_address2' => $this->input->post('insured_address2'),
				'insured_city' => $this->input->post('insured_city'),
				'insured_state' => $this->input->post('insured_state'),
				'insured_zip_code' => $this->input->post('insured_zip_code'),
				'other_insured_last_name' => $this->input->post('other_insured_last_name'),
				'other_insured_first_name' => $this->input->post('other_insured_first_name'),
				'other_insured_middle_name' => $this->input->post('other_insured_middle_name'),
				'other_group_number' => $this->input->post('other_group_number'),
				'insured_group_number' => $this->input->post('insured_group_number'),
				'insurance_plan_name' => $this->input->post('insurance_plan_name'),
				'other_insurance_plan_name' => $this->input->post('other_insurance_plan_name'),
				'insured_gender' => $this->input->post('insured_gender'),
				'insured_dob' =>date("Y-m-d", strtotime($this->input->post('insured_gender'))),
				'iclaim_id' => $this->input->post('claim_id'),
				'other_benefit_plan' => $this->input->post('other_benefit_plan'), 
			);
			
			$physician_data = array(
				'claim_id' => $claim_id,
				'qualifier' => $this->input->post('qualifier'),
				'reffering_last_name' => $this->input->post('reffering_last_name'),
				'reffering_first_name' => $this->input->post('reffering_first_name'),
				'reffering_middle_name' => $this->input->post('reffering_middle_name'),
				'federal_tax_type' => $this->input->post('federal_tax_type'),
				'federal_tax_number' => $this->input->post('federal_tax_number'),
				'other_id' => $this->input->post('other_id'),
				'npi' => $this->input->post('npi'),
				'total_amount' => $this->input->post('total_amount'),
				'paid_amount' => $this->input->post('paid_amount'),
				'service_location_name' => $this->input->post('service_location_name'),
				'service_phone_number' => $this->input->post('service_phone_number'),
				'service_address1' => $this->input->post('service_address1'),
				'service_address2' => $this->input->post('service_address2'),
				'service_city' => $this->input->post('service_city'),
				'service_state' => $this->input->post('service_state'),
				'service_zip_code' => $this->input->post('service_zip_code'),
				'service_other_id' => $this->input->post('service_other_id'),
				'service_npi' => $this->input->post('service_npi'),
				'admin_location_name' => $this->input->post('admin_location_name'),
				'admin_phone_number' => $this->input->post('admin_phone_number'),
				'admin_address1' => $this->input->post('admin_address1'),
				'admin_address2' => $this->input->post('admin_address2'),
				'admin_city' => $this->input->post('admin_city'),
				'admin_state' => $this->input->post('admin_state'),
				'admin_zip_code' => $this->input->post('admin_zip_code'),
				'admin_other_id' => $this->input->post('admin_other_id'),
				'admin_npi' => $this->input->post('admin_npi'),
				'physician_signature' =>$this->input->post('physician_signature'),
			);
			$claim_data = array(
				'claim_id' => $claim_id,
				'employment' => $this->input->post('employment'),
				'auto_accident' => $this->input->post('auto_accident'),
				'other_accident' => $this->input->post('other_accident'),
				'accident_location' => $this->input->post('accident_location'),
				'claim_code' => $this->input->post('claim_code'),
				'iqual' => $this->input->post('iqual'),
				'oqual' => $this->input->post('oqual'),
				'injury_date' =>  $injury_date,
				'qual_date' =>  $qual_date,
				'nonworking_start_date' => $nonworking_start_date,
				'nonworking_end_date' => $nonworking_end_date,
				'hospital_start_date' => $hospital_start_date,
				'hospital_end_date' => $hospital_end_date,
				'additional_claim_information' => $this->input->post('additional_claim_information'),
				'out_lab' => $this->input->post('out_lab'),
				'total_charge' => $this->input->post('total_charge'),
				'icd' => $this->input->post('icd'),
				'resubmission_code' => $this->input->post('resubmission_code'),
				'original_reference_number' => $this->input->post('original_reference_number'),
				'prior_aurthorization_number' => $this->input->post('prior_aurthorization_number'),
			);
			$patient_data = $this->security->xss_clean($patient_data);
			$insurance_data = $this->security->xss_clean($insurance_data);
			$physician_data = $this->security->xss_clean($physician_data);
			$claim_data = $this->security->xss_clean($claim_data);
			$result1 = $this->claim_model->add_claim_information($patient_data,$insurance_data,$physician_data,$claim_data);
			
			$diagnosis_a = $this->input->post('diagnosis_a');
			$diagnosis_b = $this->input->post('diagnosis_b');
			$diagnosis_c = $this->input->post('diagnosis_c');
			$diagnosis_d = $this->input->post('diagnosis_d');
			$diagnosis_e = $this->input->post('diagnosis_e');
			$diagnosis_f = $this->input->post('diagnosis_f');
			$diagnosis_g = $this->input->post('diagnosis_g');
			$diagnosis_h = $this->input->post('diagnosis_h');
			$diagnosis_i = $this->input->post('diagnosis_i');
			$diagnosis_j = $this->input->post('diagnosis_j');
			$diagnosis_k = $this->input->post('diagnosis_k');
			$diagnosis_l = $this->input->post('diagnosis_l');
			$diagnosis_id = $this->input->post('diagnosis_id');
			$claim_diagnosis_data = array(
				'claim_id' => $claim_id,
				'diagnosis_a'=>$diagnosis_a,
				'diagnosis_b'=>$diagnosis_b,
				'diagnosis_c'=>$diagnosis_c,
				'diagnosis_d'=>$diagnosis_d,
				'diagnosis_e'=>$diagnosis_e,
				'diagnosis_f'=>$diagnosis_f,
				'diagnosis_g'=>$diagnosis_g,
				'diagnosis_h'=>$diagnosis_h,
				'diagnosis_i'=>$diagnosis_i,
				'diagnosis_j'=>$diagnosis_j,
				'diagnosis_k'=>$diagnosis_k,
				'diagnosis_l'=>$diagnosis_l,			
			);
			$claim_diagnosis_data = $this->security->xss_clean($claim_diagnosis_data);
			$result3 = $this->claim_model->add_claim_diagnosis_information($claim_diagnosis_data);

			$service_start_date = $this->input->post('service_start_date');
			$service_end_date = $this->input->post('service_end_date');
			$service_place = $this->input->post('service_place');
			$emg = $this->input->post('emg');
			$cpt_hcps = $this->input->post('cpt_hcps');
			$modifier = $this->input->post('modifier');
			$diagnosis_pointer = $this->input->post('diagnosis_pointer');
			$charges = $this->input->post('charges');
			$days = $this->input->post('days');
			$epsdt = $this->input->post('epsdt');
			$fplan = $this->input->post('fplan');
			$id_qual = $this->input->post('id_qual');
			$provider_id = $this->input->post('provider_id');
			$cnpi = $this->input->post('cnpi');
			for($j=0;$j<count($service_start_date);$j++){
				$claim_additional_data = array(
					'claim_id' => $claim_id,
					'service_start_date' => 	date("Y-m-d", strtotime($service_start_date[$j])),
					'service_end_date' => 	    date("Y-m-d", strtotime($service_end_date[$j])),
					'service_place' => 	    $service_place[$j],
					'emg' => 	    $emg[$j],
					'cpt_hcps' => 	    $cpt_hcps[$j],
					'modifier' => 	    $modifier[$j],
					'diagnosis_pointer' => 	    $diagnosis_pointer[$j],
					'charges' => 	    $charges[$j],
					'days' => 	    $days[$j],
					'epsdt' => 	    $epsdt[$j],
					'fplan' => 	    $fplan[$j],
					'id_qual' => 	    $id_qual[$j],
					'provider_id' => 	    $provider_id[$j],
					'cnpi' => 	    $cnpi[$j],				
				);
				$claim_additional_data = $this->security->xss_clean($claim_additional_data);
				$result4 = $this->claim_model->add_claim_additional_information($claim_additional_data);
			}
			
			if($result1 ){
				if($claim_type==0){
					$this->session->set_flashdata('success_msg', 'Claim saved as draft Successfully!');
					redirect(base_url('admin/claims/drafts'));
				}else{
					$this->session->set_flashdata('success_msg', 'Claim Added Successfully!');
					redirect(base_url('admin/claims'));
				}
				
			}
		}
		public function update_claim(){
			//print_r($_POST);die;
			$claim_type=$this->input->post('claim_type');
			$claim_id=$this->input->post('claimm_id');
			$csd=$this->input->post('status_id');
			if($csd==0){
				if( $_SESSION['sadevelopers_admin']['client_id']>0){
					if($claim_type==0){
						$csd=0;
					}else{
						$csd=1;
					}
				}else{
					if($claim_type==0){
						
						$csd=0;
					}else{
						$csd=2;
					}
				}
			}
			$claim_data = array(
				'client_id' => $this->input->post('client_id'),
				'insurance_company_id' => $this->input->post('insurance_company_id'),
				'status' => $csd,
				'updated_date' => date('Y-m-d : h:m:s'),
			);
			$claim_data = $this->security->xss_clean($claim_data);
			$result = $this->claim_model->update_claim($claim_data,$claim_id);

			if($this->input->post('nonworking_dates') !=''){
				$string = explode('-',$this->input->post('nonworking_dates'));
				$nonworking_start_date=date("Y-m-d", strtotime($string[0]));
				$nonworking_end_date=date("Y-m-d", strtotime($string[1]));
			}
			if($this->input->post('hospital_dates') !=''){	
				$string2 = explode('-',$this->input->post('hospital_dates'));
				$hospital_start_date=date("Y-m-d", strtotime($string2[0]));
				$hospital_end_date=date("Y-m-d", strtotime($string2[1]));
			}
			$patient_data = array(
				'last_name' => $this->input->post('last_name'),
				'first_name' => $this->input->post('first_name'),
				'middle_name' => $this->input->post('middle_name'),
				'patient_dob' => date("Y-m-d", strtotime($this->input->post('patient_dob'))),
				'phone_number' => $this->input->post('phone_number'),
				'gender' => $this->input->post('gender'),
				'address1' => $this->input->post('address1'),
				'address2' => $this->input->post('address2'),
				'city' => $this->input->post('city'),
				'state' => $this->input->post('state'),
				'zip_code' => $this->input->post('zip_code'),
				'account_number' => $this->input->post('account_number'),
				'relation' => $this->input->post('relation'),
				'patient_signature' => $this->input->post('patient_signature'),
				'insured_signature' => $this->input->post('insured_signature'), 
			);
			$insurance_data = array(
				'insured_check' => $this->input->post('insured_check'),
				'insured_id_number' => $this->input->post('insured_id_number'),
				'insured_last_name' => $this->input->post('insured_last_name'),
				'insured_first_name' => $this->input->post('insured_first_name'),
				'insured_middle_name' => $this->input->post('insured_middle_name'),
				'insured_phone_number' => $this->input->post('insured_phone_number'),
				'insured_address1' => $this->input->post('insured_address1'),
				'insured_address2' => $this->input->post('insured_address2'),
				'insured_city' => $this->input->post('insured_city'),
				'insured_state' => $this->input->post('insured_state'),
				'insured_zip_code' => $this->input->post('insured_zip_code'),
				'other_insured_last_name' => $this->input->post('other_insured_last_name'),
				'other_insured_first_name' => $this->input->post('other_insured_first_name'),
				'other_insured_middle_name' => $this->input->post('other_insured_middle_name'),
				'other_group_number' => $this->input->post('other_group_number'),
				'insured_group_number' => $this->input->post('insured_group_number'),
				'insurance_plan_name' => $this->input->post('insurance_plan_name'),
				'other_insurance_plan_name' => $this->input->post('other_insurance_plan_name'),
				'insured_gender' => $this->input->post('insured_gender'),
				'insured_dob' =>date("Y-m-d", strtotime($this->input->post('insured_gender'))),
				'iclaim_id' => $this->input->post('claim_id'),
				'other_benefit_plan' => $this->input->post('other_benefit_plan'), 
			);
			
			$physician_data = array(
				'qualifier' => $this->input->post('qualifier'),
				'reffering_last_name' => $this->input->post('reffering_last_name'),
				'reffering_first_name' => $this->input->post('reffering_first_name'),
				'reffering_middle_name' => $this->input->post('reffering_middle_name'),
				'federal_tax_type' => $this->input->post('federal_tax_type'),
				'federal_tax_number' => $this->input->post('federal_tax_number'),
				'other_id' => $this->input->post('other_id'),
				'npi' => $this->input->post('npi'),
				'total_amount' => $this->input->post('total_amount'),
				'paid_amount' => $this->input->post('paid_amount'),
				'service_location_name' => $this->input->post('service_location_name'),
				'service_phone_number' => $this->input->post('service_phone_number'),
				'service_address1' => $this->input->post('service_address1'),
				'service_address2' => $this->input->post('service_address2'),
				'service_city' => $this->input->post('service_city'),
				'service_state' => $this->input->post('service_state'),
				'service_zip_code' => $this->input->post('service_zip_code'),
				'service_other_id' => $this->input->post('service_other_id'),
				'service_npi' => $this->input->post('service_npi'),
				'admin_location_name' => $this->input->post('admin_location_name'),
				'admin_phone_number' => $this->input->post('admin_phone_number'),
				'admin_address1' => $this->input->post('admin_address1'),
				'admin_address2' => $this->input->post('admin_address2'),
				'admin_city' => $this->input->post('admin_city'),
				'admin_state' => $this->input->post('admin_state'),
				'admin_zip_code' => $this->input->post('admin_zip_code'),
				'admin_other_id' => $this->input->post('admin_other_id'),
				'admin_npi' => $this->input->post('admin_npi'),
				'physician_signature' =>$this->input->post('physician_signature'),
			);
			$claim_data = array(
				'employment' => $this->input->post('employment'),
				'auto_accident' => $this->input->post('auto_accident'),
				'other_accident' => $this->input->post('other_accident'),
				'accident_location' => $this->input->post('accident_location'),
				'claim_code' => $this->input->post('claim_code'),
				'iqual' => $this->input->post('iqual'),
				'oqual' => $this->input->post('oqual'),
				'injury_date' =>  date("Y-m-d", strtotime($this->input->post('injury_date'))),
				'qual_date' =>  date("Y-m-d", strtotime($this->input->post('qual_date'))),
				'nonworking_start_date' => $nonworking_start_date,
				'nonworking_end_date' => $nonworking_end_date,
				'hospital_start_date' => $hospital_start_date,
				'hospital_end_date' => $hospital_end_date,
				'additional_claim_information' => $this->input->post('additional_claim_information'),
				'out_lab' => $this->input->post('out_lab'),
				'total_charge' => $this->input->post('total_charge'),
				'icd' => $this->input->post('icd'),
				'resubmission_code' => $this->input->post('resubmission_code'),
				'original_reference_number' => $this->input->post('original_reference_number'),
				'prior_aurthorization_number' => $this->input->post('prior_aurthorization_number'),
			);
			$patient_data = $this->security->xss_clean($patient_data);
			$insurance_data = $this->security->xss_clean($insurance_data);
			$physician_data = $this->security->xss_clean($physician_data);
			$claim_data = $this->security->xss_clean($claim_data);
			$result1 = $this->claim_model->update_claim_information($patient_data,$insurance_data,$physician_data,$claim_data,$claim_id);
			
			
			$diagnosis_a = $this->input->post('diagnosis_a');
			$diagnosis_b = $this->input->post('diagnosis_b');
			$diagnosis_c = $this->input->post('diagnosis_c');
			$diagnosis_d = $this->input->post('diagnosis_d');
			$diagnosis_e = $this->input->post('diagnosis_e');
			$diagnosis_f = $this->input->post('diagnosis_f');
			$diagnosis_g = $this->input->post('diagnosis_g');
			$diagnosis_h = $this->input->post('diagnosis_h');
			$diagnosis_i = $this->input->post('diagnosis_i');
			$diagnosis_j = $this->input->post('diagnosis_j');
			$diagnosis_k = $this->input->post('diagnosis_k');
			$diagnosis_l = $this->input->post('diagnosis_l');
			$diagnosis_id = $this->input->post('diagnosis_id');
			$claim_diagnosis_data = array(
				'diagnosis_a'=>$diagnosis_a,
				'diagnosis_b'=>$diagnosis_b,
				'diagnosis_c'=>$diagnosis_c,
				'diagnosis_d'=>$diagnosis_d,
				'diagnosis_e'=>$diagnosis_e,
				'diagnosis_f'=>$diagnosis_f,
				'diagnosis_g'=>$diagnosis_g,
				'diagnosis_h'=>$diagnosis_h,
				'diagnosis_i'=>$diagnosis_i,
				'diagnosis_j'=>$diagnosis_j,
				'diagnosis_k'=>$diagnosis_k,
				'diagnosis_l'=>$diagnosis_l,			
			);
			$claim_diagnosis_data = $this->security->xss_clean($claim_diagnosis_data);
			$result3 = $this->claim_model->update_claim_diagnosis_information($claim_diagnosis_data,$claim_id);	

			$service_start_date = $this->input->post('service_start_date');
			$service_end_date = $this->input->post('service_end_date');
			$service_place = $this->input->post('service_place');
			$emg = $this->input->post('emg');
			$cpt_hcps = $this->input->post('cpt_hcps');
			$modifier = $this->input->post('modifier');
			$diagnosis_pointer = $this->input->post('diagnosis_pointer');
			$charges = $this->input->post('charges');
			$days = $this->input->post('days');
			$epsdt = $this->input->post('epsdt');
			$fplan = $this->input->post('fplan');
			$id_qual = $this->input->post('id_qual');
			$provider_id = $this->input->post('provider_id');
			$cnpi = $this->input->post('cnpi');
			$value_id = $this->input->post('value_id');
			for($j=0;$j<count($service_start_date);$j++){
				$claim_additional_data = array(
					'claim_id' => $claim_id,
					'service_start_date' => 	date("Y-m-d", strtotime($service_start_date[$j])),
					'service_end_date' => 	    date("Y-m-d", strtotime($service_end_date[$j])),
					'service_place' => 	    $service_place[$j],
					'emg' => 	    $emg[$j],
					'cpt_hcps' => 	    $cpt_hcps[$j],
					'modifier' => 	    $modifier[$j],
					'diagnosis_pointer' => 	    $diagnosis_pointer[$j],
					'charges' => 	    $charges[$j],
					'days' => 	    $days[$j],
					'epsdt' => 	    $epsdt[$j],
					'fplan' => 	    $fplan[$j],
					'id_qual' => 	    $id_qual[$j],
					'provider_id' => 	    $provider_id[$j],
					'cnpi' => 	    $cnpi[$j],				
				);
				if($value_id[$j] ==''){
					$claim_additional_data = $this->security->xss_clean($claim_additional_data);
					$result4 = $this->claim_model->add_claim_additional_information($claim_additional_data);
				}else if($value_id[$j] !=''){
					$claim_additional_data = $this->security->xss_clean($claim_additional_data);
					$result4 = $this->claim_model->update_claim_additional_information($claim_additional_data,$value_id[$j]);
				}
				
			}
			if($result1){
				if($this->input->post('status_id') >0){
					$this->session->set_flashdata('success_msg', 'Claim Updated Successfully!');
					redirect(base_url('admin/claims'));
				}else{
					$this->session->set_flashdata('success_msg', 'Draft Updated Successfully!');
					redirect(base_url('admin/claims/drafts'));
				}
			}
		}
		function update_claim_status()
		{
			$claim_id = $_POST['claim_id'];
			$status_id         = $_POST['status_id'];
			return $this->claim_model->update_claim_status($claim_id, $status_id);
		}
		function add_claim_status_history()
		{
			$claim_id = $_POST['claim_id'];
			$status_name         = $_POST['status_name'];
			$status_description         = $_POST['status_description'];
			$status_group         = $_POST['status_group'];
			$claim_status = array(
				'claim_id' => $claim_id,
				'status_group' => 	$status_group,
				'status_name' => 	$status_name,
				'status_description' => 	$status_description			
			);
			$rrr=$this->claim_model->add_claim_status_history($claim_status,$claim_id,$status_name,$status_group);
			if($rrr){
				$this->session->set_flashdata('success_msg', 'Status Updated Successfully');
				redirect(base_url('admin/claims'));
			}
		}
		function add_claim_status_history_pa()
		{
			$claim_id = $_POST['claim_id'];
			$status_name         = $_POST['status_name'];
			$status_description         = $_POST['status_description'];
			$status_group         = $_POST['status_group'];
			$claim_status = array(
				'claim_id' => $claim_id,
				'status_name' => 	$status_name,
				'status_description' => 	$status_description			
			);
			$rrr=$this->claim_model->add_claim_status_history($claim_status,$claim_id,$status_name,$status_group);
			return $rrr;
		}
		public function claim_submssion()
		{
			$id = $_POST['claim_id'];
			$report =  $this->claim_model->get_all_active_report($id);
			$claim_file= base_url().'admin_files/generated_claims/'.$report['file_name'];
			//$email_to= $report['email_address'];
			$email_to= 'raghuv@ennobletechnologies.com';
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 25,
				'smtp_user' => 'raghuv@ennobletechnologies.com', 
				'smtp_pass' => 'Raghu@1025', 
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			  );
		  	$this->load->library('email', $config);
			if($report['file_name'] !='' && $report['email_address'] !=''){
				$email_subject ='Claim Submission';
				$content="Claim Submission";			
				$data['name'] =$first_name." ". $last_name;
				$data['email_subject'] = $email_subject;
				
				$data['content'] = $content;
				$data['client_name'] = 'admin System';
				$temp =$this->load->view('email/common_email',$data,TRUE);
				$msg="$temp";
				$this->email->from('admin@admin.com');
				$this->email->to($email_to);
				$subject = $email_subject;
				$this->email->subject($subject);
				$this->email->set_mailtype('html');
				$this->email->message($msg);
				$this->email->attach($claim_file);
				$this->email->send();
				$status_id=9;
				$claim_status=$this->claim_model->update_claim_status($id, $status_id);
			}
			return true;
		}
		public function single_report($id){
			$claim_ids=array();
			array_push($claim_ids,$id);
			$claim_ids_count=count($claim_ids);
			for($i=0;$i<$claim_ids_count;$i++){
				$this->generate_report($claim_ids[$i]);
			}
			$this->session->set_flashdata('success_msg', 'PDF generated Successfully');
			redirect(base_url('admin/claims'));
		}
		public function multiple_report(){
			$claim_ids=$_POST['myarray'];
			$claim_ids_count=count($claim_ids);
			for($i=0;$i<$claim_ids_count;$i++){
				$this->generate_report($claim_ids[$i]);
			}
			$this->session->set_flashdata('success_msg', 'PDF"s generated Successfully');
			redirect(base_url('admin/claims'));
		}
		public function filter_claims(){
			
			$insurance_company_id=$_POST['insurance_company_id'];
			$client_id=$_POST['client_id'];
			$status_id=$_POST['status_id'];
			$dates=$_POST['dates'];
			if($dates !=''){
				$string = explode('-',$dates);
				$start_date=date("Y-m-d", strtotime($string[0]));
				$end_date=date("Y-m-d", strtotime($string[1]));
			}else{
				$start_date='';
				$end_date='';
			}
			if($insurance_company_id==''){
				$insurance_company_id=null;
			}
			if($client_id==''){
				$client_id=null;
			}
			if($status_id==''){
				$status_id=null;
			}
			$rrr=$this->claim_model->filter_claims($insurance_company_id,$client_id,$status_id,$start_date,$end_date);
			$output = array('claim_list'   => $rrr);
			echo json_encode($output, true);
		}
		public function filter_claims_rs(){
			
			$insurance_company_id=$_POST['insurance_company_id'];
			$client_id=$_POST['client_id'];
			$status_id=$_POST['status_id'];
			$dates=$_POST['dates'];
			if($dates !=''){
				$string = explode('-',$dates);
				$start_date=date("Y-m-d", strtotime($string[0]));
				$end_date=date("Y-m-d", strtotime($string[1]));
			}else{
				$start_date='';
				$end_date='';
			}
			if($insurance_company_id==''){
				$insurance_company_id=null;
			}
			if($client_id==''){
				$client_id=null;
			}
			if($status_id==''){
				$status_id=null;
			}
			$rrr=$this->claim_model->filter_claims_rs($insurance_company_id,$client_id,$status_id,$start_date,$end_date);
			$output = array('claim_list'   => $rrr);
			echo json_encode($output, true);
		}
		public function claim_status_history(){
			
			$id=$_POST['id'];
			$type=$_POST['type'];
			$rrr=$this->claim_model->claim_status_history($id,$type);
			$output = array('claim_list'   => $rrr);
			echo json_encode($output, true);
		}
		public function generate_report($id){
			require_once(APPPATH.'../pdf_generation/fpdm.php'); 
			$data['claim_data'] =  $this->claim_model->get_claim_by_id($id);
			$data['claim_additional_data'] =  $this->claim_model->get_claim_additional_information_by_id($id);
			$claim_additional_data_count= count($data['claim_additional_data']);
			$data['claim_diagnosis_data'] =  $this->claim_model->get_claim_diagnosis_information_by_id($id);
			$claim_diagnosis_data_count= count($data['claim_diagnosis_data']);

			$newfile = $data['claim_data']['claim_registration_id'].'_'.date('m-d-Y_H:i:s');
			$claim_id=$data['claim_data']['claimm_id'];
			$filename = str_replace(":", '', $newfile);
			
			if($data['claim_data']['phone_number']){
			$data['claim_data']['phone_number'] = str_replace(")", '', $data['claim_data']['phone_number']);
			$data['claim_data']['phone_number'] = str_replace("(", '', $data['claim_data']['phone_number']);
			}

			$patient_dob = explode('-',$data['claim_data']['patient_dob']);
			$patient_dob_yy=$patient_dob[0];
			$patient_dob_mm=$patient_dob[1];
			$patient_dob_dd=$patient_dob[2];

			$insured_dob = explode('-',$data['claim_data']['insured_dob']);
			$insured_dob_yy=$insured_dob[0];
			$insured_dob_mm=$insured_dob[1];
			$insured_dob_dd=$insured_dob[2];

			$injury_date = explode('-',$data['claim_data']['injury_date']);
			
			$injury_date_yy=$injury_date[0];
			$injury_date_mm=$injury_date[1];
			$injury_date_dd=$injury_date[2];

			$qual_date = explode('-',$data['claim_data']['qual_date']);
			$qual_date_yy=$qual_date[0];
			$qual_date_mm=$qual_date[1];
			$qual_date_dd=$qual_date[2];

			$nonworking_start_date = explode('-',$data['claim_data']['nonworking_start_date']);
			$nonworking_start_date_yy=$nonworking_start_date[0];
			$nonworking_start_date_mm=$nonworking_start_date[1];
			$nonworking_start_date_dd=$nonworking_start_date[2];

			$nonworking_end_date = explode('-',$data['claim_data']['nonworking_end_date']);
			$nonworking_end_date_yy=$nonworking_end_date[0];
			$nonworking_end_date_mm=$nonworking_end_date[1];
			$nonworking_end_date_dd=$nonworking_end_date[2];

			$hospital_start_date = explode('-',$data['claim_data']['hospital_start_date']);
			$hospital_start_date_yy=$hospital_start_date[0];
			$hospital_start_date_mm=$hospital_start_date[1];
			$hospital_start_date_dd=$hospital_start_date[2];

			$hospital_end_date = explode('-',$data['claim_data']['hospital_end_date']);
			$hospital_end_date_yy=$hospital_end_date[0];
			$hospital_end_date_mm=$hospital_end_date[1];
			$hospital_end_date_dd=$hospital_end_date[2];
			
			$phone_number = explode('(',$data['claim_data']['phone_number']);
			$phone_number1=substr($phone_number[0],3,6);
			$phone_number2=substr($phone_number[0],6,16);

			$insured_phone_number = explode('(',$data['claim_data']['insured_phone_number']);
			$insured_phone_number=substr($insured_phone_number[0],3,6);
			$insured_phone_number=substr($insured_phone_number[0],6,16);

			$service_phone_number = explode('(',$data['claim_data']['service_phone_number']);
			$service_phone_number1=substr($service_phone_number[0],3,6);
			$service_phone_number2=substr($service_phone_number[0],6,16);

			$admin_phone_number = explode('(',$data['claim_data']['admin_phone_number']);
			$admin_phone_number1=substr($admin_phone_number[0],3,6);
			$admin_phone_number2=substr($admin_phone_number[0],6,16);

			$fields = array(
			'insurance_name' => $data['claim_data']['insurance_company_name'],
			'insurance_address' => $data['claim_data']['address1'],
			'insurance_address2' => $data['claim_data']['address2'],
			'insurance_city_state_zip' => $data['claim_data']['insured_last_name'].' '.$data['claim_data']['insured_first_name'].' '.$data['insured_claim_data']['middle_name'],
			'insurance_id' => $data['claim_data']['insured_id_number'],
			'pt_name' => $data['claim_data']['last_name'].' '.$data['claim_data']['first_name'].' '.$data['claim_data']['middle_name'],
			'birth_mm' => $patient_dob_mm,
			'birth_dd' => $patient_dob_dd,
			'birth_yy' => $patient_dob_yy,
			'ins_name' => $data['claim_data']['insured_last_name'].' '.$data['claim_data']['insured_first_name'].' '.$data['insured_claim_data']['middle_name'],
			'pt_street' => $data['claim_data']['insured_address1'].' '.$data['claim_data']['insured_address2'],
			'ins_street' => $data['claim_data']['address1'].' '.$data['claim_data']['address2'],
			'pt_city' => $data['claim_data']['city'],
			'pt_state' => $data['claim_data']['state'],
			'ins_city' => $data['claim_data']['insured_city'],
			'ins_state' => $data['claim_data']['insured_state'],
			'pt_zip' => $data['claim_data']['insured_zip_code'],
			'pt_AreaCode' => $phone_number1,
			'pt_phone' => $phone_number2,
			'NUCC USE' => '',
			'ins_zip' => $data['claim_data']['zip_code'],
			'ins_phone area' => $insured_phone_number1,
			'ins_phone' => $insured_phone_number2,
			'other_ins_name' => $data['claim_data']['other_insured_last_name'].' '.$data['claim_data']['other_insured_first_name'].' '.$data['other_insured_claim_data']['middle_name'],
			'ins_policy' => $data['claim_data']['insured_group_number'],
			'other_ins_policy' => $data['claim_data']['other_group_number'],
			'ins_dob_mm' => $insured_dob_mm,
			'ins_dob_dd' => $insured_dob_dd,
			'ins_dob_yy' => $insured_dob_yy,
			'40' => '',
			'accident_place' => $data['claim_data']['accident_location'],
			'57' => substr($data['claim_data']['iclaim_id'], 0,2),
			'58' => substr($data['claim_data']['iclaim_id'], 2,30),
			'41' => '' ,
			'ins_plan_name' => $data['claim_data']['insurance_plan_name'],
			'other_ins_plan_name' => $data['claim_data']['other_insurance_plan_name'],
			'50' => $data['claim_data']['claim_code'],
			'pt_signature' => '',
			'pt_date' => '',
			'ins_signature' => '',
			'cur_ill_mm' => $injury_date_mm,
			'cur_ill_dd' => $injury_date_dd,
			'cur_ill_yy' => $injury_date_yy,
			'73' => $data['claim_data']['iqual'],
			'74' => $data['claim_data']['oqual'],
			'sim_ill_mm' => $qual_date_mm,
			'sim_ill_dd' => $qual_date_dd,
			'sim_ill_yy' => $qual_date_yy,
			'work_mm_from' => $nonworking_start_date_mm,
			'work_dd_from' => $nonworking_start_date_dd,
			'work_yy_from' => $nonworking_start_date_yy,
			'work_mm_end' => $nonworking_end_date_mm,
			'work_dd_end' => $nonworking_end_date_dd,
			'work_yy_end' => $nonworking_end_date_yy,
			'physician number 17a1' => substr($data['claim_data']['other_id'],0,2),
			'physician number 17a' => substr($data['claim_data']['other_id'],3,19),
			'85' => $data['claim_data']['qualifier'],
			'ref_physician' => $data['claim_data']['reffering_last_name'].' '.$data['claim_data']['reffering_first_name'].' '.$data['reffering_claim_data']['middle_name'],
			'id_physician' => $data['claim_data']['npi'],
			'hosp_mm_from' => $hospital_start_date_mm,
			'hosp_dd_from' => $hospital_start_date_dd,
			'hosp_yy_from' => $hospital_start_date_yy,
			'hosp_mm_end' => $hospital_end_date_mm,
			'hosp_dd_end' => $hospital_end_date_dd,
			'hosp_yy_end' => $hospital_end_date_yy,
			'96' =>  $data['claim_data']['additional_claim_information'],
			'charge' => $data['claim_data']['total_charge'],
			'99icd' => $data['claim_data']['icd'],
			'diagnosis1' => $data['claim_data']['diagnosis_a'],
			'diagnosis2' => $data['claim_data']['diagnosis_b'],
			'diagnosis3' => $data['claim_data']['diagnosis_c'],
			'diagnosis4' => $data['claim_data']['diagnosis_d'],
			'diagnosis5' => $data['claim_data']['diagnosis_e'],
			'diagnosis6' => $data['claim_data']['diagnosis_f'],
			'diagnosis7' => $data['claim_data']['diagnosis_g'],
			'diagnosis8' => $data['claim_data']['diagnosis_h'],
			'diagnosis9' => $data['claim_data']['diagnosis_i'],
			'diagnosis10' => $data['claim_data']['diagnosis_j'],
			'diagnosis11' => $data['claim_data']['diagnosis_k'],
			'diagnosis12' => $data['claim_data']['diagnosis_l'],
			'medicaid_resub' => $data['claim_data']['resubmission_code'],
			'original_ref' => $data['claim_data']['original_reference_number'],
			'prior_auth' => $data['claim_data']['prior_aurthorization_number'],			
			'276' => '',
			'tax_id' => $data['claim_data']['federal_tax_number'],
			'pt_account' => $data['claim_data']['account_number'],
			't_charge' => $data['claim_data']['total_amount'],
			'amt_paid' => $data['claim_data']['paid_amount'],
			'doc_phone area' => $admin_phone_number1,
			'doc_phone' => $admin_phone_number2,
			'fac_name' => $data['claim_data']['service_location_name'],
			'doc_name' => $data['claim_data']['admin_location_name'],
			'fac_street' => $data['claim_data']['service_address1'].' '.$data['claim_data']['service_address2'],
			'doc_street' => $data['claim_data']['admin_address1'].' '.$data['claim_data']['admin_address2'],
			'physician_signature' => '',
			'fac_location' => $data['claim_data']['service_city'].', '.$data['claim_data']['service_state'].' '.$data['claim_data']['service_zip_code'],
			'doc_location' => $data['claim_data']['admin_city'].', '.$data['claim_data']['admin_state'].' '.$data['claim_data']['admin_zip_code'],
			'physician_date' => '',
			'pin1' => $data['claim_data']['service_npi'],
			'grp1' => $data['claim_data']['service_other_id'],
			'pin' => $data['claim_data']['admin_npi'],
			'grp' => $data['claim_data']['admin_other_id'],
			'Clear Form' => '2dhfghfgh35',
			'plan1' => 1
			);
			for($i=0;$i<$claim_additional_data_count;$i++){
				$j= $i+1;
				$service_start_date = explode('-',$data['claim_additional_data'][$i]['service_start_date']);
				$service_start_date_yy=$service_start_date[0];
				$service_start_date_mm=$service_start_date[1];
				$service_start_date_dd=$service_start_date[2];

				$service_end_date = explode('-',$data['claim_additional_data'][$i]['service_end_date']);
				$service_end_date_yy=$service_end_date[0];
				$service_end_date_mm=$service_end_date[1];
				$service_end_date_dd=$service_end_date[2];
				$fields_array = array(					
					'emg'.$j.'' => $data['claim_additional_data'][$i]['emg'],
					'local'.$j.'a' => $data['claim_additional_data'][$i]['provider_id'],
					'sv'.$j.'_mm_from' => $service_start_date_mm,
					'sv'.$j.'_dd_from' => $service_start_date_dd,
					'sv'.$j.'_yy_from' => $service_start_date_yy,
					'sv'.$j.'_mm_end' => $service_end_date_mm,
					'sv'.$j.'_dd_end' => $service_end_date_dd,
					'sv'.$j.'_yy_end' => $service_end_date_yy,
					'place'.$j.'' => $data['claim_additional_data'][$i]['service_place'],
					'type'.$j.'' => $data['claim_additional_data'][$i]['emg'],
					'mod'.$j.'' => substr( $data['claim_additional_data'][$i]['modifier'], 0,2),
					'mod'.$j.'a' => substr( $data['claim_additional_data'][$i]['modifier'], 2,4),
					'mod'.$j.'b' => substr( $data['claim_additional_data'][$i]['modifier'], 4,6),
					'mod'.$j.'c' => substr( $data['claim_additional_data'][$i]['modifier'], 6,8),
					'cpt'.$j.'' => $data['claim_additional_data'][$i]['cpt_hcps'],
					'diag'.$j.'' => $data['claim_additional_data'][$i]['diagnosis_pointer'],
					'ch'.$j.'' => number_format($data['claim_additional_data'][$i]['charges'],2),
					'day'.$j.'' => $data['claim_additional_data'][$i]['days'],
					'epsdt'.$j.'' => $data['claim_additional_data'][$i]['epsdt'],
					'local'.$j.'' => $data['claim_additional_data'][$i]['cnpi'],
					'plan'.$j.'' => $data['claim_additional_data'][$i]['fplan'],
				);
				$fields=$fields+$fields_array;
			}

			ob_start();
			$imagenurl= APPPATH.'../images/slides/Chrysanthemum.jpg';
			$pdf = new FPDM(APPPATH.'../admin_files/forms/'.$data['claim_data']['file']);
			$pdf->useCheckboxParser = true;
			//$pdf->Image($imagenurl,10,10,50,50); // X start, Y start, X width, Y width in mm
			$pdf->Load($fields, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
			$pdf->Merge();
			ob_clean();
			$file_data = array(
				'claim_id' => $claim_id,
				'file_name' => $filename.'.pdf',
				'status' => 1,
			);
			$file_data = $this->security->xss_clean($file_data);
			$file_result = $this->claim_model->add_file_data($file_data,$claim_id);
			
			if($data['claim_data']['status_id'] == 2 ){
				$status_id=8;
				$claim_status=$this->claim_model->update_claim_status($claim_id, $status_id);
			}
			
			if($file_result){
				$pdf->Output('','admin_files/generated_claims/'.$filename.'.pdf', false);
			}			
		}

		public function del_draft(){
			$id= $this->input->post('id');
			$this->db->trans_start();
			$this->db->delete('claims', array('id' => $id));
			
			$this->db->trans_complete();
			$this->session->set_flashdata('danger_msg', 'Claim Deleted Successfully!');
			echo 'success';
		}
		public function claim_status_history_id()
		{
			$id=$_POST["id"];
			$type=$_POST["type"];
			$result=$this->claim_model->claim_status_history_id($id,$type);
			foreach($result as $row)
			{
			  $str=explode(" ",$row['create_date']);
			  $output[] = array(
			   'month'   => $str[0],
			   'status'  => floatval($row["claim_status_id"]),
			   'title' => $row['claim_status_name']
			  );
			}
			 echo json_encode($output);
		}
		public function claim_status_history1()
		{	
			$id=$_POST["id"];
			$type=$_POST["type"];
			//$id=89;
			//$type=1;
			$row1=$this->claim_model->get_claim_by_cid($id);
			$start=explode(" ",$row1['created_date']);
			$row2=$this->claim_model->claim_status_history_id($id,$type);
			$diff=explode(" ",$row2[0]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row2[0]['claim_status_name'],
					'days'   => floatval($daystr)
					);
			$i=$row2[0]['status_name'];
			$i=$i+1;
			$row1=$this->claim_model->claim_status_history1($i,$id,$type);
			//echo "row1->";
			//print_r($row1);
			$diff[$i]=explode(" ",$row1['create_date']); 
			$d1= $diff[$i][0];
			$date1=date_create($d1);				
			$dsecond=date_diff($date1,$date2);
			$daysf=$dsecond->format("%a");
			$output[] = array(					
			'status'  => $row1['claim_status_name'],
			'days'   => floatval($daysf)
			);
			$i=$i+1;
			//echo $i;
			for(;$i<=6;$i++)
			{	
				//$id=89;
				//$type=1;
				$row1=$this->claim_model->claim_status_history1($i,$id,$type);
				//echo "row1->";
				//print_r($row1);
				$diff[$i]=explode(" ",$row1['create_date']); 				
				$j=$i+1;
				
				$row2=$this->claim_model->claim_status_history2($i,$id,$type);
				//echo "row2->";
				//print_r($row2);
				$diff[$j]=explode(" ",$row2['create_date']); 
				if(count($diff[$i])==2 && count($diff[$j])==2)
				{
					$d1= $diff[$i][0];
					$d2= $diff[$j][0];
					$date1=date_create($d1);
					$date2=date_create($d2);
					$dfinal=date_diff($date1,$date2);
					$dayf=$dfinal->format("%a");
					$output[] = array(					
					'status'  => $row1['claim_status_name'],
					'days'   => floatval($dayf)
					);
				}
				 			
			}
			$row1=$this->claim_model->get_lclaim_by_cidtype($id,$type);
			//$row1=$this->claim_model->get_lclaim_by_cid($id);
			//print_r($row1);			
			$start=explode(" ",$row1[0]['create_date']);
			$diff=explode(" ",$row1[1]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row1[0]['status_description'],
					'days'   => floatval($daystr)
					);
			echo json_encode($output);		
		}
		public function claim_status_history2()
		{	
			$id=$_POST["id"];
			$type=$_POST["type"];
			//$id=89;
			//$type=1;
			$row1=$this->claim_model->get_claim_by_cid($id);
			$start=explode(" ",$row1['created_date']);
			$row2=$this->claim_model->claim_status_history_id($id,$type);
			$diff=explode(" ",$row2[0]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row2[0]['claim_status_name'],
					'days'   => floatval($daystr)
					);
			$i=$row2[0]['status_name'];
			$i=$i+1;
			$row1=$this->claim_model->claim_status_history1($i,$id,$type);
			//echo "row1->";
			//print_r($row1);
			$diff[$i]=explode(" ",$row1['create_date']); 
			$d1= $diff[$i][0];
			$date1=date_create($d1);				
			$dsecond=date_diff($date1,$date2);
			$daysf=$dsecond->format("%a");
			$output[] = array(					
			'status'  => $row1['claim_status_name'],
			'days'   => floatval($daysf)
			);
			$i=$i+1;
			//echo $i;
			for(;$i<=12;$i++)
			{	
				//$id=89;
				//$type=1;
				$row1=$this->claim_model->claim_status_history1($i,$id,$type);
				//echo "row1->";
				//print_r($row1);
				$diff[$i]=explode(" ",$row1['create_date']); 
				$j=$i+1;
				
				$row2=$this->claim_model->claim_status_history2($i,$id,$type);
				//echo "row2->";
				//print_r($row2);
				$diff[$j]=explode(" ",$row2['create_date']); 
				if(count($diff[$i])==2 && count($diff[$j])==2)
				{
					$d1= $diff[$i][0];
					$d2= $diff[$j][0];
					$date1=date_create($d1);
					$date2=date_create($d2);
					$dfinal=date_diff($date1,$date2);
					$dayf=$dfinal->format("%a");
					$output[] = array(					
					'status'  => $row1['claim_status_name'],
					'days'   => floatval($dayf)
					);
				}
				 			
			}
			$row1=$this->claim_model->get_lclaim_by_cidtype($id,$type);
			//print_r($row1);			
			$start=explode(" ",$row1[0]['create_date']);
			$diff=explode(" ",$row1[1]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row1[0]['status_description'],
					'days'   => floatval($daystr)
					);
			echo json_encode($output);		
		}
		public function claim_status_history3()
		{	
			$id=$_POST["id"];
			$type=$_POST["type"];
			//$id=89;
			//$type=1;
			$row1=$this->claim_model->get_claim_by_cid($id);
			$start=explode(" ",$row1['created_date']);
			$row2=$this->claim_model->claim_status_history_id($id,$type);
			$diff=explode(" ",$row2[0]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row2[0]['claim_status_name'],
					'days'   => floatval($daystr)
					);
			$i=$row2[0]['status_name'];
			$i=$i+1;
			$row1=$this->claim_model->claim_status_history1($i,$id,$type);
			//echo "row1->";
			//print_r($row1);
			$diff[$i]=explode(" ",$row1['create_date']); 
			$d1= $diff[$i][0];
			$date1=date_create($d1);				
			$dsecond=date_diff($date1,$date2);
			$daysf=$dsecond->format("%a");
			$output[] = array(					
			'status'  => $row1['claim_status_name'],
			'days'   => floatval($daysf)
			);
			$i=$i+1;
			//echo $i;
			for(;$i<=16;$i++)
			{	
				//$id=89;
				//$type=1;
				$row1=$this->claim_model->claim_status_history1($i,$id,$type);
				//echo "row1->";
				//print_r($row1);
				$diff[$i]=explode(" ",$row1['create_date']); 
				$j=$i+1;
				
				$row2=$this->claim_model->claim_status_history2($i,$id,$type);
				//echo "row2->";
				//print_r($row2);
				$diff[$j]=explode(" ",$row2['create_date']); 
				if(count($diff[$i])==2 && count($diff[$j])==2)
				{
					$d1= $diff[$i][0];
					$d2= $diff[$j][0];
					$date1=date_create($d1);
					$date2=date_create($d2);
					$dfinal=date_diff($date1,$date2);
					$dayf=$dfinal->format("%a");
					$output[] = array(					
					'status'  => $row1['claim_status_name'],
					'days'   => floatval($dayf)
					);
				}
				 			
			}
			$row1=$this->claim_model->get_lclaim_by_cidtype($id,$type);
			//print_r($row1);			
			$start=explode(" ",$row1[0]['create_date']);
			$diff=explode(" ",$row1[1]['create_date']);			
			$d1= $start[0];
			$d2= $diff[0];
			$date1=date_create($d1);
			$date2=date_create($d2);
			$dstart=date_diff($date1,$date2);
			$daystr=$dstart->format("%a");			
			$output[] = array(					
					'status'  => $row1[0]['status_description'],
					'days'   => floatval($daystr)
					);
			echo json_encode($output);		
		}
		
	}


?>