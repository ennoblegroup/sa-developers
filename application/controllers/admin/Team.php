<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Team extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/team_model', 'team_model');
			$this->load->model('admin/role_model', 'role_model');
			$this->load->model('admin/user_model', 'user_model');
			$this->load->model('state_model', 'state_model');
			$this->load->model('admin/service_model', 'service_model');
		}

		public function index(){
			$data['all_teams'] =  $this->team_model->get_all_teams();
			$data['view'] = 'admin/team/view_teams';
			$this->load->view('admin/layout', $data);
		}
		public function view_team($id){
			$data['team'] = $this->team_model->get_team_by_id($id);
			$data['all_states'] =  $this->state_model->get_all_states();
			$data['view'] = 'admin/team/view_team';
			$this->load->view('admin/layout', $data);
		}
		
		public function add(){
			if($this->input->post('submit')){					
				
				$config['upload_path'] = 'images/team/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|PNG|JPEG|JPG';
				$config['max_size'] = '1000';
				$config['encrypt_name'] = true;
				$this->load->library('upload', $config);                                                
                $this->upload->initialize($config);				
				$this->upload->do_upload('image');
				$file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = $file_data['full_path'];
				$config['new_image'] = 'images/team/thumb/';
				$config['create_thumb'] = TRUE;
				$config['maintain_ratio'] = false;
				$config['thumb_marker'] = '';
				$config['width'] = 160;
				$config['height'] = 30;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$fname = $file_data['file_name'];
				$notes= $this->input->post('notes');
				$designation= $this->input->post('designation');
				$last_name=$this->input->post('last_name');
				$first_name=$this->input->post('first_name');
				$email=$this->input->post('email');
				$data = array(						
					'lastname' => $last_name,
					'firstname' => $first_name,
					'middlename	' => $this->input->post('middle_name'),
					'designation' => $designation,
					'mobile_no' => $this->input->post('phone_number'),						
					'email' => $email,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'image' => $fname,
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'notes' => $notes,
					'twitter' => $this->input->post('twitter'),
					'facebook' => $this->input->post('facebook'),
					'instagram' => $this->input->post('instagram'),
					'linkedin' => $this->input->post('linkedin'),
					'pinterest' => $this->input->post('pinterest'),
					'youtube' => $this->input->post('youtube'),
					'status' =>1,
					'created_at' => date('Y-m-d : h:m:s')
				);
				$data = $this->security->xss_clean($data);
				$result = $this->team_model->add_team($data);
				if($result){
					$this->session->set_flashdata('success_msg', 'Member Added Successfully!');
					redirect(base_url('admin/team'));
				}
			}
			else{
				$data['all_states'] =  $this->state_model->get_all_states();
				
				$data['view'] = 'admin/team/add_team';
				$this->load->view('admin/layout', $data);
			}
			
		}

		public function edit($id = 0){
			if($this->input->post('submit')){
				$id	= $this->input->post('team_id');
				$name = $_FILES['image']['name'];
				$sql = "select * from team where id = '$id'";
				$query = $this->db->query($sql);
				$row = $query->row_array();	
				$fileName = $row['image'];
				if($name!='') 
				{	
					$path = "images/team/" . $row['image'];
					$path1 = "images/team/thumb" . $row['image'];

					$config['upload_path'] = 'images/team/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png';
					$config['max_size'] = '0';
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);                                                     
                    $this->upload->initialize($config);
					$this->upload->do_upload('image');
					$file_data = $this->upload->data();
					$config['image_library'] = 'gd2';
					$config['source_image'] = $file_data['full_path'];
					$config['new_image'] = 'images/team/thumb/';
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = false;
					$config['thumb_marker'] = '';				
				    $config['width'] = '160';
					$config['height'] = '200';
					$this->load->library('image_lib', $config);                   
                    $this->upload->initialize($config);
					$this->image_lib->resize();
					$fileName = $file_data['file_name'];	               
				}
				
				$notes= $this->input->post('notes');
				$designation= $this->input->post('designation');
				$last_name=$this->input->post('last_name');
				$first_name=$this->input->post('first_name');
				$email=$this->input->post('email');
				$data = array(						
					'lastname' => $last_name,
					'firstname' => $first_name,
					'middlename	' => $this->input->post('middle_name'),
					'designation' => $designation,
					'mobile_no' => $this->input->post('phone_number'),						
					'email' => $email,
					'address1' => $this->input->post('address1'),
					'address2' => $this->input->post('address2'),
					'city' => $this->input->post('city'),
					'image' => $fileName,
					'state' => $this->input->post('state'),
					'zip_code' => $this->input->post('zip_code'),
					'notes' => $notes,
					'twitter' => $this->input->post('twitter'),
					'facebook' => $this->input->post('facebook'),
					'instagram' => $this->input->post('instagram'),
					'linkedin' => $this->input->post('linkedin'),
					'pinterest' => $this->input->post('pinterest'),
					'youtube' => $this->input->post('youtube'),
					'status' =>1
				);
				$data = $this->security->xss_clean($data);
				$result = $this->team_model->edit_team($data, $id);
				if($result){
					$this->session->set_flashdata('success_msg', 'Member Updated Successfully!');
					redirect(base_url('admin/team'));
				}
			}
			else{
				$data['team'] = $this->team_model->get_team_by_id($id);
				$data['all_states'] =  $this->state_model->get_all_states();
				$data['view'] = 'admin/team/edit_team';
				$this->load->view('admin/layout', $data);
			}
		}

		public function del(){
			$id= $this->input->post('id');
			$this->db->delete('team', array('id' => $id));
			$this->session->set_flashdata('danger_msg', 'Member Deleted Successfully!');
			echo 'success';
		}
		function update_status()
		{
			$team_id = $_POST['team_id'];
			$id         = $_POST['id'];
			return $this->team_model->update_status($team_id, $id);
		}
		
		public function filter_teams()
		{
			//$data['all_teams'] =  $this->team_model->get_all_teams();
			$rrr=$this->team_model->get_all_teams1();
			$output = array('teams_list'   => $rrr);
			echo json_encode($output, true);
		}
	}


?>