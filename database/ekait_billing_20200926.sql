-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 26, 2020 at 12:59 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekait_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(11, 'About Us', '<p><img class=\"aligncenter aboutImg animate\" title=\"\" src=\"/assets/website/images/about1.png\" alt=\"about\" data-animation=\"fadeInDown\"></p>\r\n<div class=\"margin30\"> </div>\r\n<div class=\"one-half column\">\r\n<h4 class=\"border-title\">Making Better Care Possible</h4>\r\n<p>We’re in an era of unprecedented complexity in healthcare. Patient care is not where it should be. While many of the organizations delivering the care need to get much healthier, too.</p>\r\n<div class=\"margin60\"> </div>\r\n<img class=\"chart animate\" title=\"\" src=\"/assets/website/images/bar-chart.png\" alt=\"chart\" data-animation=\"stretchRight\"></div>\r\n<div class=\"one-half column last\">\r\n<h4 class=\"border-title\">client Management Services</h4>\r\n<p>You can use our client management services and technology to build a business that will meet the changing needs of your customers and our healthcare system.</p>\r\n<div class=\"margin60\"> </div>\r\n<h4 class=\"border-title\">Our vision</h4>\r\n<p>admin client Systems gives you the freedom to select the right technology for your client. We provide client management software that makes it simple for you to start, run and expand your client. Make your client software more efficient and clinically driven with our extensive selection of value-added products and services.</p>\r\n<div class=\"legend-wrapper\">\r\n<div class=\"orange legend\">\r\n<div class=\"legendColorBox\">\r\n<div> </div>\r\n</div>\r\n<div class=\"legendLabel\">Production</div>\r\n</div>\r\n<div class=\"blue legend\">\r\n<div class=\"legendColorBox\">\r\n<div> </div>\r\n</div>\r\n<div class=\"legendLabel\">Production</div>\r\n</div>\r\n</div>\r\n<div class=\"margin75\"> </div>\r\n</div>', '', 1, '2020-09-18 07:09:40', '2020-06-16 12:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `create_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `name`, `phone`, `email`, `subject`, `message`, `active`, `create_id`, `create_date`, `status`) VALUES
(1, 'kumar praveen', '3343434343', 'praveens@ennobletechnologies.com', 'General/Other', 'tester', 1, 0, '2020-09-22 13:18:51', 0),
(2, 'Rudroju Test2 Raghuvarma', '9876543210', 'raghuvarma.chintu@gmail.com', 'General/Other', 'jyfjyhm', 1, 0, '2020-09-24 13:20:02', 0),
(3, 'Rudroju Test2 Raghuvarma', '9876543210', 'raghuvarma.chintu@gmail.com', 'General/Other', 'jyfjyhm', 1, 0, '2020-09-24 13:20:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `claim_registration_id` varchar(255) NOT NULL,
  `client_id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claims`
--

INSERT INTO `claims` (`id`, `claim_registration_id`, `client_id`, `insurance_company_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 'CLAIM8207735', 15, 4, 2, '2020-09-23 07:55:56', '2020-09-23 07:55:56'),
(2, 'CLAIM8301167', 15, 4, 2, '2020-09-24 08:34:47', '2020-09-24 08:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `claim_additional_information`
--

CREATE TABLE `claim_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `service_start_date` date NOT NULL,
  `service_end_date` date NOT NULL,
  `service_place` varchar(255) NOT NULL,
  `emg` varchar(255) NOT NULL,
  `cpt_hcps` varchar(255) NOT NULL,
  `modifier` varchar(255) NOT NULL,
  `diagnosis_pointer` varchar(255) NOT NULL,
  `charges` float NOT NULL,
  `days` varchar(255) NOT NULL,
  `epsdt` varchar(255) NOT NULL,
  `id_qual` varchar(255) NOT NULL,
  `provider_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_additional_information`
--

INSERT INTO `claim_additional_information` (`id`, `claim_id`, `service_start_date`, `service_end_date`, `service_place`, `emg`, `cpt_hcps`, `modifier`, `diagnosis_pointer`, `charges`, `days`, `epsdt`, `id_qual`, `provider_id`) VALUES
(1, 1, '2020-09-21', '2020-09-21', 'sdi', 'sxv', 'qwerty', 'EWGWE222', 'oiih', 22, '345', '52', '', 'qwertyuiopl'),
(2, 1, '2020-09-21', '2020-09-21', 'sdi', 'sxv', 'qwerty', 'EWGWE222', 'oiih', 22, '345', '52', '', 'qwertyuiopl'),
(3, 2, '2020-09-14', '1970-01-01', 'sdv', 'sek', 'qwerty', 'EWuGWEuu', 'oiih', 22, '345', '22', '', 'qwertyuiopl');

-- --------------------------------------------------------

--
-- Table structure for table `claim_diagnosis`
--

CREATE TABLE `claim_diagnosis` (
  `id` int(11) NOT NULL,
  `claim_id` int(11) NOT NULL,
  `diagnosis_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_diagnosis`
--

INSERT INTO `claim_diagnosis` (`id`, `claim_id`, `diagnosis_value`) VALUES
(0, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `claim_information`
--

CREATE TABLE `claim_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `employment` varchar(255) NOT NULL,
  `auto_accident` varchar(255) NOT NULL,
  `other_accident` varchar(255) NOT NULL,
  `accident_location` varchar(255) NOT NULL,
  `claim_code` varchar(255) NOT NULL,
  `injury_date` varchar(255) NOT NULL,
  `qual_date` varchar(255) NOT NULL,
  `nonworking_start_date` date NOT NULL,
  `nonworking_end_date` date NOT NULL,
  `hospital_start_date` date NOT NULL,
  `hospital_end_date` date NOT NULL,
  `additional_claim_information` varchar(255) NOT NULL,
  `out_lab` varchar(255) NOT NULL,
  `total_charge` varchar(255) NOT NULL,
  `icd` varchar(255) NOT NULL,
  `icda` varchar(255) NOT NULL,
  `icdb` varchar(255) NOT NULL,
  `icdc` varchar(255) NOT NULL,
  `resubmission_code` varchar(255) NOT NULL,
  `original_reference_number` varchar(255) NOT NULL,
  `prior_aurthorization_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_information`
--

INSERT INTO `claim_information` (`id`, `claim_id`, `employment`, `auto_accident`, `other_accident`, `accident_location`, `claim_code`, `injury_date`, `qual_date`, `nonworking_start_date`, `nonworking_end_date`, `hospital_start_date`, `hospital_end_date`, `additional_claim_information`, `out_lab`, `total_charge`, `icd`, `icda`, `icdb`, `icdc`, `resubmission_code`, `original_reference_number`, `prior_aurthorization_number`) VALUES
(1, 1, 'yes', 'yes', 'yes', '', '', '09/22/2020', '09/24/2020', '2020-09-21', '2020-09-21', '0000-00-00', '0000-00-00', '', 'yes', '55', 'sdgfg', 'dfhg', 'fghfg', 'fghfg', '', '', ''),
(2, 2, 'yes', 'yes', 'yes', '', 'u67776ty', '09/08/2020', '09/03/2020', '2020-09-24', '2020-09-24', '2020-09-24', '2020-09-24', '', 'yes', '55', 'restes', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `claim_status`
--

CREATE TABLE `claim_status` (
  `id` int(5) NOT NULL,
  `claim_id` int(5) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  `status_description` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `claim_status`
--

INSERT INTO `claim_status` (`id`, `claim_id`, `status_name`, `status_description`, `create_date`) VALUES
(1, 1, 'sbxfb', 'cbncvnvnvbn', '2020-09-23 11:55:19');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`id`, `communication_id`, `from_user_id`, `to_user_id`, `mail_to`, `mail_from`, `mail_subject`, `mail_message`, `read_status`, `created_date`, `updated_date`) VALUES
(1, 'COM4039668', 3, 7, 'test@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(2, 'COM4039668', 3, 6, 'ali@admin.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(3, 'COM4039668', 3, 5, 'naumanahmedcs@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(4, 'COM4039668', 3, 8, 'johnsmith@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(5, 'COM4039668', 3, 9, 'herrypro@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(6, 'COM8334431', 3, 12, 'anilkumaradusumilli@gmail.com', 'admin@admin.com', 'Test', '', '0', '2020-09-23 19:39:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `communications_replies`
--

INSERT INTO `communications_replies` (`id`, `communication_id`, `email_id`, `user_id`, `reply_to`, `reply_from`, `reply_message`, `read_status`, `reply_date`, `updated_date`) VALUES
(1, 5, 0, 3, '', 'admin@admin.com', 'Test Reply Message', '0', '2020-09-17 18:30:00', NULL),
(2, 5, 0, 3, '', 'admin@admin.com', 'second reply message', '0', '2020-09-17 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `contact` varchar(18) NOT NULL,
  `fax_number` varchar(200) NOT NULL,
  `day_one` varchar(50) NOT NULL,
  `day_two` varchar(50) NOT NULL,
  `day_three` varchar(50) NOT NULL,
  `start_time` varchar(250) NOT NULL DEFAULT 'AM',
  `end_time` varchar(250) NOT NULL DEFAULT 'PM',
  `start_hour` varchar(250) NOT NULL DEFAULT 'AM',
  `end_hour` varchar(250) NOT NULL DEFAULT 'PM',
  `hours` varchar(250) NOT NULL DEFAULT '00:00:00',
  `facebook` longtext NOT NULL,
  `twitter` longtext NOT NULL,
  `linkedin` text NOT NULL,
  `pinterest` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `address1`, `title`, `description`, `address2`, `city`, `country`, `state`, `zip`, `status`, `email`, `phone`, `contact`, `fax_number`, `day_one`, `day_two`, `day_three`, `start_time`, `end_time`, `start_hour`, `end_hour`, `hours`, `facebook`, `twitter`, `linkedin`, `pinterest`, `youtube`, `instagram`, `create_date`, `update_date`) VALUES
(1, '917 Franklin Avenue', 'Contact Us', 'test', '', 'Newark', 'US', 'NJ', '07107-2809', 1, 'info@admin.com', '+1 (973) 412 7300', ' ', '+1 (973) 412 7303', '', '', '', '', '', '', '', '00:00:00', 'https://www.facebook.com/', 'https://twitter.com/BranchBrook', 'https://www.linkedin.com/ ', 'https://www.pinterest.com/', 'https://www.youtube.com/', 'https://www.instagram.com/', '2019-03-21', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(8, 'Features', '<div class=\"one-half column\"><img class=\"aligncenter rollImage animate\" title=\"\" src=\"/admin/&lt;?php echo base_url();?&gt;assets/website/images/mobile-view-map1.png\" alt=\"mobilemap\" data-animation=\"fadeInLeft\"></div>\r\n<div class=\"one-half column last\">\r\n<ul class=\"list-style extra_list\">\r\n<li>Enhanced client Specialty Software.</li>\r\n<li>Certified by Surescripts to handle EPCS (Electronic Processing of Controlled Substances) transactions.</li>\r\n<li>Controlled Substance Reporting automatically tracks CSR information and uploads files to the appropriate state agency (where permitted).</li>\r\n<li>Keep your client open 24/7 by scheduling repeating processes, including such as refills, data backups, and automated report emails.</li>\r\n<li>Calculate PDC/MPRvalues for your patients individually or by group.</li>\r\n<li>Patient medication adherence reports that contribute to client star ratings.</li>\r\n<li>Interface with Mirixa and Outcomes: MTM opportunities delivered to admin™.</li>\r\n<li>Optimize profits by utilizing adherence features.</li>\r\n</ul>\r\n</div>\r\n<div class=\"margin65\"> </div>\r\n<div class=\"one-half column pull-up\">\r\n<div class=\"custom-services\">\r\n<h3>Automated Refill Management System</h3>\r\n<p>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data.</p>\r\n</div>\r\n<div class=\"margin35\"> </div>\r\n<div class=\"custom-services\">\r\n<h3>Customized Workflow Management and Reports</h3>\r\n<p>Reconciliation process ensures you will never miss a payment. <br>Generate electronic orders with wholesalers based on your customized drug ordering parameters</p>\r\n</div>\r\n<div class=\"margin35\"> </div>\r\n<div class=\"custom-services\">\r\n<h3>Patient Communication</h3>\r\n<p>Outbound calls, text/SMS messaging. <br>Inbound communication allows patients to respond via prompts. <br>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-half column last\"><img class=\"aligncenter fadeImage animate\" title=\"\" src=\"/admin/&lt;?php echo base_url();?&gt;assets/website/images/mobile-two1.png\" alt=\"mobiletwo\" data-animation=\"fadeInRight\"></div>\r\n<div class=\"margin90\"> </div>\r\n<div class=\"one-third column\">\r\n<div class=\"custom-services\">\r\n<h3>Report Generation</h3>\r\n<p>Customized reports to support the client’s unique needs. <br>The daily log provides a comprehensive report of each day’s activities. <br>Customized business analytics.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-third column\">\r\n<div class=\"custom-services\">\r\n<h3>Key Features</h3>\r\n<p>Enhanced client Specialty Software. <br>Optimize profits by utilizing adherence features. <br>Calculate PDC/MPRvalues for your patients individually or by group.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-third column last\">\r\n<div class=\"custom-services\">\r\n<h3>Advantages</h3>\r\n<p>Adherence Programs. <br>Electronic Medical Records (EMR) Vendors. <br>Electronic Medication Admin Records. <br>Interactive Voice Response Systems.</p>\r\n</div>\r\n</div>', '', 1, '2020-09-18 12:53:46', '2020-02-23 02:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `file_source` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `file_source`, `form_id`, `form_name`, `form_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(5, 'Internet', 'FRM9566207', 'CMS1500', 'This is Health Insurance Claim Form', '690ca3abac12bb69d9db4afa0718fbb7.pdf', 1, '2020-09-17 04:39:24', '2020-09-17 04:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `generated_claims`
--

CREATE TABLE `generated_claims` (
  `id` int(5) NOT NULL,
  `claim_id` int(5) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generated_claims`
--

INSERT INTO `generated_claims` (`id`, `claim_id`, `file_name`, `status`, `create_date`) VALUES
(1, 1, 'CLAIM8207735_09-24-2020_103038.pdf', 0, '2020-09-24 10:30:38'),
(2, 2, 'CLAIM8301167_09-24-2020_103038.pdf', 0, '2020-09-24 10:30:38'),
(3, 1, 'CLAIM8207735_09-24-2020_103123.pdf', 0, '2020-09-24 10:31:24'),
(4, 2, 'CLAIM8301167_09-24-2020_103124.pdf', 0, '2020-09-24 10:31:24'),
(5, 1, 'CLAIM8207735_09-24-2020_103126.pdf', 0, '2020-09-24 10:31:27'),
(6, 2, 'CLAIM8301167_09-24-2020_103127.pdf', 0, '2020-09-24 10:31:27'),
(7, 1, 'CLAIM8207735_09-24-2020_103158.pdf', 0, '2020-09-24 10:31:58'),
(8, 2, 'CLAIM8301167_09-24-2020_103158.pdf', 0, '2020-09-24 10:31:58'),
(9, 1, 'CLAIM8207735_09-24-2020_103228.pdf', 0, '2020-09-24 10:32:28'),
(10, 2, 'CLAIM8301167_09-24-2020_103229.pdf', 0, '2020-09-24 10:32:29'),
(11, 1, 'CLAIM8207735_09-24-2020_103303.pdf', 0, '2020-09-24 10:33:03'),
(12, 2, 'CLAIM8301167_09-24-2020_103304.pdf', 0, '2020-09-24 10:33:04'),
(13, 1, 'CLAIM8207735_09-24-2020_103333.pdf', 0, '2020-09-24 10:33:33'),
(14, 2, 'CLAIM8301167_09-24-2020_103334.pdf', 0, '2020-09-24 10:33:34'),
(15, 1, 'CLAIM8207735_09-24-2020_103551.pdf', 0, '2020-09-24 10:35:51'),
(16, 2, 'CLAIM8301167_09-24-2020_103551.pdf', 0, '2020-09-24 10:35:52'),
(17, 1, 'CLAIM8207735_09-24-2020_103615.pdf', 0, '2020-09-24 10:36:16'),
(18, 2, 'CLAIM8301167_09-24-2020_103616.pdf', 0, '2020-09-24 10:36:16'),
(19, 1, 'CLAIM8207735_09-24-2020_112729.pdf', 0, '2020-09-24 11:27:29'),
(20, 1, 'CLAIM8207735_09-24-2020_122827.pdf', 0, '2020-09-24 12:28:27'),
(21, 2, 'CLAIM8301167_09-24-2020_122828.pdf', 1, '2020-09-24 12:28:28'),
(22, 1, 'CLAIM8207735_09-24-2020_134927.pdf', 1, '2020-09-24 13:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `homeservices`
--

CREATE TABLE `homeservices` (
  `id` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeservices`
--

INSERT INTO `homeservices` (`id`, `title`, `description`, `image`, `status`, `create_date`, `update_date`) VALUES
(8, 'Services', '<div class=\"one-fourth column no-space\">\r\n<div class=\"service\">\r\n<div class=\"margin20\"> </div>\r\n<h4>Customized Labels</h4>\r\n<p>Custom labels are custom text values that can be accessed from Apex classes, Visualforce pages, or Lightning components.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-fourth column no-space\">\r\n<div class=\"service\">\r\n<div class=\"margin20\"> </div>\r\n<h4>Custom Programming</h4>\r\n<p>The action or process of writing a computer program to meet a given need. The need may be simple or complex.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-fourth column no-space\">\r\n<div class=\"service\">\r\n<div class=\"margin20\"> </div>\r\n<h4>Hardware Upgrade</h4>\r\n<p>Hardware upgrade is a term that describes adding new hardware that improves its performance and speed.</p>\r\n</div>\r\n</div>\r\n<div class=\"one-fourth column no-space last\">\r\n<div class=\"service\">\r\n<div class=\"margin20\"> </div>\r\n<h4>Hardware Contracts</h4>\r\n<p>A hardware service agreement is a contract used between a business providing computer hardware maintenance.</p>\r\n</div>\r\n</div>\r\n<div class=\"margin50\"> </div>\r\n<h2 class=\"border-title\">What do we provide?</h2>\r\n<div class=\"margin15\"> </div>\r\n<div class=\"tabs-vertical-container\">\r\n<ul class=\"tabs-vertical-frame one-third column\">\r\n<li><a href=\"#\"><strong>1</strong> Automated Refill Management System </a></li>\r\n<li><a href=\"#\"><strong> 2 </strong>Customized Workflow Management and Reports </a></li>\r\n<li><a href=\"#\"> <strong>3 </strong> Patient Communication </a></li>\r\n<li><a href=\"#\"> <strong>4</strong> Report Generation </a></li>\r\n<li><a href=\"#\"> <strong>5</strong> Flexible Software </a></li>\r\n<li><a href=\"#\"><strong> 6</strong> Powerful Admin Panel </a></li>\r\n</ul>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Automated Refill Management System</h3>\r\n<ul class=\"list-style\">\r\n<li>Fill electronic prescriptions by reviewing existing patient, prescriber and drug inventory data</li>\r\n<li>“At a glance,” visibility of all refill management processes through a Refill Compliance Dashboard. The dashboard can be customized to include preferred metrics, including scheduled refills, missed refills, expired refills, and Med Sync Rx.</li>\r\n<li>Automatically queues each day’s scheduled refills</li>\r\n<li>RxSync - Sync all refills to a particular date</li>\r\n<li>Return to Stock Queue - Prescriptions not picked up by patients will be flagged, with the system easily updated to reflect missed pickups, and drugs added back to inventory</li>\r\n</ul>\r\n</div>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Customized Workflow Management and Reports</h3>\r\n<ul class=\"list-style\">\r\n<li>Inventory management</li>\r\n<li>Reconciliation process ensures you will never miss a payment</li>\r\n<li>Bin management feature facilitates prescription pickup process</li>\r\n<li>Seamless management of incoming prescriptions</li>\r\n<li>Generate electronic orders with wholesalers based on your customized drug ordering parameters</li>\r\n<li>Ability to show cost comparisons between wholesalers</li>\r\n<li>Automatically update true cost from EDI files received from a wholesaler(s)</li>\r\n<li>Ensure compliance with all state and federal regulatory agencies</li>\r\n<li>Automatic backups of all data; Remote server access in case of emergency</li>\r\n</ul>\r\n</div>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Patient Communication</h3>\r\n<ul class=\"list-style\">\r\n<li>Outbound calls, text/SMS messaging</li>\r\n<li>Inbound communication allows patients to respond via prompts</li>\r\n<li>Emails to communicate information related to refilling schedules dosage alerts, and pickup availability.</li>\r\n<li>Birthday greetings and other personalized messages sent via email or text/SMS message</li>\r\n</ul>\r\n</div>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Report Generation</h3>\r\n<ul class=\"list-style\">\r\n<li>Customized reports to support the client’s unique needs</li>\r\n<li>The daily log provides a comprehensive report of each day’s activities</li>\r\n<li>Customized business analytics</li>\r\n<li>Identify key metrics and receive regular reports relevant to that data</li>\r\n<li>Business intelligence reports built into the system - why pay extra to third-party vendors?</li>\r\n</ul>\r\n</div>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Flexible Software</h3>\r\n<p>Software flexibility can mean a lot of things but when it is used to describe a whole system, it normally refers to the ability for the solution to adapt to possible or future changes in its requirements. When you design or build a solution you should try to cater for these changes which inevitably arrive in the future.</p>\r\n<p>This flexibility should be catered for with the design of the system as a whole but there is also no reason not to also include it with the smaller aspects of the system.</p>\r\n</div>\r\n<div class=\"tabs-vertical-frame-content two-third column last\">\r\n<h3>Powerful Admin Panel</h3>\r\n<p>The admin panel can help user-related functions, such as providing insight into user behavior, dealing with profiles that violate the site\'s terms and conditions, and tracking transactions. But you should know that admin sites are not just limited to user-related tasks.</p>\r\n<p>Almost all of content and layout elements could be customized in colors using a colorpicker tool.</p>\r\n</div>\r\n</div>\r\n<div class=\"margin30\"> </div>', '', 1, '2020-09-21 11:50:10', '2020-02-23 08:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_additional_information`
--

CREATE TABLE `insurance_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_checks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_additional_information`
--

INSERT INTO `insurance_additional_information` (`id`, `claim_id`, `insured_checks`) VALUES
(1, 1, 'MEDICARE'),
(2, 2, 'MEDICARE');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` int(10) NOT NULL,
  `insurance_company_id` varchar(255) NOT NULL,
  `insurance_company_name` varchar(30) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `naic_number` varchar(25) NOT NULL,
  `naic_group_code` varchar(25) NOT NULL,
  `state_of_domicile` varchar(25) NOT NULL,
  `company_type` varchar(25) NOT NULL,
  `company_status` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `insurance_company_id`, `insurance_company_name`, `ein_number`, `naic_number`, `naic_group_code`, `state_of_domicile`, `company_type`, `company_status`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `email_address`, `status`, `created_date`, `updated_date`) VALUES
(4, 'INS3371929', 'National Insurance Company', '6546456', '56456', '5465', 'NJ', 'lah', 'conservatorship', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '+1 (987) 654 3261', 'raghuvarma.@gmail.com', 1, '2020-09-17 04:39:04', '2020-09-17 04:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_forms`
--

CREATE TABLE `insurance_forms` (
  `id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `form_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_forms`
--

INSERT INTO `insurance_forms` (`id`, `insurance_company_id`, `form_id`, `status`) VALUES
(2, 4, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_information`
--

CREATE TABLE `insurance_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_id_number` varchar(255) NOT NULL,
  `insured_last_name` varchar(255) NOT NULL,
  `insured_first_name` varchar(255) NOT NULL,
  `insured_middle_name` varchar(255) NOT NULL,
  `insured_address1` varchar(255) NOT NULL,
  `insured_address2` varchar(255) NOT NULL,
  `insured_city` varchar(255) NOT NULL,
  `insured_state` varchar(255) NOT NULL,
  `insured_zip_code` varchar(255) NOT NULL,
  `other_insured_last_name` varchar(255) NOT NULL,
  `other_insured_first_name` varchar(255) NOT NULL,
  `other_insured_middle_name` varchar(255) NOT NULL,
  `insured_group_number` varchar(255) NOT NULL,
  `other_group_number` varchar(255) NOT NULL,
  `insurance_plan_name` varchar(255) NOT NULL,
  `insured_dob` date NOT NULL,
  `insured_gender` varchar(255) NOT NULL,
  `iclaim_id` varchar(255) NOT NULL,
  `other_benefit_plan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_information`
--

INSERT INTO `insurance_information` (`id`, `claim_id`, `insured_id_number`, `insured_last_name`, `insured_first_name`, `insured_middle_name`, `insured_address1`, `insured_address2`, `insured_city`, `insured_state`, `insured_zip_code`, `other_insured_last_name`, `other_insured_first_name`, `other_insured_middle_name`, `insured_group_number`, `other_group_number`, `insurance_plan_name`, `insured_dob`, `insured_gender`, `iclaim_id`, `other_benefit_plan`) VALUES
(1, 1, 'efsdfsg', 'Raghuvarma', 'Rudroju', 'Test2', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'rawetert', 'ertretret', 'ssdgsd', '', 'dsfgdfgsdgsdg', 'dfgdfgsdgsdg', '2020-09-21', 'male', '7554745745', 'yes'),
(2, 2, '769769', 'Rudroju', 'Raghuvarma', 'Test2', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '57676', 'Rudroju', 'Raghuvarma', 'Test', '', 'wetwt', 'wwet', '1970-01-01', 'male', 'hhjhg7687', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `menues`
--

CREATE TABLE `menues` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menues`
--

INSERT INTO `menues` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'System Users', 1, '2020-09-25 00:57:27'),
(2, 'System Roles', 1, '2020-09-25 00:57:27'),
(3, 'Communications', 1, '2020-09-25 04:46:42'),
(4, 'Services', 1, '2020-09-25 04:46:42'),
(5, 'Insurance Companies', 1, '2020-09-25 04:47:16'),
(6, 'Forms', 1, '2020-09-25 04:47:16'),
(7, 'Insurance Forms', 1, '2020-09-25 04:47:16'),
(8, 'clients', 1, '2020-09-25 04:47:16'),
(9, 'client Services', 1, '2020-09-25 04:47:16'),
(10, 'Claims', 1, '2020-09-25 04:47:16'),
(11, 'Reports', 1, '2020-09-25 04:48:36'),
(12, 'Submissions', 1, '2020-09-25 04:48:36'),
(13, 'Sliders', 1, '2020-09-25 04:48:36'),
(14, 'About Us', 1, '2020-09-25 04:48:36'),
(15, 'Home Services', 1, '2020-09-25 04:49:23'),
(16, 'Features', 1, '2020-09-25 04:49:23'),
(17, 'Testimonials', 1, '2020-09-25 04:49:23'),
(18, 'All Contact Messages', 1, '2020-09-25 04:49:23'),
(19, 'Contact Info', 1, '2020-09-25 04:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `role_id`, `menu_id`, `permission_id`) VALUES
(1, 10, 1, 1),
(2, 10, 1, 2),
(3, 10, 1, 3),
(4, 10, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `timings` varchar(255) NOT NULL,
  `subject` varchar(20) NOT NULL,
  `subject2` varchar(500) NOT NULL,
  `rating` int(5) NOT NULL,
  `message` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `reply_status` int(5) NOT NULL DEFAULT '0',
  `review_status` int(11) NOT NULL DEFAULT '0',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_insured_information`
--

CREATE TABLE `patient_insured_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `patient_dob` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `patient_signature` varchar(255) NOT NULL,
  `insured_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_insured_information`
--

INSERT INTO `patient_insured_information` (`id`, `claim_id`, `last_name`, `first_name`, `middle_name`, `patient_dob`, `phone_number`, `gender`, `address1`, `address2`, `city`, `state`, `zip_code`, `account_number`, `relation`, `patient_signature`, `insured_signature`, `created_date`, `updated_date`) VALUES
(1, 1, 'Raghuvarma', 'Rudroju', 'Test2', '09/22/2020', '(987)-654-3210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-2222', '12345678998745', 'self', 'logo-white-2.png', 'logo-white.png', '2020-09-21 09:30:22', NULL),
(2, 2, 'Raghuvarma', 'Rudroju', 'Test2', '2020-09-23', '(987)-654-3210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-2222', '12345678998745', 'self', 'logo.png', 'logo-white.png', '2020-09-24 08:04:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 10:25:00'),
(2, 'Create', 1, '2020-09-25 10:25:00'),
(3, 'Update', 1, '2020-09-25 10:25:11'),
(4, 'Delete', 1, '2020-09-25 10:25:11'),
(5, 'Change Order', 1, '2020-09-25 10:25:46'),
(6, 'Show/Hide', 1, '2020-09-25 10:25:46'),
(7, 'Generate Report', 1, '2020-09-26 09:52:43'),
(8, 'Submit Claim', 1, '2020-09-26 09:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_lbn_name` varchar(25) NOT NULL,
  `client_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(25) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_id`, `client_lbn_name`, `client_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `last_name`, `first_name`, `middle_name`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(15, 'PHR0397162', 'Branch Brook client', 'BBP', 'www.test.com', 'bbp@gmail.com', '645645', '3466546', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 46465, '+1 (898) 987 9879', 'Raghuvarma', 'Rudroju', 'ttrhthg', '6456546', 1, '2020-09-17 04:39:11', '2020-09-17 04:39:11');

-- --------------------------------------------------------

--
-- Table structure for table `clients_services`
--

CREATE TABLE `clients_services` (
  `id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `start_date` varchar(25) NOT NULL,
  `end_date` varchar(25) NOT NULL,
  `price` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients_services`
--

INSERT INTO `clients_services` (`id`, `service_id`, `client_id`, `start_date`, `end_date`, `price`, `status`, `created_date`, `updated_date`) VALUES
(2, 2, 15, '09/24/2020', '09/25/2020', '55', 0, '2020-09-21 19:39:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients_users`
--

CREATE TABLE `clients_users` (
  `id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients_users`
--

INSERT INTO `clients_users` (`id`, `client_id`, `last_name`, `first_name`, `middle_name`, `username`, `email`, `password`, `phone_number`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 11, 'asdfasdf', 'asdfsadf', 'asfd', '', 'asfasf@gmail.com', '879NCI7', '+1 (224) 234 2343', '223432432', 1, '2020-09-22 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `physician_information`
--

CREATE TABLE `physician_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `reffering_name` varchar(255) NOT NULL,
  `federal_tax_number` varchar(255) NOT NULL,
  `other_id` varchar(255) NOT NULL,
  `npi` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `paid_amount` varchar(255) NOT NULL,
  `service_location_name` varchar(255) NOT NULL,
  `service_phone_number` varchar(255) NOT NULL,
  `service_address1` varchar(255) NOT NULL,
  `service_address2` varchar(255) NOT NULL,
  `service_city` varchar(255) NOT NULL,
  `service_state` varchar(255) NOT NULL,
  `service_zip_code` varchar(255) NOT NULL,
  `service_other_id` varchar(255) NOT NULL,
  `service_npi` varchar(255) NOT NULL,
  `admin_location_name` varchar(255) NOT NULL,
  `admin_phone_number` varchar(255) NOT NULL,
  `admin_address1` varchar(255) NOT NULL,
  `admin_address2` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_zip_code` varchar(255) NOT NULL,
  `admin_other_id` varchar(255) NOT NULL,
  `admin_npi` varchar(255) NOT NULL,
  `physician_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `physician_information`
--

INSERT INTO `physician_information` (`id`, `claim_id`, `reffering_name`, `federal_tax_number`, `other_id`, `npi`, `total_amount`, `paid_amount`, `service_location_name`, `service_phone_number`, `service_address1`, `service_address2`, `service_city`, `service_state`, `service_zip_code`, `service_other_id`, `service_npi`, `admin_location_name`, `admin_phone_number`, `admin_address1`, `admin_address2`, `admin_city`, `admin_state`, `admin_zip_code`, `admin_other_id`, `admin_npi`, `physician_signature`, `created_date`, `updated_date`) VALUES
(1, 1, 'Rudroju Test2 Raghuvarma', 'rturtu', '', '', '', '', 'dfhdfd', '+19876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-2222', '', '', 'dfhdfhdf', 'fcxbcvcvnc', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', 'cvncvn', '12345678901234567890123456', 'logo.png', '2020-09-21 09:30:22', NULL),
(2, 2, 'Rudroju Raghuvarma', 'rturtu', '46345', '1234567890', '33', '55', '', '(987)-654-3210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', 'retrtert', '(987)-654-3261', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '435346', '56456456', 'logo.png', '2020-09-24 08:04:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `rolename`, `description`, `is_active`, `last_ip`, `created_at`, `updated_at`) VALUES
(10, 'Marketing Manager', '', 1, '', '2020-08-31 11:08:02', '2020-08-31 11:08:02'),
(11, 'Manager', '', 1, '', '2020-08-17 02:08:09', '2020-08-17 02:08:09'),
(12, 'Admin', '', 0, '', '2020-08-31 11:08:17', '2020-08-31 11:08:17'),
(13, 'Manager', '', 1, '', '2020-09-01 04:09:39', '2020-09-01 04:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `status`, `created_date`, `updated_date`) VALUES
(2, 'SER6324533', 'claim service', 'test description', 1, '2020-09-21 19:39:21', '2020-09-21 19:39:21');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `slides_id` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` int(5) NOT NULL,
  `image` text NOT NULL,
  `sorting` varchar(50) NOT NULL,
  `type` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `slides_id`, `title`, `description`, `status`, `image`, `sorting`, `type`, `home_status`, `created_date`, `updated_date`) VALUES
(116, 'SLI7004870', 'admin', '<div class=\"tp-caption custom_title2 lft\" data-x=\"0\" data-y=\"166\" data-speed=\"1000\" data-start=\"1500\" data-easing=\"easeOutExpo\">Services</div>\r\n<div class=\"tp-caption custom_content sfl\" data-x=\"0\" data-y=\"224\" data-speed=\"1000\" data-start=\"2500\" data-easing=\"easeOutExpo\">Customized Labels <br>Custom Programming <br>Hardware Upgrade <br>Hardware Contracts</div>', 1, 'Chrysanthemum.jpg', '', 0, 0, '2020-09-22', '2020-09-22'),
(117, 'SLI3048946', 'test', '<div class=\"tp-caption custom_title2 lft\" data-x=\"0\" data-y=\"166\" data-speed=\"1000\" data-start=\"1500\" data-easing=\"easeOutExpo\">Services</div>\r\n<div class=\"tp-caption custom_content sfl\" data-x=\"0\" data-y=\"224\" data-speed=\"1000\" data-start=\"2500\" data-easing=\"easeOutExpo\">Customized Labels <br>Custom Programming <br>Hardware Upgrade <br>Hardware Contracts</div>', 1, 'slider-bg.jpg', '', 0, 0, '2020-09-22', '2020-09-22');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(3, 'Alabama', 'AL', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(5) NOT NULL,
  `testimonials_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `status` int(5) NOT NULL,
  `home_status` int(5) NOT NULL,
  `update_status` int(5) NOT NULL,
  `version` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials_id`, `name`, `designation`, `description`, `image`, `status`, `home_status`, `update_status`, `version`, `created_date`, `updated_date`) VALUES
(8, 'TES0810481', 'ennoble', 'Developer', 'admin System', '', 1, 0, 0, 0, '2020-09-22 12:48:08', '2020-09-22 12:48:08'),
(9, 'TES0147327', 'kumar praveen', 'Developer', 'admin Admin', '', 1, 0, 0, 0, '2020-09-22 12:46:39', '2020-09-22 12:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'Superadmin', 'admin', 'admin', '', 'admin@admin.com', '12345', '$2y$10$tFY/JX/rEKR8ODW2ktjYtOWf3zTkvOtynrXOvrcZ2Qm9h72r9TaPW', 0, '', '', '', '', '', 1, '', '2017-09-29 10:09:44', '2017-09-30 08:09:29'),
(5, 'wwe champion', 'wwe', 'champion', '', 'naumanahmedcs@gmail.com', '12345', '$2y$10$KB0NxzAOWtbnVj.7OJujRe7G5K1lb6UG5ra3PnAAt/Oc96Wfl5tea', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-10-03 06:10:51'),
(6, 'Ali Raza', 'Ali', 'Raza', '', 'ali@admin.com', '123456', '$2y$10$RoUcgnJ1AaK125c/hFmkWexGRvEhvQKXm21YRYlNrEHuvQcH2zMMG', 0, '', '', '', '', '', 0, '', '2017-10-03 06:10:31', '2017-10-03 05:10:25'),
(7, 'Test1', 'test', 'champion', '', 'test@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(8, 'John Smith', 'John', 'Smith', '', 'johnsmith@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(9, 'Herry Jhone', 'Herry', 'Jhone', '', 'herrypro@gmail.com', '449548545624', '$2y$10$.P.vz6NaSbLPq.BvOY0umulTKBj9Ovds2jaQBdGbyKzlfjOV0O4RW', 0, '', '', '', '', '', 0, '', '2017-10-03 07:10:26', '2017-10-03 07:10:26'),
(10, 'Rudroju Rudroju', 'Rudroju', 'Rudroju', '', 'raghuvarma.chintu@gmail.com', '9876543210', '$2y$10$nPXCXsKpUa1iCfm1Yr8Uj.sF6W8gPGvmIQ5vfRgXVej.qJOk4jQo6', 0, '', '', '', '', '', 10, '', '2020-08-17 12:08:51', '2020-08-17 12:08:51'),
(14, 'johnsmith', 'Smith', 'John', 'Adusumilli', 'anila@ennobletechnologies.com', '+1 (944) 017 2243', '$2y$10$ZoGoFBXbAZzb.WER6YbmpOLg9cu8cUd4frsAdoi3OiRtw7yGaPVgW', 0, '1700 W Blancke St', '', 'Linden', 'NJ', '07036', 11, '', '2020-09-25 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_diagnosis`
--
ALTER TABLE `claim_diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_information`
--
ALTER TABLE `claim_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_status`
--
ALTER TABLE `claim_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generated_claims`
--
ALTER TABLE `generated_claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeservices`
--
ALTER TABLE `homeservices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`form_id`);

--
-- Indexes for table `insurance_information`
--
ALTER TABLE `insurance_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menues`
--
ALTER TABLE `menues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_services`
--
ALTER TABLE `clients_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `clients_users`
--
ALTER TABLE `clients_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `physician_information`
--
ALTER TABLE `physician_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `claim_information`
--
ALTER TABLE `claim_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `claim_status`
--
ALTER TABLE `claim_status`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `generated_claims`
--
ALTER TABLE `generated_claims`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `homeservices`
--
ALTER TABLE `homeservices`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `insurance_information`
--
ALTER TABLE `insurance_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menues`
--
ALTER TABLE `menues`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `clients_services`
--
ALTER TABLE `clients_services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients_users`
--
ALTER TABLE `clients_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `physician_information`
--
ALTER TABLE `physician_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);

--
-- Constraints for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  ADD CONSTRAINT `foreign key` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clients_services`
--
ALTER TABLE `clients_services`
  ADD CONSTRAINT `clients_services_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clients_services_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
