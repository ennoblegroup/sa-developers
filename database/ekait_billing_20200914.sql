-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2020 at 07:42 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekait_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_roles`
--

CREATE TABLE `ci_roles` (
  `id` int(11) NOT NULL,
  `rolename` varchar(30) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_roles`
--

INSERT INTO `ci_roles` (`id`, `rolename`, `is_active`, `last_ip`, `created_at`, `updated_at`) VALUES
(10, 'Marketing Manager', 1, '', '2020-08-31 11:08:02', '2020-08-31 11:08:02'),
(11, 'Manager', 1, '', '2020-08-17 02:08:09', '2020-08-17 02:08:09'),
(12, 'Admin', 0, '', '2020-08-31 11:08:17', '2020-08-31 11:08:17'),
(13, 'Manager', 1, '', '2020-09-01 04:09:39', '2020-09-01 04:09:39'),
(16, 'Test Role', 1, '', '2020-09-14 00:00:00', '2020-09-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'demo', 'admin', 'admin', 'admin@admin.com', '12345', '$2y$10$tFY/JX/rEKR8ODW2ktjYtOWf3zTkvOtynrXOvrcZ2Qm9h72r9TaPW', 1, '', '2017-09-29 10:09:44', '2017-09-30 08:09:29'),
(7, 'Test1', 'test', 'champion', 'test@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(6, 'Ali Raza', 'Ali', 'Raza', 'ali@admin.com', '123456', '$2y$10$RoUcgnJ1AaK125c/hFmkWexGRvEhvQKXm21YRYlNrEHuvQcH2zMMG', 0, '', '2017-10-03 06:10:31', '2017-10-03 05:10:25'),
(5, 'wwe champion', 'wwe', 'champion', 'naumanahmedcs@gmail.com', '12345', '$2y$10$KB0NxzAOWtbnVj.7OJujRe7G5K1lb6UG5ra3PnAAt/Oc96Wfl5tea', 0, '', '2017-09-29 11:09:02', '2017-10-03 06:10:51'),
(8, 'John Smith', 'John', 'Smith', 'johnsmith@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(9, 'Herry Jhone', 'Herry', 'Jhone', 'herrypro@gmail.com', '449548545624', '$2y$10$.P.vz6NaSbLPq.BvOY0umulTKBj9Ovds2jaQBdGbyKzlfjOV0O4RW', 0, '', '2017-10-03 07:10:26', '2017-10-03 07:10:26'),
(10, 'Rudroju Rudroju', 'Rudroju', 'Rudroju', 'raghuvarma.chintu@gmail.com', '9876543210', '$2y$10$nPXCXsKpUa1iCfm1Yr8Uj.sF6W8gPGvmIQ5vfRgXVej.qJOk4jQo6', 10, '', '2020-08-17 12:08:51', '2020-08-17 12:08:51'),
(11, 'Rudroju Rudroju', 'Rudroju', 'Rudroju', 'raghuv@ennobletechnologies.com', '8686865707', '$2y$10$Edp7OaCuk/9.7yX7quKUXOUc4I6M5TJ9DT0BEKlgGvBi/haRRTXfq', 12, '', '2020-08-18 04:08:18', '2020-08-18 04:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `claim_id` varchar(255) NOT NULL,
  `claim_name` varchar(25) NOT NULL,
  `claim_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `form_name` varchar(25) NOT NULL,
  `form_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` int(10) NOT NULL,
  `insurance_company_id` varchar(255) NOT NULL,
  `insurance_company_name` varchar(30) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `naic_number` varchar(25) NOT NULL,
  `naic_group_code` varchar(25) NOT NULL,
  `state_of_domicile` varchar(25) NOT NULL,
  `company_type` varchar(25) NOT NULL,
  `company_status` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `insurance_company_id`, `insurance_company_name`, `ein_number`, `naic_number`, `naic_group_code`, `state_of_domicile`, `company_type`, `company_status`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `email_address`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR4553382', 'safsadf', '224234324', '23465', '2343', 'AL', '', '', 'asdf', 'asdf', 'ssfsdfsdf', 'AL', 23443, '+1 (434) 545 4545', 'asdfsaf@ga.ocm', 0, '2020-09-14 10:53:04', '2020-09-14 10:53:04');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_lbn_name` varchar(25) NOT NULL,
  `client_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(25) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_id`, `client_lbn_name`, `client_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `last_name`, `first_name`, `middle_name`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR7218611', 'Branch Brook client', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 0, '2020-09-01 04:39:21', '2020-09-01 04:39:21'),
(2, 'PHR3506657', 'Branch Brook clientSC', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 0, '2020-09-09 12:14:22', '2020-09-09 06:39:22'),
(3, 'PHR1425832', 'Branch Brook clientASsd', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 744657, '2147483647', '', '', '', '', 0, '2020-09-09 12:15:00', '2020-09-09 06:39:00'),
(7, 'PHR1883911', 'Branch Brook clientASsd', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 0, '2020-09-01 05:39:53', '2020-09-01 05:39:53'),
(8, 'PHR3843175', 'Test client 1', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 0, '2020-09-01 06:39:50', '2020-09-01 06:39:50'),
(9, 'PHR7657641', 'Test client 1', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 0, '2020-09-01 06:39:52', '2020-09-01 06:39:52'),
(10, 'PHR8640351', 'LBn', 'DBA', 'PWN', 'asdf@test.com', '1234', '5678', 'DEA123444', 'LN1234555', 'Address1', 'Address2', 'City', 'AL', 21212, '1', 'LN', 'FN', 'MN', '999999999', 0, '2020-09-13 18:30:00', '2020-09-13 18:30:00'),
(11, 'PHR4599320', 'asdf', 'asdf', 'asdf', 'asdf@ga.com', '343424', '3434343', '34fwsf', '324fsfsdf', 'asdfa', 'asdf', 'asdf', 'AL', 34343, '+1 (234) 242 3423', 'asdfa', 'asdf', 'asdfasdf', '324324324', 0, '2020-09-13 18:30:00', '2020-09-13 18:30:00'),
(12, 'PHR8381793', 'asdf', 'asdf', 'asdf', 'asdf@ga.com', '3243242', '4234234', '4sfdsdf', 'wr4234fds', 'asdfasf', 'asdfas', 'asdfsaf', 'CT', 34343, '+1 (343) 434 3434', 'asdf', 'asdf', 'asdf', '343432423', 0, '2020-09-13 18:30:00', '2020-09-13 18:30:00'),
(13, 'PHR2878444', 'zzzzzzzzzzzzzzzzzzz', 'cxxxxxxxxxxxxxxxxx', 'vvvvvvvvvvvvvvvvvv', 'asdf@cvcvddd.com', '1111111111', '2222222', 'aaaaaaaa1', 'b22222222', 'addr1', 'ddere2a', 'city', 'AR', 12344, '+1 (999) 999 9999', 'LN', 'FN', 'MN', '111111111', 0, '2020-09-14 10:23:08', '2020-09-13 18:30:00'),
(14, 'PHR7383399', 'sfds', 'asdfsaf', 'asdf', 'asf@asdfasdf.xcom', '223424', '2343243', 'asfas2342', 'asdf23423', 'asdf', 'asdf', 'asdf', 'CO', 24234, '+1 (234) 234 3243', 'asdfaf', 'asdf', 'adsf', '234234234', 0, '2020-09-13 18:30:00', '2020-09-13 18:30:00'),
(15, 'PHR5228686', 'asfsf', 'sdfsf', 'asdfasdf', 'asdfs@asfd.com', '23432432', '4324321', 'asfwf2342', 'asdf23342', 'asdf', 'asdf', 'asdf', 'AL', 34234, '+1 (234) 234 2343', 'aasdf', 'asdf', 'asdf', '234234234', 0, '2020-09-13 18:30:00', '2020-09-13 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `clients_services`
--

CREATE TABLE `clients_services` (
  `id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(3, 'Alabama', 'AL', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_roles`
--
ALTER TABLE `ci_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_services`
--
ALTER TABLE `clients_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_roles`
--
ALTER TABLE `ci_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `clients_services`
--
ALTER TABLE `clients_services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
