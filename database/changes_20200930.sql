-- Table structure for table `menues`
--

CREATE TABLE `menus` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'System Users', 1, '2020-09-25 00:57:27'),
(2, 'System Roles', 1, '2020-09-25 00:57:27'),
(3, 'Communications', 1, '2020-09-25 04:46:42'),
(4, 'Services', 1, '2020-09-25 04:46:42'),
(5, 'Insurance Companies', 1, '2020-09-25 04:47:16'),
(6, 'Forms', 1, '2020-09-25 04:47:16'),
(7, 'Insurance Forms', 1, '2020-09-25 04:47:16'),
(8, 'clients', 1, '2020-09-25 04:47:16'),
(9, 'client Services', 1, '2020-09-25 04:47:16'),
(10, 'Claims', 1, '2020-09-25 04:47:16'),
(11, 'Reports', 1, '2020-09-25 04:48:36'),
(12, 'Submissions', 1, '2020-09-25 04:48:36'),
(13, 'Sliders', 1, '2020-09-25 04:48:36'),
(14, 'About Us', 1, '2020-09-25 04:48:36'),
(15, 'Home Services', 1, '2020-09-25 04:49:23'),
(16, 'Features', 1, '2020-09-25 04:49:23'),
(17, 'Testimonials', 1, '2020-09-25 04:49:23'),
(18, 'All Contact Messages', 1, '2020-09-25 04:49:23'),
(19, 'Contact Info', 1, '2020-09-25 04:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `menus_permissions`
--

CREATE TABLE `menus_permissions` (
  `id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus_permissions`
--

INSERT INTO `menus_permissions` (`id`, `menu_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4);

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'View', 1, '2020-09-25 10:25:00'),
(2, 'Create', 1, '2020-09-25 10:25:00'),
(3, 'Update', 1, '2020-09-25 10:25:11'),
(4, 'Delete', 1, '2020-09-25 10:25:11'),
(5, 'Change Order', 1, '2020-09-25 10:25:46'),
(6, 'Show/Hide', 1, '2020-09-25 10:25:46'),
(7, 'Generate Report', 1, '2020-09-26 09:52:43'),
(8, 'Submit Claim', 1, '2020-09-26 09:52:43');

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` int(5) NOT NULL DEFAULT 0,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `rolename`, `description`, `status`, `last_ip`, `created_at`, `updated_at`) VALUES
(10, 'Marketing Manager', '', 1, '', '2020-08-31 11:08:02', '2020-08-31 11:08:02'),
(11, 'Manager', '', 0, '', '2020-08-17 02:08:09', '2020-08-17 02:08:09'),
(12, 'Admin', '', 1, '', '2020-08-31 11:08:17', '2020-08-31 11:08:17'),
(13, 'Manager', '', 0, '', '2020-09-01 04:09:39', '2020-09-01 04:09:39'),
(15, '', '', 0, '', '2020-09-30 00:00:00', '0000-00-00 00:00:00'),
(16, 'Admin1', '', 0, '', '2020-09-30 00:00:00', '0000-00-00 00:00:00'),
(17, 'Admin3', 'Admin 3', 1, '', '2020-09-30 00:00:00', '0000-00-00 00:00:00');

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL,
  `menu_id` int(5) NOT NULL,
  `permission_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `last_ip` varchar(30) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `is_admin`, `last_ip`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Superadmin', 'admin', 'admin', '', 'admin@admin.com', '12345', '$2y$10$UCFHEvy5oLiqySYtYkhRZOWxaOJOpi3o.Sa3qUqYzSWBhamrF.19G', 0, '', '', '', '', '', 1, '', 0, '2017-09-29 10:09:44', '2017-09-30 08:09:29'),
(5, 'wwe champion', 'wwe', 'champion', '', 'naumanahmedcs@gmail.com', '12345', '$2y$10$KB0NxzAOWtbnVj.7OJujRe7G5K1lb6UG5ra3PnAAt/Oc96Wfl5tea', 0, '', '', '', '', '', 0, '', 0, '2017-09-29 11:09:02', '2017-10-03 06:10:51'),
(6, 'Ali Raza', 'Ali', 'Raza', '', 'ali@admin.com', '123456', '$2y$10$RoUcgnJ1AaK125c/hFmkWexGRvEhvQKXm21YRYlNrEHuvQcH2zMMG', 0, '', '', '', '', '', 0, '', 0, '2017-10-03 06:10:31', '2017-10-03 05:10:25'),
(7, 'Test1', 'test', 'champion', '', 'test@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', 0, '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(8, 'John Smith', 'John', 'Smith', '', 'johnsmith@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', 0, '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(9, 'Herry Jhone', 'Herry', 'Jhone', '', 'herrypro@gmail.com', '449548545624', '$2y$10$.P.vz6NaSbLPq.BvOY0umulTKBj9Ovds2jaQBdGbyKzlfjOV0O4RW', 0, '', '', '', '', '', 0, '', 0, '2017-10-03 07:10:26', '2017-10-03 07:10:26'),
(10, 'Rudroju Rudroju', 'Rudroju', 'Rudroju', '', 'raghuvarma.chintu@gmail.com', '9876543210', '$2y$10$nPXCXsKpUa1iCfm1Yr8Uj.sF6W8gPGvmIQ5vfRgXVej.qJOk4jQo6', 0, '', '', '', '', '', 10, '', 0, '2020-08-17 12:08:51', '2020-08-17 12:08:51'),
(14, 'johnsmith', 'Smith', 'John', 'Adusumilli', 'anila@ennobletechnologies.com', '+1 (944) 017 2243', '$2y$10$ZoGoFBXbAZzb.WER6YbmpOLg9cu8cUd4frsAdoi3OiRtw7yGaPVgW', 0, '1700 W Blancke St', '', 'Linden', 'NJ', '07036', 11, '', 0, '2020-09-25 00:00:00', '0000-00-00 00:00:00');


--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  ADD PRIMARY KEY (`id`);

  --
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);
  --
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);
  
  --
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`id`);
  
  
  --
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);
  
  --
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menus_permissions`
--
ALTER TABLE `menus_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
  
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
  
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
  
  
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;