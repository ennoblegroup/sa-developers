-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 24, 2020 at 01:31 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekait_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_roles`
--

CREATE TABLE `ci_roles` (
  `id` int(11) NOT NULL,
  `rolename` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_roles`
--

INSERT INTO `ci_roles` (`id`, `rolename`, `description`, `is_active`, `last_ip`, `created_at`, `updated_at`) VALUES
(10, 'Marketing Manager', '', 1, '', '2020-08-31 11:08:02', '2020-08-31 11:08:02'),
(11, 'Manager', '', 1, '', '2020-08-17 02:08:09', '2020-08-17 02:08:09'),
(12, 'Admin', '', 0, '', '2020-08-31 11:08:17', '2020-08-31 11:08:17'),
(13, 'Manager', '', 1, '', '2020-09-01 04:09:39', '2020-09-01 04:09:39');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `middlename` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` int(5) NOT NULL,
  `address1` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(5) NOT NULL,
  `zip_code` varchar(30) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `middlename`, `email`, `mobile_no`, `password`, `user_role`, `address1`, `address2`, `city`, `state`, `zip_code`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'Superadmin', 'admin', 'admin', '', 'admin@admin.com', '12345', '$2y$10$tFY/JX/rEKR8ODW2ktjYtOWf3zTkvOtynrXOvrcZ2Qm9h72r9TaPW', 0, '', '', '', '', '', 1, '', '2017-09-29 10:09:44', '2017-09-30 08:09:29'),
(7, 'Test1', 'test', 'champion', '', 'test@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(6, 'Ali Raza', 'Ali', 'Raza', '', 'ali@admin.com', '123456', '$2y$10$RoUcgnJ1AaK125c/hFmkWexGRvEhvQKXm21YRYlNrEHuvQcH2zMMG', 0, '', '', '', '', '', 0, '', '2017-10-03 06:10:31', '2017-10-03 05:10:25'),
(5, 'wwe champion', 'wwe', 'champion', '', 'naumanahmedcs@gmail.com', '12345', '$2y$10$KB0NxzAOWtbnVj.7OJujRe7G5K1lb6UG5ra3PnAAt/Oc96Wfl5tea', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-10-03 06:10:51'),
(8, 'John Smith', 'John', 'Smith', '', 'johnsmith@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '', '', '', '', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(9, 'Herry Jhone', 'Herry', 'Jhone', '', 'herrypro@gmail.com', '449548545624', '$2y$10$.P.vz6NaSbLPq.BvOY0umulTKBj9Ovds2jaQBdGbyKzlfjOV0O4RW', 0, '', '', '', '', '', 0, '', '2017-10-03 07:10:26', '2017-10-03 07:10:26'),
(10, 'Rudroju Rudroju', 'Rudroju', 'Rudroju', '', 'raghuvarma.chintu@gmail.com', '9876543210', '$2y$10$nPXCXsKpUa1iCfm1Yr8Uj.sF6W8gPGvmIQ5vfRgXVej.qJOk4jQo6', 0, '', '', '', '', '', 10, '', '2020-08-17 12:08:51', '2020-08-17 12:08:51'),
(12, '', 'Anil', 'Kumar', 'Adusumilli', 'anilkumaradusumilli@gmail.com', '+1 (944) 017 2243', '$2y$10$U742q7CF3i6ElZFjsWbXOOK8Y8dNJSyRVxKES8Ne32A79Uy//rHzW', 12, 'Flat No 402 B BLOCK Venka', 'Ram Avenue Poranki', 'Vijayawada', 'AL', '90987-2344', 0, '', '2020-09-22 00:00:00', '2020-09-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `claims`
--

CREATE TABLE `claims` (
  `id` int(10) NOT NULL,
  `claim_registration_id` varchar(255) NOT NULL,
  `client_id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `claim_additional_information`
--

CREATE TABLE `claim_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `service_start_date` varchar(255) NOT NULL,
  `service_end_date` varchar(255) NOT NULL,
  `service_place` varchar(255) NOT NULL,
  `emg` varchar(255) NOT NULL,
  `cpt_hcps` varchar(255) NOT NULL,
  `modifier` varchar(255) NOT NULL,
  `diagnosis_pointer` varchar(255) NOT NULL,
  `charges` varchar(255) NOT NULL,
  `days` varchar(255) NOT NULL,
  `epsdt` varchar(255) NOT NULL,
  `id_qual` varchar(255) NOT NULL,
  `provider_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `claim_information`
--

CREATE TABLE `claim_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `employment` varchar(255) NOT NULL,
  `auto_accident` varchar(255) NOT NULL,
  `other_accident` varchar(255) NOT NULL,
  `accident_location` varchar(255) NOT NULL,
  `claim_code` varchar(255) NOT NULL,
  `injury_date` varchar(255) NOT NULL,
  `qual_date` varchar(255) NOT NULL,
  `nonworking_dates` varchar(255) NOT NULL,
  `hospital_dates` varchar(255) NOT NULL,
  `additional_claim_information` varchar(255) NOT NULL,
  `out_lab` varchar(255) NOT NULL,
  `total_charge` varchar(255) NOT NULL,
  `icd` varchar(255) NOT NULL,
  `icda` varchar(255) NOT NULL,
  `icdb` varchar(255) NOT NULL,
  `icdc` varchar(255) NOT NULL,
  `resubmission_code` varchar(255) NOT NULL,
  `original_reference_number` varchar(255) NOT NULL,
  `prior_aurthorization_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(10) NOT NULL,
  `communication_id` varchar(255) NOT NULL,
  `from_user_id` int(10) NOT NULL,
  `to_user_id` int(10) NOT NULL,
  `mail_to` varchar(255) NOT NULL,
  `mail_from` varchar(255) NOT NULL,
  `mail_subject` text NOT NULL,
  `mail_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`id`, `communication_id`, `from_user_id`, `to_user_id`, `mail_to`, `mail_from`, `mail_subject`, `mail_message`, `read_status`, `created_date`, `updated_date`) VALUES
(1, 'COM4039668', 3, 7, 'test@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(2, 'COM4039668', 3, 6, 'ali@admin.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(3, 'COM4039668', 3, 5, 'naumanahmedcs@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(4, 'COM4039668', 3, 8, 'johnsmith@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL),
(5, 'COM4039668', 3, 9, 'herrypro@gmail.com', 'admin@admin.com', 'Test Subject', '<p>Test Message</p>', '0', '2020-09-17 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `communications_replies`
--

CREATE TABLE `communications_replies` (
  `id` int(10) NOT NULL,
  `communication_id` int(10) NOT NULL,
  `email_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `reply_to` varchar(255) NOT NULL,
  `reply_from` varchar(255) NOT NULL,
  `reply_message` text NOT NULL,
  `read_status` enum('1','0') NOT NULL,
  `reply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `communications_replies`
--

INSERT INTO `communications_replies` (`id`, `communication_id`, `email_id`, `user_id`, `reply_to`, `reply_from`, `reply_message`, `read_status`, `reply_date`, `updated_date`) VALUES
(1, 5, 0, 3, '', 'admin@admin.com', 'Test Reply Message', '0', '2020-09-17 18:30:00', NULL),
(2, 5, 0, 3, '', 'admin@admin.com', 'second reply message', '0', '2020-09-17 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(10) NOT NULL,
  `file_source` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_description` text NOT NULL,
  `file` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `file_source`, `form_id`, `form_name`, `form_description`, `file`, `status`, `created_date`, `updated_date`) VALUES
(3, 'jghgvhg', 'FRM0497338', 'yhtyd', 'tyfhtyrdyrdy', 'd1f8a96f50c77ecede62b4b06642bd8e.pdf', 1, '2020-09-13 19:39:23', '2020-09-13 19:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_additional_information`
--

CREATE TABLE `insurance_additional_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_checks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_additional_information`
--

INSERT INTO `insurance_additional_information` (`id`, `claim_id`, `insured_checks`) VALUES
(1, 2, ''),
(2, 2, ''),
(3, 2, ''),
(4, 3, 'MEDICARE'),
(5, 3, 'TRICARE'),
(6, 3, 'FECA'),
(7, 4, 'MEDICARE'),
(8, 4, 'TRICARE'),
(9, 4, 'FECA'),
(10, 5, 'MEDICARE'),
(11, 5, 'TRICARE'),
(12, 5, 'FECA'),
(13, 6, 'MEDICARE'),
(14, 6, 'TRICARE'),
(15, 6, 'FECA'),
(16, 7, 'MEDICARE'),
(17, 8, 'MEDICARE');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` int(10) NOT NULL,
  `insurance_company_id` varchar(255) NOT NULL,
  `insurance_company_name` varchar(30) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `naic_number` varchar(25) NOT NULL,
  `naic_group_code` varchar(25) NOT NULL,
  `state_of_domicile` varchar(25) NOT NULL,
  `company_type` varchar(25) NOT NULL,
  `company_status` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `insurance_company_id`, `insurance_company_name`, `ein_number`, `naic_number`, `naic_group_code`, `state_of_domicile`, `company_type`, `company_status`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `email_address`, `status`, `created_date`, `updated_date`) VALUES
(0, 'PHR9145967', 'Test Insurance Company', '123456677', '45454', '3434', 'AL', 'fraternal', 'conservatorship', 'Test Address', '', 'Kanvas', 'AR', 21212, '+1 (343) 434 3434', 'test@gmail.com', 1, '2020-09-16 18:30:00', '2020-09-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_forms`
--

CREATE TABLE `insurance_forms` (
  `id` int(5) NOT NULL,
  `insurance_company_id` int(5) NOT NULL,
  `form_id` int(5) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_information`
--

CREATE TABLE `insurance_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `insured_id_number` varchar(255) NOT NULL,
  `insured_last_name` varchar(255) NOT NULL,
  `insured_first_name` varchar(255) NOT NULL,
  `insured_middle_name` varchar(255) NOT NULL,
  `insured_address1` varchar(255) NOT NULL,
  `insured_address2` varchar(255) NOT NULL,
  `insured_city` varchar(255) NOT NULL,
  `insured_state` varchar(255) NOT NULL,
  `insured_zip_code` varchar(255) NOT NULL,
  `other_insured_last_name` varchar(255) NOT NULL,
  `other_insured_first_name` varchar(255) NOT NULL,
  `other_insured_middle_name` varchar(255) NOT NULL,
  `insured_group_number` varchar(255) NOT NULL,
  `other_group_number` varchar(255) NOT NULL,
  `insurance_plan_name` varchar(255) NOT NULL,
  `insured_gender` varchar(255) NOT NULL,
  `iclaim_id` varchar(255) NOT NULL,
  `other_benefit_plan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_information`
--

INSERT INTO `insurance_information` (`id`, `claim_id`, `insured_id_number`, `insured_last_name`, `insured_first_name`, `insured_middle_name`, `insured_address1`, `insured_address2`, `insured_city`, `insured_state`, `insured_zip_code`, `other_insured_last_name`, `other_insured_first_name`, `other_insured_middle_name`, `insured_group_number`, `other_group_number`, `insurance_plan_name`, `insured_gender`, `iclaim_id`, `other_benefit_plan`) VALUES
(1, 1, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(2, 2, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(3, 3, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(4, 4, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(5, 5, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(6, 6, '', 'Raghuvarma', 'Rudroju', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', 'rawetert', 'ertretret', 'Test', '', '', '', 'male', '', 'yes'),
(7, 7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'male', '', 'yes'),
(8, 8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'male', '', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `patient_insured_information`
--

CREATE TABLE `patient_insured_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `patient_dob` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `relation` varchar(255) NOT NULL,
  `patient_signature` varchar(255) NOT NULL,
  `insured_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_insured_information`
--

INSERT INTO `patient_insured_information` (`id`, `claim_id`, `last_name`, `first_name`, `middle_name`, `patient_dob`, `phone_number`, `gender`, `address1`, `address2`, `city`, `state`, `zip_code`, `account_number`, `relation`, `patient_signature`, `insured_signature`, `created_date`, `updated_date`) VALUES
(1, 1, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:10:41', NULL),
(2, 2, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:11:08', NULL),
(3, 3, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:11:25', NULL),
(4, 4, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:11:52', NULL),
(5, 5, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:14:12', NULL),
(6, 6, 'Raghuvarma', 'Rudroju', '', '', '+19876543210', 'male', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', 'self', '', '', '2020-09-15 18:14:24', NULL),
(7, 7, '', '', '', '', '', 'male', '', '', '', '', '', '', 'self', '', '', '2020-09-16 05:51:41', NULL),
(8, 8, '', '', '', '', '', 'male', '', '', '', '', '', '', 'self', '', '', '2020-09-16 05:52:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_lbn_name` varchar(25) NOT NULL,
  `client_dba_name` varchar(25) NOT NULL,
  `website_name` varchar(25) NOT NULL,
  `business_email` varchar(25) NOT NULL,
  `npi_number` varchar(10) NOT NULL,
  `ncpdp_number` varchar(10) NOT NULL,
  `dea_number` varchar(25) NOT NULL,
  `license_number` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_id`, `client_lbn_name`, `client_dba_name`, `website_name`, `business_email`, `npi_number`, `ncpdp_number`, `dea_number`, `license_number`, `address1`, `address2`, `city`, `state`, `zip_code`, `phone_number`, `last_name`, `first_name`, `middle_name`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PHR7218611', 'Branch Brook client', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 1, '2020-09-16 09:24:22', '2020-09-16 09:24:22'),
(9, 'PHR7657641', 'Test client 1', '', '', '', '', '', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', 7470, '2147483647', '', '', '', '', 1, '2020-09-16 09:24:27', '2020-09-16 09:24:27'),
(11, 'PHR3667301', 'Mason', 'client', 'www.masonclient.com', 'admin@masonclient.com', '2343243243', '1213232', 'sdfgd7657', 'sfs435345', 'asfsafsdfsd', 'asfasddfsadfsdf', 'asdfasdffsd', 'AL', 34343, '', '', '', '', '', 1, '2020-09-22 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients_services`
--

CREATE TABLE `clients_services` (
  `id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `start_date` varchar(25) NOT NULL,
  `end_date` varchar(25) NOT NULL,
  `price` varchar(20) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients_services`
--

INSERT INTO `clients_services` (`id`, `service_id`, `client_id`, `start_date`, `end_date`, `price`, `status`, `created_date`, `updated_date`) VALUES
(2, 1, 1, '09/17/2020', '09/17/2020', '123', 0, '2020-09-16 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients_users`
--

CREATE TABLE `clients_users` (
  `id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `middle_name` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients_users`
--

INSERT INTO `clients_users` (`id`, `client_id`, `last_name`, `first_name`, `middle_name`, `username`, `email`, `password`, `phone_number`, `ein_number`, `status`, `created_date`, `updated_date`) VALUES
(1, 11, 'asdfasdf', 'asdfsadf', 'asfd', '', 'asfasf@gmail.com', '879NCI7', '+1 (224) 234 2343', '223432432', 1, '2020-09-22 18:30:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `physician_information`
--

CREATE TABLE `physician_information` (
  `id` int(10) NOT NULL,
  `claim_id` int(10) NOT NULL,
  `reffering_name` varchar(255) NOT NULL,
  `federal_tax_number` varchar(255) NOT NULL,
  `other_id` varchar(255) NOT NULL,
  `npi` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `paid_amount` varchar(255) NOT NULL,
  `service_location_name` varchar(255) NOT NULL,
  `service_phone_number` varchar(255) NOT NULL,
  `service_address1` varchar(255) NOT NULL,
  `service_address2` varchar(255) NOT NULL,
  `service_city` varchar(255) NOT NULL,
  `service_state` varchar(255) NOT NULL,
  `service_zip_code` varchar(255) NOT NULL,
  `admin_location_name` varchar(255) NOT NULL,
  `admin_phone_number` varchar(255) NOT NULL,
  `admin_address1` varchar(255) NOT NULL,
  `admin_address2` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_zip_code` varchar(255) NOT NULL,
  `other_id2` varchar(255) NOT NULL,
  `npi2` varchar(255) NOT NULL,
  `physician_signature` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `physician_information`
--

INSERT INTO `physician_information` (`id`, `claim_id`, `reffering_name`, `federal_tax_number`, `other_id`, `npi`, `total_amount`, `paid_amount`, `service_location_name`, `service_phone_number`, `service_address1`, `service_address2`, `service_city`, `service_state`, `service_zip_code`, `admin_location_name`, `admin_phone_number`, `admin_address1`, `admin_address2`, `admin_city`, `admin_state`, `admin_zip_code`, `other_id2`, `npi2`, `physician_signature`, `created_date`, `updated_date`) VALUES
(1, 1, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:10:41', NULL),
(2, 2, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:11:08', NULL),
(3, 3, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:11:25', NULL),
(4, 4, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:11:52', NULL),
(5, 5, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:14:12', NULL),
(6, 6, '', '', '', '', '', '', '', '9876543210', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470', '', '', '141, Mountainview Blvd', '', 'Wayne', 'NJ', '07470-6522', '', '', '', '2020-09-15 18:14:24', NULL),
(7, 7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-09-16 05:51:41', NULL),
(8, 8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-09-16 05:52:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `service_name` varchar(25) NOT NULL,
  `service_description` text NOT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_id`, `service_name`, `service_description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'SER0569923', 'Test Service', 'Test Service Description', 1, '2020-09-16 18:30:00', '2020-09-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(5) NOT NULL,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `state_name`, `state_code`, `status`) VALUES
(1, 'Alabama', 'AL', 1),
(2, 'Alaska', 'AK', 1),
(3, 'Alabama', 'AL', 1),
(4, 'Arizona', 'AZ', 1),
(5, 'Arkansas', 'AR', 1),
(6, 'California', 'CA', 1),
(7, 'Colorado', 'CO', 1),
(8, 'Connecticut', 'CT', 1),
(9, 'Delaware', 'DE', 1),
(10, 'District of Columbia', 'DC', 1),
(11, 'Florida', 'FL', 1),
(12, 'Georgia', 'GA', 1),
(13, 'Hawaii', 'HI', 1),
(14, 'Idaho', 'ID', 1),
(15, 'Illinois', 'IL', 1),
(16, 'Indiana', 'IN', 1),
(17, 'Iowa', 'IA', 1),
(18, 'Kansas', 'KS', 1),
(19, 'Kentucky', 'KY', 1),
(20, 'Louisiana', 'LA', 1),
(21, 'Maine', 'ME', 1),
(22, 'Maryland', 'MD', 1),
(23, 'Massachusetts', 'MA', 1),
(24, 'Michigan', 'MI', 1),
(25, 'Minnesota', 'MN', 1),
(26, 'Mississippi', 'MS', 1),
(27, 'Missouri', 'MO', 1),
(28, 'Montana', 'MT', 1),
(29, 'Nebraska', 'NE', 1),
(30, 'Nevada', 'NV', 1),
(31, 'New Hampshire', 'NH', 1),
(32, 'New Jersey', 'NJ', 1),
(33, 'New Mexico', 'NM', 1),
(34, 'New York', 'NY', 1),
(35, 'North Carolina', 'NC', 1),
(36, 'North Dakota', 'ND', 1),
(37, 'Ohio', 'OH', 1),
(38, 'Oklahoma', 'OK', 1),
(39, 'Oregon', 'OR', 1),
(40, 'Pennsylvania', 'PA', 1),
(41, 'Puerto Rico', 'PR', 1),
(42, 'Rhode Island', 'RI', 1),
(43, 'South Carolina', 'SC', 1),
(44, 'South Dakota', 'SD', 1),
(45, 'Tennessee', 'TN', 1),
(46, 'Texas', 'TX', 1),
(47, 'Utah', 'UT', 1),
(48, 'Vermont', 'VT', 1),
(49, 'Virginia', 'VA', 1),
(50, 'Washington', 'WA', 1),
(51, 'West Virginia', 'WV', 1),
(52, 'Wisconsin', 'WI', 1),
(53, 'Wyoming', 'WY', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_roles`
--
ALTER TABLE `ci_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_additional_information`
--
ALTER TABLE `claim_additional_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `claim_information`
--
ALTER TABLE `claim_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`communication_id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_additional_information`
--
ALTER TABLE `insurance_additional_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_forms`
--
ALTER TABLE `insurance_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign key` (`form_id`);

--
-- Indexes for table `insurance_information`
--
ALTER TABLE `insurance_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_insured_information`
--
ALTER TABLE `patient_insured_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_services`
--
ALTER TABLE `clients_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `clients_users`
--
ALTER TABLE `clients_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `physician_information`
--
ALTER TABLE `physician_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_roles`
--
ALTER TABLE `ci_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communications`
--
ALTER TABLE `communications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `communications_replies`
--
ALTER TABLE `communications_replies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `clients_services`
--
ALTER TABLE `clients_services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients_users`
--
ALTER TABLE `clients_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `communications_replies`
--
ALTER TABLE `communications_replies`
  ADD CONSTRAINT `communications_replies_ibfk_1` FOREIGN KEY (`communication_id`) REFERENCES `communications` (`id`);

--
-- Constraints for table `clients_services`
--
ALTER TABLE `clients_services`
  ADD CONSTRAINT `clients_services_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clients_services_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clients_users`
--
ALTER TABLE `clients_users`
  ADD CONSTRAINT `clients_users_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
