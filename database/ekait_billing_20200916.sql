
CREATE TABLE `insurance_companies` (
  `id` int(10) NOT NULL,
  `insurance_company_id` varchar(255) NOT NULL,
  `insurance_company_name` varchar(30) NOT NULL,
  `ein_number` varchar(25) NOT NULL,
  `naic_number` varchar(25) NOT NULL,
  `naic_group_code` varchar(25) NOT NULL,
  `state_of_domicile` varchar(25) NOT NULL,
  `company_type` varchar(25) NOT NULL,
  `company_status` varchar(25) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `city` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(50) DEFAULT NULL,
  `status` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `insurance_companies`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

